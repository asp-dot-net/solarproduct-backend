﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Storage;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Invoices;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using Abp;
using TheSolarProduct.LeadActivityLogs;
using Abp.Domain.Uow;
using Abp.Timing.Timezone;
using TheSolarProduct.Authorization;
using Abp.Authorization;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Exporting;
using TheSolarProduct.JobHistory;
using TheSolarProduct.Common;
using TheSolarProduct.Common.Dto;
using System.Globalization;
using TheSolarProduct.Timing;
using NUglify.Helpers;
using TheSolarProduct.Installer;
using TheSolarProduct.ApplicationSettings;
using System.Data;
using System.Configuration;
using TheSolarProduct.Invoices.Dtos;
using Hangfire.Common;
using Stripe;
using TheSolarProduct.Jobs.Exporting;
using Abp.Runtime.Session;
using System.Security.Cryptography;

namespace TheSolarProduct.Jobs
{
    [AbpAuthorize]
    public class JobInstallerInvoicesAppService : TheSolarProductAppServiceBase, IJobInstallerInvoicesAppService
    {
        private readonly IRepository<InstallerInvoice> _jobInstallerInvoiceRepository;
        private readonly IRepository<Job> _jobJobRepository;
        private readonly IRepository<Lead> _jobLeadsRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<JobType, int> _lookup_jobTypeRepository;
        private readonly IRepository<JobStatus, int> _lookup_jobStatusRepository;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<InvoiceFile> _InvoiceFileRepository;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IInvoicePaymentsExcelExporter _invoicePaymentsExcelExporter;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<PayWayData> _PayWayDataRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IInstallerAppService _installerServiceProxy;
        private readonly IRepository<InstallerInvoicePriceList> _installerInvoicePriceListServiceProxy;
        private readonly IRepository<InvoicePaymentMethod, int> _invoicePaymentMethodRepository;
        private readonly IJobsExcelExporter _jobsExcelExporter;
        public int MaxFileBytes { get; private set; }

        public JobInstallerInvoicesAppService(IRepository<InstallerInvoice> jobInstallerInvoiceRepository,
            IRepository<Job> jobJobRepository,
            IRepository<Lead> jobLeadsRepository,
             IRepository<User, long> userRepository,
             IRepository<UserRole, long> userroleRepository,
             IRepository<Role> roleRepository,
             IRepository<UserTeam> userTeamRepository,
             ITempFileCacheManager tempFileCacheManager,
              IWebHostEnvironment env,
              IRepository<Tenant> tenantRepository,
              UserManager userManager,
              IRepository<JobType, int> lookup_jobTypeRepository,
              IRepository<JobStatus, int> lookup_jobStatusRepository,
              IRepository<ProductItem> productItemRepository,
               IRepository<JobProductItem> jobProductItemRepository,
               IRepository<InvoiceFile> InvoiceFileRepository,
               IRepository<InvoiceImportData> InvoiceImportDataRepository,
               IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
               IRepository<LeadActivityLog> leadactivityRepository,
               IUnitOfWorkManager unitOfWorkManager,
               ITimeZoneConverter timeZoneConverter, IRepository<InvoicePayment> invoicePaymentRepository,
               IInvoicePaymentsExcelExporter invoicePaymentsExcelExporter,
               IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
               IRepository<PayWayData> PayWayDataRepository,
               ICommonLookupAppService CommonDocumentSaveRepository,
               ITimeZoneService timeZoneService,
               IInstallerAppService installerServiceProxy
            , IRepository<InstallerInvoicePriceList> installerInvoicePriceListServiceProxy
            , IRepository<InvoicePaymentMethod, int> invoicePaymentMethodRepository
            , IJobsExcelExporter jobsExcelExporter
            )
        {
            _jobInstallerInvoiceRepository = jobInstallerInvoiceRepository;
            _jobJobRepository = jobJobRepository;
            _jobLeadsRepository = jobLeadsRepository;
            _userRepository = userRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _userTeamRepository = userTeamRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _env = env;
            _tenantRepository = tenantRepository;
            _userManager = userManager;
            _lookup_jobTypeRepository = lookup_jobTypeRepository;
            _lookup_jobStatusRepository = lookup_jobStatusRepository;
            _productItemRepository = productItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _InvoiceFileRepository = InvoiceFileRepository;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _dbcontextprovider = dbcontextprovider;
            _leadactivityRepository = leadactivityRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _timeZoneConverter = timeZoneConverter;
            _invoicePaymentRepository = invoicePaymentRepository;
            _invoicePaymentsExcelExporter = invoicePaymentsExcelExporter;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _PayWayDataRepository = PayWayDataRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _timeZoneService = timeZoneService;
            _installerServiceProxy = installerServiceProxy;
            _installerInvoicePriceListServiceProxy = installerInvoicePriceListServiceProxy;
            _invoicePaymentMethodRepository = invoicePaymentMethodRepository;
            _jobsExcelExporter = jobsExcelExporter;
        }

        public async Task CreateOrEdit(CreateOrEditJobInstallerInvoiceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        public bool CheckInstallerDetail(CreateOrEditJobInstallerInvoiceDto input)
        {
            if (input.Id != null)
            {

                bool existdata = _jobInstallerInvoiceRepository.GetAll().Where(e => e.InvoiceNo == input.InvoiceNo && e.InstallerId == input.InstallerId && e.Id != input.Id).Any();
                return existdata;
            }
            else
            {
                bool existdata = _jobInstallerInvoiceRepository.GetAll().Where(e => e.InvoiceNo == input.InvoiceNo && e.InstallerId == input.InstallerId).Any();
                return existdata;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Installer_New_Create)]
        protected virtual async Task Create(CreateOrEditJobInstallerInvoiceDto input)
        {
            try
            {
                input.InvDate = _timeZoneConverter.Convert(input.InvDate, (int)AbpSession.TenantId);
                input.invoiceIssuedDate = _timeZoneConverter.Convert(input.invoiceIssuedDate, (int)AbpSession.TenantId);

                var InstallerInvoice = ObjectMapper.Map<InstallerInvoice>(input);
                InstallerInvoice.invoiceIssuedDate = InstallerInvoice.invoiceIssuedDate.Value.Date;
                InstallerInvoice.InvDate = InstallerInvoice.InvDate.Value.Date;

                if (AbpSession.TenantId != null)
                {
                    InstallerInvoice.TenantId = (int)AbpSession.TenantId;
                }

                if (input.FileName != null)
                {
                    var ext = Path.GetExtension(input.FileName);
                    string filename = DateTime.Now.Ticks + "_" + "InstallerInv" + ext;
                    byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
                    var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, filename, "InstallerInvoices", input.JobId, 0, AbpSession.TenantId);
                    InstallerInvoice.FileName = filepath.filename;
                    InstallerInvoice.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\" + "InstallerInvoices" + "\\";


                }
                var insInvoId = await _jobInstallerInvoiceRepository.InsertAndGetIdAsync(InstallerInvoice);

                if (input.installerInvoicePriceListDtos != null)
                {
                    foreach (var itemPrice in input.installerInvoicePriceListDtos)
                    {
                        var installerInvoicePrice = ObjectMapper.Map<InstallerInvoicePriceList>(itemPrice);
                        installerInvoicePrice.InstallerInvoId = insInvoId;
                        //if (AbpSession.TenantId != null)
                        //{
                        //    jobvar.TenantId = (int)AbpSession.TenantId;
                        //}
                        _installerInvoicePriceListServiceProxy.Insert(installerInvoicePrice);
                    }
                }


                LeadActivityLog leadactivity = new LeadActivityLog();
                leadactivity.ActionId = 26;
                leadactivity.SectionId = 20; //New Installer Invoice Page
                leadactivity.ActionNote = "Installer Invoices Created";
                leadactivity.LeadId = (int)(_jobJobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => e.LeadId).FirstOrDefault());
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            catch (Exception e) { }
        }

        [AbpAuthorize(AppPermissions.Pages_Installer_New_Edit)]
        protected virtual async Task Update(CreateOrEditJobInstallerInvoiceDto input)
        {
            input.InvDate = _timeZoneConverter.Convert(input.InvDate, (int)AbpSession.TenantId);
            input.invoiceIssuedDate = _timeZoneConverter.Convert(input.invoiceIssuedDate, (int)AbpSession.TenantId);

            var installerInvoice = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;
            leadactivity.ActionNote = "Installer Invoice Modified";
            leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            if (installerInvoice.FileName != input.FileName && input.FileName != null)
            {
                var ext = Path.GetExtension(input.FileName);
                input.FileName = DateTime.Now.Ticks + "_" + "InstallerInv" + ext;
                byte[] ByteArray = _tempFileCacheManager.GetFile(input.FileToken);
                var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, input.FileName, "InstallerInvoices", input.JobId, 0, AbpSession.TenantId);
                input.FileName = filepath.filename;
                input.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + filepath.JobNumber + "\\" + "InstallerInvoices" + "\\";
            }
            var insInvoPriceIds = input.installerInvoicePriceListDtos.Select(e => e.Id);

            var insInvoPrice = await _installerInvoicePriceListServiceProxy.GetAll().Where(e => e.InstallerInvoId == input.Id).ToListAsync();
            foreach (var item in insInvoPrice)
            {
                if (!insInvoPriceIds.Contains(item.Id))
                {
                    _installerInvoicePriceListServiceProxy.DeleteAsync(item.Id);
                }
            }

            if (input.installerInvoicePriceListDtos != null)
            {
                foreach (var itemPrice in input.installerInvoicePriceListDtos)
                {
                    if (itemPrice.Id == null || itemPrice.Id == 0)
                    {

                        var installerInvoicePrice = ObjectMapper.Map<InstallerInvoicePriceList>(itemPrice);
                        installerInvoicePrice.InstallerInvoId = input.Id;
                        //if (AbpSession.TenantId != null)
                        //{
                        //    jobvar.TenantId = (int)AbpSession.TenantId;
                        //}
                        _installerInvoicePriceListServiceProxy.Insert(installerInvoicePrice);
                    }
                    else
                    {
                        var installerInvoicePrice = await _installerInvoicePriceListServiceProxy.FirstOrDefaultAsync((int)itemPrice.Id);
                        ObjectMapper.Map(itemPrice, installerInvoicePrice);

                    }


                }
            }


            #region Activity History
            var list = new List<JobTrackerHistory>();
            if (installerInvoice.Installation_Maintenance_Inspection != input.Installation_Maintenance_Inspection)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Invoice Type";
                jobhistory.PrevValue = installerInvoice.Installation_Maintenance_Inspection;
                jobhistory.CurValue = input.Installation_Maintenance_Inspection;
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.InstallerId != input.InstallerId)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Installer Name";
                jobhistory.PrevValue = _userRepository.GetAll().Where(e => e.Id == installerInvoice.InstallerId).Select(e => e.FullName).FirstOrDefault();
                jobhistory.CurValue = input.InstallerName;
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.InvNo != input.InvNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Inv No";
                jobhistory.PrevValue = installerInvoice.InvNo.ToString();
                jobhistory.CurValue = input.InvNo.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Amount != input.Amount)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Invoice Amount";
                jobhistory.PrevValue = installerInvoice.Amount.ToString();
                jobhistory.CurValue = input.Amount.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.InvDate.Value.Date != input.InvDate.Value.Date)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Inv Date";
                jobhistory.PrevValue = installerInvoice.InvDate.Value.Date.ToString();
                jobhistory.CurValue = input.InvDate.Value.Date.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.invoiceIssuedDate.Value.Date != input.invoiceIssuedDate.Value.Date)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Invoice Issued Date";
                jobhistory.PrevValue = installerInvoice.invoiceIssuedDate.Value.Date.ToString();
                jobhistory.CurValue = input.invoiceIssuedDate.Value.Date.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.FileName != input.FileName)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "File";
                jobhistory.PrevValue = installerInvoice.FileName;
                jobhistory.CurValue = input.FileName;
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Notes != input.Notes)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Notes";
                jobhistory.PrevValue = installerInvoice.Notes;
                jobhistory.CurValue = input.Notes;
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            #endregion

            installerInvoice.Installation_Maintenance_Inspection = input.Installation_Maintenance_Inspection;
            installerInvoice.InstallerId = input.InstallerId;
            installerInvoice.InvNo = input.InvNo;
            installerInvoice.InvoiceNo = input.InvoiceNo;
            installerInvoice.Amount = input.Amount;
            installerInvoice.InvDate = input.InvDate.Value.Date;
            installerInvoice.invoiceIssuedDate = input.invoiceIssuedDate.Value.Date;
            installerInvoice.FileName = input.FileName;
            installerInvoice.FilePath = input.FilePath;
            installerInvoice.Notes = input.Notes;
            await _jobInstallerInvoiceRepository.UpdateAsync(installerInvoice);
        }

        [AbpAuthorize(AppPermissions.Pages_Installer_New_Delete)]
        public async Task Delete(EntityDto input)
        {
            var invoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.Id);

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = 20; //New Installer Invoice Page
            leadactivity.ActionNote = "Installer Invoices Deleted";
            leadactivity.LeadId = (int)(_jobJobRepository.GetAll().Where(e => e.Id == invoice.JobId).Select(e => e.LeadId).FirstOrDefault());
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);

            await _jobInstallerInvoiceRepository.DeleteAsync(invoice);
        }

        public async Task<PagedResultDto<InstallerNewDto>> GetAll(InstallerNewInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var result = new List<InstallerNewDto>();
            var totalCount = 0;

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            if (input.DateFilter == "CreationDate")
            {
                var filteredLeads = _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Include(e => e.JobFk.LeadFk)
                //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvNo.ToString().Contains(input.Filter))
                .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                .WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvoiceNo == input.Filter)
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                .WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                .WhereIf(input.DateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.DateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5))

                .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5)) //For with battery


                .Where(e => e.IsPaid == false && e.IsVerify == false && e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);

                var pagedAndFilteredLeads = filteredLeads.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                result = await (from o in pagedAndFilteredLeads

                                join user in _userRepository.GetAll() on (int)o.InstallerId equals (int)user.Id into userjoined
                                from user in userjoined.DefaultIfEmpty()

                                select new InstallerNewDto
                                {
                                    ProjectName = o.JobFk.JobNumber,
                                    Customer = o.JobFk.LeadFk.CompanyName,
                                    Address = o.JobFk.Address,
                                    Suburb = o.JobFk.Suburb,
                                    State = o.JobFk.State,
                                    PCode = o.JobFk.PostalCode,
                                    InstallDate = o.JobFk.InstallationDate,
                                    InstallComplate = o.JobFk.InstalledcompleteDate,
                                    Ammount = o.Amount,
                                    InvDate = o.InvDate,
                                    Issued = o.InvDate,
                                    PaymentDate = o.PayDate,
                                    PaymentStaus = o.AdvancePayDate,
                                    id = o.Id,
                                    installername = user.Name + "/" + user.CompanyName,
                                    invoiceType = o.Installation_Maintenance_Inspection,
                                    invoiceNotes = o.Notes,
                                    InvNo = o.InvNo,
                                    InvoiceNO = o.InvoiceNo,
                                    invoiceinstallerId = o.Id,
                                    Filenameinv = o.FileName,
                                    FilePathinv = o.FilePath,
                                    JobId = o.JobId
                                    //Summary = summary
                                }).ToListAsync();


                totalCount = await filteredLeads.CountAsync();
            }
                

            if(input.DateFilter == "InstallComplete" || input.DateFilter == "InstallDate")
            {
                var installerInvoiceIds = _jobInstallerInvoiceRepository.GetAll().AsNoTracking().Select(e => e.JobId);

                var filteredjob = _jobJobRepository.GetAll().Include(e => e.LeadFk)
                //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvNo.ToString().Contains(input.Filter))
                .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                // .WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvNo.ToString() == input.Filter)
                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                // .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                .WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                .WhereIf(input.DateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.DateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                
                .WhereIf(input.DateFilter == "InstallDate" && input.StartDate != null, e => e.InstallationDate.Value.Date >= SDate.Value.Date)
                .WhereIf(input.DateFilter == "InstallDate" && input.EndDate != null, e => e.InstallationDate.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5)) //For with battery


                .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && !installerInvoiceIds.Any(i => i == e.Id))
                ;

                var pagedAndFilteredLeads = filteredjob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                result = await (from o in pagedAndFilteredLeads

                                join user in _userRepository.GetAll() on (int)o.InstallerId equals (int)user.Id into userjoined
                                from user in userjoined.DefaultIfEmpty()

                                select new InstallerNewDto
                                {
                                    ProjectName = o.JobNumber,
                                    Customer = o.LeadFk.CompanyName,
                                    Address = o.Address,
                                    Suburb = o.Suburb,
                                    State = o.State,
                                    PCode = o.PostalCode,
                                    InstallDate = o.InstallationDate,
                                    InstallComplate = o.InstalledcompleteDate,
                                    //Ammount = o.Amount,
                                    //InvDate = o.InvDate,
                                    //Issued = o.InvDate,
                                    //PaymentDate = o.PayDate,
                                    //PaymentStaus = o.AdvancePayDate,
                                    id = 0,
                                    installername = user.Name + "/" + user.CompanyName,
                                    //invoiceType = o.Installation_Maintenance_Inspection,
                                    //invoiceNotes = o.Notes,
                                    //InvNo = o.InvNo,
                                    //InvoiceNO = o.InvoiceNo,
                                    //invoiceinstallerId = o.Id,
                                   // Filenameinv = o.FileName,
                                    //FilePathinv = o.FilePath,
                                    JobId = o.Id,
                                    //Summary = summary
                                }).ToListAsync();


                totalCount = await filteredjob.CountAsync();
            }

            //InstallerNewSummaryCountDto summary = new InstallerNewSummaryCountDto();
            //summary.Total = filteredLeads.Count();
            //summary.Pending = filteredLeads.Where(e => e.IsVerify != true).Count();
            //summary.Verified = filteredLeads.Where(e => e.IsVerify != true).Count();

            return new PagedResultDto<InstallerNewDto>(totalCount, result);
        }

        [UnitOfWork]
        public async Task<List<GetSearchResultDto>> GetSearchFilter(string str, string filterName)
        {
            TextInfo textInfo = new CultureInfo("en-GB", false).TextInfo;

            var orgid = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.OrganizationUnitId).ToList();

            var listOfResult = (from lead in _jobLeadsRepository.GetAll()
                                join job in _jobJobRepository.GetAll() on lead.Id equals job.LeadId into jobjoined
                                from job in jobjoined.DefaultIfEmpty()

                                join ur in _userRepository.GetAll() on lead.AssignToUserID equals (int?)ur.Id into urJoined
                                from ur in urJoined.DefaultIfEmpty()

                                where (orgid.Contains(lead.OrganizationId) && lead.AssignToUserID != null)
                                where ((filterName == "cust" && (job.JobNumber == str || lead.CompanyName == str || lead.Mobile == str || lead.Email == str))
                                        || (filterName == "JobNumber" && job.JobNumber == str
                                         || (filterName == "MobileNo" && lead.Mobile == str)
                                         || (filterName == "Email" && lead.Email == str)
                                         || (filterName == "PhoneNo" && lead.Phone == str)
                                         //|| (filterName == "CompanyName" && lead.CompanyName == str.Trim())
                                         //|| (filterName == "Addess" && lead.Address == str.Trim())
                                         ))

                                let companyName = (string.IsNullOrEmpty(lead.CompanyName) ? "" : Convert.ToString(lead.CompanyName))
                                let jobnumber = (string.IsNullOrEmpty(job.JobNumber) ? "" : Convert.ToString(job.JobNumber))
                                let Address = (string.IsNullOrEmpty(lead.Address) ? "" : Convert.ToString(lead.Address + ", "))
                                let Suburb = (string.IsNullOrEmpty(lead.Suburb) ? "" : Convert.ToString(lead.Suburb + ", "))
                                let State = (string.IsNullOrEmpty(lead.State) ? "" : Convert.ToString(lead.State + "-"))
                                let PostalCode = (string.IsNullOrEmpty(lead.PostCode) ? "" : Convert.ToString(lead.PostCode))
                                let FullAddress = string.IsNullOrEmpty(Address + Suburb + State + PostalCode) ? "" : (Address + Suburb + State + PostalCode).Trim().ToLower()

                                select new GetSearchResultDto
                                {
                                    LeadId = lead.Id,
                                    CustomerName = textInfo.ToTitleCase(companyName.Trim().ToLower()),
                                    JobNumber = job.JobNumber,
                                    Mobile = lead.Mobile,
                                    Email = lead.Email,
                                    //Address = textInfo.ToTitleCase((Address + Suburb + State + PostalCode).ToString().Trim().ToLower()),
                                    Address = textInfo.ToTitleCase(FullAddress),
                                    SalesRep = ur.FullName
                                });

            return await listOfResult.ToListAsync();
        }

        [UnitOfWork]
        public async Task<List<GetSearchResultDto>> GetSearchFilterByOrg(string str, string InvoiceType, int OrganizationId)
        {
            TextInfo textInfo = new CultureInfo("en-GB", false).TextInfo;

            var jobs = _jobJobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.JobNumber.Contains(str) && e.LeadFk.OrganizationId == OrganizationId )
                .WhereIf(InvoiceType == "Installation", e => e.InstallationDate != null);

            var listOfResult = (from o in jobs

                                let jobnumber = (Convert.ToString(o.JobNumber) == null || Convert.ToString(o.JobNumber) == "" ? "" : Convert.ToString(o.JobNumber))
                                let Address = (Convert.ToString(o.Address) == null || Convert.ToString(o.Address) == "" ? "" : Convert.ToString(o.Address + ", "))
                                let Suburb = (Convert.ToString(o.Suburb) == null || Convert.ToString(o.Suburb) == "" ? "" : Convert.ToString(o.Suburb + ", "))
                                let State = (Convert.ToString(o.State) == null || Convert.ToString(o.State) == "" ? "" : Convert.ToString(o.State + "-"))
                                let PostalCode = (Convert.ToString(o.PostalCode) == null || Convert.ToString(o.PostalCode) == "" ? "" : Convert.ToString(o.PostalCode))
                                select new GetSearchResultDto
                                {
                                    JobId = o.Id,
                                    LeadId = o.LeadFk.Id,
                                    CustomerName = textInfo.ToTitleCase(o.LeadFk.CompanyName.Trim().ToLower()),
                                    JobNumber = o.JobNumber,
                                    Mobile = o.LeadFk.Mobile,
                                    Email = o.LeadFk.Email,
                                    Address = textInfo.ToTitleCase((Address + Suburb + State + PostalCode).ToString().Trim().ToLower()),
                                    InstallerId = o.InstallerId

                                });

            return await listOfResult.ToListAsync();
        }

        public async Task<List<string>> GetInstallerForDropdown(string leadSourceName)
        {
            var User_List = _userRepository
             .GetAll().Where(e => e.Name.Contains(leadSourceName) || e.Surname.Contains(leadSourceName) || e.CompanyName.Contains(leadSourceName));
            var ListOfuser = (from user in User_List
                              join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                              from ur in urJoined.DefaultIfEmpty()
                              join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                              from us in usJoined.DefaultIfEmpty()
                              where (us != null && us.DisplayName == "Installer")
                              select new jobnoLookupTableDto()
                              {
                                  Value = (int)user.Id,
                                  Name = user.Name == null || user.FullName == null ? "" : user.Name.ToString() + "." + user.Surname.ToString() + "| " + user.CompanyName
                              });
            return await ListOfuser
                   .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }

        public async Task<CreateOrEditJobInstallerInvoiceDto> GetDataForEdit(EntityDto input)
        {
            var installerInvoice = await _jobInstallerInvoiceRepository.GetAsync(input.Id);
            var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(installerInvoice);
            output.IsGSTInclude = output.IsGSTInclude == true ? true : false;
            var job = await _jobJobRepository.FirstOrDefaultAsync((int)output.JobId);
            var insInvoPrice = await _installerInvoicePriceListServiceProxy.GetAll().Include(e => e.PriceItemListFK).Where(e => e.InstallerInvoId == output.Id).ToListAsync();

            if (output != null)
            {
                output.JobNumber = job.JobNumber;
                output.InstallerName = _userRepository.GetAll().Where(e => e.Id == output.InstallerId).Select(e => e.FullName).FirstOrDefault();
                output.InstallerCompanyName = _userRepository.GetAll().Where(e => e.Id == output.InstallerId).Select(e => e.CompanyName).FirstOrDefault();
                //output.installerInvoicePriceListDtos = new List<InstallerInvoicePriceListDto>();
                output.installerInvoicePriceListDtos = (from o in insInvoPrice
                                                        select new InstallerInvoicePriceListDto()
                                                        {
                                                            Id = o.Id,
                                                            InstallerInvoId = o.InstallerInvoId,
                                                            PriceItemId = o.PriceItemId,
                                                            Amount = o.Amount,
                                                            Notes = o.Notes,
                                                            PriceItem = o.PriceItemListFK== null ?"": o.PriceItemListFK.Name
                                                        }).ToList();
            }

            return output;
        }

        public async Task<PagedResultDto<GetInstallerInvoiceForViewDto>> GetPendingInstallerInvoice(GetAllInstallerInvoiceInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredInstallerInvoice = _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Include(e => e.JobFk.LeadFk).Include(e => e.JobFk.PaymentOptionFk)
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.IsPaid == false);

            #region Summary Count
            InstallerSummaryCountDto summary = new InstallerSummaryCountDto();
            summary.Total = filteredInstallerInvoice.Count();
            summary.Pending = filteredInstallerInvoice.Where(e => e.IsVerify == false).Count();
            summary.Verified = filteredInstallerInvoice.Where(e => e.IsVerify == true).Count();
            #endregion

            var filter = filteredInstallerInvoice
                 //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvNo.ToString().Contains(input.Filter))
                 .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                 .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                 .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                 .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                 .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                 .WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvoiceNo == input.Filter)
                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                 .WhereIf(input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.Suburb), e => e.JobFk.Suburb == input.Suburb)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.State == input.State)

                 .WhereIf(input.DateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                 .WhereIf(input.DateFilter == "InstallDate" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "InstallDate" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.DateFilter == "InstallCompleteDate" && input.StartDate != null, e => e.JobFk.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "InstallCompleteDate" && input.EndDate != null, e => e.JobFk.InstalledcompleteDate.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.InvoiceStatus == 1, e => e.IsVerify == false)
                 .WhereIf(input.InvoiceStatus == 2, e => e.IsVerify == true);

            var pagedAndFilteredJobs = filter.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 29 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            var result = (from o in pagedAndFilteredJobs

                          join user in _userRepository.GetAll() on o.InstallerId equals (int)user.Id into userjoined
                          from user in userjoined.DefaultIfEmpty()

                          select new GetInstallerInvoiceForViewDto()
                          {
                              Id = o.Id,
                              JobId = o.JobFk.Id,
                              LeadId = (int)o.JobFk.LeadId,
                              IsVerify = o.IsVerify,
                              PaidStatus = o.IsPaid,
                              JobNumber = o.JobFk.JobNumber,
                              State = o.JobFk.State,
                              InstallerName = user != null ? user.FullName : "",
                              InstallerCompanyName = user != null ? user.CompanyName : "",
                              Installation_Maintenance_Inspection = o.Installation_Maintenance_Inspection,
                              InvNo = o.InvNo,
                              Amount = o.Amount,
                              InvDate = o.InvDate,
                              InvoiceIssuedDate = o.invoiceIssuedDate,
                              InstalledCompleteDate = o.JobFk.InstalledcompleteDate,
                              TotalCost = o.JobFk.TotalCost,
                              OwningAmt = (o.JobFk.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.JobId).Any() ? _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.JobId).Select(e => e.PaidAmmount).Sum() : 0)),
                              RemarkIfOwing = o.Remark_if_owing,
                              FinanceName = o.JobFk.PaymentOptionFk.Name,
                              GridConnection = o.JobFk.IsGridConnected,
                              NotesOrReasonforPending = o.NotesOrReasonforPending,
                              InvoiceNo = o.InvoiceNo,

                              ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),

                              SummaryCount = summary,

                          });

            var totalCount = await filter.CountAsync();

            return new PagedResultDto<GetInstallerInvoiceForViewDto>(
                totalCount,
                await result.ToListAsync()
            );
        }

        public async Task<FileDto> GetPendingInstallerInvoiceExcel(GetAllInstallerInvoiceExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredInstallerInvoice = _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Include(e => e.JobFk.LeadFk).Include(e => e.JobFk.PaymentOptionFk)
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.IsPaid == false);

            

            var filter = filteredInstallerInvoice
                 //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvNo.ToString().Contains(input.Filter))
                 .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                 .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                 .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                 .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                 .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                 .WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvoiceNo == input.Filter)
                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                 .WhereIf(input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.Suburb), e => e.JobFk.Suburb == input.Suburb)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.State == input.State)

                 .WhereIf(input.DateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                 .WhereIf(input.DateFilter == "InstallDate" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "InstallDate" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.DateFilter == "InstallCompleteDate" && input.StartDate != null, e => e.JobFk.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "InstallCompleteDate" && input.EndDate != null, e => e.JobFk.InstalledcompleteDate.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.InvoiceStatus == 1, e => e.IsVerify == false)
                 .WhereIf(input.InvoiceStatus == 2, e => e.IsVerify == true);


            var activity = _leadactivityRepository.GetAll().Where(e => e.SectionId == 29 && e.LeadFk.OrganizationId == input.OrganizationUnit);

            var result = (from o in filter

                          join user in _userRepository.GetAll() on o.InstallerId equals (int)user.Id into userjoined
                          from user in userjoined.DefaultIfEmpty()

                          select new GetInstallerInvoiceForExportDto()
                          {
                              Id = o.Id,
                              JobId = o.JobFk.Id,
                              LeadId = (int)o.JobFk.LeadId,
                              IsVerify = o.IsVerify,
                              PaidStatus = o.IsPaid,
                              JobNumber = o.JobFk.JobNumber,
                              State = o.JobFk.State,
                              InstallerName = user != null ? user.FullName : "",
                              InstallerCompanyName = user != null ? user.CompanyName : "",
                              Installation_Maintenance_Inspection = o.Installation_Maintenance_Inspection,
                              InvNo = o.InvNo,
                              Amount = o.Amount,
                              InvDate = o.InvDate,
                              InvoiceIssuedDate = o.invoiceIssuedDate,
                              InstalledCompleteDate = o.JobFk.InstalledcompleteDate,
                              TotalCost = o.JobFk.TotalCost,
                              OwningAmt = (o.JobFk.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.JobId).Any() ? _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == o.JobId).Select(e => e.PaidAmmount).Sum() : 0)),
                              RemarkIfOwing = o.Remark_if_owing,
                              FinanceName = o.JobFk.PaymentOptionFk.Name,
                              GridConnection = o.JobFk.IsGridConnected,
                              NotesOrReasonforPending = o.NotesOrReasonforPending,
                              InvoiceNo = o.InvoiceNo,

                              ActivityComment = activity.Where(e => e.ActionId == 24 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),
                              ActivityReminderTime = activity.Where(e => e.ActionId == 8 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                              ActivityDescription = activity.Where(e => e.ActionId == 8 && e.LeadId == o.JobFk.LeadId).OrderByDescending(e => e.Id).Select(e => e.ActivityNote).FirstOrDefault(),


                          });

                   
            return _jobsExcelExporter.ExportPendingInstallerInvoiceToFile(await result.ToListAsync());


        }

        public async Task<CreateOrEditJobInstallerInvoiceDto> GetInstallerInvoiceForEdit(EntityDto input)
        {
            var installerInvoice = await _jobInstallerInvoiceRepository.GetAsync(input.Id);
            var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(installerInvoice);

            var job = await _jobJobRepository.GetAll().Include(e => e.HouseTypeFk).Include(e => e.LeadFk).Include(e => e.PaymentOptionFk).Where(e => e.Id == output.JobId).FirstOrDefaultAsync();


            if (output != null)
            {
                var Panels = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1).Select(e => e.Id).ToList();
                var Inverts = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2).Select(e => e.Id).ToList();

                output.JobNumber = job.JobNumber;
                output.State = job.LeadFk.State;
                output.InstalledcompleteDate = job.InstalledcompleteDate != null ? Convert.ToDateTime(job.InstalledcompleteDate).ToString("dd MMM yyyy") : "";
                output.InstallerName = _userRepository.GetAll().Where(e => e.Id == output.InstallerId).Select(e => e.FullName).FirstOrDefault();
                output.InstallerCompanyName = _userRepository.GetAll().Where(e => e.Id == output.InstallerId).Select(e => e.CompanyName).FirstOrDefault();
                output.TotalCost = job.TotalCost;
                output.owningAmt = Convert.ToDecimal(job.TotalCost - (_InvoiceImportDataRepository.GetAll().Where(e => e.JobId == job.Id).Any() ? _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == job.Id).Select(e => e.PaidAmmount).Sum() : 0));
                output.TotalNoOfPanel = _jobProductItemRepository.GetAll().Where(e => Panels.Contains((int)e.ProductItemId) && e.JobId == job.Id).Select(e => e.Quantity).Sum();
                output.totalNoOfInvert = _jobProductItemRepository.GetAll().Where(e => Inverts.Contains((int)e.ProductItemId) && e.JobId == job.Id).Select(e => e.Quantity).Sum();
                output.FinanceName = job.PaymentOptionFk.Name;
                output.Verifieddoublestory = job.HouseTypeFk.Name;
                output.threephase = Convert.ToString(job.MeterPhaseId);
                output.GridConnection = job.IsGridConnected == true ? "Yes" : "No";
            }

            return output;
        }

        public async Task UpdateAddNotes(CreateOrEditJobInstallerInvoiceDto input)
        {
            var installerInvoice = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = input.IsVerify == true ? "Installer Invoice Verified" : "Updated Installer Invoice Notes";
            leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            #region Activity History
            var list = new List<JobTrackerHistory>();
            if (installerInvoice.EmailSent != input.EmailSent)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Email Sent";
                jobhistory.PrevValue = installerInvoice.EmailSent == "1" ? "Yes" : installerInvoice.EmailSent == "0" ? "No" : "";
                jobhistory.CurValue = input.EmailSent == "1" ? "Yes" : input.EmailSent == "0" ? "No" : "";
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.IsVerify != input.IsVerify)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Is Verify";
                jobhistory.PrevValue = installerInvoice.IsVerify.ToString();
                jobhistory.CurValue = input.IsVerify.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Noof_Panels_invoice != input.Noof_Panels_invoice)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Noof_Panels_invoice";
                jobhistory.PrevValue = installerInvoice.Noof_Panels_invoice.ToString();
                jobhistory.CurValue = input.Noof_Panels_invoice.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.ActualPanels_Installed != input.ActualPanels_Installed)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ActualPanels_Installed";
                jobhistory.PrevValue = installerInvoice.ActualPanels_Installed.ToString();
                jobhistory.CurValue = input.ActualPanels_Installed.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Splits != input.Splits)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Splits";
                jobhistory.PrevValue = installerInvoice.Splits == 1 ? "Yes" : installerInvoice.Splits == 0 ? "No" : "";
                jobhistory.CurValue = input.Splits == 1 ? "Yes" : input.Splits == 0 ? "No" : "";
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.TravelKMfromWarehouse != input.TravelKMfromWarehouse)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "TravelKMfromWarehouse";
                jobhistory.PrevValue = installerInvoice.TravelKMfromWarehouse;
                jobhistory.CurValue = input.TravelKMfromWarehouse;
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.otherExtraInvoiceNumber != input.otherExtraInvoiceNumber)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "otherExtraInvoiceNumber";
                jobhistory.PrevValue = installerInvoice.otherExtraInvoiceNumber;
                jobhistory.CurValue = input.otherExtraInvoiceNumber;
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.NotesOrReasonforPending != input.NotesOrReasonforPending)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "NotesOrReasonforPending";
                jobhistory.PrevValue = installerInvoice.NotesOrReasonforPending;
                jobhistory.CurValue = input.NotesOrReasonforPending;
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Installer_selfie != input.Installer_selfie)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Installer_selfie";
                jobhistory.PrevValue = installerInvoice.Installer_selfie.ToString();
                jobhistory.CurValue = input.Installer_selfie.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Front_of_property != input.Front_of_property)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Front_of_property";
                jobhistory.PrevValue = installerInvoice.Front_of_property.ToString();
                jobhistory.CurValue = input.Front_of_property.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Inst_Des_Ele_sign != input.Inst_Des_Ele_sign)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Inst_Des_Ele_sign";
                jobhistory.PrevValue = installerInvoice.Inst_Des_Ele_sign.ToString();
                jobhistory.CurValue = input.Inst_Des_Ele_sign.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Cx_Sign != input.Cx_Sign)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "cx_Sign";
                jobhistory.PrevValue = installerInvoice.Cx_Sign.ToString();
                jobhistory.CurValue = input.Cx_Sign.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.CES != input.CES)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "ces";
                jobhistory.PrevValue = installerInvoice.CES.ToString();
                jobhistory.CurValue = input.CES.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.PanelsSerialNo != input.PanelsSerialNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "PanelsSerialNo";
                jobhistory.PrevValue = installerInvoice.PanelsSerialNo.ToString();
                jobhistory.CurValue = input.PanelsSerialNo.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.InverterSerialNo != input.InverterSerialNo)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "InverterSerialNo";
                jobhistory.PrevValue = installerInvoice.InverterSerialNo.ToString();
                jobhistory.CurValue = input.InverterSerialNo.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.InstalaltionPic != input.InstalaltionPic)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "InstalaltionPic";
                jobhistory.PrevValue = installerInvoice.InstalaltionPic.ToString();
                jobhistory.CurValue = input.InstalaltionPic.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Wi_FiDongle != input.Wi_FiDongle)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Wi_FiDongle";
                jobhistory.PrevValue = installerInvoice.Wi_FiDongle.ToString();
                jobhistory.CurValue = input.Wi_FiDongle.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            if (installerInvoice.Traded != input.Traded)
            {
                JobTrackerHistory jobhistory = new JobTrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    jobhistory.TenantId = (int)AbpSession.TenantId;
                }
                jobhistory.FieldName = "Traded";
                jobhistory.PrevValue = installerInvoice.Traded.ToString();
                jobhistory.CurValue = input.Traded.ToString();
                jobhistory.Action = "Installer Invoice Edit";
                jobhistory.LastmodifiedDateTime = DateTime.UtcNow;
                jobhistory.JobIDId = (int)installerInvoice.JobId;
                jobhistory.JobActionId = activityId;
                list.Add(jobhistory);
            }

            await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            #endregion

            installerInvoice.EmailSent = input.EmailSent;
            installerInvoice.IsVerify = input.IsVerify;
            installerInvoice.Noof_Panels_invoice = input.Noof_Panels_invoice;
            installerInvoice.ActualPanels_Installed = input.ActualPanels_Installed;
            installerInvoice.Splits = input.Splits;
            installerInvoice.TravelKMfromWarehouse = input.TravelKMfromWarehouse;
            installerInvoice.otherExtraInvoiceNumber = input.otherExtraInvoiceNumber;
            installerInvoice.NotesOrReasonforPending = input.NotesOrReasonforPending;
            installerInvoice.Installer_selfie = input.Installer_selfie;
            installerInvoice.Front_of_property = input.Front_of_property;
            installerInvoice.Inst_Des_Ele_sign = input.Inst_Des_Ele_sign;
            installerInvoice.Cx_Sign = input.Cx_Sign;
            installerInvoice.CES = input.CES;
            installerInvoice.PanelsSerialNo = input.PanelsSerialNo;
            installerInvoice.InverterSerialNo = input.InverterSerialNo;
            installerInvoice.InstalaltionPic = input.InstalaltionPic;
            installerInvoice.Wi_FiDongle = input.Wi_FiDongle;
            installerInvoice.Traded = input.Traded;

            await _jobInstallerInvoiceRepository.UpdateAsync(installerInvoice);
        }

        public async Task RevertVerifiedInvoice(int installerInvoiceId)
        {
            //var invoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)installerInvoiceId);
            var installerInvoice = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.Id == installerInvoiceId).FirstOrDefaultAsync();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = 29;
            leadactivity.ActionNote = "Installer Invoice Unverified";
            leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            installerInvoice.IsVerify = false;

            await _jobInstallerInvoiceRepository.UpdateAsync(installerInvoice);
        }

        public async Task UpdateApprovedInvoice(CreateOrEditJobInstallerInvoiceDto input)
        {
            input.ApproedDate = _timeZoneConverter.Convert(input.ApproedDate, (int)AbpSession.TenantId);
            input.DueDate = _timeZoneConverter.Convert(input.DueDate, (int)AbpSession.TenantId);


            var installerInvoice = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "Installer Invoice Approved";
            leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            installerInvoice.IsPaid = input.IsPaid;
            installerInvoice.ApproedDate = input.ApproedDate;
            installerInvoice.ApproedAmount = input.ApproedAmount;
            installerInvoice.DueDate = input.DueDate;
            installerInvoice.PriorityId = input.PriorityId;
            installerInvoice.ApprovedNotes = input.ApprovedNotes;

            await _jobInstallerInvoiceRepository.UpdateAsync(installerInvoice);
        }

        public async Task<PagedResultDto<InstallerPaidDto>> GetInstallerApprovedList(InstallerPaidInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var result = new List<InstallerPaidDto>();
            var totalCount = 0;
            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };
            InstallerPaidSummaryCountDto summary = new InstallerPaidSummaryCountDto();

            if (input.DateFilter == "InstallCompleteDate" && input.IsInvoice == "No")
            {
                var installerInvoiceIds = _jobInstallerInvoiceRepository.GetAll().AsNoTracking().Select(e => e.JobId);

                var filteredjob = _jobJobRepository.GetAll().Include(e => e.LeadFk)
               .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter)
                     .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter)
                     .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Phone == input.Filter)
                     .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter)
                     .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Email == input.Filter)
                     //.WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvoiceNo == input.Filter)
                     //.WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                     .WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                     .WhereIf(!string.IsNullOrWhiteSpace(input.Suburb), e => e.Suburb == input.Suburb)
                     //.WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.State == input.State)
                     .WhereIf(input.StateList != null && input.StateList.Count() > 0, e => input.StateList.Contains(e.State))
                     .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.LeadFk.Area == input.AreaName)

                .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                // .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                //.WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                .WhereIf(input.DateFilter == "InstallCompleteDate" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.DateFilter == "InstallCompleteDate" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)


                      .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5)) //For with battery

                .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && !installerInvoiceIds.Any(i => i == e.Id))
                ;

                var pagedAndFilteredLeads = filteredjob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                #region Summary Count
                summary.Total = filteredjob.Count();
                summary.Paid = 0;
                summary.UnPaid = 0;
                summary.TotalMetroKw = filteredjob.Where(e => e.LeadFk.Area == "Metro").Any() ? filteredjob.Where(e => e.LeadFk.Area == "Metro").Sum(e => e.SystemCapacity) : 0;
                summary.TotalMetroPrice = 0;
                summary.MetroPricePerKw = summary.TotalMetroKw > 0 ? summary.TotalMetroPrice / summary.TotalMetroKw : 0;
                summary.TotalRegionalKw = filteredjob.Where(e => e.LeadFk.Area == "Regional").Any() ? filteredjob.Where(e => e.LeadFk.Area == "Regional").Sum(e => e.SystemCapacity) : 0;
                summary.TotalRegionalPrice =  0;
                summary.RegionalPricePerKw = summary.TotalRegionalKw > 0 ? summary.TotalRegionalPrice / summary.TotalRegionalKw : 0;
                #endregion

                result = await (from o in pagedAndFilteredLeads

                                join user in _userRepository.GetAll() on (int)o.InstallerId equals (int)user.Id into userjoined
                                from user in userjoined.DefaultIfEmpty()

                                select new InstallerPaidDto()
                                {
                                    Id = o.Id,
                                    JobId = o.Id,
                                    LeadId = (int)o.LeadId,
                                    JobNumber = o.JobNumber,
                                    Customer = o.LeadFk.CompanyName,
                                    Address = o.Address + ", " + o.Suburb + ", " + o.State + "-" + o.PostalCode,
                                    PCode = o.PostalCode,
                                    InstallDate = o.InstallationDate,
                                    //InvNo = o.InvNo,
                                    //InvDate = o.InvDate,
                                    //Ammount = o.Amount,
                                    //ApprovedDate = o.ApproedDate,
                                    //ApprovedAmount = o.ApproedAmount,
                                   // ApprovedNotes = o.ApprovedNotes,
                                    //BankRefNo = o.BankRefNo,
                                    //IsPayment = o.Date == null ? false : true,
                                    ///DueDate = o.DueDate,
                                   /// FileName = o.FilePath + "/" + o.FileName,
                                    SummaryCount = summary,
                                    //InvoiceNo = o.InvoiceNo
                                }).ToListAsync();


                totalCount = await filteredjob.CountAsync();
            }
            else
            {
                var query = _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Include(e => e.JobFk.LeadFk).Include(e => e.JobFk.PaymentOptionFk)
               .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
               .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
               .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                     //.Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit).ToListAsync();// && e.ApproedAmount != null && e.ApproedDate != null && e.IsPaid == true);



                     // var query = filteredInstallerInvoice
                     //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvNo.ToString().Contains(input.Filter))
                     .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                     .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                     .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                     .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                     .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                     .WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvoiceNo == input.Filter)
                     //.WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                     .WhereIf(input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                     .WhereIf(!string.IsNullOrWhiteSpace(input.Suburb), e => e.JobFk.Suburb == input.Suburb)
                     //.WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.State == input.State)
                     .WhereIf(input.StateList != null && input.StateList.Count() > 0, e => input.StateList.Contains(e.JobFk.State))
                     .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.JobFk.LeadFk.Area == input.AreaName)

                     .WhereIf(input.DateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                     .WhereIf(input.DateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                     .WhereIf(input.DateFilter == "InstallDate" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.Date >= SDate.Value.Date)
                     .WhereIf(input.DateFilter == "InstallDate" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.Date <= EDate.Value.Date)

                     .WhereIf(input.DateFilter == "InstallCompleteDate" && input.StartDate != null, e => e.JobFk.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                     .WhereIf(input.DateFilter == "InstallCompleteDate" && input.EndDate != null, e => e.JobFk.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                     .WhereIf(input.DateFilter == "ApprovedDate" && input.StartDate != null, e => e.ApproedDate.Value.Date >= SDate.Value.Date)
                     .WhereIf(input.DateFilter == "ApprovedDate" && input.EndDate != null, e => e.ApproedDate.Value.Date <= EDate.Value.Date)

                     .WhereIf(input.DateFilter == "PaidDate" && input.StartDate != null, e => e.Date.Value.Date >= SDate.Value.Date)
                     .WhereIf(input.DateFilter == "PaidDate" && input.EndDate != null, e => e.Date.Value.Date <= EDate.Value.Date)

                     .WhereIf(input.InvoiceStatus == 1, e => e.Date != null)
                     .WhereIf(input.InvoiceStatus == 2, e => e.Date == null)

                      .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                      .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5))

                      .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                     .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                     .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                     .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                     .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                     .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5))
                     .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType);  //For with battery
                     //.WhereIf(input.IsInvoice == "Yes", e => e.IsPaid == true)
                     //.WhereIf(input.IsInvoice == "No", e => e.IsPaid == false && e.IsVerify == false)
                //     .ToList();
                //var filter = query
                    //.WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType);
                var pagedAndFilteredJobs = query.OrderBy(input.Sorting ?? "id desc").PageBy(input);
                //var filteredInstallerInvoice= pagedAndFilteredJobs.WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType);
                #region Summary Count

                summary.Total = pagedAndFilteredJobs.Count();
                summary.Paid = pagedAndFilteredJobs.Where(e => e.Date != null).Count();
                summary.UnPaid = pagedAndFilteredJobs.Where(e => e.Date == null).Count();
                summary.TotalMetroKw = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro").Sum(e => e.JobFk.SystemCapacity) : 0;
                summary.TotalMetroPrice = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro").Sum(e => e.ApproedAmount) : 0;
                summary.MetroPricePerKw = summary.TotalMetroKw > 0 ? summary.TotalMetroPrice / summary.TotalMetroKw : 0;
                summary.TotalRegionalKw = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional").Sum(e => e.JobFk.SystemCapacity) : 0;
                summary.TotalRegionalPrice = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional").Sum(e => e.ApproedAmount) : 0;
                summary.RegionalPricePerKw = summary.TotalRegionalKw > 0 ? summary.TotalRegionalPrice / summary.TotalRegionalKw : 0;


                summary.TotalInspection = pagedAndFilteredJobs.Where(e => e.Installation_Maintenance_Inspection == "Inspection").Any() ? pagedAndFilteredJobs.Where(e => e.Installation_Maintenance_Inspection == "Inspection").Count() : 0;
                summary.PaidInspection = pagedAndFilteredJobs.Where(e => e.Date != null && e.Installation_Maintenance_Inspection == "Inspection").Count();
                summary.UnPaidInspection = pagedAndFilteredJobs.Where(e => e.Date == null && e.Installation_Maintenance_Inspection == "Inspection").Count();
                summary.TotalMetroKwInspection = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Inspection").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Inspection").Sum(e => e.JobFk.SystemCapacity) : 0;
                summary.TotalMetroPriceInspection = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Inspection").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Inspection").Sum(e => e.ApproedAmount) : 0;
                summary.MetroPricePerKwInspection = summary.TotalMetroKwInspection > 0 ? summary.TotalMetroPriceInspection / summary.TotalMetroKwInspection : 0;
                summary.TotalRegionalKwInspection = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Inspection").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Inspection").Sum(e => e.JobFk.SystemCapacity) : 0;
                summary.TotalRegionalPriceInspection = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Inspection").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Inspection").Sum(e => e.ApproedAmount) : 0;
                summary.RegionalPricePerKwInspection = summary.TotalRegionalKwInspection > 0 ? summary.TotalRegionalPriceInspection / summary.TotalRegionalKwInspection : 0;


                summary.TotalInstallation = pagedAndFilteredJobs.Where(e => e.Installation_Maintenance_Inspection == "Installation").Count();
                summary.PaidInstallation = pagedAndFilteredJobs.Where(e => e.Date != null && e.Installation_Maintenance_Inspection == "Installation").Count();
                summary.UnPaidInstallation = pagedAndFilteredJobs.Where(e => e.Date == null && e.Installation_Maintenance_Inspection == "Installation").Count();
                summary.TotalMetroKwInstallation = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Installation").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Installation").Sum(e => e.JobFk.SystemCapacity) : 0;
                summary.TotalMetroPriceInstallation = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Installation").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Installation").Sum(e => e.ApproedAmount) : 0;
                summary.MetroPricePerKwInstallation = summary.TotalMetroKwInstallation > 0 ? summary.TotalMetroPriceInstallation / summary.TotalMetroKwInstallation : 0;
                summary.TotalRegionalKwInstallation = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Installation").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Installation").Sum(e => e.JobFk.SystemCapacity) : 0;
                summary.TotalRegionalPriceInstallation = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Installation").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Installation").Sum(e => e.ApproedAmount) : 0;
                summary.RegionalPricePerKwInstallation = summary.TotalRegionalKwInstallation > 0 ? summary.TotalRegionalPriceInstallation / summary.TotalRegionalKwInstallation : 0;


                summary.TotalMaintenance = pagedAndFilteredJobs.Where(e => e.Installation_Maintenance_Inspection == "Maintenance").Count();
                summary.PaidMaintenance = pagedAndFilteredJobs.Where(e => e.Date != null && e.Installation_Maintenance_Inspection == "Maintenance").Count();
                summary.UnPaidMaintenance = pagedAndFilteredJobs.Where(e => e.Date == null && e.Installation_Maintenance_Inspection == "Maintenance").Count();
                summary.TotalMetroKwMaintenance = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Maintenance").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Maintenance").Sum(e => e.JobFk.SystemCapacity) : 0;
                summary.TotalMetroPriceMaintenance = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Maintenance").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Metro" && e.Installation_Maintenance_Inspection == "Maintenance").Sum(e => e.ApproedAmount) : 0;
                summary.MetroPricePerKwMaintenance = summary.TotalMetroKwMaintenance > 0 ? summary.TotalMetroPriceMaintenance / summary.TotalMetroKwMaintenance : 0;
                summary.TotalRegionalKwMaintenance = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Maintenance").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Maintenance").Sum(e => e.JobFk.SystemCapacity) : 0;
                summary.TotalRegionalPriceMaintenance = pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Maintenance").Any() ? pagedAndFilteredJobs.Where(e => e.JobFk.LeadFk.Area == "Regional" && e.Installation_Maintenance_Inspection == "Maintenance").Sum(e => e.ApproedAmount) : 0;
                summary.RegionalPricePerKwMaintenance = summary.TotalRegionalKwMaintenance > 0 ? summary.TotalRegionalPriceMaintenance / summary.TotalRegionalKwMaintenance : 0;
                #endregion

                //var pagedAndFilteredJobs = filter.AsQueryable().OrderBy(input.Sorting ?? "id desc").PageBy(input);

                result = (from o in pagedAndFilteredJobs

                          join user in _userRepository.GetAll() on (int)o.InstallerId equals (int)user.Id into userjoined
                          from user in userjoined.DefaultIfEmpty()

                          select new InstallerPaidDto()
                          {
                              Id = o.Id,
                              JobId = o.JobFk.Id,
                              LeadId = (int)o.JobFk.LeadId,
                              JobNumber = o.JobFk.JobNumber,
                              Customer = o.JobFk.LeadFk.CompanyName,
                              Address = o.JobFk.Address + ", " + o.JobFk.Suburb + ", " + o.JobFk.State + "-" + o.JobFk.PostalCode,
                              PCode = o.JobFk.PostalCode,
                              InstallDate = o.JobFk.InstallationDate,
                              InvNo = o.InvNo,
                              InvDate = o.InvDate,
                              Ammount = o.Amount,
                              ApprovedDate = o.ApproedDate,
                              ApprovedAmount = o.ApproedAmount,
                              ApprovedNotes = o.ApprovedNotes,
                              BankRefNo = o.BankRefNo,
                              IsPayment = o.Date == null ? false : true,
                              DueDate = o.DueDate,
                              FileName = o.FilePath + "/" + o.FileName,
                              SummaryCount = summary,
                              InvoiceNo = o.InvoiceNo
                          }).ToList();




                totalCount = pagedAndFilteredJobs.Count();
            }


            return new PagedResultDto<InstallerPaidDto>(totalCount, result);
        }

        public async Task RevertApprovedInvoice(int installerinvoiceId)
        {
            var installerInvoice = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.Id == installerinvoiceId).FirstOrDefaultAsync();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = 30;
            leadactivity.ActionNote = "Installer Invoice Unapproved";
            leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            installerInvoice.ApproedDate = null;
            installerInvoice.IsPaid = false;
            installerInvoice.ApproedDate = null;
            installerInvoice.ApproedAmount = null;
            installerInvoice.DueDate = null;
            installerInvoice.PriorityId = null;
            installerInvoice.ApprovedNotes = null;

            await _jobInstallerInvoiceRepository.UpdateAsync(installerInvoice);
        }

        public async Task UpdateInvoicePayment(CreateOrEditJobInstallerInvoiceDto input)
        {
            input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);
            var installerInvoice = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = input.SectionId;
            leadactivity.ActionNote = "Installer Invoice Paid";
            leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            installerInvoice.Date = input.Date.Value.Date;
            installerInvoice.BankRefNo = input.BankRefNo;
            installerInvoice.Remarks = input.Remarks;

            await _jobInstallerInvoiceRepository.UpdateAsync(installerInvoice);
        }

        public async Task RevertInvoicePayment(int Id, int? sectionid)
        {
            //input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);
            var installerInvoice = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.Id == Id).FirstOrDefaultAsync();

            LeadActivityLog leadactivity = new LeadActivityLog();
            leadactivity.ActionId = 26;
            leadactivity.SectionId = sectionid;
            leadactivity.ActionNote = "Revert Installer Invoice Payment";
            leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

            installerInvoice.Date = null;
            installerInvoice.BankRefNo = null;
            installerInvoice.Remarks = null;

            await _jobInstallerInvoiceRepository.UpdateAsync(installerInvoice);
        }


        //Old Code
        //public async Task<CreateOrEditJobInstallerInvoiceDto> GetEditInstallerInvoice(EntityDto input)
        //{
        //    var invoicePayment = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync(input.Id);

        //    var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(invoicePayment);

        //    return output;
        //}



        //public async Task<List<CreateOrEditJobInstallerInvoiceDto>> GetJobInstllationInvoicesByJobId(EntityDto input)
        //{
        //    return await _jobInstallerInvoiceRepository.GetAll()
        //        .Where(e => e.JobId == input.Id)
        //    .Select(installerinvoicess => new CreateOrEditJobInstallerInvoiceDto
        //    {
        //        Id = installerinvoicess.Id,
        //        InvTypeId = installerinvoicess.InvTypeId,
        //        Amount = installerinvoicess.Amount,
        //        InvDate = installerinvoicess.InvDate,
        //        AdvanceAmount = installerinvoicess.AdvanceAmount,
        //        LessDeductAmount = installerinvoicess.LessDeductAmount,
        //        TotalAmount = installerinvoicess.TotalAmount,
        //        AdvancePayDate = installerinvoicess.AdvancePayDate,
        //        PayDate = installerinvoicess.PayDate,
        //        PaymentsTypeId = installerinvoicess.PaymentsTypeId,
        //        InstallerId = installerinvoicess.InstallerId,
        //        InvNo = installerinvoicess.InvNo,
        //        Notes = installerinvoicess.Notes,
        //        Remarks = installerinvoicess.Remarks,
        //        IsPaid = installerinvoicess.IsPaid,
        //        IsVerify = installerinvoicess.IsVerify,
        //        FilePath = installerinvoicess.FilePath,
        //        FileName = installerinvoicess.FileName,
        //    }).ToListAsync();
        //}


        //public async Task<PagedResultDto<InstallerPaidDto>> GetInstallerPaidList(InstallerPaidInputDto input)
        //{

        //    var filteredLeads = _jobInstallerInvoiceRepository.GetAll().WhereIf(input.PaymentStatus == 1, e => e.IsPaid == true)
        //        .WhereIf(input.PaymentStatus == 2, e => e.IsPaid == false)
        //        //.WhereIf(input.InstallerID != 0, e => e.InstallerId == input.InstallerID)
        //        .Where(e => e.ApproedAmount == null && e.ApproedDate == null);
        //    //.WhereIf(input.OrganizationUnit != null, e => e.OrganizationId == input.OrganizationUnit);

        //    var pagedAndFilteredLeads = filteredLeads
        //    .OrderBy(input.Sorting ?? "id desc")
        //    .PageBy(input);

        //    var result = (from master in pagedAndFilteredLeads
        //                  join job in _jobJobRepository.GetAll() on master.JobId equals job.Id
        //                  join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id
        //                  // join jie in _jobInstallerInvoiceRepository.GetAll() on job.Id equals jie.JobId
        //                  //  where jie.IsPaid == true && jie.IsVerify == true

        //                  where master.IsVerify == true && lead.OrganizationId == input.OrganizationUnit
        //                  select new InstallerPaidDto
        //                  {
        //                      ProjectName = job.JobNumber,
        //                      Customer = lead.CompanyName,
        //                      Address = job.Address,
        //                      Suburb = job.Suburb,
        //                      State = job.State,
        //                      PCode = lead.PostCode,
        //                      InstallDate = job.InstallationDate,
        //                      InstallComplate = job.InstalledcompleteDate,
        //                      Ammount = master.Amount,
        //                      InvDate = master.InvDate,
        //                      Issued = master.InvDate,
        //                      PaymentDate = master.PayDate,
        //                      PaymentStaus = master.AdvancePayDate,
        //                      //Id = job.Id,
        //                      LeadId = job.LeadId,
        //                      installerinvoiceId = master.Id,
        //                      Total = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Count()),
        //                      Pending = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Where(e => e.IsVerify != true).Count()),
        //                      Verified = Convert.ToString(_jobInstallerInvoiceRepository.GetAll().Where(e => e.IsVerify == true).Count()),
        //                      paidStatus = master.IsPaid
        //                  }).Distinct().ToList();


        //    var Count = result.Count();

        //    return new PagedResultDto<InstallerPaidDto>(Count, result.ToList());

        //}

        public async Task<List<CommonLookupDto>> GetAllJobNumberList()
        {
            //var jobnumber = _jobJobRepository.GetAll().Where(e=>e.JobStatusId>5);
            var jobnumber = _jobJobRepository.GetAll();
            var filterjobnumberList = (from item in jobnumber
                                       join o1 in _jobLeadsRepository.GetAll() on item.LeadId equals o1.Id into j1
                                       from s1 in j1.DefaultIfEmpty()
                                       where s1.IsDeleted == false
                                       select new CommonLookupDto()
                                       {
                                           Id = item.Id,
                                           DisplayName = item.JobNumber + "-" + s1.CompanyName,
                                       });

            return filterjobnumberList.ToList();
        }

        public List<NameValue<string>> promotionGetAllLeadSource(string leadSourceName)
        {
            var ListOfLeadSource = (from job in _jobJobRepository.GetAll()
                                    join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id into leadjoined
                                    from lead in leadjoined
                                    where (job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || job.Address.Contains(leadSourceName))
                                    select
                                        new jobnoLookupTableDto
                                        {
                                            Value = job.Id,
                                            Name = Convert.ToString(job.JobNumber) + "| " + Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(job.Address)
                                        }).ToList();


            //Making NameValue Collection of Multi-Select DropDown

            var leadSource = new List<NameValue<string>>();
            foreach (var item in ListOfLeadSource)
                if (item.Name != null)
                {
                    leadSource.Add(new NameValue { Name = item.Name, Value = item.Value.ToString() });
                }


            return
                leadSource.IsNullOrEmpty() ?
                leadSource.ToList() :
                leadSource.Where(c => c.Name.ToLower().Contains(leadSourceName.ToLower())).ToList();

        }

        [UnitOfWork]
        public async Task<List<string>> GetAlljobNumberforAdd(string leadSourceName)
        {
            var ListOfLeadSource = (from lead in _jobLeadsRepository.GetAll()
                                    join job in _jobJobRepository.GetAll() on lead.Id equals job.LeadId into jobjoined
                                    from job in jobjoined.DefaultIfEmpty()
                                        //(from job in _jobJobRepository.GetAll()
                                        //                     join lead in _jobLeadsRepository.GetAll() on job.LeadId equals lead.Id into leadjoined
                                        //                     from lead in leadjoined
                                    where (job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || job.Address.Contains(leadSourceName))
                                    select new jobnoLookupTableDto
                                    {
                                        Value = job.Id,
                                        Name = Convert.ToString(job.JobNumber) + "| " + Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(job.Address)
                                        // Name =   Convert.ToString(lead.CompanyName)  
                                    });
            return await ListOfLeadSource
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }

        public async Task<List<string>> GetAlljobNumberforAddToDo(string leadSourceName)
        {
            var orgid = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.OrganizationUnitId).ToList();
            var ListOfLeadSource = (from lead in _jobLeadsRepository.GetAll()
                                    join job in _jobJobRepository.GetAll() on lead.Id equals job.LeadId into jobjoined
                                    from job in jobjoined.DefaultIfEmpty()
                                    where ((job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || lead.Address.Contains(leadSourceName) || job.Address.Contains(leadSourceName) || lead.Mobile.Contains(leadSourceName) || lead.Email.Contains(leadSourceName)) && orgid.Contains(lead.OrganizationId))
                                    // where (lead.CompanyName.Contains(leadSourceName))
                                    let jobnumber = (Convert.ToString(job.JobNumber) == null || Convert.ToString(job.JobNumber) == "" ? "" : Convert.ToString(job.JobNumber))
                                    let Address = (Convert.ToString(job.Address) == null || Convert.ToString(job.Address) == "" ? "" : Convert.ToString(job.Address))
                                    let Suburb = (Convert.ToString(job.Suburb) == null || Convert.ToString(job.Suburb) == "" ? "" : Convert.ToString(job.Suburb))
                                    let State = (Convert.ToString(job.State) == null || Convert.ToString(job.State) == "" ? "" : Convert.ToString(job.State))
                                    let PostalCode = (Convert.ToString(job.PostalCode) == null || Convert.ToString(job.PostalCode) == "" ? "" : Convert.ToString(job.PostalCode))
                                    select new jobnoLookupTableDto
                                    {
                                        Value = lead.Id,
                                        //Name = jobnumber + "| " + Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(lead.Mobile) + "| " + Convert.ToString(lead.Email) + "| " + Address + "," + Suburb + "," + State + "," + PostalCode
                                        Name = jobnumber == "" ? Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(lead.Email) + "| " + Convert.ToString(lead.Address) : jobnumber + "| " + Convert.ToString(lead.CompanyName) + "| " + Convert.ToString(lead.Mobile) + "| " + Convert.ToString(lead.Email) + "| " + Address + "," + Suburb + "," + State + "," + PostalCode
                                    });
            return await ListOfLeadSource
                    .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        }


        //[UnitOfWork]
        //public async Task<List<string>> GetAlljobNumberforCommonFilter(string leadSourceName)
        //{

        //    var orgid = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.OrganizationUnitId).ToList();
        //    var ListOfLeadSource = (from lead in _jobLeadsRepository.GetAll()
        //                            join job in _jobJobRepository.GetAll() on lead.Id equals job.LeadId into jobjoined
        //                            from job in jobjoined.DefaultIfEmpty()
        //                            where ((job.JobNumber.Contains(leadSourceName) || lead.CompanyName.Contains(leadSourceName) || job.Address.Contains(leadSourceName) || lead.Mobile.Contains(leadSourceName) || lead.Email.Contains(leadSourceName)) && orgid.Contains(lead.OrganizationId))
        //                            //where (lead.CompanyName.Contains(leadSourceName))
        //                            let jobnumber = (Convert.ToString(job.JobNumber) == null || Convert.ToString(job.JobNumber) == "" ? "" : Convert.ToString(job.JobNumber))
        //                            let Address = (Convert.ToString(job.Address) == null || Convert.ToString(job.Address) == "" ? "" : Convert.ToString(job.Address))
        //                            let Suburb = (Convert.ToString(job.Suburb) == null || Convert.ToString(job.Suburb) == "" ? "" : Convert.ToString(job.Suburb))
        //                            let State = (Convert.ToString(job.State) == null || Convert.ToString(job.State) == "" ? "" : Convert.ToString(job.State))
        //                            let PostalCode = (Convert.ToString(job.PostalCode) == null || Convert.ToString(job.PostalCode) == "" ? "" : Convert.ToString(job.PostalCode))
        //                            select new jobnoLookupTableDto
        //                            {
        //                                Value = job.Id,
        //                                Name = jobnumber + "|" + Convert.ToString(lead.CompanyName) + "|" + Convert.ToString(lead.Mobile) + "|" + Convert.ToString(lead.Email) + "|" + Address + "," + Suburb + "," + State + "," + PostalCode
        //                            });
        //    return await ListOfLeadSource
        //            .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        //}


        //[UnitOfWork]
        //public async Task<List<string>> GetOnlyprojectno(string leadSourceName)
        //{
        //    using (_unitOfWorkManager.Current.SetTenantId(null))
        //    {
        //        var result = _jobJobRepository.GetAll()
        //        .Where(c => c.JobNumber.Contains(leadSourceName));
        //        var suburbs = from o in result
        //                      select new jobnoLookupTableDto()
        //                      {
        //                          Value = o.Id,
        //                          Name = o.Suburb.ToString()
        //                      };

        //        return await suburbs
        //            .Select(ResultDto => ResultDto == null || ResultDto.Name == null ? "" : ResultDto.Name.ToString()).ToListAsync();
        //    }
        //}

        public async Task<List<JobJobTypeLookupTableDto>> GetInstallerForTableDropdown()
        {
            var User_List = _userRepository
           .GetAll().Where(e => e.IsActive == true);

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Installer")
                            select new JobJobTypeLookupTableDto()
                            {
                                Id = (int)user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            });

            return await UserList.ToListAsync();
        }


        //public async Task<TotalSammaryCountDto> getAllTrackerCount(int organizationid)
        //{
        //    var output = new TotalSammaryCountDto();
        //    var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //    IList<string> role = await _userManager.GetRolesAsync(User);
        //    var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //    var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();


        //    output.TotalinsInvoice = _jobInstallerInvoiceRepository.GetAll()
        //    .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
        //    .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
        //    .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
        //    .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid).Select(e => e.Id).Count();

        //    output.PendinginsInvoice = _jobInstallerInvoiceRepository.GetAll()
        //       .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
        //       .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
        //       .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
        //       .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.IsVerify == false).Select(e => e.Id).Count();

        //    output.VerifiedinsInvoice = _jobInstallerInvoiceRepository.GetAll()
        //       .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
        //       .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
        //       .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
        //       .Where(e => e.JobFk.LeadFk.OrganizationId == organizationid && e.IsVerify == true && e.ApproedDate == null && e.IsPaid == false).Select(e => e.Id).Count();
        //    return output;

        //}

        public async Task<PagedResultDto<InstallerInvoiceFilesViewDto>> GetAllInvoiceFiles(InstallerInvoiceFilesInput input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var filteredLeads = _InvoiceFileRepository.GetAll();

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var fielist = (from o in pagedAndFilteredLeads
                           select new InstallerInvoiceFilesViewDto()
                           {
                               CreatedUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                               FileName = o.FileName,
                               FilePath = o.FilePath,
                               CreatedDate = o.CreationTime
                           });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<InstallerInvoiceFilesViewDto>(
                totalCount,
                await fielist.ToListAsync()
            );
        }

        public async Task<PagedResultDto<InstallerInvoiceImportDataViewDto>> GetAllInvoiceImportData(InstallerInvoiceImportDataInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var filteredLeads = _InvoiceImportDataRepository.GetAll().Include(e => e.JobFk)
                               .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationID)
                               .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.JobNumber == input.FilterText)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.Mobile == input.FilterText)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.CompanyName == input.FilterText)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.Email == input.FilterText)
                               .WhereIf(input.Paymenttypeid != 0, e => e.JobFk.PaymentOptionId == input.Paymenttypeid)
                               .WhereIf(input.Payby != 0, e => e.InvoicePaymentMethodFk.Id == input.Payby)
                               .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.LeadFk.State == input.State)

                               .WhereIf(input.DateFilterType == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                               .WhereIf(input.DateFilterType == "Creation" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                               .WhereIf(input.DateFilterType == "InstallDate" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.Date >= SDate.Value.Date)
                               .WhereIf(input.DateFilterType == "InstallDate" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.Date <= EDate.Value.Date)

                               .WhereIf(input.DateFilterType == "BankDate" && input.StartDate != null, e => e.Date.Value.Date >= SDate.Value.Date)
                               .WhereIf(input.DateFilterType == "BankDate" && input.EndDate != null, e => e.Date.Value.Date <= EDate.Value.Date)
                               .AsNoTracking()
                               .Select(e => new { e.Id, e.JobId, e.JobFk.LeadId, e.JobFk.TotalCost, e.InvoicePaymentMethodId, e.PaidAmmount, e.CreatorUserId, e.Date, e.JobNumber, e.InvoicePaymentMethodFk, e.Description, e.AllotedBy, e.InvoiceNotesDescription, e.CreationTime, e.SSCharge, e.ReceiptNumber, e.PurchaseNumber })
                               ;

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var TotalInvoicePayment = filteredLeads.DistinctBy(e => e.JobId).Select(e => e.TotalCost).Sum();
            var AmmountRcv = await filteredLeads.Where(e => e.InvoicePaymentMethodId != 5).Select(e => e.PaidAmmount).SumAsync();
            var AmmountRcvWO = await filteredLeads.Where(e => e.InvoicePaymentMethodId == 5).Select(e => e.PaidAmmount).SumAsync();

            var fielist = (from o in pagedAndFilteredLeads
                           select new InstallerInvoiceImportDataViewDto()
                           {
                               Importdata = new ImportDataViewDto
                               {
                                   CreatedUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).AsNoTracking().Select(e => e.FullName).FirstOrDefault(),
                                   Date = o.Date,
                                   JobNumber = o.JobNumber,
                                   PaymentMethodName = o.InvoicePaymentMethodFk.PaymentMethod,
                                   Description = o.Description,
                                   PaidAmmount = o.PaidAmmount,
                                   AllotedBy = o.AllotedBy,
                                   InvoiceNotDescription = o.InvoiceNotesDescription,
                                   CreatedDate = o.CreationTime,
                                   LeadId = o.LeadId,
                                   SSCharge = o.SSCharge,
                                   ReceiptNumber = o.ReceiptNumber,
                                   PurchaseNumber = o.PurchaseNumber,
                                   ID = o.Id
                               },

                               TotalInvoicePayment = TotalInvoicePayment,
                               AmmountRcv = AmmountRcv,
                               AmmountRcvWO = AmmountRcvWO
                           });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<InstallerInvoiceImportDataViewDto>(
                totalCount,
                await fielist.ToListAsync()
            );
        }

        public async Task<FileDto> getinvoiceimportToExcel(InstallerInvoiceImportDataInput input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var filteredLeads = _InvoiceImportDataRepository.GetAll()
            //                   .WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.JobFk.JobNumber.Contains(input.FilterText) || e.JobFk.LeadFk.Mobile.Contains(input.FilterText) || e.JobFk.LeadFk.CompanyName.Contains(input.FilterText) || e.JobFk.LeadFk.Mobile.Contains(input.FilterText))
            //                   .WhereIf(input.Paymenttypeid != 0, e => e.JobFk.PaymentOptionId == input.Paymenttypeid)
            //                   .WhereIf(input.Payby != 0, e => e.InvoicePaymentMethodFk.Id == input.Payby)
            //                   .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.LeadFk.State == input.State)
            //                   .WhereIf(input.DateFilterType == "CreationDate" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
            //                   .WhereIf(input.DateFilterType == "BankDate" && input.StartDate != null && input.EndDate != null, e => e.Date.Value.AddHours(10).Date >= SDate.Value.Date && e.Date.Value.AddHours(10).Date <= EDate.Value.Date)
            //                   .WhereIf(input.DateFilterType == "InstallDate" && input.StartDate != null && input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.JobFk.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)
            //                   .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationID);

            //var pagedAndFilteredLeads = filteredLeads
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var Jobids = filteredLeads.Select(e => e.JobId).ToList();

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var filteredLeads = _InvoiceImportDataRepository.GetAll().Include(e => e.JobFk)
                              .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.JobNumber == input.FilterText)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.Mobile == input.FilterText)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.CompanyName == input.FilterText)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.Email == input.FilterText)
                               .WhereIf(input.Paymenttypeid != 0, e => e.JobFk.PaymentOptionId == input.Paymenttypeid)
                               .WhereIf(input.Payby != 0, e => e.InvoicePaymentMethodFk.Id == input.Payby)
                               .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.LeadFk.State == input.State)

                               //.WhereIf(input.DateFilterType == "CreationDate" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                               //.WhereIf(input.DateFilterType == "BankDate" && input.StartDate != null && input.EndDate != null, e => e.Date.Value.AddHours(10).Date >= SDate.Value.Date && e.Date.Value.AddHours(10).Date <= EDate.Value.Date)
                               //.WhereIf(input.DateFilterType == "InstallDate" && input.StartDate != null && input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(10).Date >= SDate.Value.Date && e.JobFk.InstallationDate.Value.AddHours(10).Date <= EDate.Value.Date)

                               .WhereIf(input.DateFilterType == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                               .WhereIf(input.DateFilterType == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                               .WhereIf(input.DateFilterType == "InstallDate" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.Date >= SDate.Value.Date)
                               .WhereIf(input.DateFilterType == "InstallDate" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.Date <= EDate.Value.Date)

                               .WhereIf(input.DateFilterType == "BankDate" && input.StartDate != null, e => e.Date.Value.Date >= SDate.Value.Date)
                               .WhereIf(input.DateFilterType == "BankDate" && input.EndDate != null, e => e.Date.Value.Date <= EDate.Value.Date)

                               .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationID);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc");

            var query = (from o in pagedAndFilteredLeads
                         select new InstallerInvoiceImportDataViewDto()
                         {
                             Importdata = new ImportDataViewDto
                             {
                                 CreatedUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                 Date = o.Date,
                                 JobNumber = o.JobNumber,
                                 PaymentMethodName = o.InvoicePaymentmothodName,
                                 Description = o.Description,
                                 PaidAmmount = o.PaidAmmount,
                                 AllotedBy = o.AllotedBy,
                                 InvoiceNotDescription = o.InvoiceNotesDescription,
                                 CreatedDate = o.CreationTime,
                                 LeadId = o.JobFk.LeadId,
                                 SSCharge = o.SSCharge,
                                 ReceiptNumber = o.ReceiptNumber,
                                 PurchaseNumber = o.PurchaseNumber
                             },
                         });


            var importdatalistListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _invoicePaymentsExcelExporter.invoiceimportdataExportToFile(importdatalistListDtos, "InvoicePaid.xlsx");
            }
            else
            {
                //return _invoicePaymentsExcelExporter.invoiceimportdataCsvExport(importdatalistListDtos);
                return _invoicePaymentsExcelExporter.invoiceimportdataExportToFile(importdatalistListDtos, "InvoicePaid.csv");
            }

        }

        public CreateOrEditImportDto getImportDataForEdit(int? Id)
        {

            var importdata = (from o in _InvoiceImportDataRepository.GetAll()
                              where (o.Id == Id)
                              select new CreateOrEditImportDto
                              {
                                  Date = o.Date,
                                  JobNumber = o.JobNumber,
                                  Description = o.Description,
                                  PaidAmmount = o.PaidAmmount,
                                  AllotedBy = o.AllotedBy,
                                  InvoiceNotesDescription = o.InvoiceNotesDescription,
                                  SSCharge = o.SSCharge,
                                  ReceiptNumber = o.ReceiptNumber,
                                  InvoicePaymentMethodId = o.InvoicePaymentMethodId,
                                  PurchaseNumber = o.PurchaseNumber,
                                  Id = o.Id,
                                  JobId = o.JobId
                              }).FirstOrDefault();

            return importdata;
        }

        public async Task DeleteImportData(EntityDto input, int SectionId)
        {

            var edition = await _InvoiceImportDataRepository.FirstOrDefaultAsync((int)input.Id);
            var leadid = _jobJobRepository.GetAll().Where(e => e.Id == edition.JobId).Select(e => e.LeadId).FirstOrDefault();
            LeadActivityLog leadactivity = new LeadActivityLog();

            leadactivity.ActionId = 37;
            leadactivity.SectionId = SectionId != null ? SectionId : 0;
            leadactivity.ActionNote = "Invoice Paid Deleted";
            leadactivity.LeadId = (int)leadid;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            _leadactivityRepository.InsertAndGetId(leadactivity);
            await _InvoiceImportDataRepository.DeleteAsync(edition);
        }

        public async Task Update(CreateOrEditImportDto input)
        {

            var exitdata = await _invoicePaymentRepository.GetAll().Where(e => e.ReceiptNumber == input.ReceiptNumber && e.JobId == input.JobId && e.Id != input.Id && e.InvoicePayTotal == input.PaidAmmount).FirstOrDefaultAsync();

            if (exitdata != null)
            {
                exitdata.IsVerified = true;
                await _invoicePaymentRepository.UpdateAsync(exitdata);
            }

                input.Date = _timeZoneConverter.Convert(input.Date, (int)AbpSession.TenantId);

                var importdatas = await _InvoiceImportDataRepository.FirstOrDefaultAsync((int)input.Id);
                var leadid = _jobJobRepository.GetAll().Where(e => e.Id == input.JobId).Select(e => e.LeadId).FirstOrDefault();
                LeadActivityLog leadactivity = new LeadActivityLog();

                leadactivity.ActionId = 38;
                leadactivity.SectionId = input.SectionId != null ? input.SectionId : 0;
                leadactivity.ActionNote = "Invoice Paid Updated";
                leadactivity.LeadId = (int)leadid;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var jobactionid = _leadactivityRepository.InsertAndGetId(leadactivity);

                var list = new List<JobTrackerHistory>();
                if (importdatas.Date != input.Date)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "Date";
                    jobhistory.PrevValue = importdatas.Date != null ? importdatas.Date.ToString() : "";
                    jobhistory.CurValue = input.Date.ToString();
                    jobhistory.Action = "Invoice Paid Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = importdatas.JobId;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (importdatas.JobNumber != input.JobNumber)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "JobNumber";
                    jobhistory.PrevValue = importdatas.JobNumber != null ? importdatas.JobNumber.ToString() : "";
                    jobhistory.CurValue = input.JobNumber.ToString();
                    jobhistory.Action = "Invoice Paid Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = importdatas.JobId;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (importdatas.PaidAmmount != input.PaidAmmount)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "PaidAmmount";
                    jobhistory.PrevValue = importdatas.PaidAmmount != null ? importdatas.PaidAmmount.ToString() : "";
                    jobhistory.CurValue = input.PaidAmmount.ToString();
                    jobhistory.Action = "Invoice Paid Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = importdatas.JobId;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (importdatas.AllotedBy != input.AllotedBy)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "AllotedBy";
                    jobhistory.PrevValue = importdatas.AllotedBy != null ? importdatas.AllotedBy.ToString() : "";
                    jobhistory.CurValue = input.AllotedBy.ToString();
                    jobhistory.Action = "Invoice Paid Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = importdatas.JobId;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }


                if (importdatas.SSCharge != input.SSCharge)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "SSCharge";
                    jobhistory.PrevValue = importdatas.SSCharge != null ? importdatas.SSCharge.ToString() : "";
                    jobhistory.CurValue = input.SSCharge.ToString();
                    jobhistory.Action = "Invoice Paid Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = importdatas.JobId;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (importdatas.ReceiptNumber != input.ReceiptNumber)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "ReceiptNumber";
                    jobhistory.PrevValue = importdatas.ReceiptNumber != null ? importdatas.ReceiptNumber.ToString() : "";
                    jobhistory.CurValue = input.ReceiptNumber.ToString();
                    jobhistory.Action = "Invoice Paid Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = importdatas.JobId;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (importdatas.InvoicePaymentMethodId != input.InvoicePaymentMethodId)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "InvoicePaymentMethodId";
                    jobhistory.PrevValue = importdatas.InvoicePaymentMethodId != null ? importdatas.InvoicePaymentMethodId.ToString() : "";
                    jobhistory.CurValue = input.InvoicePaymentMethodId.ToString();
                    jobhistory.Action = "Invoice Paid Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = importdatas.JobId;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }

                if (importdatas.PurchaseNumber != input.PurchaseNumber)
                {
                    JobTrackerHistory jobhistory = new JobTrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        jobhistory.TenantId = (int)AbpSession.TenantId;
                    }
                    jobhistory.FieldName = "PurchaseNumber";
                    jobhistory.PrevValue = importdatas.PurchaseNumber != null ? importdatas.PurchaseNumber.ToString() : "";
                    jobhistory.CurValue = input.PurchaseNumber.ToString();
                    jobhistory.Action = "Invoice Paid Edit";
                    jobhistory.LastmodifiedDateTime = DateTime.Now;
                    jobhistory.JobIDId = importdatas.JobId;
                    jobhistory.JobActionId = jobactionid;
                    list.Add(jobhistory);
                }
                await _dbcontextprovider.GetDbContext().JobTrackerHistorys.AddRangeAsync(list);

                importdatas.Date = input.Date;
                importdatas.JobNumber = input.JobNumber;
                importdatas.Description = input.Description;
                importdatas.PaidAmmount = input.PaidAmmount;
                importdatas.AllotedBy = input.AllotedBy;
                importdatas.InvoiceNotesDescription = input.InvoiceNotesDescription;
                importdatas.SSCharge = input.SSCharge;
                importdatas.ReceiptNumber = input.ReceiptNumber;
                importdatas.InvoicePaymentMethodId = input.InvoicePaymentMethodId;
                importdatas.PurchaseNumber = input.PurchaseNumber;
                await _InvoiceImportDataRepository.UpdateAsync(importdatas);

                await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            


        }

        public async Task<PagedResultDto<PayWayViewDataDto>> GetPayWayListingData(PayWayInputDataDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var filteredLeads = _PayWayDataRepository.GetAll()
                               //.WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.JobFk.JobNumber.Contains(input.FilterText) || e.JobFk.LeadFk.Mobile.Contains(input.FilterText) || e.JobFk.LeadFk.CompanyName.Contains(input.FilterText) || e.JobFk.LeadFk.Mobile.Contains(input.FilterText))
                               .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.JobNumber == input.FilterText)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.Mobile == input.FilterText)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.CompanyName == input.FilterText)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.Email == input.FilterText)
                               .WhereIf(input.Paymenttypeid != 0, e => e.PaymentType == input.Paymenttypeid)
                               .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.LeadFk.State == input.State)
                               .WhereIf(input.DateFilterType == "CreationDate" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                               .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationID);

            var pagedAndFilteredLeads = filteredLeads
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input);
            var Jobids = filteredLeads.Select(e => e.jobId).ToList();
            var leads = (from o in pagedAndFilteredLeads
                         select new PayWayViewDataDto()
                         {
                             payway = new AllPayWayDatasDto
                             {
                                 CreatedUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                 JobNumber = o.JobFk.JobNumber,
                                 PaymentMethodName = o.paymentMethod,
                                 CreatedDate = o.CreationTime,
                                 LeadId = o.JobFk.LeadId,
                                 ReceiptNumber = o.receiptNumber,
                                 transactionId = o.transactionId,
                                 status = o.status,
                                 responseCode = o.responseCode,
                                 responseText = o.responseText,
                                 transactionType = o.transactionType,
                                 customerName = o.customerName,
                                 customerNumber = o.customerNumber,
                                 PaidAmmount = o.principalAmount,
                                 ID = o.Id,
                             },
                             TotalInvoicePayment = _jobJobRepository.GetAll().Where(e => Jobids.Contains(e.Id)).Select(e => e.TotalCost).Sum(),
                             AmmountRcv = filteredLeads.Select(e => e.principalAmount).Sum()
                         });
            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<PayWayViewDataDto>(
                totalCount, await
                leads.ToListAsync()
            );

        }

        public async Task<FileDto> GetInvoicePayWayimportToExcel(PayWayInputDataDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var filteredLeads = _PayWayDataRepository.GetAll()
                               //.WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.JobFk.JobNumber.Contains(input.FilterText) || e.JobFk.LeadFk.Mobile.Contains(input.FilterText) || e.JobFk.LeadFk.CompanyName.Contains(input.FilterText) || e.JobFk.LeadFk.Mobile.Contains(input.FilterText))
                               .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.JobNumber == input.FilterText)
                               .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.Mobile == input.FilterText)
                               .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.CompanyName == input.FilterText)
                               .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.FilterText), e => e.JobFk.LeadFk.Email == input.FilterText)
                               .WhereIf(input.Paymenttypeid != 0, e => e.PaymentType == input.Paymenttypeid)
                               .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.LeadFk.State == input.State)
                               .WhereIf(input.DateFilterType == "CreationDate" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                               .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationID);

            var pagedAndFilteredLeads = filteredLeads
               .OrderBy(input.Sorting ?? "id desc");

            //var Jobids = filteredLeads.Select(e => e.jobId).ToList();

            var query = (from o in pagedAndFilteredLeads
                         select new PayWayViewDataDto()
                         {
                             payway = new AllPayWayDatasDto
                             {
                                 CreatedUser = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                 JobNumber = o.JobFk.JobNumber,
                                 PaymentMethodName = o.paymentMethod,
                                 CreatedDate = o.CreationTime,
                                 LeadId = o.JobFk.LeadId,
                                 ReceiptNumber = o.receiptNumber,
                                 transactionId = o.transactionId,
                                 status = o.status,
                                 responseCode = o.responseCode,
                                 responseText = o.responseText,
                                 transactionType = o.transactionType,
                                 customerName = o.customerName,
                                 customerNumber = o.customerNumber,
                                 PaidAmmount = o.principalAmount,
                                 ID = o.Id,
                             },
                             //TotalInvoicePayment = _jobJobRepository.GetAll().Where(e => Jobids.Contains(e.Id)).Select(e => e.TotalCost).Sum(),
                             //AmmountRcv = filteredLeads.Select(e => e.principalAmount).Sum()
                         });


            var importdatalistListDtos = await query.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _invoicePaymentsExcelExporter.invoicePayWayimportdataExportToFile(importdatalistListDtos, "InvoicePayWay.xlsx");
            }
            else
            {
                //return _invoicePaymentsExcelExporter.invoicePayWayimportdataCsvExport(importdatalistListDtos);
                return _invoicePaymentsExcelExporter.invoicePayWayimportdataExportToFile(importdatalistListDtos, "InvoicePayWay.csv");
            }

        }

        public async Task<List<GetSearchResultDto>> GetAllJobNumberForMyService(string input)
        {
            TextInfo textInfo = new CultureInfo("en-GB", false).TextInfo;
            var ListOfLeadSource = (from lead in _jobLeadsRepository.GetAll()
                                    join job in _jobJobRepository.GetAll() on lead.Id equals job.LeadId into jobjoined
                                    from job in jobjoined.DefaultIfEmpty()
                                    where (job.JobNumber.Contains(input) || lead.CompanyName.Contains(input) || job.LeadFk.Mobile.Contains(input))
                                    select new GetSearchResultDto
                                    {
                                        JobId = job.Id,
                                        CustomerName = textInfo.ToTitleCase(job.LeadFk.CompanyName.Trim().ToLower()),
                                        JobNumber = job.JobNumber,
                                        Mobile = job.LeadFk.Mobile,
                                        Email = job.LeadFk.Email,
                                        Address = job.LeadFk.Address,
                                        State = job.State

                                    });
            return await ListOfLeadSource.ToListAsync();
        }

        public bool CheckDuplicateJobnumber(string inspection, int jobId, int id)
        {
            if (id != null || id != 0)
            {

                bool existdata = _jobInstallerInvoiceRepository.GetAll().Where(e => e.Installation_Maintenance_Inspection == inspection && e.JobId == jobId && e.Id != id).Any();
                return existdata;
            }
            else
            {
                bool existdata = _jobInstallerInvoiceRepository.GetAll().Where(e => e.Installation_Maintenance_Inspection == inspection && e.JobId == jobId).Any();
                return existdata;
            }
        }

        public async Task CreateManualInvoicePaid(CreatePaidInvoiceDto input)
        {
            input.PaymentDate = _timeZoneConverter.Convert(input.PaymentDate, (int)AbpSession.TenantId);
            InvoiceImportData invoicepayment = new InvoiceImportData();

            var sscharge = 0.0M;
            if (input.SSCharge == null)
            {
                sscharge = 0;
            }
            else
            {
                sscharge = Convert.ToDecimal(input.SSCharge);
            }

            if (input.JobId == 0)
            {
                throw new Exception("ProjectNo Not Found");
            }

            var InvoicePaymentmothodName = await _invoicePaymentMethodRepository.GetAll().Where(e => e.Id == input.InvoicePaymentMethodId).Select(e => e.ShortCode).FirstOrDefaultAsync();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();

            var exitdata = await _invoicePaymentRepository.GetAll().Where(e => e.ReceiptNumber == invoicepayment.ReceiptNumber && e.JobId == input.JobId && e.InvoicePayTotal == input.AmountPaid).FirstOrDefaultAsync();

            if(exitdata != null)
            {
                exitdata.IsVerified = true;
                await _invoicePaymentRepository.UpdateAsync(exitdata);
            }
            else
            {
                invoicepayment.TenantId = (int)AbpSession.TenantId;
                invoicepayment.Date = input.PaymentDate;
                invoicepayment.JobNumber = input.JobNumber;
                invoicepayment.JobId = input.JobId;
                invoicepayment.InvoicePaymentmothodName = InvoicePaymentmothodName;
                invoicepayment.InvoicePaymentMethodId = input.InvoicePaymentMethodId;
                invoicepayment.Description = input.Description;
                invoicepayment.PaidAmmount = Convert.ToDecimal(input.AmountPaid);
                invoicepayment.AllotedBy = User.Name;
                invoicepayment.PurchaseNumber = input.PurchaseNumber;
                invoicepayment.ReceiptNumber = input.ReceiptNumber;
                invoicepayment.InvoiceNotesDescription = input.InvoiceNotesDescription;
                invoicepayment.SSCharge = sscharge;
                invoicepayment.CreatorUserId = AbpSession.UserId;
                invoicepayment.IsManual = true;

                await _InvoiceImportDataRepository.InsertAsync(invoicepayment);
            }


           

            //var jobs = _jobJobRepository.GetAll().Where(e => e.Id == input.JobId).FirstOrDefault();
            //LeadActivityLog leadactivity = new LeadActivityLog();
            //leadactivity.ActionId = 58;
            //leadactivity.SectionId = input.SectionId;
            //leadactivity.ActionNote = "Manually Invoice Created";
            //leadactivity.LeadId = (int)jobs.LeadId;
            //if (AbpSession.TenantId != null)
            //{
            //    leadactivity.TenantId = (int)AbpSession.TenantId;
            //}
            //_leadactivityRepository.InsertAndGetId(leadactivity);

            if (!string.IsNullOrEmpty(invoicepayment.ReceiptNumber))
            {
                var _ip = await _invoicePaymentRepository.GetAll().Where(e => e.ReceiptNumber == invoicepayment.ReceiptNumber && e.InvoicePayTotal == invoicepayment.PaidAmmount).FirstOrDefaultAsync();

                if (_ip != null)
                {
                    _ip.IsVerified = true;
                    await _invoicePaymentRepository.UpdateAsync(_ip);
                }
            }

        }

        public async Task<FileDto> GetAllToExcel(InstallerNewInputForExcelDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredLeads = _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Include(e => e.JobFk.LeadFk)
                //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvNo.ToString().Contains(input.Filter))
                .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                .WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvNo.ToString() == input.Filter)
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                .WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                .WhereIf(input.DateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.DateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .Where(e => e.IsPaid == false && e.IsVerify == false && e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);

            //InstallerNewSummaryCountDto summary = new InstallerNewSummaryCountDto();
            //summary.Total = filteredLeads.Count();
            //summary.Pending = filteredLeads.Where(e => e.IsVerify != true).Count();
            //summary.Verified = filteredLeads.Where(e => e.IsVerify != true).Count();


            var result = (from o in filteredLeads

                          join user in _userRepository.GetAll() on (int)o.InstallerId equals (int)user.Id into userjoined
                          from user in userjoined.DefaultIfEmpty()

                          select new InstallerNewDto
                          {
                              ProjectName = o.JobFk.JobNumber,
                              Customer = o.JobFk.LeadFk.CompanyName,
                              Address = o.JobFk.Address,
                              Suburb = o.JobFk.Suburb,
                              State = o.JobFk.State,
                              PCode = o.JobFk.PostalCode,
                              InstallDate = o.JobFk.InstallationDate,
                              InstallComplate = o.JobFk.InstalledcompleteDate,
                              Ammount = o.Amount,
                              InvDate = o.InvDate,
                              Issued = o.InvDate,
                              PaymentDate = o.PayDate,
                              PaymentStaus = o.AdvancePayDate,
                              id = o.Id,
                              installername = user.Name + "/" + user.CompanyName,
                              invoiceType = o.Installation_Maintenance_Inspection,
                              invoiceNotes = o.Notes,
                              InvNo = o.InvNo,
                              InvoiceNO = o.InvoiceNo,
                              invoiceinstallerId = o.Id,
                              Filenameinv = o.FileName,
                              FilePathinv = o.FilePath,
                              JobId = o.JobId
                              //Summary = summary
                          });

            return _jobsExcelExporter.ExportInstallerNewToFile(result.ToList());

        }
        public async Task VerifyOrReadyToPay(List<int> ids, int verifyOrReadyToPay)
        {
            foreach (var item in ids)
            {
                var installerInvoice = await _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Where(e => e.Id == item).FirstOrDefaultAsync();

                if(installerInvoice != null)
                {
                    if(verifyOrReadyToPay == 1)
                    {
                        installerInvoice.IsVerify = true;

                        LeadActivityLog leadactivity = new LeadActivityLog();
                        leadactivity.ActionId = 26;
                        leadactivity.SectionId = 29;
                        leadactivity.ActionNote = "Installer Invoice Verify";
                        leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
                    }
                    else if(verifyOrReadyToPay == 2)
                    {
                        var curruntDate = _timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId);

                        LeadActivityLog leadactivity = new LeadActivityLog();
                        leadactivity.ActionId = 26;
                        leadactivity.SectionId = 29;
                        leadactivity.ActionNote = "Installer Invoice Approved";
                        leadactivity.LeadId = (int)installerInvoice.JobFk.LeadId;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        var activityId = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                        installerInvoice.IsPaid = true;
                        installerInvoice.ApproedDate = curruntDate;
                        installerInvoice.ApproedAmount = installerInvoice.Amount;
                        installerInvoice.DueDate = curruntDate;
                        installerInvoice.PriorityId = 2;
                        installerInvoice.ApprovedNotes = "Installer Invoice Approved";
                    }

                    await _jobInstallerInvoiceRepository.UpdateAsync(installerInvoice);
                }
            }
        }

        public async Task<FileDto> GetInstallerApprovedListToExcel(InstallerPaidInputForExcelDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredInstallerInvoice = _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Include(e => e.JobFk.LeadFk).Include(e => e.JobFk.PaymentOptionFk)
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit && e.ApproedAmount != null && e.ApproedDate != null && e.IsPaid == true);

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            var filter = filteredInstallerInvoice
                 //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvNo.ToString().Contains(input.Filter))
                 .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                 .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                 .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                 .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                 .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                 .WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvNo.ToString() == input.Filter)
                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                 .WhereIf(input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.Suburb), e => e.JobFk.Suburb == input.Suburb)
                 //.WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.JobFk.State == input.State)
                 .WhereIf(input.StateList != null && input.StateList.Count() > 0, e => input.StateList.Contains(e.JobFk.State))
                 .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.JobFk.LeadFk.Area == input.AreaName)

                 .WhereIf(input.DateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                 .WhereIf(input.DateFilter == "InstallDate" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "InstallDate" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.DateFilter == "InstallCompleteDate" && input.StartDate != null, e => e.JobFk.InstalledcompleteDate.Value.Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "InstallCompleteDate" && input.EndDate != null, e => e.JobFk.InstalledcompleteDate.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.DateFilter == "ApprovedDate" && input.StartDate != null, e => e.ApproedDate.Value.Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "ApprovedDate" && input.EndDate != null, e => e.ApproedDate.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.DateFilter == "PaidDate" && input.StartDate != null, e => e.Date.Value.Date >= SDate.Value.Date)
                 .WhereIf(input.DateFilter == "PaidDate" && input.EndDate != null, e => e.Date.Value.Date <= EDate.Value.Date)

                 .WhereIf(input.InvoiceStatus == 1, e => e.Date != null)
                 .WhereIf(input.InvoiceStatus == 2, e => e.Date == null)

                  .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                      .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5))

                      .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                     .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                     .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                     .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                     .WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                     .WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5));


            var result = (from o in filter

                          join user in _userRepository.GetAll() on (int)o.InstallerId equals (int)user.Id into userjoined
                          from user in userjoined.DefaultIfEmpty()

                          select new InstallerPaidExcelDto()
                          {
                              Id = o.Id,
                              JobId = o.JobFk.Id,
                              LeadId = (int)o.JobFk.LeadId,
                              JobNumber = o.JobFk.JobNumber,
                              Customer = o.JobFk.LeadFk.CompanyName,
                              Address = o.JobFk.Address + ", " + o.JobFk.Suburb + ", " + o.JobFk.State + "-" + o.JobFk.PostalCode,
                              PCode = o.JobFk.PostalCode,
                              InstallDate = o.JobFk.InstallationDate,
                              InvoiceNo = o.InvoiceNo,
                              InvDate = o.InvDate,
                              DueDate = o.DueDate,
                              Amount = o.Amount,
                              InvoiceType=o.Installation_Maintenance_Inspection,
                              ApprovedDate = o.ApproedDate,
                              ApprovedAmount = o.ApproedAmount,
                              ApprovedNotes = o.ApprovedNotes,
                              BankRefNo = o.BankRefNo,
                              Area = o.JobFk.LeadFk.Area,
                              InstallerName = user != null ? user.FullName : "",
                              State = o.JobFk.State,
                              SysPrice = o.JobFk.TotalCost,
                              SysCapacity = o.JobFk.SystemCapacity
                          });


            return _jobsExcelExporter.ExportReadyToPayInstallerInvoiceToFile(await result.ToListAsync());
        }

        public async Task<FileDto> GetAllToExcelPerc(InstallerNewInputForExcelDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var filteredLeads = _jobInstallerInvoiceRepository.GetAll().Include(e => e.JobFk).Include(e => e.JobFk.LeadFk)
                //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFk.JobNumber.Contains(input.Filter) || e.JobFk.LeadFk.CompanyName.Contains(input.Filter) || e.JobFk.LeadFk.Mobile.Contains(input.Filter) || e.JobFk.LeadFk.Phone.Contains(input.Filter) || e.JobFk.LeadFk.Email.Contains(input.Filter) || e.InvNo.ToString().Contains(input.Filter))
                .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.JobNumber == input.Filter)
                .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Mobile == input.Filter)
                .WhereIf(input.FilterName == "Phone" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Phone == input.Filter)
                .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.CompanyName == input.Filter)
                .WhereIf(input.FilterName == "Email" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobFk.LeadFk.Email == input.Filter)
                .WhereIf(input.FilterName == "InvNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.InvNo.ToString() == input.Filter)
                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.Installation_Maintenance_Inspection == input.InvoiceType)
                .WhereIf(input.InstallerId != null && input.InstallerId != 0, e => e.InstallerId == input.InstallerId)
                .WhereIf(input.DateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.DateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .Where(e => e.IsPaid == false && e.IsVerify == false && e.JobFk.LeadFk.OrganizationId == input.OrganizationUnit);

            //InstallerNewSummaryCountDto summary = new InstallerNewSummaryCountDto();
            //summary.Total = filteredLeads.Count();
            //summary.Pending = filteredLeads.Where(e => e.IsVerify != true).Count();
            //summary.Verified = filteredLeads.Where(e => e.IsVerify != true).Count();


            var result = (from o in filteredLeads

                          join user in _userRepository.GetAll() on (int)o.InstallerId equals (int)user.Id into userjoined
                          from user in userjoined.DefaultIfEmpty()

                          let BetteryKw = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && j.JobId == o.JobId).Sum(e => e.Quantity * e.ProductItemFk.Size)

                          select new InstallerNewDto
                          {
                              PaymentDate = o.CreationTime.AddHours(diffHour),
                              State = o.JobFk.State,
                              Area = o.JobFk.LeadFk.Area,
                              ProjectName = o.JobFk.JobNumber,
                              CompanyName = user.CompanyName,
                              installername = user.Name,
                              SysAmount = o.JobFk.TotalCost,
                              Per = o.JobFk.TotalCost > 0  && o.Amount > 0? ( o.Amount / o.JobFk.TotalCost) * 100  : 0,
                              Ammount = o.Amount,
                              invoiceType = o.Installation_Maintenance_Inspection,
                              invoiceNotes = o.Notes,
                              TotalKW = o.JobFk.SystemCapacity > 0 ? (BetteryKw > 0 ? (o.JobFk.SystemCapacity + BetteryKw) : o.JobFk.SystemCapacity) : (BetteryKw > 0 ? BetteryKw : 0),
                              AVGInvAmtPerKw = o.JobFk.SystemCapacity + BetteryKw > 0 ? o.Amount / o.JobFk.SystemCapacity + BetteryKw : 0
                          });

            return _jobsExcelExporter.ExportInvoicePerToFile(result.ToList());

        }

    }
}