﻿using System.Collections.Generic;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.Storage;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Transactions;
using Abp.Domain.Uow;
using TheSolarProduct.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.Common;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.DataVaults;
using TheSolarProduct.Quotations;
using TheSolarProduct.ThirdPartyApis.GreenDeals;
using Newtonsoft.Json;

namespace TheSolarProduct.Jobs
{
    [AbpAuthorize(AppPermissions.Pages_ProductItems, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class ProductItemsAppService : TheSolarProductAppServiceBase, IProductItemsAppService
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IProductItemsExcelExporter _productItemsExcelExporter;
        private readonly IRepository<ProductType, int> _lookup_productTypeRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<ProductItemLocation> _productiteamlocationRepository;
        private readonly DbContext _context;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private TheSolarProductDbContext _database;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IGreenDealAppService _greenDealRepository;

        public ProductItemsAppService(IRepository<ProductItem> productItemRepository
            , IProductItemsExcelExporter productItemsExcelExporter
            , IRepository<ProductType, int> lookup_productTypeRepository
            , IWebHostEnvironment env
            , IRepository<Tenant> tenantRepository
            , ITempFileCacheManager tempFileCacheManager,
            IRepository<Warehouselocation> warehouselocationRepository,
            IRepository<ProductItemLocation> productiteamlocationRepository
            //, DbContext context
            , IUnitOfWorkManager unitOfWorkManager
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            ICommonLookupAppService CommonDocumentSaveRepository,
            IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
            IGreenDealAppService greenDealRepository)
        {
            _productItemRepository = productItemRepository;
            _productItemsExcelExporter = productItemsExcelExporter;
            _lookup_productTypeRepository = lookup_productTypeRepository;
            _env = env;
            _tenantRepository = tenantRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _warehouselocationRepository = warehouselocationRepository;
            _productiteamlocationRepository = productiteamlocationRepository;
            //_context = context;
            _unitOfWorkManager = unitOfWorkManager;
            _dbcontextprovider = dbcontextprovider;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _greenDealRepository = greenDealRepository;

        }

        public async Task<PagedResultDto<GetProductItemForViewDto>> GetAll(GetAllProductItemsInput input)
        {
            var productLocation = _productiteamlocationRepository.GetAll().AsNoTracking();
            var productIds = new List<int>();
            if (input.location != null)
            {
                productIds = await productLocation.Where(e => input.location.Contains(e.WarehouselocationId) && e.SalesTag == true).Select(e => e.ProductItemId).Distinct().ToListAsync();

            }



            var filteredProductItems = _productItemRepository.GetAll()
                        .Include(e => e.ProductTypeFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Manufacturer.Contains(input.Filter) || e.Model.Contains(input.Filter) || e.Series.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ManufacturerFilter), e => e.Manufacturer == input.ManufacturerFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ModelFilter), e => e.Model == input.ModelFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SeriesFilter), e => e.Series == input.SeriesFilter)
                        .WhereIf(input.MinSizeFilter != null, e => e.Size >= input.MinSizeFilter)
                        .WhereIf(input.MaxSizeFilter != null, e => e.Size <= input.MaxSizeFilter)
                        .WhereIf(input.MinCodeFilter != null, e => e.Code >= input.MinCodeFilter)
                        .WhereIf(input.MaxCodeFilter != null, e => e.Code <= input.MaxCodeFilter)
                        .WhereIf(input.MinExpiryDateFilter != null, e => e.ExpiryDate >= input.MinExpiryDateFilter)
                        .WhereIf(input.MaxExpiryDateFilter != null, e => e.ExpiryDate <= input.MaxExpiryDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ProductTypeNameFilter), e => e.ProductTypeFk != null && e.ProductTypeFk.Name == input.ProductTypeNameFilter)
                        .WhereIf(input.ActiveOrDeactive != null && input.ActiveOrDeactive == "Active", e => e.Active == true)
                        .WhereIf(input.ActiveOrDeactive != null && input.ActiveOrDeactive == "Deactive", e => e.Active != true)
                        .WhereIf(input.ProductTypeId > 0, e => e.ProductTypeId == input.ProductTypeId)
                        .WhereIf(input.Attechment == "No", e => string.IsNullOrEmpty(e.FileName))
                        .WhereIf(input.Attechment == "Yes", e => !string.IsNullOrEmpty(e.FileName))
                        .WhereIf(input.location != null, e => productIds.Contains(e.Id));

            var pagedAndFilteredProductItems = filteredProductItems
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var productItems = from o in pagedAndFilteredProductItems
                               join o1 in _lookup_productTypeRepository.GetAll() on o.ProductTypeId equals o1.Id into j1
                               from s1 in j1.DefaultIfEmpty()

                               select new GetProductItemForViewDto()
                               {
                                   ProductItem = new ProductItemDto
                                   {
                                       Name = o.Name,
                                       Manufacturer = o.Manufacturer,
                                       Model = o.Model,
                                       Series = o.Series,
                                       Size = o.Size,
                                       Code = o.Code,
                                       Id = o.Id,
                                       FileName = o.FileName,
                                       FilePath = o.FilePath,
                                       ExpiryDate = o.ExpiryDate,
                                       Status = o.Active == true ? "Active" : "Deactiive"
                                   },
                                   ProductTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
                               };

            var totalCount = await filteredProductItems.CountAsync();

            return new PagedResultDto<GetProductItemForViewDto>(
                totalCount,
                await productItems.ToListAsync()
            );
        }

        public async Task<GetProductItemForViewDto> GetProductItemForView(int id)
        {
            var productItem = await _productItemRepository.GetAsync(id);

            var output = new GetProductItemForViewDto { ProductItem = ObjectMapper.Map<ProductItemDto>(productItem) };

            if (output.ProductItem.ProductTypeId != null)
            {
                var _lookupProductType = await _lookup_productTypeRepository.FirstOrDefaultAsync((int)output.ProductItem.ProductTypeId);
                output.ProductTypeName = _lookupProductType?.Name?.ToString();
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ProductItems_Edit)]
        public async Task<GetProductItemForEditOutput> GetProductItemForEdit(EntityDto input)
        {
            var productItem = await _productItemRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProductItemForEditOutput { ProductItem = ObjectMapper.Map<CreateOrEditProductItemDto>(productItem) };
            output.ProductItem.IsActive = productItem.Active;
            if (output.ProductItem.ProductTypeId != null)
            {
                var _lookupProductType = await _lookup_productTypeRepository.FirstOrDefaultAsync((int)output.ProductItem.ProductTypeId);
                output.ProductTypeName = _lookupProductType?.Name?.ToString();
            }
            output.ProductItem.QLDSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 1).Select(e => e.SalesTag).FirstOrDefault();
            output.ProductItem.VICSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 2).Select(e => e.SalesTag).FirstOrDefault();
            output.ProductItem.NSWSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 3).Select(e => e.SalesTag).FirstOrDefault();
            output.ProductItem.WASalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 4).Select(e => e.SalesTag).FirstOrDefault();
            output.ProductItem.NTSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 5).Select(e => e.SalesTag).FirstOrDefault();
            output.ProductItem.TASSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 6).Select(e => e.SalesTag).FirstOrDefault();
            output.ProductItem.SASalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 7).Select(e => e.SalesTag).FirstOrDefault();
            output.ProductItem.AUSSalesTag = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id && e.WarehouselocationId == 8).Select(e => e.SalesTag).FirstOrDefault();
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditProductItemDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ProductItems_Create)]
        protected virtual async Task Create(CreateOrEditProductItemDto input)
        {
            var productItem = ObjectMapper.Map<ProductItem>(input);

            if (AbpSession.TenantId != null)
            {
                productItem.TenantId = (int)AbpSession.TenantId;
            }
            productItem.Active = input.IsActive;
            int Productiteamid = _productItemRepository.InsertAndGetId(productItem);

            var locationids = _warehouselocationRepository.GetAll().Select(e => e.Id).ToList();

            foreach (var warehouseid in locationids)
            {
                bool salestage = false;
                if (warehouseid == 1)
                {
                    if (input.QLDSalesTag == true)
                    {
                        salestage = true;
                    }
                }
                else if (warehouseid == 2)
                {
                    if (input.VICSalesTag == true)
                    {
                        salestage = true;
                    }
                }
                else if (warehouseid == 3)
                {
                    if (input.NSWSalesTag == true)
                    {
                        salestage = true;
                    }
                }
                else if (warehouseid == 4)
                {
                    if (input.WASalesTag == true)
                    {
                        salestage = true;
                    }
                }
                else if (warehouseid == 5)
                {
                    if (input.NTSalesTag == true)
                    {
                        salestage = true;
                    }
                }
                else if (warehouseid == 6)
                {
                    if (input.TASSalesTag == true)
                    {
                        salestage = true;
                    }
                }
                else if (warehouseid == 7)
                {
                    if (input.SASalesTag == true)
                    {
                        salestage = true;
                    }
                }
                else if (warehouseid == 8)
                {
                    if (input.AUSSalesTag == true)
                    {
                        salestage = true;
                    }
                }
                ProductItemLocation productiteamlocation = new ProductItemLocation();
                productiteamlocation.ProductItemId = Productiteamid;
                productiteamlocation.WarehouselocationId = warehouseid;
                productiteamlocation.SalesTag = salestage;
                if (AbpSession.TenantId != null)
                {
                    productiteamlocation.TenantId = (int)AbpSession.TenantId;
                }
                await _productiteamlocationRepository.InsertAsync(productiteamlocation);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ProductItems_Edit)]
        protected virtual async Task Update(CreateOrEditProductItemDto input)
        {
            var productItem = await _productItemRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 15;
            dataVaultLog.ActionNote = "Product Items Updated : " + productItem.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != productItem.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Stock Item";
                history.PrevValue = productItem.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Manufacturer != productItem.Manufacturer)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Brand";
                history.PrevValue = productItem.Manufacturer;
                history.CurValue = input.Manufacturer;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Model != productItem.Model)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Stock Model";
                history.PrevValue = productItem.Model;
                history.CurValue = input.Model;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Series != productItem.Series)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Stock Series";
                history.PrevValue = productItem.Series;
                history.CurValue = input.Series;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Size != productItem.Size)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Stock Size";
                history.PrevValue = productItem.Size.ToString();
                history.CurValue = input.Size.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            //if (input.StockId != productItem.st)
            //{
            //    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
            //    history.FieldName = "Stock";
            //    history.PrevValue = productItem.StockId.ToString();
            //    history.CurValue = input.StockId.ToString();
            //    history.Action = "Update";
            //    history.ActivityLogId = dataVaultLogId;
            //    List.Add(history);
            //}

            if (input.IsActive != productItem.Active)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Active";
                history.PrevValue = productItem.Active.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ProductTypeId != productItem.ProductTypeId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Stock Category";
                history.PrevValue = productItem.ProductTypeId > 0 ? _lookup_productTypeRepository.Get(productItem.ProductTypeId).Name : "";
                history.CurValue = input.ProductTypeId > 0 ? _lookup_productTypeRepository.Get(input.ProductTypeId).Name : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsScanned != productItem.IsScanned)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsScanned";
                history.PrevValue = productItem.IsScanned.ToString();
                history.CurValue = input.IsScanned.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ECommerce != productItem.ECommerce)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ECommerce";
                history.PrevValue = productItem.ECommerce.ToString();
                history.CurValue = input.ECommerce.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.PerformanceWarranty != productItem.PerformanceWarranty)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "PerformanceWarranty";
                history.PrevValue = productItem.PerformanceWarranty.ToString();
                history.CurValue = input.PerformanceWarranty.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ProductWarranty != productItem.ProductWarranty)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ProductWarranty";
                history.PrevValue = productItem.ProductWarranty.ToString();
                history.CurValue = input.ProductWarranty.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ExtendedWarranty != productItem.ExtendedWarranty)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ExtendedWarranty";
                history.PrevValue = productItem.ExtendedWarranty.ToString();
                history.CurValue = input.ExtendedWarranty.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.WorkmanshipWarranty != productItem.WorkmanshipWarranty)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "WorkmanshipWarranty";
                history.PrevValue = productItem.WorkmanshipWarranty.ToString();
                history.CurValue = input.WorkmanshipWarranty.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.PricePerWatt != productItem.PricePerWatt)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "PricePerWatt";
                history.PrevValue = productItem.PricePerWatt.ToString();
                history.CurValue = input.PricePerWatt.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Amount != productItem.Amount)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Amount";
                history.PrevValue = productItem.Amount.ToString();
                history.CurValue = input.Amount.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ManuEmail != productItem.ManuEmail)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ManuEmail";
                history.PrevValue = productItem.ManuEmail;
                history.CurValue = input.ManuEmail;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ManuPhone != productItem.ManuPhone)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ManuPhone";
                history.PrevValue = productItem.ManuPhone;
                history.CurValue = input.ManuPhone;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            
            if (input.ManuMobile != productItem.ManuMobile)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ManuMobile";
                history.PrevValue = productItem.ManuMobile;
                history.CurValue = input.ManuMobile;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            
            if (input.ManuWebsite != productItem.ManuWebsite)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "ManuWebsite";
                history.PrevValue = productItem.ManuWebsite;
                history.CurValue = input.ManuWebsite;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            productItem.Active = input.IsActive;
            ObjectMapper.Map(input, productItem);

            var locationids = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == input.Id).ToList();

            if (locationids.Count > 0)
            {
                foreach (var warehouseid in locationids)
                {
                    ///  bool salestage = false;
                    if (warehouseid.WarehouselocationId == 1)
                    {
                        if (warehouseid.SalesTag != input.QLDSalesTag)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "QLDSales";
                            history.PrevValue = warehouseid.SalesTag.ToString();
                            history.CurValue = input.QLDSalesTag.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        warehouseid.SalesTag = input.QLDSalesTag;
                    }
                    else if (warehouseid.WarehouselocationId == 2)
                    {
                        if (warehouseid.SalesTag != input.VICSalesTag)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "VICSales";
                            history.PrevValue = warehouseid.SalesTag.ToString();
                            history.CurValue = input.VICSalesTag.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        warehouseid.SalesTag = input.VICSalesTag;
                    }
                    else if (warehouseid.WarehouselocationId == 3)
                    {
                        if (warehouseid.SalesTag != input.NSWSalesTag)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "NSWSales";
                            history.PrevValue = warehouseid.SalesTag.ToString();
                            history.CurValue = input.NSWSalesTag.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        warehouseid.SalesTag = input.NSWSalesTag;
                    }
                    else if (warehouseid.WarehouselocationId == 4)
                    {
                        if (warehouseid.SalesTag != input.WASalesTag)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "WASales";
                            history.PrevValue = warehouseid.SalesTag.ToString();
                            history.CurValue = input.WASalesTag.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        warehouseid.SalesTag = input.WASalesTag;
                    }
                    else if (warehouseid.WarehouselocationId == 5)
                    {
                        if (warehouseid.SalesTag != input.NTSalesTag)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "NTSales";
                            history.PrevValue = warehouseid.SalesTag.ToString();
                            history.CurValue = input.NTSalesTag.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        warehouseid.SalesTag = input.NTSalesTag;
                    }
                    else if (warehouseid.WarehouselocationId == 6)
                    {
                        if (warehouseid.SalesTag != input.TASSalesTag)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "TASSales";
                            history.PrevValue = warehouseid.SalesTag.ToString();
                            history.CurValue = input.TASSalesTag.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        warehouseid.SalesTag = input.TASSalesTag;
                    }
                    else if (warehouseid.WarehouselocationId == 7)
                    {
                        if (warehouseid.SalesTag != input.SASalesTag)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "SASales";
                            history.PrevValue = warehouseid.SalesTag.ToString();
                            history.CurValue = input.SASalesTag.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        warehouseid.SalesTag = input.SASalesTag;
                    }
                    else if (warehouseid.WarehouselocationId == 8)
                    {
                        if (warehouseid.SalesTag != input.AUSSalesTag)
                        {
                            DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                            history.FieldName = "AUSSales";
                            history.PrevValue = warehouseid.SalesTag.ToString();
                            history.CurValue = input.AUSSalesTag.ToString();
                            history.Action = "Update";
                            history.ActivityLogId = dataVaultLogId;
                            List.Add(history);
                        }
                        warehouseid.SalesTag = input.AUSSalesTag;
                    }
                    await _productiteamlocationRepository.UpdateAsync(warehouseid);
                }
            }
            else
            {
                var warehousids = _warehouselocationRepository.GetAll().Select(e => e.Id).ToList();

                foreach (int warehouseid in warehousids)
                {
                    ProductItemLocation productiteamlocation = new ProductItemLocation();

                    bool salestage = false;
                    if (warehouseid == 1)
                    {
                        //if (salestage != input.QLDSalesTag)
                        //{
                        //	DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        //	history.FieldName = "IsQLDSales";
                        //	history.PrevValue = productiteamlocation.SalesTag.ToString();
                        //	history.CurValue = input.AUSSalesTag.ToString();
                        //	history.Action = "Update";
                        //	history.ActivityLogId = dataVaultLogId;
                        //	List.Add(history);
                        //}
                        if (input.QLDSalesTag == true)
                        {
                            salestage = true;
                        }
                    }
                    else if (warehouseid == 2)
                    {

                        if (input.VICSalesTag == true)
                        {
                            salestage = true;
                        }
                    }
                    else if (warehouseid == 3)
                    {
                        if (input.NSWSalesTag == true)
                        {
                            salestage = true;
                        }
                    }
                    else if (warehouseid == 4)
                    {
                        if (input.WASalesTag == true)
                        {
                            salestage = true;
                        }
                    }
                    else if (warehouseid == 5)
                    {
                        if (input.NTSalesTag == true)
                        {
                            salestage = true;
                        }
                    }
                    else if (warehouseid == 6)
                    {
                        if (input.TASSalesTag == true)
                        {
                            salestage = true;
                        }
                    }
                    else if (warehouseid == 7)
                    {
                        if (input.SASalesTag == true)
                        {
                            salestage = true;
                        }
                    }
                    else if (warehouseid == 8)
                    {
                        if (input.AUSSalesTag == true)
                        {
                            salestage = true;
                        }
                    }
                    productiteamlocation.ProductItemId = (int)input.Id;
                    productiteamlocation.WarehouselocationId = warehouseid;
                    productiteamlocation.SalesTag = salestage;
                    if (AbpSession.TenantId != null)
                    {
                        productiteamlocation.TenantId = (int)AbpSession.TenantId;
                    }
                    await _productiteamlocationRepository.InsertAsync(productiteamlocation);
                }

            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

        }

        [AbpAuthorize(AppPermissions.Pages_ProductItems_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _productItemRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 15;
            dataVaultLog.ActionNote = "Product Items Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            await _productItemRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetProductItemsToExcel(GetAllProductItemsForExcelInput input)
        {

            //var filteredProductItems = _productItemRepository.GetAll()
            //            .Include(e => e.ProductTypeFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Manufacturer.Contains(input.Filter) || e.Model.Contains(input.Filter) || e.Series.Contains(input.Filter))
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.ManufacturerFilter), e => e.Manufacturer == input.ManufacturerFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.ModelFilter), e => e.Model == input.ModelFilter)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.SeriesFilter), e => e.Series == input.SeriesFilter)
            //            .WhereIf(input.MinSizeFilter != null, e => e.Size >= input.MinSizeFilter)
            //            .WhereIf(input.MaxSizeFilter != null, e => e.Size <= input.MaxSizeFilter)
            //            .WhereIf(input.MinCodeFilter != null, e => e.Code >= input.MinCodeFilter)
            //            .WhereIf(input.MaxCodeFilter != null, e => e.Code <= input.MaxCodeFilter)

            //            .WhereIf(input.MinExpiryDateFilter != null, e => e.ExpiryDate >= input.MinExpiryDateFilter)
            //            .WhereIf(input.MaxExpiryDateFilter != null, e => e.ExpiryDate <= input.MaxExpiryDateFilter)

            //            .WhereIf(!string.IsNullOrWhiteSpace(input.ProductTypeNameFilter), e => e.ProductTypeFk != null && e.ProductTypeFk.Name == input.ProductTypeNameFilter);
            var productLocation = _productiteamlocationRepository.GetAll().AsNoTracking();
            var productIds = new List<int>();
            if (input.location != null)
            {
                productIds = await productLocation.Where(e => input.location.Contains(e.WarehouselocationId) && e.SalesTag == true).Select(e => e.ProductItemId).Distinct().ToListAsync();

            }



            var filteredProductItems = _productItemRepository.GetAll()
                        .Include(e => e.ProductTypeFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Manufacturer.Contains(input.Filter) || e.Model.Contains(input.Filter) || e.Series.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ManufacturerFilter), e => e.Manufacturer == input.ManufacturerFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ModelFilter), e => e.Model == input.ModelFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SeriesFilter), e => e.Series == input.SeriesFilter)
                        .WhereIf(input.MinSizeFilter != null, e => e.Size >= input.MinSizeFilter)
                        .WhereIf(input.MaxSizeFilter != null, e => e.Size <= input.MaxSizeFilter)
                        .WhereIf(input.MinCodeFilter != null, e => e.Code >= input.MinCodeFilter)
                        .WhereIf(input.MaxCodeFilter != null, e => e.Code <= input.MaxCodeFilter)
                        .WhereIf(input.MinExpiryDateFilter != null, e => e.ExpiryDate >= input.MinExpiryDateFilter)
                        .WhereIf(input.MaxExpiryDateFilter != null, e => e.ExpiryDate <= input.MaxExpiryDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ProductTypeNameFilter), e => e.ProductTypeFk != null && e.ProductTypeFk.Name == input.ProductTypeNameFilter)
                        .WhereIf(input.ActiveOrDeactive != null && input.ActiveOrDeactive == "Active", e => e.Active == true)
                        .WhereIf(input.ActiveOrDeactive != null && input.ActiveOrDeactive == "Deactive", e => e.Active != true)
                        .WhereIf(input.ProductTypeId > 0, e => e.ProductTypeId == input.ProductTypeId)
                        .WhereIf(input.Attechment == "No", e => string.IsNullOrEmpty(e.FileName))
                        .WhereIf(input.Attechment == "Yes", e => !string.IsNullOrEmpty(e.FileName))
                        .WhereIf(input.location != null, e => productIds.Contains(e.Id));

            var query = (from o in filteredProductItems
                         join o1 in _lookup_productTypeRepository.GetAll() on o.ProductTypeId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         let loactions = _productiteamlocationRepository.GetAll().Where(e => e.ProductItemId == o.Id)

                         select new GetProductItemForViewDto()
                         {
                             ProductItem = new ProductItemDto
                             {
                                 Name = o.Name,
                                 Manufacturer = o.Manufacturer,
                                 Model = o.Model,
                                 Series = o.Series,
                                 Size = o.Size,
                                 Code = o.Code,
                                 Id = o.Id
                             },
                             ProductTypeName = s1 == null || s1.Name == null ? "" : s1.Name.ToString(),
                             NSW = loactions.Where(e => e.WarehouselocationId == 3).Select(e => e.SalesTag).FirstOrDefault() == true ? "Open" : "Close",
                             QLD = loactions.Where(e => e.WarehouselocationId == 1).Select(e => e.SalesTag).FirstOrDefault() == true ? "Open" : "Close",
                             NT = loactions.Where(e => e.WarehouselocationId == 5).Select(e => e.SalesTag).FirstOrDefault() == true ? "Open" : "Close",
                             SA = loactions.Where(e => e.WarehouselocationId == 7).Select(e => e.SalesTag).FirstOrDefault() == true ? "Open" : "Close",
                             AUS = loactions.Where(e => e.WarehouselocationId == 8).Select(e => e.SalesTag).FirstOrDefault() == true ? "Open" : "Close",
                             TAS = loactions.Where(e => e.WarehouselocationId == 6).Select(e => e.SalesTag).FirstOrDefault() == true ? "Open" : "Close",
                             WA = loactions.Where(e => e.WarehouselocationId == 4).Select(e => e.SalesTag).FirstOrDefault() == true ? "Open" : "Close",
                             VIC = loactions.Where(e => e.WarehouselocationId == 2).Select(e => e.SalesTag).FirstOrDefault() == true ? "Open" : "Close"
                         }); ;

            var totalCount = await filteredProductItems.CountAsync();

            var productItemListDtos = await query.ToListAsync();

            return _productItemsExcelExporter.ExportToFile(productItemListDtos);
        }

        public async Task<List<ProductItemProductTypeLookupTableDto>> GetAllProductTypeForTableDropdown()
        {
            return await _lookup_productTypeRepository.GetAll()
                .Select(productType => new ProductItemProductTypeLookupTableDto
                {
                    Id = productType.Id,
                    DisplayName = productType == null || productType.Name == null ? "" : productType.Name.ToString()
                }).ToListAsync();
        }

        public async Task FetchAndInsert()
        {
            int ProductItemId = 0;
            int StockItemsLocationId = 0;

            using (_unitOfWorkManager.Current.SetTenantId(AbpSession.TenantId))
            {
                ProductItemId = _productItemRepository.GetAll().OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault();
                StockItemsLocationId = _productiteamlocationRepository.GetAll().OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault();
            }

            //String SqlconString = "Data Source=52.140.99.10;Initial Catalog=quickstockdb;Persist Security Info=True;User Id=ariseadmin;Password=Google$2020#d;Min Pool Size=20; Max Pool Size=200;";
            String SqlconString = ApplicationSettingConsts.QuickStockSqlconString;
            List<CreateOrEditProductItemDto> ProductItem = new List<CreateOrEditProductItemDto>();
            List<CreateOrEditProductItemDto> StockItemsLocation = new List<CreateOrEditProductItemDto>();


            using (SqlConnection sqlCon = new SqlConnection(SqlconString))
            {
                sqlCon.Open();
                SqlCommand sql_cmnd2 = new SqlCommand("GetData_tblStockItems", sqlCon);
                sql_cmnd2.CommandType = CommandType.StoredProcedure;
                sql_cmnd2.Parameters.AddWithValue("@Id", SqlDbType.Int).Value = ProductItemId;
                var tablePicklist = new DataTable();
                tablePicklist.Load(sql_cmnd2.ExecuteReader());
                var query = from pickItem in tablePicklist.AsEnumerable()
                            select new CreateOrEditProductItemDto
                            {
                                Id = pickItem.Field<int>("StockItemID"),
                                ProductTypeId = pickItem.Field<int>("StockCategoryID"),
                                Name = pickItem.Field<string>("StockItem"),
                                Manufacturer = pickItem.Field<string>("StockManufacturer"),
                                Model = pickItem.Field<string>("StockModel"),
                                Series = pickItem.Field<string>("StockSeries"),
                                Size = pickItem.Field<decimal?>("Size"),
                                Code = pickItem.Field<int?>("StockCode"),
                                ShortName = pickItem.Field<string>("ShortName"),
                                Description = pickItem.Field<string>("StockDescription"),
                                InverterCert = pickItem.Field<string>("InverterCert"),
                                IsActive = pickItem.Field<bool>("Active"),
                                ApprovedDate = pickItem.Field<DateTime?>("ApprovedDate"),
                                ExpiryDate = pickItem.Field<DateTime?>("ExpiryDate"),
                                FireTested = pickItem.Field<string>("Firetested"),
                                ACPower = pickItem.Field<string>("ACPower"),
                                StockId = Convert.ToString(pickItem.Field<int>("StockItemID")),
                            };
                sqlCon.Close();
                ProductItem = query.ToList();
            }

            foreach (var item in ProductItem)
            {
                var productItem = ObjectMapper.Map<ProductItem>(item);

                using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
                {
                    _dbcontextprovider.GetDbContext().ProductItems.AddRange(productItem);

                    await _dbcontextprovider.GetDbContext().Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.ProductItems ON");

                    _dbcontextprovider.GetDbContext().SaveChanges();

                    await _dbcontextprovider.GetDbContext().Database.ExecuteSqlCommandAsync("SET IDENTITY_INSERT dbo.ProductItems OFF");

                    await uow.CompleteAsync();
                }
            }
        }


        public async Task SaveProductIteamDocument(string FileToken, string FileNames, int id)
        {
            var ProductItems = _productItemRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
            string FileName = ProductItems.Model.Replace("\\", "") + DateTime.Now.Ticks + ".pdf";
            byte[] ByteArray = _tempFileCacheManager.GetFile(FileToken);
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileName, "ProductItem", 0, 0, AbpSession.TenantId);

            ProductItems.FileName = filepath.filename;
            ProductItems.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\ProductItem\\";
            await _productItemRepository.UpdateAsync(ProductItems);
        }

        public async Task<List<GetProductItemsCountWithProdoctTypeDto>> GetAllProductItemsCountWithProdoctType()
        {
            var Products = await _productItemRepository.GetAll()
                .Include(e => e.ProductTypeFk)
                .Where(e => e.ECommerce == true)
                .GroupBy(e => new { e.ProductTypeFk.Name, e.ProductTypeId, e.ProductTypeFk.Image })
                .Select(e => new { e.Key.ProductTypeId, e.Key.Name, e.Key.Image, items = e.Count() })
                .AsNoTracking().ToListAsync();

            var result = from o in Products
                         select new GetProductItemsCountWithProdoctTypeDto()
                         {
                             Id = o.ProductTypeId,
                             ProductType = o.Name,
                             Image = o.Image,
                             ProductItemCount = o.items
                         };

            return result.ToList();
        }

        public async Task SaveProductIteamImage(string FileToken, int id)
        {
            var ProductItems = _productItemRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
            string FileName = ProductItems.Model.Replace("\\", "") + DateTime.Now.Ticks + ".jpg";
            byte[] ByteArray = _tempFileCacheManager.GetFile(FileToken);
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileName, "ProductItemImage", 0, 0, AbpSession.TenantId);

            ProductItems.ImageName = filepath.filename;
            ProductItems.ImagePath = "\\Documents" + "\\" + filepath.TenantName + "\\ProductItemImage\\";
            await _productItemRepository.UpdateAsync(ProductItems);
        }

        //public async Task FetchProductItemFormGreenDeal()
        //{
        //    //Get Panels Form Green Deal
        //    var cecPanelRespose = await _greenDealRepository.GetCECPanels();
        //    bool IsPanelSuccess = cecPanelRespose.IsSuccessStatusCode;

        //    int itemNameNull = 0;
        //    if (IsPanelSuccess)
        //    {
        //        var jsonResponse = await cecPanelRespose.Content.ReadAsStringAsync();
        //        ThirdPartyApis.GreenDeals.Dtos.RootObject rootObject = JsonConvert.DeserializeObject<ThirdPartyApis.GreenDeals.Dtos.RootObject>(jsonResponse);

        //        if (rootObject != null)
        //        {
        //            int updatePanel = 0; int newPanel = 0;
        //            foreach (var item in rootObject.Response.CECProductItems)
        //            {
        //                //var panelItem = _productItemRepository.GetAll().Where(e => e.Model.ToLower().Trim() == item.Model.ToLower().Trim()).FirstOrDefault();
        //                //if(panelItem != null)
        //                //{
        //                //	updatePanel++;
        //                //                  }
        //                //else
        //                //{
        //                //	newPanel++;
        //                //                  }
        //                if(string.IsNullOrEmpty(item.Name))
        //                {
        //                    itemNameNull++;
        //                }
        //            }
        //        }
        //    }

        //    //Get Inverters Form Green Deal
        //    var cecInverterRespose = await _greenDealRepository.GetCECInverters();
        //    bool IsInverterSuccess = cecPanelRespose.IsSuccessStatusCode;

        //    if (IsInverterSuccess)
        //    {
        //        var jsonResponse = await cecInverterRespose.Content.ReadAsStringAsync();
        //        ThirdPartyApis.GreenDeals.Dtos.RootObject rootObject = JsonConvert.DeserializeObject<ThirdPartyApis.GreenDeals.Dtos.RootObject>(jsonResponse);

        //        if (rootObject != null)
        //        {
        //            foreach (var item in rootObject.Response.CECProductItems)
        //            {
        //                if (string.IsNullOrEmpty(item.Name))
        //                {
        //                    itemNameNull++;
        //                }
        //            }
        //        }
        //    }

        //    //Get Battries Form Green Deal
        //    var cecBatteryRespose = await _greenDealRepository.GetCECBattteris();
        //    bool IsBatterySuccess = cecPanelRespose.IsSuccessStatusCode;

        //    if (IsBatterySuccess)
        //    {
        //        var jsonResponse = await cecBatteryRespose.Content.ReadAsStringAsync();
        //        ThirdPartyApis.GreenDeals.Dtos.RootObject rootObject = JsonConvert.DeserializeObject<ThirdPartyApis.GreenDeals.Dtos.RootObject>(jsonResponse);

        //        if (rootObject != null)
        //        {
        //            foreach (var item in rootObject.Response.CECProductItems)
        //            {
        //                if (string.IsNullOrEmpty(item.Name))
        //                {
        //                    itemNameNull++;
        //                }
        //            }
        //        }
        //    }
        //}
    }
}