﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class ProductItemsExcelExporter : NpoiExcelExporterBase, IProductItemsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProductItemsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetProductItemForViewDto> productItems)
        {
            return CreateExcelPackage(
                "ProductItems.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("ProductItems"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Manufacturer"),
                        L("Model"),
                        L("Series"),
                        L("Size"),
                        L("ShortName"),
                        L("Description"),
                        L("Code"),
                        L("StockQuantity"),
                        L("MinLevel"),
                        L("CostPrice"),
                        L("ACPower"),
                        L("StockItem"),
                        L("StockId"),
                        (L("ProductType")) + L("Name"),
                        "QLD",
                        "NSW",
                        "VIC",
                        "NT",
                        "AUS",
                        "TAS",
                        "Wa",
                        "SA"
                        );

                    AddObjects(
                        sheet, 2, productItems,
                        _ => _.ProductItem.Name,
                        _ => _.ProductItem.Manufacturer,
                        _ => _.ProductItem.Model,
                        _ => _.ProductItem.Series,
                        _ => _.ProductItem.Size,
                        _ => _.ProductItem.ShortName,
                        _ => _.ProductItem.Description,
                        _ => _.ProductItem.Code,
                        _ => _.ProductItem.StockQuantity,
                        _ => _.ProductItem.MinLevel,
                        _ => _.ProductItem.CostPrice,
                        _ => _.ProductItem.ACPower,
                        _ => _.ProductItem.StockItem,
                        _ => _.ProductItem.StockId,
                        _ => _.ProductTypeName,
                        _ => _.QLD,
                        _ => _.NSW,
                        _ => _.VIC,
                        _ => _.NT,
                        _ => _.AUS,
                        _ => _.TAS,
                        _ => _.WA,
                        _ => _.SA
                        );

					
					
                });
        }
    }
}
