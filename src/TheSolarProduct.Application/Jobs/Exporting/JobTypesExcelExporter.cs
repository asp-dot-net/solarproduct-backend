﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class JobTypesExcelExporter : NpoiExcelExporterBase, IJobTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobTypeForViewDto> jobTypes)
        {
            return CreateExcelPackage(
                "JobTypes.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("JobTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DisplayOrder"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, jobTypes,
                        _ => _.JobType.Name,
                        _ => _.JobType.DisplayOrder,
                        _ => _.JobType.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
