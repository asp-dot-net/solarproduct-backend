﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class DepositOptionsExcelExporter : NpoiExcelExporterBase, IDepositOptionsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DepositOptionsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDepositOptionForViewDto> depositOptions)
        {
            return CreateExcelPackage(
                "DepositOptions.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("DepositOptions"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DisplayOrder")
                        );

                    AddObjects(
                        sheet, 2, depositOptions,
                        _ => _.DepositOption.Name,
                        _ => _.DepositOption.DisplayOrder
                        );

                    for (var i = 1; i <= depositOptions.Count; i++)
                    {
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                    }

                });
        }
    }
}
