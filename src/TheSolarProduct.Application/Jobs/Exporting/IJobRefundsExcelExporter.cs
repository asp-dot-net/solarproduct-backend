﻿using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.JobTrackers.Dtos;

namespace TheSolarProduct.Jobs.Exporting
{
    public interface IJobRefundsExcelExporter
    {
        FileDto ExportToFile(List<GetViewRefundTrackerDto> jobRefunds, string FileName);
    }
}