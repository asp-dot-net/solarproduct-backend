﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class JobPromotionsExcelExporter : NpoiExcelExporterBase, IJobPromotionsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobPromotionsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobPromotionForViewDto> jobs)
        {
            return CreateExcelPackage(
                "FreebiesTracker.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("FreebiesTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        L("CompanyName"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("Email"),
                        L("ProjectStatus"),
                        L("BalQwing"),
                        L("TrackingNumber"),
                        L("SmsSend"),
                        L("LeadOwner"),
                        L("Description")
                        );

                    AddObjects(
                        sheet, 2, jobs,
                        _ => _.JobNumber,
                        _ => _.CompanyName,
                        _ => _.Mobile,
                        _ => _.Address,
                        _ => _.Suburb,
                        _ => _.State,
                        _ => _.PostCode,
                        _ => _.Email,
                        _ => _.ProjectStatus,
                        _ => _.BalQwing,
                        _ => _.JobPromotion.TrackingNumber,
                        _ => _.JobPromotion.SmsSend,
                        _ => _.CurrentLeadOwaner,
                        _ => _.JobPromotion.Description
                        );

                    for (var i = 1; i <= jobs.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");
                    }
                    sheet.AutoSizeColumn(9);

                });
        }

        public FileDto ExportCsvToFile(List<GetJobPromotionForViewDto> jobs)
        {
            return CreateExcelPackage(
                "FreebiesTracker.csv",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("FreebiesTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        L("CompanyName"),
                        L("Mobile"),
                        L("Address"),
                        L("Suburb"),
                        L("State"),
                        L("PostalCode"),
                        L("Email"),
                        L("ProjectStatus"),
                        L("BalQwing"),
                        L("TrackingNumber"),
                        L("SmsSend"),
                        L("LeadOwner"),
                        L("Description")
                        );

                    AddObjects(
                        sheet, 2, jobs,
                        _ => _.JobNumber,
                        _ => _.CompanyName,
                        _ => _.Mobile,
                        _ => _.Address,
                        _ => _.Suburb,
                        _ => _.State,
                        _ => _.PostCode,
                        _ => _.Email,
                        _ => _.ProjectStatus,
                        _ => _.BalQwing,
                        _ => _.JobPromotion.TrackingNumber,
                        _ => _.JobPromotion.SmsSend,
                        _ => _.CurrentLeadOwaner,
                        _ => _.JobPromotion.Description
                        );



                });
        }
    }
}
