﻿using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Jobs.Exporting
{
    public interface IHouseTypesExcelExporter
    {
        FileDto ExportToFile(List<GetHouseTypeForViewDto> houseTypes);
    }
}