﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class ElecDistributorsExcelExporter : NpoiExcelExporterBase, IElecDistributorsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ElecDistributorsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetElecDistributorForViewDto> elecDistributors)
        {
            return CreateExcelPackage(
                "ElecDistributors.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("ElecDistributors"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Seq"),
                        L("NSW"),
                        L("SA"),
                        L("QLD"),
                        L("VIC"),
                        L("WA"),
                        L("ACT"),
                        L("TAS"),
                        L("NT"),
                        L("ElectDistABB"),
                        L("ElecDistAppReq"),
                        L("GreenBoatDistributor")
                        );

                    AddObjects(
                        sheet, 2, elecDistributors,
                        _ => _.ElecDistributor.Name,
                        _ => _.ElecDistributor.Seq,
                        _ => _.ElecDistributor.NSW,
                        _ => _.ElecDistributor.SA,
                        _ => _.ElecDistributor.QLD,
                        _ => _.ElecDistributor.VIC,
                        _ => _.ElecDistributor.WA,
                        _ => _.ElecDistributor.ACT,
                        _ => _.ElecDistributor.TAS,
                        _ => _.ElecDistributor.NT,
                        _ => _.ElecDistributor.ElectDistABB,
                        _ => _.ElecDistributor.ElecDistAppReq,
                        _ => _.ElecDistributor.GreenBoatDistributor
                        );

					
					
                });
        }
    }
}
