﻿using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Jobs.Exporting
{
    public interface IJobStatusesExcelExporter
    {
        FileDto ExportToFile(List<GetJobStatusForViewDto> jobStatuses);
    }
}