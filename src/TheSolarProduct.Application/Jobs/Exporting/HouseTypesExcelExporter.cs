﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class HouseTypesExcelExporter : NpoiExcelExporterBase, IHouseTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public HouseTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetHouseTypeForViewDto> houseTypes)
        {
            return CreateExcelPackage(
                "HouseTypes.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("HouseTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, houseTypes,
                        _ => _.HouseType.Name,
                         _ => _.HouseType.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
