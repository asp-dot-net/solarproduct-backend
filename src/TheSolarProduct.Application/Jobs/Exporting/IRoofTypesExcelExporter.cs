﻿using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Jobs.Exporting
{
    public interface IRoofTypesExcelExporter
    {
        FileDto ExportToFile(List<GetRoofTypeForViewDto> roofTypes);
    }
}