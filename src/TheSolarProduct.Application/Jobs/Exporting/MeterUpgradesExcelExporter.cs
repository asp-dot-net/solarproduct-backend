﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class MeterUpgradesExcelExporter : NpoiExcelExporterBase, IMeterUpgradesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public MeterUpgradesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetMeterUpgradeForViewDto> meterUpgrades)
        {
            return CreateExcelPackage(
                "MeterUpgrades.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("MeterUpgrades"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DisplayOrder")
                        );

                    AddObjects(
                        sheet, 2, meterUpgrades,
                        _ => _.MeterUpgrade.Name,
                        _ => _.MeterUpgrade.DisplayOrder
                        );

					
					
                });
        }
    }
}
