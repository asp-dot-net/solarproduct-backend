﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.JobTrackers.Dtos;

namespace TheSolarProduct.Jobs.Exporting
{
    public class JobRefundsExcelExporter : NpoiExcelExporterBase, IJobRefundsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobRefundsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetViewRefundTrackerDto> jobRefunds, string FileName)
        {
            return CreateExcelPackage(
                FileName,
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("JobRefunds"));

                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        L("CompanyName"),
                        "Refund Request",
                        "Verify By",
                        "Not Verify Days",
                        "Not Paid Days",
                        L("Amount"),
                        "Paid Amount",
                        L("BSBNo"),
                        L("AccountNo"),
                        L("LeadOwner"),
                        "Payment Type",
                        (L("RefundReason")),
                        L("RefundNotes"),
                        (L("PaymentOption")),
                        L("PaidDate"),
                        L("PreviousProjectStatus"),
                        L("CurrentProjectStatus"),
                        L("SmsSend"),
                        L("EmailSend"),
                        L("CreatedBy"),
                        L("ReminderDate"),
                        L("Reminder"),
                        L("Comment")
                        );

                    AddObjects(
                        sheet, 2, jobRefunds,
                        _ => _.JobNumber,
                        _ => _.CompanyName,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.VerifyBy,
                        _ => _.NotVerifiedDays,
                        _ => _.NotPaidDays,
                        _ => _.Amount,
                        _ => _.PaidAmount,
                        _ => _.BSBNo,
                        _ => _.AccountNo,
                        _ => _.CurrentLeadOwaner,
                        _ => _.PaymentType,
                        _ => _.RefundReasonName,
                        _ => _.RefundNotes,
                        _ => _.PaymentOptionName,
                        _ => _timeZoneConverter.Convert(_.PaidDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.PreviousProjectStatus,
                        _ => _.ProjectStatus,
                        _ => (_.JobRefundSmsSend != true ? "Yes" :"No"),
                        _ => (_.JobRefundEmailSend != true ? "Yes" : "No"),
                        _ => _.RequestedBy,
                        _ => _.ActivityReminderTime,
                        _ => _.ActivityDescription,
                        _ => _.ActivityComment

                        );

                    //for (var i = 1; i <= jobRefunds.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[8], "yyyy-mm-dd");
                    //}
                    //sheet.AutoSizeColumn(6);
                    //for (var i = 1; i <= jobRefunds.Count; i++)
                    //{
                    //    SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                    //}
                    //sheet.AutoSizeColumn(1);
                    //for (var i = 1; i <= jobRefunds.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd-MM-yyyy");
                    //}
                    //sheet.AutoSizeColumn(7);
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= jobRefunds.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[2], "dd/mm/yyyy");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[4], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[5], "00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "00.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "00.00");
                        SetCellDataFormat(sheet.GetRow(i).Cells[15], "dd/mm/yyyy");
                    }
                });
        }

        //public FileDto ExportCsvToFile(List<GetViewRefundTrackerDto> jobRefunds)
        //{
        //    return CreateExcelPackage(
        //        "JobRefunds.csv",
        //        excelPackage =>
        //        {

        //            var sheet = excelPackage.CreateSheet(L("JobRefunds"));

        //            AddHeader(
        //                sheet,
        //                L("ProjectNo"),
        //                L("Amount"),
        //                L("BSBNo"),
        //                L("AccountNo"),
        //                L("LeadOwner"),
        //                (L("RefundReason")) + L("Name"),
        //                (L("PaymentOption")) + L("Name"),
        //                L("PaidDate"),
        //                L("ProjectStatus"),
        //                L("CreatedBy")
        //                );

        //            AddObjects(
        //                sheet, 2, jobRefunds,
        //                _ => _.JobNumber,
        //                _ => _.Amount,
        //                _ => _.BSBNo,
        //                _ => _.AccountNo,
        //                _ => _.CurrentLeadOwaner,
        //                _ => _.RefundReasonName,
        //                _ => _.PaymentOptionName,
        //                _ => _timeZoneConverter.Convert(_.PaidDate, _abpSession.TenantId, _abpSession.GetUserId()),
        //                _ => _.ProjectStatus,
        //                _ => _.RequestedBy
        //                );

        //            for (var i = 1; i <= jobRefunds.Count; i++)
        //            {
        //                SetCellDataFormat(sheet.GetRow(i).Cells[8], "yyyy-mm-dd");
        //            }
        //            sheet.AutoSizeColumn(6);
        //        });
        //}
    }
}
