﻿using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Jobs.Exporting
{
    public interface IJobsExcelExporter
    {
        FileDto ExportToFile(List<GetJobForViewDto> jobs);

        FileDto ExportInstallerNewToFile(List<InstallerNewDto> jobs);

        FileDto ExportReadyToPayInstallerInvoiceToFile(List<InstallerPaidExcelDto> invoice);

        FileDto ExportPendingInstallerInvoiceToFile(List<GetInstallerInvoiceForExportDto> invoice);

        FileDto ExportInvoicePerToFile(List<InstallerNewDto> jobs);

    }


}