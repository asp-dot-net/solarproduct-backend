﻿using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Jobs.Exporting
{
    public interface IProductItemsExcelExporter
    {
        FileDto ExportToFile(List<GetProductItemForViewDto> productItems);
    }
}