﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class FreebieTransportsExcelExporter : NpoiExcelExporterBase, IFreebieTransportsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public FreebieTransportsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetFreebieTransportForViewDto> freebieTransports)
        {
            return CreateExcelPackage(
                "FreebieTransports.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("FreebieTransports"));

                    AddHeader(
                        sheet,
                        L("Name"),
                         L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, freebieTransports,
                        _ => _.FreebieTransport.Name,
                         _ => _.FreebieTransport.IsActive ? L("Yes") : L("No")
                        );

                });
        }
    }
}