﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.Promotions;

namespace TheSolarProduct.Jobs.Exporting
{
    public class JobsExcelExporter : NpoiExcelExporterBase, IJobsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobForViewDto> jobs)
        {
            return CreateExcelPackage(
                "Jobs.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("Jobs"));

                    AddHeader(
                        sheet,
                        L("RegPlanNo"),
                        L("LotNumber"),
                        L("Suburb"),
                        L("State"),
                        L("UnitNo"),
                        L("UnitType"),
                        (L("JobType")) + L("Name"),
                        (L("JobStatus")) + L("Name"),
                        (L("ElecDistributor")) + L("Name"),
                        (L("Lead")) + L("CompanyName"),
                        (L("ElecRetailer")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, jobs,
                        _ => _.Job.RegPlanNo,
                        _ => _.Job.LotNumber,
                        _ => _.Job.Suburb,
                        _ => _.Job.State,
                        _ => _.Job.UnitNo,
                        _ => _.Job.UnitType,
                        _ => _.JobTypeName,
                        _ => _.JobStatusName,
                        _ => _.ElecDistributorName,
                        _ => _.LeadCompanyName,
                        _ => _.ElecRetailerName
                        );

					
					
                });
        }

        public FileDto ExportInstallerNewToFile(List<InstallerNewDto> jobs)
        {
            return CreateExcelPackage(
                "InstallerNewInvoice.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("InstallerNewInvoice");

                    AddHeader(
                        sheet,
                        L("JobNumber"),
                        L("Customer"),
                        L("Address"),
                        L("PostCode"),
                        L("InstallDate"),
                        L("InstallComplete"),
                        L("InvoiceNo"),
                        "Installer Name/ Company Name",
                        "Invoice Type",
                        "Invoice Amount",
                        "Invoice Date",
                        "Issued Date",
                        "Invoice Notes"
                        );

                    AddObjects(
                        sheet, 2, jobs,
                        _ => _.ProjectName,
                        _ => _.Customer,
                        _ => _.Address,
                        _ => _.PCode,
                        _ => _.InstallDate,
                        _ => _.InstallComplate,
                        _ => _.InvoiceNO,
                        _ => _.installername,
                        _ => _.invoiceType,
                        _ => _.Ammount,
                        _ => _.InvDate,
                        _ => _.Issued,
                        _ => _.invoiceNotes
                    );

                    for (var i = 1; i <= jobs.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[4], "dd/MM/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[5], "dd/MM/yyyy");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "00.00");
                        SetCellDataFormat(sheet.GetRow(i).Cells[10], "dd/MM/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[11], "dd/MM/yyyy");
                    }
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                });
        }
          
        public FileDto ExportReadyToPayInstallerInvoiceToFile(List<InstallerPaidExcelDto> invoice)
        {
            return CreateExcelPackage(
                "ReadyToPayInstallerInvoice.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("ReadyToPayInstallerInvoice");

                    AddHeader(
                        sheet,
                        "JobNumber",
                        "Customer",
                        "Address",
                        "State",
                        "Post Code",
                        "Install Date",
                        "Invoice No",
                        "Invoice Type",
                        "Invoice Date",
                        "Due Date",
                        "Amount",
                        "Approved Date",
                        "Approved Amount",
                        "Approved Notes",
                        "Bank Ref No",
                        "Area",
                        "Installer Name",
                        "SysPrice",
                        "SysCapacity"
                        );

                    AddObjects(
                        sheet, 2, invoice,
                        _ => _.JobNumber,
                        _ => _.Customer,
                        _ => _.Address,
                        _ => _.State,
                        _ => _.PCode,
                        _ => _.InstallDate,
                        _ => _.InvoiceNo,
                         _ => _.InvoiceType,
                        _ => _.InvDate,
                        _ => _.DueDate,
                        _ => _.Amount,
                        _ => _.ApprovedDate,
                        _ => _.ApprovedAmount,
                        _ => _.ApprovedNotes,
                        _ => _.BankRefNo,
                        _ => _.Area,
                        _ => _.InstallerName,
                        _ => _.SysPrice,
                        _ => _.SysCapacity
                        );

                    for (var i = 1; i <= invoice.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[5], "dd/MM/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[8], "dd/MM/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[9], "dd/MM/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[11], "dd/MM/yyyy");

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[17], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[18], "0.00");
                    }
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public FileDto ExportPendingInstallerInvoiceToFile(List<GetInstallerInvoiceForExportDto> invoice)
        {
            return CreateExcelPackage(
                "PandingInstallerInvoice.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("PandingInstallerInvoice");

                    AddHeader(
                        sheet,
                        "JobNumber",
                        "State",
                        "Installer Name/ Company Name",
                        "Invoice Type",
                        "Invoice No",
                        "Invoice Amount",
                        "Invoice Date",
                        "Issued Date",
                        "Install Complete Date",
                        "SysPrice",
                        "OWing Amount",
                        "Remark If OWing",
                        "Payment Type",
                        "Grid Connection",
                        "Notes or Reason for Panding"
                        );

                    AddObjects(
                        sheet, 2, invoice,
                        _ => _.JobNumber,
                        _ => _.State,
                        _ => _.InstallerName + " / " + _.InstallerCompanyName,
                        _ => _.Installation_Maintenance_Inspection,
                        _ => _.InvoiceNo,
                        _ => _.Amount,
                        _ => _.InvDate,
                        _ => _.InvoiceIssuedDate,
                        _ => _.InstalledCompleteDate,
                        _ => _.TotalCost,
                        _ => _.OwningAmt,
                        _ => _.RemarkIfOwing,
                        _ => _.FinanceName,
                        _ => _.GridConnection == true ? "yes" : "No",
                        _ => _.NotesOrReasonforPending
                        );

                    for (var i = 1; i <= invoice.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "00.00");
                        SetCellDataFormat(sheet.GetRow(i).Cells[6], "dd/mm/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[7], "dd/mm/yyyy");
                        SetCellDataFormat(sheet.GetRow(i).Cells[8], "dd/mm/yyyy");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "00.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "00.00");
                    }
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                });
        }

        public FileDto ExportInvoicePerToFile(List<InstallerNewDto> jobs)
        {
            return CreateExcelPackage(
                "InstallerNewInvoicePerc.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet("InstallerNewInvoicePerc");

                    AddHeader(
                        sheet,
                        "Payment Date",
                        "State",
                        "Area",
                        L("JobNumber"),
                        "Installer Company Name",
                        "Installer Name",
                        "System Price",
                        "Percentage",
                        "KW",
                        "Invoice Amount",
                        "AVG Invoice Price per KW",
                        "Invoice Type",
                        "Invoice Notes"
                        );

                    AddObjects(
                        sheet, 2, jobs,
                        _ => _.PaymentDate,
                        _ => _.State,
                        _ => _.Area,
                        _ => _.ProjectName,
                        _ => _.CompanyName,
                        _ => _.installername,
                        _ => _.SysAmount,
                        _ => _.Per,
                        _ => _.TotalKW,
                        _ => _.Ammount,
                        _ => _.AVGInvAmtPerKw,
                        _ => _.invoiceType,
                        _ => _.invoiceNotes
                    );

                    for (var i = 1; i <= jobs.Count; i++)
                    {
                        SetCellDataFormat(sheet.GetRow(i).Cells[0], "dd/MM/yyyy");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "00.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "00.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "00.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "00.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "00.00");
                    }
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                });
        }


    }
}
