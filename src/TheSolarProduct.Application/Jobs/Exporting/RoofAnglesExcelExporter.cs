﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class RoofAnglesExcelExporter : NpoiExcelExporterBase, IRoofAnglesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RoofAnglesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRoofAngleForViewDto> roofAngles)
        {
            return CreateExcelPackage(
                "RoofAngles.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("RoofAngles"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DisplayOrder")
                        );

                    AddObjects(
                        sheet, 2, roofAngles,
                        _ => _.RoofAngle.Name,
                        _ => _.RoofAngle.DisplayOrder
                        );

					
					
                });
        }
    }
}
