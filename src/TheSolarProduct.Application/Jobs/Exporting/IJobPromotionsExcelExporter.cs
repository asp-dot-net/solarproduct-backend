﻿using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Jobs.Exporting
{
    public interface IJobPromotionsExcelExporter
    {
        FileDto ExportToFile(List<GetJobPromotionForViewDto> jobs);
        FileDto ExportCsvToFile(List<GetJobPromotionForViewDto> jobs);
    }
}