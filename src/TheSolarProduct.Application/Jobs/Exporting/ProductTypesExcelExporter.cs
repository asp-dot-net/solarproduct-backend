﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class ProductTypesExcelExporter : NpoiExcelExporterBase, IProductTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProductTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetProductTypeForViewDto> productTypes)
        {
            return CreateExcelPackage(
                "ProductTypes.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("ProductTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DisplayOrder")
                        );

                    AddObjects(
                        sheet, 2, productTypes,
                        _ => _.ProductType.Name,
                        _ => _.ProductType.DisplayOrder
                        );

					
					
                });
        }
    }
}
