﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class MeterPhasesExcelExporter : NpoiExcelExporterBase, IMeterPhasesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public MeterPhasesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetMeterPhaseForViewDto> meterPhases)
        {
            return CreateExcelPackage(
                "MeterPhases.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("MeterPhases"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DisplayOrder")
                        );

                    AddObjects(
                        sheet, 2, meterPhases,
                        _ => _.MeterPhase.Name,
                        _ => _.MeterPhase.DisplayOrder
                        );

					
					
                });
        }
    }
}
