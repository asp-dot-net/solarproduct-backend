﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class PaymentOptionsExcelExporter : NpoiExcelExporterBase, IPaymentOptionsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentOptionsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPaymentOptionForViewDto> paymentOptions)
        {
            return CreateExcelPackage(
                "PaymentOptions.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("PaymentOptions"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DisplayOrder"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, paymentOptions,
                        _ => _.PaymentOption.Name,
                        _ => _.PaymentOption.DisplayOrder,
                        _=> _.PaymentOption.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
