﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class ElecRetailersExcelExporter : NpoiExcelExporterBase, IElecRetailersExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ElecRetailersExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetElecRetailerForViewDto> elecRetailers)
        {
            return CreateExcelPackage(
                "ElecRetailers.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("ElecRetailers"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("NSW"),
                        L("SA"),
                        L("QLD"),
                        L("VIC"),
                        L("WA"),
                        L("ACT"),
                        L("TAS"),
                        L("NT"),
                        L("ElectricityProviderId")
                        );

                    AddObjects(
                        sheet, 2, elecRetailers,
                        _ => _.ElecRetailer.Name,
                        _ => _.ElecRetailer.NSW,
                        _ => _.ElecRetailer.SA,
                        _ => _.ElecRetailer.QLD,
                        _ => _.ElecRetailer.VIC,
                        _ => _.ElecRetailer.WA,
                        _ => _.ElecRetailer.ACT,
                        _ => _.ElecRetailer.TAS,
                        _ => _.ElecRetailer.NT,
                        _ => _.ElecRetailer.ElectricityProviderId
                        );

					
					
                });
        }
    }
}
