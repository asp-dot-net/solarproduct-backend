﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs.Importing.Dto;

namespace TheSolarProduct.Jobs.Exporting
{
   public interface IStcExcelFileExporter
    {
        FileDto ExportToFile(List<ImportStcDto> leadListDtos);
    }
}
