﻿using System.Collections.Generic;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Jobs.Exporting
{
    public interface IRoofAnglesExcelExporter
    {
        FileDto ExportToFile(List<GetRoofAngleForViewDto> roofAngles);
    }
}