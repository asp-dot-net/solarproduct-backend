﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class RefundReasonsExcelExporter : NpoiExcelExporterBase, IRefundReasonsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RefundReasonsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRefundReasonForViewDto> refundReasons)
        {
            return CreateExcelPackage(
                "RefundReasons.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("RefundReasons"));

                    AddHeader(
                        sheet,
                        L("Name"),
                         L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, refundReasons,
                        _ => _.RefundReason.Name,
                         _ => _.RefundReason.IsActive ? L("Yes") : L("No")
                        );

                });
        }
    }
}