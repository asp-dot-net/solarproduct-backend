﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class VariationsExcelExporter : NpoiExcelExporterBase, IVariationsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public VariationsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetVariationForViewDto> variations)
        {
            return CreateExcelPackage(
                "Variations.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("Variations"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Action"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, variations,
                        _ => _.Variation.Name,
                        _ => _.Variation.Action,
                         _ => _.Variation.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
