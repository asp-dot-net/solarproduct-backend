﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class RoofTypesExcelExporter : NpoiExcelExporterBase, IRoofTypesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RoofTypesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRoofTypeForViewDto> roofTypes)
        {
            return CreateExcelPackage(
                "RoofTypes.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("RoofTypes"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DisplayOrder"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, roofTypes,
                        _ => _.RoofType.Name,
                        _ => _.RoofType.DisplayOrder,
                        _ => _.RoofType.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
