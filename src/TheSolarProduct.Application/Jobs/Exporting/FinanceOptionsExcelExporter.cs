﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class FinanceOptionsExcelExporter : NpoiExcelExporterBase, IFinanceOptionsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public FinanceOptionsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetFinanceOptionForViewDto> financeOptions)
        {
            return CreateExcelPackage(
                "FinanceOptions.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("FinanceOptions"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("isActive")
                        );

                    AddObjects(
                        sheet, 2, financeOptions,
                        _ => _.FinanceOption.Name,
                         _ => _.FinanceOption.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
