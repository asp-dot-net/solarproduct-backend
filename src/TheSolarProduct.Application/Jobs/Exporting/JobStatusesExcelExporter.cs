﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Jobs.Exporting
{
    public class JobStatusesExcelExporter : NpoiExcelExporterBase, IJobStatusesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobStatusesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobStatusForViewDto> jobStatuses)
        {
            return CreateExcelPackage(
                "JobStatuses.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("JobStatuses"));

                    AddHeader(
                        sheet,
                        L("Name"),
                          L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, jobStatuses,
                        _ => _.JobStatus.Name,
                         _ => _.JobStatus.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
