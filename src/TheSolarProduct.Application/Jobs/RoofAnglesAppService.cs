﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_RoofAngles, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class RoofAnglesAppService : TheSolarProductAppServiceBase, IRoofAnglesAppService
	{
		private readonly IRepository<RoofAngle> _roofAngleRepository;
		private readonly IRoofAnglesExcelExporter _roofAnglesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public RoofAnglesAppService(IRepository<RoofAngle> roofAngleRepository, IRoofAnglesExcelExporter roofAnglesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
		{
			_roofAngleRepository = roofAngleRepository;
			_roofAnglesExcelExporter = roofAnglesExcelExporter;
			_dbcontextprovider = dbcontextprovider;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;

		}

		public async Task<PagedResultDto<GetRoofAngleForViewDto>> GetAll(GetAllRoofAnglesInput input)
		{

			var filteredRoofAngles = _roofAngleRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var pagedAndFilteredRoofAngles = filteredRoofAngles
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var roofAngles = from o in pagedAndFilteredRoofAngles
							 select new GetRoofAngleForViewDto()
							 {
								 RoofAngle = new RoofAngleDto
								 {
									 Name = o.Name,
									 DisplayOrder = o.DisplayOrder,
									 Id = o.Id,
									 IsActive=o.IsActive,
								 }
							 };

			var totalCount = await filteredRoofAngles.CountAsync();

			return new PagedResultDto<GetRoofAngleForViewDto>(
				totalCount,
				await roofAngles.ToListAsync()
			);
		}

		public async Task<GetRoofAngleForViewDto> GetRoofAngleForView(int id)
		{
			var roofAngle = await _roofAngleRepository.GetAsync(id);

			var output = new GetRoofAngleForViewDto { RoofAngle = ObjectMapper.Map<RoofAngleDto>(roofAngle) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_RoofAngles_Edit)]
		public async Task<GetRoofAngleForEditOutput> GetRoofAngleForEdit(EntityDto input)
		{
			var roofAngle = await _roofAngleRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetRoofAngleForEditOutput { RoofAngle = ObjectMapper.Map<CreateOrEditRoofAngleDto>(roofAngle) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditRoofAngleDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_RoofAngles_Create)]
		protected virtual async Task Create(CreateOrEditRoofAngleDto input)
		{
			var roofAngle = ObjectMapper.Map<RoofAngle>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	roofAngle.TenantId = (int)AbpSession.TenantId;
            //}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 18;
            dataVaultLog.ActionNote = "Roof Angle Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _roofAngleRepository.InsertAsync(roofAngle);
		}

		[AbpAuthorize(AppPermissions.Pages_RoofAngles_Edit)]
		protected virtual async Task Update(CreateOrEditRoofAngleDto input)
		{
			var roofAngle = await _roofAngleRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 18;
            dataVaultLog.ActionNote = "Roof Angle Updated : " + roofAngle.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != roofAngle.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = roofAngle.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsActive != roofAngle.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = roofAngle.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, roofAngle);
		}

		[AbpAuthorize(AppPermissions.Pages_RoofAngles_Delete)]
		public async Task Delete(EntityDto input)
		{
            var Name = _roofAngleRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 18;
            dataVaultLog.ActionNote = "Roof Angle Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _roofAngleRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetRoofAnglesToExcel(GetAllRoofAnglesForExcelInput input)
		{

			var filteredRoofAngles = _roofAngleRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var query = (from o in filteredRoofAngles
						 select new GetRoofAngleForViewDto()
						 {
							 RoofAngle = new RoofAngleDto
							 {
								 Name = o.Name,
								 DisplayOrder = o.DisplayOrder,
								 Id = o.Id,
								 IsActive=o.IsActive
							 }
						 });


			var roofAngleListDtos = await query.ToListAsync();

			return _roofAnglesExcelExporter.ExportToFile(roofAngleListDtos);
		}


	}
}