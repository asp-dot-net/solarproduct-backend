﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_MeterUpgrades, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class MeterUpgradesAppService : TheSolarProductAppServiceBase, IMeterUpgradesAppService
	{
		private readonly IRepository<MeterUpgrade> _meterUpgradeRepository;
		private readonly IMeterUpgradesExcelExporter _meterUpgradesExcelExporter;


		public MeterUpgradesAppService(IRepository<MeterUpgrade> meterUpgradeRepository, IMeterUpgradesExcelExporter meterUpgradesExcelExporter)
		{
			_meterUpgradeRepository = meterUpgradeRepository;
			_meterUpgradesExcelExporter = meterUpgradesExcelExporter;

		}

		public async Task<PagedResultDto<GetMeterUpgradeForViewDto>> GetAll(GetAllMeterUpgradesInput input)
		{

			var filteredMeterUpgrades = _meterUpgradeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var pagedAndFilteredMeterUpgrades = filteredMeterUpgrades
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var meterUpgrades = from o in pagedAndFilteredMeterUpgrades
								select new GetMeterUpgradeForViewDto()
								{
									MeterUpgrade = new MeterUpgradeDto
									{
										Name = o.Name,
										DisplayOrder = o.DisplayOrder,
										Id = o.Id
									}
								};

			var totalCount = await filteredMeterUpgrades.CountAsync();

			return new PagedResultDto<GetMeterUpgradeForViewDto>(
				totalCount,
				await meterUpgrades.ToListAsync()
			);
		}

		public async Task<GetMeterUpgradeForViewDto> GetMeterUpgradeForView(int id)
		{
			var meterUpgrade = await _meterUpgradeRepository.GetAsync(id);

			var output = new GetMeterUpgradeForViewDto { MeterUpgrade = ObjectMapper.Map<MeterUpgradeDto>(meterUpgrade) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MeterUpgrades_Edit)]
		public async Task<GetMeterUpgradeForEditOutput> GetMeterUpgradeForEdit(EntityDto input)
		{
			var meterUpgrade = await _meterUpgradeRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMeterUpgradeForEditOutput { MeterUpgrade = ObjectMapper.Map<CreateOrEditMeterUpgradeDto>(meterUpgrade) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMeterUpgradeDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MeterUpgrades_Create)]
		protected virtual async Task Create(CreateOrEditMeterUpgradeDto input)
		{
			var meterUpgrade = ObjectMapper.Map<MeterUpgrade>(input);

			//if (AbpSession.TenantId != null)
			//{
			//	meterUpgrade.TenantId = (int)AbpSession.TenantId;
			//}

			await _meterUpgradeRepository.InsertAsync(meterUpgrade);
		}

		[AbpAuthorize(AppPermissions.Pages_MeterUpgrades_Edit)]
		protected virtual async Task Update(CreateOrEditMeterUpgradeDto input)
		{
			var meterUpgrade = await _meterUpgradeRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, meterUpgrade);
		}

		[AbpAuthorize(AppPermissions.Pages_MeterUpgrades_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _meterUpgradeRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetMeterUpgradesToExcel(GetAllMeterUpgradesForExcelInput input)
		{

			var filteredMeterUpgrades = _meterUpgradeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var query = (from o in filteredMeterUpgrades
						 select new GetMeterUpgradeForViewDto()
						 {
							 MeterUpgrade = new MeterUpgradeDto
							 {
								 Name = o.Name,
								 DisplayOrder = o.DisplayOrder,
								 Id = o.Id
							 }
						 });


			var meterUpgradeListDtos = await query.ToListAsync();

			return _meterUpgradesExcelExporter.ExportToFile(meterUpgradeListDtos);
		}


	}
}