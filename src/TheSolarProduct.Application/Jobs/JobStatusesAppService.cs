﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.CancelReasons;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_JobStatuses, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class JobStatusesAppService : TheSolarProductAppServiceBase, IJobStatusesAppService
	{
		private readonly IRepository<JobStatus> _jobStatusRepository;
		private readonly IJobStatusesExcelExporter _jobStatusesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public JobStatusesAppService(IRepository<JobStatus> jobStatusRepository, IJobStatusesExcelExporter jobStatusesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
		{
			_jobStatusRepository = jobStatusRepository;
			_jobStatusesExcelExporter = jobStatusesExcelExporter;
			_dbcontextprovider = dbcontextprovider;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;

		}

		public async Task<PagedResultDto<GetJobStatusForViewDto>> GetAll(GetAllJobStatusesInput input)
		{

			var filteredJobStatuses = _jobStatusRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

			var pagedAndFilteredJobStatuses = filteredJobStatuses
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var jobStatuses = from o in pagedAndFilteredJobStatuses
							  select new GetJobStatusForViewDto()
							  {
								  JobStatus = new JobStatusDto
								  {
									  Name = o.Name,
									  Id = o.Id,
									  IsActive = o.IsActive,
								  }
							  };

			var totalCount = await filteredJobStatuses.CountAsync();

			return new PagedResultDto<GetJobStatusForViewDto>(
				totalCount,
				await jobStatuses.ToListAsync()
			);
		}

		public async Task<GetJobStatusForViewDto> GetJobStatusForView(int id)
		{
			var jobStatus = await _jobStatusRepository.GetAsync(id);

			var output = new GetJobStatusForViewDto { JobStatus = ObjectMapper.Map<JobStatusDto>(jobStatus) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_JobStatuses_Edit)]
		public async Task<GetJobStatusForEditOutput> GetJobStatusForEdit(EntityDto input)
		{
			var jobStatus = await _jobStatusRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetJobStatusForEditOutput { JobStatus = ObjectMapper.Map<CreateOrEditJobStatusDto>(jobStatus) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditJobStatusDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_JobStatuses_Create)]
		protected virtual async Task Create(CreateOrEditJobStatusDto input)
		{
			var jobStatus = ObjectMapper.Map<JobStatus>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	jobStatus.TenantId = (int)AbpSession.TenantId;
            //}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 13;
            dataVaultLog.ActionNote = "Job Status Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _jobStatusRepository.InsertAsync(jobStatus);
		}

		[AbpAuthorize(AppPermissions.Pages_JobStatuses_Edit)]
		protected virtual async Task Update(CreateOrEditJobStatusDto input)
		{
			var jobStatus = await _jobStatusRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 13;
            dataVaultLog.ActionNote = "Job Status Updated : " + jobStatus.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != jobStatus.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = jobStatus.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != jobStatus.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = jobStatus.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, jobStatus);
		}

		[AbpAuthorize(AppPermissions.Pages_JobStatuses_Delete)]
		public async Task Delete(EntityDto input)
		{
            var Name = _jobStatusRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 13;
            dataVaultLog.ActionNote = "Job Status Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _jobStatusRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetJobStatusesToExcel(GetAllJobStatusesForExcelInput input)
		{

			var filteredJobStatuses = _jobStatusRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

			var query = (from o in filteredJobStatuses
						 select new GetJobStatusForViewDto()
						 {
							 JobStatus = new JobStatusDto
							 {
								 Name = o.Name,
								 Id = o.Id,
								 IsActive = o.IsActive,
							 }
						 });


			var jobStatusListDtos = await query.ToListAsync();

			return _jobStatusesExcelExporter.ExportToFile(jobStatusListDtos);
		}


	}
}