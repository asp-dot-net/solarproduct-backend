﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_HouseTypes, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class HouseTypesAppService : TheSolarProductAppServiceBase, IHouseTypesAppService
    {
		 private readonly IRepository<HouseType> _houseTypeRepository;
		 private readonly IHouseTypesExcelExporter _houseTypesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public HouseTypesAppService(IRepository<HouseType> houseTypeRepository, IHouseTypesExcelExporter houseTypesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_houseTypeRepository = houseTypeRepository;
			_houseTypesExcelExporter = houseTypesExcelExporter;
			_dbcontextprovider = dbcontextprovider;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
			
		  }

		 public async Task<PagedResultDto<GetHouseTypeForViewDto>> GetAll(GetAllHouseTypesInput input)
         {
			
			var filteredHouseTypes = _houseTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(input.MinDisplayOrderFilter != null, e => e.DisplayOrder >= input.MinDisplayOrderFilter)
						.WhereIf(input.MaxDisplayOrderFilter != null, e => e.DisplayOrder <= input.MaxDisplayOrderFilter);

			var pagedAndFilteredHouseTypes = filteredHouseTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var houseTypes = from o in pagedAndFilteredHouseTypes
                         select new GetHouseTypeForViewDto() {
							HouseType = new HouseTypeDto
							{
                                Name = o.Name,
                                DisplayOrder = o.DisplayOrder,
								Id = o.Id,
                                IsActive=o.IsActive,
                            }
						};

            var totalCount = await filteredHouseTypes.CountAsync();

            return new PagedResultDto<GetHouseTypeForViewDto>(
                totalCount,
                await houseTypes.ToListAsync()
            );
         }
		 
		 public async Task<GetHouseTypeForViewDto> GetHouseTypeForView(int id)
         {
            var houseType = await _houseTypeRepository.GetAsync(id);

            var output = new GetHouseTypeForViewDto { HouseType = ObjectMapper.Map<HouseTypeDto>(houseType) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_HouseTypes_Edit)]
		 public async Task<GetHouseTypeForEditOutput> GetHouseTypeForEdit(EntityDto input)
         {
            var houseType = await _houseTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetHouseTypeForEditOutput {HouseType = ObjectMapper.Map<CreateOrEditHouseTypeDto>(houseType)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditHouseTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_HouseTypes_Create)]
		 protected virtual async Task Create(CreateOrEditHouseTypeDto input)
         {
            var houseType = ObjectMapper.Map<HouseType>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	houseType.TenantId = (int)AbpSession.TenantId;
            //}
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 16;
            dataVaultLog.ActionNote = "House Type Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _houseTypeRepository.InsertAsync(houseType);
         }

		 [AbpAuthorize(AppPermissions.Pages_HouseTypes_Edit)]
		 protected virtual async Task Update(CreateOrEditHouseTypeDto input)
         {
            var houseType = await _houseTypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 16;
            dataVaultLog.ActionNote = "House Type Updated : " + houseType.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != houseType.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = houseType.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != houseType.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = houseType.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, houseType);
         }

		 [AbpAuthorize(AppPermissions.Pages_HouseTypes_Delete)]
         public async Task Delete(EntityDto input)
         {

            var Name = _houseTypeRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 16;
            dataVaultLog.ActionNote = "House Type Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _houseTypeRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetHouseTypesToExcel(GetAllHouseTypesForExcelInput input)
         {
			
			var filteredHouseTypes = _houseTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(input.MinDisplayOrderFilter != null, e => e.DisplayOrder >= input.MinDisplayOrderFilter)
						.WhereIf(input.MaxDisplayOrderFilter != null, e => e.DisplayOrder <= input.MaxDisplayOrderFilter);

			var query = (from o in filteredHouseTypes
                         select new GetHouseTypeForViewDto() { 
							HouseType = new HouseTypeDto
							{
                                Name = o.Name,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var houseTypeListDtos = await query.ToListAsync();

            return _houseTypesExcelExporter.ExportToFile(houseTypeListDtos);
         }


    }
}