﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_Variations, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
    public class VariationsAppService : TheSolarProductAppServiceBase, IVariationsAppService
    {
		 private readonly IRepository<Variation> _variationRepository;
		 private readonly IVariationsExcelExporter _variationsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public VariationsAppService(IRepository<Variation> variationRepository, IVariationsExcelExporter variationsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_variationRepository = variationRepository;
			_variationsExcelExporter = variationsExcelExporter;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
		  }

		 public async Task<PagedResultDto<GetVariationForViewDto>> GetAll(GetAllVariationsInput input)
         {
			
			var filteredVariations = _variationRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Action.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ActionFilter),  e => e.Action == input.ActionFilter);

			var pagedAndFilteredVariations = filteredVariations
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var variations = from o in pagedAndFilteredVariations
                         select new GetVariationForViewDto() {
							Variation = new VariationDto
							{
                                Name = o.Name,
                                Action = o.Action,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredVariations.CountAsync();

            return new PagedResultDto<GetVariationForViewDto>(
                totalCount,
                await variations.ToListAsync()
            );
         }
		 
		 public async Task<GetVariationForViewDto> GetVariationForView(int id)
         {
            var variation = await _variationRepository.GetAsync(id);

            var output = new GetVariationForViewDto { Variation = ObjectMapper.Map<VariationDto>(variation) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_Variations_Edit)]
		 public async Task<GetVariationForEditOutput> GetVariationForEdit(EntityDto input)
         {
            var variation = await _variationRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetVariationForEditOutput {Variation = ObjectMapper.Map<CreateOrEditVariationDto>(variation)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditVariationDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_Variations_Create)]
		 protected virtual async Task Create(CreateOrEditVariationDto input)
         {
            var variation = ObjectMapper.Map<Variation>(input);

			
			if (AbpSession.TenantId != null)
			{
				variation.TenantId = (int) AbpSession.TenantId;
			}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 23;
            dataVaultLog.ActionNote = "Price Variations Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _variationRepository.InsertAsync(variation);
         }

		 [AbpAuthorize(AppPermissions.Pages_Variations_Edit)]
		 protected virtual async Task Update(CreateOrEditVariationDto input)
         {
            var variation = await _variationRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 23;
            dataVaultLog.ActionNote = "Price Variations Updated : " + variation.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != variation.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = variation.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != variation.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = variation.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Action != variation.Action)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Action";
                history.PrevValue = variation.Action;
                history.CurValue = input.Action;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, variation);
         }

		 [AbpAuthorize(AppPermissions.Pages_Variations_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _variationRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 23;
            dataVaultLog.ActionNote = "Price Variations Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _variationRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetVariationsToExcel(GetAllVariationsForExcelInput input)
         {
			
			var filteredVariations = _variationRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter) || e.Action.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.ActionFilter),  e => e.Action == input.ActionFilter);

			var query = (from o in filteredVariations
                         select new GetVariationForViewDto() { 
							Variation = new VariationDto
							{
                                Name = o.Name,
                                Action = o.Action,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var variationListDtos = await query.ToListAsync();

            return _variationsExcelExporter.ExportToFile(variationListDtos);
         }


    }
}