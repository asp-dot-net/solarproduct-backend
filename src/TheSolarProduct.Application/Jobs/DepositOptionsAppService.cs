﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_DepositOptions, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class DepositOptionsAppService : TheSolarProductAppServiceBase, IDepositOptionsAppService
    {
		 private readonly IRepository<DepositOption> _depositOptionRepository;
		 private readonly IDepositOptionsExcelExporter _depositOptionsExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public DepositOptionsAppService(IRepository<DepositOption> depositOptionRepository, IDepositOptionsExcelExporter depositOptionsExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_depositOptionRepository = depositOptionRepository;
			_depositOptionsExcelExporter = depositOptionsExcelExporter;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
			_dbcontextprovider = dbcontextprovider;
		  }

		 public async Task<PagedResultDto<GetDepositOptionForViewDto>> GetAll(GetAllDepositOptionsInput input)
         {
			
			var filteredDepositOptions = _depositOptionRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var pagedAndFilteredDepositOptions = filteredDepositOptions
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var depositOptions = from o in pagedAndFilteredDepositOptions
                         select new GetDepositOptionForViewDto() {
							DepositOption = new DepositOptionDto
							{
                                Name = o.Name,
                                DisplayOrder = o.DisplayOrder,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredDepositOptions.CountAsync();

            return new PagedResultDto<GetDepositOptionForViewDto>(
                totalCount,
                await depositOptions.ToListAsync()
            );
         }
		 
		 public async Task<GetDepositOptionForViewDto> GetDepositOptionForView(int id)
         {
            var depositOption = await _depositOptionRepository.GetAsync(id);

            var output = new GetDepositOptionForViewDto { DepositOption = ObjectMapper.Map<DepositOptionDto>(depositOption) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_DepositOptions_Edit)]
		 public async Task<GetDepositOptionForEditOutput> GetDepositOptionForEdit(EntityDto input)
         {
            var depositOption = await _depositOptionRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetDepositOptionForEditOutput {DepositOption = ObjectMapper.Map<CreateOrEditDepositOptionDto>(depositOption)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditDepositOptionDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_DepositOptions_Create)]
		 protected virtual async Task Create(CreateOrEditDepositOptionDto input)
         {
            var depositOption = ObjectMapper.Map<DepositOption>(input);

			
			if (AbpSession.TenantId != null)
			{
				depositOption.TenantId = (int) AbpSession.TenantId;
			}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 20;
            dataVaultLog.ActionNote = "Payment Option Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _depositOptionRepository.InsertAsync(depositOption);
         }

		 [AbpAuthorize(AppPermissions.Pages_DepositOptions_Edit)]
		 protected virtual async Task Update(CreateOrEditDepositOptionDto input)
         {
            var depositOption = await _depositOptionRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 20;
            dataVaultLog.ActionNote = "Payment Option Updated : " + depositOption.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != depositOption.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = depositOption.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != depositOption.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = depositOption.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, depositOption);
         }

		 [AbpAuthorize(AppPermissions.Pages_DepositOptions_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _depositOptionRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 20;
            dataVaultLog.ActionNote = "Payment Option Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _depositOptionRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetDepositOptionsToExcel(GetAllDepositOptionsForExcelInput input)
         {
			
			var filteredDepositOptions = _depositOptionRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var query = (from o in filteredDepositOptions
                         select new GetDepositOptionForViewDto() { 
							DepositOption = new DepositOptionDto
							{
                                Name = o.Name,
                                DisplayOrder = o.DisplayOrder,
                                Id = o.Id
							}
						 });


            var depositOptionListDtos = await query.ToListAsync();

            return _depositOptionsExcelExporter.ExportToFile(depositOptionListDtos);
         }


    }
}