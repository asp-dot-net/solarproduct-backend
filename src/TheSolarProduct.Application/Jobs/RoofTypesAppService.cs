﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_RoofTypes, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class RoofTypesAppService : TheSolarProductAppServiceBase, IRoofTypesAppService
    {
		 private readonly IRepository<RoofType> _roofTypeRepository;
		 private readonly IRoofTypesExcelExporter _roofTypesExcelExporter;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;


        public RoofTypesAppService(IRepository<RoofType> roofTypeRepository, IRoofTypesExcelExporter roofTypesExcelExporter, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider) 
		  {
			_roofTypeRepository = roofTypeRepository;
			_roofTypesExcelExporter = roofTypesExcelExporter;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
			_dbcontextprovider = dbcontextprovider;
		  }

		 public async Task<PagedResultDto<GetRoofTypeForViewDto>> GetAll(GetAllRoofTypesInput input)
         {
			
			var filteredRoofTypes = _roofTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var pagedAndFilteredRoofTypes = filteredRoofTypes
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var roofTypes = from o in pagedAndFilteredRoofTypes
                         select new GetRoofTypeForViewDto() {
							RoofType = new RoofTypeDto
							{
                                Name = o.Name,
                                DisplayOrder = o.DisplayOrder,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredRoofTypes.CountAsync();

            return new PagedResultDto<GetRoofTypeForViewDto>(
                totalCount,
                await roofTypes.ToListAsync()
            );
         }
		 
		 public async Task<GetRoofTypeForViewDto> GetRoofTypeForView(int id)
         {
            var roofType = await _roofTypeRepository.GetAsync(id);

            var output = new GetRoofTypeForViewDto { RoofType = ObjectMapper.Map<RoofTypeDto>(roofType) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_RoofTypes_Edit)]
		 public async Task<GetRoofTypeForEditOutput> GetRoofTypeForEdit(EntityDto input)
         {
            var roofType = await _roofTypeRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetRoofTypeForEditOutput {RoofType = ObjectMapper.Map<CreateOrEditRoofTypeDto>(roofType)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditRoofTypeDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_RoofTypes_Create)]
		 protected virtual async Task Create(CreateOrEditRoofTypeDto input)
         {
            var roofType = ObjectMapper.Map<RoofType>(input);

            //if (AbpSession.TenantId != null)
            //{
            //	roofType.TenantId = (int)AbpSession.TenantId;
            //}
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 17;
            dataVaultLog.ActionNote = "Roof Type Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _roofTypeRepository.InsertAsync(roofType);
         }

		 [AbpAuthorize(AppPermissions.Pages_RoofTypes_Edit)]
		 protected virtual async Task Update(CreateOrEditRoofTypeDto input)
         {
            var roofType = await _roofTypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 17;
            dataVaultLog.ActionNote = "Roof Type Updated : " + roofType.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != roofType.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = roofType.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != roofType.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = roofType.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, roofType);
         }

		 [AbpAuthorize(AppPermissions.Pages_RoofTypes_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _roofTypeRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 17;
            dataVaultLog.ActionNote = "Roof Type Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _roofTypeRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetRoofTypesToExcel(GetAllRoofTypesForExcelInput input)
         {
			
			var filteredRoofTypes = _roofTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var query = (from o in filteredRoofTypes
                         select new GetRoofTypeForViewDto() { 
							RoofType = new RoofTypeDto
							{
                                Name = o.Name,
                                DisplayOrder = o.DisplayOrder,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var roofTypeListDtos = await query.ToListAsync();

            return _roofTypesExcelExporter.ExportToFile(roofTypeListDtos);
         }


    }
}