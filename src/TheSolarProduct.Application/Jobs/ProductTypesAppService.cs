﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.Common;
using TheSolarProduct.Storage;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.ApplicationSettings;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_ProductTypes, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class ProductTypesAppService : TheSolarProductAppServiceBase, IProductTypesAppService
	{
		private readonly IRepository<ProductType> _productTypeRepository;
		private readonly IProductTypesExcelExporter _productTypesExcelExporter;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly TempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public ProductTypesAppService(IRepository<ProductType> productTypeRepository, IProductTypesExcelExporter productTypesExcelExporter, ICommonLookupAppService CommonDocumentSaveRepository, TempFileCacheManager tempFileCacheManager, IRepository<Tenant> tenantRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
		{
			_productTypeRepository = productTypeRepository;
			_productTypesExcelExporter = productTypesExcelExporter;
			_CommonDocumentSaveRepository = CommonDocumentSaveRepository;
			_tempFileCacheManager = tempFileCacheManager;
			_tenantRepository = tenantRepository;
			_dbcontextprovider = dbcontextprovider;
			_dataVaultActivityLogRepository = dataVaultActivityLogRepository;
		}

		public async Task<PagedResultDto<GetProductTypeForViewDto>> GetAll(GetAllProductTypesInput input)
		{

			var filteredProductTypes = _productTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var pagedAndFilteredProductTypes = filteredProductTypes
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var productTypes = from o in pagedAndFilteredProductTypes
							   select new GetProductTypeForViewDto()
							   {
								   ProductType = new ProductTypeDto
								   {
									   Name = o.Name,
									   Id = o.Id
								   }
							   };

			var totalCount = await filteredProductTypes.CountAsync();

			return new PagedResultDto<GetProductTypeForViewDto>(
				totalCount,
				await productTypes.ToListAsync()
			);
		}

		public async Task<GetProductTypeForViewDto> GetProductTypeForView(int id)
		{
			var productType = await _productTypeRepository.GetAsync(id);

			var output = new GetProductTypeForViewDto { ProductType = ObjectMapper.Map<ProductTypeDto>(productType) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_ProductTypes_Edit)]
		public async Task<GetProductTypeForEditOutput> GetProductTypeForEdit(EntityDto input)
		{
			var productType = await _productTypeRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetProductTypeForEditOutput { ProductType = ObjectMapper.Map<CreateOrEditProductTypeDto>(productType) };

            output.ProductType.ImgPath = ApplicationSettingConsts.ViewDocumentPath + productType.Image;

            return output;
		}

		public async Task CreateOrEdit(CreateOrEditProductTypeDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_ProductTypes_Create)]
		protected virtual async Task Create(CreateOrEditProductTypeDto input)
		{
            var FileName = DateTime.Now.Ticks + "_" + input.Name.Replace(" ","") + ".png";

            var InstallerByteArray = _tempFileCacheManager.GetFile(input.Img);

            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();

            var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(InstallerByteArray, FileName, "ProductType", AbpSession.TenantId);

            input.ImgPath = "\\Documents\\" + TenantName + "\\" + "WholeSale" + "\\ProductType" + "\\" + FileName;

            var productType = ObjectMapper.Map<ProductType>(input);
			productType.Image = input.ImgPath;

            //if (AbpSession.TenantId != null)
            //{
            //	productType.TenantId = (int) AbpSession.TenantId;
            //}

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 14;
            dataVaultLog.ActionNote = "Product Type Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _productTypeRepository.InsertAsync(productType);
		}

		[AbpAuthorize(AppPermissions.Pages_ProductTypes_Edit)]
		protected virtual async Task Update(CreateOrEditProductTypeDto input)
		{



            if (!string.IsNullOrEmpty(input.Img))
            {
                var FileName = DateTime.Now.Ticks + "_" + input.Name.Replace(" ","") + ".png";
                var InstallerByteArray = _tempFileCacheManager.GetFile(input.Img);

            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                var filepath = _CommonDocumentSaveRepository.SaveEcommerceDocument(InstallerByteArray, FileName, "ProductType", AbpSession.TenantId);
                input.ImgPath = "\\Documents\\" + TenantName + "\\" + "WholeSale" + "\\ProductType" + "\\" + FileName;

            }

            var productType = await _productTypeRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 14;
            dataVaultLog.ActionNote = "Product Type Updated : " + productType.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != productType.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = productType.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.ImgPath != productType.Image)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Image";
                history.PrevValue = productType.Image;
                history.CurValue = input.ImgPath;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            productType.Image = input.ImgPath;

            ObjectMapper.Map(input, productType);
		}

		[AbpAuthorize(AppPermissions.Pages_ProductTypes_Delete)]
		public async Task Delete(EntityDto input)
		{
            var Name = _productTypeRepository.Get(input.Id).Name;
            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 14;
            dataVaultLog.ActionNote = "Product Type Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _productTypeRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetProductTypesToExcel(GetAllProductTypesForExcelInput input)
		{

			var filteredProductTypes = _productTypeRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var query = (from o in filteredProductTypes
						 select new GetProductTypeForViewDto()
						 {
							 ProductType = new ProductTypeDto
							 {
								 Name = o.Name,
								 Id = o.Id
							 }
						 });


			var productTypeListDtos = await query.ToListAsync();

			return _productTypesExcelExporter.ExportToFile(productTypeListDtos);
		}


	}
}