﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_MeterPhases, AppPermissions.Pages_Jobs, AppPermissions.Pages_LeadTracker)]
	public class MeterPhasesAppService : TheSolarProductAppServiceBase, IMeterPhasesAppService
	{
		private readonly IRepository<MeterPhase> _meterPhaseRepository;
		private readonly IMeterPhasesExcelExporter _meterPhasesExcelExporter;


		public MeterPhasesAppService(IRepository<MeterPhase> meterPhaseRepository, IMeterPhasesExcelExporter meterPhasesExcelExporter)
		{
			_meterPhaseRepository = meterPhaseRepository;
			_meterPhasesExcelExporter = meterPhasesExcelExporter;

		}

		public async Task<PagedResultDto<GetMeterPhaseForViewDto>> GetAll(GetAllMeterPhasesInput input)
		{

			var filteredMeterPhases = _meterPhaseRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var pagedAndFilteredMeterPhases = filteredMeterPhases
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var meterPhases = from o in pagedAndFilteredMeterPhases
							  select new GetMeterPhaseForViewDto()
							  {
								  MeterPhase = new MeterPhaseDto
								  {
									  Name = o.Name,
									  DisplayOrder = o.DisplayOrder,
									  Id = o.Id
								  }
							  };

			var totalCount = await filteredMeterPhases.CountAsync();

			return new PagedResultDto<GetMeterPhaseForViewDto>(
				totalCount,
				await meterPhases.ToListAsync()
			);
		}

		public async Task<GetMeterPhaseForViewDto> GetMeterPhaseForView(int id)
		{
			var meterPhase = await _meterPhaseRepository.GetAsync(id);

			var output = new GetMeterPhaseForViewDto { MeterPhase = ObjectMapper.Map<MeterPhaseDto>(meterPhase) };

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_MeterPhases_Edit)]
		public async Task<GetMeterPhaseForEditOutput> GetMeterPhaseForEdit(EntityDto input)
		{
			var meterPhase = await _meterPhaseRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMeterPhaseForEditOutput { MeterPhase = ObjectMapper.Map<CreateOrEditMeterPhaseDto>(meterPhase) };

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMeterPhaseDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MeterPhases_Create)]
		protected virtual async Task Create(CreateOrEditMeterPhaseDto input)
		{
			var meterPhase = ObjectMapper.Map<MeterPhase>(input);

			//if (AbpSession.TenantId != null)
			//{
			//	meterPhase.TenantId = (int)AbpSession.TenantId;
			//}

			await _meterPhaseRepository.InsertAsync(meterPhase);
		}

		[AbpAuthorize(AppPermissions.Pages_MeterPhases_Edit)]
		protected virtual async Task Update(CreateOrEditMeterPhaseDto input)
		{
			var meterPhase = await _meterPhaseRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, meterPhase);
		}

		[AbpAuthorize(AppPermissions.Pages_MeterPhases_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _meterPhaseRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetMeterPhasesToExcel(GetAllMeterPhasesForExcelInput input)
		{

			var filteredMeterPhases = _meterPhaseRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name == input.NameFilter);

			var query = (from o in filteredMeterPhases
						 select new GetMeterPhaseForViewDto()
						 {
							 MeterPhase = new MeterPhaseDto
							 {
								 Name = o.Name,
								 DisplayOrder = o.DisplayOrder,
								 Id = o.Id
							 }
						 });


			var meterPhaseListDtos = await query.ToListAsync();

			return _meterPhasesExcelExporter.ExportToFile(meterPhaseListDtos);
		}


	}
}