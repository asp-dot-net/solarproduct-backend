﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Currencies;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.IncoTerms;
using TheSolarProduct.Jobs;
using TheSolarProduct.LeadActions;
using TheSolarProduct.Organizations;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.PaymentTypes;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.PurchaseDocumentListes;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.StockFroms;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockOrderFors;
using TheSolarProduct.StockOrders.Exporting;
using TheSolarProduct.StockOrders;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Vendors;
using System.Linq;
using TheSolarProduct.StockPayments.Dtos;
using Abp.Data;
using System;
using System.Data;
using TheSolarProduct.Timing;
using TheSolarProduct.StockOrder;
using TheSolarProduct.DataVaults;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Abp.Collections.Extensions;
using Telerik.Reporting;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using Microsoft.AspNetCore.Mvc;
using TheSolarProduct.StockPayments.Exporting;
using NPOI.SS.Formula.Functions;



namespace TheSolarProduct.StockPayments
{
    
    public class StockPaymentAppService : TheSolarProductAppServiceBase, IStockPaymentAppService

    {
        private readonly IStockPaymentExcelExporter _stockPaymentExcelExporter;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<StockPayment> _stockPaymentRepository;
        private readonly IRepository<StockPaymentOrderList> _StockPaymentOrderListRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _organizationRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<LeadAction, int> _lookup_leadActionRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IRepository<QuickStockSection> _sectionRepository;
        private readonly IRepository<QuickStockActivityLogHistory> _purchaseOrderHistoryRepository;
       
        private readonly IRepository<Currency> _currencyRepository;
        private readonly IRepository<StockOrderFor> _stockorderforRepository;
        private readonly IRepository<StockSerialNo> _stcokSerialnoRepository;
        private readonly IRepository<PurchaseDocumentList> _purchasedocumentListRepository;
        private readonly IRepository<PurchaseOrderDocument> _purchasedocumentRepository;
        
        public StockPaymentAppService(
              IRepository<PurchaseOrder> purchaseOrderRepository,
              IRepository<StockPayment> stockPaymentRepository,
              IRepository<StockPaymentOrderList> StockPaymentOrderListRepository,
              IRepository<ExtendOrganizationUnit, long> organizationRepository,
              ITimeZoneConverter timeZoneConverter,
              IStockPaymentExcelExporter IStockPaymentExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<QuickStockActivityLog> quickStockActivityLogRepository,
              IRepository<QuickStockActivityLogHistory> purchaseOrderHistoryRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
              IRepository<LeadAction, int> lookup_leadActionRepository,
              IRepository<User, long> userRepository,
              IRepository<QuickStockSection> sectionRepository,
              IRepository<Currency> currencyRepository,
              IRepository<StockOrderFor> stockorderforRepository,
              IRepository<StockSerialNo> stcokSerialnoRepository,
              IRepository<PurchaseDocumentList> purchasedocumentListRepository,
              IRepository<PurchaseOrderDocument> purchasedocumentRepository
              )
        {
            _purchaseOrderRepository = purchaseOrderRepository;
            _stockPaymentRepository = stockPaymentRepository;
            _StockPaymentOrderListRepository = StockPaymentOrderListRepository;
            _organizationRepository = organizationRepository;
            _timeZoneConverter = timeZoneConverter;
            _stockPaymentExcelExporter = IStockPaymentExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _purchaseOrderHistoryRepository = purchaseOrderHistoryRepository;
            _dbcontextprovider = dbcontextprovider;
            _lookup_leadActionRepository = lookup_leadActionRepository;
            _userRepository = userRepository;
            _sectionRepository = sectionRepository;
            
            _currencyRepository = currencyRepository;
            _stockorderforRepository = stockorderforRepository;
            _stcokSerialnoRepository = stcokSerialnoRepository;
            _purchasedocumentListRepository = purchasedocumentListRepository;
            _purchasedocumentRepository = purchasedocumentRepository;

        }

        public async Task<PagedResultDto<GetAllStockPaymentOutputDto>> GetAll(GetAllStockPaymentInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var StockPayment = _stockPaymentRepository.GetAll()
                    .Include(e => e.PaymentMethodFK)
                    .Include(e => e.CurrencyFk)
                    .WhereIf(input.FilterName == "InvoiceNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.StockNo.ToString().Contains(input.Filter))
                    .WhereIf(input.PaymentMethodFilter > 0, e => e.PaymentMethodId == input.PaymentMethodFilter)
                    .WhereIf(input.GstTypeIdFilter > 0, e => e.GSTType == input.GstTypeIdFilter)
                    .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                    .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                    .Where(e => e.OrganizationId == input.OrganizationId);

            var pagedAndFilteredStockPayment = StockPayment
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input);

            var outputStockPayment = from o in pagedAndFilteredStockPayment
                                   select new GetAllStockPaymentOutputDto()
                                   {
                                       Id = o.Id,
                                      StockNo = o.StockNo,
                                       OrganizationId = o.OrganizationId,
                                       Currency = o.CurrencyFk.Name,
                                       PaymentAmount = o.PaymentAmount,
                                       PaymentMethod=o.PaymentMethodFK.Name,
                                       PaymentDate = o.PaymentDate,
                                       AUD=o.AUD,
                                       GSTType = o.GSTType == 1 ? "With GST" : (o.GSTType == 2 ? "Without GST" : " "),
                                       Rate =o.Rate,
                                       USD=o.USD      

                                   };
            var StockPaymentItems = outputStockPayment.ToList();

            
            var totalCount = await StockPayment.CountAsync();

            return new PagedResultDto<GetAllStockPaymentOutputDto>(
                totalCount,
                StockPaymentItems
            );
        }
        public async Task CreateOrEdit(CreateOrEditStockPaymentDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);  
            }
        }
        protected virtual async Task Update(CreateOrEditStockPaymentDto input)
        {
            var stockPayment = await _stockPaymentRepository.FirstOrDefaultAsync((int)input.Id);
            //input.PaymentDueDate = _timeZoneConverter.Convert(input.PaymentDueDate, (int)AbpSession.TenantId);
            //input.ETA = _timeZoneConverter.Convert(input.ETA, (int)AbpSession.TenantId);
            //input.ETD = _timeZoneConverter.Convert(input.ETD, (int)AbpSession.TenantId);
            //input.TargetArrivalDate = _timeZoneConverter.Convert(input.TargetArrivalDate, (int)AbpSession.TenantId);

            //var quickStockLog = new QuickStockActivityLog();
            //quickStockLog.Action = "Updated";
            //quickStockLog.SectionId = 1;
            //quickStockLog.ActionId = 69;
            //quickStockLog.PurchaseOrderId = purchaseOrder.Id;
            //quickStockLog.ActionNote = "Purchase Order Updated";
            //quickStockLog.Type = "Purchase Order";
            //var quickStockLogId = await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            //var List = new List<QuickStockActivityLogHistory>();

            //if (input.VendorId != purchaseOrder.VendorId)
            //{
            //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
            //    history.FieldName = "Vendor";
            //    history.PrevValue = purchaseOrder.VendorId > 0 ? _vendorReposiorty.GetAll().Where(e => e.Id == purchaseOrder.VendorId).FirstOrDefault().CompanyName : "";
            //    history.CurValue = input.VendorId > 0 ? _vendorReposiorty.GetAll().Where(e => e.Id == input.VendorId).FirstOrDefault().CompanyName : "";
            //    history.Action = "Update";
            //    history.ActivityLogId = quickStockLogId;

            //    List.Add(history);
            //}
            //if (input.PurchaseCompanyId != purchaseOrder.PurchaseCompanyId)
            //{
            //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
            //    history.FieldName = "PurchaseCompany";
            //    history.PrevValue = purchaseOrder.PurchaseCompanyId > 0 ? _purchaseCompanyReposiorty.GetAll().Where(e => e.Id == purchaseOrder.PurchaseCompanyId).FirstOrDefault().Name : "";
            //    history.CurValue = input.PurchaseCompanyId > 0 ? _purchaseCompanyReposiorty.GetAll().Where(e => e.Id == input.PurchaseCompanyId).FirstOrDefault().Name : "";
            //    history.Action = "Update";
            //    history.ActivityLogId = quickStockLogId;
            //    List.Add(history);
            //}
            //if (input.ManualOrderNo != purchaseOrder.ManualOrderNo)
            //{
            //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
            //    history.FieldName = "ManualOrderNo";
            //    history.PrevValue = purchaseOrder.ManualOrderNo;
            //    history.CurValue = input.ManualOrderNo;
            //    history.Action = "Update";
            //    history.ActivityLogId = quickStockLogId;
            //    List.Add(history);
            //}
            //if (input.PhysicalDeliveryDate != purchaseOrder.PhysicalDeliveryDate)
            //{
            //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
            //    history.FieldName = "PhysicalDeliveryDate";
            //    history.PrevValue = purchaseOrder.PhysicalDeliveryDate.ToString();
            //    history.CurValue = input.PhysicalDeliveryDate.ToString();
            //    history.Action = "Update";
            //    history.ActivityLogId = quickStockLogId;
            //    List.Add(history);
            //}
            //if (input.CurrencyId != purchaseOrder.CurrencyId)
            //{
            //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
            //    history.FieldName = "Currency";
            //    history.PrevValue = purchaseOrder.CurrencyId > 0 ? _currencyRepository.GetAll().Where(e => e.Id == purchaseOrder.CurrencyId).FirstOrDefault().Name : "";
            //    history.CurValue = input.CurrencyId > 0 ? _currencyRepository.GetAll().Where(e => e.Id == input.CurrencyId).FirstOrDefault().Name : "";
            //    history.Action = "Update";
            //    history.ActivityLogId = quickStockLogId;
            //    List.Add(history);
            //}
            //if (input.GSTType != purchaseOrder.GSTType)
            //{
            //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
            //    history.FieldName = "GSTType";
            //    history.PrevValue = purchaseOrder.GSTType == 1 ? "With GST" : "WithOut GST";
            //    history.CurValue = input.GSTType == 1 ? " WithGST" : "WithOut GST";
            //    history.Action = "Update";
            //    history.ActivityLogId = quickStockLogId;
            //    List.Add(history);
            //}
           

            var stockPaymentOrderItemIds = await _StockPaymentOrderListRepository.GetAll().Where(e => e.StockPaymentId == stockPayment.Id).ToListAsync();
            var CurrentstockPaymentOrderItemIds = input.StockPaymentOrderlist != null ? input.StockPaymentOrderlist.Select(e => e.Id).ToList() : new List<int?>();
            var liststockPayment = stockPaymentOrderItemIds.Where(e => !CurrentstockPaymentOrderItemIds.Contains(e.Id));

            foreach (var item in liststockPayment)
            {
                //QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();

                //history.FieldName = "purchaseOrderItem Deleted";
                //history.PrevValue = " ";
                //history.CurValue = item.ProductItemFk?.Name;
                //history.Action = "Deleted";
                //history.ActivityLogId = quickStockLogId;
                //List.Add(history);

                await _StockPaymentOrderListRepository.DeleteAsync(item);
            }
            foreach (var item in input.StockPaymentOrderlist)
            {
                item.StockPaymentId = stockPayment.Id;
                if ((item.Id != null ? item.Id : 0) == 0)
                {

                    var StockPaymentOrder = ObjectMapper.Map<StockPaymentOrderList>(item);
                    //QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //history.FieldName = "purchaseOrderItem Added";
                    //history.PrevValue = "";
                    //history.CurValue = item.ProductItem;
                    //history.Action = "Add";
                    //history.ActivityLogId = quickStockLogId;
                    //List.Add(history);
                    await _StockPaymentOrderListRepository.InsertAsync(StockPaymentOrder);
                }

                else

                {
                    var StockPaymentOrder = await _StockPaymentOrderListRepository.GetAsync((int)item.Id);

                    //if (item.StockPaymentId != StockPaymentOrder.StockPaymentId)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "PurchaseOrder";
                    //    history.PrevValue = purchaseOrderItem.PurchaseOrderId.ToString();
                    //    history.CurValue = item.PurchaseOrderId.ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}
                    //if (item.ProductItemId != purchaseOrderItem.ProductItemId)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "ProductItem";
                    //    history.PrevValue = purchaseOrderItem.ProductItemId.ToString();
                    //    history.CurValue = item.ProductItemId.ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}
                    //if (item.Quantity != purchaseOrderItem.Quantity)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "Quantity";
                    //    history.PrevValue = purchaseOrderItem.Quantity.ToString();
                    //    history.CurValue = item.Quantity.ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}
                    //if (item.PricePerWatt != purchaseOrderItem.PricePerWatt)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "PricePerWatt";
                    //    history.PrevValue = purchaseOrderItem.PricePerWatt.ToString();
                    //    history.CurValue = item.PricePerWatt
                    //        .ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}
                    //if (item.UnitRate != purchaseOrderItem.UnitRate)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "UnitRate";
                    //    history.PrevValue = purchaseOrderItem.UnitRate.ToString();
                    //    history.CurValue = item.UnitRate.ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}
                    //if (item.EstRate != purchaseOrderItem.EstRate)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "EstRate";
                    //    history.PrevValue = purchaseOrderItem.EstRate.ToString();
                    //    history.CurValue = item.EstRate.ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}
                    //if (item.Amount != purchaseOrderItem.Amount)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "Amount";
                    //    history.PrevValue = purchaseOrderItem.Amount.ToString();
                    //    history.CurValue = item.Amount.ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}
                    //if (item.ModelNo != purchaseOrderItem.ModelNo)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "ModelNo";
                    //    history.PrevValue = purchaseOrderItem.ModelNo.ToString();
                    //    history.CurValue = item.ModelNo.ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}
                    //if (item.ExpiryDate != purchaseOrderItem.ExpiryDate)
                    //{
                    //    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    //    history.FieldName = "ExpiryDate";
                    //    history.PrevValue = purchaseOrderItem.ExpiryDate.ToString();
                    //    history.CurValue = item.ExpiryDate.ToString();
                    //    history.Action = "Update";
                    //    history.ActivityLogId = quickStockLogId;
                    //    List.Add(history);
                    //}

                    ObjectMapper.Map(item, StockPaymentOrder);
                    await _StockPaymentOrderListRepository.UpdateAsync(StockPaymentOrder);
                    //purchaseOrderItem = ObjectMapper.Map<PurchaseOrderItem>(item);


                }

            }
            //await _dbcontextprovider.GetDbContext().QuickStockActivityLogHistory.AddRangeAsync(List);
            //await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, stockPayment);
        }
        protected virtual async Task Create(CreateOrEditStockPaymentDto input)
        {
            input.PaymentDate = _timeZoneConverter.Convert(input.PaymentDate, (int)AbpSession.TenantId);
            
            var StockPayment = ObjectMapper.Map<StockPayment>(input);
            var StockPaymentid = await _stockPaymentRepository.InsertAndGetIdAsync(StockPayment);
            //var quickStockLog = new QuickStockActivityLog();
            //quickStockLog.Action = "Created";
            //quickStockLog.SectionId = 1;
            //quickStockLog.ActionId = 68;
            //quickStockLog.Type = "Purchase Order";
            //quickStockLog.PurchaseOrderId = purchaseOrderid;
            //quickStockLog.ActionNote = "Purchase Order Created ";

            //await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            foreach (var item in input.StockPaymentOrderlist)
            {
                item.StockPaymentId = StockPaymentid;
                var purchaseOrderoitem = ObjectMapper.Map<StockPaymentOrderList>(item);

                await _StockPaymentOrderListRepository.InsertAsync(purchaseOrderoitem);
            }
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _stockPaymentRepository.Get(input.Id);

            //var quickStockLog = new QuickStockActivityLog();
            //quickStockLog.Action = "Deleted";
            //quickStockLog.SectionId = 1;
            //quickStockLog.ActionId = 70;
            //quickStockLog.PurchaseOrderId = input.Id;
            //quickStockLog.ActionNote = "purchase order Deleted : " + Name;
            //quickStockLog.Type = "Purchase Order";
            //await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            await _stockPaymentRepository.DeleteAsync(input.Id);

        }
        public async Task<GetStockPaymentForEditOutput> GetStockPaymentForEdit(EntityDto input)
        {
            var stockPayment = await _stockPaymentRepository.GetAsync(input.Id);

            var stockPaymentDto = ObjectMapper.Map<CreateOrEditStockPaymentDto>(stockPayment);

            var stockPaymentOrderItem = await _StockPaymentOrderListRepository.GetAll().Include(e => e.StockPaymentFK).Include(e => e.PurchaseOrderFK).Where(e => e.StockPaymentId == input.Id).ToListAsync();

            stockPaymentDto.StockPaymentOrderlist = new List<StockPaymentOrderListDto>();
            foreach (var item in stockPaymentOrderItem)
            {
                var stockPaymentOrder = new StockPaymentOrderListDto();
                stockPaymentOrder.Id = item.Id;
                stockPaymentOrder.StockPaymentId = item.StockPaymentId;
                stockPaymentOrder.PurchaseOrderId = item.PurchaseOrderId;
                stockPaymentOrder.Amount = item.Amount;
                stockPaymentOrder.OrderNo = item.PurchaseOrderFK.OrderNo;
                stockPaymentDto.StockPaymentOrderlist.Add(stockPaymentOrder);
            }

            var output = new GetStockPaymentForEditOutput
            {
                StockPayment = stockPaymentDto
            };


            return output;
        }

        public async Task<long> GetLastStockNo(int organizationId)
        {
            var StockNo = await _stockPaymentRepository.GetAll().OrderByDescending(e => e.Id).Select(e => e.StockNo).FirstOrDefaultAsync();

            if (StockNo == 0)
            {

                StockNo = 1000;
                return StockNo+1;
            }

            return StockNo + 1;
        }

        public async Task<List<GetOrderNoSuggestionDto>> GetOrderNoSuggestions(string search,int orgid,int currencyId,int gstType)
        {
            return await _purchaseOrderRepository.GetAll()
                .Where(o => o.OrderNo.ToString().Contains(search) && o.IsDeleted == false && o.OrganizationId == orgid && o.CurrencyId == currencyId && o.GSTType == gstType) 
                .Select(o => new GetOrderNoSuggestionDto
                {
                    OrderNo = o.OrderNo.ToString(), 
                    PurchaseOrderId = o.Id
                })
                .ToListAsync();
        }

        public async Task<GetStockPaymentForEditOutput> GetStockPaymentByStockNo(long stockNo)
        {

            var stockPayment = await _stockPaymentRepository.FirstOrDefaultAsync(po => po.StockNo == stockNo);
            var stockPaymentDto = ObjectMapper.Map<CreateOrEditStockPaymentDto>(stockPayment);
            var stockPaymentOrderItems = await _StockPaymentOrderListRepository.GetAll()
                                           .Include(e => e.StockPaymentFK)
                                           .Where(e => e.StockPaymentId == stockPayment.Id)
                                           .ToListAsync();

            stockPaymentDto.StockPaymentOrderlist = new List<StockPaymentOrderListDto>();
            foreach (var item in stockPaymentOrderItems)
            {
                var stockPaymentOrderItemsDto = new StockPaymentOrderListDto
                {
                     StockPaymentId = item.StockPaymentId,
                     PurchaseOrderId = item.PurchaseOrderId,
                     Amount = item.Amount,
                     OrderNo = item.PurchaseOrderFK.OrderNo

            };

                stockPaymentDto.StockPaymentOrderlist.Add(stockPaymentOrderItemsDto);
            }
            var output = new GetStockPaymentForEditOutput
            {
                StockPayment = stockPaymentDto
            };
            return output;
        }
        public async Task<FileDto> GetStockPaymentToExcel(GetStockPaymentForExcelInput input)
        {

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var StockPayment = _stockPaymentRepository.GetAll()
                    .Include(e => e.PaymentMethodFK)
                    .Include(e => e.CurrencyFk)
                    .WhereIf(input.FilterName == "InvoiceNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.StockNo.ToString().Contains(input.Filter))
                    .WhereIf(input.PaymentMethodFilter > 0, e => e.PaymentMethodId == input.PaymentMethodFilter)
                    .WhereIf(input.GstTypeIdFilter > 0, e => e.GSTType == input.GstTypeIdFilter)
                    .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                    .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                    .WhereIf(input.Datefilter == "PaymentDate" && input.StartDate != null, e => e.PaymentDate.Value.Date >= SDate.Value.Date)
                    .WhereIf(input.Datefilter == "PaymentDate" && input.EndDate != null, e => e.PaymentDate.Value.Date <= EDate.Value.Date)
                    .Where(e => e.OrganizationId == input.OrganizationId);
            var query = from o in StockPayment
                        select new GetAllStockPaymentOutputDto()
                        {
                            Id = o.Id,
                            StockNo = o.StockNo,
                            OrganizationId = o.OrganizationId,
                            Currency = o.CurrencyFk.Name,
                            PaymentAmount = o.PaymentAmount,
                            PaymentMethod = o.PaymentMethodFK.Name,
                            PaymentDate = o.PaymentDate,
                            AUD = o.AUD,
                            GSTType = o.GSTType == 1 ? "With GST" : (o.GSTType == 2 ? "Without GST" : " "),
                            Rate = o.Rate,
                            USD = o.USD

                        };
            var stockPaymentItems = query.ToList();

           
            var ListDtos = stockPaymentItems;
            return _stockPaymentExcelExporter.ExportToFile(ListDtos);
        }

    }
}
