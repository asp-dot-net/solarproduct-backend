﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockOrders;
using TheSolarProduct.StockOrders.Exporting;
using TheSolarProduct.StockPayments.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.StockPayments.Exporting
{
    
    public class StockPaymentExcelExporter : NpoiExcelExporterBase, IStockPaymentExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StockPaymentExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllStockPaymentOutputDto> StockPayment)
        {
            return CreateExcelPackage(
                "StockPayment.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("StockPayment");

                    //int[] columnWidths = { 10, 19, 18, 19, 11, 11, 18, 14, 14, 14, 16, 11 };

                    //for (int i = 0; i < columnWidths.Length; i++)
                    //{
                    //    sheet.SetColumnWidth(i, columnWidths[i] * 256); 
                    //}

                    AddHeader(
                        sheet,
                        "StockNO",
                        "PaymentMethod",
                        "PaymentDate",
                        "Currency",
                        "GSTType",
                        "Rate",
                        "PaymentAmount",
                        "AUD",
                        "USD"
                       
         
                    );
                    AddObjects(
                        sheet,
                        2,
                        StockPayment,
                        _ => _.StockNo,
                        _ => _.PaymentMethod,
                        _ => _.PaymentDate,
                        _ => _.Currency,
                        _ => _.GSTType,
                        _ => _.Rate,
                        _ => _.PaymentAmount,
                        _ => _.AUD,
                        _ => _.USD
                       
                    );
                    for (var i = 0; i < 9; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                    for (var i = 1; i <= StockPayment.Count; i++)
                    {

                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");

                    }
                }
            );
        }



    }
}
