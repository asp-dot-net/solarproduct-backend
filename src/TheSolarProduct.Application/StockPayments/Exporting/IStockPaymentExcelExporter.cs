﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.StockPayments.Dtos;

namespace TheSolarProduct.StockPayments.Exporting
{
    public interface IStockPaymentExcelExporter
    {
        FileDto ExportToFile(List<GetAllStockPaymentOutputDto> StockPayment);
    }
}
