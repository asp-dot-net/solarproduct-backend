﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.ProductPackages.Dtos;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using System;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.PostCodeRange;
using TheSolarProduct.InstallationCost.InstallationItemLists;
using TheSolarProduct.Jobs;
using Abp.Organizations;
using TheSolarProduct.States;
using TheSolarProduct.CategoryInstallations;

namespace TheSolarProduct.ProductPackages
{
    [AbpAuthorize]
    public class ProductPackageAppService : TheSolarProductAppServiceBase, IProductPackageAppService
    {
        private readonly IRepository<ProductPackage> _productPackageRepository;
        private readonly IRepository<ProductPackageItem> _productPackageItemRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<State> _stateRepository;


        public ProductPackageAppService(IRepository<ProductPackage> productPackageRepository, IRepository<ProductPackageItem> productPackageItemRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider, IRepository<ProductItem> productItemRepository, IRepository<OrganizationUnit, long> organizationUnitRepository, IRepository<State> stateRepository)
        {
            _productPackageRepository = productPackageRepository;
            _productPackageItemRepository = productPackageItemRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _productItemRepository = productItemRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _stateRepository = stateRepository;
        }

        public async Task<PagedResultDto<GetProductPackageForViewDto>> GetAll(GetAllProductPackageForInput input)
        {
            var filtered = _productPackageRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var pagedAndFiltered = filtered
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var outputList = from o in pagedAndFiltered
                                 select new GetProductPackageForViewDto()
                                 {
                                     ProductPackage = new ProductPackageDto
                                     {
                                         Name = o.Name,
                                         Price = o.Price,
                                         IsActive = o.IsActive,
                                         Id = o.Id
                                     }
                                 };

            var totalCount = await outputList.CountAsync();

            return new PagedResultDto<GetProductPackageForViewDto>(
                totalCount,
                await outputList.ToListAsync()
            );
        }

        public async Task<GetProductPackageForViewDto> GetProductPackageForView(int id)
        {
            var retult = await _productPackageRepository.GetAsync(id);

            var output = new GetProductPackageForViewDto { ProductPackage = ObjectMapper.Map<ProductPackageDto>(retult) };

            return output;
        }

        //[AbpAuthorize(AppPermissions.Pages_ProductPackages_Edit)]
        public async Task<GetProductPackageForEditOutput> GetProductPackageForEdit(EntityDto input)
        {
            var result = await _productPackageRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProductPackageForEditOutput { ProductPackage = ObjectMapper.Map<CreateOrEditProductPackageDto>(result) };

            var Organizations = result.Organizations.Split(",");
            output.ProductPackage.OrganizationIds = new List<int>();
            foreach (var id in Organizations)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    output.ProductPackage.OrganizationIds.Add(Convert.ToInt16(id));

                }
            }

            var States = result.States.Split(",").ToList();
            output.ProductPackage.StateIds = new List<int>();
            foreach (var id in States)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    output.ProductPackage.StateIds.Add(Convert.ToInt16(id));

                }
            }

            var packageItemResult = _productPackageItemRepository.GetAll().Include(e => e.ProductItemFk).Where(e => e.ProductPackageId == input.Id);

            output.ProductPackage.ProductPackageItem = (from o in packageItemResult
                                                       select new CreateOrEditProductPackageItemDto()
                                                       {
                                                           Id = o.Id,
                                                           ProductItemId = o.ProductItemId,
                                                           ProductPackageId = o.ProductPackageId,
                                                           ProductTypeId = o.ProductItemFk.ProductTypeId,
                                                           ProductItem = o.ProductItemFk.Name,
                                                           Model = o.ProductItemFk.Model,
                                                           Quantity = o.Quantity

                                                       }).ToList();

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditProductPackageDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ProductPackages_Create)]
        protected virtual async Task Create(CreateOrEditProductPackageDto input)
        {
            var output = ObjectMapper.Map<ProductPackage>(input);

            output.Organizations = "";
            foreach (var id in input.OrganizationIds)
            {
                output.Organizations = output.Organizations + id + ",";
            }

            output.States = "";
            foreach (var id in input.StateIds)
            {
                output.States = output.States + id + ",";
            }

            await _productPackageRepository.InsertAndGetIdAsync(output);

            if(output.Id > 0)
            {
                foreach (var item in input.ProductPackageItem)
                {
                    var outputItem = ObjectMapper.Map<ProductPackageItem>(item);
                    outputItem.ProductPackageId = output.Id;
                    await _productPackageItemRepository.InsertAsync(outputItem);
                }
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 42;
            dataVaultLog.ActionNote = "Product Package Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);
        }

        [AbpAuthorize(AppPermissions.Pages_ProductPackages_Edit)]
        protected virtual async Task Update(CreateOrEditProductPackageDto input)
        {
            var output = await _productPackageRepository.FirstOrDefaultAsync((int)input.Id);

            var Organizations = "";
            var orgids = new List<int>();
            foreach (var id in output.Organizations.Split(','))
            {
                if(!string.IsNullOrEmpty(id))
                {
                    orgids.Add(Convert.ToInt32(id));

                }
            }

            var inputOrgIds = new List<int>();

            var stateIds = new List<int>();

            foreach (var id in output.States.Split(','))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    stateIds.Add(Convert.ToInt32(id));

                }
            }

            var inputStateIds = new List<int>();

            foreach (var id in input.OrganizationIds)
            {
                Organizations = Organizations + id + ",";
                inputOrgIds.Add(Convert.ToInt32(id));
            }

            var States = "";
            foreach (var id in input.StateIds)
            {
                States = States + id + ",";
                inputStateIds.Add(Convert.ToInt32(id));
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 42;
            dataVaultLog.ActionNote = "Product Package Updated : " + output.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != output.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = output.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Price != output.Price)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Price";
                history.PrevValue = output.Price.ToString();
                history.CurValue = input.Price.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.IsActive != output.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = output.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (States != output.States)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "States";
                history.PrevValue = string.Join(" ,",_stateRepository.GetAll().Where(e => stateIds.Contains(e.Id)).Select(e => e.Name));
                history.CurValue = string.Join(" ,", _stateRepository.GetAll().Where(e => inputStateIds.Contains(e.Id)).Select(e => e.Name));
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (Organizations != output.Organizations)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Organizations";
                history.PrevValue = string.Join(" ,", _organizationUnitRepository.GetAll().Where(e => orgids.Contains((int)e.Id)).Select(e => e.DisplayName));
                history.CurValue = string.Join(" ,", _organizationUnitRepository.GetAll().Where(e => inputOrgIds.Contains((int)e.Id)).Select(e => e.DisplayName)); 
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            output.Organizations = Organizations; 
            output.States = States;

            ObjectMapper.Map(input, output);
            var Items = _productPackageItemRepository.GetAll().Where(e => e.ProductPackageId == input.Id).ToList();

            var itemIds = input.ProductPackageItem.Where(e => e.Id > 0).Select(e => e.Id).ToList();

            var deleteItems = Items.Where(e => !itemIds.Contains(e.Id)).ToList();

            foreach (var item in deleteItems)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Item Delete";
                history.PrevValue = _productItemRepository.Get(item.ProductItemId).Name;
                history.CurValue = "";
                history.Action = "Delete";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _productPackageItemRepository.HardDeleteAsync(e => e.ProductPackageId == input.Id);
            foreach(var item in input.ProductPackageItem)
            {
                var installationItemList = ObjectMapper.Map<ProductPackageItem>(item);
                installationItemList.ProductPackageId = (int)input.Id;

                await _productPackageItemRepository.InsertAndGetIdAsync(installationItemList);
                if (item.Id > 0)
                {
                    var updateItem = Items.Where(e => e.Id == item.Id).FirstOrDefault();

                    if (item.ProductItemId != updateItem.ProductItemId)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "ProductItem";
                        history.PrevValue = updateItem.ProductItemId > 0 ? _productItemRepository.Get(updateItem.ProductItemId).Name : "";
                        history.CurValue = item.ProductItemId > 0 ? _productItemRepository.Get(item.ProductItemId).Name : "";
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }
                    if (item.Quantity != updateItem.Quantity)
                    {
                        DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                        history.FieldName = "Quantity";
                        history.PrevValue = updateItem.Quantity.ToString();
                        history.CurValue = item.Quantity.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = dataVaultLogId;
                        List.Add(history);
                    }

                }
                else
                {
                    DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                    history.FieldName = "Item Added";
                    history.PrevValue = _productItemRepository.Get(item.ProductItemId).Name;
                    history.CurValue = "";
                    history.Action = "Added";
                    history.ActivityLogId = dataVaultLogId;
                    List.Add(history);
                }
            }


            #region Product Package Item
            //if (input.ProductPackageItem != null)
            //{
            //    var existData = _productPackageItemRepository.GetAll().Include(e => e.ProductItemFk).AsNoTracking().Where(x => x.ProductPackageId == input.Id).ToList();
            //    if (existData.Any())
            //    {
            //        var Ids = input.ProductPackageItem.Select(e => e.Id).ToList();
            //        foreach (var item in existData)
            //        {
            //            if (Ids.Count > 0)
            //            {
            //                bool containsItem = Ids.Any(x => x == item.Id);
            //                if (containsItem == false)
            //                {
            //                    await _productPackageItemRepository.DeleteAsync(item);
            //                }
            //            }
            //            else
            //            {
            //                await _productPackageItemRepository.DeleteAsync(item);
            //            }
                        
            //        }

            //        foreach (var item in input.ProductPackageItem)
            //        {
            //            if (item.Id != null && item.Id != 0)
            //            {
            //                var productPackageItem = await _productPackageItemRepository.GetAsync((int)item.Id);
            //                item.ProductPackageId = output.Id;
            //                ObjectMapper.Map(item, productPackageItem);

                            
            //            }
            //            else
            //            {
            //                var dto = ObjectMapper.Map<ProductPackageItem>(item);
            //                dto.ProductPackageId = output.Id;
            //                await _productPackageItemRepository.InsertAsync(dto);

                            
            //            }
            //        }
            //    }
            //    else
            //    {
            //        foreach (var item in input.ProductPackageItem)
            //        {
            //            var dto = ObjectMapper.Map<ProductPackageItem>(item);
            //            dto.ProductPackageId = output.Id;
            //            await _productPackageItemRepository.InsertAsync(dto);

            //        }
            //    }
            //}
            
            #endregion
            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_ProductPackages_Delete)]
        public async Task Delete(EntityDto input)
        {
            var postCode = _productPackageRepository.Get(input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 42;
            dataVaultLog.ActionNote = "Product Package Deleted : " + postCode.Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _productPackageRepository.DeleteAsync(input.Id);
        }

        public async Task<List<NameValueDto>> GetProductPackageForDropdown()
        {
            var result = _productPackageRepository.GetAll().Where(e => e.IsActive == true);

            var output = from o in result
                         select new NameValueDto()
                         {
                             Name = o.Name,
                             Value = o.Id.ToString()
                         };

            return await output.ToListAsync();
        }

        public async Task<List<CreateOrEditProductPackageItemDto>> GetProductPackageItemByPackageId(int productPackageId)
        {
            var result = _productPackageItemRepository.GetAll().Where(e => e.ProductPackageId == productPackageId);

            var output = from o in result
                         select new CreateOrEditProductPackageItemDto()
                         {
                             Id = o.Id,
                             ProductItemId = o.ProductItemId,
                             ProductPackageId = o.ProductPackageId,
                             ProductTypeId = o.ProductItemFk.ProductTypeId,
                             Model = o.ProductItemFk.Model,
                             ProductItem = o.ProductItemFk.Name,
                             Quantity = o.Quantity,
                             Size = o.ProductItemFk.Size,
                             DatasheetUrl = (o.ProductItemFk.FilePath + o.ProductItemFk.FileName),
                             RefNo = ""
                         };

            return await output.ToListAsync();
        }

        public async Task<List<GetAllPackageDetailsDto>> GetAllPackageDetails()
        {
            var productPackages = _productPackageRepository.GetAll().Where(e => e.IsActive == true);

            var output = from o in productPackages
                         select new GetAllPackageDetailsDto()
                         {
                             Id = o.Id,
                             Name = o.Name,
                             Price = o.Price,
                             PackageItems = (from p in _productPackageItemRepository.GetAll().Where(e => e.ProductPackageId == o.Id)
                                            select new PackageItems
                                            {
                                                ProductType = p.ProductItemFk.ProductTypeFk.Name,
                                                ProductItem = p.ProductItemFk.Name,
                                                Model = p.ProductItemFk.Model,
                                                Quantity = p.Quantity
                                            }).ToList(),
                         };

            return await output.ToListAsync();
        }
    }
}
