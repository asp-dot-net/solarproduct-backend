﻿

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.StreetNames.Exporting;
using TheSolarProduct.StreetNames.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.StreetTypes.Exporting;
using TheSolarProduct.DataVaults;
using TheSolarProduct.StreetTypes;

namespace TheSolarProduct.StreetNames
{
	[AbpAuthorize(AppPermissions.Pages_StreetNames)]
    public class StreetNamesAppService : TheSolarProductAppServiceBase, IStreetNamesAppService
    {
		 private readonly IRepository<StreetName> _streetNameRepository;
		 private readonly IStreetNamesExcelExporter _streetNamesExcelExporter;
         private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
         private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public StreetNamesAppService(IRepository<StreetName> streetNameRepository,
			 IStreetNamesExcelExporter streetNamesExcelExporter,
             IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
             IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            ) 
		  {
			_streetNameRepository = streetNameRepository;
			_streetNamesExcelExporter = streetNamesExcelExporter;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

		 public async Task<PagedResultDto<GetStreetNameForViewDto>> GetAll(GetAllStreetNamesInput input)
         {
			
			var filteredStreetNames = _streetNameRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var pagedAndFilteredStreetNames = filteredStreetNames
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

			var streetNames = from o in pagedAndFilteredStreetNames
                         select new GetStreetNameForViewDto() {
							StreetName = new StreetNameDto
							{
                                Name = o.Name,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						};

            var totalCount = await filteredStreetNames.CountAsync();

            return new PagedResultDto<GetStreetNameForViewDto>(
                totalCount,
                await streetNames.ToListAsync()
            );
         }
		 
		 public async Task<GetStreetNameForViewDto> GetStreetNameForView(int id)
         {
            var streetName = await _streetNameRepository.GetAsync(id);

            var output = new GetStreetNameForViewDto { StreetName = ObjectMapper.Map<StreetNameDto>(streetName) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_StreetNames_Edit)]
		 public async Task<GetStreetNameForEditOutput> GetStreetNameForEdit(EntityDto input)
         {
            var streetName = await _streetNameRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetStreetNameForEditOutput {StreetName = ObjectMapper.Map<CreateOrEditStreetNameDto>(streetName)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditStreetNameDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_StreetNames_Create)]
		 protected virtual async Task Create(CreateOrEditStreetNameDto input)
         {
            var streetName = ObjectMapper.Map<StreetName>(input);
			streetName.Name = streetName.Name.ToUpper();

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 74;
            dataVaultLog.ActionNote = "StreetName Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _streetNameRepository.InsertAsync(streetName);
         }

		 [AbpAuthorize(AppPermissions.Pages_StreetNames_Edit)]
		 protected virtual async Task Update(CreateOrEditStreetNameDto input)
         {
            var streetName = await _streetNameRepository.FirstOrDefaultAsync((int)input.Id);
			streetName.Name = streetName.Name.ToUpper();

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 74;
            dataVaultLog.ActionNote = "StreetName Updated : " + streetName.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != streetName.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = streetName.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.IsActive != streetName.IsActive)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "IsActive";
                history.PrevValue = streetName.IsActive.ToString();
                history.CurValue = input.IsActive.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();
            ObjectMapper.Map(input, streetName);
         }

		 [AbpAuthorize(AppPermissions.Pages_StreetNames_Delete)]
         public async Task Delete(EntityDto input)
         {
            var Name = _streetNameRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 74;
            dataVaultLog.ActionNote = "StreetName Deleted : " + Name;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _streetNameRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetStreetNamesToExcel(GetAllStreetNamesForExcelInput input)
         {
			
			var filteredStreetNames = _streetNameRepository.GetAll()
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter);

			var query = (from o in filteredStreetNames
                         select new GetStreetNameForViewDto() { 
							StreetName = new StreetNameDto
							{
                                Name = o.Name,
                                Id = o.Id,
                                IsActive = o.IsActive,
							}
						 });


            var streetNameListDtos = await query.ToListAsync();

            return _streetNamesExcelExporter.ExportToFile(streetNameListDtos);
         }


    }
}