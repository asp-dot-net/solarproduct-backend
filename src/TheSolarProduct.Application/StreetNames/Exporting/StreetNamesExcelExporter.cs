﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.StreetNames.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.StreetNames.Exporting
{
    public class StreetNamesExcelExporter : NpoiExcelExporterBase, IStreetNamesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StreetNamesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetStreetNameForViewDto> streetNames)
        {
            return CreateExcelPackage(
                "StreetNames.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("StreetNames"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, 2, streetNames,
                        _ => _.StreetName.Name,
                        _ => _.StreetName.IsActive ? L("Yes") : L("No")
                        );

					
					
                });
        }
    }
}
