﻿using System.Collections.Generic;
using TheSolarProduct.StreetNames.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.StreetNames.Exporting
{
    public interface IStreetNamesExcelExporter
    {
        FileDto ExportToFile(List<GetStreetNameForViewDto> streetNames);
    }
}