﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.StockOrders.Importing.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.StockOrders.Importing
{
    
    public class InvalidPurchaseOrderItemExporter : NpoiExcelExporterBase, ITransientDependency
    {
        public InvalidPurchaseOrderItemExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportPurchaseOrderItemDto> stcListDtos)
        {

            return CreateExcelPackage(
         "InvalidSTCS.xlsx",
          excelPackage =>
          {
              var sheet = excelPackage.CreateSheet("InvalidSTC");


              AddHeader(sheet,
                  "SerialNo",
                  "Exception"
                  );


              AddObjects(
                    sheet, 2, stcListDtos,
                    _ => _.SerialNo,
                    _ => _.Exception
                );




          });

        }
    }
}
