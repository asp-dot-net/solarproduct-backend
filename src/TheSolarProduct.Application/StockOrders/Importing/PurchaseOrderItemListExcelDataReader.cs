﻿using Abp.Localization.Sources;
using Abp.Localization;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.StockOrders.Importing.Dto;
using System.Linq;

namespace TheSolarProduct.StockOrders.Importing
{
    
    public class PurchaseOrderItemListExcelDataReader : NpoiExcelImporterBase<ImportPurchaseOrderItemDto>, IPurchaseOrderItemListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public PurchaseOrderItemListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
        }

        public List<ImportPurchaseOrderItemDto> GetPurchaseOrderItemFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportPurchaseOrderItemDto ProcessExcelRow(ISheet worksheet, int row)
        {
           
            var exceptionMessage = new StringBuilder();
            var PurchaseOrderItem = new ImportPurchaseOrderItemDto();

            try
            {

                worksheet.GetRow(row).Cells[0].SetCellType(CellType.String);
                PurchaseOrderItem.SerialNo = worksheet.GetRow(row).Cells[0]?.StringCellValue;
                worksheet.GetRow(row).Cells[1].SetCellType(CellType.String);
                PurchaseOrderItem.PalletNo = worksheet.GetRow(row).Cells[1]?.StringCellValue;

            }

            catch (System.Exception exception)
            {
                PurchaseOrderItem.Exception = exception.Message;
            }

            return PurchaseOrderItem;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }
    }
}
