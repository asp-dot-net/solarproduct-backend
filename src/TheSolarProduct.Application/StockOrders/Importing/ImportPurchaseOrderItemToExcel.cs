﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.Invoices.Importing;
using TheSolarProduct.Jobs.Importing;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.Notifications;
using TheSolarProduct.Storage;
using TheSolarProduct.StockOrder.Dtos;
using Abp.ObjectMapping;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.StockOrders.Importing.Dto;
using TheSolarProduct.QuickStocks;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.WholeSaleLeadActivityLogs;
using TheSolarProduct.WholeSaleLeads;
using Microsoft.Azure.KeyVault.Models;
using TheSolarProduct.StockOrders.SerialNoStatuses;

namespace TheSolarProduct.StockOrders.Importing
{

    public class ImportPurchaseOrderItemToExcel : BackgroundJob<ImportPurchaseOrderItemFromExcelArgs>, ITransientDependency
    {
        private readonly IPurchaseOrderItemListExcelDataReader _purchaseOrderItemListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly InvalidPurchaseOrderItemExporter _invalidPurchaseOrderItemExporter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<StockSerialNo> _stockSerialNoRepository;
        private readonly IRepository<Organizations.ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly IRepository<PurchaseOrderItem> _purchaseOrderItemRepository;
        private readonly IRepository<StockSerialNoLogHistory> _stockSerialNoLogHistoryRepository;

        public ImportPurchaseOrderItemToExcel(
            IPurchaseOrderItemListExcelDataReader IPurchaseOrderItemListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            InvalidPurchaseOrderItemExporter InvalidPurchaseOrderItemExporter,
            IAbpSession abpSession,
            IRepository<User, long> userRepository,
            IRepository<PurchaseOrderItem> purchaseOrderItemRepository,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
            IRepository<QuickStockActivityLog> quickStockActivityLogRepository,
            IRepository<Organizations.ExtendOrganizationUnit, long> extendOrganizationUnitRepository
            , IRepository<StockSerialNo> stockSerialNoRepository
            , IRepository<LeadActivityLog> leadactivityRepository
            , IRepository<StockSerialNoLogHistory> stockSerialNoLogHistoryRepository
            )
        {
            _purchaseOrderItemListExcelDataReader = IPurchaseOrderItemListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _invalidPurchaseOrderItemExporter = InvalidPurchaseOrderItemExporter;
            _abpSession = abpSession;
            _userRepository = userRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _env = env;
            _tenantRepository = tenantRepository;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _stockSerialNoRepository = stockSerialNoRepository;
            _leadactivityRepository = leadactivityRepository;
            _purchaseOrderItemRepository = purchaseOrderItemRepository;
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _stockSerialNoLogHistoryRepository = stockSerialNoLogHistoryRepository;
        }

        //[UnitOfWork]
        //public override void Execute(ImportPurchaseOrderItemFromExcelArgs args)
        //{


        //    var Installations = GetPurchaseOrderItemListFromExcelOrNull(args);
        //    if (Installations == null || !Installations.Any())
        //    {
        //        SendInvalidExcelNotification(args);
        //        return;
        //    }

        //    var duplicateSerialNumbersInFile = Installations
        //        .GroupBy(i => i.SerialNo.Trim().ToUpper())
        //        .Where(g => g.Count() > 1)
        //        .Select(g => g.Key)
        //        .ToList();

        //    if (duplicateSerialNumbersInFile.Any())
        //    {
        //        SendInvalidExcelNotification(args);
        //        return;
        //    }
        //    using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
        //    {
        //        var serialNumbers = Installations.Select(i => i.SerialNo.Trim().ToUpper()).ToList();
        //        var totalQuantity = _purchaseOrderItemRepository.GetAll()
        //            .Where(poi => poi.PurchaseOrderId == args.PurchaseOrderId && poi.ProductItemId == args.ProductItemId)
        //            .Select(poi => poi.Quantity)
        //            .FirstOrDefault();

        //        var serialNoCount = _stockSerialNoRepository.GetAll()
        //            .Where(sn => sn.PurchaseOrderId == args.PurchaseOrderId && sn.ProductItemId == args.ProductItemId)
        //            .Count();

        //        var pendingQuantity = totalQuantity - serialNoCount;

        //        if (pendingQuantity < serialNumbers.Count)
        //        {
        //            SendPendingQuantityExceededNotification(args);
        //            return;
        //        }

        //        var duplicateSerialNumbers = _stockSerialNoRepository.GetAll()
        //            .Where(sn => serialNumbers.Contains(sn.SerialNo) && sn.PurchaseOrderId == args.PurchaseOrderId && sn.ProductItemId == args.ProductItemId)
        //            .Select(sn => sn.SerialNo)
        //            .ToList();

        //        if (duplicateSerialNumbers.Any())
        //        {
        //            SendDuplicateSerialNotification(args);
        //            return;
        //        }

        //    }
        //    var invalidInstallations = new List<ImportPurchaseOrderItemDto>();

        //    foreach (var installation in Installations)
        //    {
        //        if (installation.CanBeImported())
        //        {
        //            try
        //            {
        //                AsyncHelper.RunSync(() => CreatePurchaseOrderItemAsync(installation, args));
        //            }
        //            catch (UserFriendlyException)
        //            {
        //                invalidInstallations.Add(installation);
        //            }
        //            catch (Exception)
        //            {
        //                invalidInstallations.Add(installation);
        //            }
        //        }
        //        else
        //        {
        //            invalidInstallations.Add(installation);
        //        }
        //    }

        //    using (var uow = _unitOfWorkManager.Begin())
        //    {
        //        using (CurrentUnitOfWork.SetTenantId(args.TenantId))
        //        {
        //            AsyncHelper.RunSync(() => ProcessImportPurchaseOrderItemResultAsync(args, invalidInstallations));


        //            var productItemName = _purchaseOrderItemRepository.GetAll().Include(poi => poi.ProductItemFk).Include(poi => poi.ProductItemFk.ProductTypeFk).Where(poi => poi.ProductItemId == args.ProductItemId).Select(poi => poi.ProductItemFk.Name).FirstOrDefault();

        //            var logMessage = $" {Installations.Count} serial numbers imported for {productItemName}";

        //            var quickStockLog = new QuickStockActivityLog();
        //            quickStockLog.Action = "Imported";
        //            quickStockLog.SectionId = 1;
        //            quickStockLog.ActionId = 75;
        //            quickStockLog.Type = "StockOrder";
        //            quickStockLog.PurchaseOrderId = args.PurchaseOrderId;
        //            quickStockLog.ActionNote = logMessage;
        //            quickStockLog.CreatorUserId = (int)args.User.UserId;
        //            _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);
        //        }
        //        uow.Complete();
        //    }
        //}
        [UnitOfWork]
        public override void Execute(ImportPurchaseOrderItemFromExcelArgs args)
        {

            //var fileObject = new BinaryObject(args.TenantId, args.FileBytes);
            //_binaryObjectManager.SaveAsync(fileObject);

            var WholeSaleLeads = GetPurchaseOrderItemListFromExcelOrNull(args);
            if (WholeSaleLeads == null || !WholeSaleLeads.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidWholeSaleLeads = new List<ImportPurchaseOrderItemDto>();

                foreach (var WholeSaleLead in WholeSaleLeads)
                {
                    if (WholeSaleLead.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreatePurchaseOrderItemAsync(WholeSaleLead, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            invalidWholeSaleLeads.Add(WholeSaleLead);
                        }
                        catch (Exception e)
                        {
                            invalidWholeSaleLeads.Add(WholeSaleLead);
                        }
                    }
                    else
                    {
                        invalidWholeSaleLeads.Add(WholeSaleLead);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportPurchaseOrderItemResultAsync(args, invalidWholeSaleLeads, WholeSaleLeads.Count));

                    }
                    uow.Complete();
                }
            }
        }


        private List<ImportPurchaseOrderItemDto> GetPurchaseOrderItemListFromExcelOrNull(ImportPurchaseOrderItemFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        return _purchaseOrderItemListExcelDataReader.GetPurchaseOrderItemFromExcel(file.Bytes);
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreatePurchaseOrderItemAsync(ImportPurchaseOrderItemDto input, ImportPurchaseOrderItemFromExcelArgs args)
        {

            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                var duplicateSerialNumbers = _stockSerialNoRepository.GetAll()
                    .Where(sn => sn.SerialNo == input.SerialNo && sn.PurchaseOrderId == args.PurchaseOrderId && sn.ProductItemId == args.ProductItemId);
                    
                if(!duplicateSerialNumbers.Any())
                {
                    var newRecData = new StockSerialNo
                    {
                        PurchaseOrderId = args.PurchaseOrderId,
                        ProductItemId = args.ProductItemId,
                        WarehouseLocationId = args.WarehouseLocationId,
                        SerialNo = input.SerialNo,
                        IsApp = false,
                        PalletNo = input.PalletNo,
                        CreatorUserId = (int)args.User.UserId,
                        SerialNoStatusId = 1,

                    };
                    //await _stockSerialNoRepository.InsertAsync(newRecData);
                    var stockSerialNoid = await _stockSerialNoRepository.InsertAndGetIdAsync(newRecData);
                    var SerialNoLogHistroy = new StockSerialNoLogHistory
                    {
                        StockSerialNoId = stockSerialNoid,
                        SerialNoStatusId = 1,
                        CreatorUserId= (int)args.User.UserId,   
                        ActionNote = input.SerialNo +" Serial No Imported By Excel"
                    };
                    await _stockSerialNoLogHistoryRepository.InsertAsync(SerialNoLogHistroy);
                    
                }
                

            }

        }

        private void SendInvalidExcelNotification(ImportPurchaseOrderItemFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToSerialNo", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private void SendDuplicateSerialNotification(ImportPurchaseOrderItemFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("The SerialNo Already exist", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }
        private void SendPendingQuantityExceededNotification(ImportPurchaseOrderItemFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("you can not import serial no Exceeded limit", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportPurchaseOrderItemResultAsync(ImportPurchaseOrderItemFromExcelArgs args,
          List<ImportPurchaseOrderItemDto> invalidInstallation,int Count)
        {
            if (invalidInstallation.Any())
            {
                var file = _invalidPurchaseOrderItemExporter.ExportToFile(invalidInstallation);
                await _appNotifier.SomeInstallationCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var productItemName = _purchaseOrderItemRepository.GetAll().Include(poi => poi.ProductItemFk).Include(poi => poi.ProductItemFk.ProductTypeFk).Where(poi => poi.ProductItemId == args.ProductItemId).Select(poi => poi.ProductItemFk.Name).FirstOrDefault();

                var logMessage = $" {Count} serial numbers imported for {productItemName}";

                QuickStockActivityLog quickStockLog = new QuickStockActivityLog
                {
                    Action = "Imported",
                    SectionId = 1,
                    ActionId = 75,
                    Type = "StockOrder",
                    PurchaseOrderId = args.PurchaseOrderId,
                    ActionNote = logMessage,
                    CreatorUserId = (int)args.User.UserId

                };

                await _quickStockActivityLogRepository.InsertAsync(quickStockLog);

                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllSerialNoImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
               
               
            }
        }
    }
}

