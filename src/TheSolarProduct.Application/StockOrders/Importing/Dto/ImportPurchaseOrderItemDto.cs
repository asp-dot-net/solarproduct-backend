﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.StockOrders.Importing.Dto
{
    public class ImportPurchaseOrderItemDto
    {
        public string SerialNo { get; set; }

        public string PalletNo { get; set; }
        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
