﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Invoices.Importing.Dto;
using TheSolarProduct.StockOrders.Importing.Dto;

namespace TheSolarProduct.StockOrders.Importing
{
    public interface IPurchaseOrderItemListExcelDataReader : ITransientDependency
    {
        List<ImportPurchaseOrderItemDto> GetPurchaseOrderItemFromExcel(byte[] fileBytes);
    }
}
