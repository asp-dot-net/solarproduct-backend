﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockOrderReceivedReport.Dtos;
using TheSolarProduct.StockOrderReceivedReport;
using TheSolarProduct.QuickStocks;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using System.Linq;
using System.Data;
using System.Linq.Dynamic.Core;
using Abp.Collections.Extensions;


namespace TheSolarProduct.StockOrders
{
    
    public class SerialNoHistoryReportAppService : TheSolarProductAppServiceBase, IStockOrderReceivedReportAppService
    {
        private readonly IRepository<PurchaseOrderItem> _PurchaseOrderItemRepository;
        private readonly IRepository<StockSerialNoLogHistory> _StockSerialNoLogHistoryRepository;
        private readonly IRepository<PurchaseOrder> _PurchaseOrderRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly ITimeZoneConverter _timeZoneConverter;

        public SerialNoHistoryReportAppService(
            IRepository<PurchaseOrderItem> PurchaseOrderItemRepository,
            IRepository<PurchaseOrder> PurchaseOrderRepository,
            IRepository<StockSerialNoLogHistory> StockSerialNoLogHistoryRepository,
            IRepository<DataVaultActivityLog> dataVaultActivityLogRepository,
            IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
            ITimeZoneConverter timeZoneConverter
            )
        {
            _PurchaseOrderItemRepository = PurchaseOrderItemRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _StockSerialNoLogHistoryRepository = StockSerialNoLogHistoryRepository;
            _PurchaseOrderRepository = PurchaseOrderRepository;
            _timeZoneConverter = timeZoneConverter;
        }
        public async Task<PagedResultDto<GetAllSerialNoHistoryReportDto>> GetAll(GetAllSerialNoHistoryReportInput input)
        {
            var SDate = _timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            var EDate = _timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);


            var StockSerialNoLog = _StockSerialNoLogHistoryRepository.GetAll()
                .Include(e => e.StockSerialNoFk)
                .Include(e => e.SerialNoStatusFk)
                 .Include(e => e.StockSerialNoFk.PurchaseOrderFk.PaymentTypeFk)
               
                .WhereIf(input.FilterName == "SerialNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.StockSerialNoFk.SerialNo.Contains(input.Filter))
                .WhereIf(input.FilterName == "ItemName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.StockSerialNoFk.PurchaseOrderFk.PaymentTypeFk.Name.Contains(input.Filter))
                .WhereIf(input.FilterName == "PalletNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.StockSerialNoFk.PalletNo.Contains(input.Filter))
                //.WhereIf(input.FilterName == "OrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.OrderNo.ToString().Contains(input.Filter))

                .WhereIf(input.SerialNoStatusIds != null && input.SerialNoStatusIds.Count() > 0, e => input.SerialNoStatusIds.Contains(e.SerialNoStatusId))

                .WhereIf(input.Datefilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                .WhereIf(input.Datefilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                ;

            var pagedAndFilteredStockSerialNoLog = StockSerialNoLog
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input);



            var moduleSummary = await StockSerialNoLog
     .GroupBy(x => x.StockSerialNoFk.ProductItemFk.ProductTypeId)
     .Select(g => new
     {
         ProductTypeId = g.Key,
         TotalQuantity = g.Count()
     })
     .ToListAsync();

            var summary = new GetAllSerialNoHistoryReportDto
            {
                ModuleSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 1)?.TotalQuantity ?? 0,
                InvertersSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 2)?.TotalQuantity ?? 0,
                MountingSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 3)?.TotalQuantity ?? 0,
                ElectricalSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 4)?.TotalQuantity ?? 0,
                BatterySummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 5)?.TotalQuantity ?? 0,
                OthersSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 6)?.TotalQuantity ?? 0,
                SerialNoHistoryReport = new List<GetAllSerialNoHistoryReport>()
            };


            summary.SerialNoHistoryReport = await pagedAndFilteredStockSerialNoLog.Select(o => new GetAllSerialNoHistoryReport

            {

                Id = o.Id,
                OrderNo=o.StockSerialNoFk.PurchaseOrderFk.OrderNo,
                CatogoryName =o.StockSerialNoFk.ProductItemFk.ProductTypeFk.Name,
                ItemName = o.StockSerialNoFk.ProductItemFk.Name,
                SerialNo = o.StockSerialNoFk.SerialNo,
                PalletNo = o.StockSerialNoFk.PalletNo,
                InstallerName="",
                NameOrConsignmentNo="",
                ModuleName=o.SerialNoStatusFk.Status,
                Location=o.StockSerialNoFk.WarehouseLocationFk.location,
                DeductBy="",
                Message=o.ActionNote,
            })
       .ToListAsync();


            var totalCount = await StockSerialNoLog.CountAsync();




            return new PagedResultDto<GetAllSerialNoHistoryReportDto>(
                totalCount,
                new List<GetAllSerialNoHistoryReportDto> { summary }
            );
        }


    }
}
