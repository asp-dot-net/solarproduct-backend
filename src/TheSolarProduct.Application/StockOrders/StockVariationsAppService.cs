﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Exporting;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockOrder;
using TheSolarProduct.Jobs;

namespace TheSolarProduct.StockOrders
{
    public class StockVariationsAppService : TheSolarProductAppServiceBase, IStockVariationsAppService
    {
        private readonly IRepository<StockVariation> _variationRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public StockVariationsAppService(IRepository<StockVariation> variationRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _variationRepository = variationRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task CreateOrEdit(StockVariationDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(StockVariationDto input)
        {
            var variation = ObjectMapper.Map<StockVariation>(input);


            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 77;
            dataVaultLog.ActionNote = "Stock Variations Created : " + input.Name;
            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _variationRepository.InsertAsync(variation);
        }

        protected virtual async Task Update(StockVariationDto input)
        {
            var variation = await _variationRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 77;
            dataVaultLog.ActionNote = "Stock Variations Updated : " + variation.Name;

            dataVaultLog.IsInventory = true;
            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != variation.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = variation.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Action != variation.Action)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Action";
                history.PrevValue = variation.Action;
                history.CurValue = input.Action;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }


            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, variation);
        }

        public async Task Delete(EntityDto input)
        {
            var Name = _variationRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 77;
            dataVaultLog.ActionNote = "Stock Variations Deleted : " + Name;

            dataVaultLog.IsInventory = true;
            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _variationRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<GetStockVariationForViewDto>> GetAll(GetAllVariationsInput input)
        {
            var filteredVariations = _variationRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Action.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ActionFilter), e => e.Action == input.ActionFilter);

            var pagedAndFilteredVariations = filteredVariations
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var variations = from o in pagedAndFilteredVariations
                             select new GetStockVariationForViewDto()
                             {
                                 StockVariationDto = new StockVariationDto
                                 {
                                     Name = o.Name,
                                     Action = o.Action,
                                     Id = o.Id
                                 }
                             };

            var totalCount = await filteredVariations.CountAsync();

            return new PagedResultDto<GetStockVariationForViewDto>(
                totalCount,
                await variations.ToListAsync()
            );
        }

        public async Task<StockVariationDto> GetVariationForEdit(EntityDto input)
        {
            var variation = await _variationRepository.GetAsync(input.Id);

            var output =  ObjectMapper.Map<StockVariationDto>(variation);

            return output;
        }

        public async Task<GetStockVariationForViewDto> GetVariationForView(int id)
        {
            var variation = await _variationRepository.GetAsync(id);

            var output = new GetStockVariationForViewDto { StockVariationDto = ObjectMapper.Map<StockVariationDto>(variation) };

            return output;
        }
    }
}
