﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using AutoMapper.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting.Drawing;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Jobs;
using TheSolarProduct.StockOrder;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockOrderReceivedReport;
using TheSolarProduct.StockOrderReceivedReport.Dtos;

namespace TheSolarProduct.StockOrders
{
    public class StockOrderReceivedReportAppService : TheSolarProductAppServiceBase, IStockOrderReceivedReportAppService
    {
        private readonly IRepository<PurchaseOrderItem> _PurchaseOrderItemRepository;
        private readonly IRepository<PurchaseOrder> _PurchaseOrderRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public StockOrderReceivedReportAppService(IRepository<PurchaseOrderItem> PurchaseOrderItemRepository, IRepository<PurchaseOrder> PurchaseOrderRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _PurchaseOrderItemRepository = PurchaseOrderItemRepository;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;
            _dbcontextprovider = dbcontextprovider;
            _PurchaseOrderRepository = PurchaseOrderRepository;
        }
        public async Task<PagedResultDto<GetAllStockOrderReceivedReportDto>> GetAll(GetAllStockOrderReceivedReportInput input)
        {
            var purchaseOrderIds = _PurchaseOrderRepository.GetAll()
        .Include(e => e.StockOrderStatusFk)
        .Include(e => e.WarehouseLocationFk)
        .WhereIf(input.FilterName == "ContainerNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ContainerNo.Contains(input.Filter))
        .WhereIf(input.FilterName == "ManualOrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ManualOrderNo.Contains(input.Filter))
        .WhereIf(input.StockOrderStatusIds != null && input.StockOrderStatusIds.Any(), e => input.StockOrderStatusIds.Contains(e.StockOrderStatusFk.Id))
        //.WhereIf(input.StateFitlerIds != null && input.StateFitlerIds.Any(), e => input.StateFitlerIds.Contains(e.WarehouseLocationFk.Id))
        .WhereIf(input.PurchaseCompanyIdFiletr > 0, e => e.PurchaseCompanyId == input.PurchaseCompanyIdFiletr)
        .WhereIf(input.OrderTypeIdFiletr > 0, e => e.StockFromId == input.OrderTypeIdFiletr)
        .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= input.StartDate.Value.Date)
        .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= input.EndDate.Value.Date)
        .Where(e => e.OrganizationId == input.OrganizationId)
        .Select(p => p.Id)
        .ToList();


            var stockOrderReceivedReport = _PurchaseOrderItemRepository
     .GetAll()
     .Include(e => e.ProductItemFk)
     .Include(e => e.PurchaseOrderFk.WarehouseLocationFk)
     .Where(item => purchaseOrderIds.Contains((int)item.PurchaseOrderId)) 
     .WhereIf(input.FilterName == "ItemName" && !string.IsNullOrWhiteSpace(input.Filter),
              e => e.ProductItemFk.Name.Contains(input.Filter))
     .WhereIf(input.FilterName == "ModelNo" && !string.IsNullOrWhiteSpace(input.Filter),
              e => e.ModelNo.Contains(input.Filter))
     .WhereIf(input.ProductTypeIdFiletr > 0,
              e => e.ProductItemFk.ProductTypeId == input.ProductTypeIdFiletr)
     .Select(item => new
     {
         ItemName = item.ProductItemFk.Name,
         Quantity = item.Quantity ?? 0,
         ModelNo = item.ModelNo,
         Location = item.PurchaseOrderFk.WarehouseLocationFk.location,
         LocationId = item.PurchaseOrderFk.WarehouseLocationFk.Id,
         PurchaseOrderId = item.PurchaseOrderId,
         Size = item.ProductItemFk.Size
     });



            var groupedResult = stockOrderReceivedReport
                .GroupBy(item => new { item.ItemName, item.ModelNo, item.Location ,item.LocationId, item.Size })
                .Select(group => new
                {
                    ItemName = group.Key.ItemName,
                    ModelNo = group.Key.ModelNo,
                    Location = group.Key.Location,
                    LocationId=group.Key.LocationId,
                    Size=group.Key.Size,
                    TotalQuantity = group.Sum(x => x.Quantity) 
                })
                .ToList() 
                .GroupBy(item => new { item.ItemName, item.ModelNo,item.Size}) 
                .Select(group => new
                {
                    ItemName = group.Key.ItemName,
                    ModelNo = group.Key.ModelNo,
                    Brisbane = (input.StateFitlerIds?.Contains(1) == true)? group.FirstOrDefault(x => x.LocationId == 1)?.TotalQuantity ?? 0: 0,
                    Melbourne = (input.StateFitlerIds?.Contains(2) == true) ? group.FirstOrDefault(x => x.LocationId == 2)?.TotalQuantity ?? 0 : 0,
                    Sydeny = (input.StateFitlerIds?.Contains(3) == true) ? group.FirstOrDefault(x => x.LocationId == 3)?.TotalQuantity ?? 0 : 0,
                    Perth = (input.StateFitlerIds?.Contains(4) == true) ? group.FirstOrDefault(x => x.LocationId == 4)?.TotalQuantity ?? 0 : 0,
                    Darwin = (input.StateFitlerIds?.Contains(5) == true) ? group.FirstOrDefault(x => x.LocationId == 5)?.TotalQuantity ?? 0 : 0,
                    Hobart = (input.StateFitlerIds?.Contains(6) == true) ? group.FirstOrDefault(x => x.LocationId == 6)?.TotalQuantity ?? 0 : 0,
                    Adelaide = (input.StateFitlerIds?.Contains(7) == true) ? group.FirstOrDefault(x => x.LocationId == 7)?.TotalQuantity ?? 0 : 0,
                    Canberra = (input.StateFitlerIds?.Contains(8) == true) ? group.FirstOrDefault(x => x.LocationId == 8)?.TotalQuantity ?? 0 : 0,
                    Size= group.Key.Size,
                    Total =
        ((input.StateFitlerIds?.Contains(1) == true) ? group.FirstOrDefault(x => x.LocationId == 1)?.TotalQuantity ?? 0 : 0) +
        ((input.StateFitlerIds?.Contains(2) == true) ? group.FirstOrDefault(x => x.LocationId == 2)?.TotalQuantity ?? 0 : 0) +
        ((input.StateFitlerIds?.Contains(3) == true) ? group.FirstOrDefault(x => x.LocationId == 3)?.TotalQuantity ?? 0 : 0) +
        ((input.StateFitlerIds?.Contains(4) == true) ? group.FirstOrDefault(x => x.LocationId == 4)?.TotalQuantity ?? 0 : 0) +
        ((input.StateFitlerIds?.Contains(5) == true) ? group.FirstOrDefault(x => x.LocationId == 5)?.TotalQuantity ?? 0 : 0) +
        ((input.StateFitlerIds?.Contains(6) == true) ? group.FirstOrDefault(x => x.LocationId == 6)?.TotalQuantity ?? 0 : 0) +
        ((input.StateFitlerIds?.Contains(7) == true) ? group.FirstOrDefault(x => x.LocationId == 7)?.TotalQuantity ?? 0 : 0) +
        ((input.StateFitlerIds?.Contains(8) == true) ? group.FirstOrDefault(x => x.LocationId == 8)?.TotalQuantity ?? 0 : 0)

                })
                .OrderBy(e => e.ItemName)
                .ToList();


            
            var summaryResult = stockOrderReceivedReport
                .GroupBy(item => item.Location)
                .Select(group => new
                {
                    Location = group.Key,
                    TotalQuantity = group.Sum(x => x.Quantity)
                })
                .ToList();

            
            var summary = new GetAllStockOrderReceivedReportDto
            {
                BrisbaneSummary = summaryResult.FirstOrDefault(x => x.Location == "Brisbane")?.TotalQuantity ?? 0,
                MelbourneSummary = summaryResult.FirstOrDefault(x => x.Location == "Melbourne")?.TotalQuantity ?? 0,
                SydenySummary = summaryResult.FirstOrDefault(x => x.Location == "Sydney")?.TotalQuantity ?? 0,
                CanberraSummary = summaryResult.FirstOrDefault(x => x.Location == "Canberra")?.TotalQuantity ?? 0,
                DarwinSummary = summaryResult.FirstOrDefault(x => x.Location == "Darwin")?.TotalQuantity ?? 0,
                HobartSummary = summaryResult.FirstOrDefault(x => x.Location == "Hobart")?.TotalQuantity ?? 0,
                AdelaideSummary = summaryResult.FirstOrDefault(x => x.Location == "Adelaide")?.TotalQuantity ?? 0,
                PerthSummary = summaryResult.FirstOrDefault(x => x.Location == "Perth")?.TotalQuantity ?? 0,
                StockOrderReceivedReport = new List<GetAllStockOrderReceivedReport>(),
            };

            
            summary.StockOrderReceivedReport = groupedResult
                .Select(o => new GetAllStockOrderReceivedReport
                {
                    ProductItem = o.ItemName,
                    ModelNo = o.ModelNo,
                    Size = o.Size,
                    Brisbane = o.Brisbane,
                    Melbourne = o.Melbourne,
                    Sydeny = o.Sydeny,
                    Canberra = o.Canberra,
                    Darwin = o.Darwin,
                    Hobart = o.Hobart,
                    Adelaide = o.Adelaide,
                    Perth = o.Perth,
                    Total = o.Total,
                })
                .ToList();


            var totalCount = groupedResult.Count; 



            return new PagedResultDto<GetAllStockOrderReceivedReportDto>(
                totalCount,
                new List<GetAllStockOrderReceivedReportDto> { summary }
            );
        }


    }
}
