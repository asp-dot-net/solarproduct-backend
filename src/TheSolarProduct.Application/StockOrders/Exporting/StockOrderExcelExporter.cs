﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using Telerik.Reporting;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Migrations;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockOrdersStatus.Exporting;
using TheSolarProduct.StockOrderStatus.Dtos;
using TheSolarProduct.StockPaymentTrackers.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.StockOrders.Exporting
{
    public class StockOrderExcelExporter : NpoiExcelExporterBase, IStockOrderExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StockOrderExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
            base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllStockOrderOutputDto> PurchaseOrder)
        {
            return CreateExcelPackage(
                "PurchaseOrder.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("PurchaseOrder");

                    //int[] columnWidths = { 10, 19, 18, 19, 11, 11, 18, 14, 14, 14, 16, 11 };

                    //for (int i = 0; i < columnWidths.Length; i++)
                    //{
                    //    sheet.SetColumnWidth(i, columnWidths[i] * 256); 
                    //}
                   
                    AddHeader(
                        sheet,
                        "OrderNo",
                        "StockOrderStatus",
                        "VendorInoviceNo",
                        "PurchaseCompany",
                        "Location",
                        "OrderDate",
                        "Exp.DeliveryDate",
                        "ManualOrderNo",
                        "ContainerNo",
                        "Vendor",
                        "PaymentType",
                        "PaymentStatus",
                        "Currency",
                        "Qty",
                        "PendingQty",
                        "AmountWithGst",
                        "AmountWithOutGst"


                    );
                    AddObjects(
                        sheet,
                        2, 
                        PurchaseOrder,
                        _ => _.OrderNo,
                        _ => _.StockOrderStatus,
                        _ => _.VendorInoviceNo,
                        _ => _.PurchaseCompany,
                        _ => _.WarehouseLocation,
                        _ => _.OrderDate,
                        _ => _.ExpDelivaryDate,
                        _ => _.ManualOrderNo,
                        _ => _.ContainerNo,
                        _ => _.Vendor,
                        _ => _.PaymentType,
                        _ => _.PaymentStatus,
                        _ => _.Currency,
                        _ => _.Qty,
                        _ => _.PendingQty,
                        _ => _.AmountWithGst,
                        _ => _.AmountWithOutGst
                    );
                    for (var i = 1; i <= PurchaseOrder.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[13], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[14], "0.00");
                       
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[15], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[16], "0.00");
                    }
                    for (var i = 0; i < 9; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                }
            );
        }

        public FileDto PaymentTrackerExportToFile(List<GetAllStockPaymentTrackerOutputDto> PurchaseOrder)
        {
            return CreateExcelPackage(
               "StockPaymentTracker.xlsx",
               excelPackage =>
               {
                   var sheet = excelPackage.CreateSheet("StockPaymentTracker");

                   //int[] columnWidths = { 10, 19, 18, 19, 11, 11, 18, 14, 14, 14, 16, 11 };

                   //for (int i = 0; i < columnWidths.Length; i++)
                   //{
                   //    sheet.SetColumnWidth(i, columnWidths[i] * 256); 
                   //}

                   AddHeader(
                       sheet,
                       "OrderNo",
                       "InoviceNo",
                       "Location",
                       "Order Date",
                       "Manual OrderNo",
                       "Delivery Date",
                       "Payment Method",
                       "Currency",
                       "GST Type",
                       "Qty",
                       "Amount",
                       "Paid Amount",
                       "Remaining Amount"
                   );
                   AddObjects(
                       sheet,
                       2,
                       PurchaseOrder,
                       _ => _.OrderNo,
                       _ => _.VendorInoviceNo,
                       _ => _.WarehouseLocation,
                       _ => _.OrderDate,
                       _ => _.ManualOrderNo,
                       _ => _.DeliveryDate,
                       _ => _.PaymentMethod,
                       _ => _.Currency,
                       _ => _.GSTType,
                       _ => _.QTY,
                       _ => _.Amount,
                       _ => _.PaidAmount,
                       _ => _.RemAmount
                   );
                   for (var i = 0; i < 9; i++)
                   {
                       sheet.AutoSizeColumn(i);
                   }
                   for (var i = 1; i <= PurchaseOrder.Count; i++)
                   {
                       SetCellDataFormat(sheet.GetRow(i).Cells[3], "dd/mm/yyyy");
                       SetCellDataFormat(sheet.GetRow(i).Cells[5], "dd/mm/yyyy");
                       SetintCellDataFormat(sheet.GetRow(i).Cells[9], "00");
                       SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "0.00");
                       SetdecimalCellDataFormat(sheet.GetRow(i).Cells[11], "0.00");
                       SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");
                   }
               }
           );
        }
    }
}
