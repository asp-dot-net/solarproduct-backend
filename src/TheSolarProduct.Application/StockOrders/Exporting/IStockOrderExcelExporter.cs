﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.StockOrder.Dtos;
using TheSolarProduct.StockPaymentTrackers.Dtos;

namespace TheSolarProduct.StockOrders.Exporting
{
    public interface IStockOrderExcelExporter
    {
        FileDto ExportToFile(List<GetAllStockOrderOutputDto> PurchaseOrder);

        FileDto PaymentTrackerExportToFile(List<GetAllStockPaymentTrackerOutputDto> PurchaseOrder);
        
    }
}
