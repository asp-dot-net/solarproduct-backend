﻿using Abp.Application.Services.Dto;
using Abp.Data;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Timing;
using TheSolarProduct.StockOrder;
using Abp.Authorization.Users;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.DataVaults;
using TheSolarProduct.StockOrder.Dtos;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Collections.Extensions;
using TheSolarProduct.StockOrders.Exporting;
using Abp.Timing.Timezone;
using Telerik.Reporting;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.Organizations;
using TheSolarProduct.QuickStockApp.StockOrder.Dto;
using TheSolarProduct.LeadActions;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Leads.Dtos;
using TheSolarProduct.Leads;
using Microsoft.Azure.KeyVault.Models;
using TheSolarProduct.WholeSaleLeads;
using TheSolarProduct.Vendors;
using TheSolarProduct.PurchaseCompanies;
using TheSolarProduct.DeliveryTypes;
using TheSolarProduct.PaymentStatuses;
using TheSolarProduct.PaymentTypes;
using TheSolarProduct.Jobs;
using TheSolarProduct.IncoTerms;
using TheSolarProduct.StockFroms;
using TheSolarProduct.Currencies;
using TheSolarProduct.StockOrderFors;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.PurchaseDocumentListes;
using TheSolarProduct.PurchaseDocumentLists.Dtos;
using TheSolarProduct.Quotations.Dtos;
using Hangfire.Common;
using TheSolarProduct.Promotions.Dtos;
using TheSolarProduct.Promotions;
using TheSolarProduct.CheckApplications;


namespace TheSolarProduct.StockOrders
{
    public class StockOrderAppService : TheSolarProductAppServiceBase, IStockOrderAppService

    {
        private readonly IStockOrderExcelExporter _stockOrderExcelExporter;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<PurchaseOrderItem> _purchaseOrderItemRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _organizationRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IRepository<LeadAction, int> _lookup_leadActionRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<QuickStockActivityLog> _quickStockActivityLogRepository;
        private readonly IRepository<QuickStockSection> _sectionRepository;
        private readonly IRepository<QuickStockActivityLogHistory> _purchaseOrderHistoryRepository;
        private readonly IRepository<Vendor> _vendorReposiorty;
        private readonly IRepository<PurchaseCompany> _purchaseCompanyReposiorty;
        private readonly IRepository<StockOrderStatuses.StockOrderStatus> _stockOrderStatusReposiorty;
        private readonly IRepository<DeliveryType> _deliveryTypeReposiorty;
        private readonly IRepository<PaymentStatus> _paymentStatusRepository;
        private readonly IRepository<PaymentType> _paymentTypeRepository;
        private readonly IRepository<Warehouselocation> _warehouselocationRepository;
        private readonly IRepository<IncoTerm> _IncoTermRepository;
        private readonly IRepository<StockFrom> _stockfromsRepository;
        private readonly IRepository<Currency> _currencyRepository;
        private readonly IRepository<StockOrderFor> _stockorderforRepository;
        private readonly IRepository<StockSerialNo> _stcokSerialnoRepository;
        private readonly IRepository<PurchaseDocumentList> _purchasedocumentListRepository;
        private readonly IRepository<PurchaseOrderDocument> _purchasedocumentRepository;
        private readonly IRepository<StockOrderVariation> _stockOrderVariationRepository;
        private readonly IRepository<StockVariation> _stockVariationRepository;

        public StockOrderAppService(
              IRepository<PurchaseOrder> purchaseOrderRepository,
              IRepository<PurchaseOrderItem> purchaseOrderItemRepository,
              IRepository<ExtendOrganizationUnit, long> organizationRepository,
              ITimeZoneConverter timeZoneConverter,
              IStockOrderExcelExporter IStockOrderExcelExporter,
              IRepository<UserTeam> userTeamRepository,
              IRepository<UserRole, long> userRoleRepository,
              IRepository<Role> roleRepository,
              IRepository<QuickStockActivityLog> quickStockActivityLogRepository,
              IRepository<QuickStockActivityLogHistory> purchaseOrderHistoryRepository,
              IDbContextProvider<TheSolarProductDbContext> dbcontextprovider,
              IRepository<LeadAction, int> lookup_leadActionRepository,
              IRepository<User, long> userRepository,
              IRepository<QuickStockSection> sectionRepository,
              IRepository<Vendor> vendorReposiortory,
              IRepository<PurchaseCompany> purchaseCompanyReposiorty,
              IRepository<StockOrderStatuses.StockOrderStatus> stockOrderStatusReposiorty,
              IRepository<DeliveryType> deliveryTypeReposiorty,
              IRepository<PaymentStatus> paymentStatusRepository,
              IRepository<PaymentType> paymentTypeRepository,
              IRepository<Warehouselocation> warehouselocationRepository,
              IRepository<IncoTerm> IncoTermRepository,
              IRepository<StockFrom> stockfromsRepository,
              IRepository<Currency> currencyRepository,
              IRepository<StockOrderFor> stockorderforRepository,
              IRepository<StockSerialNo> stcokSerialnoRepository,
              IRepository<PurchaseDocumentList> purchasedocumentListRepository,
              IRepository<PurchaseOrderDocument> purchasedocumentRepository,
              IRepository<StockOrderVariation> stockOrderVariationRepository,
              IRepository<StockVariation> stockVariationRepository
              )
        {
            _purchaseOrderRepository = purchaseOrderRepository;
            _purchaseOrderItemRepository = purchaseOrderItemRepository;
            _organizationRepository = organizationRepository;
            _timeZoneConverter = timeZoneConverter;
            _stockOrderExcelExporter = IStockOrderExcelExporter;
            _userTeamRepository = userTeamRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _quickStockActivityLogRepository = quickStockActivityLogRepository;
            _purchaseOrderHistoryRepository = purchaseOrderHistoryRepository;
            _dbcontextprovider = dbcontextprovider;
            _lookup_leadActionRepository = lookup_leadActionRepository;
            _userRepository = userRepository;
            _sectionRepository = sectionRepository;
            _vendorReposiorty = vendorReposiortory;
            _purchaseCompanyReposiorty = purchaseCompanyReposiorty;
            _stockOrderStatusReposiorty = stockOrderStatusReposiorty;
             _deliveryTypeReposiorty= deliveryTypeReposiorty;
            _paymentStatusRepository = paymentStatusRepository;
            _paymentTypeRepository = paymentTypeRepository;
             _warehouselocationRepository= warehouselocationRepository;
            _IncoTermRepository = IncoTermRepository;
            _stockfromsRepository = stockfromsRepository;
            _currencyRepository = currencyRepository;
            _stockorderforRepository = stockorderforRepository;
            _stcokSerialnoRepository = stcokSerialnoRepository;
            _purchasedocumentListRepository= purchasedocumentListRepository;
            _purchasedocumentRepository= purchasedocumentRepository;
            _stockOrderVariationRepository = stockOrderVariationRepository;
            _stockVariationRepository = stockVariationRepository;
        }
        public async Task<PagedResultDto<GetAllStockOrderDto>> GetAll(GetAllStockOerderInput input)
        {
            var SDate = _timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            var EDate = _timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

           
            var PurchaseOrder = _purchaseOrderRepository.GetAll()
                .Include(e => e.PurchaseCompanyFk)
                .Include(e => e.StockOrderForFk)
                .Include(e => e.StockFromFk)
                .Include(e => e.PaymentTypeFk)
                .Include(e => e.PaymentStatusFk)
                .Include(e => e.WarehouseLocationFk)
                .Include(e => e.CurrencyFk)
                .Include(e => e.DeliveryTypeFk)
                .Include(e => e.StockOrderStatusFk)
                .Include(e => e.TransportCompanyFk)
                .Include(e => e.VendorFK)
                .WhereIf(input.FilterName == "ContainerNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ContainerNo.Contains(input.Filter))
                .WhereIf(input.FilterName == "ManualOrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ManualOrderNo.Contains(input.Filter))
                .WhereIf(input.FilterName == "VendorInoviceNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.VendorInoviceNo.Contains(input.Filter))
                .WhereIf(input.FilterName == "OrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.OrderNo.ToString().Contains(input.Filter))
                .WhereIf(input.StockOrderStatusIds != null && input.StockOrderStatusIds.Count() > 0, e => input.StockOrderStatusIds.Contains(e.StockOrderStatusFk.Id))
                .WhereIf(input.WarehouseLocationIdFiletr != null && input.WarehouseLocationIdFiletr.Count() > 0, e => input.WarehouseLocationIdFiletr.Contains(e.WarehouseLocationFk.Id))
                .WhereIf(input.PaymentStatusIdFiletr > 0, e => e.PaymentStatusId == input.PaymentStatusIdFiletr)
                .WhereIf(input.PaymentTypeIdFiletr != null && input.PaymentTypeIdFiletr.Count() > 0, e => input.PaymentTypeIdFiletr.Contains(e.PaymentTypeFk.Id))
                .WhereIf(input.VendorIdFiletr > 0, e => e.VendorId == input.VendorIdFiletr)
                .WhereIf(input.GstTypeIdFilter > 0, e => e.GSTType == input.GstTypeIdFilter)
                .WhereIf(input.DeliveryTypeIdFiletr > 0, e => e.DeliveryTypeId == input.DeliveryTypeIdFiletr)
                .WhereIf(input.StockOrderIdFiletr > 0, e => e.StockOrderForId == input.StockOrderIdFiletr)
                .WhereIf(input.TransportCompanyFilter > 0, e => e.TransportCompanyId == input.TransportCompanyFilter)
                .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                .Where(e => e.OrganizationId == input.OrganizationId);

            var pagedAndFilteredpurchaseorder = PurchaseOrder
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input);
            var purchaseOrderIds = await pagedAndFilteredpurchaseorder.Select(o => o.Id).ToListAsync();

            var moduleSummary = await _purchaseOrderItemRepository.GetAll()
                .Include(e => e.ProductItemFk)
                .Where(e => purchaseOrderIds.Contains((int)(int?)e.PurchaseOrderId)) 
                .GroupBy(e => e.ProductItemFk.ProductTypeId)
                .Select(g => new
                {
                    ProductTypeId = g.Key,
                    TotalQuantity = g.Sum(e => e.Quantity)
                })
                .ToListAsync();


            var summary = new GetAllStockOrderDto
            {
                ModuleSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 1)?.TotalQuantity ?? 0,
                InvertersSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 2)?.TotalQuantity ?? 0,
                MountingSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 3)?.TotalQuantity ?? 0,
                ElectricalSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 4)?.TotalQuantity ?? 0,
                BatterySummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 5)?.TotalQuantity ?? 0,
                OthersSummary = moduleSummary.FirstOrDefault(x => x.ProductTypeId == 6)?.TotalQuantity ?? 0,
                StockOrderList = new List<GetAllStockOrderOutputDto>() 
            };

            
            summary.StockOrderList = await pagedAndFilteredpurchaseorder.Select(o => new GetAllStockOrderOutputDto
            
            {
                
                Id = o.Id,
                OrderNo = o.OrderNo,
                OrganizationId = o.OrganizationId,
                VendorInoviceNo = o.VendorInoviceNo,
                Vendor = o.VendorFK.CompanyName,
                PurchaseCompany = o.PurchaseCompanyFk.Name,
                ManualOrderNo = o.ManualOrderNo,
                ContainerNo = o.ContainerNo,
                StockOrderStatus = o.StockOrderStatusFk.Name,
                PaymentType = o.PaymentTypeFk.Name,
                PaymentStatus = o.PaymentStatusFk.Name,
                WarehouseLocation = o.WarehouseLocationFk.location,
                Currency = o.CurrencyFk.Name,
                OrderDate = o.CreationTime,
                Qty = _purchaseOrderItemRepository.GetAll().Where(e => e.PurchaseOrderId == o.Id).Sum(e => e.Quantity),
                isSubmit = o.IsSubmitDate != null,
                PendingQty = _purchaseOrderItemRepository.GetAll().Where(e => e.PurchaseOrderId == o.Id).Sum(e => e.Quantity) -_stcokSerialnoRepository.GetAll().Where(e => e.PurchaseOrderId == o.Id).Count()
                 }).ToListAsync();

            
            var totalCount = await PurchaseOrder.CountAsync();

            
            return new PagedResultDto<GetAllStockOrderDto>
                (
                totalCount,
                new List<GetAllStockOrderDto> { summary }
                );
        }

        public async Task<long> GetLastOrderNumber(int organizationId)
        {
            var orders = await _purchaseOrderRepository.GetAll().OrderByDescending(e => e.Id).Select(e => e.OrderNo).FirstOrDefaultAsync();
           
            if (orders == 0)
            {
                var organization = await _organizationRepository.GetAll()
                    .Where(org => org.Id == organizationId)
                    .Select(org => org.ProjectOrderNo)
                    .FirstOrDefaultAsync();

                return (long)organization ;
            }

            return orders +1 ;
        }
        public async Task<int?> GetCreditDays(int vendorId)
        {
            var vendor = await _vendorReposiorty.FirstOrDefaultAsync(v => v.Id == vendorId);
            return vendor?.CreditDays;
        }
        public async Task<GetStockOrderForEditOutput> GetStockOrderForEdit(EntityDto input)
        {
            var purchaseOrder = await _purchaseOrderRepository.GetAsync(input.Id);

            var purchaseOrderDto = ObjectMapper.Map<CreateOrEditStockOrderDto>(purchaseOrder);

        
            var purchaseOrderItem = await _purchaseOrderItemRepository.GetAll().Include(e => e.ProductItemFk).Where(e => e.PurchaseOrderId == input.Id).ToListAsync();
            var Variations = await _stockOrderVariationRepository.GetAll().Where(e => e.PurchaseOrderId == input.Id).ToListAsync();

            purchaseOrderDto.PurchaseOrderInfo = new List<PurchaseOrderItemDto>();
            foreach (var item in purchaseOrderItem)
            {
                var purchaseOrderItems = new PurchaseOrderItemDto();
                purchaseOrderItems.Id = item.Id;
                purchaseOrderItems.ProductTypeId = item.ProductItemFk.ProductTypeId;
                purchaseOrderItems.ProductItemId = item.ProductItemId;
                purchaseOrderItems.ProductItem = item.ProductItemFk.Name;
                purchaseOrderItems.UnitRate = item.UnitRate;
                purchaseOrderItems.EstRate = item.EstRate;
                purchaseOrderItems.Amount = item.Amount;
                purchaseOrderItems.Quantity = item.Quantity;
                purchaseOrderItems.PricePerWatt = item.PricePerWatt;
                purchaseOrderItems.ModelNo = item.ModelNo;
                purchaseOrderItems.ExpiryDate = item.ExpiryDate;
                purchaseOrderDto.PurchaseOrderInfo.Add(purchaseOrderItems);
            }
            purchaseOrderDto.StockOrderVariationDto = new List<CreateOrEditStockOrderVariation>();
            foreach (var item in Variations)
            {
                var variation = ObjectMapper.Map<CreateOrEditStockOrderVariation>(item);
                purchaseOrderDto.StockOrderVariationDto.Add(variation);

            }


            var output = new GetStockOrderForEditOutput
            {
                StockOrder = purchaseOrderDto,
                Vendor = purchaseOrderDto.VendorId > 0 ? _vendorReposiorty.GetAll().Where(e => e.Id == purchaseOrderDto.VendorId).FirstOrDefault().CompanyName : ""
        };

           
            return output;
        }
        public async Task CreateOrEdit(CreateOrEditStockOrderDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Update(CreateOrEditStockOrderDto input)
        {
            var purchaseOrder = await _purchaseOrderRepository.FirstOrDefaultAsync((int)input.Id);
            //input.PaymentDueDate = _timeZoneConverter.Convert(input.PaymentDueDate, (int)AbpSession.TenantId);
            //input.ETA = _timeZoneConverter.Convert(input.ETA, (int)AbpSession.TenantId);
            //input.ETD = _timeZoneConverter.Convert(input.ETD, (int)AbpSession.TenantId);
            //input.TargetArrivalDate = _timeZoneConverter.Convert(input.TargetArrivalDate, (int)AbpSession.TenantId);

            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Updated";
            quickStockLog.SectionId = 1;
            quickStockLog.ActionId = 69;
            quickStockLog.PurchaseOrderId= purchaseOrder.Id;
            quickStockLog.ActionNote = "Purchase Order Updated";
            quickStockLog.Type = "Purchase Order";
            var quickStockLogId = await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            var List = new List<QuickStockActivityLogHistory>();
            
            if (input.VendorId != purchaseOrder.VendorId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "Vendor";
                history.PrevValue = purchaseOrder.VendorId > 0 ? _vendorReposiorty.GetAll().Where(e => e.Id == purchaseOrder.VendorId).FirstOrDefault().CompanyName : "";
                history.CurValue = input.VendorId > 0 ? _vendorReposiorty.GetAll().Where(e => e.Id == input.VendorId).FirstOrDefault().CompanyName : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                
                List.Add(history);
            }
            if (input.PurchaseCompanyId != purchaseOrder.PurchaseCompanyId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "PurchaseCompany";
                history.PrevValue = purchaseOrder.PurchaseCompanyId > 0 ? _purchaseCompanyReposiorty.GetAll().Where(e => e.Id == purchaseOrder.PurchaseCompanyId).FirstOrDefault().Name : "";
                history.CurValue = input.PurchaseCompanyId > 0 ? _purchaseCompanyReposiorty.GetAll().Where(e => e.Id == input.PurchaseCompanyId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.ManualOrderNo != purchaseOrder.ManualOrderNo)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "ManualOrderNo";
                history.PrevValue = purchaseOrder.ManualOrderNo;
                history.CurValue = input.ManualOrderNo;
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.VendorInoviceNo != purchaseOrder.VendorInoviceNo)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "VendorInoviceNo";
                history.PrevValue = purchaseOrder.VendorInoviceNo;
                history.CurValue = input.VendorInoviceNo;
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.ContainerNo != purchaseOrder.ContainerNo)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "ContainerNo";
                history.PrevValue = purchaseOrder.ContainerNo;
                history.CurValue = input.ContainerNo;
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.ProductNotes != purchaseOrder.ProductNotes)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "ProductNotes";
                history.PrevValue = purchaseOrder.ProductNotes;
                history.CurValue = input.ProductNotes;
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.StockOrderStatusId != purchaseOrder.StockOrderStatusId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "StockOrderStatus";
                history.PrevValue = purchaseOrder.StockOrderStatusId > 0 ? _stockOrderStatusReposiorty.GetAll().Where(e => e.Id == purchaseOrder.StockOrderStatusId).FirstOrDefault().Name : "";
                history.CurValue = input.StockOrderStatusId > 0 ? _stockOrderStatusReposiorty.GetAll().Where(e => e.Id == input.StockOrderStatusId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.StockFromId != purchaseOrder.StockFromId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "StockFrom";
                history.PrevValue = purchaseOrder.StockFromId > 0 ? _stockfromsRepository.GetAll().Where(e => e.Id == purchaseOrder.StockFromId).FirstOrDefault().Name : "";
                history.CurValue = input.StockFromId > 0 ? _stockfromsRepository.GetAll().Where(e => e.Id == input.StockFromId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.DeliveryTypeId != purchaseOrder.DeliveryTypeId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "DeliveryType";
                history.PrevValue = purchaseOrder.DeliveryTypeId > 0 ? _deliveryTypeReposiorty.GetAll().Where(e => e.Id == purchaseOrder.DeliveryTypeId).FirstOrDefault().Name : "";
                history.CurValue = input.DeliveryTypeId > 0 ? _deliveryTypeReposiorty.GetAll().Where(e => e.Id == input.DeliveryTypeId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.PaymentStatusId != purchaseOrder.PaymentStatusId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "PaymentStatus";
                history.PrevValue = purchaseOrder.PaymentStatusId > 0 ? _paymentStatusRepository.GetAll().Where(e => e.Id == purchaseOrder.PaymentStatusId).FirstOrDefault().Name : "";
                history.CurValue = input.PaymentStatusId > 0 ? _paymentStatusRepository.GetAll().Where(e => e.Id == input.PaymentStatusId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.PaymentTypeId != purchaseOrder.PaymentTypeId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "PaymentType";
                history.PrevValue = purchaseOrder.PaymentTypeId > 0 ? _paymentTypeRepository.GetAll().Where(e => e.Id == purchaseOrder.PaymentTypeId).FirstOrDefault().Name : "";
                history.CurValue = input.PaymentTypeId > 0 ? _paymentTypeRepository.GetAll().Where(e => e.Id == input.PaymentTypeId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.WarehouseLocationId != purchaseOrder.WarehouseLocationId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "WarehouseLocation";
                history.PrevValue = purchaseOrder.WarehouseLocationId > 0 ? _warehouselocationRepository.GetAll().Where(e => e.Id == purchaseOrder.WarehouseLocationId).FirstOrDefault().location : "";
                history.CurValue = input.WarehouseLocationId > 0 ? _warehouselocationRepository.GetAll().Where(e => e.Id == input.WarehouseLocationId).FirstOrDefault().location : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.IncoTermId != purchaseOrder.IncoTermId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "IncoTerm";
                history.PrevValue = purchaseOrder.IncoTermId > 0 ? _IncoTermRepository.GetAll().Where(e => e.Id == purchaseOrder.IncoTermId).FirstOrDefault().Name : "";
                history.CurValue = input.IncoTermId > 0 ? _IncoTermRepository.GetAll().Where(e => e.Id == input.IncoTermId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.PaymentDueDate != purchaseOrder.PaymentDueDate)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "PaymentDueDate";
                history.PrevValue = purchaseOrder.PaymentDueDate.ToString();
                history.CurValue = input.PaymentDueDate.ToString();
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.TargetArrivalDate != purchaseOrder.TargetArrivalDate)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "TargetArrivalDate";
                history.PrevValue = purchaseOrder.TargetArrivalDate.ToString();
                history.CurValue = input.TargetArrivalDate.ToString();
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.ETD != purchaseOrder.ETD)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "ETD";
                history.PrevValue = purchaseOrder.ETD.ToString();
                history.CurValue = input.ETD.ToString();
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.PhysicalDeliveryDate != purchaseOrder.PhysicalDeliveryDate)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "PhysicalDeliveryDate";
                history.PrevValue = purchaseOrder.PhysicalDeliveryDate.ToString();
                history.CurValue = input.PhysicalDeliveryDate.ToString();
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.CurrencyId != purchaseOrder.CurrencyId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "Currency";
                history.PrevValue = purchaseOrder.CurrencyId > 0 ? _currencyRepository.GetAll().Where(e => e.Id == purchaseOrder.CurrencyId).FirstOrDefault().Name : "";
                history.CurValue = input.CurrencyId > 0 ? _currencyRepository.GetAll().Where(e => e.Id == input.CurrencyId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.GSTType != purchaseOrder.GSTType)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "GSTType";
                history.PrevValue = purchaseOrder.GSTType == 1 ? "With GST" : "WithOut GST";
                history.CurValue = input.GSTType == 1 ? " WithGST" : "WithOut GST";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.StockOrderForId != purchaseOrder.StockOrderForId)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "StockOrderFor";
                history.PrevValue = purchaseOrder.StockOrderForId > 0 ? _stockorderforRepository.GetAll().Where(e => e.Id == purchaseOrder.StockOrderForId).FirstOrDefault().Name : "";
                history.CurValue = input.StockOrderForId > 0 ? _stockorderforRepository.GetAll().Where(e => e.Id == input.StockOrderForId).FirstOrDefault().Name : "";
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }
            if (input.AdditionalNotes != purchaseOrder.AdditionalNotes)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                history.FieldName = "AdditionalNotes";
                history.PrevValue = purchaseOrder.AdditionalNotes;
                history.CurValue = input.AdditionalNotes;
                history.Action = "Update";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);
            }

            var purchaseOrderItemIds = await _purchaseOrderItemRepository.GetAll().Where(e => e.PurchaseOrderId == purchaseOrder.Id).ToListAsync();
            var CurrentpurchaseOrderItemIds = input.PurchaseOrderInfo != null ? input.PurchaseOrderInfo.Select(e => e.Id).ToList() : new List<int?>();
            var listpurchaseOrder = purchaseOrderItemIds.Where(e => !CurrentpurchaseOrderItemIds.Contains(e.Id));

            

            foreach (var item in listpurchaseOrder)
            {
                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();

                history.FieldName = "purchaseOrderItem Deleted";
                history.PrevValue = " ";
                history.CurValue  = item.ProductItemFk?.Name;
                history.Action = "Deleted";
                history.ActivityLogId = quickStockLogId;
                List.Add(history);

                await _purchaseOrderItemRepository.DeleteAsync(item);
            }
            foreach (var item in input.PurchaseOrderInfo)
            {
                item.PurchaseOrderId = purchaseOrder.Id;
                if ((item.Id != null ? item.Id : 0) == 0)
                {
                    
                    var purchaseOrderItem = ObjectMapper.Map<PurchaseOrderItem>(item);
                    QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                    history.FieldName = "purchaseOrderItem Added";
                    history.PrevValue = "";
                    history.CurValue  = item.ProductItem;
                    history.Action = "Add";
                    history.ActivityLogId = quickStockLogId;
                    List.Add(history);
                    await _purchaseOrderItemRepository.InsertAsync(purchaseOrderItem);
                }

                else
                
                {
                    var purchaseOrderItem = await _purchaseOrderItemRepository.GetAsync((int)item.Id);

                    if (item.PurchaseOrderId != purchaseOrderItem.PurchaseOrderId)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "PurchaseOrder";
                        history.PrevValue = purchaseOrderItem.PurchaseOrderId.ToString();
                        history.CurValue = item.PurchaseOrderId.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.ProductItemId != purchaseOrderItem.ProductItemId)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "ProductItem";
                        history.PrevValue = purchaseOrderItem.ProductItemId.ToString();
                        history.CurValue = item.ProductItemId.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.Quantity != purchaseOrderItem.Quantity)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "Quantity";
                        history.PrevValue = purchaseOrderItem.Quantity.ToString();
                        history.CurValue = item.Quantity.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.PricePerWatt != purchaseOrderItem.PricePerWatt)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "PricePerWatt";
                        history.PrevValue = purchaseOrderItem.PricePerWatt.ToString();
                        history.CurValue = item.PricePerWatt
                            .ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.UnitRate != purchaseOrderItem.UnitRate)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "UnitRate";
                        history.PrevValue = purchaseOrderItem.UnitRate.ToString();
                        history.CurValue = item.UnitRate.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.EstRate != purchaseOrderItem.EstRate)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "EstRate";
                        history.PrevValue = purchaseOrderItem.EstRate.ToString();
                        history.CurValue = item.EstRate.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.Amount != purchaseOrderItem.Amount)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "Amount";
                        history.PrevValue = purchaseOrderItem.Amount.ToString();
                        history.CurValue = item.Amount.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.ModelNo != purchaseOrderItem.ModelNo)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "ModelNo";
                        history.PrevValue = purchaseOrderItem.ModelNo.ToString();
                        history.CurValue = item.ModelNo.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }
                    if (item.ExpiryDate != purchaseOrderItem.ExpiryDate)
                    {
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "ExpiryDate";
                        history.PrevValue = purchaseOrderItem.ExpiryDate.ToString();
                        history.CurValue = item.ExpiryDate.ToString();
                        history.Action = "Update";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);
                    }

                    ObjectMapper.Map(item, purchaseOrderItem);
                    await _purchaseOrderItemRepository.UpdateAsync(purchaseOrderItem);
                    //purchaseOrderItem = ObjectMapper.Map<PurchaseOrderItem>(item);
                    

                }

            }

            if (input.StockOrderVariationDto != null)
            {
                
                var variationList = _stockVariationRepository.GetAll();
                var IdList = input.StockOrderVariationDto.Select(e => e.Id).ToList();
                var existingData = _stockOrderVariationRepository.GetAll().AsNoTracking().Where(x => x.PurchaseOrderId == purchaseOrder.Id).ToList();

                if (existingData != null)
                {
                    foreach (var item in existingData)
                    {
                        if (IdList != null)
                        {
                            bool containsItem = IdList.Any(x => x == item.Id);
                            if (containsItem == false)
                            {
                                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                                history.FieldName = "Variation Deleted";
                                history.PrevValue = variationList.Where(x => x.Id == item.StockVariationId).Select(x => x.Name).FirstOrDefault();
                                history.CurValue = "Deleted";
                                history.Action = "Varition Delete";
                                history.ActivityLogId = quickStockLogId;
                                List.Add(history);

                                _stockOrderVariationRepository.Delete(x => x.Id == item.Id);
                            }
                        }
                    }
                }

                foreach (var jobvariation in input.StockOrderVariationDto)
                {
                    if (jobvariation.Id != null && jobvariation.Id != 0)
                    {
                        var existData = _stockOrderVariationRepository.GetAll().AsNoTracking().Where(x => x.Id == jobvariation.Id).FirstOrDefault();
                        var olddetail = ObjectMapper.Map<StockOrderVariation>(jobvariation);
                        olddetail.PurchaseOrderId = purchaseOrder.Id;
                        _stockOrderVariationRepository.Update(olddetail);

                        var existVariation = variationList.Where(x => x.Id == existData.StockVariationId).Select(x => x.Name).FirstOrDefault();
                        var currentVariation = variationList.Where(x => x.Id == jobvariation.StockVariationId).Select(x => x.Name).FirstOrDefault();

                        if ((jobvariation.StockVariationId != null && jobvariation.StockVariationId != 0 && existData.StockVariationId != jobvariation.StockVariationId)
                            || (jobvariation.Cost != null && existData.Cost != jobvariation.Cost) || (jobvariation.Notes != null && existData.Notes != jobvariation.Notes))
                        {
                            //var existVariation = variationList.Where(x => x.Id == existData.VariationId).Select(x => x.Name).FirstOrDefault();
                            //var currentVariation = variationList.Where(x => x.Id == jobvariation.VariationId).Select(x => x.Name).FirstOrDefault();

                            if (existVariation != currentVariation)
                            {
                                QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                                history.FieldName = "Variation Edit"; 
                                history.PrevValue = existVariation + " - $" + existData.Cost.ToString() + " " + existData.Notes;
                                history.CurValue = currentVariation + " - $" + jobvariation.Cost.ToString() + " " + jobvariation.Notes;
                                history.Action = "Varition Edit";
                                history.ActivityLogId = quickStockLogId;
                                List.Add(history);
                              
                            }
                        }
                     }
                    //else if (jobvariation.VariationId != null)
                    else if(jobvariation.StockVariationId > 0)
                    {
                        var olddetail = ObjectMapper.Map<StockOrderVariation>(jobvariation);
                        olddetail.PurchaseOrderId = purchaseOrder.Id;

                        var varName = variationList.Where(x => x.Id == olddetail.PurchaseOrderId).Select(x => x.Name).FirstOrDefault();
                        QuickStockActivityLogHistory history = new QuickStockActivityLogHistory();
                        history.FieldName = "Variation Added";
                        history.PrevValue = "";
                        history.CurValue = varName + " - $" + jobvariation.Cost.ToString() + " - " + jobvariation.Notes;
                        history.Action = "Varition Added";
                        history.ActivityLogId = quickStockLogId;
                        List.Add(history);

                        _stockOrderVariationRepository.Insert(olddetail);
                    }
                }
            }


            await _dbcontextprovider.GetDbContext().QuickStockActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, purchaseOrder);
        }
        protected virtual async Task Create(CreateOrEditStockOrderDto input)
        {
            input.PaymentDueDate = _timeZoneConverter.Convert(input.PaymentDueDate, (int)AbpSession.TenantId);
            input.ETA = _timeZoneConverter.Convert(input.ETA, (int)AbpSession.TenantId);
            input.ETD = _timeZoneConverter.Convert(input.ETD, (int)AbpSession.TenantId);
            input.TargetArrivalDate = _timeZoneConverter.Convert(input.TargetArrivalDate, (int)AbpSession.TenantId);
            var purchaseOrder = ObjectMapper.Map<PurchaseOrder>(input);
            var purchaseOrderid = await _purchaseOrderRepository.InsertAndGetIdAsync(purchaseOrder);
            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Created";
            quickStockLog.SectionId = 1;
            quickStockLog.ActionId = 68;
            quickStockLog.Type = "Purchase Order";
            quickStockLog.PurchaseOrderId = purchaseOrderid;
            quickStockLog.ActionNote = "Purchase Order Created " ;

            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);
            if (input.StockOrderVariationDto != null)
            {
                foreach (var jobvariation in input.StockOrderVariationDto)
                {
                    var jobvar = ObjectMapper.Map<StockOrderVariation>(jobvariation);
                    jobvar.StockVariationId = input.Id;
                    _stockOrderVariationRepository.Insert(jobvar);
                }
            }
            foreach (var item in input.PurchaseOrderInfo)
            {
                item.PurchaseOrderId = purchaseOrderid;
                var purchaseOrderoitem = ObjectMapper.Map<PurchaseOrderItem>(item);

                await _purchaseOrderItemRepository.InsertAsync(purchaseOrderoitem);
            }
        }
        public async Task<GetStockOrderForViewDto> GetStockOrderById(int id)
        {
            var purchaseOrder = await _purchaseOrderRepository
                .GetAll()
                .Include(po => po.PurchaseCompanyFk)
                .Include(po => po.StockOrderForFk)
                .Include(po => po.StockFromFk)
                .Include(po => po.PaymentTypeFk)
                .Include(po => po.PaymentStatusFk)
                .Include(po => po.WarehouseLocationFk)
                .Include(po => po.CurrencyFk)
                .Include(po => po.DeliveryTypeFk)
                .Include(po => po.StockOrderStatusFk)
                .Include(po => po.TransportCompanyFk)
                .Include(po => po.VendorFK)
                .Include(po => po.IncoTermFk)
                .FirstOrDefaultAsync(po => po.Id == id);

            var purchaseOrderItems = await _purchaseOrderItemRepository
                .GetAll()
                .Include(poi => poi.ProductItemFk)
                .Include(poi => poi.ProductItemFk.ProductTypeFk)
                .Where(poi => poi.PurchaseOrderId == id)
                .ToListAsync();

            var stockOrderDto = new StockOrderDto
            {
                Id = purchaseOrder.Id,
                OrderNo = purchaseOrder.OrderNo,
                VendorInoviceNo = purchaseOrder.VendorInoviceNo,
                Vendor = purchaseOrder.VendorFK?.CompanyName ?? "",
                PurchaseCompany = purchaseOrder.PurchaseCompanyFk?.Name ?? " ",
                ManualOrderNo = purchaseOrder.ManualOrderNo,
                ContainerNo = purchaseOrder.ContainerNo,
                StockOrderStatus = purchaseOrder.StockOrderStatusFk?.Name ?? " ",
                StockFrom = purchaseOrder.StockFromFk?.Name ?? " ",
                DeliveryType = purchaseOrder.DeliveryTypeFk?.Name ?? " ",
                PaymentType = purchaseOrder.PaymentTypeFk?.Name ?? " ",
                PaymentStatus = purchaseOrder.PaymentStatusFk?.Name ?? " ",
                WarehouseLocation = purchaseOrder.WarehouseLocationFk?.location ?? " ",
                WarehouseLocationId = purchaseOrder.WarehouseLocationId,
                Currency = purchaseOrder.CurrencyFk?.Name ?? " ",
                IncoTerm = purchaseOrder.IncoTermFk?.Name ?? " ",
                GSTType = purchaseOrder.GSTType == 1 ? "With GST" : (purchaseOrder.GSTType == 2 ? "Without GST" : " "),
                StockOrderFor = purchaseOrder.StockOrderForFk?.Name ?? " ",
                TargetArrivalDate = purchaseOrder.TargetArrivalDate,
                PaymentDueDate = purchaseOrder.PaymentDueDate,
                DocSentDate = purchaseOrder.DocSentDate,
                ETD = purchaseOrder.ETD,
                PhysicalDeliveryDate = purchaseOrder.PhysicalDeliveryDate,
                AdditionalNotes = purchaseOrder.AdditionalNotes,
                ProductNotes = purchaseOrder.ProductNotes,
                OrderDate = purchaseOrder.CreationTime,
                isSubmit = purchaseOrder.IsSubmitDate != null,
            };

            var purchaseOrderItemDtos = new List<GetPurchaseOrderItemListForViewDto>();

            foreach (var item in purchaseOrderItems)
            {
                // Calculate the pending quantity
                var serialNoCount = await GetSerialNoCountForPurchaseOrderItem(id,(int)item.ProductItemId);
                var isApp = await CheckIfSerialNoIsApp(id, (int)item.ProductItemId);

                purchaseOrderItemDtos.Add(new GetPurchaseOrderItemListForViewDto
                {
                    Id = item.Id,
                    ProductTypeId = item.ProductItemFk.ProductTypeId,
                    ProductItemId = item.ProductItemId,
                    ProductType = item.ProductItemFk.ProductTypeFk.Name,
                    ProductItem = item.ProductItemFk?.Name,
                    UnitRate = item.UnitRate,
                    EstRate = item.EstRate,
                    Amount = item.Amount,
                    Quantity = item.Quantity,
                    PendingQuantity = item.Quantity - serialNoCount,
                    UploadedQuantity=serialNoCount,
                    PricePerWatt = item.PricePerWatt,
                    ModelNo = item.ModelNo,
                    ExpiryDate = item.ExpiryDate,
                    IsApp = isApp,
                });
            }

            var getStockOrderForViewDto = new GetStockOrderForViewDto
            {
                PurchaseOrder = stockOrderDto,
                PurchaseOrderItemList = purchaseOrderItemDtos
            };
            return getStockOrderForViewDto;
        }
        private async Task<int> GetSerialNoCountForPurchaseOrderItem(int purchaseOrderId, int productItemId)
        {
            var result=_stcokSerialnoRepository.GetAll()
                .Where(e => e.PurchaseOrderId == purchaseOrderId && e.ProductItemId == productItemId);
            
            var count= result.Count();
            return count;
        }
        private async Task<bool> CheckIfSerialNoIsApp(int purchaseOrderId, int productItemId)
        {
            return await _stcokSerialnoRepository.GetAll()
                .AnyAsync(e => e.PurchaseOrderId == purchaseOrderId && e.ProductItemId == productItemId && e.IsApp);
        }
        public async Task<FileDto> GetStockOrderToExcel(GetStockorderForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var PurchaseOrder = _purchaseOrderRepository.GetAll()
                     .Include(e => e.PurchaseCompanyFk)
                     .Include(e => e.StockOrderForFk)
                     .Include(e => e.StockFromFk)
                     .Include(e => e.PaymentTypeFk)
                     .Include(e => e.PaymentStatusFk)
                     .Include(e => e.WarehouseLocationFk)
                     .Include(e => e.CurrencyFk)
                     .Include(e => e.DeliveryTypeFk)
                     .Include(e => e.StockOrderStatusFk)
                     .Include(e => e.TransportCompanyFk)
                     .WhereIf(input.FilterName == "ContainerNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ContainerNo.Contains(input.Filter))
                     .WhereIf(input.FilterName == "ManualOrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.ManualOrderNo.Contains(input.Filter))
                     .WhereIf(input.FilterName == "VendorInoviceNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.VendorInoviceNo.Contains(input.Filter))
                     .WhereIf(input.FilterName == "OrderNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.OrderNo.ToString().Contains(input.Filter))
                     .WhereIf(input.StockOrderStatusIds != null && input.StockOrderStatusIds.Count() > 0, e => input.StockOrderStatusIds.Contains((e.StockOrderStatusFk.Id)))
                     .WhereIf(input.WarehouseLocationIdFiletr != null && input.WarehouseLocationIdFiletr.Count() > 0, e => input.WarehouseLocationIdFiletr.Contains((e.WarehouseLocationFk.Id)))
                     .WhereIf(input.PaymentStatusIdFiletr > 0, e => e.PaymentStatusId == input.PaymentStatusIdFiletr)
                     //.WhereIf(input.PaymentStatusIdFiletr != null && input.PaymentStatusIdFiletr.Count() > 0, e => input.PaymentStatusIdFiletr.Contains((e.PaymentStatusFk.Id)))
                     .WhereIf(input.PaymentTypeIdFiletr != null && input.PaymentTypeIdFiletr.Count() > 0, e => input.PaymentTypeIdFiletr.Contains((e.PaymentTypeFk.Id)))
                     .WhereIf(input.VendorIdFiletr > 0, e => e.VendorId == input.VendorIdFiletr)
                     .WhereIf(input.GstTypeIdFilter > 0, e => e.GSTType == input.GstTypeIdFilter)
                     .WhereIf(input.DeliveryTypeIdFiletr > 0, e => e.DeliveryTypeId == input.DeliveryTypeIdFiletr)
                     .WhereIf(input.StockOrderIdFiletr > 0, e => e.StockOrderForId == input.StockOrderIdFiletr)
                     .WhereIf(input.TransportCompanyFilter > 0, e => e.TransportCompanyId == input.TransportCompanyFilter)
                     .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
                     .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
                     .Where(e => e.OrganizationId == input.OrganizationId);

            var query = from o in PurchaseOrder
                        select new GetAllStockOrderOutputDto()
                                   {
                                       Id = o.Id,
                                       OrderNo = o.OrderNo,
                                       OrganizationId = o.OrganizationId,
                                       VendorInoviceNo = o.VendorInoviceNo,
                                       Vendor = o.VendorFK.CompanyName,
                                       PurchaseCompany = o.PurchaseCompanyFk.Name,
                                       ManualOrderNo = o.ManualOrderNo,
                                       ContainerNo = o.ContainerNo,
                                       StockOrderStatus = o.StockOrderStatusFk.Name,
                                       StockFrom = o.StockFromFk.Name,
                                       DeliveryType = o.DeliveryTypeFk.Name,
                                       PaymentType = o.PaymentTypeFk.Name,
                                       PaymentStatus = o.PaymentStatusFk.Name,
                                       WarehouseLocation = o.WarehouseLocationFk.location,
                                       Currency = o.CurrencyFk.Name,
                                       OrderDate = o.CreationTime

                                   };
            var stockOrderItems = query.ToList();

            for (int i = 0; i < stockOrderItems.Count; i++)
            {
                var stockOrder = stockOrderItems[i];
                int totalQuantity = _purchaseOrderItemRepository
                    .GetAll()
                    .Where(e => e.PurchaseOrderId == stockOrder.Id)
                    .Sum(e => e.Quantity ?? 0);

                stockOrder.Qty = totalQuantity;
            }
            var ListDtos = stockOrderItems;
            return _stockOrderExcelExporter.ExportToFile(ListDtos);
        }
 
        public async Task Delete(EntityDto input)
        {
            var Name = _purchaseOrderRepository.Get(input.Id);

            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Deleted";
            quickStockLog.SectionId = 1;
            quickStockLog.ActionId = 70;
            quickStockLog.PurchaseOrderId = input.Id;
            quickStockLog.ActionNote = "purchase order Deleted : " + Name;
            quickStockLog.Type = "Purchase Order";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            await _purchaseOrderRepository.DeleteAsync(input.Id);
            var purchaseOrderItems = await _purchaseOrderItemRepository.GetAllListAsync(x => x.PurchaseOrderId == input.Id);
            foreach (var item in purchaseOrderItems)
            {
                await _purchaseOrderItemRepository.DeleteAsync(item.Id);
            }
        }

        public async Task<GetStockOrderForEditOutput> GetStockOrderByOrderNo(long orderNo)
        {
           
            var purchaseOrder = await _purchaseOrderRepository.FirstOrDefaultAsync(po => po.OrderNo == orderNo);
            var purchaseOrderDto = ObjectMapper.Map<CreateOrEditStockOrderDto>(purchaseOrder);
            var purchaseOrderItems = await _purchaseOrderItemRepository.GetAll()
                                           .Include(e => e.ProductItemFk)
                                           .Where(e => e.PurchaseOrderId == purchaseOrder.Id)
                                           .ToListAsync();

            purchaseOrderDto.PurchaseOrderInfo = new List<PurchaseOrderItemDto>();
            foreach (var item in purchaseOrderItems)
            {
                var purchaseOrderItemDto = new PurchaseOrderItemDto
                {
                    ProductTypeId = item.ProductItemFk.ProductTypeId,
                    ProductItemId = item.ProductItemId,
                    ProductItem = item.ProductItemFk.Name,
                    UnitRate = item.UnitRate,
                    EstRate = item.EstRate,
                    Amount = item.Amount,
                    Quantity = item.Quantity,
                    PricePerWatt = item.PricePerWatt,
                    ModelNo = item.ModelNo,
                    ExpiryDate = item.ExpiryDate
                };

                purchaseOrderDto.PurchaseOrderInfo.Add(purchaseOrderItemDto);
            }
            var output = new GetStockOrderForEditOutput
            {
                StockOrder = purchaseOrderDto
            };
            return output;
        }
        public async Task<List<GetPurchaseOrderActivityLogViewDto>> GetPurchaseOrderActivityLog(GetPurchaseOrderActivityLogInput input)
        {

            List<int> purchaseOrderActionids = new List<int>() { 68, 69, 70 };
            var allactionids = _lookup_leadActionRepository.GetAll().Select(e => e.Id).ToList();
           
            var Result = _quickStockActivityLogRepository.GetAll()
                .Where(L => L.PurchaseOrderId == input.PurchaseOrderId)
                ////.WhereIf(input.ActionId == 0, L => allactionids.Contains(L.ActionId))
                ////.WhereIf(input.ActionId == 1, L => purchaseOrderActionids.Contains(L.ActionId))
                .WhereIf(input.CurrentPage != false, L => L.SectionId == input.SectionId)
                ;

            if (!input.AllActivity)
            {
                Result = Result.OrderByDescending(e => e.Id).Take(10);
            }
            else
            {
                Result = Result.OrderByDescending(e => e.Id);
            }

            var purchaseOrders = from o in Result


                        join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        select new GetPurchaseOrderActivityLogViewDto()
                        {
                            Id = o.Id,
                            ActionName = o.ActionFk.ActionName,
                            ActionId = o.ActionId,
                            ActionNote = o.ActionNote,
                            PurchaseOrderId = o.PurchaseOrderId,
                            ActivityDate = o.ActivityDate,
                            Section = o.SectionFK.SectionName,
                            ActivitysDate = o.ActivityDate.Value.ToString("dd-MM-yyyy hh:mm:ss"),
                            CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                            CreationTime = o.CreationTime,

                        };

            return new List<GetPurchaseOrderActivityLogViewDto>(
                   await purchaseOrders.ToListAsync()
               );
        }
        public async Task<List<PurchaseOrderHistoryDto>>GetPurchaseOrderActivityLogHistory(GetPurchaseOrderActivityLogInput input)
        {
            try
            {
                var varr = _purchaseOrderHistoryRepository.GetAll().Include(e => e.QuickStockActivityLogFK);
                var Result = (from item in _purchaseOrderHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              where (item.ActivityLogId == input.PurchaseOrderId)
                              select new PurchaseOrderHistoryDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  PrevValue = item.PrevValue,
                                  CurValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              })
                    .OrderByDescending(e => e.Id).ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<PurchaseDocumentListDto> GetPurchaseDocumentData(int id)
        {
            var purchaseDocument = await _purchasedocumentListRepository.GetAll()
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();
           
            var purchaseDocumentDto = new PurchaseDocumentListDto
            {
                Id = purchaseDocument.Id,
                Name = purchaseDocument.Name,
                Size = purchaseDocument.Size,
                Formats = purchaseDocument.Format.Replace("Excel", "xls,xlsx,xml").Replace("ZIP", "7z,zip").Split(',').ToList(),

            };

            return purchaseDocumentDto;
        }
        public async Task<ListResultDto<GetAllPurchaseDocumentEditOutput>> GetAllPurchaseDocument(EntityDto input)
        {
            var purchaseOrderDocuments = _purchasedocumentRepository.GetAll().Where(e => e.PurchaseOrderId == input.Id);
            var User_List = _userRepository.GetAll().AsNoTracking();
            var result = purchaseOrderDocuments
                .Select(document => new GetAllPurchaseDocumentEditOutput
                {
                    Id = document.Id,
                    CreationDate = document.CreationTime,
                    CreatedBy = User_List.Where(e => e.Id == document.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                    FileName=document.FileName,
                    FilePath=document.FilePath,
                    PurchaseDocumentTitle = document.PurchaseDocumentListFk.Name,
                    DocName = document.DocumentName
                })
                .ToList();

            return new ListResultDto<GetAllPurchaseDocumentEditOutput>(result);
        }

        public async Task DeletePurchaseDoucment(EntityDto input)
        {
            var purchaseOrderDocuments = _purchasedocumentRepository.Get(input.Id);
            var name = await _purchasedocumentListRepository.GetAll()
                .Where(e => e.Id == purchaseOrderDocuments.PurchaseDocumentId)
                .Select(e => e.Name)
                .FirstOrDefaultAsync();
            var quickStockLog = new QuickStockActivityLog();
            quickStockLog.Action = "Deleted";
            quickStockLog.SectionId = 1;
            quickStockLog.ActionId = 85;
            quickStockLog.PurchaseOrderId = purchaseOrderDocuments.PurchaseOrderId;
            quickStockLog.ActionNote = "Document Deleted For: "+ name;
            quickStockLog.Type = "Purchase Order";
            await _quickStockActivityLogRepository.InsertAndGetIdAsync(quickStockLog);

            await _purchasedocumentRepository.DeleteAsync(input.Id);

        }

        public async Task<GetStockOrderForViewDto> GetPoDataByPurchaseOrderId(int PurchaseOrderId)
        {
            
            var purchaseOrder = await _purchaseOrderRepository.GetAll()
                    .Include(e => e.PurchaseCompanyFk)
                    .Include(e => e.StockOrderForFk)
                    .Include(e => e.StockFromFk)
                    .Include(e => e.PaymentTypeFk)
                    .Include(e => e.PaymentStatusFk)
                    .Include(e => e.WarehouseLocationFk)
                    .Include(e => e.CurrencyFk)
                    .Include(e => e.DeliveryTypeFk)
                    .Include(e => e.StockOrderStatusFk)
                    .Include(e => e.TransportCompanyFk)
                    .Include(e => e.VendorFK)
                    .Where(e => e.Id == PurchaseOrderId).AsNoTracking().FirstOrDefaultAsync();

            var purchaseOrderItems = await _purchaseOrderItemRepository
                .GetAll()
                .Include(poi => poi.ProductItemFk)
                .Include(poi => poi.ProductItemFk.ProductTypeFk)
                .Where(poi => poi.PurchaseOrderId == PurchaseOrderId)
                .ToListAsync();

            var orgId = purchaseOrder.OrganizationId;

            var stockOrderDto = new StockOrderDto
            {
                Id = purchaseOrder.Id,
                OrderNo = purchaseOrder.OrderNo,
                
                Vendor = purchaseOrder.VendorFK?.CompanyName ?? "",
                VendorAddress = purchaseOrder.VendorFK.Address + purchaseOrder.VendorFK.UnitNo + purchaseOrder.VendorFK.StreetName,
                PurchaseCompany = purchaseOrder.PurchaseCompanyFk?.Name ?? " ",
                PaymentTerm = purchaseOrder.VendorFK.CreditDays,
                WarehouseLocation = purchaseOrder.WarehouseLocationFk.location ,               
                Currency = purchaseOrder.CurrencyFk?.Name ?? " ",                             
                PaymentDueDate = purchaseOrder.PaymentDueDate,
                AdditionalNotes = purchaseOrder.AdditionalNotes,
                OrderDatee = purchaseOrder.CreationTime.ToString("dd-MM-yyyy")
            };
            var quoteTemplate = GetTemplate(orgId, 13);
            if (!string.IsNullOrEmpty(quoteTemplate.ViewHtml))
            {
                stockOrderDto.ViewHtml = quoteTemplate.ViewHtml;
            }
            var purchaseOrderItemDtos = new List<GetPurchaseOrderItemListForViewDto>();

            foreach (var item in purchaseOrderItems)
            {
               

                purchaseOrderItemDtos.Add(new GetPurchaseOrderItemListForViewDto
                {
                    Id = item.Id,         
                    ProductType = item.ProductItemFk.ProductTypeFk.Name,
                    ProductItem = item.ProductItemFk?.Name,
                    UnitRate = item.UnitRate,
                    EstRate = item.EstRate,
                    Amount = item.Amount,
                    Quantity = item.Quantity,
                    ExpiryDatee = item.ExpiryDate.Value.ToString("dd-MM-yyyy"),
                    PricePerWatt = item.PricePerWatt,
                    ModelNo = item.ModelNo,
                   
                    
                });
            }

            var getStockOrderForViewDto = new GetStockOrderForViewDto
            {
                PurchaseOrder = stockOrderDto,
                PurchaseOrderItemList = purchaseOrderItemDtos
            };
            return getStockOrderForViewDto;


        }
        public async Task RevertAllSerialNo(int PurchaseOrderId, int productItemId)
        {
            var result = _stcokSerialnoRepository.GetAll().Where(e => e.PurchaseOrderId == PurchaseOrderId && e.ProductItemId == productItemId);
           foreach (var item in result)
            {
                _stcokSerialnoRepository.Delete(item);
            }


        }
    }
}
