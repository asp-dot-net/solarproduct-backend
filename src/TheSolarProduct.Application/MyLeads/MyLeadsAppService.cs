﻿using TheSolarProduct.PostCodes;
using System.Collections.Generic;
using TheSolarProduct.States;
using System.Collections.Generic;
using TheSolarProduct.LeadSources;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.MyLeads.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.LeadStatuses;
using TheSolarProduct.UnitTypes;
using TheSolarProduct.StreetNames;
using TheSolarProduct.StreetTypes;
using TheSolarProduct.LeadActivityLogs;
using TheSolarProduct.LeadActions;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Runtime.Session;
using Abp;
using TheSolarProduct.MyLeads;
using TheSolarProduct.Leads;
using TheSolarProduct.Myleads.Exporting;
using TheSolarProduct.QuickStocks;
using TheSolarProduct.StockOrders;
using TheSolarProduct.LeadHistory;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Sections;
namespace TheSolarProduct.Myleads
{
	[AbpAuthorize(AppPermissions.Pages_Leads)]
	public class MyLeadsAppService : TheSolarProductAppServiceBase, IMyLeadsAppService
	{
		private readonly IRepository<Lead> _leadRepository;
		private readonly IRepository<LeadActivityLog> _leadactivityRepository;
		private readonly IRepository<LeadAction, int> _lookup_leadActionRepository;
		private readonly IMyleadsExcelExporter _leadsExcelExporter;
		private readonly IRepository<PostCode, int> _lookup_postCodeRepository;
		private readonly IRepository<State, int> _lookup_stateRepository;
		private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
		private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
		private readonly IRepository<UnitType, int> _lookup_unitTypeRepository;
		private readonly IRepository<StreetName, int> _lookup_streetNameRepository;
		private readonly IRepository<StreetType, int> _lookup_streetTypeRepository;
		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<UserRole, long> _userroleRepository;
		private readonly IRepository<Role> _roleRepository;
		private readonly IRepository<LeadStatus> _leadStatus;
        private readonly IRepository<Section> _SectionAppService;


        public MyLeadsAppService(IRepository<Lead> leadRepository, IMyleadsExcelExporter leadsExcelExporter, IRepository<PostCode, int> lookup_postCodeRepository, IRepository<State, int> lookup_stateRepository, IRepository<LeadSource, int> lookup_leadSourceRepository, IRepository<LeadStatus, int> lookup_leadStatusRepository
			, IRepository<UnitType, int> lookup_unitTypeRepository
			, IRepository<StreetName, int> lookup_streetNameRepository
			, IRepository<StreetType, int> lookup_streetTypeRepository
			, IRepository<LeadActivityLog> leadactivityRepository
			, IRepository<LeadAction, int> lookup_leadActionRepository
			, IRepository<User, long> userRepository
			, IRepository<UserRole, long> userroleRepository
			, IRepository<Role> roleRepository
			, IRepository<LeadStatus> leadStatus
			, IRepository<Section> SectionAppService
           

            )
		{
			_leadRepository = leadRepository;
			_leadsExcelExporter = leadsExcelExporter;
			_lookup_postCodeRepository = lookup_postCodeRepository;
			_lookup_stateRepository = lookup_stateRepository;
			_lookup_leadSourceRepository = lookup_leadSourceRepository;
			_lookup_leadStatusRepository = lookup_leadStatusRepository;
			_lookup_unitTypeRepository = lookup_unitTypeRepository;
			_lookup_streetNameRepository = lookup_streetNameRepository;
			_lookup_streetTypeRepository = lookup_streetTypeRepository;
			_leadactivityRepository = leadactivityRepository;
			_lookup_leadActionRepository = lookup_leadActionRepository;
			_userRepository = userRepository;
			_userroleRepository = userroleRepository;
			_roleRepository = roleRepository;
			_leadStatus = leadStatus;
            _SectionAppService = SectionAppService;

        }

		public async Task<PagedResultDto<GetMyLeadForViewDto>> GetAll(GetAllMyLeadsInput input)
		{

			var filteredLeads = _leadRepository.GetAll()
						//.Include(e => e.SuburbFk)
						//.Include(e => e.StateFk)
						.Include(e => e.LeadStatusFk)
						//.Include(e => e.LeadSourceFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.SuburbFk != null && e.SuburbFk.Suburb == input.PostCodeSuburbFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateFk != null && e.StateFk.Name == input.StateNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceFk != null && e.LeadSourceFk.Name == input.LeadSourceNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.leadStatusName), e => e.LeadStatusFk != null && e.LeadStatusFk.Status == input.leadStatusName)
						.WhereIf(input.leadStatusId != null,e => e.LeadStatusId == input.leadStatusId)
						.WhereIf(input.StartDate != null,e => e.CreationTime >=  input.StartDate)
						.WhereIf(input.EndDate != null, e => e.CreationTime <= input.EndDate);

			var pagedAndFilteredLeads = filteredLeads
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var leads = from o in pagedAndFilteredLeads
						//join o1 in _lookup_postCodeRepository.GetAll() on o.SuburbId equals o1.Id into j1
						//from s1 in j1.DefaultIfEmpty()

						//join o2 in _lookup_stateRepository.GetAll() on o.StateId equals o2.Id into j2
						//from s2 in j2.DefaultIfEmpty()

						join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
						from s3 in j3.DefaultIfEmpty()

						//join o4 in _lookup_leadSourceRepository.GetAll() on o.LeadSourceId equals o4.Id into j4
						//from s4 in j4.DefaultIfEmpty()

						select new GetMyLeadForViewDto()
						{
							Lead = new MyLeadDto
							{
								CompanyName = o.CompanyName,
								Email = o.Email,
								Phone = o.Phone,
								Mobile = o.Mobile,
								Address = o.Address,
								Requirements = o.Requirements,
								Id = o.Id,
								PostCode = o.PostCode,
								LeadStatusID = o.LeadStatusId
							},
							//PostCodeSuburb = s1 == null || s1.Suburb == null ? "" : s1.Suburb.ToString(),
							//StateName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
							LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
							//LeadSourceName = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
						};

			foreach (var item in leads)
			{
				item.Duplicate = _leadRepository.GetAll().Where(e => e.Email == item.Lead.Email && e.Phone == item.Lead.Phone).Any();

				item.WebDuplicate = _leadRepository.GetAll().Where(e => e.Email == item.Lead.Email && e.Phone == item.Lead.Phone).Any();
			}

			var totalCount = await filteredLeads.CountAsync();

			return new PagedResultDto<GetMyLeadForViewDto>(
				totalCount,
				await leads.ToListAsync()
			);
		}

		public async Task<GetMyLeadForViewDto> GetLeadForView(int id)
		{
			var lead = await _leadRepository.GetAsync(id);

			var output = new GetMyLeadForViewDto { Lead = ObjectMapper.Map<MyLeadDto>(lead) };

			//if (output.Lead.SuburbId != null)
			//{
			//	var _lookupPostCode = await _lookup_postCodeRepository.FirstOrDefaultAsync((int)output.Lead.SuburbId);
			//	output.PostCodeSuburb = _lookupPostCode?.Suburb?.ToString();
			//}

			//if (output.Lead.StateId != null)
			//{
			//	var _lookupState = await _lookup_stateRepository.FirstOrDefaultAsync((int)output.Lead.StateId);
			//	output.StateName = _lookupState?.Name?.ToString();
			//}

			//if (output.Lead.LeadSourceId != null)
			//{
			//	var _lookupLeadSource = await _lookup_leadSourceRepository.FirstOrDefaultAsync((int)output.Lead.LeadSourceId);
			//	output.LeadSourceName = _lookupLeadSource?.Name?.ToString();
			//}

			if (output.Lead.LeadStatusID != null)
			{
				var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.Lead.LeadStatusID);
				output.LeadStatusName = _lookupLeadStatus?.Status?.ToString();
			}

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_Leads_Edit)]
		public async Task<GetMyLeadForEditOutput> GetLeadForEdit(EntityDto input)
		{
			var lead = await _leadRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMyLeadForEditOutput { Lead = ObjectMapper.Map<CreateOrEditMyLeadDto>(lead) };

			//if (output.Lead.SuburbId != null)
			//{
			//	var _lookupPostCode = await _lookup_postCodeRepository.FirstOrDefaultAsync((int)output.Lead.SuburbId);
			//	output.PostCodeSuburb = _lookupPostCode?.Suburb?.ToString();
			//}

			//if (output.Lead.StateId != null)
			//{
			//	var _lookupState = await _lookup_stateRepository.FirstOrDefaultAsync((int)output.Lead.StateId);
			//	output.StateName = _lookupState?.Name?.ToString();
			//}

			//if (output.Lead.LeadSourceId != null)
			//{
			//	var _lookupLeadSource = await _lookup_leadSourceRepository.FirstOrDefaultAsync((int)output.Lead.LeadSourceId);
			//	output.LeadSourceName = _lookupLeadSource?.Name?.ToString();
			//}

			if (output.Lead.LeadStatusID != null)
			{
				var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.Lead.LeadStatusID);
				output.LeadStatusName = _lookupLeadStatus?.Status?.ToString();
			}

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditMyLeadDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_Leads_Create)]
		protected virtual async Task Create(CreateOrEditMyLeadDto input)
		{
           
            var result = _leadRepository.GetAll().Where(e => e.Email == input.Email && e.Phone == input.Phone && e.Mobile == input.Mobile).Any();
			if (!result)
			{
				input.LeadStatusID = 1;
				var lead = ObjectMapper.Map<Lead>(input);
				if (AbpSession.TenantId != null)
				{
					lead.TenantId = (int)AbpSession.TenantId;
				}
				lead.LeadStatusId = 1;

				await _leadRepository.InsertAndGetIdAsync(lead);

				LeadActivityLog leadactivity = new LeadActivityLog();
				leadactivity.ActionId = 1;
				leadactivity.SectionId = 0;
				leadactivity.ActionNote = "New Lead";
				leadactivity.LeadId = lead.Id;
				if (AbpSession.TenantId != null)
				{
					leadactivity.TenantId = (int)AbpSession.TenantId;
				}
				
				

                await _leadactivityRepository.InsertAsync(leadactivity);
            }
		}

		[AbpAuthorize(AppPermissions.Pages_Leads_Edit)]
		protected virtual async Task Update(CreateOrEditMyLeadDto input)
		{
			var result = _leadRepository.GetAll().Where(e => e.Email == input.Email && e.Phone == input.Phone && e.Mobile == input.Mobile).Any();
			if (!result)
			{
				var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);
				//input.LeadStatusID = _lookup_leadStatusRepository.GetAll().Where(s => s.Status == input.LeadStatusName).Select(s => s.Id).FirstOrDefault();
				input.LeadStatusID = 1;
				ObjectMapper.Map(input, lead);

				LeadActivityLog leadactivity = new LeadActivityLog();
				leadactivity.ActionId = 2;
				leadactivity.SectionId = 0;
				leadactivity.ActionNote = "Lead Modified";
				leadactivity.LeadId = lead.Id;
				if (AbpSession.TenantId != null)
				{
					leadactivity.TenantId = (int)AbpSession.TenantId;
				}
				await _leadactivityRepository.InsertAsync(leadactivity);
			}			
		}

		public bool LeadExist(CreateOrEditMyLeadDto input)
		{
			var result = _leadRepository.GetAll().Where(e => e.Email == input.Email && e.Phone == input.Phone && e.Mobile == input.Mobile).Any();

			return result;
		}

		[AbpAuthorize(AppPermissions.Pages_Leads_Edit)]
		public async Task ChangeStatus(GetMyLeadForChangeStatusOutput input)
		{
			var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);
			input.LeadStatusID = input.LeadStatusID;
			await _leadRepository.UpdateAsync(lead);

			LeadActivityLog leadactivity = new LeadActivityLog();
			leadactivity.ActionId = 5;
			leadactivity.ActionNote = "Lead Status Changed";
			leadactivity.LeadId = lead.Id;
			if (AbpSession.TenantId != null)
			{
				leadactivity.TenantId = (int)AbpSession.TenantId;
			}
			await _leadactivityRepository.InsertAsync(leadactivity);
		}

		[AbpAuthorize(AppPermissions.Pages_Leads_Edit)]
		public async Task<GetMyLeadForChangeStatusOutput> GetLeadForChangeStatus(EntityDto input)
		{
			var lead = await _leadRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetMyLeadForChangeStatusOutput();
			output.Id = lead.Id;
			output.LeadStatusID = lead.LeadStatusId;
			output.CompanyName = lead.CompanyName;

			if (output.LeadStatusID != null)
			{
				var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.LeadStatusID);
				output.StatusName = _lookupLeadStatus?.Status?.ToString();
			}

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_Leads_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _leadRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetLeadsToExcel(GetAllMyLeadsForExcelInput input)
		{

			var filteredLeads = _leadRepository.GetAll()
						//.Include(e => e.SuburbFk)
						//.Include(e => e.StateFk)
						.Include(e => e.LeadStatusFk)
						//.Include(e => e.LeadSourceFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.SuburbFk != null && e.SuburbFk.Suburb == input.PostCodeSuburbFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateFk != null && e.StateFk.Name == input.StateNameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter);
						//.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceFk != null && e.LeadSourceFk.Name == input.LeadSourceNameFilter);

			var query = (from o in filteredLeads
						 //join o1 in _lookup_postCodeRepository.GetAll() on o.SuburbId equals o1.Id into j1
						 //from s1 in j1.DefaultIfEmpty()

						 //join o2 in _lookup_stateRepository.GetAll() on o.StateId equals o2.Id into j2
						 //from s2 in j2.DefaultIfEmpty()

						 join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
						 from s3 in j3.DefaultIfEmpty()

						 //join o4 in _lookup_leadSourceRepository.GetAll() on o.LeadSourceId equals o4.Id into j4
						 //from s4 in j4.DefaultIfEmpty()

						 select new GetMyLeadForViewDto()
						 {
							 Lead = new MyLeadDto
							 {
								 CompanyName = o.CompanyName,
								 Email = o.Email,
								 Phone = o.Phone,
								 Mobile = o.Mobile,
								 Address = o.Address,
								 Requirements = o.Requirements,
								 Id = o.Id,
								 PostCode = o.PostCode
							 },
							 //PostCodeSuburb = s1 == null || s1.Suburb == null ? "" : s1.Suburb.ToString(),
							 //StateName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
							 LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
							 //LeadSourceName = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
						 });


			var leadListDtos = await query.ToListAsync();

			return _leadsExcelExporter.ExportToFile(leadListDtos);
		}


		[AbpAuthorize(AppPermissions.Pages_Leads)]
		public async Task<List<MyLeadSuburbLookupTableDto>> GetAllSuburbForPostCodeTableDropdown()
		{
			return await _lookup_postCodeRepository.GetAll()
				.Select(postCode => new MyLeadSuburbLookupTableDto
				{
					Id = postCode.Id,
					DisplayName = postCode == null || postCode.Suburb == null ? "" : postCode.Suburb.ToString().ToUpper()
				}).ToListAsync();
		}

		[AbpAuthorize(AppPermissions.Pages_Leads)]
		public async Task<List<MyLeadStateLookupTableDto>> GetAllStateForTableDropdown()
		{
			return await _lookup_stateRepository.GetAll()
				.Select(state => new MyLeadStateLookupTableDto
				{
					Id = state.Id,
					DisplayName = state == null || state.Name == null ? "" : state.Name.ToString().ToUpper()
				}).ToListAsync();
		}

		[AbpAuthorize(AppPermissions.Pages_Leads)]
		public async Task<List<MyLeadSuburbLookupTableDto>> GetAllPostCodeForTableDropdown()
		{
			return await _lookup_postCodeRepository.GetAll()
				.Select(postCode => new MyLeadSuburbLookupTableDto
				{
					Id = postCode.Id,
					DisplayName = postCode == null || postCode.PostalCode == null ? "" : postCode.PostalCode.ToString().ToUpper()
				}).ToListAsync();
		}

		[AbpAuthorize(AppPermissions.Pages_Leads)]
		public async Task<List<MyLeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown()
		{
			return await _lookup_leadSourceRepository.GetAll()
				.Select(leadSource => new MyLeadSourceLookupTableDto
				{
					Id = leadSource.Id,
					DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
				}).ToListAsync();
		}

		public async Task<List<MyLeadUnitTypeLookupTableDto>> GetAllUnitTypeForUnitTypeTableDropdown()
		{
			return await _lookup_unitTypeRepository.GetAll()
				.Select(leadUnitType => new MyLeadUnitTypeLookupTableDto
				{
					Id = leadUnitType.Id,
					DisplayName = leadUnitType == null || leadUnitType.Name == null ? "" : leadUnitType.Name.ToString().ToUpper()
				}).ToListAsync();
		}

		public async Task<List<MyLeadStreetNameLookupTableDto>> GetAllStreetNameForStreetNameTableDropdown()
		{
			return await _lookup_streetNameRepository.GetAll()
				.Select(leadStreetName => new MyLeadStreetNameLookupTableDto
				{
					Id = leadStreetName.Id,
					DisplayName = leadStreetName == null || leadStreetName.Name == null ? "" : leadStreetName.Name.ToString().ToUpper()
				}).ToListAsync();
		}

		public async Task<List<MyLeadStreetTypeLookupTableDto>> GetAllStreetTypeForStreetTypeTableDropdown()
		{
			return await _lookup_streetTypeRepository.GetAll()
				.Select(leadStreetType => new MyLeadStreetTypeLookupTableDto
				{
					Id = leadStreetType.Id,
					DisplayName = leadStreetType == null || leadStreetType.Name == null ? "" : leadStreetType.Name.ToString().ToUpper()
				}).ToListAsync();
		}

		public async Task<List<MyLeadStatusLookupTableDto>> GetAllLeadStatusForTableDropdown(string lead)
		{
			return await _lookup_leadStatusRepository.GetAll()
				.WhereIf(!string.IsNullOrWhiteSpace(lead), c => c.Status.ToLower().Contains(lead.ToLower()))
				.Select(leadStatus => new MyLeadStatusLookupTableDto
				{
					Value = leadStatus.Id,
					Name = leadStatus == null || leadStatus.Status == null ? "" : leadStatus.Status.ToString()
				}).ToListAsync();
		}

		public NameValue<string> SendAndGetSelectedValue(NameValue<string> selectedValue)
		{
			return selectedValue;
		}

		public int GetSuburbId(string PostCode)
		{
			return _lookup_postCodeRepository.GetAll().Where(P => P.PostalCode == PostCode).Select(P => P.Id).FirstOrDefault();
		}

		public int StateId(string StateName)
		{
			return _lookup_stateRepository.GetAll().Where(P => P.Name == StateName).Select(P => P.Id).FirstOrDefault();
		}

		public async Task<GetMyLeadForAssignOrTransferOutput> GetLeadForAssignOrTransfer(EntityDto input)
		{
			var lead = await _leadRepository.GetAsync(input.Id);
			var output = new GetMyLeadForAssignOrTransferOutput();
			output.CompanyName = lead.CompanyName;
			output.Id = lead.Id;

			if (output.AssignToUserID != null)
			{
				var _lookupUser = await _userRepository.FirstOrDefaultAsync((int)output.AssignToUserID);
				output.UserName = _lookupUser?.FullName?.ToString();
			}
			return output;
		}

		public async Task<List<MyLeadUsersLookupTableDto>> GetAllUserLeadAssignDropdown()
		{
			var User_List = _userRepository
		   .GetAll();

			var UserList = (from user in User_List
							join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
							from ur in urJoined.DefaultIfEmpty()

							join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
							from us in usJoined.DefaultIfEmpty()

							where (us != null && us.DisplayName == "Sales Rep" || us.DisplayName == "Sales Manager")

							select new MyLeadUsersLookupTableDto()
							{
								Id = user.Id,
								DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
							});

			return new List<MyLeadUsersLookupTableDto>(
				await UserList.ToListAsync()
			);
		}

		public async Task AssignOrTransferLead(GetMyLeadForAssignOrTransferOutput input)
		{
			var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);
			
			if (lead.AssignToUserID == null)
			{
				lead.AssignToUserID = input.AssignToUserID;
				lead.LeadStatusId = 2;
				await _leadRepository.InsertAsync(lead);

				LeadActivityLog leadactivity = new LeadActivityLog();
				leadactivity.ActionId = 3;
				leadactivity.SectionId = 0;
				leadactivity.ActionNote = "Lead Assign";
				leadactivity.LeadId = lead.Id;
				if (AbpSession.TenantId != null)
				{
					leadactivity.TenantId = (int)AbpSession.TenantId;
				}
				await _leadactivityRepository.InsertAsync(leadactivity);
			}
			else
			{
				lead.AssignToUserID = input.AssignToUserID;
				lead.LeadStatusId = 2;
				await _leadRepository.InsertAsync(lead);

				LeadActivityLog leadactivity = new LeadActivityLog();
				leadactivity.ActionId = 4;
				leadactivity.SectionId = 0;
				leadactivity.ActionNote = "Lead Transfer";				
				leadactivity.LeadId = lead.Id;
				if (AbpSession.TenantId != null)
				{
					leadactivity.TenantId = (int)AbpSession.TenantId;
				}
				await _leadactivityRepository.InsertAsync(leadactivity);
			}
		}

		public async Task<List<GetMyLeadsActivityLogViewDto>> GetLeadActivityLog(GetMyLeadsActivityLogInput input)
		{
			var Result = _leadactivityRepository.GetAll().Where(L => L.LeadId == input.LeadId);

			var leads = from o in Result
						join o1 in _lookup_leadActionRepository.GetAll() on o.ActionId equals o1.Id into j1
						from s1 in j1.DefaultIfEmpty()

						join o2 in _leadRepository.GetAll() on o.LeadId equals o2.Id into j2
						from s2 in j2.DefaultIfEmpty()

						join o3 in _userRepository.GetAll() on o.LeadId equals o3.Id into j3
						from s3 in j3.DefaultIfEmpty()

						select new GetMyLeadsActivityLogViewDto()
						{
							ActionName = s1 == null || s1.ActionName == null ? "" : s1.ActionName.ToString(),
							ActionId = o.ActionId,
							ActionNote = o.ActionNote,
							LeadId = o.LeadId,
							CreationTime = o.CreationTime,
							CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
							LeadCompanyName = s2 == null || s2.CompanyName == null ? "" : s2.CompanyName.ToString(),

                            Section = _SectionAppService.GetAll().Where(e => e.SectionId == o.SectionId).Select(e => e.SectionName).FirstOrDefault(),
                        };

			return new List<GetMyLeadsActivityLogViewDto>(
				await leads.ToListAsync()
			);
		}

		public GetMyLeadCountsDto GetLeadCounts()
		{
			var Result = new GetMyLeadCountsDto();

			Result.New = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 1).Count());
			Result.UnHandled = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 2).Count());
			Result.Cold = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 3).Count());
			Result.Warm = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 4).Count());
			Result.Hot = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 5).Count());
			Result.Upgrade = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 6).Count());
			Result.Rejected = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 7).Count());
			Result.Cancelled = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 8).Count());
			Result.Closed = Convert.ToString(_leadRepository.GetAll().Where(l => l.LeadStatusId == 9).Count());
			Result.New = Convert.ToString(_leadRepository.GetAll().Count());
			return Result;
		}

		public async Task<List<GetMyLeadForViewDto>> GetUserLeadForDashboard()
		{
			int UserId = (int)AbpSession.UserId;

			var Leads = _leadRepository.GetAll()						
						.Where(e => e.AssignToUserID == UserId);

			var listdata = Leads.MapTo<List<GetMyLeadForViewDto>>();

			return new List<GetMyLeadForViewDto>(listdata);
		}
	}
}