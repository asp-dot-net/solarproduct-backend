﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.MyLeads.Dtos;
using Abp.Authorization;
using TheSolarProduct.Authorization;

namespace TheSolarProduct.Myleads.Exporting
{
    [AbpAuthorize(AppPermissions.Pages)]
    public class MyLeadsExcelExporter : NpoiExcelExporterBase, IMyleadsExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public MyLeadsExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetMyLeadForViewDto> leads)
        {
            return CreateExcelPackage(
                "Leads.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("Leads"));

                    AddHeader(
                        sheet,
                        L("CopanyName"),
                        L("Email"),
                        L("Phone"),
                        L("Mobile"),
                        L("Address"),
                        L("Requirements"),
                        (L("PostCode")) + L("Suburb"),
                        (L("State")) + L("Name"),
                        (L("PostCode")) + L("PostalCode"),
                        (L("LeadSource")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, leads,
                        _ => _.Lead.CompanyName,
                        _ => _.Lead.Email,
                        _ => _.Lead.Phone,
                        _ => _.Lead.Mobile,
                        _ => _.Lead.Address,
                        _ => _.Lead.Requirements,
                        _ => _.PostCodeSuburb,
                        _ => _.StateName,
                        _ => _.Lead.PostCode,
                        _ => _.LeadSourceName
                        );

					
					
                });
        }
    }
}
