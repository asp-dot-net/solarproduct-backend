﻿using System.Collections.Generic;
using TheSolarProduct.MyLeads.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Myleads.Exporting
{
    public interface IMyleadsExcelExporter
    {
        FileDto ExportToFile(List<GetMyLeadForViewDto> leads);
    }

}