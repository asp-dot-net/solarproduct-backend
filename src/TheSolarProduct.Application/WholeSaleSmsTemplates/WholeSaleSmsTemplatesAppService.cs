﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.SmsTemplates.Exporting;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Organizations;
using TheSolarProduct.WholeSaleSmsTemplates.Dtos;
using Abp.Authorization;
using TheSolarProduct.Authorization;
using TheSolarProduct.SmsTemplates;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Wholesales.DataVault;
using TheSolarProduct.Quotations;

namespace TheSolarProduct.WholeSaleSmsTemplates
{
    [AbpAuthorize]
    public class WholeSaleSmsTemplatesAppService : TheSolarProductAppServiceBase, IWholeSaleSmsTemplatesAppService
    {
        private readonly IRepository<WholeSaleSmsTemplate> _wholeSalesmsTemplateRepository;
        private readonly IWholeSaleSmsTemplatesExcelExporter _wholeSalesmsTemplatesExcelExporter;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<WholeSaleDataVaultActivityLogs> _WholeSaleDataVaultActivityRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public WholeSaleSmsTemplatesAppService(
            IRepository<WholeSaleSmsTemplate> wholeSalesmsTemplateRepository,
             IWholeSaleSmsTemplatesExcelExporter wholeSalesmsTemplatesExcelExporter,
             IRepository<OrganizationUnit, long> organizationUnitRepository,
             IRepository<WholeSaleDataVaultActivityLogs> WholeSaleDataVaultActivityRepository,
             IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            )
        {
            _wholeSalesmsTemplateRepository = wholeSalesmsTemplateRepository;
            _wholeSalesmsTemplatesExcelExporter = wholeSalesmsTemplatesExcelExporter;
            _organizationUnitRepository = organizationUnitRepository;
            _WholeSaleDataVaultActivityRepository = WholeSaleDataVaultActivityRepository;
            _dbcontextprovider = dbcontextprovider;
        }

        public async Task CreateOrEdit(CreateOrEditWholeSaleSmsTemplateDto input)
        {

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditWholeSaleSmsTemplateDto input)
        {
            var wholeSalesmsTemplates = ObjectMapper.Map<WholeSaleSmsTemplate>(input);

            if (AbpSession.TenantId != null)
            {
                wholeSalesmsTemplates.TenantId = (int)AbpSession.TenantId;
            }
            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 3;
            dataVaultLog.ActionNote = "Sms Templates Created : " + input.Name;
            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

            await _wholeSalesmsTemplateRepository.InsertAsync(wholeSalesmsTemplates);

        }

        protected virtual async Task Update(CreateOrEditWholeSaleSmsTemplateDto input)
        {
            var wholeSalesmsTemplates = await _wholeSalesmsTemplateRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 3;
            dataVaultLog.ActionNote = "Sms Templates Updated : " + wholeSalesmsTemplates.Name;

            var dataVaultLogId = await _WholeSaleDataVaultActivityRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<WholeSaleDataVaultActivityLogHistory>();

            if (input.Name != wholeSalesmsTemplates.Name)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = wholeSalesmsTemplates.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.Text != wholeSalesmsTemplates.Text)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "Text";
                history.PrevValue = wholeSalesmsTemplates.Text;
                history.CurValue = input.Text;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
            if (input.OrganizationUnitId != wholeSalesmsTemplates.OrganizationUnitId)
            {
                WholeSaleDataVaultActivityLogHistory history = new WholeSaleDataVaultActivityLogHistory();
                history.FieldName = "OrganizationUnitId";
                history.PrevValue = wholeSalesmsTemplates.OrganizationUnitId.ToString();
                history.CurValue = input.OrganizationUnitId.ToString();
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }
           
            await _dbcontextprovider.GetDbContext().WholeSaleDataVaultActivityLogHistorys.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, wholeSalesmsTemplates);

        }

        public async Task Delete(EntityDto input)
        {
           

            var Name = _wholeSalesmsTemplateRepository.Get(input.Id).Name;
            await _wholeSalesmsTemplateRepository.DeleteAsync(input.Id);

            var dataVaultLog = new WholeSaleDataVaultActivityLogs();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 3;
            dataVaultLog.ActionNote = "Sms Templates Deleted : " + Name;

            await _WholeSaleDataVaultActivityRepository.InsertAsync(dataVaultLog);

        }

        public async Task<PagedResultDto<GetWholeSaleSmsTemplateForViewDto>> GetAll(GetAllWholeSaleSmsTemplatesInput input)
        {
            var filteredSmsTemplates = _wholeSalesmsTemplateRepository.GetAll()
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Text.Contains(input.Filter));
            var pagedAndFilteredSmsTemplates = filteredSmsTemplates
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var wholeSalesmsTemplates = from o in pagedAndFilteredSmsTemplates
                               select new
                               {
                                   o.Name,
                                   o.Text,
                                   Id = o.Id,
                                   Organization = o.OrganizationUnitId
                               };

            var totalCount = await filteredSmsTemplates.CountAsync();

            var dbList = await wholeSalesmsTemplates.ToListAsync();
            var results = new List<GetWholeSaleSmsTemplateForViewDto>();

            foreach (var o in dbList)
            {
                var res = new GetWholeSaleSmsTemplateForViewDto()
                {
                    WholeSaleSmsTemplate = new WholeSaleSmsTemplateDto
                    {

                        Name = o.Name,
                        Text = o.Text,
                        Id = o.Id,

                    },
                    Organization = _organizationUnitRepository.GetAll().Where(e => e.Id == o.Organization).Select(e => e.DisplayName).FirstOrDefault()
                };

                results.Add(res);
            }

            return new PagedResultDto<GetWholeSaleSmsTemplateForViewDto>(
               totalCount,
               results
           );
        }

        public async Task<GetWholeSaleSmsTemplateForEditOutput> GetWholeSaleSmsTemplateForEdit(EntityDto input)
        {
            var wholeSalesmsTemplates = await _wholeSalesmsTemplateRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetWholeSaleSmsTemplateForEditOutput { wholeSaleSmsTemplate = ObjectMapper.Map<CreateOrEditWholeSaleSmsTemplateDto>(wholeSalesmsTemplates) };

            return output;
        }

        public async Task<GetWholeSaleSmsTemplateForViewDto> GetWholeSaleSmsTemplateForView(int id)
        {
            var wholeSalesmsTemplates = await _wholeSalesmsTemplateRepository.GetAsync(id);

            var output = new GetWholeSaleSmsTemplateForViewDto { WholeSaleSmsTemplate = ObjectMapper.Map<WholeSaleSmsTemplateDto>(wholeSalesmsTemplates) };

            return output;
        }

        public async Task<FileDto> GetWholeSaleSmsTemplatesToExcel(GetAllWholeSaleSmsTemplatesForExcelInput input)
        {
            var filteredSmsTemplates = _wholeSalesmsTemplateRepository.GetAll()
                       .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Text.Contains(input.Filter));

            var query = (from o in filteredSmsTemplates
                         select new GetWholeSaleSmsTemplateForViewDto()
                         {
                             WholeSaleSmsTemplate = new WholeSaleSmsTemplateDto
                             {
                                 Name = o.Name,
                                 Text = o.Text,
                                 Id = o.Id
                             }
                         });

            var smsTemplateListDtos = await query.ToListAsync();

            return _wholeSalesmsTemplatesExcelExporter.ExportToFile(smsTemplateListDtos);
        }
    }
}
