﻿using System.Collections.Generic;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.WholeSaleSmsTemplates.Dtos;

namespace TheSolarProduct.SmsTemplates.Exporting
{
    public interface IWholeSaleSmsTemplatesExcelExporter
    {
        FileDto ExportToFile(List<GetWholeSaleSmsTemplateForViewDto> wholeSalesmsTemplates);
    }
}