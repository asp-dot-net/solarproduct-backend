﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;
using TheSolarProduct.WholeSaleSmsTemplates.Dtos;

namespace TheSolarProduct.SmsTemplates.Exporting
{
    public class WholeSaleSmsTemplatesExcelExporter : NpoiExcelExporterBase, IWholeSaleSmsTemplatesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public WholeSaleSmsTemplatesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetWholeSaleSmsTemplateForViewDto> wholeSalesmsTemplates)
        {
            return CreateExcelPackage(
                "SmsTemplates.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("SmsTemplates"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Text")
                        );

                    AddObjects(
                        sheet, 2, wholeSalesmsTemplates,
                        _ => _.WholeSaleSmsTemplate.Name,
                        _ => _.WholeSaleSmsTemplate.Text
                        );

                });
        }
    }
}