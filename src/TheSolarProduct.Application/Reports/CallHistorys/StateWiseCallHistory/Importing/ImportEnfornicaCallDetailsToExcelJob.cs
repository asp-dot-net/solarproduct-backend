﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Threading;
using Abp.UI;
using System;
using System.Collections.Generic;
using TheSolarProduct.CallHistory;
using TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Dto;
using TheSolarProduct.Notifications;
using TheSolarProduct.Storage;
using TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing.Dto;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing
{
    public class ImportEnfornicaCallDetailsToExcelJob : BackgroundJob<ImportEnfornicaCallDetailsFromExcelArgs>, ITransientDependency
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IEnfornicaCallDetailsExcelDataReader _enfornicaCallDetailsExcelDataReader;
        private readonly IAppNotifier _appNotifier;
        private readonly IInvalidEnfornicaCallDetailsExporter _invalidEnfornicaCallDetailsExporter;
        private readonly IRepository<EnfornicaCallDetails> _enfornicaCallDetailsRepository;

        public ImportEnfornicaCallDetailsToExcelJob(IUnitOfWorkManager unitOfWorkManager
            , IBinaryObjectManager binaryObjectManager
            , IEnfornicaCallDetailsExcelDataReader enfornicaCallDetailsExcelDataReader
            , IAppNotifier appNotifier
            , IInvalidEnfornicaCallDetailsExporter invalidEnfornicaCallDetailsExporter
            , IRepository<EnfornicaCallDetails> enfornicaCallDetailsRepository
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _binaryObjectManager = binaryObjectManager;
            _enfornicaCallDetailsExcelDataReader = enfornicaCallDetailsExcelDataReader;
            _appNotifier = appNotifier;
            _invalidEnfornicaCallDetailsExporter = invalidEnfornicaCallDetailsExporter;
            _enfornicaCallDetailsRepository = enfornicaCallDetailsRepository;
        }

        [UnitOfWork]
        public override void Execute(ImportEnfornicaCallDetailsFromExcelArgs args)
        {
            var enfornicaCallDetail = GetEnfornicaCallDetailFromExcelOrNull(args);
            if (enfornicaCallDetail == null || !enfornicaCallDetail.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidEnfornicaCallDetail = new List<ImportEnfornicaCallDetailDto>();

                foreach (var stc in enfornicaCallDetail)
                {
                    if (stc.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateEnfornicaCallDetailAsync(stc, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            invalidEnfornicaCallDetail.Add(stc);
                        }
                        catch (Exception e)
                        {
                            invalidEnfornicaCallDetail.Add(stc);
                        }
                    }
                    else
                    {
                        invalidEnfornicaCallDetail.Add(stc);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportEnfornicaCallDetailsResultAsync(args, invalidEnfornicaCallDetail));
                    }
                    uow.Complete();
                }
            }
        }

        private List<ImportEnfornicaCallDetailDto> GetEnfornicaCallDetailFromExcelOrNull(ImportEnfornicaCallDetailsFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        return _enfornicaCallDetailsExcelDataReader.GetEnfornicaCallDetailFromExcel(file.Bytes);
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private void SendInvalidExcelNotification(ImportEnfornicaCallDetailsFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToEnfornicaCallDetail", TheSolarProductConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task CreateEnfornicaCallDetailAsync(ImportEnfornicaCallDetailDto input, ImportEnfornicaCallDetailsFromExcelArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (_unitOfWorkManager.Current.SetTenantId(args.TenantId))
                {
                    if (input.CreateMethod == "INCOMING_CALL")
                    {
                        var enfornicaCallDetails = await _enfornicaCallDetailsRepository.GetAll().Where(e => e.CreationTime == input.CreationDate && e.From == input.From).FirstOrDefaultAsync();

                        if (enfornicaCallDetails != null)
                        {
                            enfornicaCallDetails.CreateMethod = input.CreateMethod;
                            enfornicaCallDetails.StartTime = input.StartTime;
                            enfornicaCallDetails.EndTime = input.EndTime;
                            enfornicaCallDetails.AnswerTime = input.AnswerTime;
                            enfornicaCallDetails.AnswerDuration = input.AnswerDuration;
                            enfornicaCallDetails.FromLocationRegionCode = input.FromLocationRegionCode;
                            enfornicaCallDetails.FromLocationAdministrativeArea = input.FromLocationAdministrativeArea;
                            enfornicaCallDetails.FromLocationLocality = input.FromLocationLocality;
                            enfornicaCallDetails.FromLocationCoordinatesLatitude = input.FromLocationCoordinatesLatitude;
                            enfornicaCallDetails.FromLocationCoordinatesLongitude = input.FromLocationCoordinatesLongitude;
                            enfornicaCallDetails.LabelsMoli = input.LabelsMoli;
                            enfornicaCallDetails.PriceCurrencyCode = input.PriceCurrencyCode;
                            enfornicaCallDetails.ToDisplayName = input.ToDisplayName;
                            enfornicaCallDetails.RingTime = input.RingTime;
                            enfornicaCallDetails.Cost = input.Cost;
                            enfornicaCallDetails.LastModifierUserId = args.User.UserId;
                            enfornicaCallDetails.OrganizationUnitId = args.OrganizationId;
                            await _enfornicaCallDetailsRepository.UpdateAsync(enfornicaCallDetails);
                        }
                        else
                        {
                            EnfornicaCallDetails newEnfornicaCallDetails = new EnfornicaCallDetails();
                            newEnfornicaCallDetails.CreationTime = input.CreationDate;
                            newEnfornicaCallDetails.From = input.From;
                            newEnfornicaCallDetails.CreateMethod = input.CreateMethod;
                            newEnfornicaCallDetails.StartTime = input.StartTime;
                            newEnfornicaCallDetails.EndTime = input.EndTime;
                            newEnfornicaCallDetails.AnswerTime = input.AnswerTime;
                            newEnfornicaCallDetails.AnswerDuration = input.AnswerDuration;
                            newEnfornicaCallDetails.FromLocationRegionCode = input.FromLocationRegionCode;
                            newEnfornicaCallDetails.FromLocationAdministrativeArea = input.FromLocationAdministrativeArea;
                            newEnfornicaCallDetails.FromLocationLocality = input.FromLocationLocality;
                            newEnfornicaCallDetails.FromLocationCoordinatesLatitude = input.FromLocationCoordinatesLatitude;
                            newEnfornicaCallDetails.FromLocationCoordinatesLongitude = input.FromLocationCoordinatesLongitude;
                            newEnfornicaCallDetails.LabelsMoli = input.LabelsMoli;
                            newEnfornicaCallDetails.PriceCurrencyCode = input.PriceCurrencyCode;
                            newEnfornicaCallDetails.ToDisplayName = input.ToDisplayName;
                            newEnfornicaCallDetails.RingTime = input.RingTime;
                            newEnfornicaCallDetails.Cost = input.Cost;
                            newEnfornicaCallDetails.CreatorUserId = args.User.UserId;
                            newEnfornicaCallDetails.OrganizationUnitId = args.OrganizationId;
                            await _enfornicaCallDetailsRepository.InsertAsync(newEnfornicaCallDetails);
                        }

                    }

                }
                uow.Complete();
            }
        }

        private async Task ProcessImportEnfornicaCallDetailsResultAsync(ImportEnfornicaCallDetailsFromExcelArgs args, List<ImportEnfornicaCallDetailDto> invalidstc)
        {
            if (invalidstc.Any())
            {
                var file = _invalidEnfornicaCallDetailsExporter.ExportToFile(invalidstc);
                await _appNotifier.SomeEnfornicaCallDetailCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllEnfornicaCallDetailSuccessfullyImportedFromExcel", TheSolarProductConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }
    }
}
