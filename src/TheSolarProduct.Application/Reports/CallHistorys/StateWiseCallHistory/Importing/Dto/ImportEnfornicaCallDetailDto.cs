﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing.Dto
{
    public class ImportEnfornicaCallDetailDto
    {
        public DateTime CreationDate { get; set; }

        public int CreateUser { get; set; }

        public string From { get; set; }

        public string CreateTime { get; set; }

        public string CreateMethod { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public DateTime AnswerTime { get; set; }

        public string AnswerDuration { get; set; }

        public string FromLocationRegionCode { get; set; }

        public string FromLocationAdministrativeArea { get; set; }

        public string FromLocationLocality { get; set; }

        public string FromLocationCoordinatesLatitude { get; set; }

        public string FromLocationCoordinatesLongitude { get; set; }

        public string LabelsMoli { get; set; }

        public string PriceCurrencyCode { get; set; }

        public string ToDisplayName { get; set; }

        public string RingTime { get; set; }

        public string Cost { get; set; }

        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
