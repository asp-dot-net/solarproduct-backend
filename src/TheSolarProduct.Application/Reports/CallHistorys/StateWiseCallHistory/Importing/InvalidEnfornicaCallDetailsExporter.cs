﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing
{
    public class InvalidEnfornicaCallDetailsExporter : NpoiExcelExporterBase, IInvalidEnfornicaCallDetailsExporter, ITransientDependency
    {
        public InvalidEnfornicaCallDetailsExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {

        }

        public FileDto ExportToFile(List<ImportEnfornicaCallDetailDto> enfornicaCallDetailDtos)
        {
            return CreateExcelPackage("InvalidEnfornicaCallDetail.xlsx",
              excelPackage =>
              {
                  var sheet = excelPackage.CreateSheet("InvalidEnfornicaCallDetail");


                  AddHeader(sheet,
                      "CreateTime",
                      "From",
                      "CreateMethod",
                      "StartTime",
                      "EndTime",
                      "AnswerTime",
                      "AnswerDuration",
                      "FromLocationRegionCode",
                      "FromLocationAdministrativeArea",
                      "FromLocationLocality",
                      "FromLocationCoordinatesLatitude",
                      "FromLocationCoordinatesLongitude",
                      "LabelsMoli",
                      "PriceCurrencyCode",
                      "ToDisplayName",
                      "RingTime",
                      "Cost",
                      "Exception"
                      );


                  AddObjects(
                        sheet, 2, enfornicaCallDetailDtos,
                        _ => _.CreationDate,
                        _ => _.From,
                        _ => _.CreateMethod,
                        _ => _.StartTime,
                        _ => _.EndTime,
                        _ => _.AnswerTime,
                        _ => _.AnswerDuration,
                        _ => _.FromLocationRegionCode,
                        _ => _.FromLocationAdministrativeArea,
                        _ => _.FromLocationLocality,
                        _ => _.FromLocationCoordinatesLatitude,
                        _ => _.FromLocationCoordinatesLongitude,
                        _ => _.LabelsMoli,
                        _ => _.PriceCurrencyCode,
                        _ => _.ToDisplayName,
                        _ => _.RingTime,
                        _ => _.Cost,
                        _ => _.Exception
                    );
              });
        }
    }
}
