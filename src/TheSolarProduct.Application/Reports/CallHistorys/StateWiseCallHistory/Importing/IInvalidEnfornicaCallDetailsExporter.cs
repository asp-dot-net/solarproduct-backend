﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing.Dto;

namespace TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing
{
    public interface IInvalidEnfornicaCallDetailsExporter
    {
        FileDto ExportToFile(List<ImportEnfornicaCallDetailDto> enfornicaCallDetailDtos);
    }
}
