﻿using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing.Dto;
using System.Linq;

namespace TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing
{
    public class EnfornicaCallDetailsExcelDataReader : NpoiExcelImporterBase<ImportEnfornicaCallDetailDto>, IEnfornicaCallDetailsExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public EnfornicaCallDetailsExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(TheSolarProductConsts.LocalizationSourceName);
        }

        public List<ImportEnfornicaCallDetailDto> GetEnfornicaCallDetailFromExcel(byte[] fileBytes)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow);
        }

        private ImportEnfornicaCallDetailDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (IsRowEmpty(worksheet, row))
            {
                return null;
            }

            var enfornicaCallDetail = new ImportEnfornicaCallDetailDto();

            IRow roww = worksheet.GetRow(row);
            List<ICell> cells = roww.Cells;
            List<string> rowData = new List<string>();

            //for (int colNumber = 0; colNumber < 16; colNumber++)
            //{ 
            //    ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
            //    rowData.Add(cell.ToString());
            //}

            try
            {
                enfornicaCallDetail.From = Convert.ToString(cells[0].ToString());
                enfornicaCallDetail.CreateMethod = Convert.ToString(cells[1].ToString());
                enfornicaCallDetail.CreationDate = DateTime.Parse(cells[2].DateCellValue.ToString());
                enfornicaCallDetail.StartTime = DateTime.Parse(cells[3].DateCellValue.ToString());
                enfornicaCallDetail.AnswerTime = DateTime.Parse(cells[4].DateCellValue.ToString());
                enfornicaCallDetail.EndTime = DateTime.Parse(cells[5].DateCellValue.ToString());
                enfornicaCallDetail.AnswerDuration = Convert.ToString(cells[6].ToString());
                enfornicaCallDetail.FromLocationRegionCode = Convert.ToString(cells[7].ToString());
                enfornicaCallDetail.FromLocationAdministrativeArea = Convert.ToString(cells[8].ToString());
                enfornicaCallDetail.FromLocationLocality = Convert.ToString(cells[9].ToString());
                enfornicaCallDetail.FromLocationCoordinatesLatitude = Convert.ToString(cells[10].ToString());
                enfornicaCallDetail.FromLocationCoordinatesLongitude = Convert.ToString(cells[11].ToString());
                enfornicaCallDetail.LabelsMoli = Convert.ToString(cells[12].ToString());
                enfornicaCallDetail.PriceCurrencyCode = Convert.ToString(cells[13].ToString());
                enfornicaCallDetail.ToDisplayName = Convert.ToString(cells[14].ToString());
                enfornicaCallDetail.Cost = Convert.ToString(cells[15].ToString());


                //enfornicaCallDetail.From = Convert.ToString(rowData[2]);
                //enfornicaCallDetail.CreateMethod = Convert.ToString(rowData[15]);
                //enfornicaCallDetail.CreationDate = DateTime.Parse(rowData[17], CultureInfo.CreateSpecificCulture("en-AU"));
                //enfornicaCallDetail.StartTime = DateTime.Parse(rowData[18], CultureInfo.CreateSpecificCulture("en-AU"));
                //enfornicaCallDetail.AnswerTime = DateTime.Parse(rowData[19], CultureInfo.CreateSpecificCulture("en-AU"));
                //enfornicaCallDetail.EndTime = DateTime.Parse(rowData[20], CultureInfo.CreateSpecificCulture("en-AU"));
                //enfornicaCallDetail.AnswerDuration = Convert.ToString(rowData[21]);
                //enfornicaCallDetail.FromLocationRegionCode = Convert.ToString(rowData[22]);
                //enfornicaCallDetail.FromLocationAdministrativeArea = Convert.ToString(rowData[23]);
                //enfornicaCallDetail.FromLocationLocality = Convert.ToString(rowData[24]);
                //enfornicaCallDetail.FromLocationCoordinatesLatitude = Convert.ToString(rowData[25]);
                //enfornicaCallDetail.FromLocationCoordinatesLongitude = Convert.ToString(rowData[26]);
                //enfornicaCallDetail.LabelsMoli = Convert.ToString(rowData[27]);
                //enfornicaCallDetail.PriceCurrencyCode = Convert.ToString(rowData[28]);
                //enfornicaCallDetail.ToDisplayName = Convert.ToString(rowData[29]);
                //enfornicaCallDetail.Cost = Convert.ToString(rowData[31]);
            }

            catch (System.Exception exception)
            {
                enfornicaCallDetail.Exception = exception.Message;
            }


            return enfornicaCallDetail;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0} IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.NumericCellValue.ToString()) || cell.NumericCellValue == 0;
        }
    }
}
