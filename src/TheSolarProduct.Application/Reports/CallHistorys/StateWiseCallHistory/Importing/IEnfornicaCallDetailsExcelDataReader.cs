﻿using Abp.Dependency;
using System.Collections.Generic;
using TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing.Dto;

namespace TheSolarProduct.Reports.CallHistorys.StateWiseCallHistory.Importing
{
    public interface IEnfornicaCallDetailsExcelDataReader : ITransientDependency
    {
        List<ImportEnfornicaCallDetailDto> GetEnfornicaCallDetailFromExcel(byte[] fileBytes);
    }
}
