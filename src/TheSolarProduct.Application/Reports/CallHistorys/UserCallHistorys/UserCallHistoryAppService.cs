﻿using System.Collections.Generic;
using TheSolarProduct.Leads;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Leads.Exporting;
using Abp.Timing.Timezone;
using TheSolarProduct.Timing;
using TheSolarProduct.CallHistory;
using TheSolarProduct.CallFlowQueues;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;

namespace TheSolarProduct.Reports.CallHistorys.UserCallHistorys
{
    [AbpAuthorize]
    public class UserCallHistoryAppService : TheSolarProductAppServiceBase, IUserCallHistoryAppService
    {
        private readonly IRepository<CallHistory.CallHistory> _callHistoryRepository;
        private readonly IRepository<CallType> _callTypeRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<Lead> _leadRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<EnfornicaCallDetails> _enfornicaCallDetailsRepository;
        private readonly IRepository<CallFlowQueue> _callFlowQueueRepository;
        private readonly IRepository<CallFlowQueueOrganizationUnit> _callFlowQueueOrganizationUnitRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;

        public UserCallHistoryAppService(
            IRepository<CallHistory.CallHistory> callHistoryRepository
            , IRepository<CallType> callTypeRepository
            , IRepository<User, long> userRepository
            , ITimeZoneConverter timeZoneConverter
            , IRepository<Lead> leadRepository
            , ILeadsExcelExporter leadsExcelExporter
            , ITimeZoneService timeZoneService
            , IRepository<EnfornicaCallDetails> enfornicaCallDetailsRepository
            , IRepository<CallFlowQueue> callFlowQueueRepository
            , IRepository<CallFlowQueueOrganizationUnit> callFlowQueueOrganizationUnitRepository
            , UserManager userManager
            , IRepository<UserTeam> userTeamRepository
            )
        {
            _callHistoryRepository = callHistoryRepository;
            _callTypeRepository = callTypeRepository;
            _userRepository = userRepository;
            _timeZoneConverter = timeZoneConverter;
            _leadRepository = leadRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _timeZoneService = timeZoneService;
            _enfornicaCallDetailsRepository = enfornicaCallDetailsRepository;
            _callFlowQueueRepository = callFlowQueueRepository;
            _callFlowQueueOrganizationUnitRepository = callFlowQueueOrganizationUnitRepository;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
        }

        public async Task<PagedResultDto<GetAllUserCallHistoryDto>> GetAll(GetAllUserCallHistoryInputDto input)
        {
            #region Old Host Code
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var callHistory = new List<GetAllUserCallHistoryDto>();
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var users = _userRepository.GetAll().Where(e => e.ExtensionNumber != null && e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnitId))
            //            .WhereIf(input.UserId != 0, e => e.Id == input.UserId)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id);

            //var _users = users.ToList();
            //var totalCount = 0;

            //using (var uow = UnitOfWorkManager.Begin())
            //{
            //    using (CurrentUnitOfWork.SetTenantId(null))
            //    {
            //        using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            //        {
            //            var filtered = _callHistoryRepository.GetAll()
            //                .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number == input.Mobile)
            //                .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
            //                .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date).ToList();
            //            //.WhereIf(!string.IsNullOrEmpty(input.State), e => e.State == input.State).ToList();

            //            var res = from o in _users

            //                      select new GetAllUserCallHistoryDto()
            //                      {
            //                          Id = (int)o.Id,
            //                          UserName = o.FullName.ToString(),
            //                          Missed = filtered.Where(e => e.CallType == "Missed" && e.Agent == o.ExtensionNumber).Count(),
            //                          Inbound = filtered.Where(e => e.CallType == "Inbound" && e.Agent == o.ExtensionNumber).Count(),
            //                          Outbound = filtered.Where(e => e.CallType == "Outbound" && e.Agent == o.ExtensionNumber).Count(),
            //                          Notanswered = filtered.Where(e => e.CallType == "Notanswered" && e.Agent == o.ExtensionNumber).Count(),
            //                          Agent = o.ExtensionNumber
            //                      };

            //            var summaryCount = new UserCallHistoryCount();
            //            summaryCount.TotalMissed = res.Sum(e => e.Missed);
            //            summaryCount.TotalInbound = res.Sum(e => e.Inbound);
            //            summaryCount.TotalOutbound = res.Sum(e => e.Outbound);
            //            summaryCount.TotalNotanswered = res.Sum(e => e.Notanswered);

            //            var pagedAndFiltered = res.Where(e => e.Inbound != 0 || e.Outbound != 0 || e.Notanswered != 0 || e.Missed != 0).AsQueryable()
            //                .OrderBy(input.Sorting ?? "id desc")
            //                .PageBy(input);

            //            callHistory = pagedAndFiltered.ToList();

            //            if (callHistory.Count() > 0)
            //                callHistory[0].Summary = summaryCount;

            //            totalCount = res.Where(e => e.Inbound != 0 || e.Outbound != 0 || e.Notanswered != 0 || e.Missed != 0).Count();
            //            uow.Complete();
            //        }
            //    }
            //}
            #endregion

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var users = _userRepository.GetAll().Where(e => e.ExtensionNumber != null && e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnitId))
                        .WhereIf(input.UserId != 0, e => e.Id == input.UserId)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                        .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id).ToList();

            //var _user = users.ToList();

            var filtered = _callHistoryRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number == input.Mobile)
                .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date).ToList();

            var res = from o in users

                      select new GetAllUserCallHistoryDto
                      {
                          Id = (int)o.Id,
                          UserName = o.FullName.ToString(),
                          Missed = filtered.Where(e => e.CallType == "Missed" && e.Agent == o.ExtensionNumber).Count(),
                          Inbound = filtered.Where(e => e.CallType == "Inbound" && e.Agent == o.ExtensionNumber).Count(),
                          Outbound = filtered.Where(e => e.CallType == "Outbound" && e.Agent == o.ExtensionNumber).Count(),
                          Notanswered = filtered.Where(e => e.CallType == "Notanswered" && e.Agent == o.ExtensionNumber).Count(),
                          Agent = o.ExtensionNumber
                      };

            var reSum = res.ToList();
            var summaryCount = new UserCallHistoryCount();
            summaryCount.TotalMissed = reSum.Sum(e => e.Missed);
            summaryCount.TotalInbound = reSum.Sum(e => e.Inbound);
            summaryCount.TotalOutbound = reSum.Sum(e => e.Outbound);
            summaryCount.TotalNotanswered = reSum.Sum(e => e.Notanswered);

            var pagedAndFiltered = res.Where(e => e.Inbound != 0 || e.Outbound != 0 || e.Notanswered != 0 || e.Missed != 0).AsQueryable()
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var callHistory = pagedAndFiltered.ToList();

            if (callHistory.Count() > 0)
            {
                callHistory[0].Summary = summaryCount;
            }

            var totalCount = res.Where(e => e.Inbound != 0 || e.Outbound != 0 || e.Notanswered != 0 || e.Missed != 0).Count();

            return new PagedResultDto<GetAllUserCallHistoryDto>(totalCount, callHistory);
        }

        public async Task<List<GetCallHistoryDetailDto>> GetUSerCallHistoryDetails(GetUserCallHistoryDetailsInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var showMobileNoPermission = PermissionChecker.IsGranted(Authorization.AppPermissions.Pages_CallHistory_UserCallHistory_ShowMobileNo);

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number == input.Mobile)
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date)
                            .Where(e => e.Agent == input.Agent && e.CallType == input.CallType);

            var res = from o in filtered

                      select new GetCallHistoryDetailDto()
                      {
                          Mobile = showMobileNoPermission == true ? o.Number : "+61*********",
                          Duration = o.Duration,
                          CreationTime = o.CallStartTimeUTC
                      };

            return await res.ToListAsync();
        }

        public async Task<FileDto> GetUserCallHistoryToExcel(GetAllUserCallHistoryExcelInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var callHistory = new List<GetAllUserCallHistoryDto>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var users = _userRepository.GetAll().Where(e => e.ExtensionNumber != null && e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnitId))
                .WhereIf(input.UserId != 0, e => e.Id == input.UserId)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id).ToList();

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number == input.Mobile)
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date).ToList();

            var res = from o in users

                      select new GetAllUserCallHistoryDto()
                      {
                          Id = (int)o.Id,
                          UserName = o.FullName.ToString(),
                          Missed = filtered.Where(e => e.CallType == "Missed" && e.Agent == o.ExtensionNumber).Count(),
                          Inbound = filtered.Where(e => e.CallType == "Inbound" && e.Agent == o.ExtensionNumber).Count(),
                          Outbound = filtered.Where(e => e.CallType == "Outbound" && e.Agent == o.ExtensionNumber).Count(),
                          Notanswered = filtered.Where(e => e.CallType == "Notanswered" && e.Agent == o.ExtensionNumber).Count(),
                          Agent = o.ExtensionNumber
                      };

            callHistory = res.Where(e => e.Inbound != 0 || e.Outbound != 0 || e.Notanswered != 0 || e.Missed != 0).ToList();

            if (input.excelorcsv == 1)
            {
                return _leadsExcelExporter.UserCallHistoryExport(callHistory, "UserCallHistory.xlsx");
            }
            else
            {
                return _leadsExcelExporter.UserCallHistoryExport(callHistory, "UserCallHistory.csv");
            }
        }

        public async Task<FileDto> GetUserCallHistoryDetailsToExcel(GetUserCallHistoryDetailsInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var showMobileNoPermission = PermissionChecker.IsGranted(Authorization.AppPermissions.Pages_CallHistory_UserCallHistory_ShowMobileNo);

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number == input.Mobile)
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date)
                            .Where(e => e.Agent == input.Agent && e.CallType == input.CallType);

            var res = from o in filtered

                      select new GetCallHistoryDetailDto()
                      {
                          Mobile = showMobileNoPermission == true ? o.Number : "+61*********",
                          Duration = o.Duration,
                          CreationTime = o.CallStartTimeUTC
                      };

            var callHistory = await res.ToListAsync();

            return _leadsExcelExporter.UserCallHistoryDetailsExport(callHistory, "UserCallHistoryDetails.xlsx");
        }

        public async Task<PagedResultDto<GetAllStateCallHistoryDto>> GetAllStateWiseCallHistory(GetAllStateCallHistoryInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var h = Convert.ToDouble("-" + diffHour);

            var filtered = _enfornicaCallDetailsRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.From == input.Mobile)
                            .WhereIf(input.StartDate != null, e => e.CreationTime.Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CreationTime.Date <= EDate.Value.Date)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.FromLocationAdministrativeArea == input.State)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Locality), e => e.FromLocationLocality.Contains(input.Locality))
                            .Where(e => e.OrganizationUnitId == input.OrganizationUnitId);

            var pagedAndFiltered = filtered
                            .OrderBy(input.Sorting ?? "id desc")
                            .PageBy(input);

            List<string> Statelist = new List<string> { "QLD", "SA", "NSW", "VIC" };
            var summaryCount = new StateCallHistoryCount();
            summaryCount.TotalQLD = filtered.Where(e => e.FromLocationAdministrativeArea == "QLD").Count();
            summaryCount.TotalSA = filtered.Where(e => e.FromLocationAdministrativeArea == "SA").Count();
            summaryCount.TotalNSW = filtered.Where(e => e.FromLocationAdministrativeArea == "NSW").Count();
            summaryCount.TotalVIC = filtered.Where(e => e.FromLocationAdministrativeArea == "VIC").Count();
            summaryCount.TotalOther = filtered.Where(e => !Statelist.Contains(e.FromLocationAdministrativeArea)).Count();

            var res = from o in pagedAndFiltered
                      select new GetAllStateCallHistoryDto()
                      {
                          Id = (int)o.Id,
                          FromLocationAdministrativeArea = o.FromLocationAdministrativeArea,
                          FromLocationLocality = o.FromLocationLocality,
                          CreationDate = o.CreationTime.AddHours(h),
                          StartTime = o.StartTime.AddHours(h),
                          EndTime = o.EndTime.AddHours(h),
                          AnswerDuration = o.AnswerDuration,
                          Cost = o.Cost,
                          Summary = summaryCount
                      };

            var totalCount = await filtered.CountAsync();



            return new PagedResultDto<GetAllStateCallHistoryDto>(totalCount, await res.ToListAsync());
        }

        public async Task<FileDto> GetStateWiseCallHistoryToExcel(GetAllStateCallHistoryExcelInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var h = Convert.ToDouble("-" + diffHour);

            var filtered = _enfornicaCallDetailsRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.From == input.Mobile)
                            .WhereIf(input.StartDate != null, e => e.CreationTime.Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CreationTime.Date <= EDate.Value.Date)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.FromLocationAdministrativeArea == input.State)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Locality), e => e.FromLocationLocality.Contains(input.Locality))
                            .Where(e => e.OrganizationUnitId == input.OrganizationUnitId).Select(e => new {e.Id, e.FromLocationLocality, e.FromLocationAdministrativeArea, e.CreationTime, e.StartTime, e.EndTime, e.AnswerDuration, e.Cost }).ToList();

            var res = from
                         o in filtered
                      select new GetAllStateCallHistoryDto()
                      {
                          Id = (int)o.Id,
                          FromLocationLocality = o.FromLocationLocality,
                          FromLocationAdministrativeArea = o.FromLocationAdministrativeArea,
                          CreationDate = o.CreationTime,
                          StartTime = o.StartTime,
                          EndTime = o.EndTime,
                          AnswerDuration = o.AnswerDuration,
                          Cost = o.Cost
                      };


            if (input.excelorcsv == 1)
            {
                return _leadsExcelExporter.StateWiseCallHistoryExport(res.ToList(), "StateWiseCallHistory.xlsx");
            }
            else
            {
                return _leadsExcelExporter.StateWiseCallHistoryExport(res.ToList(), "StateWiseCallHistory.csv");
            }
        }

        public async Task<PagedResultDto<GetAllCallQueueHistoryDto>> GetAllCallQueueCallHistory(GetCallQueueHistoryInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var organizationIDs = _callFlowQueueOrganizationUnitRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnitId).Select(e => e.CallFlowQueueId);
            var callQueue = _callFlowQueueRepository.GetAll().Where(e => organizationIDs.Contains(e.Id));

            var totalCount = 0;
            //List<string> userExtentionNumber = new List<string>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var userExtentionNumber = _userRepository.GetAll()
                .Where(e => e.ExtensionNumber != null )
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id)
                .Select(e => e.ExtensionNumber);

            var filtered = _callHistoryRepository.GetAll()
                            .Where(e => userExtentionNumber.Any(u => e.Agent == u) && !string.IsNullOrEmpty(e.QueueExtension) )
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number.Contains(input.Mobile))
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date).Select(e => new {e.Agent, e.QueueExtension}).OrderBy(e => e.Agent);

            var res = from o in callQueue

                      select new GetAllCallQueueHistoryDto()
                      {
                          Id = (int)o.Id,
                          Name = o.Name,
                          Count = filtered.Where(e => e.QueueExtension == o.ExtentionNumber.ToString()).Count(),
                          ExtentionNumber = o.ExtentionNumber,
                      };

            //res = res.Where(e => e.Count > 0);

            var pagedAndFiltered = res
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var callHistory = await pagedAndFiltered.ToListAsync();

            totalCount = await res.CountAsync();

            if (callHistory != null && callHistory.Count > 0)
            {
                callHistory[0].TotalCalls = res.ToList().Sum(e => e.Count);
            }

            return new PagedResultDto<GetAllCallQueueHistoryDto>(totalCount, callHistory);
        }

        public async Task<List<GetAllCallFlowQueueCountDetailsDto>> GetCallQueueDetailsCount(GetCallQueueDetailsCountInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var users = _userRepository.GetAll()
                .Where(e => e.ExtensionNumber != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id);

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number.Contains(input.Mobile))
                            .Where(e => e.QueueExtension == input.QueueExtentionnumber)
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date);

            var res = await (from o in users

                      select new GetAllCallFlowQueueCountDetailsDto()
                      {
                          User = o.FullName.ToString(),
                          Count = filtered.Where(e => e.Agent == o.ExtensionNumber).Count(),
                          ExtentionNumber = o.ExtensionNumber,
                      }).ToListAsync();

            var callHistory =  res.Where(e => e.Count != 0).ToList();

           // var r = callHistory.OrderBy(e=> e.ExtentionNumber).Select(e => new { e.ExtentionNumber, e.Count });

            return callHistory;
        }

        public async Task<List<GetAllCallFlowQueueCallHistoryDetailsDto>> GetCallQueueCallHistoryDetails(GetCallQueueDetailsCountInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var showMobileNoPermission = PermissionChecker.IsGranted(Authorization.AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_Detail_UserCallDetails_ShowMobileNo);

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number.Contains(input.Mobile))
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date)
                            .Where(e => e.Agent == input.ExtentionNumber)
                            .Where(e => e.QueueExtension == input.QueueExtentionnumber);

            var summaryCount = new GetcallFlowDetailSummary();
            summaryCount.TotalDuration = filtered.Sum(e => (Convert.ToInt32(e.Duration.Substring(0, 2)) * 60) + Convert.ToInt32(e.Duration.Substring(3, 5)));
            summaryCount.AvgDuration = filtered.Average(e => (Convert.ToInt32(e.Duration.Substring(0, 2)) * 60) + Convert.ToInt32(e.Duration.Substring(3, 5)));

            var res = from o in filtered

                      select new GetAllCallFlowQueueCallHistoryDetailsDto()
                      {
                          Mobile = showMobileNoPermission == true ? o.Number : "+61*********",
                          Duration = o.Duration,
                          StartTime = o.CallStartTimeUTC,
                          EndTime = o.CallEndTimeUTC,
                          Summary = summaryCount
                      };

            var callHistory = await res.ToListAsync();

            return callHistory;
        }

        public async Task<FileDto> GetAllCallQueueCallHistoryExcel(GetCallQueueHistoryInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var organizationIDs = _callFlowQueueOrganizationUnitRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnitId).Select(e => e.CallFlowQueueId);
            var callQueue = _callFlowQueueRepository.GetAll().Where(e => organizationIDs.Contains(e.Id));

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number.Contains(input.Mobile))
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date);
            //.WhereIf(!string.IsNullOrEmpty(input.State), e => e.State == input.State).ToList();

            var callHistory = (from o in callQueue

                               select new GetAllCallQueueHistoryDto()
                               {
                                   Id = (int)o.Id,
                                   Name = o.Name,
                                   Count = filtered.Where(e => e.QueueExtension == o.ExtentionNumber.ToString()).Count(),
                                   ExtentionNumber = o.ExtentionNumber
                               });

            callHistory = callHistory.Where(e => e.Count > 0);

            return _leadsExcelExporter.CallFlowQueueExport(await callHistory.ToListAsync(), "CallFlowQueueReport.xlsx");

        }

        public async Task<FileDto> GetCallQueueDetailsCountExcel(GetCallQueueDetailsCountInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();


            var users = _userRepository.GetAll().Where(e => e.ExtensionNumber != null && e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnitId))
                 .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                 .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id);

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number.Contains(input.Mobile))
                            .Where(e => e.QueueExtension == input.QueueExtentionnumber)
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date);

            var res = from o in users

                      select new GetAllCallFlowQueueCountDetailsDto()
                      {
                          User = o.FullName.ToString(),
                          Count = filtered.Where(e => e.Agent == o.ExtensionNumber).Count(),
                          ExtentionNumber = o.ExtensionNumber,
                      };

            var callHistory = await res.Where(e => e.Count != 0).ToListAsync();

            return _leadsExcelExporter.CallFlowQueueDetailExport(callHistory, "CallFlowQueueReport.xlsx");

        }

        public async Task<FileDto> GetCallQueueCallHistoryDetailsExcel(GetCallQueueDetailsCountInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var showMobileNoPermission = PermissionChecker.IsGranted(Authorization.AppPermissions.Pages_CallHistory_CallFlowQueueCallHistory_Detail_UserCallDetails_ShowMobileNo);

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number.Contains(input.Mobile))
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date)
                            .Where(e => e.Agent == input.ExtentionNumber)
                            .Where(e => e.QueueExtension == input.QueueExtentionnumber);

            var summaryCount = new GetcallFlowDetailSummary();
            summaryCount.TotalDuration = filtered.Sum(e => (Convert.ToInt32(e.Duration.Substring(0, 2)) * 60) + Convert.ToInt32(e.Duration.Substring(3, 5)));
            summaryCount.AvgDuration = filtered.Average(e => (Convert.ToInt32(e.Duration.Substring(0, 2)) * 60) + Convert.ToInt32(e.Duration.Substring(3, 5)));

            var res = from o in filtered

                      select new GetAllCallFlowQueueCallHistoryDetailsDto()
                      {
                          Mobile = showMobileNoPermission == true ? o.Number : "",
                          Duration = o.Duration,
                          StartTime = o.CallStartTimeUTC,
                          EndTime = o.CallEndTimeUTC,
                          Summary = summaryCount
                      };

            var callHistory = await res.ToListAsync();

            return _leadsExcelExporter.CallFlowQueueCallHistoryDetailExport(callHistory, "CallFlowQueueReport.xlsx");

        }

        public async Task<List<GetAllLocalityCountOfState>> GetAllLocalityCountOfStateHistory(GetAllStateCallHistoryInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            List<string> Statelist = new List<string> { "QLD", "SA", "NSW", "VIC" };

            var filtered = _enfornicaCallDetailsRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.From.Contains(input.Mobile))
                            .WhereIf(input.StartDate != null, e => e.CreationTime.Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CreationTime.Date <= EDate.Value.Date)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.State) && input.State != "Other", e => e.FromLocationAdministrativeArea == input.State)
                            .WhereIf(input.State == "Other", e => !Statelist.Contains(e.FromLocationAdministrativeArea))
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Locality), e => e.FromLocationLocality == input.Locality)
                            .Where(e => e.OrganizationUnitId == input.OrganizationUnitId);

            var res = filtered.GroupBy(e => e.FromLocationLocality)
                        .Select(group => new GetAllLocalityCountOfState
                        {
                            FromLocationLocality = group.Key,
                            Count = group.Count()
                        });

            return await res.ToListAsync();
        }

        public async Task<FileDto> GetAllLocalityCountOfStateHistoryExport(GetAllStateCallHistoryInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            List<string> Statelist = new List<string> { "QLD", "SA", "NSW", "VIC" };

            var filtered = _enfornicaCallDetailsRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.From.Contains(input.Mobile))
                            .WhereIf(input.StartDate != null, e => e.CreationTime.Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CreationTime.Date <= EDate.Value.Date)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.State) && input.State != "Other", e => e.FromLocationAdministrativeArea == input.State)
                            .WhereIf(input.State == "Other", e => !Statelist.Contains(e.FromLocationAdministrativeArea))
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Locality), e => e.FromLocationLocality == input.Locality)
                            .Where(e => e.OrganizationUnitId == input.OrganizationUnitId);

            var res = filtered.GroupBy(e => e.FromLocationLocality)
                        .Select(group => new GetAllLocalityCountOfState
                        {
                            FromLocationLocality = group.Key,
                            Count = group.Count()
                        });

            return _leadsExcelExporter.LocalityCountOfStateHistoryExport(await res.ToListAsync(), "LocalityCountOfStateHistory.xlsx");

        }

        public async Task<PagedResultDto<GetAllCallQueueWeeklyHistoryDto>> GetAllCallQueueweeklyHistory(GetCallQueueWeeklyHistoryInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            List<DateTime?> dates = new List<DateTime?>();
            dates.Add(SDate);
            for (int i = 1; i <= 6; i++)
            {
                dates.Add(SDate.Value.Date.AddDays(i));
            }


            var organizationIDs = _callFlowQueueOrganizationUnitRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnitId).Select(e => e.CallFlowQueueId);
            var callQueue = _callFlowQueueRepository.GetAll().Where(e => organizationIDs.Contains(e.Id)).AsNoTracking().Select(e => new {e.Id, e.Name, e.ExtentionNumber});

            var totalCount = 0;
            //List<string> userExtentionNumber = new List<string>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var userExtentionNumber = _userRepository.GetAll()
                .Where(e => e.ExtensionNumber != null && e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnitId))
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id).AsNoTracking()
                .Select(e => e.ExtensionNumber);

            var filtered = _callHistoryRepository.GetAll()
                            .Where(e => userExtentionNumber.Any(u => e.Agent == u))
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number == input.Mobile)
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date && e.CallStartTimeUTC.AddHours(diffHour).Date <= dates[6].Value.Date)
                            .GroupBy(e => new { e.CallStartTimeUTC.AddHours(diffHour).Date, e.QueueExtension}).Select(e => new { e.Key.Date, e.Key.QueueExtension, count = e.Count()}).OrderBy(e => e.QueueExtension);

            var res = from o in callQueue
                      let history = filtered.Where(e => (!string.IsNullOrEmpty(e.QueueExtension) ? Convert.ToInt32(e.QueueExtension) : 0) == o.ExtentionNumber)
                      select new GetAllCallQueueWeeklyHistoryDto()
                      {
                          Id = (int)o.Id,
                          Name = o.Name,
                          Count = history.Sum(e => e.count),
                          ExtentionNumber = o.ExtentionNumber,
                          MondayCount = history.Where(e => e.Date == dates[0].Value.Date).Sum(e => e.count),
                          TuesdayCount = history.Where(e => e.Date ==  dates[1].Value.Date).Sum(e => e.count ) ,
                          WednesdayCount = history.Where(e => e.Date == dates[2].Value.Date).Sum(e => e.count),
                          ThrusdayCount = history.Where(e => e.Date == dates[3].Value.Date).Sum(e => e.count ),
                          FridayCount = history.Where(e => e.Date == dates[4].Value.Date).Sum(e => e.count),
                          SatdayCount = history.Where(e => e.Date == dates[5].Value.Date).Sum(e => e.count),
                          SundayCount = history.Where(e => e.Date == dates[6].Value.Date).Sum(e => e.count)
                      };

            //res = res.Where(e => e.Count > 0);

            var pagedAndFiltered = res
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var callHistory = await pagedAndFiltered.ToListAsync();

            totalCount = await res.CountAsync();

            if (callHistory != null && callHistory.Count > 0)
            {
                callHistory[0].TotalCalls = res.ToList().Sum(e => e.Count);
            }

            return new PagedResultDto<GetAllCallQueueWeeklyHistoryDto>(totalCount, callHistory);
        }

        public async Task<PagedResultDto<GetAllCallFlowQueueUserWiseCountDto>> GetCallQueueUserWiseCount(GetCallQueueUserWiseCountInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var organizationIDs = _callFlowQueueOrganizationUnitRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnitId).Select(e => e.CallFlowQueueId);
            var callQueue = _callFlowQueueRepository.GetAll().Where(e => organizationIDs.Contains(e.Id) && e.ExtentionNumber != null).Select(e => e.ExtentionNumber);

            var totalCount = 0;
            //List<string> userExtentionNumber = new List<string>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var userExtentionNumber = _userRepository.GetAll()
                .Where(e => e.ExtensionNumber != null && e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnitId))
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id)
                .Select(e => new { e.ExtensionNumber, e.FullName, e.Id });

            var filtered = _callHistoryRepository.GetAll()
                            .Where(e => callQueue.Any(u => Convert.ToInt32(e.QueueExtension) == u) )
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number.Contains(input.Mobile))
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date).Select(e => new {e.Agent});

            var res = from o in userExtentionNumber

                      select new GetAllCallFlowQueueUserWiseCountDto()
                      {
                          Id = (int)o.Id,
                          User = o.FullName,
                          Count = filtered.Where(e => e.Agent == o.ExtensionNumber).Count(),
                          ExtentionNumber = o.ExtensionNumber,
                      };

            //res = res.Where(e => e.Count > 0);
            // var result = res.AsQueryable().Where(e => e.Count != 0);
            IQueryable<GetAllCallFlowQueueUserWiseCountDto> result = res.ToList().Where(e => e.Count > 0).AsQueryable();
            //var result = data.;

            var pagedAndFiltered = result//.AsQueryable()
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var callHistory =  pagedAndFiltered.ToList();

            totalCount = result.Count();

            if (callHistory != null && callHistory.Count > 0)
            {
                callHistory[0].TotalCalls = result.ToList().Sum(e => e.Count);
            }

            return new PagedResultDto<GetAllCallFlowQueueUserWiseCountDto>(totalCount, callHistory);
        }

        public async Task<List<GetAllCallQueueHistoryDto>> GetCallQueueUserWiseDetailsCount(GetCallQueueUserWiseDetailsCountInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var organizationIDs = _callFlowQueueOrganizationUnitRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnitId).Select(e => e.CallFlowQueueId);
            var callQueue = _callFlowQueueRepository.GetAll().Where(e => organizationIDs.Contains(e.Id));

            var users = _userRepository.GetAll()
                .Where(e => e.ExtensionNumber != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id);

            var filtered = _callHistoryRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Mobile), e => e.Number.Contains(input.Mobile))
                            .Where(e => e.Agent == input.QueueExtentionnumber)
                            .WhereIf(input.StartDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.CallStartTimeUTC.AddHours(diffHour).Date <= EDate.Value.Date);

            var res = from o in callQueue

                      select new GetAllCallQueueHistoryDto()
                      {
                          Name = o.Name,
                          Count = filtered.Where(e => Convert.ToInt32(e.QueueExtension) == o.ExtentionNumber).Count(),
                          ExtentionNumber = o.ExtentionNumber,
                      };

            var callHistory = await res.Where(e => e.Count != 0).ToListAsync();

            return callHistory;
        }

    }
}
