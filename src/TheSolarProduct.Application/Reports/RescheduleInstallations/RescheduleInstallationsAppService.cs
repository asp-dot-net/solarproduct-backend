﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.TheSolarDemo;
//using System.Globalization;
using Abp.Timing.Timezone;
using TheSolarProduct.Timing;
using TheSolarProduct.Reports.ProductSolds.Dtos;
using Abp.Collections.Extensions;
using TheSolarProduct.Reports.ProductSolds.Exporting;
using TheSolarProduct.Reports.Variations.Dtos;
using TheSolarProduct.JobHistory;
using Abp.Extensions;
using TheSolarProduct.Reports.RescheduleInstallations.Dtos;

namespace TheSolarProduct.Reports.RescheduleInstallations
{
    public class RescheduleInstallationsAppService : TheSolarProductAppServiceBase, IRescheduleInstallationsAppService
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<JobTrackerHistory> _jobTrackerHistoryRepository;

        public RescheduleInstallationsAppService(
            ITimeZoneConverter timeZoneConverter
            , ITimeZoneService timeZoneService
            , IRepository<Job> jobRepository
            , IRepository<JobProductItem> jobProductItemRepository
            , IRepository<User, long> userRepository
            , UserManager userManager
            , IRepository<UserTeam> userTeamRepository
            , IRepository<JobTrackerHistory> jobTrackerHistoryRepository

            )
        {
            _jobRepository = jobRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _jobProductItemRepository = jobProductItemRepository;
            _userRepository = userRepository;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _jobTrackerHistoryRepository = jobTrackerHistoryRepository;
        }

        public async Task<PagedResultDto<GetAllRescheduleInstallationDto>> GetAll(GetRescheduleInstallationInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var User_List = _userRepository.GetAll().AsNoTracking();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = await team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var filteredJobs = _jobRepository.GetAll()
                        .Include(e => e.JobStatusFk)
                        .Include(e => e.LeadFk)
                        .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit)
                        .WhereIf(input.FilterName == "JobNumber" && !string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber == input.Filter.Trim())
                        .WhereIf(input.FilterName == "MobileNo" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.Mobile == input.Filter.Trim())
                        .WhereIf(input.FilterName == "CompanyName" && !string.IsNullOrWhiteSpace(input.Filter), e => e.LeadFk.CompanyName == input.Filter.Trim())
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                        ////.WhereIf(!string.IsNullOrWhiteSpace(input.JobTypeNameFilter), e => e.JobTypeFk != null && e.JobTypeFk.Name == input.JobTypeNameFilter)
                        //.WhereIf(input.JobTypeNameListFilter.Count() > 0, e => input.JobTypeNameListFilter.Contains(e.JobTypeFk.Name))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.JobStatusNameFilter), e => e.JobStatusFk != null && e.JobStatusFk.Name == input.JobStatusNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.RoofTypeNameFilter), e => e.RoofTypeFk != null && e.RoofTypeFk.Name == input.RoofTypeNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.RoofAngleNameFilter), e => e.RoofAngleFk != null && e.RoofAngleFk.Name == input.RoofAngleNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.ElecDistributorNameFilter), e => e.ElecDistributorFk != null && e.ElecDistributorFk.Name == input.ElecDistributorNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.LeadCompanyNameFilter), e => e.LeadFk != null && e.LeadFk.CompanyName == input.LeadCompanyNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.ElecRetailerNameFilter), e => e.ElecRetailerFk != null && e.ElecRetailerFk.Name == input.ElecRetailerNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.PaymentOptionNameFilter), e => e.PaymentOptionFk != null && e.PaymentOptionFk.Name == input.PaymentOptionNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.DepositOptionNameFilter), e => e.DepositOptionFk != null && e.DepositOptionFk.Name == input.DepositOptionNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.PromotionOfferNameFilter), e => e.PromotionOfferFk != null && e.PromotionOfferFk.Name == input.PromotionOfferNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.HouseTypeNameFilter), e => e.HouseTypeFk != null && e.HouseTypeFk.Name == input.HouseTypeNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.FinanceOptionNameFilter), e => e.FinanceOptionFk != null && e.FinanceOptionFk.Name == input.FinanceOptionNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.PostalCode, input.PostalCodeFrom) >= 0)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.PostalCode, input.PostalCodeTo) <= 0)
                        //.WhereIf(input.paymentid != 0, e => e.PaymentOptionId == input.paymentid)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
                        //.WhereIf(input.jobStatusIDFilter != null && input.jobStatusIDFilter.Count() > 0, e => input.jobStatusIDFilter.Contains((int)e.JobStatusId))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.LeadFk.Area == input.AreaName)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                        .WhereIf(input.JobDateFilter == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.StartDate != null, e => e.JobReminderTime.Value.Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "NextFollowUp" && input.EndDate != null, e => e.JobReminderTime.Value.Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallBook" && input.StartDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallBook" && input.EndDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.JobDateFilter == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                        
                        //.WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => (e.JobStatusId != 1 || e.JobTypeId == 6))
                        .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        //.WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.LeadFk.AssignToUserID == input.SalesRepId)
                        //.WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.LeadFk.AssignToUserID == input.SalesManagerId)

                        //.WhereIf(!string.IsNullOrWhiteSpace(input.PanelModel), e => PanelDetails.Contains(e.Id))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.BatteryModel), e => batteryDetails.Contains(e.Id))

                        //.WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.LeadFk.AssignToUserID))
                        //.WhereIf(input.Solarvicstatus != 0, e => e.SolarRebateStatus == input.Solarvicstatus)
                        //.WhereIf(input.Readytoactive != null && input.Readytoactive == 1, e => e.IsActive == true && (e.JobStatusId == 1 || e.JobStatusId == 4))
                        //.WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemEndRange != null && input.SystemEndRange != 0, e => e.SystemCapacity >= input.SystemStrtRange && e.SystemCapacity <= input.SystemEndRange)
                        //.WhereIf(input.SystemStrtRange != null && input.SystemStrtRange != 0 && input.SystemEndRange == null && input.SystemEndRange == 0, e => e.SystemCapacity == input.SystemStrtRange)
                        //.WhereIf(input.SystemEndRange != null && input.SystemEndRange != 0 && input.SystemStrtRange == null && input.SystemStrtRange == 0, e => e.SystemCapacity == input.SystemEndRange)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.InvertModel), e => InvertDetails.Contains(e.Id))
                        //.WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))

                        //.WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        //.WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        //.WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        //.WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        //.WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        //.WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        //.WhereIf(input.BatteryFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery
                        //.WhereIf(input.BatteryFilter == 6, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5)) //For with battery

                        //.WhereIf(input.PreviousJobStatusId != null && input.PreviousJobStatusId.Count() > 0, e => e.JobStatusId == 3 && input.PreviousJobStatusId.Contains(_previousJobStatusRepository.GetAll().Where(pr => pr.JobId == e.Id).AsNoTracking().OrderByDescending(r => r.Id).FirstOrDefault().PreviousId))

                        //.WhereIf(!string.IsNullOrEmpty(input.PriceType), e => e.PriceType == input.PriceType)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter) || e.LeadFk.Mobile.Contains(input.Filter) || e.LeadFk.CompanyName.Contains(input.Filter))

                        .AsNoTracking()
                        .Select(e => new { e.Id, e.SystemCapacity, e.JobNumber, e.PostalCode, e.LeadId, e.State,e.LeadFk.Area });

           

            var jobs = from o in filteredJobs                     

                       select new GetAllRescheduleInstallationDto()
                       {
                           Id = o.Id,
                           State = o.State,
                           Area=o.Area,
                           SystemCapacity = o.SystemCapacity,
                           Jobnumber = o.JobNumber,
                           PostCode = o.PostalCode,
                           NoOfPanel = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 1 && e.JobId == o.Id).Select(e => e.Quantity).Sum(),
                           RescheduleCount = _jobTrackerHistoryRepository.GetAll().Where(e => e.JobIDId == o.Id && e.FieldName == "Installation Date" && e.Action == "Installation Details Edit" && !string.IsNullOrEmpty(e.CurValue)).GroupBy(e => e.CurValue.Substring(0,10)).Select(e => e.Key).Count() - 1,
                       };

            var result = await jobs.Where(e => e.RescheduleCount > 0).ToListAsync();

            var pagedAndFilteredJobs = result.AsQueryable()
              .OrderBy(input.Sorting ?? "id desc")
              .PageBy(input);


            var totalCount = result.Count();

            var output = pagedAndFilteredJobs.ToList();
            if (totalCount > 0)
            {
                output[0].summary = new List<SummaryRescheduleInstallationDto>();
                output[0].summary = (from s in result.GroupBy(e => e.State).Select(e => new { st = e.Key, c = e.Count() })
                                     select new SummaryRescheduleInstallationDto()
                                     {
                                         state = s.st,
                                         count = s.c
                                     }).ToList();
            }

            return new PagedResultDto<GetAllRescheduleInstallationDto>(totalCount, output);
        }

        public async Task<List<GetRescheduleInstallationHistoryDto>> GetRescheduleInstallationHistory(int jobId)
        {
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var PrevResult = _jobTrackerHistoryRepository.GetAll().Where(e => e.JobIDId == jobId && e.FieldName == "Installation Date" && e.Action == "Installation Details Edit" && !string.IsNullOrEmpty(e.CurValue)).OrderBy(e => e.JobActionId)
                .GroupBy(e => e.CurValue.Substring(0, 10)).Select(e => new { CurValue = e.Key, JobActionId = e.Max(m => m.JobActionId) });

            var PrevFirstResult = PrevResult.OrderBy(e => e.JobActionId).FirstOrDefault();


            var result = PrevResult.Where(e => e.JobActionId != PrevFirstResult.JobActionId).OrderBy(e => e.JobActionId);

            var pagedAndFilteredJobs = from o in result
                                       let prevAction = PrevResult.Where(e => e.JobActionId < o.JobActionId).OrderByDescending(e => e.JobActionId).FirstOrDefault()
                                       select new GetRescheduleInstallationHistoryDto()
                                       {
                                           InstallationDate = prevAction.CurValue + " - " + o.CurValue,                                           
                                           Installer = _jobTrackerHistoryRepository.GetAll().Where(e => e.JobActionId <= prevAction.JobActionId && e.FieldName == "Installer" && e.Action == "Installation Details Edit" && !string.IsNullOrEmpty(e.CurValue)).OrderByDescending(e => e.Id).FirstOrDefault().CurValue
                                                        + " - " + _jobTrackerHistoryRepository.GetAll().Where(e => e.JobActionId <= o.JobActionId && e.FieldName == "Installer" && e.Action == "Installation Details Edit" && !string.IsNullOrEmpty(e.CurValue)).OrderByDescending(e => e.Id).FirstOrDefault().CurValue,
                                           CreateDate = _jobTrackerHistoryRepository.GetAll().Where(e => e.JobActionId == o.JobActionId && e.FieldName == "Installation Date" && e.Action == "Installation Details Edit" && !string.IsNullOrEmpty(e.CurValue)).FirstOrDefault().CreationTime.AddHours(-(diffHour)),
                                          // PrevCreateDate = _jobTrackerHistoryRepository.GetAll().Where(e => e.JobActionId <= prevAction.JobActionId && e.FieldName == "Installer" && !string.IsNullOrEmpty(e.CurValue)).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,
                                       };

            return await pagedAndFilteredJobs.ToListAsync();
        }
    }
}
