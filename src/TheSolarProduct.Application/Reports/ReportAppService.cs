﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Timing.Timezone;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Common;
using TheSolarProduct.Invoices;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.Leads;
using TheSolarProduct.MultiTenancy;
using TheSolarProduct.PickList;
using TheSolarProduct.PickList.Dtos;
using TheSolarProduct.Quotations;
using TheSolarProduct.TheSolarDemo;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Quotations.Dtos;
using TheSolarProduct.Organizations;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.DeclarationForms.Dtos;
using Abp.Authorization;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Reports.ReviewReports.Dtos;
using Abp.Application.Services.Dto;
using Abp.Linq.Extensions;
using System.Linq.Dynamic.Core;
using TheSolarProduct.Jobs.Dtos.Pylon;
using TheSolarProduct.StockOrderReceivedReport.Dtos;

namespace TheSolarProduct.Reports
{
    [AbpAuthorize]
    public class ReportAppService : TheSolarProductAppServiceBase, IReportAppService
    {
        //private const string pathfordoc = @"s:\documents.thesolarproduct.com";
        private const string pathfordoc = ApplicationSettingConsts.DocumentPath;

        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<RoofType, int> _roofTypeRepository;
        private readonly IRepository<RoofAngle, int> _roofAngleRepository;
        private readonly IRepository<HouseType, int> _houseTypeRepository;
        private readonly IRepository<ElecDistributor, int> _elecDistributorRepository;
        private readonly IRepository<ElecRetailer, int> _elecRetailerRepository;
        private readonly IRepository<ProductItem> _ProductItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<JobStatus> _jobStatusRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<InvoicePayment> _invoicePaymentRepository;
        private readonly IRepository<InvoicePaymentMethod> _invoicePaymentMethodRepository;
        private readonly IRepository<JobRefund> _jobRefundRepository;
        private readonly IRepository<RefundReason> _refundReasonRepository;
        private readonly IAppFolders _appFolders;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<Document> _documentRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IRepository<Quotation> _quotationRepository;
        private readonly IRepository<Warehouselocation> _wareHouseLocationRepository;
        private readonly IPickListAppService _pickListAppService;
        private readonly IRepository<FinanceOption> _financeOptionRepository;
        private readonly IRepository<DepositOption> _depositOptionRepository;
        private readonly IRepository<PaymentOption> _paymentOptionRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<InvoiceImportData> _InvoiceImportDataRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<Quotations.JobAcknowledgement> _jobAcknowledgementRepqository;
        private readonly IRepository<DeclarationForm> _declarationFormRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IRepository<JobReview> _JobReviewRepository;
        //private readonly IRepository<ExtendOrganizationUnit> _extendedOrganizationUnitRepository;

        public ReportAppService(IRepository<Job> jobRepository
            , IRepository<RoofType, int> roofTypeRepository
            , IRepository<RoofAngle, int> roofAngleRepository
            , IRepository<HouseType, int> houseTypeRepository
            , IRepository<ElecDistributor, int> elecDistributorRepository
            , IRepository<ElecRetailer, int> elecRetailerRepository
            , IRepository<ProductItem> ProductItemRepository
            , IRepository<JobProductItem> jobProductItemRepository
            , IRepository<Lead> leadRepository
            , IRepository<JobStatus> jobStatusRepository
            , IRepository<User, long> userRepository
            , IRepository<UserRole, long> userroleRepository
            , IRepository<Role> roleRepository
            , IRepository<UserTeam> userTeamRepository
            , IRepository<InvoicePayment> invoicePaymentRepository
            , IRepository<InvoicePaymentMethod> invoicePaymentMethodRepository
            , IRepository<JobRefund> jobRefundRepository
            , IRepository<RefundReason> refundReasonRepository
            , IAppFolders appFolders
            , IWebHostEnvironment env
            , IRepository<Tenant> tenantRepository
            , IRepository<Document> documentRepository
            , IRepository<Variation> variationRepository
            , IRepository<JobVariation> jobVariationRepository
            , IRepository<Quotation> quotationRepository
            , IPickListAppService pickListAppService
            , IRepository<Warehouselocation> wareHouseLocationRepository
            , IRepository<FinanceOption> financeOptionRepository
            , IRepository<DepositOption> depositOptionRepository
            , IRepository<PaymentOption> paymentOptionRepository
            , UserManager userManager,
            IRepository<InvoiceImportData> InvoiceImportDataRepository,
            ICommonLookupAppService CommonDocumentSaveRepository
            , IRepository<Quotations.JobAcknowledgement> jobAcknowledgementRepqository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository
            , IRepository<DeclarationForm> declarationFormRepository
            , IRepository<JobReview> JobReviewRepository
        )
        {
            _jobRepository = jobRepository;
            _roofTypeRepository = roofTypeRepository;
            _roofAngleRepository = roofAngleRepository;
            _houseTypeRepository = houseTypeRepository;
            _elecDistributorRepository = elecDistributorRepository;
            _elecRetailerRepository = elecRetailerRepository;
            _ProductItemRepository = ProductItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _leadRepository = leadRepository;
            _jobStatusRepository = jobStatusRepository;
            _userRepository = userRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _userTeamRepository = userTeamRepository;
            _invoicePaymentRepository = invoicePaymentRepository;
            _invoicePaymentMethodRepository = invoicePaymentMethodRepository;
            _jobRefundRepository = jobRefundRepository;
            _refundReasonRepository = refundReasonRepository;
            _appFolders = appFolders;
            _env = env;
            _tenantRepository = tenantRepository;
            _documentRepository = documentRepository;
            _variationRepository = variationRepository;
            _jobVariationRepository = jobVariationRepository;
            _quotationRepository = quotationRepository;
            _pickListAppService = pickListAppService;
            _wareHouseLocationRepository = wareHouseLocationRepository;
            _financeOptionRepository = financeOptionRepository;
            _depositOptionRepository = depositOptionRepository;
            _paymentOptionRepository = paymentOptionRepository;
            _userManager = userManager;
            _InvoiceImportDataRepository = InvoiceImportDataRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _jobAcknowledgementRepqository = jobAcknowledgementRepqository;
            _timeZoneConverter = timeZoneConverter;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _declarationFormRepository = declarationFormRepository;
            _JobReviewRepository = JobReviewRepository;
        }

        //public string Quotation(int JobId, int? QuoteId, string ImagePath, string QuoteNo, int OrgId)
        //{
        //    if (OrgId == 1)
        //    {

        //        var report_parent = new Report.NewQuote();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        var RoofType = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
        //        var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofAngleId).Select(e => e.Name).FirstOrDefault();
        //        var HouseType = _houseTypeRepository.GetAll().Where(e => e.Id == JobDetail.HouseTypeId).Select(e => e.Name).FirstOrDefault();
        //        var Dist = _elecDistributorRepository.GetAll().Where(e => e.Id == JobDetail.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
        //        var Retailer = _elecRetailerRepository.GetAll().Where(e => e.Id == JobDetail.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
        //        var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
        //        var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
        //        var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
        //        var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).ToList();
        //        var NearMap = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 1 && e.JobId == JobId).OrderByDescending(e => e.Id).FirstOrDefault();
        //        var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
        //        var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
        //        var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
        //        var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
        //        var SalesRep = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
        //        var TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.InvoicePaymentStatusId == 2).Select(e => e.InvoicePayTotal).Sum();

        //        var Finance = _financeOptionRepository.GetAll().Where(e => e.Id == JobDetail.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
        //        var Dep = _depositOptionRepository.GetAll().Where(e => e.Id == JobDetail.DepositOptionId).Select(e => e.Name).FirstOrDefault();
        //        var Payment = _paymentOptionRepository.GetAll().Where(e => e.Id == JobDetail.PaymentOptionId).Select(e => e.Name).FirstOrDefault();

        //        var QunityAndModelList = (from o in _jobProductItemRepository.GetAll()
        //                                  join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
        //                                  from s1 in j1.DefaultIfEmpty()
        //                                  where (o.JobId == JobDetail.Id)
        //                                  select new
        //                                  {
        //                                      Name = o.Quantity + " x " + s1.Name
        //                                  }).ToList();

        //        //	var QunityAndModel = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id)).Select(e => e.Name).ToList();


        //        Telerik.Reporting.TextBox QuoteNumber1 = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Date1 = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox QuoteNumber2 = report_parent.Items.Find("textBox124", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Date2 = report_parent.Items.Find("textBox123", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Address2 = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox MeaterPhase = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox MeaterUpgrade = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NoofStory = report_parent.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RoofPinch = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RoofTypes = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox EnergyDist = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox EnergyRetailer = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NMINumber = report_parent.Items.Find("textBox97", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox PaymentMethod = report_parent.Items.Find("textBox98", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox InstDate = report_parent.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InstName = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InstMobile = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InstEmail = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.TextBox Capecity = report_parent.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Total = report_parent.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SubTotal = report_parent.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox STC = report_parent.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox STCDesc = report_parent.Items.Find("textBox26", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox GrandTotal = report_parent.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox FinalQuote = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SpecialDiscount = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox AdditionalCharges = report_parent.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Deposite = report_parent.Items.Find("textBox30", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox DepositeText = report_parent.Items.Find("textBox92", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Balance = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;

        //        //Telerik.Reporting.TextBox Panel = report_parent.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Inverter1 = report_parent.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Inverter2 = report_parent.Items.Find("textBox172", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Inverter3 = report_parent.Items.Find("textBox171", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Inverter4 = report_parent.Items.Find("textBox177", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox InverterModelNo2 = report_parent.Items.Find("textBox176", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Inverter3 = report_parent.Items.Find("textBox179", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox InverterModelNo3 = report_parent.Items.Find("textBox178", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Notes = report_parent.Items.Find("textBox32", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RefNo = report_parent.Items.Find("textBox175", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.TextBox SignCustName = report_parent.Items.Find("textBox174", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SignCustDate = report_parent.Items.Find("textBox173", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.PictureBox CustSignature = report_parent.Items.Find("pictureBox6", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox NearMap1 = report_parent.Items.Find("pictureBox7", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox NearMap2 = report_parent.Items.Find("pictureBox8", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox NearMap3 = report_parent.Items.Find("pictureBox9", true)[0] as Telerik.Reporting.PictureBox;

        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG2 = report_parent.Items.Find("pictureBox2", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG3 = report_parent.Items.Find("pictureBox3", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG4 = report_parent.Items.Find("pictureBox4", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG5 = report_parent.Items.Find("pictureBox5", true)[0] as Telerik.Reporting.PictureBox;

        //        Telerik.Reporting.Table SyatemDetail = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        //        SyatemDetail.DataSource = QunityAndModelList;
        //        var Path = _env.WebRootPath;

        //        Image Url1 = Image.FromFile(Path + "/PDFImages/Quote1.png");
        //        Image Url2 = Image.FromFile(Path + "/PDFImages/Quote2.png");
        //        Image Url3 = Image.FromFile(Path + "/PDFImages/Quote3.png");
        //        Image Url4 = Image.FromFile(Path + "/PDFImages/Quote4.png");
        //        Image Url5 = Image.FromFile(Path + "/PDFImages/Quote5.png");

        //        IMG1.Value = Url1;
        //        IMG2.Value = Url2;
        //        IMG3.Value = Url3;
        //        IMG4.Value = Url4;
        //        IMG5.Value = Url5;

        //        var test = 0;
        //        int i = 0;
        //        foreach (var item in Addition)
        //        {
        //            if (i == 0)
        //            {
        //                test = Convert.ToInt32(item);
        //            }
        //            else
        //            {
        //                test = test + Convert.ToInt32(item);
        //            }
        //            i++;
        //        }
        //        var test1 = 0;
        //        int j = 0;
        //        foreach (var item in Discount)
        //        {
        //            if (j == 0)
        //            {
        //                test1 = Convert.ToInt32(item);
        //            }
        //            else
        //            {
        //                test1 = test1 + Convert.ToInt32(item);
        //            }
        //            j++;
        //        }

        //        QuoteNumber1.Value = JobDetail.JobNumber;
        //        QuoteNumber2.Value = JobDetail.JobNumber;
        //        Date1.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
        //        Date2.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
        //        Name.Value = Lead.CompanyName;
        //        Mobile.Value = ": " + Lead.Mobile;
        //        Email.Value = ": " + Lead.Email;
        //        if (JobDetail.Address != null)
        //        {
        //            Address1.Value = ": " + Convert.ToString(JobDetail.Address).Trim();
        //        }
        //        Address2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
        //        Capecity.Value = Convert.ToString(JobDetail.SystemCapacity) + " KW SOLAR";
        //        Notes.Value = JobDetail.Note;
        //        if (SalesRep != null)
        //        {
        //            InstName.Value = SalesRep.FullName;
        //            if (SalesRep.Mobile != null)
        //            {
        //                InstMobile.Value = SalesRep.PhoneNumber + "/" + SalesRep.Mobile;
        //            }
        //            else
        //            {
        //                InstMobile.Value = SalesRep.PhoneNumber;
        //            }
        //            InstEmail.Value = SalesRep.EmailAddress;
        //        }
        //        RefNo.Value = JobDetail.JobNumber;
        //        SubTotal.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate);
        //        STC.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
        //        STCDesc.Value = JobDetail.STC + " * " + JobDetail.STCPrice;
        //        GrandTotal.Value = Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate)) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate));

        //        if (TotalDeposite == 0 || TotalDeposite == null)
        //        {
        //            DepositeText.Value = "Deposit Required";
        //            Deposite.Value = JobDetail.DepositRequired == 0 ? "$ " + Math.Round((double)JobDetail.BasicCost * 10 / 100) + ".00" : "$ " + Math.Round((double)JobDetail.DepositRequired) + ".00";
        //        }
        //        else
        //        {
        //            DepositeText.Value = "Deposit Paid";
        //            Deposite.Value = "$ " + TotalDeposite;
        //        }

        //        Total.Value = Convert.ToString(JobDetail.BasicCost) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost);
        //        if (TotalDeposite == 0 || TotalDeposite == null)
        //        {
        //            Balance.Value = Convert.ToString((JobDetail.BasicCost) + test - test1 - JobDetail.DepositRequired) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost) + test - test1 - JobDetail.DepositRequired);
        //        }
        //        else
        //        {
        //            Balance.Value = Convert.ToString((JobDetail.BasicCost) + test - test1 - TotalDeposite) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost) + test - test1 - TotalDeposite);
        //        }

        //        MeaterPhase.Value = ": " + Convert.ToString(JobDetail.MeterPhaseId);

        //        if (JobDetail.PaymentOptionId == 1)
        //        {
        //            PaymentMethod.Value = "Cash";
        //        }
        //        else
        //        {
        //            PaymentMethod.Value = Payment + " with " + Dep + " for " + Finance;
        //        }

        //        SpecialDiscount.Value = test1 == 0 ? "$ 0.00" : "$ " + test1 + ".00";
        //        AdditionalCharges.Value = test == 0 ? "$ 0.00" : "$ " + test + ".00";
        //        if (JobDetail.MeterUpgradeId == 1)
        //        {
        //            MeaterUpgrade.Value = ": " + "Yes";
        //        }
        //        else
        //        {
        //            MeaterUpgrade.Value = ": " + "No";
        //        }
        //        NoofStory.Value = ": " + HouseType;
        //        RoofTypes.Value = ": " + RoofType;
        //        RoofPinch.Value = ": " + RoofAngle;
        //        EnergyDist.Value = ": " + Dist;
        //        EnergyRetailer.Value = ": " + Retailer;
        //        NMINumber.Value = ": " + JobDetail.NMINumber;
        //        //if (JobDetail.InstallationDate != null)
        //        //{
        //        //	InstDate.Value = JobDetail.InstallationDate.Value.ToString("DD/MM/yyyy");
        //        //}
        //        //if (PanelDetail != null)
        //        //{
        //        //	var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

        //        //	//Panel.Value = PanelQTY + " x " + PanelDetail.Name;
        //        //	//PanelModelNo.Value = PanelDetail.Model;
        //        //}
        //        //for (int k = 0; k < InverterDetail.Count; k++)
        //        //{
        //        //	if(k == 0)
        //        //	{
        //        //		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

        //        //		//Inverter1.Value = InverterQTY + " x " + InverterDetail[k].Name;
        //        //		//InverterModelNo1.Value = InverterDetail[k].Model;
        //        //	}
        //        //	if (k == 1)
        //        //	{
        //        //		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

        //        //		//Inverter2.Value = InverterQTY + " x " + InverterDetail[k].Name;
        //        //		//InverterModelNo2.Value = InverterDetail[k].Model;
        //        //	}
        //        //	if (k == 2)
        //        //	{
        //        //		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

        //        //		//Inverter3.Value = InverterQTY + " x " + InverterDetail[k].Name;
        //        //		//InverterModelNo3.Value = InverterDetail[k].Model;
        //        //	}
        //        //	if (k == 3)
        //        //	{
        //        //		var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail[k].Id).Select(e => e.Quantity).FirstOrDefault();

        //        //		//Inverter4.Value = InverterQTY + " x " + InverterDetail[k].Name;
        //        //		//InverterModelNo4.Value = InverterDetail[k].Model;
        //        //	}
        //        //}
        //        //if (InverterDetail != null)
        //        //{
        //        //	var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail.Id).Select(e => e.Quantity).FirstOrDefault();

        //        //	Inverter.Value = InverterQTY + " x " + InverterDetail.Name;
        //        //	InverterModelNo.Value = InverterDetail.Model;
        //        //}
        //        SignCustName.Value = Lead.CompanyName;
        //        SignCustDate.Value = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
        //        if (ImagePath != null)
        //        {
        //            CustSignature.Value = ImagePath;
        //        }

        //        //if (NearMap != null) // This is live
        //        if (NearMap == null)
        //        {
        //            if (!string.IsNullOrEmpty(NearMap.NearMapImage1))
        //            {
        //                Image image1 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage1);
        //                NearMap1.Value = image1;
        //            }
        //            if (!string.IsNullOrEmpty(NearMap.NearMapImage2))
        //            {
        //                Image image2 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage2);
        //                NearMap2.Value = image2;
        //            }
        //            if (!string.IsNullOrEmpty(NearMap.NearMapImage3))
        //            {
        //                Image image3 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage3);
        //                NearMap3.Value = image3;
        //            }
        //        }

        //        var Filename = DateTime.Now.Ticks + "_Quotation";
        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else if (OrgId == 2)
        //    {
        //        var report_parent = new Report.SavvyQuote();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        var RoofType = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
        //        var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofAngleId).Select(e => e.Name).FirstOrDefault();
        //        var HouseType = _houseTypeRepository.GetAll().Where(e => e.Id == JobDetail.HouseTypeId).Select(e => e.Name).FirstOrDefault();
        //        var Dist = _elecDistributorRepository.GetAll().Where(e => e.Id == JobDetail.ElecDistributorId).Select(e => e.Name).FirstOrDefault();
        //        var Retailer = _elecRetailerRepository.GetAll().Where(e => e.Id == JobDetail.ElecRetailerId).Select(e => e.Name).FirstOrDefault();
        //        var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
        //        var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
        //        var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).OrderByDescending(e => e.Id).FirstOrDefault();
        //        var Inverter = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).OrderByDescending(e => e.Id).FirstOrDefault();
        //        var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).ToList();
        //        var NearMap = _documentRepository.GetAll().Where(e => e.DocumentTypeId == 1 && e.JobId == JobId).OrderByDescending(e => e.Id).FirstOrDefault();
        //        var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
        //        var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
        //        var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
        //        var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
        //        var SalesRep = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
        //        var TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.InvoicePaymentStatusId == 2).Select(e => e.InvoicePayTotal).Sum();

        //        var Finance = _financeOptionRepository.GetAll().Where(e => e.Id == JobDetail.FinanceOptionId).Select(e => e.Name).FirstOrDefault();
        //        var Dep = _depositOptionRepository.GetAll().Where(e => e.Id == JobDetail.DepositOptionId).Select(e => e.Name).FirstOrDefault();
        //        var Payment = _paymentOptionRepository.GetAll().Where(e => e.Id == JobDetail.PaymentOptionId).Select(e => e.Name).FirstOrDefault();

        //        var QunityAndModelList = (from o in _jobProductItemRepository.GetAll()
        //                                  join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
        //                                  from s1 in j1.DefaultIfEmpty()
        //                                  where (o.JobId == JobDetail.Id)
        //                                  select new
        //                                  {
        //                                      Name = o.Quantity + " x " + s1.Name
        //                                  }).ToList();

        //        Telerik.Reporting.TextBox QuoteNumber1 = report_parent.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Date1 = report_parent.Items.Find("textBox140", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox22", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox MeaterPhase = report_parent.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NoofStory = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RoofPinch = report_parent.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RoofTypes = report_parent.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox EnergyDist = report_parent.Items.Find("textBox26", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox EnergyRetailer = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Total = report_parent.Items.Find("textBox41", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Capecity = report_parent.Items.Find("textBox142", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Panel = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Inverters = report_parent.Items.Find("textBox40", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox referanceno = report_parent.Items.Find("textBox200", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox deprquired = report_parent.Items.Find("textBox219", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SubTotal = report_parent.Items.Find("textBox75", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox STC = report_parent.Items.Find("textBox74", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox GrandTotal = report_parent.Items.Find("textBox73", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox STCIncetive = report_parent.Items.Find("textBox72", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SolarVICRebate = report_parent.Items.Find("textBox71", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SolarVICLoanDiscount = report_parent.Items.Find("textBox70", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox STCDesc = report_parent.Items.Find("textBox69", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Deposite = report_parent.Items.Find("textBox68", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Balance = report_parent.Items.Find("textBox67", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SignCustName = report_parent.Items.Find("textBox176", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SignCustDate = report_parent.Items.Find("textBox177", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox FinalPrice = report_parent.Items.Find("textBox220", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.PictureBox CustSignature = report_parent.Items.Find("pictureBox6", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox NearMap1 = report_parent.Items.Find("pictureBox11", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox NearMap2 = report_parent.Items.Find("pictureBox12", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox NearMap3 = report_parent.Items.Find("pictureBox13", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.TextBox Notes = report_parent.Items.Find("textBox199", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox9", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG2 = report_parent.Items.Find("pictureBox8", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG3 = report_parent.Items.Find("pictureBox5", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG4 = report_parent.Items.Find("pictureBox10", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG5 = report_parent.Items.Find("pictureBox3", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG6 = report_parent.Items.Find("pictureBox4", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.PictureBox IMG7 = report_parent.Items.Find("pictureBox7", true)[0] as Telerik.Reporting.PictureBox;

        //        Telerik.Reporting.Table SyatemDetail = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        //        SyatemDetail.DataSource = QunityAndModelList;

        //        var Path = _env.WebRootPath;

        //        Image Url1 = Image.FromFile(Path + "/PDFImages/SavvyQuote1.png");
        //        Image Url2 = Image.FromFile(Path + "/PDFImages/SavvyQuote2.png");
        //        Image Url3 = Image.FromFile(Path + "/PDFImages/SavvyQuote3.png");
        //        Image Url4 = Image.FromFile(Path + "/PDFImages/SavvyQuote4.png");

        //        IMG1.Value = Url1;
        //        IMG2.Value = Url2;
        //        IMG3.Value = Url3;
        //        IMG4.Value = Url4;
        //        IMG5.Value = Url4;
        //        IMG6.Value = Url4;
        //        IMG7.Value = Url4;

        //        var test = 0;
        //        int i = 0;
        //        foreach (var item in Addition)
        //        {
        //            if (i == 0)
        //            {
        //                test = Convert.ToInt32(item);
        //            }
        //            else
        //            {
        //                test = test + Convert.ToInt32(item);
        //            }
        //            i++;
        //        }
        //        var test1 = 0;
        //        int j = 0;
        //        foreach (var item in Discount)
        //        {
        //            if (j == 0)
        //            {
        //                test1 = Convert.ToInt32(item);
        //            }
        //            else
        //            {
        //                test1 = test1 + Convert.ToInt32(item);
        //            }
        //            j++;
        //        }
        //        FinalPrice.Value =
        //        referanceno.Value = ":" + JobDetail.JobNumber;
        //        deprquired.Value = ":" + Convert.ToString(JobDetail.DepositRequired);
        //        QuoteNumber1.Value = JobDetail.JobNumber;
        //        Date1.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
        //        Name.Value = Lead.CompanyName;
        //        Mobile.Value = Lead.Mobile;
        //        Email.Value = Lead.Email;
        //        if (JobDetail.Address != null)
        //        {
        //            Address1.Value = Convert.ToString(JobDetail.Address).Trim() + " " + JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode; ;
        //        }
        //        Capecity.Value = Convert.ToString(JobDetail.SystemCapacity) + "KW" + "Solar Photovoltaic System Including";
        //        Notes.Value = JobDetail.Note;
        //        SubTotal.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate);
        //        Total.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate);
        //        STC.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
        //        STCDesc.Value = JobDetail.STC + " * " + JobDetail.STCPrice;
        //        GrandTotal.Value = Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate)) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate));
        //        STCIncetive.Value = Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate) + ((test - test1))) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate) + ((test - test1)));
        //        SolarVICRebate.Value = Convert.ToString(JobDetail.SolarVICRebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.SolarVICRebate);
        //        SolarVICLoanDiscount.Value = Convert.ToString(JobDetail.SolarVICLoanDiscont) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.SolarVICLoanDiscont);
        //        var stcintentives = Convert.ToDecimal((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate)) == null ? 0 : Convert.ToDecimal((JobDetail.BasicCost + JobDetail.Rebate) - (JobDetail.Rebate));
        //        var SolarVICRebates = Convert.ToDecimal(JobDetail.SolarVICRebate) == null ? 0 : Convert.ToDecimal(JobDetail.SolarVICRebate);
        //        var SolarVICLoanDiscounts = Convert.ToDecimal(JobDetail.SolarVICLoanDiscont) == null ? 0 : Convert.ToDecimal(JobDetail.SolarVICLoanDiscont);

        //        FinalPrice.Value = Convert.ToString(stcintentives - SolarVICRebates - SolarVICLoanDiscounts);

        //        var totaalvicRebate = ((JobDetail.SolarVICRebate == null ? 0 : JobDetail.SolarVICRebate) + (JobDetail.SolarVICLoanDiscont == null ? 0 : JobDetail.SolarVICLoanDiscont));

        //        decimal? FinalDeposite;
        //        if (TotalDeposite == 0 || TotalDeposite == null)
        //        {
        //            //DepositeText.Value = "Deposit Required";
        //            decimal? finalcost = ((JobDetail.BasicCost) - (totaalvicRebate));
        //            Deposite.Value = Convert.ToString(JobDetail.DepositRequired);
        //            FinalDeposite = JobDetail.DepositRequired;
        //        }
        //        else
        //        {
        //            //DepositeText.Value = "Deposit Paid";
        //            Deposite.Value = Convert.ToString(JobDetail.DepositRequired);
        //            FinalDeposite = TotalDeposite;
        //        }

        //        if (TotalDeposite == 0 || TotalDeposite == null)
        //        {
        //            Balance.Value = Convert.ToString((JobDetail.BasicCost) + test - test1 - (totaalvicRebate) - JobDetail.DepositRequired) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost) + test - test1 - (totaalvicRebate) - FinalDeposite);
        //        }
        //        else
        //        {
        //            Balance.Value = Convert.ToString((JobDetail.BasicCost) + test - test1 - (totaalvicRebate) - JobDetail.DepositRequired) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost) + test - test1 - (totaalvicRebate) - JobDetail.DepositRequired);
        //        }

        //        MeaterPhase.Value = Convert.ToString(JobDetail.MeterPhaseId);

        //        NoofStory.Value = HouseType;
        //        RoofTypes.Value = RoofType;
        //        RoofPinch.Value = RoofAngle;
        //        EnergyDist.Value = Dist;
        //        EnergyRetailer.Value = Retailer;
        //        //if (PanelDetail != null)
        //        //{
        //        //	var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

        //        //	Panel.Value = PanelQTY + " x " + PanelDetail.Name;
        //        //	//PanelModelNo.Value = PanelDetail.Model;
        //        //}
        //        //if (Inverter != null)
        //        //{
        //        //	var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == Inverter.Id).Select(e => e.Quantity).FirstOrDefault();

        //        //	Inverters.Value = InverterQTY + " x " + Inverter.Name;
        //        //	//InverterModelNo.Value = InverterDetail.Model;
        //        //}
        //        SignCustName.Value = Lead.CompanyName;
        //        SignCustDate.Value = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
        //        if (ImagePath != null)
        //        {
        //            CustSignature.Value = ImagePath;
        //        }

        //        if (NearMap != null)
        //        {
        //            if (!string.IsNullOrEmpty(NearMap.NearMapImage1))
        //            {
        //                Image image1 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage1);
        //                NearMap1.Value = image1;
        //            }
        //            if (!string.IsNullOrEmpty(NearMap.NearMapImage2))
        //            {
        //                Image image2 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage2);
        //                NearMap2.Value = image2;
        //            }
        //            if (!string.IsNullOrEmpty(NearMap.NearMapImage3))
        //            {
        //                Image image3 = Image.FromFile(pathfordoc + NearMap.FilePath + "\\" + NearMap.NearMapImage3);
        //                NearMap3.Value = image3;
        //            }
        //        }

        //        var Filename = DateTime.Now.Ticks + "_Quotation";
        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        //public async Task<string> Refund(int JobId, int OrgId, int Id)
        //{
        //    if (OrgId == 0)
        //    {
        //        OrgId = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadFk.OrganizationId).FirstOrDefault();
        //    }
        //    if (OrgId == 1)
        //    {
        //        var report_parent = new Report.NewRefundReceipt();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
        //        var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
        //        //var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
        //        //var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();
        //        var JobStatus = _jobStatusRepository.GetAll().Where(e => e.Id == JobDetail.JobStatusId).Select(e => e.Name).FirstOrDefault();
        //        var CurrentUser = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
        //        var DepDateDetail = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayDate).FirstOrDefault();
        //        var JobRefundDetails = _jobRefundRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.Id == Id).FirstOrDefault();
        //        var RefundReason = _refundReasonRepository.GetAll().Where(e => e.Id == JobRefundDetails.RefundReasonId).Select(e => e.Name).FirstOrDefault();
        //        //IList<string> role = await _userManager.GetRolesAsync(CurrentUser);

        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox22", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ProjectNo = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Status = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SalesPerson = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox DepDate = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox EnterDate = report_parent.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ManagerName = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NoofPanel = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Reason = report_parent.Items.Find("textBox48", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.TextBox OName = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox OEmail = report_parent.Items.Find("textBox32", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox OMobile = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox OProjectNo = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox BankName = report_parent.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox AccountName = report_parent.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox BSENo = report_parent.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ACNo = report_parent.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Amount = report_parent.Items.Find("textBox19", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RefundDate = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ReasonNotRefund = report_parent.Items.Find("textBox49", true)[0] as Telerik.Reporting.TextBox;
        //        var Path = _env.WebRootPath;
        //        Image Url1 = Image.FromFile(Path + "/PDFImages/Refund.png");
        //        IMG1.Value = Url1;

        //        Date.Value = DateTime.Now.AddHours(15).ToString("MMMM dd, yyyy");
        //        Name.Value = Lead.CompanyName;
        //        Email.Value = Lead.Email;
        //        Mobile.Value = Lead.Mobile;
        //        ProjectNo.Value = ": " + JobDetail.JobNumber;
        //        Status.Value = ": " + JobStatus;
        //        if (CurrentUser != null)
        //        {
        //            IList<string> role = await _userManager.GetRolesAsync(CurrentUser);
        //            if (role.Contains("Sales Manager"))
        //            {
        //                SalesPerson.Value = ": ";
        //                ManagerName.Value = ": " + CurrentUser.FullName;
        //            }
        //            else
        //            {
        //                var User_List = _userRepository.GetAll().ToList();
        //                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

        //                var UserList = (from user in User_List
        //                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
        //                                from ur in urJoined.DefaultIfEmpty()

        //                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
        //                                from us in usJoined.DefaultIfEmpty()

        //                                join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
        //                                from ut in utJoined.DefaultIfEmpty()

        //                                where us != null && us.DisplayName == "Sales Rep" && ut != null && ut.TeamId == TeamId
        //                                select user).Distinct().ToList();

        //                SalesPerson.Value = ": " + CurrentUser.FullName;
        //                //ManagerName.Value = ": " + UserList[0].FullName;
        //            }
        //        }
        //        else
        //        {
        //            SalesPerson.Value = ": ";
        //            ManagerName.Value = ": ";
        //        }


        //        DepDate.Value = ": " + DepDateDetail.Value.ToString("dd/MM/yyyy");
        //        EnterDate.Value = ": " + JobDetail.CreationTime.ToString("dd/MM/yyyy");
        //        //NoofPanel.Value = ": " + Convert.ToString(PanelQTY);
        //        Reason.Value = RefundReason;

        //        OName.Value = Lead.CompanyName;
        //        OEmail.Value = Lead.Email;
        //        OMobile.Value = Lead.Mobile;
        //        OProjectNo.Value = ": " + JobDetail.JobNumber;
        //        BankName.Value = ": " + JobRefundDetails.BankName;
        //        AccountName.Value = ": " + JobRefundDetails.AccountName;
        //        BSENo.Value = ": " + JobRefundDetails.BSBNo;
        //        ACNo.Value = ": " + JobRefundDetails.AccountNo;
        //        Amount.Value = ": " + Convert.ToString(JobRefundDetails.Amount);
        //        if (JobRefundDetails.PaidDate != null)
        //        {
        //            RefundDate.Value = ": " + JobRefundDetails.PaidDate.Value.ToString("DD/MM/yyyy");
        //        }
        //        ReasonNotRefund.Value = JobRefundDetails.Remarks;

        //        var Filename = DateTime.Now.Ticks + "_Refund";

        //        var MainFolder = pathfordoc;

        //        MainFolder = MainFolder + "\\Documents";
        //        var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //        var JobNumber1 = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

        //        string TenantPath = MainFolder + "\\" + TenantName;
        //        string JobPath = TenantPath + "\\" + JobNumber1;
        //        string SignaturePath = JobPath + "\\Reports";
        //        if (JobPath.Contains(JobNumber1))
        //        {
        //            FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //            foreach (var item in files)
        //            {
        //                if (item.FullName.Contains("_Refund.pdf"))
        //                {
        //                    System.IO.File.Delete(Convert.ToString(item));
        //                }
        //            }
        //        }
        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else if (OrgId == 2)
        //    {

        //        var report_parent = new Report.SavvyRefund();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
        //        var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
        //        var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
        //        var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();
        //        var JobStatus = _jobStatusRepository.GetAll().Where(e => e.Id == JobDetail.JobStatusId).Select(e => e.Name).FirstOrDefault();
        //        var CurrentUser = _userRepository.GetAll().Where(e => e.Id == Lead.AssignToUserID).FirstOrDefault();
        //        var DepDateDetail = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayDate).FirstOrDefault();
        //        var JobRefundDetails = _jobRefundRepository.GetAll().Where(e => e.JobId == JobDetail.Id && e.Id == Id).FirstOrDefault();
        //        var RefundReason = _refundReasonRepository.GetAll().Where(e => e.Id == JobRefundDetails.RefundReasonId).Select(e => e.Name).FirstOrDefault();
        //        //IList<string> role = await _userManager.GetRolesAsync(CurrentUser);

        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.TextBox OProjectNo = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox JobNumber = report_parent.Items.Find("textBox36", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Status = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SalesPerson = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox DepDate = report_parent.Items.Find("textBox40", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox EnterDate = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ManagerName = report_parent.Items.Find("textBox38", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NoofPanel = report_parent.Items.Find("textBox37", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Reason = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox OName = report_parent.Items.Find("textBox32", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox BankName = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox AccountName = report_parent.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox BSENo = report_parent.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ACNo = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Amount = report_parent.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RefundDate = report_parent.Items.Find("textBox42", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ReasonNotRefund = report_parent.Items.Find("textBox41", true)[0] as Telerik.Reporting.TextBox;
        //        var Path = _env.WebRootPath;
        //        Image Url1 = Image.FromFile(Path + "/PDFImages/SavvyRefund.png");
        //        IMG1.Value = Url1;

        //        Name.Value = Lead.CompanyName;
        //        Status.Value = JobStatus;
        //        if (CurrentUser != null)
        //        {
        //            IList<string> role = await _userManager.GetRolesAsync(CurrentUser);
        //            if (role.Contains("Sales Manager"))
        //            {
        //                SalesPerson.Value = ": ";
        //                ManagerName.Value = ": " + CurrentUser.FullName;
        //            }
        //            else
        //            {
        //                var User_List = _userRepository.GetAll().ToList();
        //                var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

        //                var UserList = (from user in User_List
        //                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
        //                                from ur in urJoined.DefaultIfEmpty()

        //                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
        //                                from us in usJoined.DefaultIfEmpty()

        //                                join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
        //                                from ut in utJoined.DefaultIfEmpty()

        //                                where us != null && us.DisplayName == "Sales Rep" && ut != null && ut.TeamId == TeamId
        //                                select user).Distinct().ToList();

        //                SalesPerson.Value = ": " + CurrentUser.FullName;
        //                ManagerName.Value = ": " + UserList[0].FullName;
        //            }
        //        }
        //        else
        //        {
        //            SalesPerson.Value = ": ";
        //            ManagerName.Value = ": ";
        //        }

        //        DepDate.Value = DepDateDetail.Value.ToString("dd/MM/yyyy");
        //        EnterDate.Value = JobDetail.CreationTime.ToString("dd/MM/yyyy");
        //        NoofPanel.Value = Convert.ToString(PanelQTY);
        //        Reason.Value = JobRefundDetails.Notes;

        //        OName.Value = Lead.CompanyName;
        //        OProjectNo.Value = JobDetail.JobNumber;
        //        JobNumber.Value = JobDetail.JobNumber;
        //        BankName.Value = JobRefundDetails.BankName;
        //        AccountName.Value = JobRefundDetails.AccountName;
        //        BSENo.Value = JobRefundDetails.BSBNo;
        //        ACNo.Value = JobRefundDetails.AccountNo;
        //        Amount.Value = "$ " + Convert.ToString(JobRefundDetails.Amount);
        //        if (JobRefundDetails.PaidDate != null)
        //        {
        //            RefundDate.Value = JobRefundDetails.PaidDate.Value.ToString("DD/MM/yyyy");
        //        }
        //        ReasonNotRefund.Value = JobRefundDetails.Remarks;

        //        var Filename = DateTime.Now.Ticks + "_SavvyRefund";

        //        var MainFolder = pathfordoc;
        //        MainFolder = MainFolder + "\\Documents";
        //        var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //        var JobNumber1 = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

        //        string TenantPath = MainFolder + "\\" + TenantName;
        //        string JobPath = TenantPath + "\\" + JobNumber1;
        //        string SignaturePath = JobPath + "\\Reports";
        //        if (JobPath.Contains(JobNumber1))
        //        {
        //            FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //            foreach (var item in files)
        //            {
        //                if (item.FullName.Contains("_Refund.pdf"))
        //                {
        //                    System.IO.File.Delete(Convert.ToString(item));
        //                }
        //            }
        //        }

        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else
        //    {
        //        return null;
        //    }

        //}

        //public string TaxInvoice(int JobId, int OrgId, bool? invoiceimpotorpayment)
        //{
        //    if (OrgId == 0)
        //    {
        //        OrgId = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadFk.OrganizationId).FirstOrDefault();
        //    }
        //    if (OrgId == 1)
        //    {

        //        var report_parent = new Report.NewTaxInvoice();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
        //        var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
        //        var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
        //        var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).FirstOrDefault();
        //        var DepositeDetail = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).FirstOrDefault();
        //        var DepositeDetailList = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayTotal).Sum();

        //        var importDepositeDetail = _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobDetail.Id).FirstOrDefault();
        //        var importDepositeDetailList = _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.PaidAmmount).Sum();

        //        var QunityAndModelList = (from o in _jobProductItemRepository.GetAll()
        //                                  join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
        //                                  from s1 in j1.DefaultIfEmpty()
        //                                  where (o.JobId == JobDetail.Id)
        //                                  select new
        //                                  {
        //                                      Name = o.Quantity + " x " + s1.Name
        //                                  }).ToList();

        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;

        //        Telerik.Reporting.TextBox CustName = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustMobile = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustEmail = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustAddress1 = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox CustAddress2 = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox BalanceDue = report_parent.Items.Find("textBox54", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InvoiceNo = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Panel = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox Inverter = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.TextBox TotalCost = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox LessSTC = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NetCost = report_parent.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox DepositePaid = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Balance = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox HeaderDate = report_parent.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox FinalBalance = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.Table SyatemDetail = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        //        SyatemDetail.DataSource = QunityAndModelList;
        //        //Telerik.Reporting.Table Syatem1Detail = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        //        //Syatem1Detail.DataSource = InverterList;

        //        var Path = _env.WebRootPath;
        //        Image Url1 = Image.FromFile(Path + "/PDFImages/TaxInvoice.png");
        //        IMG1.Value = Url1;

        //        CustName.Value = Lead.CompanyName;
        //        CustMobile.Value = ": " + Lead.Mobile;
        //        CustEmail.Value = ": " + Lead.Email;
        //        if (JobDetail.Address != null)
        //        {
        //            CustAddress1.Value = ": " + JobDetail.Address.Trim() + " " + JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
        //        }
        //        //CustAddress2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;

        //        InvoiceNo.Value = JobDetail.JobNumber;
        //        TotalCost.Value = Convert.ToString(JobDetail.TotalCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost + JobDetail.Rebate);
        //        LessSTC.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
        //        NetCost.Value = Convert.ToString(JobDetail.TotalCost) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost);
        //        if (DepositeDetail != null && invoiceimpotorpayment == false)
        //        {
        //            DepositePaid.Value = Convert.ToString(DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(DepositeDetailList);
        //        }
        //        if (DepositeDetail != null && invoiceimpotorpayment == true)
        //        {
        //            DepositePaid.Value = Convert.ToString(importDepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(importDepositeDetailList);
        //        }
        //        if (invoiceimpotorpayment == false)
        //        {
        //            Balance.Value = Convert.ToString(JobDetail.TotalCost - DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - DepositeDetailList);
        //            FinalBalance.Value = Convert.ToString(JobDetail.TotalCost - DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - DepositeDetailList);
        //            BalanceDue.Value = Convert.ToString(JobDetail.TotalCost - DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - DepositeDetailList);
        //        }
        //        if (invoiceimpotorpayment == true)
        //        {
        //            Balance.Value = Convert.ToString(JobDetail.TotalCost - importDepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - importDepositeDetailList);
        //            FinalBalance.Value = Convert.ToString(JobDetail.TotalCost - importDepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - importDepositeDetailList);
        //            BalanceDue.Value = Convert.ToString(JobDetail.TotalCost - importDepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost - importDepositeDetailList);
        //        }
        //        Date.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
        //        HeaderDate.Value = DateTime.Now.AddHours(15).ToString("MMMM dd, yyyy");
        //        //GST.Value = Convert.ToString(JobDetail.BasicCost * 10 / 100) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost * 10 / 100);
        //        //if (PanelDetail != null)
        //        //{
        //        //	var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

        //        //	Panel.Value = PanelQTY + " x " + PanelDetail.Name + "(" + PanelDetail.Model + ")";
        //        //}
        //        //if (InverterDetail != null)
        //        //{
        //        //	var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail.Id).Select(e => e.Quantity).FirstOrDefault();

        //        //	Inverter.Value = InverterQTY + " x " + InverterDetail.Name + "(" + InverterDetail.Model + ") KW Inverter";
        //        //}

        //        var Filename = DateTime.Now.Ticks + "_TaxInvoice";
        //        var MainFolder = pathfordoc;
        //        MainFolder = MainFolder + "\\Documents";
        //        var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //        var JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

        //        string TenantPath = MainFolder + "\\" + TenantName;
        //        string JobPath = TenantPath + "\\" + JobNumber;
        //        string SignaturePath = JobPath + "\\Reports";
        //        if (JobPath.Contains(JobNumber))
        //        {
        //            FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //            foreach (var item in files)
        //            {
        //                if (item.FullName.Contains("_TaxInvoice.pdf"))
        //                {
        //                    System.IO.File.Delete(Convert.ToString(item));
        //                }
        //            }
        //        }
        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else if (OrgId == 2)
        //    {

        //        var report_parent = new Report.SavvyTaxInvoice();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        var ProductItemList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).Select(e => e.ProductItemId).ToList();
        //        var ProductItemsList = _jobProductItemRepository.GetAll().Where(j => j.JobId == JobDetail.Id).ToList();
        //        var PanelDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 1).FirstOrDefault();
        //        var InverterDetail = _ProductItemRepository.GetAll().Where(e => ProductItemList.Contains(e.Id) && e.ProductTypeId == 2).FirstOrDefault();
        //        var DepositeDetail = _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.PaidAmmount).Sum();
        //        var DepositeDetailList = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayTotal).Sum();

        //        var importDepositeDetail = _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobDetail.Id).FirstOrDefault();
        //        var importDepositeDetailList = _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.PaidAmmount).Sum();


        //        var QunityAndModelList = (from o in _jobProductItemRepository.GetAll()
        //                                  join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
        //                                  from s1 in j1.DefaultIfEmpty()
        //                                  where (o.JobId == JobDetail.Id)
        //                                  select new
        //                                  {
        //                                      Name = o.Quantity + " x " + s1.Name
        //                                  }).ToList();

        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.TextBox InvoiceNo = report_parent.Items.Find("textBox55", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.TextBox CustName = report_parent.Items.Find("textBox59", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustMobile = report_parent.Items.Find("textBox58", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustPhone = report_parent.Items.Find("textBox57", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustEmail = report_parent.Items.Find("textBox56", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustAddress1 = report_parent.Items.Find("textBox63", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustState = report_parent.Items.Find("textBox61", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustSuburb = report_parent.Items.Find("textBox62", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustPostCode = report_parent.Items.Find("textBox60", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox BalanceDue = report_parent.Items.Find("textBox78", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Panel = report_parent.Items.Find("textBox65", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Inverter = report_parent.Items.Find("textBox66", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Capecity = report_parent.Items.Find("textBox64", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox TotalCost = report_parent.Items.Find("textBox68", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox LessSTC = report_parent.Items.Find("textBox67", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SolarvicLoan = report_parent.Items.Find("textBox72", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox SolarvicRebate = report_parent.Items.Find("textBox70", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NetCost = report_parent.Items.Find("textBox74", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox DepositePaid = report_parent.Items.Find("textBox76", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox HeaderDate = report_parent.Items.Find("textBox54", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox GST = report_parent.Items.Find("textBox80", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox referanceno = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox deprquired = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;



        //        referanceno.Value = ":" + JobDetail.JobNumber;
        //        deprquired.Value = ":" + Convert.ToString(JobDetail.DepositRequired);
        //        Capecity.Value = Convert.ToString(JobDetail.SystemCapacity) + "KW";
        //        var Path = _env.WebRootPath;
        //        Image Url1 = Image.FromFile(Path + "/PDFImages/SavvyTaxInvoice.png");
        //        IMG1.Value = Url1;

        //        CustName.Value = Lead.CompanyName;
        //        CustMobile.Value = Lead.Mobile;
        //        CustPhone.Value = Lead.Phone;
        //        CustEmail.Value = Lead.Email;
        //        if (JobDetail.Address != null)
        //        {
        //            CustAddress1.Value = JobDetail.Address.Trim();
        //            CustState.Value = JobDetail.State;
        //            CustSuburb.Value = JobDetail.Suburb;
        //            CustPostCode.Value = JobDetail.PostalCode;
        //        }
        //        InvoiceNo.Value = JobDetail.JobNumber;
        //        TotalCost.Value = Convert.ToString(JobDetail.TotalCost + JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.TotalCost + JobDetail.Rebate);
        //        LessSTC.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
        //        SolarvicLoan.Value = Convert.ToString(JobDetail.SolarVICLoanDiscont) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.SolarVICLoanDiscont);
        //        SolarvicRebate.Value = Convert.ToString(JobDetail.SolarVICRebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.SolarVICRebate);
        //        var totalvalue = JobDetail.TotalCost + JobDetail.Rebate == null ? 0 : JobDetail.TotalCost + JobDetail.Rebate;
        //        var lessvic = JobDetail.Rebate == null ? 0 : JobDetail.Rebate;
        //        var solarvicLoan = JobDetail.SolarVICLoanDiscont == null ? 0 : JobDetail.SolarVICLoanDiscont;
        //        var solarvicRebate = JobDetail.SolarVICRebate == null ? 0 : JobDetail.SolarVICRebate;
        //        var finalnetcost = (totalvalue - lessvic - solarvicLoan - solarvicRebate);
        //        NetCost.Value = Convert.ToString(finalnetcost) == null ? "$ 0.00" : "$ " + Convert.ToString(finalnetcost);

        //        if (DepositeDetail != null && invoiceimpotorpayment == false)
        //        {
        //            DepositePaid.Value = Convert.ToString(DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(DepositeDetailList);
        //        }
        //        if (DepositeDetail != null && invoiceimpotorpayment == true)
        //        {
        //            DepositePaid.Value = Convert.ToString(importDepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(importDepositeDetailList);
        //        }
        //        if (invoiceimpotorpayment == false)
        //        {
        //            BalanceDue.Value = Convert.ToString(finalnetcost - DepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(finalnetcost - DepositeDetailList);
        //        }
        //        if (invoiceimpotorpayment == true)
        //        {
        //            BalanceDue.Value = Convert.ToString(finalnetcost - importDepositeDetailList) == null ? "$ 0.00" : "$ " + Convert.ToString(finalnetcost - importDepositeDetailList);
        //        }
        //        HeaderDate.Value = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
        //        GST.Value = Convert.ToString(JobDetail.BasicCost * 10 / 100) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost * 10 / 100);
        //        if (PanelDetail != null)
        //        {
        //            var PanelQTY = ProductItemsList.Where(e => e.ProductItemId == PanelDetail.Id).Select(e => e.Quantity).FirstOrDefault();

        //            Panel.Value = PanelQTY + " x " + PanelDetail.Name;
        //        }
        //        if (InverterDetail != null)
        //        {
        //            var InverterQTY = ProductItemsList.Where(e => e.ProductItemId == InverterDetail.Id).Select(e => e.Quantity).FirstOrDefault();

        //            Inverter.Value = InverterQTY + " x " + InverterDetail.Name;
        //        }

        //        var Filename = DateTime.Now.Ticks + "_TaxInvoice";

        //        var MainFolder = pathfordoc;
        //        MainFolder = MainFolder + "\\Documents";
        //        var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //        var JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

        //        string TenantPath = MainFolder + "\\" + TenantName;
        //        string JobPath = TenantPath + "\\" + JobNumber;
        //        string SignaturePath = JobPath + "\\Reports";
        //        if (JobPath.Contains(JobNumber))
        //        {
        //            FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //            foreach (var item in files)
        //            {
        //                if (item.FullName.Contains("_TaxInvoice.pdf"))
        //                {
        //                    System.IO.File.Delete(Convert.ToString(item));
        //                }
        //            }
        //        }
        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        //public string PaymentReceipt(int JobId, int OrgId, bool? invoiceimpotorpayment)
        //{
        //    if (OrgId == 0)
        //    {
        //        OrgId = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadFk.OrganizationId).FirstOrDefault();
        //    }
        //    if (OrgId == 1)
        //    {
        //        var report_parent = new Report.NewPaymentReceipt();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        //var TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayTotal).Sum();
        //        decimal? TotalDeposite = 0.0M;
        //        if (invoiceimpotorpayment == true)
        //        {
        //            TotalDeposite = _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.PaidAmmount).Sum();
        //        }
        //        if (invoiceimpotorpayment == false)
        //        {
        //            TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayTotal).Sum();
        //        }
        //        var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
        //        var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
        //        var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
        //        var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();

        //        var DepositeDetailList = from o in _InvoiceImportDataRepository.GetAll()
        //                                 join o1 in _invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o1.Id into j1
        //                                 from s1 in j1.DefaultIfEmpty()
        //                                 where (o.JobId == JobDetail.Id)
        //                                 select new
        //                                 {
        //                                     o.Date,
        //                                     o.PaidAmmount,
        //                                     o.SSCharge,
        //                                     s1.PaymentMethod
        //                                 };

        //        var DepositeList = from o in _invoicePaymentRepository.GetAll()
        //                           join o1 in _invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o1.Id into j1
        //                           from s1 in j1.DefaultIfEmpty()
        //                           where (o.JobId == JobDetail.Id)
        //                           select new
        //                           {
        //                               o.InvoicePayDate,
        //                               o.InvoicePayTotal,
        //                               o.CCSurcharge,
        //                               s1.PaymentMethod
        //                           };

        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;

        //        Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Address2 = report_parent.Items.Find("textBox14", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InvoiceTotal = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox TotalCost = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox LessDiscount = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox LessSTCRebate = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NetCost = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox BalanceDue = report_parent.Items.Find("textBox15", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox HeaderDate = report_parent.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustName = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.Table Payment = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;

        //        var Path = _env.WebRootPath;
        //        Image Url1 = Image.FromFile(Path + "/PDFImages/PaymentReceipt.png");
        //        IMG1.Value = Url1;

        //        if (JobDetail.Address != null)
        //        {
        //            Address1.Value = JobDetail.Address.Trim();
        //        }
        //        Address2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
        //        Name.Value = Lead.CompanyName;
        //        Mobile.Value = ": " + Lead.Mobile;
        //        Email.Value = ": " + Lead.Email;
        //        InvoiceTotal.Value = Convert.ToString(JobDetail.BasicCost) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost);
        //        decimal Add = 0;
        //        foreach (var item in Addition)
        //        {
        //            Add = (decimal)+item;
        //        }
        //        TotalCost.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate + Add) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate + Add);
        //        LessSTCRebate.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
        //        decimal Disc = 0;
        //        foreach (var item in Discount)
        //        {
        //            Disc = (decimal)+item;
        //        }
        //        LessDiscount.Value = Convert.ToString(Disc) == null ? "0.00" : "$ " + Convert.ToString(Disc);
        //        NetCost.Value = Convert.ToString(JobDetail.BasicCost - Disc) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost - Disc);
        //        if (invoiceimpotorpayment == true)
        //        {
        //            Payment.DataSource = DepositeDetailList.ToList();
        //        }
        //        if (invoiceimpotorpayment == false)
        //        {
        //            Payment.DataSource = DepositeList.ToList();
        //        }
        //        var Due = JobDetail.TotalCost - TotalDeposite;
        //        var GST = Due * 10 / 100;
        //        BalanceDue.Value = ": " + Convert.ToString(Due) == null ? "$ 0.00" : "$ " + Convert.ToString(Due);
        //        CustName.Value = Lead.CompanyName;
        //        Date.Value = DateTime.Now.AddHours(15).ToString("dd MMM, yyyy");
        //        HeaderDate.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");

        //        var Filename = DateTime.Now.Ticks + "_PaymentReceipt";
        //        var MainFolder = pathfordoc;
        //        MainFolder = MainFolder + "\\Documents";
        //        var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //        var JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

        //        string TenantPath = MainFolder + "\\" + TenantName;
        //        string JobPath = TenantPath + "\\" + JobNumber;
        //        string SignaturePath = JobPath + "\\Reports";
        //        if (JobPath.Contains(JobNumber))
        //        {
        //            FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //            foreach (var item in files)
        //            {
        //                if (item.FullName.Contains("_PaymentReceipt.pdf"))
        //                {
        //                    System.IO.File.Delete(Convert.ToString(item));
        //                }
        //            }
        //        }
        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else if (OrgId == 2)
        //    {
        //        var report_parent = new Report.SavvyPaymentReceipt();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        decimal? TotalDeposite = 0.0M;
        //        if (invoiceimpotorpayment == true)
        //        {
        //            TotalDeposite = _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.PaidAmmount).Sum();
        //        }
        //        if (invoiceimpotorpayment == false)
        //        {
        //            TotalDeposite = _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobDetail.Id).Select(e => e.InvoicePayTotal).Sum();
        //        }
        //        var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
        //        var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
        //        var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
        //        var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();

        //        var DepositeDetailList = from o in _InvoiceImportDataRepository.GetAll()
        //                                 join o1 in _invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o1.Id into j1
        //                                 from s1 in j1.DefaultIfEmpty()
        //                                 where (o.JobId == JobDetail.Id)
        //                                 select new
        //                                 {
        //                                     o.Date,
        //                                     o.PaidAmmount,
        //                                     o.SSCharge,
        //                                     s1.PaymentMethod
        //                                 };

        //        var DepositeList = from o in _invoicePaymentRepository.GetAll()
        //                           join o1 in _invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o1.Id into j1
        //                           from s1 in j1.DefaultIfEmpty()
        //                           where (o.JobId == JobDetail.Id)
        //                           select new
        //                           {
        //                               Date = o.InvoicePayDate,
        //                               PaidAmmount = o.InvoicePayTotal,
        //                               SSCharge = o.CCSurcharge,
        //                               s1.PaymentMethod
        //                           };


        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.TextBox headerInvoiceNo = report_parent.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InvoiceNo = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox36", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox28", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Phone = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox30", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox32", true)[0] as Telerik.Reporting.TextBox;
        //        //Telerik.Reporting.TextBox InvoiceTotal = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox LessRebate = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox TotalCost = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox LessDiscount = report_parent.Items.Find("textBox37", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox LessSTCRebate = report_parent.Items.Find("textBox40", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NetCost = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox BalanceDue = report_parent.Items.Find("textBox56", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.Table Payment = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;

        //        var Path = _env.WebRootPath;
        //        Image Url1 = Image.FromFile(Path + "/PDFImages/SavvyPaymentReceipt.png");
        //        IMG1.Value = Url1;

        //        if (JobDetail.Address != null)
        //        {
        //            Address1.Value = JobDetail.Address.Trim() + " " + JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
        //        }
        //        headerInvoiceNo.Value = JobDetail.JobNumber;
        //        InvoiceNo.Value = JobDetail.JobNumber;
        //        Name.Value = Lead.CompanyName;
        //        Mobile.Value = Lead.Mobile;
        //        Email.Value = Lead.Email;
        //        Phone.Value = Lead.Phone;
        //        var SolarVICRebates = Convert.ToDecimal(JobDetail.SolarVICRebate) == null ? 0 : Convert.ToDecimal(JobDetail.SolarVICRebate);
        //        var SolarVICLoanDiscounts = Convert.ToDecimal(JobDetail.SolarVICLoanDiscont) == null ? 0 : Convert.ToDecimal(JobDetail.SolarVICLoanDiscont);
        //        var totalvicrebate = (SolarVICRebates + SolarVICLoanDiscounts);
        //        LessRebate.Value = Convert.ToString(totalvicrebate) == null ? "$ 0.00" : "$ " + Convert.ToString(totalvicrebate);
        //        //InvoiceTotal.Value = Convert.ToString(JobDetail.BasicCost) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost);
        //        decimal Add = 0;
        //        foreach (var item in Addition)
        //        {
        //            Add = (decimal)+item;
        //        }
        //        TotalCost.Value = Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate + Add) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.BasicCost + JobDetail.Rebate + Add);
        //        LessSTCRebate.Value = Convert.ToString(JobDetail.Rebate) == null ? "$ 0.00" : "$ " + Convert.ToString(JobDetail.Rebate);
        //        var Leesstc = JobDetail.Rebate == null ? 0 : JobDetail.Rebate;

        //        decimal Disc = 0;
        //        foreach (var item in Discount)
        //        {
        //            Disc = (decimal)+item;
        //        }
        //        LessDiscount.Value = Convert.ToString(Disc) == null ? "0.00" : "$ " + Convert.ToString(Disc);
        //        var finalstcandvic = (Leesstc + totalvicrebate + Disc);
        //        NetCost.Value = Convert.ToString(((JobDetail.BasicCost + JobDetail.Rebate + Add) - finalstcandvic) == null ? "$ 0.00" : "$ " + Convert.ToString((JobDetail.BasicCost + JobDetail.Rebate + Add) - finalstcandvic));
        //        if (invoiceimpotorpayment == true)
        //        {
        //            Payment.DataSource = DepositeDetailList.ToList();
        //        }
        //        if (invoiceimpotorpayment == false)
        //        {
        //            Payment.DataSource = DepositeList.ToList();
        //        }
        //        var Due = ((JobDetail.TotalCost - TotalDeposite) - totalvicrebate);
        //        var GST = Due * 10 / 100;
        //        BalanceDue.Value = ": " + Convert.ToString(Due) == null ? "$ 0.00" : "$ " + Convert.ToString(Due);
        //        Date.Value = DateTime.Now.AddHours(15).ToString("dd/MM/yyyy");
        //        var Filename = DateTime.Now.Ticks + "_PaymentReceipt";


        //        var MainFolder = pathfordoc;
        //        MainFolder = MainFolder + "\\Documents";
        //        var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //        var JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

        //        string TenantPath = MainFolder + "\\" + TenantName;
        //        string JobPath = TenantPath + "\\" + JobNumber;
        //        string SignaturePath = JobPath + "\\Reports";
        //        if (JobPath.Contains(JobNumber))
        //        {
        //            FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //            foreach (var item in files)
        //            {
        //                if (item.FullName.Contains("_PaymentReceipt.pdf"))
        //                {
        //                    System.IO.File.Delete(Convert.ToString(item));
        //                }
        //            }
        //        }
        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        //public async Task<string> PickList(int JobId, int OrgId, int PicklistId)
        //{
        //    if (OrgId == 0)
        //    {
        //        OrgId = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadFk.OrganizationId).FirstOrDefault();
        //    }
        //    if (OrgId == 1)
        //    {
        //        var report_parent = new Report.NewPickList();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        var Installer = _userRepository.GetAll().Where(e => e.Id == JobDetail.InstallerId).FirstOrDefault();
        //        var RoofTypeName = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
        //        var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
        //        var WareHouse = _wareHouseLocationRepository.GetAll().Where(e => e.Id == JobDetail.WarehouseLocation).Select(e => e.state).FirstOrDefault();
        //        var PickList = await _pickListAppService.GetPickListForReport(PicklistId);

        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;

        //        Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustAddress1 = report_parent.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustAddress2 = report_parent.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InstallDate = report_parent.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InstallerName = report_parent.Items.Find("textBox46", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox PickedBy = report_parent.Items.Find("textBox48", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox PickedDate = report_parent.Items.Find("textBox47", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.TextBox HouseType = report_parent.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RoofType = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Angle = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ManualQuote = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ProjectNo = report_parent.Items.Find("textBox22", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Store = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Notes = report_parent.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.Table PickListTable = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;

        //        var Path = _env.WebRootPath;
        //        Image Url1 = Image.FromFile(Path + "/PDFImages/PickList.png");
        //        IMG1.Value = Url1;

        //        Name.Value = Lead.CompanyName;
        //        Mobile.Value = Lead.Mobile;
        //        Email.Value = Lead.Email;
        //        CustAddress1.Value = JobDetail.Address.Trim();
        //        CustAddress2.Value = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
        //        InstallDate.Value = JobDetail.InstallationDate.Value.ToString("dd MMM yyyy");
        //        InstallerName.Value = ": " + Installer.FullName;
        //        PickedBy.Value = ": " + Installer.FullName;
        //        PickedDate.Value = ": " + DateTime.Now.AddHours(15).ToString("dd MMM yyyy");
        //        if (JobDetail.HouseTypeId == 1)
        //        {
        //            HouseType.Value = ": " + "Single";
        //        }
        //        else if (JobDetail.HouseTypeId == 2)
        //        {
        //            HouseType.Value = ": " + "Double";
        //        }
        //        else
        //        {
        //            HouseType.Value = ": " + "Multiple";
        //        }
        //        RoofType.Value = ": " + RoofTypeName;
        //        Angle.Value = ": " + RoofAngle;
        //        ProjectNo.Value = ": " + JobDetail.JobNumber;
        //        Store.Value = ": " + WareHouse;
        //        ManualQuote.Value = ": " + JobDetail.ManualQuote;
        //        Notes.Value = JobDetail.InstallationNotes;
        //        PickListTable.DataSource = PickList;

        //        var Filename = DateTime.Now.Ticks + "_PickList";

        //        var MainFolder = pathfordoc;
        //        MainFolder = MainFolder + "\\Documents";
        //        var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //        var JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

        //        string TenantPath = MainFolder + "\\" + TenantName;
        //        string JobPath = TenantPath + "\\" + JobNumber;
        //        string SignaturePath = JobPath + "\\Reports";
        //        if (JobPath.Contains(JobNumber))
        //        {
        //            FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //            foreach (var item in files)
        //            {
        //                if (item.FullName.Contains("_PickList.pdf"))
        //                {
        //                    System.IO.File.Delete(Convert.ToString(item));
        //                }
        //            }
        //        }

        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else if (OrgId == 2)
        //    {
        //        var report_parent = new Report.SavvyPickList();
        //        var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
        //        var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
        //        var Installer = _userRepository.GetAll().Where(e => e.Id == JobDetail.InstallerId).FirstOrDefault();
        //        var RoofTypeName = _roofTypeRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
        //        var RoofAngle = _roofAngleRepository.GetAll().Where(e => e.Id == JobDetail.RoofTypeId).Select(e => e.Name).FirstOrDefault();
        //        var WareHouse = _wareHouseLocationRepository.GetAll().Where(e => e.Id == JobDetail.WarehouseLocation).Select(e => e.state).FirstOrDefault();
        //        var PickList = await _pickListAppService.GetPickListForReport(PicklistId);

        //        Telerik.Reporting.PictureBox IMG1 = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;
        //        Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ProjectNo = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox36", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox35", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox RoofType = report_parent.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox ManualQuote = report_parent.Items.Find("textBox33", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox CustAddress1 = report_parent.Items.Find("textBox32", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InstallerName = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Mobile = report_parent.Items.Find("textBox40", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox HouseType = report_parent.Items.Find("textBox39", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Angle = report_parent.Items.Find("textBox38", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Store = report_parent.Items.Find("textBox37", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox NameInstaller = report_parent.Items.Find("textBox54", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox InstallDate = report_parent.Items.Find("textBox53", true)[0] as Telerik.Reporting.TextBox;
        //        Telerik.Reporting.TextBox Notes = report_parent.Items.Find("textBox55", true)[0] as Telerik.Reporting.TextBox;

        //        Telerik.Reporting.Table PickListTable = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;

        //        var Path = _env.WebRootPath;
        //        Image Url1 = Image.FromFile(Path + "/PDFImages/SavvyPickList.png");
        //        IMG1.Value = Url1;

        //        Name.Value = Lead.CompanyName;
        //        Mobile.Value = Lead.Mobile;
        //        Email.Value = Lead.Email;
        //        var address = JobDetail.Address.Trim();
        //        CustAddress1.Value = address + " " + JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
        //        InstallDate.Value = DateTime.Now.AddHours(15).ToString("MMM dd, yyyy");
        //        Date.Value = JobDetail.InstallationDate.Value == null ? null : JobDetail.InstallationDate.Value.ToString("dd MMM yyyy");
        //        InstallerName.Value = Installer.FullName;
        //        NameInstaller.Value = Installer.FullName;
        //        if (JobDetail.HouseTypeId == 1)
        //        {
        //            HouseType.Value = "Single";
        //        }
        //        else if (JobDetail.HouseTypeId == 2)
        //        {
        //            HouseType.Value = "Double";
        //        }
        //        else
        //        {
        //            HouseType.Value = "Multiple";
        //        }
        //        RoofType.Value = RoofTypeName;
        //        Angle.Value = RoofAngle;
        //        ProjectNo.Value = JobDetail.JobNumber;
        //        Store.Value = WareHouse;
        //        ManualQuote.Value = JobDetail.ManualQuote;
        //        Notes.Value = JobDetail.InstallationNotes;
        //        PickListTable.DataSource = PickList;

        //        var Filename = DateTime.Now.Ticks + "_SavvyPickList";


        //        var MainFolder = pathfordoc;
        //        MainFolder = MainFolder + "\\Documents";
        //        var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //        var JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();

        //        string TenantPath = MainFolder + "\\" + TenantName;
        //        string JobPath = TenantPath + "\\" + JobNumber;
        //        string SignaturePath = JobPath + "\\Reports";
        //        if (JobPath.Contains(JobNumber))
        //        {
        //            FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //            foreach (var item in files)
        //            {
        //                if (item.FullName.Contains("_PickList.pdf"))
        //                {
        //                    System.IO.File.Delete(Convert.ToString(item));
        //                }
        //            }
        //        }

        //        SaveTelerikPDF(report_parent, JobDetail.Id, JobDetail.TenantId, Filename);
        //        return Filename;
        //    }
        //    else
        //    {
        //        return null;
        //    }

        //}

        //private void SaveTelerikPDF(Telerik.Reporting.Report rpt, int JobId, int TenantId, string FileName)
        //{
        //    Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        //    Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        //    instanceReportSource.ReportDocument = rpt;

        //    Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        //    string fileName = FileName + "." + result.Extension;

        //    //string fullPath = Path.Combine(_appFolders.Report, fileName);
        //    var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(result.DocumentBytes, fileName, "Reports", JobId, 0, AbpSession.TenantId);
        //}

        //public string Acknowledgement(int DocId)
        //{
        //    var report_parent = new Report.acknowledgement();

        //    var JobAcknoDetails = _jobAcknowledgementRepqository.GetAll().Where(e => e.Id == DocId).FirstOrDefault();
        //    var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobAcknoDetails.JobId).FirstOrDefault();
        //    var OrgId = _jobRepository.GetAll().Where(e => e.Id == JobAcknoDetails.JobId).Select(e => e.LeadFk.OrganizationId).FirstOrDefault();

        //    Telerik.Reporting.TextBox txtLogo = report_parent.Items.Find("txtLogo", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.TextBox txtHeader = report_parent.Items.Find("txtHeader", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.TextBox txtName = report_parent.Items.Find("txtName", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.TextBox txtAddress = report_parent.Items.Find("txtAddress", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.TextBox txtMobile = report_parent.Items.Find("txtMobile", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.TextBox txtEmail = report_parent.Items.Find("txtEmail", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.TextBox txtDocNo = report_parent.Items.Find("txtDocNo", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.TextBox txtDetails = report_parent.Items.Find("txtDetails", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.TextBox txtDetails1 = report_parent.Items.Find("txtDetails1", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.PictureBox picSign = report_parent.Items.Find("picSign", true)[0] as Telerik.Reporting.PictureBox;
        //    Telerik.Reporting.TextBox txtSignDate = report_parent.Items.Find("txtSignDate", true)[0] as Telerik.Reporting.TextBox;
        //    Telerik.Reporting.CheckBox chkYes = report_parent.Items.Find("chkYes", true)[0] as Telerik.Reporting.CheckBox;
        //    Telerik.Reporting.CheckBox chkNo = report_parent.Items.Find("chkNo", true)[0] as Telerik.Reporting.CheckBox;

        //    if (OrgId == 1)
        //    {
        //        txtLogo.Value = "Solar Bridge";
        //    }
        //    else
        //    {
        //        txtLogo.Value = "Savvy Solar";
        //    }

        //    var Filename = "";
        //    string fn = "";
        //    if (JobAcknoDetails.DocType == "Export Control")
        //    {
        //        txtHeader.Value = "Export Control Acknowledgement";
        //        txtDetails.Value = "Yes, I accept the connection approval with imposed export control limit of " + JobAcknoDetails.Kw + "kw. I understand that this may impact ROI/Expected Payback period.";
        //        txtDetails1.Value = "No, I don’t agree with imposed export control limit. I wish to cancel the project and get refund of my deposit (if paid). Please contact your sales rep for refund process.";
        //        Filename = DateTime.Now.Ticks + "_ExportControl";
        //        fn = "_ExportControl";
        //    }
        //    else
        //    {
        //        txtHeader.Value = "Feed In Tariff Form";
        //        txtDetails.Value = "Yes, I understand that the upgrade/ panel add-on/ system replacement may significantly reduce the current Feed-in-Tariff I am receiving from the retailer. I have discussed the new Feed-in-Tariff with my current/preferred retailer and new rates have been agreed mutually. I understand that this may impact ROI/Expected Payback period.";
        //        txtDetails1.Value = "No, I don’t want to proceed further and request to refund the deposit. Please contact your sales rep for refund process.";
        //        Filename = DateTime.Now.Ticks + "_FeedInTariff";
        //        fn = "_FeedInTariff";
        //    }

        //    txtName.Value = JobAcknoDetails.Name;
        //    txtAddress.Value = JobAcknoDetails.Address.Trim();
        //    txtMobile.Value = JobAcknoDetails.Mobile;
        //    txtEmail.Value = JobAcknoDetails.Email;
        //    txtDocNo.Value = JobAcknoDetails.Id.ToString();

        //    if (JobAcknoDetails.IsSigned == true)
        //    {
        //        picSign.Value = JobAcknoDetails.SignFilePath;
        //        txtSignDate.Value = JobAcknoDetails.SignDate.ToString();

        //        if (JobAcknoDetails.YN == true)
        //        {
        //            chkYes.Value = "True";
        //            chkNo.Value = "False";
        //        }
        //        else
        //        {
        //            chkYes.Value = "False";
        //            chkNo.Value = "True";
        //        }
        //    }
        //    else
        //    {
        //        chkYes.Value = "False";
        //        chkNo.Value = "False";
        //    }

        //    var MainFolder = pathfordoc;
        //    MainFolder = MainFolder + "\\Documents";
        //    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == JobDetail.TenantId).Select(e => e.TenancyName).FirstOrDefault();
        //    var JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobAcknoDetails.JobId).Select(e => e.JobNumber).FirstOrDefault();

        //    string TenantPath = MainFolder + "\\" + TenantName;
        //    string JobPath = TenantPath + "\\" + JobNumber;
        //    string SignaturePath = JobPath + "\\Reports";
        //    if (JobPath.Contains(JobNumber))
        //    {
        //        FileInfo[] files = new DirectoryInfo(SignaturePath).GetFiles("*.pdf");
        //        foreach (var item in files)
        //        {
        //            if (item.FullName.Contains(fn + ".pdf"))
        //            {
        //                System.IO.File.Delete(Convert.ToString(item));
        //            }
        //        }
        //    }

        //    var jobId = Convert.ToInt32(JobAcknoDetails.JobId);
        //    SaveTelerikPDF(report_parent, jobId, JobAcknoDetails.JobFk.TenantId, Filename);
        //    return Filename;
        //}

        public async Task<TexInvoiceDto> TaxInvoiceByJobId(int JobId)
        {
            var output = new TexInvoiceDto();

            var JobDetail = await _jobRepository.GetAll()
                .Include(e => e.LeadFk)
                .Include(e => e.HouseTypeFk)
                .Include(e => e.RoofTypeFk)
                .Include(e => e.RoofAngleFk)
                .Include(e => e.ElecDistributorFk)
                .Include(e => e.ElecRetailerFk)
                .Where(e => e.Id == JobId).FirstOrDefaultAsync();

            var InvoiceImportData = await _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobId && e.InvoicePaymentmothodName != "SVR").ToListAsync();

            var quoteTemplate = GetTemplate(JobDetail.LeadFk.OrganizationId, 3); // 3 Template Id Tax Invoice 
            if (!string.IsNullOrEmpty(quoteTemplate.ViewHtml))
            {
                output.ViewHtml = quoteTemplate.ViewHtml;
            }

            output.JobNumber = JobDetail.JobNumber;
            output.Date = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId)).Value.Date.ToString("MMMM dd, yyyy");

            //Customer Details
            output.Name = JobDetail.LeadFk.CompanyName;
            output.Mobile = JobDetail.LeadFk.Mobile;
            output.Email = JobDetail.LeadFk.Email;

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            if (JobDetail.Address != null)
            {
                string address1 = JobDetail.Address.Trim().ToLower();

                output.AddressLine1 = textInfo.ToTitleCase(address1);
            }
            output.AddressLine2 = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
            output.AddressLine2 = textInfo.ToTitleCase(output.AddressLine2.ToLower());


            output.MeterPhase = Convert.ToString(JobDetail.MeterPhaseId);
            if (JobDetail.MeterUpgradeId == 1)
            {
                output.MeterUpgrad = "Yes";
            }
            else
            {
                output.MeterUpgrad = "No";
            }

            output.RoofType = JobDetail.RoofTypeFk != null ? JobDetail.RoofTypeFk.Name : "";
            output.PropertyType = JobDetail.HouseTypeFk != null ? JobDetail.HouseTypeFk.Name : "";
            output.RoofPitch = JobDetail.RoofAngleFk != null ? JobDetail.RoofAngleFk.Name : "";
            output.ElecDist = JobDetail.ElecDistributorFk != null ? JobDetail.ElecDistributorFk.Name : "";
            output.ElecRetailer = JobDetail.ElecRetailerFk != null ? JobDetail.ElecRetailerFk.Name : "";
            output.NMINumber = JobDetail.NMINumber;
            output.MeterNo = JobDetail.PeakMeterNo;

            output.qunityAndModelLists = (from o in _jobProductItemRepository.GetAll()
                                          join o1 in _ProductItemRepository.GetAll() on o.ProductItemId equals o1.Id into j1
                                          from s1 in j1.DefaultIfEmpty()
                                          where (o.JobId == JobDetail.Id)
                                          select new Quotations.Dtos.QunityAndModelList
                                          {
                                              Name = o.Quantity + " x " + s1.Name
                                          }).ToList();


            var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
            var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
            decimal _discount = 0;
            int j = 0;
            foreach (var item in Discount)
            {
                if (j == 0)
                {
                    _discount = Convert.ToDecimal(item);
                }
                else
                {
                    _discount = _discount + Convert.ToDecimal(item);
                }
                j++;
            }

            var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
            var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
            decimal _addition = 0;
            int i = 0;
            foreach (var item in Addition)
            {
                if (i == 0)
                {
                    _addition = Convert.ToDecimal(item);
                }
                else
                {
                    _addition = _addition + Convert.ToDecimal(item);
                }
                i++;
            }

            var totalAmount = !string.IsNullOrEmpty(JobDetail.TotalCost.ToString()) ? Convert.ToDecimal(JobDetail.TotalCost) : 0;
            var rebate = JobDetail.Rebate == null ? 0 : Convert.ToDecimal(JobDetail.Rebate);
            var paidAmount = InvoiceImportData.Select(e => e.PaidAmmount).Sum();

            output.TotalCost = ((JobDetail.TotalCost + rebate + _discount) - _addition).ToString();
            output.Stc = rebate.ToString("F");

            decimal _netCost = (totalAmount - (!string.IsNullOrEmpty(JobDetail.SolarVICRebate.ToString()) ? Convert.ToDecimal(JobDetail.SolarVICRebate) : 0)
                - (!string.IsNullOrEmpty(JobDetail.SolarVICLoanDiscont.ToString()) ? Convert.ToDecimal(JobDetail.SolarVICLoanDiscont) : 0));

            output.NetCost = _netCost.ToString("F");
            output.Deposit = paidAmount.ToString();

            output.BalanceDue = (_netCost - (paidAmount == null ? 0 : paidAmount)).ToString();

            output.SolarRebate = JobDetail.SolarVICRebate.ToString();
            output.SolarLoan = JobDetail.SolarVICLoanDiscont.ToString();

            output.ACharge = _addition.ToString("F");
            output.Discount = _discount.ToString("F");

            output.TCost = (JobDetail.TotalCost + rebate + _discount).ToString();

            // For Invoice Payment
            var InvoicePayment = await _invoicePaymentRepository.GetAll().Where(e => e.JobId == JobId).ToListAsync();

            if (InvoicePayment != null)
            {
                var pa = InvoicePayment.Select(e => e.InvoicePayTotal).Sum();
                output.InvoicePaymentPaid = pa.ToString();
                output.InvoicePaymentBalanceDue = (_netCost - (pa == null ? 0 : pa)).ToString();
            }

            return output;
        }

        public async Task<PaymentReceiptDto> PaymentReceiptByJobId(int JobId)
        {
            var output = new PaymentReceiptDto();

            var JobDetail = await _jobRepository.GetAll()
                .Include(e => e.LeadFk)
                .Include(e => e.HouseTypeFk)
                .Include(e => e.RoofTypeFk)
                .Include(e => e.RoofAngleFk)
                .Include(e => e.ElecDistributorFk)
                .Include(e => e.ElecRetailerFk)
                .Where(e => e.Id == JobId).FirstOrDefaultAsync();

            var InvoiceImportData = await _InvoiceImportDataRepository.GetAll().Where(e => e.JobId == JobId && e.InvoicePaymentmothodName != "SVR").ToListAsync();

            var quoteTemplate = GetTemplate(JobDetail.LeadFk.OrganizationId, 4); // 4 Template Id Payment Receipt 
            if (!string.IsNullOrEmpty(quoteTemplate.ViewHtml))
            {
                output.ViewHtml = quoteTemplate.ViewHtml;
            }

            output.JobNumber = JobDetail.JobNumber;
            output.Date = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId)).Value.Date.ToString("MMMM dd, yyyy");
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            //Customer Details
            output.Name = textInfo.ToTitleCase(JobDetail.LeadFk.CompanyName.ToLower());
            output.Mobile = JobDetail.LeadFk.Mobile;
            output.Email = JobDetail.LeadFk.Email;

            if (JobDetail.LeadFk.Address != null)
            {
                string address1 = JobDetail.LeadFk.Address.Trim().ToLower();

                output.AddressLine1 = textInfo.ToTitleCase(address1);
            }
            output.AddressLine2 = JobDetail.LeadFk.Suburb + ", " + JobDetail.LeadFk.State + " - " + JobDetail.LeadFk.PostCode;
            output.AddressLine2 = textInfo.ToTitleCase(output.AddressLine2.ToLower());

            if (JobDetail.Address != null)
            {
                string address1 = JobDetail.Address.Trim().ToLower();
                output.SiteAddressLine1 = textInfo.ToTitleCase(address1);
            }
            output.SiteAddressLine2 = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
            output.SiteAddressLine2 = textInfo.ToTitleCase(output.SiteAddressLine2.ToLower());

            var MinusList = _variationRepository.GetAll().Where(e => e.Action == "Minus").Select(e => e.Id).ToList();
            var Discount = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && MinusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
            decimal _discount = 0;
            int j = 0;
            foreach (var item in Discount)
            {
                if (j == 0)
                {
                    _discount = Convert.ToDecimal(item);
                }
                else
                {
                    _discount = _discount + Convert.ToDecimal(item);
                }
                j++;
            }

            var PlusList = _variationRepository.GetAll().Where(e => e.Action == "Plus").Select(e => e.Id).ToList();
            var Addition = _jobVariationRepository.GetAll().Where(e => e.JobId == JobDetail.Id && PlusList.Contains((int)e.VariationId)).Select(e => e.Cost).ToList();
            decimal _addition = 0;
            int i = 0;
            foreach (var item in Addition)
            {
                if (i == 0)
                {
                    _addition = Convert.ToDecimal(item);
                }
                else
                {
                    _addition = _addition + Convert.ToDecimal(item);
                }
                i++;
            }

            var totalAmount = !string.IsNullOrEmpty(JobDetail.TotalCost.ToString()) ? Convert.ToDecimal(JobDetail.TotalCost) : 0;
            var rebate = JobDetail.Rebate == null ? 0 : Convert.ToDecimal(JobDetail.Rebate);
            var paidAmount = InvoiceImportData.Select(e => e.PaidAmmount).Sum();

            output.TotalCost = ((JobDetail.TotalCost + rebate + _discount) - _addition).ToString();
            output.LessStcRebate = rebate.ToString("F");

            decimal _netCost = (totalAmount - (!string.IsNullOrEmpty(JobDetail.SolarVICRebate.ToString()) ? Convert.ToDecimal(JobDetail.SolarVICRebate) : 0)
                - (!string.IsNullOrEmpty(JobDetail.SolarVICLoanDiscont.ToString()) ? Convert.ToDecimal(JobDetail.SolarVICLoanDiscont) : 0));

            output.NetCost = _netCost.ToString("F");
            output.BalanceDue = (_netCost - (paidAmount == null ? 0 : paidAmount)).ToString();
            output.SolarVICRebate = JobDetail.SolarVICRebate.ToString();
            output.SolarVICLoan = JobDetail.SolarVICLoanDiscont.ToString();
            output.ACharge = _addition.ToString("F");
            output.Discount = _discount.ToString("F");

            output.PaymentDetails = (from o in _InvoiceImportDataRepository.GetAll().Include(e => e.InvoicePaymentMethodFk)
                                         //join o1 in _invoicePaymentMethodRepository.GetAll() on o.InvoicePaymentMethodId equals o1.Id into j1
                                         //from s1 in j1.DefaultIfEmpty()
                                     where (o.JobId == JobDetail.Id)
                                     select new PaymentDetails
                                     {
                                         Date = o.Date != null ? Convert.ToDateTime(o.Date).ToString("MMMM dd, yyyy") : "",
                                         Amount = o.PaidAmmount != null ? o.PaidAmmount.ToString() : "0.00",
                                         SSCharge = o.SSCharge != null ? o.SSCharge.ToString() : "0.00",
                                         Method = o.InvoicePaymentMethodFk.PaymentMethod
                                     }).ToList();

            var InvoicePayment = await _invoicePaymentRepository.GetAll().Include(e => e.InvoicePaymentMethodFk).Where(e => e.JobId == JobId).ToListAsync();
            if (InvoicePayment != null)
            {
                var pa = InvoicePayment.Select(e => e.InvoicePayTotal).Sum();
                output.InvoicePaymentPaid = pa.ToString();
                output.InvoicePaymentBalanceDue = (_netCost - (pa == null ? 0 : pa)).ToString();

                output.InvoicePaymentPaymentDetails = (from o in InvoicePayment
                                                       select new PaymentDetails
                                                       {
                                                           Date = o.InvoicePayDate != null ? Convert.ToDateTime(o.InvoicePayDate).ToString("MMMM dd, yyyy") : "",
                                                           Amount = o.InvoicePayTotal != null ? o.InvoicePayTotal.ToString() : "0.00",
                                                           SSCharge = o.CCSurcharge != null ? o.CCSurcharge.ToString() : "0.00",
                                                           Method = o.InvoicePaymentMethodFk.PaymentMethod
                                                       }).ToList();
            }

            return output;
        }

        public async Task<GetPicklistDataDto> PicklistByPickListId(int JobId, int PicklistId)
        {
            var output = new GetPicklistDataDto();

            var JobDetail = _jobRepository.GetAll()
                .Include(e => e.LeadFk)
                .Include(e => e.HouseTypeFk)
                .Include(e => e.RoofTypeFk)
                .Include(e => e.RoofAngleFk)
                .Include(e => e.ElecDistributorFk)
                .Include(e => e.ElecRetailerFk)
                .Where(e => e.Id == JobId).FirstOrDefault();

            var PickList = await _pickListAppService.GetPickListById(PicklistId);

            var quoteTemplate = GetTemplate(JobDetail.LeadFk.OrganizationId, 5); // 5 Template Id PickList
            if (!string.IsNullOrEmpty(quoteTemplate.ViewHtml))
            {
                output.ViewHtml = quoteTemplate.ViewHtml;
            }

            output.JobNumber = JobDetail.JobNumber;
            output.Date = (_timeZoneConverter.Convert(PickList.CreationTime, (int)AbpSession.TenantId)).Value.Date.ToString("MMMM dd, yyyy");

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            //Customer Details
            output.Name = textInfo.ToTitleCase(JobDetail.LeadFk.CompanyName.ToLower());
            output.Mobile = JobDetail.LeadFk.Mobile;
            output.Email = JobDetail.LeadFk.Email;
            if (JobDetail.LeadFk.Address != null)
            {
                string address1 = JobDetail.LeadFk.Address.Trim().ToLower();

                output.AddressLine1 = textInfo.ToTitleCase(address1);
            }
            output.AddressLine2 = JobDetail.LeadFk.Suburb + ", " + JobDetail.LeadFk.State + " - " + JobDetail.LeadFk.PostCode;
            output.AddressLine2 = textInfo.ToTitleCase(output.AddressLine2.ToLower());

            output.RoofType = JobDetail.RoofTypeFk != null ? JobDetail.RoofTypeFk.Name : "";
            output.PropertyType = JobDetail.HouseTypeFk != null ? JobDetail.HouseTypeFk.Name : "";
            output.RoofPitch = JobDetail.RoofAngleFk != null ? JobDetail.RoofAngleFk.Name : "";
            output.ElecDist = JobDetail.ElecDistributorFk != null ? JobDetail.ElecDistributorFk.Name : "";
            output.ElecRetailer = JobDetail.ElecRetailerFk != null ? JobDetail.ElecRetailerFk.Name : "";
            output.NMINumber = JobDetail.NMINumber;
            output.MeterNo = JobDetail.PeakMeterNo;
            output.ManualQuoteNo = JobDetail.ManualQuote;
            output.MeterPhase = Convert.ToString(JobDetail.MeterPhaseId);
            if (JobDetail.MeterUpgradeId == 1)
            {
                output.MeterUpgrad = "Yes";
            }
            else
            {
                output.MeterUpgrad = "No";
            }

            output.StoreLoc = !string.IsNullOrEmpty(PickList.LocationId.ToString()) ? _wareHouseLocationRepository.GetAll().Where(e => e.Id == PickList.LocationId).Select(e => e.location).FirstOrDefault() : "";

            //Installation Details
            output.InstallerName = PickList.InstallerName;
            output.InstallerMobile = _userRepository.GetAll().Where(e => e.CompanyName == PickList.InstallerName).Select(e => e.Mobile).FirstOrDefault();
            output.InstallationDate = PickList.InstallBookedDate.ToString("MMMM dd, yyyy");

            if (JobDetail.Address != null)
            {
                string address1 = JobDetail.Address.Trim().ToLower();
                output.SiteAddressLine1 = textInfo.ToTitleCase(address1);
            }
            output.SiteAddressLine2 = JobDetail.Suburb + ", " + JobDetail.State + " - " + JobDetail.PostalCode;
            output.SiteAddressLine2 = textInfo.ToTitleCase(output.SiteAddressLine2.ToLower());

            // Special Notes 
            output.Notes = JobDetail.InstallationNotes;
            output.InstallerNotes = JobDetail.InstallerNotes;

            var PicklistItem = _pickListAppService.GetPickListItemDetailsByPickId(PicklistId);

            var ProductItem = _ProductItemRepository.GetAll().Include(e => e.ProductTypeFk).Select(e => new { e.Id, e.ProductTypeFk, e.Name, e.Model });

            output.PicklistItemDetails = (from o in PicklistItem
                                          select new PicklistItemDetails
                                          {
                                              Category = ProductItem.Where(e => e.Id == o.StockItemId).Select(e => e.ProductTypeFk.Name).FirstOrDefault(),
                                              Item = ProductItem.Where(e => e.Id == o.StockItemId).Select(e => e.Name).FirstOrDefault(),
                                              Model = ProductItem.Where(e => e.Id == o.StockItemId).Select(e => e.Model).FirstOrDefault(),
                                              Qty = o.OrderQuantity.ToString()

                                          }).ToList();

            return output;
        }

        public async Task<GetAcknoledgeementDto> GetAcknowledgementById(int Id)
        {
            var output = new GetAcknoledgeementDto();

            var JobAcknoDetails = _jobAcknowledgementRepqository.GetAll().Where(e => e.Id == Id).FirstOrDefault();
            var Jobs = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == JobAcknoDetails.JobId).FirstOrDefaultAsync();
            var org = _extendedOrganizationUnitRepository.GetAll();

            if (JobAcknoDetails != null)
            {
                var TemplateId = JobAcknoDetails.DocType == "Export Control" ? 6 : JobAcknoDetails.DocType == "Feed In Tariff" ? 7 : 0;

                var template = GetTemplate(Jobs.LeadFk.OrganizationId, TemplateId);
                if (!string.IsNullOrEmpty(template.ViewHtml))
                {
                    output.ViewHtml = template.ViewHtml;
                }

                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                output.DocType = JobAcknoDetails.DocType;
                output.Name = textInfo.ToTitleCase(JobAcknoDetails.Name.ToLower());
                if (Jobs.Address != null)
                {
                    output.Address1 = textInfo.ToTitleCase(Convert.ToString(Jobs.Address.ToLower()).Trim()).Trim();
                }
                output.Address2 = textInfo.ToTitleCase((Jobs.Suburb + ", " + Jobs.State + "-" + Jobs.PostalCode).ToLower()).Trim();

                output.Mobile = JobAcknoDetails.Mobile;
                output.Email = JobAcknoDetails.Email;
                output.JobNumber = JobAcknoDetails.JobFk.JobNumber;
                output.Kw = JobAcknoDetails.Kw.ToString();

                if (JobAcknoDetails.IsSigned == true)
                {
                    output.YN = JobAcknoDetails.YN;
                    output.SignSrc = JobAcknoDetails.SignFilePath.Replace(@"s:\documents.thesolarproduct.com", "");
                    output.SignDate = JobAcknoDetails.LastModificationTime == null ? "" : Convert.ToDateTime(_timeZoneConverter.Convert(JobAcknoDetails.LastModificationTime, (int)AbpSession.TenantId)).ToString("MMM dd, yyyy hh:mm tt");
                }

                output.OrgLogo = org.Where(e => e.Id == Jobs.LeadFk.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();

                if (!string.IsNullOrEmpty(output.OrgLogo))
                {
                    output.OrgLogo = output.OrgLogo.Replace("\\", "/");
                }
            }

            return output;
        }

        public async Task<GetDeclarationFormDto> GetDeclarationById(int Id)
        {
            var output = new GetDeclarationFormDto();

            var declarationForm = _declarationFormRepository.GetAll().Where(e => e.Id == Id).FirstOrDefault();
            var Jobs = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == declarationForm.JobId).FirstOrDefaultAsync();
            var org = _extendedOrganizationUnitRepository.GetAll();

            if (declarationForm != null)
            {
                var TemplateId = declarationForm.DocType == "Shading Declaration" ? 11 : declarationForm.DocType == "Efficiency Declaration" ? 12 : 0;

                var template = GetTemplate(Jobs.LeadFk.OrganizationId, TemplateId);
                if (!string.IsNullOrEmpty(template.ViewHtml))
                {
                    output.ViewHtml = template.ViewHtml;
                }

                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                output.DocType = declarationForm.DocType;
                output.Name = textInfo.ToTitleCase(declarationForm.JobFk.LeadFk.CompanyName.ToLower());
                if (Jobs.Address != null)
                {
                    output.Address1 = textInfo.ToTitleCase(Convert.ToString(Jobs.Address.ToLower()).Trim()).Trim();
                }
                output.Address2 = textInfo.ToTitleCase((Jobs.Suburb + ", " + Jobs.State + "-" + Jobs.PostalCode).ToLower()).Trim();

                output.Mobile = declarationForm.JobFk.LeadFk.Mobile;
                output.Email = declarationForm.JobFk.LeadFk.Email;
                output.JobNumber = declarationForm.JobFk.JobNumber;
                output.Kw = declarationForm.Kw.ToString();
                output.State = declarationForm.JobFk.State;
                if (declarationForm.IsSigned == true)
                {
                    //output.YN = declarationForm.YN;
                    output.SignSrc = declarationForm.SignFilePath.Replace(@"s:\documents.thesolarproduct.com", "");
                    output.SignDate = declarationForm.LastModificationTime == null ? "" : Convert.ToDateTime(_timeZoneConverter.Convert(declarationForm.LastModificationTime, (int)AbpSession.TenantId)).ToString("MMM dd, yyyy hh:mm tt");
                }

                output.OrgLogo = org.Where(e => e.Id == Jobs.LeadFk.OrganizationId).Select(e => (e.LogoFilePath + e.LogoFileName)).FirstOrDefault();

                if (!string.IsNullOrEmpty(output.OrgLogo))
                {
                    output.OrgLogo = output.OrgLogo.Replace("\\", "/");
                }
            }

            return output;
        }

        public async Task<RefundFormDto> GetRefundByJobId(int Id)
        {
            var output = new RefundFormDto();

            var Refund = await _jobRefundRepository.GetAll().Include(e => e.RefundReasonFk).Where(e => e.Id == Id).FirstOrDefaultAsync();

            var JobDetail = await _jobRepository.GetAll()
                .Include(e => e.LeadFk)
                .Include(e => e.HouseTypeFk)
                .Include(e => e.RoofTypeFk)
                .Include(e => e.RoofAngleFk)
                .Include(e => e.ElecDistributorFk)
                .Include(e => e.ElecRetailerFk)
                .Include(e => e.JobStatusFk)
                .Where(e => e.Id == Refund.JobId).FirstOrDefaultAsync();

            var _templateId = Refund.RefundReasonFk.Name == "Referral" ? 9 : 8; // 9 Template Id Referral 8 - Refund 

            var refundTemplate = GetTemplate(JobDetail.LeadFk.OrganizationId, _templateId);
            if (!string.IsNullOrEmpty(refundTemplate.ViewHtml))
            {
                output.ViewHtml = refundTemplate.ViewHtml;
            }

            output.JobNumber = JobDetail.JobNumber;
            output.JobStatus = JobDetail.JobStatusFk.Name;
            output.Date = (_timeZoneConverter.Convert(Refund.CreationTime, (int)AbpSession.TenantId)).Value.Date.ToString("MMMM dd, yyyy");
            output.DepositDate = JobDetail.DepositeRecceivedDate != null ? Convert.ToDateTime(JobDetail.DepositeRecceivedDate).ToString("MMMM dd, yyyy") : "";

            //Customer Details
            output.Name = JobDetail.LeadFk.CompanyName;
            output.Mobile = JobDetail.LeadFk.Mobile;
            output.Email = JobDetail.LeadFk.Email;

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            if (JobDetail.LeadFk.Address != null)
            {
                string address1 = JobDetail.LeadFk.Address.Trim().ToLower();
                output.AddressLine1 = textInfo.ToTitleCase(address1);
            }
            output.AddressLine2 = JobDetail.LeadFk.Suburb + ", " + JobDetail.LeadFk.State + " - " + JobDetail.LeadFk.PostCode;
            output.AddressLine2 = textInfo.ToTitleCase(output.AddressLine2.ToLower());

            output.MeterPhase = Convert.ToString(JobDetail.MeterPhaseId);
            if (JobDetail.MeterUpgradeId == 1)
            {
                output.MeterUpgrad = "Yes";
            }
            else
            {
                output.MeterUpgrad = "No";
            }

            output.RoofType = JobDetail.RoofTypeFk != null ? JobDetail.RoofTypeFk.Name : "";
            output.PropertyType = JobDetail.HouseTypeFk != null ? JobDetail.HouseTypeFk.Name : "";
            output.RoofPitch = JobDetail.RoofAngleFk != null ? JobDetail.RoofAngleFk.Name : "";
            output.ElecDist = JobDetail.ElecDistributorFk != null ? JobDetail.ElecDistributorFk.Name : "";
            output.ElecRetailer = JobDetail.ElecRetailerFk != null ? JobDetail.ElecRetailerFk.Name : "";

            var User_List = _userRepository.GetAll().ToList();
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == JobDetail.LeadFk.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()

                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()

                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()

                            where us != null && ut != null && ut.TeamId == TeamId
                            select user).Distinct().FirstOrDefault();

            var CurrentUser = _userRepository.GetAll().Where(e => e.Id == JobDetail.LeadFk.AssignToUserID).FirstOrDefault();
            output.SalesRepName = CurrentUser.FullName;
            output.Manager = UserList != null ? UserList.FullName : "";

            var NoOfPanels = _jobProductItemRepository.GetAll().Where(e => (int)e.JobId == JobDetail.Id && e.ProductItemFk.ProductTypeId == 1).Sum(e => e.Quantity);
            output.NoOfPanels = NoOfPanels.ToString();

            output.RefundReason = Refund.Notes;
            output.CusBankName = Refund.BankName;
            output.CusAccName = Refund.AccountName;
            output.CusAccNumber = Refund.AccountNo;
            output.RefundAmount = Refund.Amount != null ? (decimal)Refund.Amount : 0;
            output.CusBSB = Refund.BSBNo;
            output.RefundDate = Refund.PaidDate != null ? Convert.ToDateTime(Refund.PaidDate).ToString("MMMM dd, yyyy") : "";

            return output;
        }

        public async Task<ReferralFormDto> GetReffarelByJobId(int Id)
        {
            var output = new ReferralFormDto();

            var JobDetail = await _jobRepository.GetAll().Include(e => e.LeadFk).Include(e => e.JobStatusFk).Where(e => e.Id == Id).FirstOrDefaultAsync();

            //var JobDetail = await _jobRepository.GetAll()
            //    .Include(e => e.LeadFk)
            //    .Include(e => e.HouseTypeFk)
            //    .Include(e => e.RoofTypeFk)
            //    .Include(e => e.RoofAngleFk)
            //    .Include(e => e.ElecDistributorFk)
            //    .Include(e => e.ElecRetailerFk)
            //    .Include(e => e.JobStatusFk)
            //    .Where(e => e.Id == Refund.JobId).FirstOrDefaultAsync();

            //var _templateId = Refund.RefundReasonFk.Name == "Referral" ? 9 : 8; // 9 Template Id Referral 8 - Refund 

            var refundTemplate = GetTemplate(JobDetail.LeadFk.OrganizationId, 9);
            if (!string.IsNullOrEmpty(refundTemplate.ViewHtml))
            {
                output.ViewHtml = refundTemplate.ViewHtml;
            }

            output.JobNumber = JobDetail.JobNumber;
            output.JobStatus = JobDetail.JobStatusFk.Name;
            output.EntryDate = (_timeZoneConverter.Convert(JobDetail.CreationTime, (int)AbpSession.TenantId)).Value.Date.ToString("MMMM dd, yyyy");
            output.DepositDate = JobDetail.DepositeRecceivedDate != null ? Convert.ToDateTime(JobDetail.DepositeRecceivedDate).ToString("MMMM dd, yyyy") : "";

            //Customer Details
            output.Name = JobDetail.LeadFk.CompanyName;
            output.Mobile = JobDetail.LeadFk.Mobile;
            output.Email = JobDetail.LeadFk.Email;

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            if (JobDetail.LeadFk.Address != null)
            {
                string address1 = JobDetail.LeadFk.Address.Trim().ToLower();
                output.AddressLine1 = textInfo.ToTitleCase(address1);
            }
            output.AddressLine2 = JobDetail.LeadFk.Suburb + ", " + JobDetail.LeadFk.State + " - " + JobDetail.LeadFk.PostCode;
            output.AddressLine2 = textInfo.ToTitleCase(output.AddressLine2.ToLower());

            //output.MeterPhase = Convert.ToString(JobDetail.MeterPhaseId);
            //if (JobDetail.MeterUpgradeId == 1)
            //{
            //    output.MeterUpgrad = "Yes";
            //}
            //else
            //{
            //    output.MeterUpgrad = "No";
            //}

            //output.RoofType = JobDetail.RoofTypeFk != null ? JobDetail.RoofTypeFk.Name : "";
            //output.PropertyType = JobDetail.HouseTypeFk != null ? JobDetail.HouseTypeFk.Name : "";
            //output.RoofPitch = JobDetail.RoofAngleFk != null ? JobDetail.RoofAngleFk.Name : "";
            //output.ElecDist = JobDetail.ElecDistributorFk != null ? JobDetail.ElecDistributorFk.Name : "";
            //output.ElecRetailer = JobDetail.ElecRetailerFk != null ? JobDetail.ElecRetailerFk.Name : "";

            var User_List = _userRepository.GetAll().ToList();
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == JobDetail.LeadFk.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()

                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()

                            join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                            from ut in utJoined.DefaultIfEmpty()

                            where us != null && ut != null && ut.TeamId == TeamId
                            select user).Distinct().FirstOrDefault();

            var CurrentUser = _userRepository.GetAll().Where(e => e.Id == JobDetail.LeadFk.AssignToUserID).FirstOrDefault();
            output.SalesRepName = CurrentUser.FullName;
            output.Manager = UserList != null ? UserList.FullName : "";

            var NoOfPanels = _jobProductItemRepository.GetAll().Where(e => (int)e.JobId == JobDetail.Id && e.ProductItemFk.ProductTypeId == 1).Sum(e => e.Quantity);
            output.NoOfPanels = NoOfPanels.ToString();

            //output.RefundReason = Refund.Notes;
            //output.CusBankName = Refund.BankName;
            output.CusAccName = JobDetail.AccountName;
            output.CusAccNumber = JobDetail.AccountNo;
            output.RefAmount = JobDetail.ReferralAmount != null ? (decimal)JobDetail.ReferralAmount : 0;
            output.CusBSB = JobDetail.BsbNo;
            //output.RefundDate = Refund.PaidDate != null ? Convert.ToDateTime(Refund.PaidDate).ToString("MMMM dd, yyyy") : "";

            var Refferel = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadId == JobDetail.LeadFk.ReferralLeadId).Select(e => new { e.LeadFk.CompanyName, e.JobNumber, e.LeadFk.Address }).FirstOrDefault();

            output.RefName = Refferel.CompanyName;
            output.RefProj = Refferel.JobNumber;
            if (Refferel.Address != null)
            {
                string address1 = Refferel.Address.Trim().ToLower();
                output.RefAddress = textInfo.ToTitleCase(address1);
            }


            return output;
        }

        public async Task<PagedResultDto<ReviewReportDto>> GetAllReviewList(ReviewReportInputDto input)
        {
            var SDate = _timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            var EDate = _timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);
            var users = _userRepository.GetAll()
              .AsNoTracking()
              .Select(u => new { u.Id, u.UserName })
              .ToList();
            var reviews = _JobReviewRepository.GetAll()
       .Include(r => r.JobFk)
      
       .WhereIf(input.ReviewType > 0, e => e.ReviewTypeId == input.ReviewType)
       .WhereIf(input.Datefilter == "OrderDate" && input.StartDate != null, e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date)
       .WhereIf(input.Datefilter == "OrderDate" && input.EndDate != null, e => e.CreationTime.AddHours(10).Date <= EDate.Value.Date)
       .ToList();



            var reviewReport = reviews.GroupBy(r => r.CreatorUserId).Select(group =>
            {
                var username = users.FirstOrDefault(u => u.Id == group.Key)?.UserName;

                if (string.IsNullOrEmpty(username)) return null;

                return new ReviewReportDto
                {
                    User = username,
                    ACTcount = group.Count(r => r.JobFk.State == "ACT").ToString(),
                    NSWcount = group.Count(r => r.JobFk.State == "NSW").ToString(),
                    NTcount = group.Count(r => r.JobFk.State == "NT").ToString(),
                    QLDcount = group.Count(r => r.JobFk.State == "QLD").ToString(),
                    SAcount = group.Count(r => r.JobFk.State == "SA").ToString(),
                    TAScount = group.Count(r => r.JobFk.State == "TAS").ToString(),
                    VICcount = group.Count(r => r.JobFk.State == "VIC").ToString(),
                    WAcount = group.Count(r => r.JobFk.State == "WA").ToString()
                };
            })
           .Where(report => report != null)
           .ToList();

            if (input.FilterName == "UserName" && !string.IsNullOrWhiteSpace(input.Filter))
            {
                reviewReport = reviewReport
                    .Where(report => report.User != null && report.User.Contains(input.Filter))
                    .ToList();
            }
                var totalCount = reviewReport.Count;



            return new PagedResultDto<ReviewReportDto>(
    totalCount,
    reviewReport
);

        }


    }
}