﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Data;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Timing.Timezone;
using NPOI.OpenXmlFormats.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.CallHistory;
using TheSolarProduct.Dto;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.Leads;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;
using TheSolarProduct.Reports.LeadAssigns.Dtos;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Timing;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;
using NPOI.SS.Formula.Functions;
using Abp.MultiTenancy;
using TheSolarProduct.Reports.JobCost.Dtos;
using Abp.Threading.Extensions;
using Abp.Authorization;

namespace TheSolarProduct.Reports.LeadAssigns
{
    [AbpAuthorize]
    public class LeadAssignReportAppService : TheSolarProductAppServiceBase, ILeadAssignReportAppService
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<LeadSource, int> _leadSourceRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;

        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IActiveTransactionProvider _transactionProvider;

        public LeadAssignReportAppService(
           IRepository<User, long> userRepository
            , IRepository<Role> roleRepository
            , IRepository<LeadSource, int> leadSourceRepository
            , IRepository<Lead> leadRepository
            , ITimeZoneConverter timeZoneConverter
            , ITimeZoneService timeZoneService
            , IRepository<UserRole, long> userRoleRepository
            , IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository
            , IRepository<UserTeam> userTeamRepository
            , ILeadsExcelExporter leadsExcelExporter
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , IActiveTransactionProvider transactionProvider
            )
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _leadSourceRepository = leadSourceRepository;
            _leadRepository = leadRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _userroleRepository = userRoleRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userTeamRepository = userTeamRepository;
            _leadsExcelExporter = leadsExcelExporter;
            _dbcontextprovider = dbcontextprovider;
            _transactionProvider = transactionProvider;
        }

        public async Task<PagedResultDto<GetAllLeadAssignDto>> GetAll(GetAllLeadAssignInputDto input)
        {
            //List<int> Google_list = new List<int> {1,19,23,25 };
            //List<int> Facebook_list = new List<int> {4,17,18,24,26};
            //List<int> Others_list = new List<int> { 5,6,7,8,9,10,11,12,13,14,15,16,20,21,22,27,28,29 };

            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var salesRape = _roleRepository.GetAll().ToList().Where(e => e.DisplayName == "Sales Rep").Select(e => e.Id).FirstOrDefault();

            //var users = _userRepository.GetAll()
            //    .Where(e => e.Roles.Any(o => o.RoleId == salesRape))
            //    .Where(e => e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnitId))
            //              .WhereIf(input.UserId != 0, e => e.Id == input.UserId);

            //var filtered = _leadRepository.GetAll().Where(e=> e.OrganizationId == input.OrganizationUnitId)
            //  .WhereIf(input.StartDate != null, e => e.AssignDate >= input.StartDate)
            //                .WhereIf(input.EndDate != null, e => e.AssignDate <= input.EndDate);

            //var res = from o in users

            //          select new GetAllLeadAssignDto()
            //          {
            //              Id = (int)o.Id,
            //              UserName = o.FullName.ToString(),
            //              TV = filtered.Where(e => e.LeadSourceId == 3 && e.AssignToUserID == o.Id).Count(),
            //              Google = filtered.Where(e => Google_list.Contains((int)e.LeadSourceId) && e.AssignToUserID == o.Id).Count(),
            //              Facebook = filtered.Where(e => Facebook_list.Contains((int)e.LeadSourceId) && e.AssignToUserID == o.Id).Count(),
            //              Others = filtered.Where(e => Others_list.Contains((int)e.LeadSourceId) && e.AssignToUserID == o.Id).Count(),
            //              Refferal = filtered.Where(e => e.LeadSourceId == 2 && e.AssignToUserID == o.Id).Count()
            //          };

            //var data = res.ToList();

            //var summaryCount = new LeadAssignSummaryCount();
            //summaryCount.TotalTV = data.Sum(e => e.TV);
            //summaryCount.TotalGoogle = data.Sum(e => e.Google);
            //summaryCount.TotalFacebook = data.Sum(e => e.Facebook);
            //summaryCount.TotalOthers = data.Sum(e => e.Others);
            //summaryCount.TotalRefferal = data.Sum(e => e.Refferal);

            //var pagedAndFiltered = res.Where(e => e.TV != 0 || e.Google != 0 || e.Facebook != 0 || e.Others != 0 || e.Refferal != 0).AsQueryable()
            //               .OrderBy(input.Sorting ?? "id desc")
            //                .PageBy(input);

            //var callHistory = pagedAndFiltered.ToList();

            //if (callHistory.Count() > 0)
            //    callHistory[0].Summary = summaryCount;

            //var totalCount = res.Where(e => e.TV != 0 || e.Google != 0 || e.Facebook != 0 || e.Others != 0 || e.Refferal != 0).Count();

            //return new PagedResultDto<GetAllLeadAssignDto>(totalCount, callHistory);

            var itemDtos = GetLeadAssign(input);

            var summaryCount = new LeadAssignSummaryCount();
            summaryCount.TotalTV = itemDtos.Sum(e => e.TV);
            summaryCount.TotalGoogle = itemDtos.Sum(e => e.Google);
            summaryCount.TotalFacebook = itemDtos.Sum(e => e.Facebook);
            summaryCount.TotalRefferal = itemDtos.Sum(e => e.Refferal);
            summaryCount.TotalOthers = itemDtos.Sum(e => e.Others);

            var pagedAndFilteredJobs = itemDtos
                .OrderBy(input.Sorting ?? "UserName Asc")
                .PageBy(input);

            var output = from o in pagedAndFilteredJobs

                         select new GetAllLeadAssignDto()
                         {
                             UserName = o.UserName,
                             TV = o.TV,
                             Google = o.Google,
                             Facebook = o.Facebook,
                             Refferal = o.Refferal,
                             Others = o.Others,
                             Total=o.TV + o.Facebook+ o.Google+o.Refferal+o.Others,
                             Summary = summaryCount
                         };

            var totalCount = itemDtos.Count();

            return new PagedResultDto<GetAllLeadAssignDto>(totalCount, output.ToList());
        }

        public async Task<FileDto> GetLeadAssignToExcel(GetAllLeadAssignExportInputDto input)
        {
            GetAllLeadAssignInputDto input1 = new GetAllLeadAssignInputDto()
            {
                OrganizationUnitId = input.OrganizationUnitId,
                UserId = input.UserId,
                StartDate = input.StartDate,
                EndDate = input.EndDate
            };

            var itemDtos = GetLeadAssign(input1);

            if (input.excelorcsv == 1)
            {
                return _leadsExcelExporter.LeadAssignExport(itemDtos.ToList(), "LeadAssign.xlsx");
            }
            else
            {
                return _leadsExcelExporter.LeadAssignExport(itemDtos.ToList(), "LeadAssign.csv");
            }
        }

        private DbTransaction GetActiveTransaction()
        {
            return (DbTransaction)_transactionProvider.GetActiveTransaction(new ActiveTransactionProviderArgs
            {
                    {"ContextType", typeof(TheSolarProductDbContext) },
                    {"MultiTenancySide", MultiTenancySides.Tenant }
            });
        }

        protected virtual IQueryable<GetAllLeadAssignDto> GetLeadAssign(GetAllLeadAssignInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            #region Call Store Procedure
            var command = _dbcontextprovider.GetDbContext().Database.GetDbConnection().CreateCommand();
            command.CommandText = "GetLeadAssign";
            command.CommandType = CommandType.StoredProcedure;
            command.Transaction = GetActiveTransaction();
            command.CommandTimeout = 100000;

            var parameter = command.CreateParameter();
            parameter.ParameterName = "@OrganizationUnitId";
            parameter.DbType = DbType.Int32;
            parameter.Value = input.OrganizationUnitId;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@UserId";
            parameter.DbType = DbType.Int32;
            if (input.UserId != 0)
                parameter.Value = input.UserId;
            else
                parameter.Value = DBNull.Value;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DiffHour";
            parameter.Value = diffHour;
            parameter.DbType = DbType.Double;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartDate";
            if (SDate != null)
                parameter.Value = SDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndDate";
            if (EDate != null)
                parameter.Value = EDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DateType";
            parameter.Value = input.DateType;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            //command.Connection.Open();
            DbDataReader reader = command.ExecuteReader();
            //command.Connection.Open();
            var table = new DataTable();
            table.Load(reader);
            #endregion

            var result = from item in table.AsEnumerable()
                         select new GetAllLeadAssignDto()
                         {
                             UserName = item.Field<string>("Name"),
                             TV = item.Field<int>("TV"),
                             Google = item.Field<int>("Google"),
                             Facebook = item.Field<int>("Facebook"),
                             Refferal = item.Field<int>("Refferal"),
                             Others = item.Field<int>("Others"),
                             TVSold = item.Field<int>("TVSold"),
                             GoogleSold = item.Field<int>("GoogleSold"),
                             FacebookSold = item.Field<int>("FacebookSold"),
                             RefferalSold = item.Field<int>("RefferalSold"),
                             OthersSold = item.Field<int>("OthersSold"),
                             TVRatio = (double)item.Field<int>("TV") != 0 ? Math.Round(Convert.ToDouble(item.Field<int>("TVSold")) / Convert.ToDouble(item.Field<int>("TV")) * 100,2) : 0,
                             GoogleRatio = (double)item.Field<int>("Google") != 0 ? Math.Round((Convert.ToDouble(item.Field<int>("GoogleSold")) / Convert.ToDouble(item.Field<int>("Google"))) * 100,2) : 0,
                             FacebookRatio = (double)item.Field<int>("Facebook") != 0 ? Math.Round((Convert.ToDouble(item.Field<int>("FacebookSold")) / Convert.ToDouble(item.Field<int>("Facebook"))) * 100, 2) : 0,
                             RefferalRatio = (double)item.Field<int>("Refferal") != 0 ? Math.Round((Convert.ToDouble(item.Field<int>("RefferalSold")) / Convert.ToDouble(item.Field<int>("Refferal"))) * 100, 2) : 0,
                             OthersRatio = (double)item.Field<int>("Others") != 0 ? Math.Round((Convert.ToDouble(item.Field<int>("OthersSold")) / Convert.ToDouble(item.Field<int>("Others"))) * 100, 2) : 0
                         };

            return result.AsQueryable();
        }
    }
}
