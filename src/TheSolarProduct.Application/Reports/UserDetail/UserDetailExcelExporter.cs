﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.UserDetail.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Reports.UserDetail
{
    public class UserDetailExcelExporter : NpoiExcelExporterBase, IUserDetailExcelExporter
    {
        private readonly ITempFileCacheManager _tempFileCacheManager;
        public UserDetailExcelExporter(
            ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _tempFileCacheManager = tempFileCacheManager;
        }

        public FileDto UserDetailExport(List<GetAllUserDetailDto> detail, string fileName)
        {
            string[] Header = new string[] { "User Name", "Email", "Mobile", "ExtensionNumber", "Organization", "Roles", "Teams", "IsActive" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CallHistory"));
                    AddHeader(sheet, Header);
                    AddObjects(
                       sheet, 2, detail,
                       _ => _.UserName,
                       _ => _.Email,
                       _ => _.Mobile,
                       _ => _.Extention,
                       _ => _.Organization,
                       _ => _.Roles,
                       _ => _.Teams,
                       _ => _.IsActive == true ? "Yes" : "No"
                       );

                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
