﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using System.Data;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.TheSolarDemo;
using Abp.Organizations;
using TheSolarProduct.Reports.UserDetail.Dto;
using System.Data.Entity;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Reports.UserDetail
{
    public class UserDetailReportAppService : TheSolarProductAppServiceBase, IUserDetailReportAppService
    {

        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly  IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly UserManager _userManager;
        private readonly IUserDetailExcelExporter _userExcelExporter;


        public UserDetailReportAppService(
            IRepository<User, long> userRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<UserTeam> userTeamRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<Role> roleRepository,
            UserManager userManager,
            IUserDetailExcelExporter userExcelExporter
            )
        {
            _userRepository = userRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userRoleRepository = userRoleRepository;
            _userTeamRepository = userTeamRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _roleRepository = roleRepository;
            _userManager = userManager;
            _userExcelExporter = userExcelExporter;
        }


        public async Task<PagedResultDto<GetAllUserDetailDto>> GetAll(GetUserDetailInput input)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var UserIds = _userRoleRepository.GetAll().Where(e => e.RoleId != 6).Select(e => e.UserId).Distinct().ToList();

            var users = _userRepository.GetAll().Where(e => UserIds.Contains(e.Id))
                .WhereIf(input.OrganizationUnit > 0, e => e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnit))
                .WhereIf(input.FilterName == "Name" && !string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter || e.Surname == input.Filter)
                .WhereIf(input.FilterName == "Mobile" && !string.IsNullOrEmpty(input.Filter), e => e.Mobile == input.Filter)
                .WhereIf(input.FilterName == "Extension" && !string.IsNullOrEmpty(input.Filter), e => e.ExtensionNumber == input.Filter)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id).AsNoTracking()
                .Select(e => new { e.Id, e.FullName, e.EmailAddress, e.Mobile, e.ExtensionNumber, e.IsActive });

            var pagedAndFiltered = users
                  .OrderBy(input.Sorting ?? "Id desc")
                  .PageBy(input).ToList();

            var result = (from u in pagedAndFiltered

                          let orgsIds = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == u.Id).AsNoTracking().Select(e => e.OrganizationUnitId).ToList()
                          let orgs = orgsIds.Count() > 0 ? string.Join(", ", _organizationUnitRepository.GetAll().Where(e => orgsIds.Contains(e.Id)).AsNoTracking().Select(e => e.DisplayName).ToList()) : ""

                          let roleIds = _userRoleRepository.GetAll().Where(e => e.UserId == u.Id && e.RoleId != 6).AsNoTracking().Select(e => e.RoleId).ToList()
                          let roles = roleIds.Count() > 0 ? string.Join(", ", _roleRepository.GetAll().Where(e => roleIds.Contains(e.Id)).AsNoTracking().Select(e => e.Name).ToList()) : ""

                          let teams = string.Join(", ", _userTeamRepository.GetAll().Where(e => e.UserId == u.Id).AsNoTracking().Select(e => e.TeamFk.Name).Distinct().ToList())

                          select new GetAllUserDetailDto
                          {
                              Id = (int)u.Id,
                              UserName = u.FullName,
                              Email = u.EmailAddress,
                              Mobile = u.Mobile,
                              Extention = u.ExtensionNumber,
                              IsActive = u.IsActive,
                              Organization = orgs,
                              Roles = roles,
                              Teams = teams,
                          }).ToList();

            var totalcount = users.Count();

            return new PagedResultDto<GetAllUserDetailDto>(totalcount, result);

        }

        public async Task<FileDto> GetUserDetailToExcel(GetUserDetailExportInput input)
        {
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var UserIds = _userRoleRepository.GetAll().Where(e => e.RoleId != 6).Select(e => e.UserId).Distinct().ToList();

            var users = _userRepository.GetAll().Where(e => UserIds.Contains(e.Id))
                .WhereIf(input.OrganizationUnit > 0, e => e.OrganizationUnits.Any(o => o.OrganizationUnitId == input.OrganizationUnit))
                .WhereIf(input.FilterName == "Name" && !string.IsNullOrEmpty(input.Filter), e => e.Name == input.Filter || e.Surname == input.Filter)
                .WhereIf(input.FilterName == "Mobile" && !string.IsNullOrEmpty(input.Filter), e => e.Mobile == input.Filter)
                .WhereIf(input.FilterName == "Extension" && !string.IsNullOrEmpty(input.Filter), e => e.ExtensionNumber == input.Filter)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                .WhereIf(role.Contains("Sales Rep"), e => e.Id == User.Id).AsNoTracking()
                .Select(e => new { e.Id, e.FullName, e.EmailAddress, e.Mobile, e.ExtensionNumber, e.IsActive });

            var pagedAndFiltered = users
                  .OrderBy("Id desc").ToList();

            var result = (from u in pagedAndFiltered

                          let orgsIds = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == u.Id).AsNoTracking().Select(e => e.OrganizationUnitId).ToList()
                          let orgs = orgsIds.Count() > 0 ? string.Join(", ", _organizationUnitRepository.GetAll().Where(e => orgsIds.Contains(e.Id)).AsNoTracking().Select(e => e.DisplayName).ToList()) : ""

                          let roleIds = _userRoleRepository.GetAll().Where(e => e.UserId == u.Id && e.RoleId != 6).AsNoTracking().Select(e => e.RoleId).ToList()
                          let roles = roleIds.Count() > 0 ? string.Join(", ", _roleRepository.GetAll().Where(e => roleIds.Contains(e.Id)).AsNoTracking().Select(e => e.Name).ToList()) : ""

                          let teams = string.Join(", ", _userTeamRepository.GetAll().Where(e => e.UserId == u.Id).AsNoTracking().Select(e => e.TeamFk.Name).Distinct().ToList())

                          select new GetAllUserDetailDto
                          {
                              Id = (int)u.Id,
                              UserName = u.FullName,
                              Email = u.EmailAddress,
                              Mobile = u.Mobile,
                              Extention = u.ExtensionNumber,
                              IsActive = u.IsActive,
                              Organization = orgs,
                              Roles = roles,
                              Teams = teams,
                          }).ToList();

            return _userExcelExporter.UserDetailExport(result, input.fileName);

        }


    }
}
