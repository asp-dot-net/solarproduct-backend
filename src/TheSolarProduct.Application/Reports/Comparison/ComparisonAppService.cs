﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.LeadSources;
//using System.Globalization;
using Abp.Timing.Timezone;
//using RestSharp.Authenticators;
using TheSolarProduct.Timing;
//using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
//using TheSolarProduct.PickList.Dtos;
//using System.Collections;
//using Abp.Domain.Entities;
//using Microsoft.AspNetCore.Mvc;
//using NUglify.JavaScript.Syntax;
using TheSolarProduct.Reports.Comparison.Dtos;
using Job = TheSolarProduct.Jobs.Job;
using Abp.Collections.Extensions;
using NPOI.SS.Formula.Functions;
using System;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Expense;
using TheSolarProduct.States;
using TheSolarProduct.Leads;
using TheSolarProduct.Authorization.Users;
using Microsoft.AspNetCore.Identity;
using Stripe;
using Abp.Application.Services.Dto;
using TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos;
using Abp.Linq.Extensions;
using Twilio.TwiML.Voice;

namespace TheSolarProduct.Reports.Comparison
{
    public class ComparisonAppService : TheSolarProductAppServiceBase, IComparisonAppService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<LeadExpense> _leadExpenseRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;

        public ComparisonAppService(
            IRepository<Job> jobRepository,
            IRepository<LeadSource, int> lookup_leadSourceRepository,
            ITimeZoneConverter timeZoneConverter,
            ITimeZoneService timeZoneService,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<LeadExpense> leadExpenseRepository,
            IRepository<State> stateRepository,
            IRepository<Lead> leadRepository,
            IRepository<UserTeam> userTeamRepository,
            IRepository<User, long> userRepository,
            UserManager userManager
            ) 
        {
            _jobRepository = jobRepository;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _jobProductItemRepository = jobProductItemRepository;
            _leadExpenseRepository = leadExpenseRepository;
            _stateRepository = stateRepository;
            _leadRepository = leadRepository;
            _userRepository = userRepository;
            _userTeamRepository = userTeamRepository;
            _userManager = userManager;
        }

        public async Task<GetAllComparisonDto> GetAll(GetComparisonInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var User_List = _userRepository.GetAll().AsNoTracking();
            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> RoleName = await _userManager.GetRolesAsync(User);
            //var leadSources = _lookup_leadSourceRepository.GetAll();
            //// .WhereIf(input.leadSources != null && input.leadSources.Count() > 0, e => input.leadSources.Contains((int)e.Id))
            //// .Where(e => orgnize.Contains(e.Id));
            //// .OrderBy(p => p.Id).ToList();

            //var result = new PagedResultDto<GetAllComparisonDto>();
            input.Leadsources = input.Leadsources != null ? input.Leadsources : new List<int>();
            input.States = input.States != null ? input.States : new List<string>();
            input.AreaFilter = input.AreaFilter != null ? input.AreaFilter : new List<string>();

            var stateIds = _stateRepository.GetAll().WhereIf(input.States.Count() > 0, e => input.States.Contains(e.Name)).Select(e => e.Id).ToList();

            var leadexpence = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentFk.OrganizationId == input.OrgId)
                                .WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                                .WhereIf(input.States.Count() > 0, e => stateIds.Contains((int)e.StateId))
                                .GroupBy(e => new{ e.LeadinvestmentFk.FromDate, e.LeadinvestmentFk.ToDate})
                                .Select(e => new { Amount = e.Sum(s => s.Amount), FromDate = e.Key.FromDate, ToDate = e.Key.ToDate }).ToList();
                        
            var LeadList = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrgId && e.HideDublicate != true)
                           
                           .Select(e => new { e.LeadSourceId, e.State, CreationTime = e.CreationTime.AddHours(diffHour).Date, e.AssignToUserID, e.Area }).ToList();

            var Lead = LeadList.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                           .WhereIf(RoleName.Contains("User"), e => e.AssignToUserID != null)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                .Select(e => e.CreationTime).ToList();


            var Last_SDate = SDate.Value.Date.AddYears(-1);
            var Last_EDate = EDate.Value.Date.AddYears(-1);

            var Last_SDate2 = SDate.Value.Date.AddYears(-2);
            var Last_EDate2 = EDate.Value.Date.AddYears(-2);

            var JobsList = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrgId)
                          .Select(e => new {e.Id, InstallationDate = e.InstallationDate.Value.AddHours(diffHour).Date, FirstDepositDate = e.FirstDepositDate.Value.Date, e.SystemCapacity, e.LeadFk.LeadSourceId, e.State, e.LeadFk.AssignToUserID,e .LeadFk.Area, e.BatteryKw }).ToList();

            var Jobs = JobsList.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                           .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                           .WhereIf(RoleName.Contains("User"), e => e.AssignToUserID != null)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                 .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                //.WhereIf(!string.IsNullOrEmpty(input.AreaFilter), e => e.Area == input.AreaFilter)
                .Select(e => new { e.Id, e.InstallationDate, e.FirstDepositDate, e.SystemCapacity, BatteryKw = e.BatteryKw > 0 ? e.BatteryKw : 0  }).ToList();

            var result = new GetAllComparisonDto();

            var expence = leadexpence.Where(e => (e.FromDate.Date >= SDate.Value.Date && e.ToDate.Date <= EDate.Value.Date) || (e.FromDate.Date <= SDate.Value.Date && e.ToDate.Date >= EDate.Value.Date));

            var dates = new List<DateTime>();

            for (var dt = SDate.Value.Date; dt <= EDate.Value.Date; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }

            result.Spend = 0;
            foreach (var e in expence)
            {
                var eday = (e.ToDate.Date - e.FromDate.Date).Days + 1;
                var perdayAmount = e.Amount / eday;

                var daysC = dates.Where(d => d >= e.FromDate.Date && d <= e.ToDate.Date).Count();

                result.Spend = result.Spend + (perdayAmount * daysC);

            }

            var Lastexpence = leadexpence.Where(e => (e.FromDate.Date >= Last_SDate && e.ToDate.Date <= Last_EDate) || (e.FromDate.Date <= Last_SDate && e.ToDate.Date >= Last_EDate));

            var Lastdates = new List<DateTime>();

            for (var dt = Last_SDate; dt <= Last_EDate; dt = dt.AddDays(1))
            {
                Lastdates.Add(dt);
            }

            result.LastSpend = 0;
            foreach (var e in Lastexpence)
            {
                var Last_eday = (e.ToDate.Date - e.FromDate.Date).Days + 1;
                var Last_perdayAmount = e.Amount / Last_eday;

                var Last_daysC = Lastdates.Where(d => d >= e.FromDate.Date && d <= e.ToDate.Date).Count();

                result.LastSpend = result.LastSpend + (Last_perdayAmount * Last_daysC);

            }

            var Lastexpence2 = leadexpence.Where(e => (e.FromDate.Date >= Last_SDate2 && e.ToDate.Date <= Last_EDate2) || (e.FromDate.Date <= Last_SDate2 && e.ToDate.Date >= Last_EDate2));

            var Lastdates2 = new List<DateTime>();

            for (var dt = Last_SDate2; dt <= Last_EDate2; dt = dt.AddDays(1))
            {
                Lastdates2.Add(dt);
            }

            result.LastSpend2 = 0;
            foreach (var e in Lastexpence2)
            {
                var Last_perdayAmount2 = e.Amount / (e.ToDate.Date - e.FromDate.Date).Days + 1;

                var Last_daysC2 = Lastdates2.Where(d => d >= e.FromDate.Date && d <= e.ToDate.Date).Count();

                result.LastSpend2 = result.LastSpend2 + (Last_perdayAmount2 * Last_daysC2);

            }

            //result.Spend = leadexpence.Where(e => e.FromDate.Date >= SDate.Value.Date && e.ToDate.Date <= EDate.Value.Date).Sum(e => e.Amount);
            //result.LastSpend = leadexpence.Where(e => e.FromDate.Date >= Last_SDate && e.ToDate.Date <= Last_EDate).Sum(e => e.Amount);
            //result.LastSpend2 = leadexpence.Where(e => e.FromDate.Date >= Last_SDate2 && e.ToDate.Date <= Last_EDate2).Sum(e => e.Amount);


            var PanelSold_NoOfPanelJobs = Jobs.Where(j => j.FirstDepositDate >= SDate.Value.Date && j.FirstDepositDate <= EDate.Value.Date).Select(e => e.Id).ToList();
            //result.PanelSold_NoOfPanel = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 1 && PanelSold_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity);

            result.BatterySold_KW = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && PanelSold_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);
            //(from o in _jobProductItemRepository.GetAll().AsNoTracking()e.
            //             join j in Jobs on o.JobId equals j.Id
            //             where o.ProductItemFk.ProductTypeId == 5
            //             select (o.ProductItemFk.Size * o.Quantity)
            //                           ).Sum();
            result.PanelSold_KW = Jobs.Where(j => j.FirstDepositDate >= SDate.Value.Date && j.FirstDepositDate <= EDate.Value.Date).Sum(e => e.SystemCapacity);// + batterykw_sold;

            //result.TotalLead = PanelSold_NoOfPanelJobs.Count();
            result.TotalLead = Lead.Where(e => e >= SDate.Value.Date && e <= EDate.Value.Date).Count();


            var Installed_NoOfPanelJobs = Jobs.Where(j => j.InstallationDate >= SDate.Value.Date && j.InstallationDate <= EDate.Value.Date).Select(e => e.Id).ToList();
            //result.Installed_NoOfPanel = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 1 && Installed_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity);
            var batterykw_Installed = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && Installed_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);

            result.Installed_KW = Jobs.Where(j => j.InstallationDate >= SDate.Value.Date && j.InstallationDate <= EDate.Value.Date).Sum(e => e.SystemCapacity) + batterykw_Installed;
            //result.TotalLeadInstalled = Installed_NoOfPanelJobs.Count();

            var LastPanelSold_NoOfPanelJobs = Jobs.Where(j => j.FirstDepositDate >= Last_SDate && j.FirstDepositDate <= Last_EDate).Select(e => e.Id).ToList();
            //result.LastPanelSold_NoOfPanel = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 1 && LastPanelSold_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity);
            result.LastBatterySold_KW = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && LastPanelSold_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);

            result.LastPanelSold_KW = Jobs.Where(j => j.FirstDepositDate >= Last_SDate && j.FirstDepositDate <= Last_EDate).Sum(e => e.SystemCapacity);// + Lastbatterykw_sold;
            //result.LastTotalLead = LastPanelSold_NoOfPanelJobs.Count();
            result.LastTotalLead = Lead.Where(e => e >= Last_SDate && e <= Last_EDate).Count();

            var LastInstalled_NoOfPanelJobs = Jobs.Where(j => j.InstallationDate >= Last_SDate && j.InstallationDate <= Last_EDate).Select(e => e.Id).ToList();
            //result.LastInstalled_NoOfPanel = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 1 && LastInstalled_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity);
            var Lastbatterykw_Installed = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && LastInstalled_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);

            result.LastInstalled_KW = Jobs.Where(j => j.InstallationDate >= Last_SDate && j.InstallationDate <= Last_EDate).Sum(e => e.SystemCapacity ) + Lastbatterykw_Installed;
            //result.LastTotalLeadInstalled = LastInstalled_NoOfPanelJobs.Count();


            var LastPanelSold2_NoOfPanelJobs = Jobs.Where(j => j.FirstDepositDate >= Last_SDate2 && j.FirstDepositDate <= Last_EDate2).Select(e => e.Id).ToList();
            //result.LastPanelSold2_NoOfPanel = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 1 && LastPanelSold2_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity);
            result.LastBatterySold_KW = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && LastPanelSold2_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);

            result.LastPanelSold2_KW = Jobs.Where(j => j.FirstDepositDate >= Last_SDate2 && j.FirstDepositDate <= Last_EDate2).Sum(e => e.SystemCapacity);// + Lastbatterykw_sold2;
            //result.LastTotalLead2 = LastPanelSold2_NoOfPanelJobs.Count();
            result.LastTotalLead2 = Lead.Where(e => e >= Last_SDate2 && e <= Last_EDate2).Count();


            var LastInstalled2_NoOfPanelJobs = Jobs.Where(j => j.InstallationDate >= Last_SDate2 && j.InstallationDate <= Last_EDate2).Select(e => e.Id).ToList();
            //result.LastInstalled2_NoOfPanel = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 1 && LastInstalled2_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity);
            var Lastbatterykw_Installed2 = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && LastInstalled2_NoOfPanelJobs.Contains((int)j.JobId)).Sum(e => e.Quantity * e.ProductItemFk.Size);

            result.LastInstalled2_KW = Jobs.Where(j => j.InstallationDate >= Last_SDate2 && j.InstallationDate <= Last_EDate2).Sum(e => e.SystemCapacity )+ Lastbatterykw_Installed2;
            //result.LastTotalLeadInstalled2 = LastInstalled2_NoOfPanelJobs.Count();

            result.Date = SDate?.ToString("dd MMM yy") + " - " + EDate?.ToString("dd MMM yy");
            result.LastDate = Last_SDate.ToString("dd MMM yy") + " - " + Last_EDate.ToString("dd MMM yy");
            result.LastDate2 = Last_SDate2.ToString("dd MMM yy") + " - " + Last_EDate2.ToString("dd MMM yy");


            //var result = from l in leadSources

            //             //let jobs = 

            //             select new GetAllComparisonDto()
            //             {
            //                 //PanelSold = jobs.Where(e => e.InstallationDate >= SDate.Value.Date).Sum(e => e.)
            //             };

            return result;

        }

        public async Task<PagedResultDto<GetAllMonthlyComparisonDto>> GetAllMonthly(GetMonthlyComparisonInput input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var User_List = _userRepository.GetAll().AsNoTracking();
            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            input.Leadsources = input.Leadsources != null ? input.Leadsources : new List<int>();
            input.States = input.States != null ? input.States : new List<string>();
            input.AreaFilter = input.AreaFilter != null ? input.AreaFilter : new List<string>();

            var LeadList = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrgId && e.HideDublicate != true)

                           .Select(e => new { e.LeadSourceId, e.State, CreationTime = e.CreationTime.AddHours(diffHour).Date, e.AssignToUserID, e.Area }).ToList();

            var Lead = LeadList.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                .Select(e => e.CreationTime).ToList();

            //var Last_SDate = SDate.Value.Date.AddYears(-1);
            //var Last_EDate = EDate.Value.Date.AddYears(-1);

            //var Last_SDate2 = SDate.Value.Date.AddYears(-2);
            //var Last_EDate2 = EDate.Value.Date.AddYears(-2);


            var JobsList = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrgId && e.FirstDepositDate.Value.Year > 1)
                          .Select(e => new { e.Id, InstallationDate = e.InstallationDate.Value.AddHours(diffHour).Date, FirstDepositDate = e.FirstDepositDate.Value.Date, e.SystemCapacity, e.LeadFk.LeadSourceId, e.State, e.LeadFk.AssignToUserID, e.LeadFk.Area, e.BatteryKw }).ToList();

            var Jobs = JobsList.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                           .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                 .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                //.WhereIf(!string.IsNullOrEmpty(input.AreaFilter), e => e.Area == input.AreaFilter)
                .Select(e => new { e.Id, e.InstallationDate, e.FirstDepositDate, e.SystemCapacity, BatteryKw = e.BatteryKw > 0 ? e.BatteryKw : 0 }).ToList();

            var jobIdList = Jobs.Select(e => e.Id).ToList();

            var ListbettryKw = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && j.JobFk.LeadFk.OrganizationId == input.OrgId && j.JobFk.FirstDepositDate.Value.Year > 1)
                .Select(e => new { InstallationDate = e.JobFk.InstallationDate.Value.AddHours(diffHour).Date, FirstDepositDate = e.JobFk.FirstDepositDate.Value.Date, kw = e.Quantity * e.ProductItemFk.Size, e.JobFk.LeadFk.LeadSourceId, e.JobFk.State, e.JobFk.LeadFk.AssignToUserID, e.JobFk.LeadFk.Area }).ToList();
            
            var bettryKwList = ListbettryKw.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                           .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                 .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                //.WhereIf(!string.IsNullOrEmpty(input.AreaFilter), e => e.Area == input.AreaFilter)
                .Select(e => new { e.InstallationDate, e.FirstDepositDate, e.kw }).ToList();


            var yearList = Jobs.Select(e => e.FirstDepositDate.Year).Distinct().ToList();

            var stateIds = _stateRepository.GetAll().WhereIf(input.States.Count() > 0, e => input.States.Contains(e.Name)).Select(e => e.Id).ToList();

            var leadexpence = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentFk.OrganizationId == input.OrgId)
                                .WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                                .WhereIf(input.States.Count() > 0, e => stateIds.Contains((int)e.StateId))
                                .GroupBy(e => new { e.LeadinvestmentFk.FromDate, e.LeadinvestmentFk.ToDate })
                                .Select(e => new { Amount = e.Sum(s => s.Amount), FromDate = e.Key.FromDate, ToDate = e.Key.ToDate }).ToList();

            var result = from o in yearList
                         select new GetAllMonthlyComparisonDto
                         {
                             Year = o,
                             Jan_Lead = Lead.Where(e => e.Year == o && e.Month == 1).Count(),
                             Jan_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 1).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 1).Sum(e => e.kw),
                             Jan_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 1).Sum(e => e.SystemCapacity),
                             Jan_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 1).Sum(e => e.kw),
                             Jan_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o,1,1)).Date && e.ToDate.Date <= (new DateTime(o, 1, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 1, 1)).Date && e.ToDate.Date >= (new DateTime(o, 1, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Fab_Lead = Lead.Where(e => e.Year == o && e.Month == 2).Count(),
                             Fab_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 2).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 2).Sum(e => e.kw),
                             Fab_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 2).Sum(e => e.SystemCapacity),
                             Fab_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 2).Sum(e => e.kw),
                             Fab_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o,2,1)).Date && e.ToDate.Date <= (new DateTime(o, 2, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 2, 1)).Date && e.ToDate.Date >= (new DateTime(o,2, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Mar_Lead = Lead.Where(e => e.Year == o && e.Month == 3).Count(),
                             Mar_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 3).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 3).Sum(e => e.kw),
                             Mar_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 3).Sum(e => e.SystemCapacity),
                             Mar_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 3).Sum(e => e.kw),
                             Mar_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 3,1)).Date && e.ToDate.Date <= (new DateTime(o, 3, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 3, 1)).Date && e.ToDate.Date >= (new DateTime(o, 3, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Apr_Lead = Lead.Where(e => e.Year == o && e.Month == 4).Count(),
                             Apr_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 4).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 4).Sum(e => e.kw),
                             Apr_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 4).Sum(e => e.SystemCapacity),
                             Apr_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 4).Sum(e => e.kw),
                             Apr_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 4,1)).Date && e.ToDate.Date <= (new DateTime(o, 4, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 4, 1)).Date && e.ToDate.Date >= (new DateTime(o, 4, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             May_Lead = Lead.Where(e => e.Year == o && e.Month == 5).Count(),
                             May_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 5).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 5).Sum(e => e.kw),
                             May_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 5).Sum(e => e.SystemCapacity),
                             May_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 5).Sum(e => e.kw),
                             May_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 5,1)).Date && e.ToDate.Date <= (new DateTime(o, 5, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 5, 1)).Date && e.ToDate.Date >= (new DateTime(o, 5, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Jun_Lead = Lead.Where(e => e.Year == o && e.Month == 6).Count(),
                             Jun_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 6).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 6).Sum(e => e.kw),
                             Jun_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 6).Sum(e => e.SystemCapacity),
                             Jun_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 6).Sum(e => e.kw),
                             Jun_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 6,1)).Date && e.ToDate.Date <= (new DateTime(o, 6, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 6, 1)).Date && e.ToDate.Date >= (new DateTime(o, 6, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Jul_Lead = Lead.Where(e => e.Year == o && e.Month == 7).Count(),
                             Jul_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 7).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 7).Sum(e => e.kw),
                             Jul_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 7).Sum(e => e.SystemCapacity),
                             Jul_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 7).Sum(e => e.kw),
                             Jul_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 7,1)).Date && e.ToDate.Date <= (new DateTime(o, 7, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 7, 1)).Date && e.ToDate.Date >= (new DateTime(o, 7, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Aug_Lead = Lead.Where(e => e.Year == o && e.Month == 8).Count(),
                             Aug_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 8).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 8).Sum(e => e.kw),
                             Aug_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 8).Sum(e => e.SystemCapacity),
                             Aug_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 8).Sum(e => e.kw),
                             Aug_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 8,1)).Date && e.ToDate.Date <= (new DateTime(o, 8, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 8, 1)).Date && e.ToDate.Date >= (new DateTime(o, 8, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Sep_Lead = Lead.Where(e => e.Year == o && e.Month == 9).Count(),
                             Sep_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 9).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 9).Sum(e => e.kw),
                             Sep_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 9).Sum(e => e.SystemCapacity),
                             Sep_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 9).Sum(e => e.kw),
                             Sep_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 9,1)).Date && e.ToDate.Date <= (new DateTime(o, 9, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 9, 1)).Date && e.ToDate.Date >= (new DateTime(o, 9, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Oct_Lead = Lead.Where(e => e.Year == o && e.Month == 10).Count(),
                             Oct_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 10).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 10).Sum(e => e.kw),
                             Oct_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 10).Sum(e => e.SystemCapacity),
                             Oct_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 10).Sum(e => e.kw),
                             Oct_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 10,1)).Date && e.ToDate.Date <= (new DateTime(o, 10, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 10, 1)).Date && e.ToDate.Date >= (new DateTime(o, 10, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Nov_Lead = Lead.Where(e => e.Year == o && e.Month == 11).Count(),
                             Nov_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 11).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 11).Sum(e => e.kw),
                             Nov_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 11).Sum(e => e.SystemCapacity),
                             Nov_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 11).Sum(e => e.kw),
                             Nov_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 11,1)).Date && e.ToDate.Date <= (new DateTime(o, 11, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 11, 1)).Date && e.ToDate.Date >= (new DateTime(o, 11, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                             Dec_Lead = Lead.Where(e => e.Year == o && e.Month == 12).Count(),
                             Dec_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 12).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 12).Sum(e => e.kw),
                             Dec_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 12).Sum(e => e.SystemCapacity),
                             Dec_BatterySold_KW = bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 12).Sum(e => e.kw),
                             Dec_Expence = leadexpence.Where(e => (e.FromDate.Date >= (new DateTime(o, 12,1)).Date && e.ToDate.Date <= (new DateTime(o, 12, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(o, 12, 1)).Date && e.ToDate.Date >= (new DateTime(o, 12, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount),
                         };

            var totalCount = result.Count();

            var pagedAndFilteredJobs = result.AsQueryable()
                .OrderBy(input.Sorting ?? "Year desc")
                .PageBy(input);

            var FilteredJobs = pagedAndFilteredJobs.ToList();

            return new PagedResultDto<GetAllMonthlyComparisonDto>(totalCount, FilteredJobs);


        }

        public async Task<PagedResultDto<GetAllComparisonDto>> GetAllLeadSource(GetLeadComparisonInput input)

        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var User_List = _userRepository.GetAll().AsNoTracking();
            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            input.Leadsources = input.Leadsources != null ? input.Leadsources : new List<int>();
            input.States = input.States != null ? input.States : new List<string>();
            input.AreaFilter = input.AreaFilter != null ? input.AreaFilter : new List<string>();

            var stateIds = _stateRepository.GetAll().WhereIf(input.States.Count() > 0, e => input.States.Contains(e.Name)).Select(e => e.Id).ToList();

            //var leadexpence = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentFk.OrganizationId == input.OrgId)
            //                    .WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
            //                    .WhereIf(input.States.Count() > 0, e => stateIds.Contains((int)e.StateId))
            //                    .GroupBy(e => new { e.LeadinvestmentFk.FromDate, e.LeadinvestmentFk.ToDate })
            //                    .Select(e => new { e.Key.LeadSourceId,Amount = e.Sum(s => s.Amount), FromDate = e.Key.FromDate, ToDate = e.Key.ToDate }).ToList();
            var leadexpence = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentFk.OrganizationId == input.OrgId)
                        .WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                        .WhereIf(input.States.Count() > 0, e => stateIds.Contains((int)e.StateId))
                        .GroupBy(e => new { e.LeadinvestmentFk.FromDate, e.LeadinvestmentFk.ToDate ,e.LeadSourceId })

                        .Select(e => new { e.Key.LeadSourceId, Amount = e.Sum(s => s.Amount), FromDate = e.Key.FromDate, ToDate = e.Key.ToDate }).ToList();

            var LeadList = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrgId && e.HideDublicate != true)

                           .Select(e => new { e.LeadSourceId, e.LeadSource, e.State, CreationTime = e.CreationTime.AddHours(diffHour).Date, e.AssignToUserID, e.Area }).ToList();

            var Lead = LeadList.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                .Select(e => new { e.LeadSourceId, e.LeadSource, e.CreationTime }).ToList();


            var Last_SDate = SDate.Value.Date.AddYears(-1);
            var Last_EDate = EDate.Value.Date.AddYears(-1);

            var Last_SDate2 = SDate.Value.Date.AddYears(-2);
            var Last_EDate2 = EDate.Value.Date.AddYears(-2);

            var JobsList = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrgId)
                          .Select(e => new { e.Id, InstallationDate = e.InstallationDate.Value.AddHours(diffHour).Date, FirstDepositDate = e.FirstDepositDate.Value.Date, e.SystemCapacity, e.LeadFk.LeadSourceId, e.LeadFk.LeadSource, e.State, e.LeadFk.AssignToUserID, e.LeadFk.Area, e.BatteryKw }).ToList();

            var Jobs = JobsList.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                           .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                 .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                //.WhereIf(!string.IsNullOrEmpty(input.AreaFilter), e => e.Area == input.AreaFilter)
                .Select(e => new { e.Id, e.InstallationDate, e.FirstDepositDate, e.SystemCapacity, e.LeadSourceId, e.LeadSource, BatteryKw = e.BatteryKw > 0 ? e.BatteryKw : 0 }).ToList();

            var LeadSource = Jobs.Where(e => e.LeadSourceId > 0).Select(e => e.LeadSourceId).Distinct().ToList();

            var ListbettryKw = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && j.JobFk.LeadFk.OrganizationId == input.OrgId && j.JobFk.FirstDepositDate.Value.Year > 1)
                .Select(e => new { InstallationDate = e.JobFk.InstallationDate.Value.AddHours(diffHour).Date, FirstDepositDate = e.JobFk.FirstDepositDate.Value.Date, kw = e.Quantity * e.ProductItemFk.Size, e.JobFk.LeadFk.LeadSourceId, e.JobFk.State, e.JobFk.LeadFk.AssignToUserID, e.JobFk.LeadFk.Area }).ToList();
            var bettryKwList = ListbettryKw.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                          .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                          .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                          .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
               .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
               .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
               //.WhereIf(!string.IsNullOrEmpty(input.AreaFilter), e => e.Area == input.AreaFilter)
               .Select(e => new { e.InstallationDate, e.FirstDepositDate, e.LeadSourceId, e.kw }).ToList();
         

            var result = ( from o in LeadSource
                         select new GetAllComparisonDto
                         {
                             LeadSourceId = o,

                             LeadSource = Lead.FirstOrDefault(l => l.LeadSourceId == o)?.LeadSource,
                             TotalLead = Lead.Count(e => e.LeadSourceId == o && e.CreationTime >= SDate.Value.Date && e.CreationTime <= EDate.Value.Date),

                             PanelSold_KW = Jobs.Where(j => j.LeadSourceId == o && j.FirstDepositDate >= SDate.Value.Date && j.FirstDepositDate <= EDate.Value.Date)
                                        .Sum(e => e.SystemCapacity),

                             BatterySold_KW = bettryKwList.Where(j => j.LeadSourceId == o && j.FirstDepositDate >= SDate.Value.Date && j.FirstDepositDate <= EDate.Value.Date).Sum(e => e.kw),

                             Installed_KW = Jobs.Where(j => j.LeadSourceId == o && j.InstallationDate >= SDate.Value.Date && j.InstallationDate <= EDate.Value.Date).Sum(e => e.SystemCapacity)
                                          + bettryKwList.Where(j => j.LeadSourceId == o && j.InstallationDate >= SDate.Value.Date && j.InstallationDate <= EDate.Value.Date).Sum(e => e.kw),

                             //Jan_Installed_KW = Jobs.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 1).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.InstallationDate.Year == o && j.InstallationDate.Month == 1).Sum(e => e.kw),
                             //Jan_PanelSold_KW = Jobs.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 1).Sum(e => e.SystemCapacity) + bettryKwList.Where(j => j.FirstDepositDate.Year == o && j.FirstDepositDate.Month == 1).Sum(e => e.kw),


                             LastTotalLead = Lead.Where(e => e.LeadSourceId == o && e.CreationTime >= Last_SDate.Date && e.CreationTime <= Last_EDate.Date).Count(),

                             LastPanelSold_KW = Jobs.Where(j => j.LeadSourceId == o && j.FirstDepositDate >= Last_SDate.Date && j.FirstDepositDate <= Last_EDate.Date).Sum(e => e.SystemCapacity),

                             LastBatterySold_KW = bettryKwList.Where(j => j.LeadSourceId == o && j.FirstDepositDate >= Last_SDate.Date && j.FirstDepositDate <= Last_EDate.Date).Sum(e => e.kw),

                             LastInstalled_KW = Jobs.Where(j => j.LeadSourceId == o && j.InstallationDate >= Last_SDate.Date && j.InstallationDate <= Last_EDate.Date).Sum(e => e.SystemCapacity)
                                          + bettryKwList.Where(j => j.LeadSourceId == o && j.InstallationDate >= Last_SDate.Date && j.InstallationDate <= Last_EDate.Date).Sum(e => e.kw),



                             LastTotalLead2 = Lead.Where(e => e.LeadSourceId == o && e.CreationTime >= Last_SDate2.Date && e.CreationTime <= Last_EDate2.Date).Count(),

                             LastPanelSold2_KW = Jobs.Where(j => j.LeadSourceId == o && j.FirstDepositDate >= Last_SDate2.Date && j.FirstDepositDate <= Last_EDate2.Date).Sum(e => e.SystemCapacity),
                             LastBatterySold_KW2 = bettryKwList.Where(j => j.LeadSourceId == o && j.FirstDepositDate >= Last_SDate2.Date && j.FirstDepositDate <= Last_EDate2.Date).Sum(e => e.kw),

                             LastInstalled2_KW = Jobs.Where(j => j.LeadSourceId == o && j.InstallationDate >= Last_SDate2.Date && j.InstallationDate <= Last_EDate2.Date).Sum(e => e.SystemCapacity)
                                          + bettryKwList.Where(j => j.LeadSourceId == o && j.InstallationDate >= Last_SDate2.Date && j.InstallationDate <= Last_EDate2.Date).Sum(e => e.kw),


                             Date = SDate?.ToString("dd MMM yy") + " - " + EDate?.ToString("dd MMM yy"),
                             LastDate = Last_SDate.ToString("dd MMM yy") + " - " + Last_EDate.ToString("dd MMM yy"),
                             LastDate2 = Last_SDate2.ToString("dd MMM yy") + " - " + Last_EDate2.ToString("dd MMM yy")

                         }).ToList();

            foreach (var resultItem in result)
            {
                var expence = leadexpence
                    .Where(e => (e.FromDate.Date >= SDate.Value.Date && e.ToDate.Date <= EDate.Value.Date) || (e.FromDate.Date <= SDate.Value.Date && e.ToDate.Date >= EDate.Value.Date))
                    .Where(e => e.LeadSourceId == resultItem.LeadSourceId);
                //var expence = leadexpence.Where(e => (e.FromDate.Date >= SDate.Value.Date && e.ToDate.Date <= EDate.Value.Date) || (e.FromDate.Date <= SDate.Value.Date && e.ToDate.Date >= EDate.Value.Date));

                var dates = new List<DateTime>();

                for (var dt = SDate.Value.Date; dt <= EDate.Value.Date; dt = dt.AddDays(1))
                {
                    dates.Add(dt);
                }

                resultItem.Spend = 0;
                foreach (var e in expence)
                {
                    var eday = (e.ToDate.Date - e.FromDate.Date).Days + 1;
                    var perdayAmount = e.Amount / eday;

                    var daysC = dates.Where(d => d >= e.FromDate.Date && d <= e.ToDate.Date).Count();

                    resultItem.Spend = resultItem.Spend + (perdayAmount * daysC);

                }

                var Lastexpence = leadexpence.Where(e => (e.FromDate.Date >= Last_SDate && e.ToDate.Date <= Last_EDate) || (e.FromDate.Date <= Last_SDate && e.ToDate.Date >= Last_EDate))
                     .Where(e => e.LeadSourceId == resultItem.LeadSourceId);

                var Lastdates = new List<DateTime>();

                for (var dt = Last_SDate; dt <= Last_EDate; dt = dt.AddDays(1))
                {
                    Lastdates.Add(dt);
                }

                resultItem.LastSpend = 0;
                foreach (var e in Lastexpence)
                {
                    var Last_eday = (e.ToDate.Date - e.FromDate.Date).Days + 1;
                    var Last_perdayAmount = e.Amount / Last_eday;

                    var Last_daysC = Lastdates.Where(d => d >= e.FromDate.Date && d <= e.ToDate.Date).Count();

                    resultItem.LastSpend = resultItem.LastSpend + (Last_perdayAmount * Last_daysC);

                }

                var Lastexpence2 = leadexpence.Where(e => (e.FromDate.Date >= Last_SDate2 && e.ToDate.Date <= Last_EDate2) || (e.FromDate.Date <= Last_SDate2 && e.ToDate.Date >= Last_EDate2))
                     .Where(e => e.LeadSourceId == resultItem.LeadSourceId);

                var Lastdates2 = new List<DateTime>();

                for (var dt = Last_SDate2; dt <= Last_EDate2; dt = dt.AddDays(1))
                {
                    Lastdates2.Add(dt);
                }

                resultItem.LastSpend2 = 0;
                foreach (var e in Lastexpence2)
                {
                    var Last_perdayAmount2 = e.Amount / (e.ToDate.Date - e.FromDate.Date).Days + 1;

                    var Last_daysC2 = Lastdates2.Where(d => d >= e.FromDate.Date && d <= e.ToDate.Date).Count();

                    resultItem.LastSpend2 = resultItem.LastSpend2 + (Last_perdayAmount2 * Last_daysC2);

                }
                //var dates = Enumerable.Range(0, (int)(EDate - SDate).Value.TotalDays + 1)
                //    .Select(offset => SDate.Value.AddDays(offset))
                //    .ToList();

                //decimal totalSpend = 0;
                //foreach (var e in expence)
                //{

                //    var eday = (e.ToDate.Date - e.FromDate.Date).Days + 1;
                //    var perdayAmount = e.Amount / eday;

                //    //var daysC = dates.Count(d => d >= e.FromDate.Date && d <= e.ToDate.Date);
                //    var daysC = dates.Where(d => d >= e.FromDate.Date && d <= e.ToDate.Date).Count();

                //    totalSpend += perdayAmount * daysC;
                //}

                //resultItem.Spend = totalSpend;

                //var Lastexpence = leadexpence
                //    .Where(e => (e.FromDate.Date >= Last_SDate && e.ToDate.Date <= Last_EDate) || (e.FromDate.Date <= Last_SDate && e.ToDate.Date >= Last_EDate))
                //    .Where(e => e.LeadSourceId == resultItem.LeadSourceId);

                //totalSpend = 0;
                //foreach (var e in Lastexpence)
                //{
                //    var Last_eday = (e.ToDate.Date - e.FromDate.Date).Days + 1;
                //    var Last_perdayAmount = e.Amount / Last_eday;

                //    var Last_daysC = dates.Count(d => d >= e.FromDate.Date && d <= e.ToDate.Date);

                //    totalSpend += Last_perdayAmount * Last_daysC;
                //}

                //resultItem.LastSpend = totalSpend;

                //var Lastexpence2 = leadexpence
                //    .Where(e => (e.FromDate.Date >= Last_SDate2 && e.ToDate.Date <= Last_EDate2) || (e.FromDate.Date <= Last_SDate2 && e.ToDate.Date >= Last_EDate2))
                //    .Where(e => e.LeadSourceId == resultItem.LeadSourceId);

                //totalSpend = 0;
                //foreach (var e in Lastexpence2)
                //{
                //    var Last_perdayAmount2 = e.Amount / (e.ToDate.Date - e.FromDate.Date).Days + 1;

                //    var Last_daysC2 = dates.Count(d => d >= e.FromDate.Date && d <= e.ToDate.Date);

                //    totalSpend += Last_perdayAmount2 * Last_daysC2;
                //}

                //resultItem.LastSpend2 = totalSpend;
            }

            result = result.Where(r => r.TotalLead != 0
                        || r.PanelSold_KW != 0
                        || r.BatterySold_KW != 0
                        || r.Installed_KW != 0

                         || r.LastTotalLead2 != 0
                        || r.LastPanelSold2_KW != 0
                        || r.LastBatterySold_KW2 != 0
                        || r.LastInstalled2_KW != 0

                        || r.LastTotalLead != 0
                        || r.LastPanelSold_KW != 0
                        || r.LastBatterySold_KW != 0
                        || r.LastInstalled_KW != 0


                        || r.Spend != 0
                        || r.LastSpend != 0
                        || r.LastSpend2 != 0
                     
              ).ToList();
            var summary = new GetAllComparisonDto();
            if(result.Count() > 0)
            {
                summary.LeadSource = "Total";
                summary.TotalLead = result.Sum(e => e.TotalLead);
                summary.PanelSold_KW = result.Sum(e => e.PanelSold_KW);
                summary.BatterySold_KW = result.Sum(e => e.BatterySold_KW);
                summary.Installed_KW = result.Sum(e => e.Installed_KW);
                summary.LastTotalLead = result.Sum(e => e.LastTotalLead);
                summary.LastPanelSold_KW = result.Sum(e => e.LastPanelSold_KW);
                summary.LastBatterySold_KW = result.Sum(e => e.LastBatterySold_KW);
                summary.LastInstalled_KW = result.Sum(e => e.LastInstalled_KW);
                summary.LastTotalLead2 = result.Sum(e => e.LastTotalLead2);
                summary.LastPanelSold2_KW = result.Sum(e => e.LastPanelSold2_KW);
                summary.LastBatterySold_KW2 = result.Sum(e => e.LastBatterySold_KW2);
                summary.LastInstalled2_KW = result.Sum(e => e.LastInstalled2_KW);
                summary.Spend=result.Sum(e => e.Spend);
                summary.LastSpend = result.Sum(e => e.LastSpend);
                summary.LastSpend2 = result.Sum(e => e.LastSpend2);
                result.Add(summary);
            }


            var totalCount = result.Count();

            return new PagedResultDto<GetAllComparisonDto>(totalCount, result);
        }

        public async Task<PagedResultDto<GetAllLeadSourceMonthlyComparisonDto>> GetAllLeadSourceMonthly(GetLeadSourceMonthlyComparisonInputDto input)
        {
            //var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            //var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            if(input.Months != null)
            {
                input.Months = input.Months.OrderBy(e => e).ToList();
            }

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var User_List = _userRepository.GetAll().AsNoTracking();
            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            var TeamId = await user_teamList.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

            var User = await User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> RoleName = await _userManager.GetRolesAsync(User);

            input.Leadsources = input.Leadsources != null ? input.Leadsources : new List<int>();
            input.States = input.States != null ? input.States : new List<string>();
            input.AreaFilter = input.AreaFilter != null ? input.AreaFilter : new List<string>();

            var stateIds = _stateRepository.GetAll().WhereIf(input.States.Count() > 0, e => input.States.Contains(e.Name)).Select(e => e.Id).ToList();

            //var leadexpence = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentFk.OrganizationId == input.OrgId)
            //                    .WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
            //                    .WhereIf(input.States.Count() > 0, e => stateIds.Contains((int)e.StateId))
            //                    .GroupBy(e => new { e.LeadinvestmentFk.FromDate, e.LeadinvestmentFk.ToDate })
            //                    .Select(e => new { e.Key.LeadSourceId,Amount = e.Sum(s => s.Amount), FromDate = e.Key.FromDate, ToDate = e.Key.ToDate }).ToList();
            var leadexpence = _leadExpenseRepository.GetAll().Where(e => e.LeadinvestmentFk.OrganizationId == input.OrgId)
                        .WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                        .WhereIf(input.States.Count() > 0, e => stateIds.Contains((int)e.StateId))
                        .GroupBy(e => new { e.LeadinvestmentFk.FromDate, e.LeadinvestmentFk.ToDate, e.LeadSourceId })

                        .Select(e => new { e.Key.LeadSourceId, Amount = e.Sum(s => s.Amount), FromDate = e.Key.FromDate, ToDate = e.Key.ToDate }).ToList();

            var LeadList = _leadRepository.GetAll().Where(e => e.OrganizationId == input.OrgId && e.HideDublicate != true)

                           .Select(e => new { e.LeadSourceId, e.LeadSource, e.State, CreationTime = e.CreationTime.AddHours(diffHour).Date, e.AssignToUserID, e.Area }).ToList();

            var Lead = LeadList.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                           .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                           .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                .Select(e => new { e.LeadSourceId, e.LeadSource, e.CreationTime }).ToList();




            var JobsList = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrgId)
                          .Select(e => new { e.Id, InstallationDate = e.InstallationDate.Value.AddHours(diffHour).Date, FirstDepositDate = e.FirstDepositDate.Value.Date, e.SystemCapacity, e.LeadFk.LeadSourceId, e.LeadFk.LeadSource, e.State, e.LeadFk.AssignToUserID, e.LeadFk.Area, e.BatteryKw }).ToList();

            var Jobs = JobsList.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
                .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                 .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
                .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
                //.WhereIf(!string.IsNullOrEmpty(input.AreaFilter), e => e.Area == input.AreaFilter)
                .Select(e => new { e.Id, e.InstallationDate, e.FirstDepositDate, e.SystemCapacity, e.LeadSourceId, e.LeadSource, BatteryKw = e.BatteryKw > 0 ? e.BatteryKw : 0 }).ToList();

            var LeadSource = Jobs.Where(e => e.LeadSourceId > 0).Select(e => e.LeadSourceId).Distinct().ToList();

            var ListbettryKw = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5 && j.JobFk.LeadFk.OrganizationId == input.OrgId && j.JobFk.FirstDepositDate.Value.Year > 1)
                .Select(e => new { InstallationDate = e.JobFk.InstallationDate.Value.AddHours(diffHour).Date, FirstDepositDate = e.JobFk.FirstDepositDate.Value.Date, kw = e.Quantity * e.ProductItemFk.Size, e.JobFk.LeadFk.LeadSourceId, e.JobFk.State, e.JobFk.LeadFk.AssignToUserID, e.JobFk.LeadFk.Area }).ToList();
            var bettryKwList = ListbettryKw.WhereIf(input.Leadsources.Count() > 0, e => input.Leadsources.Contains((int)e.LeadSourceId))
               .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
               .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
               .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
               .WhereIf(input.States.Count() > 0, e => input.States.Contains(e.State))
                .WhereIf(input.AreaFilter.Count() > 0 && input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area) || string.IsNullOrEmpty(e.Area))
               .WhereIf(input.AreaFilter.Count() > 0 && !input.AreaFilter.Contains("NoArea"), e => input.AreaFilter.Contains(e.Area))
               //.WhereIf(!string.IsNullOrEmpty(input.AreaFilter), e => e.Area == input.AreaFilter)
               .Select(e => new { e.InstallationDate, e.FirstDepositDate, e.LeadSourceId, e.kw }).ToList();

            var result = (from o in LeadSource
                          select new GetAllLeadSourceMonthlyComparisonDto()
                          {
                              Id = (int)o,


                              Leadsource = Lead.FirstOrDefault(l => l.LeadSourceId == o)?.LeadSource,

                              compareValues = (from m in input.Months
                                               select new GetMonthsCompareValue()
                                               {
                                                   Month = m,
                                                   TotalLead = Lead.Count(e => e.LeadSourceId == o && e.CreationTime.Year == input.Year && e.CreationTime.Month == m),
                                                   PanelSold_KW = Jobs.Where(j => j.LeadSourceId == o && j.FirstDepositDate.Year == input.Year && j.FirstDepositDate.Month == m)
                                                                    .Sum(e => e.SystemCapacity),

                                                   BatterySold_KW = bettryKwList.Where(j => j.LeadSourceId == o && j.FirstDepositDate.Year == input.Year && j.FirstDepositDate.Month == m).Sum(e => e.kw),

                                                   Installed_KW = Jobs.Where(j => j.LeadSourceId == o && j.InstallationDate.Year == input.Year && j.InstallationDate.Month == m).Sum(e => e.SystemCapacity)
                                                                    + bettryKwList.Where(j => j.LeadSourceId == o && j.InstallationDate.Year == input.Year && j.InstallationDate.Month == m).Sum(e => e.kw),

                                                  //pend = leadexpence.Where(e => (e.LeadSourceId == o) && (e.FromDate.Date >= (new DateTime(input.Year, m, 1)).Date && e.ToDate.Date <= (new DateTime(input.Year, m, 1)).AddMonths(1).AddDays(-1).Date) || (e.FromDate.Date <= (new DateTime(input.Year, m, 1)).Date && e.ToDate.Date >= (new DateTime(input.Year, m, 1)).AddMonths(1).AddDays(-1).Date)).Sum(e => e.Amount)
                                               }).ToList()



                          }).ToList();

            var cnt = 5 * input.Months.Count;

            List<GetAllLeadSourceMonthlyComparisonDto> leadlistRemove = new List<GetAllLeadSourceMonthlyComparisonDto>();
            foreach (var resultItem in result)
            {
                var valid = 0;

                foreach(var monthItm in resultItem.compareValues)
                {

                    var SDate = (new DateTime(input.Year, monthItm.Month, 1));
                    var EDate = (new DateTime(input.Year, monthItm.Month, 1)).AddMonths(1).AddDays(-1);
                    var expence = leadexpence
                    .Where(e => (e.FromDate.Date >= SDate.Date && e.ToDate.Date <= EDate.Date) || (e.FromDate.Date <= SDate.Date && e.ToDate.Date >= EDate.Date))
                    .Where(e => e.LeadSourceId == resultItem.Id);
                    //var expence = leadexpence.Where(e => (e.FromDate.Date >= SDate.Value.Date && e.ToDate.Date <= EDate.Value.Date) || (e.FromDate.Date <= SDate.Value.Date && e.ToDate.Date >= EDate.Value.Date));

                    var dates = new List<DateTime>();

                    for (var dt = SDate.Date; dt <= EDate.Date; dt = dt.AddDays(1))
                    {
                        dates.Add(dt);
                    }

                    monthItm.Spend = 0;
                    foreach (var e in expence)
                    {
                        var eday = (e.ToDate.Date - e.FromDate.Date).Days + 1;
                        var perdayAmount = e.Amount / eday;

                        var daysC = dates.Where(d => d >= e.FromDate.Date && d <= e.ToDate.Date).Count();

                        monthItm.Spend = monthItm.Spend + (perdayAmount * daysC);

                    }

                    if(!(monthItm.TotalLead > 0))
                    {
                        valid++;
                    } 
                    if(!(monthItm.Installed_KW > 0))
                    {
                        valid++;
                    }
                    if (!(monthItm.BatterySold_KW > 0))
                    {
                        valid++;
                    }
                    if (!(monthItm.PanelSold_KW > 0))
                    {
                        valid++;
                    }
                    if (!(monthItm.Spend > 0))
                    {
                        valid++;
                    }
                }

                if(valid == cnt)
                {
                    leadlistRemove.Add(resultItem);
                }
                
            }


            result.RemoveAll(r => leadlistRemove.Any(a => a == r));


            var totalCount = result.Count();

            var pagedAndFilteredJobs = result.AsQueryable()
                .OrderBy(input.Sorting ?? "Leadsource desc")
                .PageBy(input);

            var FilteredJobs = pagedAndFilteredJobs.ToList();

            return new PagedResultDto<GetAllLeadSourceMonthlyComparisonDto>(totalCount, FilteredJobs);
        }



    }
}
    

