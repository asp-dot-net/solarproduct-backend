﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.TheSolarDemo;
//using System.Globalization;
using Abp.Timing.Timezone;
using TheSolarProduct.Timing;
using TheSolarProduct.Reports.ProductSolds.Dtos;
using Abp.Collections.Extensions;
using TheSolarProduct.Reports.ProductSolds.Exporting;
using Hangfire.Common;
using TheSolarProduct.Reports.SmsCountReports.Dtos;
using TheSolarProduct.LeadActivityLogs;
using System.Text.RegularExpressions;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.SmsAmounts;
using TheSolarProduct.Sections;
using TheSolarProduct.Jobs.Dtos.Pylon;

namespace TheSolarProduct.Reports.SmsCountReports
{
    public class SmsCountReportAppService : TheSolarProductAppServiceBase, ISmsCountReportAppService
    {
        private readonly IRepository<LeadActivityLog> _leadactivityRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IProductSoldExcelExport _productSoldExcelExport;
        private readonly IRepository<SmsAmount> _smsAmountRepository;
        private readonly IRepository<Section> _sectionRepository;

        public SmsCountReportAppService(
            IRepository<LeadActivityLog> leadactivityRepository,
            ITimeZoneConverter timeZoneConverter,
            ITimeZoneService timeZoneService,
            UserManager userManager,
            IRepository<UserTeam> userTeamRepository,
            IRepository<User, long> userRepository,
            IProductSoldExcelExport productSoldExcelExport,
            IRepository<SmsAmount> smsAmountRepository,
            IRepository<Section> sectionRepository)
        {
            _leadactivityRepository = leadactivityRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _userRepository = userRepository;
            _productSoldExcelExport = productSoldExcelExport;
            _smsAmountRepository = smsAmountRepository;
            _sectionRepository = sectionRepository;
        }

        public async Task<PagedResultDto<GetAllSmsCountDto>> GetAll(GetAllSmsCountInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });


            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var smsamounts = await _smsAmountRepository.GetAll().Select(e => e.Amount).FirstOrDefaultAsync();

            var Filtered = _leadactivityRepository.GetAll().Include(e => e.LeadFk)
                              .Where(e => (e.ActionId == 6 || e.ActionId == 21)  && e.LeadFk.OrganizationId == input.OrganizationUnitId)
                              .WhereIf(role.Contains("Admin"), e => e.CreatorUserId != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.CreatorUserId))
                              .WhereIf(role.Contains("Sales Rep"), e => e.CreatorUserId == User.Id)
                              .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                              .WhereIf(input.UserId != null && input.UserId != 0, e => e.CreatorUserId == input.UserId)
                              .AsNoTracking()
                              .GroupBy(e=>e.CreatorUserId)
                              .Select(e=> new {uid = e.Key, smsCount= e.Count(), creditCount = e.Sum(s=> (int)(Math.Ceiling((decimal)s.Body.Length / 160))), amount = e.Sum(s => Math.Ceiling((decimal)s.Body.Length/ 160)) * (smsamounts != null ? smsamounts : 0) });
           
            SummarySmsCount summary = new SummarySmsCount();
            summary.TotalSmsCount = Filtered.Sum(e => e.smsCount);
            summary.TotalCreditCount = Filtered.Sum(e => e.creditCount);
            summary.TotalAmount = Filtered.Sum(e => e.amount);

            var pagedAndFiltered = Filtered
                 .OrderBy(input.Sorting ?? "amount desc")
                 .PageBy(input).ToList();

            var result = from u in pagedAndFiltered
                         select new GetAllSmsCountDto
                         {
                             Id = (int)u.uid,
                             userName = _userRepository.Get((long)u.uid).FullName,
                             SmsCount = u.smsCount,
                             CreditCount = u.creditCount,
                             Amount = u.amount,
                             SummarySmsCount = summary
                         };


            var totalcount = Filtered.Count();


            return new PagedResultDto<GetAllSmsCountDto>(totalcount, result.ToList());
        }

        public async Task<FileDto> GetSmsCountToExcel(GetAllExcelSmsCountInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });


            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var smsamounts = await _smsAmountRepository.GetAll().Select(e => e.Amount).FirstOrDefaultAsync();
            var Filtered = _leadactivityRepository.GetAll().Include(e => e.LeadFk)
                              .Where(e => (e.ActionId == 6 || e.ActionId == 21) && e.LeadFk.OrganizationId == input.OrganizationUnitId)
                              .WhereIf(role.Contains("Admin"), e => e.CreatorUserId != null)
                              .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.CreatorUserId))
                              .WhereIf(role.Contains("Sales Rep"), e => e.CreatorUserId == User.Id)
                              .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                              .WhereIf(input.UserId != null && input.UserId != 0, e => e.CreatorUserId == input.UserId)
                              .AsNoTracking()
                              .GroupBy(e => e.CreatorUserId)
                              .Select(e => new { uid = e.Key, smsCount = e.Count(), creditCount = e.Sum(s => s.Body.Length) / 160, amount = e.Sum(s => s.Body.Length) / 160 * (smsamounts != null ? smsamounts : 0) }).ToList();

            var result = from u in Filtered
                         select new GetAllSmsCountDto
                         {
                             Id = (int)u.uid,
                             userName = _userRepository.Get((long)u.uid).FullName,
                             SmsCount = u.smsCount,
                             CreditCount = u.creditCount,
                             Amount = u.amount
                         };
            return _productSoldExcelExport.SmsCountExportToFile(result.ToList(),input.ExcelOrCSV);

        }

        public async Task<List<GetAllSmsCountSectionWiseDto>> GetAllSectionWise(GetAllExcelSmsCountInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);
            //var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });


            //var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var smsamounts = await _smsAmountRepository.GetAll().Select(e => e.Amount).FirstOrDefaultAsync();

            var Filtered = _leadactivityRepository.GetAll().Include(e => e.LeadFk)
                              .Where(e => (e.ActionId == 6 || e.ActionId == 21) && e.LeadFk.OrganizationId == input.OrganizationUnitId)
                              //.WhereIf(role.Contains("Admin"), e => e.CreatorUserId != null)
                              //.WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.CreatorUserId))
                              //.WhereIf(role.Contains("Sales Rep"), e => e.CreatorUserId == User.Id)
                              .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                              .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                              .WhereIf(input.UserId != null && input.UserId != 0, e => e.CreatorUserId == input.UserId)
                              .AsNoTracking()
                              .GroupBy(e => e.SectionId)
                              .Select(e => new { id = e.Key, smsCount = e.Count(), creditCount = e.Sum(s => s.Body.Length) / 160, amount = e.Sum(s => s.Body.Length) / 160 * (smsamounts != null ? smsamounts : 0) });


            var result = from u in Filtered
                         select new GetAllSmsCountSectionWiseDto
                         {
                             Id = (int)u.id,
                             Section = u.id > 0 ? _sectionRepository.GetAll().Where(e => e.SectionId == u.id).Select(e => e.SectionName).FirstOrDefault() : "",
                             SmsCount = u.smsCount,
                             CreditCount = u.creditCount,
                             Amount = u.amount
                         };

            return result.ToList();
        }
    }
}
