﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Invoices.Dtos;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.PickList.Dtos;
using TheSolarProduct.Quotations.Dtos;

namespace TheSolarProduct.Reports
{
	public interface IReportAppService : IApplicationService
	{
		//string Quotation(int JobId, int? QuoteId, string ImagePath, string QuoteNo, int OrgId);

		//Task<string> Refund(int JobId, int OrgId, int Id);

		//string TaxInvoice(int JobId, int OrgId, bool? invoiceimpotorpayment);

		//string PaymentReceipt(int JobId, int OrgId, bool? invoiceimpotorpayment);

		//Task<string> PickList(int JobId, int OrgId, int PicklistId);

		//string Acknowledgement(int DocId);

		Task<TexInvoiceDto> TaxInvoiceByJobId(int JobId);

		Task<PaymentReceiptDto> PaymentReceiptByJobId(int JobId);

		Task<GetPicklistDataDto> PicklistByPickListId(int JobId, int PicklistId);

		Task<GetAcknoledgeementDto> GetAcknowledgementById(int Id);

		Task<RefundFormDto> GetRefundByJobId(int Id);

		Task<ReferralFormDto> GetReffarelByJobId(int Id);

    }
}
