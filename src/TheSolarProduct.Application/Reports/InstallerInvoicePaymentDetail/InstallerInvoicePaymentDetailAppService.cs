﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.InstallationCost.InstallationItemLists;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Timing;
using System.Linq;
using System.Linq.Dynamic.Core;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using TheSolarProduct.Reports.JobCost.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.JobCost.Exporting;
using Abp.EntityFrameworkCore;
using System.Data;
using Abp.Data;
using Abp.MultiTenancy;
using System.Data.Common;
using Abp.Authorization;
using TheSolarProduct.CategoryInstallations;
using Abp.Collections.Extensions;
using TheSolarProduct.InstallationCost.PostCodeCost;
using Abp.Organizations;
using TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos;
using Hangfire.Common;
using Hangfire.Storage.Monitoring;
using NPOI.SS.Formula.Functions;
using Microsoft.AspNetCore.Identity;
using TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Exporting;
using Microsoft.AspNetCore.Mvc;

namespace TheSolarProduct.Reports.InstallerInvoicePaymentDetail
{
    public class InstallerInvoicePaymentDetailAppService : TheSolarProductAppServiceBase, IInstallerInvoicePaymentDetailAppService
    {
        private readonly IRepository<InstallerInvoicePriceList> _installerInvoicePriceListServiceProxy;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly InstallerInvoicePaymentDetailExporter _installerInvoicePaymentDetailExporter;
        private readonly IRepository<InstallerInvoice> _jobInstallerInvoiceRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        public InstallerInvoicePaymentDetailAppService(
            IRepository<InstallerInvoicePriceList> installerInvoicePriceListServiceProxy
            , ITimeZoneConverter timeZoneConverter
            , ITimeZoneService timeZoneService
            , IRepository<User, long> userRepository
            , UserManager userManager
            , IRepository<UserTeam> userTeamRepository
            , InstallerInvoicePaymentDetailExporter installerInvoicePaymentDetailExporter
            , IRepository<InstallerInvoice> jobInstallerInvoiceRepository
            , IRepository<JobProductItem> jobProductItemRepository
            )
        {
            _installerInvoicePriceListServiceProxy = installerInvoicePriceListServiceProxy;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _userRepository = userRepository;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _installerInvoicePaymentDetailExporter = installerInvoicePaymentDetailExporter;
            _jobInstallerInvoiceRepository = jobInstallerInvoiceRepository;
            _jobProductItemRepository = jobProductItemRepository;
        }

        public async Task<PagedResultDto<InstallerInvoicePaymentDetailDto>> GetAll(InstallerInvoicePaymentDetailInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var InvoicePriceList = await _installerInvoicePriceListServiceProxy.GetAll().Include(e => e.PriceItemListFK)
                 .Include(e => e.InstallerInvoiceFK)
                 .Include(e => e.InstallerInvoiceFK.JobFk)
                 .Where(e => e.InstallerInvoiceFK.JobFk.LeadFk.OrganizationId == input.orgId)
                 .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.InstallerInvoiceFK.JobFk.JobNumber == input.filter)
                 .WhereIf(!string.IsNullOrEmpty(input.Areafilter), e => e.InstallerInvoiceFK.JobFk.LeadFk.Area == input.Areafilter)
                 .WhereIf(input.InstallerId > 0, e => e.InstallerInvoiceFK.InstallerId == input.InstallerId)
                .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .WhereIf(role.Contains("Admin"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID == User.Id)
                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.InstallerInvoiceFK.Installation_Maintenance_Inspection == input.InvoiceType)
                 .Select(e => new { e.PriceItemId, e.PriceItemListFK.Name, e.InstallerInvoiceFK.JobFk.State, e.Amount, invAmount = e.InstallerInvoiceFK.Amount, e.InstallerInvoId }).ToListAsync();
                 ;
            var installerInvoicePriceList = InvoicePriceList.GroupBy(e => new { e.PriceItemId, e.Name, e.State })
                 .Select(e => new { e.Key.PriceItemId, e.Key.Name, e.Key.State, Amount = e.Sum(s => s.Amount) }).ToList();

            var priceitem = installerInvoicePriceList.GroupBy(e => new { e.PriceItemId, e.Name }).Select(e => e.Key);

            var result = from o in priceitem
                         select new InstallerInvoicePaymentDetailDto
                         {
                             Price = o.Name,
                             NSW = installerInvoicePriceList.Where(e => e.PriceItemId == o.PriceItemId && e.State == "NSW").Sum(s => s.Amount),
                             SA = installerInvoicePriceList.Where(e => e.PriceItemId == o.PriceItemId && e.State == "SA").Sum(s => s.Amount),
                             VIC = installerInvoicePriceList.Where(e => e.PriceItemId == o.PriceItemId && e.State == "VIC").Sum(s => s.Amount),
                             QLD = installerInvoicePriceList.Where(e => e.PriceItemId == o.PriceItemId && e.State == "QLD").Sum(s => s.Amount),
                         };

            var output = result.Where(e => e.NSW > 0 || e.SA > 0 || e.VIC > 0 || e.QLD > 0).ToList();

            var totalCount = output.Count();

            var pagedAndFilteredJobs = output.AsQueryable()
                .OrderBy(input.Sorting ?? "Price")
                .PageBy(input);

            var FilteredJobs = pagedAndFilteredJobs.ToList();

            if (FilteredJobs.Count() > 0)
            {
                FilteredJobs[0].detailSummary = new InstallerInvoicePaymentDetailSummary();
                //var SummaryList = InvoicePriceList.GroupBy(e => new { e.InstallerInvoId, e.State }).Select(e => new {e.Key.State, invAmount = e.Sum(m => m.Amount)}).ToList();

                FilteredJobs[0].detailSummary.SA = output.Sum(e => e.SA);
                FilteredJobs[0].detailSummary.QLD = output.Sum(e => e.QLD);
                FilteredJobs[0].detailSummary.VIC = output.Sum(e => e.VIC);
                FilteredJobs[0].detailSummary.NSW = output.Sum(e => e.NSW);

                //FilteredJobs[0].detailSummary.SA = SummaryList.Where(e => e.State == "SA").Select(e => e.invAmount).Sum();
                //FilteredJobs[0].detailSummary.QLD = SummaryList.Where(e => e.State == "QLD").Select(e => e.invAmount).Sum();
                //FilteredJobs[0].detailSummary.VIC = SummaryList.Where(e => e.State == "VIC").Select(e => e.invAmount).Sum();
                //FilteredJobs[0].detailSummary.NSW = SummaryList.Where(e => e.State == "NSW").Select(e => e.invAmount).Sum();
                FilteredJobs[0].detailSummary.total = FilteredJobs[0].detailSummary.SA + FilteredJobs[0].detailSummary.QLD + FilteredJobs[0].detailSummary.VIC + FilteredJobs[0].detailSummary.NSW;
            }


            return new PagedResultDto<InstallerInvoicePaymentDetailDto>(totalCount, FilteredJobs);

        }


        public async Task<FileDto> GetInstallerInvoicePaymentExport(InstallerInvoicePaymentDetailInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var installerInvoicePriceList = await _installerInvoicePriceListServiceProxy.GetAll().Include(e => e.PriceItemListFK)
                 .Include(e => e.InstallerInvoiceFK)
                 .Include(e => e.InstallerInvoiceFK.JobFk)
                 .Where(e => e.InstallerInvoiceFK.JobFk.LeadFk.OrganizationId == input.orgId)
                 .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.InstallerInvoiceFK.JobFk.JobNumber == input.filter)
                 .WhereIf(!string.IsNullOrEmpty(input.Areafilter), e => e.InstallerInvoiceFK.JobFk.LeadFk.Area == input.Areafilter)
                 .WhereIf(input.InstallerId > 0, e => e.InstallerInvoiceFK.InstallerId == input.InstallerId)
                .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .WhereIf(role.Contains("Admin"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID == User.Id)
                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.InstallerInvoiceFK.Installation_Maintenance_Inspection == input.InvoiceType)
                 .GroupBy(e => new { e.PriceItemId, e.PriceItemListFK.Name, e.InstallerInvoiceFK.JobFk.State })
                 .Select(e => new { e.Key.PriceItemId, e.Key.Name, e.Key.State, Amount = e.Sum(s => s.Amount) }).ToListAsync();

            var priceitem = installerInvoicePriceList.GroupBy(e => new { e.PriceItemId, e.Name }).Select(e => e.Key);

            var result = from o in priceitem
                         select new InstallerInvoicePaymentDetailDto
                         {
                             Price = o.Name,
                             NSW = installerInvoicePriceList.Where(e => e.PriceItemId == o.PriceItemId && e.State == "NSW").Sum(s => s.Amount),
                             SA = installerInvoicePriceList.Where(e => e.PriceItemId == o.PriceItemId && e.State == "SA").Sum(s => s.Amount),
                             VIC = installerInvoicePriceList.Where(e => e.PriceItemId == o.PriceItemId && e.State == "VIC").Sum(s => s.Amount),
                             QLD = installerInvoicePriceList.Where(e => e.PriceItemId == o.PriceItemId && e.State == "QLD").Sum(s => s.Amount),
                         };

            var output = result.Where(e => e.NSW > 0 || e.SA > 0 || e.VIC > 0 || e.QLD > 0).ToList();

            return _installerInvoicePaymentDetailExporter.ExportToFile(output, "InstallerInvoicePayment.xlsx");
        }


        public async Task<PagedResultDto<GetAllInstallerPaymentInstallerWiseDto>> GetAllInstallerPaymentInstallerWise(GetInstallerPaymentInstallerWiseInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var installerInvoicePriceList = _installerInvoicePriceListServiceProxy.GetAll().Include(e => e.PriceItemListFK)
                 .Include(e => e.InstallerInvoiceFK)
                 .Include(e => e.InstallerInvoiceFK.JobFk)
                 .Include(e => e.InstallerInvoiceFK.JobFk.LeadFk)
                 //.Where(e => e.InstallerInvoiceFK.JobFk.LeadFk.OrganizationId == input.orgId)
                 //.WhereIf(!string.IsNullOrEmpty(input.filter), e => e.InstallerInvoiceFK.JobFk.JobNumber == input.filter)
                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.InstallerInvoiceFK.Installation_Maintenance_Inspection == input.InvoiceType)
                //.WhereIf(input.InstallerId > 0, e => e.InstallerInvoiceFK.InstallerId == input.InstallerId)
                .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.InstallerInvoiceFK.JobFk.LeadFk.State == input.State)
                .WhereIf(role.Contains("Admin"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID == User.Id).ToList()
                .Where(e => e.InstallerInvoiceFK.InstallerId > 0)
                 //.GroupBy(e => new { e.InstallerInvoiceFK.InstallerId, e.InstallerInvoiceFK.JobFk.LeadFk.Area })
                 .GroupBy(e => new { e.InstallerInvoiceFK, e.InstallerInvoiceFK.JobId, e.InstallerInvoiceFK.JobFk.LeadFk.Area })
                 .Select(e => new { e.Key.InstallerInvoiceFK.InstallerId, e.Key.InstallerInvoiceFK.JobId, e.Key.Area, Amount = e.Key.InstallerInvoiceFK.ApproedAmount, KW = e.Key.InstallerInvoiceFK.JobFk.SystemCapacity });

            var priceitem = installerInvoicePriceList.GroupBy(e => new { e.InstallerId }).Select(e => e.Key);

            var KWList = installerInvoicePriceList.GroupBy(e => new { e.InstallerId, e.JobId, e.Area, e.KW }).Select(e => new { e.Key.InstallerId, e.Key.Area, e.Key.KW });
            var jobids = installerInvoicePriceList.Select(e => e.JobId).ToList();

            var batteryKwList = await _jobProductItemRepository.GetAll()
              .Where(j => j.ProductItemFk.ProductTypeId == 5 && jobids.Contains(j.JobId))
              .Select(e => new { kw = e.Quantity * e.ProductItemFk.Size, e.JobFk.InstallerId, e.JobFk.LeadFk.Area })
              .ToListAsync();

            var result = from o in priceitem
                         select new GetAllInstallerPaymentInstallerWiseDto
                         {
                             Installer = _userRepository.Get((long)o.InstallerId).FullName,
                             installerId = o.InstallerId,
                            
                             MetroKW = KWList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.KW) + batteryKwList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.kw),
                             MetroPrice = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.Amount),
                             
                             MetroPricePerKW =( KWList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.KW)) > 0 && (installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.Amount)) > 0 ?
                                               ( installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.Amount))
                                               / (KWList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.KW) +
                                               (batteryKwList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.kw))) : 0,



                             RegionalKW = KWList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.KW) + batteryKwList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.kw),
                             RegionalPrice = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.Amount),
                             
                             RegionalPricePerKW = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.Amount) > 0 && KWList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.KW) > 0 ?
                                                    installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.Amount) / (KWList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.KW) +
                                                    batteryKwList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.kw)) : 0


                         };

            var output = result.Where(e => e.MetroPrice > 0 || e.MetroKW > 0 || e.RegionalKW > 0 || e.RegionalPrice > 0).ToList();

            var totalCount = output.Count();

            var pagedAndFilteredJobs = output.AsQueryable()
                .OrderBy(input.Sorting ?? "Installer")
                .PageBy(input);

            var FilteredJobs = pagedAndFilteredJobs.ToList();

            if (FilteredJobs.Count() > 0)
            {
                FilteredJobs[0].Summary = new InstallerPaymentInstallerWiseSummary();
                FilteredJobs[0].Summary.MetroKW = output.Sum(e => e.MetroKW);
                FilteredJobs[0].Summary.MetroPrice = output.Sum(e => e.MetroPrice);
                FilteredJobs[0].Summary.MetroPricePerKW = output.Sum(e => e.MetroPricePerKW) > 0 ? output.Sum(e => e.MetroPrice)/output.Sum(e => e.MetroKW) : 0;
                FilteredJobs[0].Summary.RegionalKW = output.Sum(e => e.RegionalKW);
                FilteredJobs[0].Summary.RegionalPrice = output.Sum(e => e.RegionalPrice);
                FilteredJobs[0].Summary.RegionalPricePerKW = output.Sum(e => e.RegionalKW) > 0 ? output.Sum(e => e.RegionalPrice) / output.Sum(e => e.RegionalKW) : 0;
            }

            return new PagedResultDto<GetAllInstallerPaymentInstallerWiseDto>(totalCount, FilteredJobs);
        }

        public async Task<FileDto> GetInstallerPaymentInstallerWiseExport(GetInstallerPaymentInstallerWiseInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var installerInvoicePriceList = _installerInvoicePriceListServiceProxy.GetAll().Include(e => e.PriceItemListFK)
                 .Include(e => e.InstallerInvoiceFK)
                 .Include(e => e.InstallerInvoiceFK.JobFk)
                 .Include(e => e.InstallerInvoiceFK.JobFk.LeadFk)
                 //.Where(e => e.InstallerInvoiceFK.JobFk.LeadFk.OrganizationId == input.orgId)
                 //.WhereIf(!string.IsNullOrEmpty(input.filter), e => e.InstallerInvoiceFK.JobFk.JobNumber == input.filter)
                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.InstallerInvoiceFK.Installation_Maintenance_Inspection == input.InvoiceType)
                //.WhereIf(input.InstallerId > 0, e => e.InstallerInvoiceFK.InstallerId == input.InstallerId)
                .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .WhereIf(role.Contains("Admin"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID == User.Id).ToList()

                 .GroupBy(e => new { e.InstallerInvoiceFK.InstallerId, e.InstallerInvoiceFK.JobFk.LeadFk.Area })
                 .Select(e => new { e.Key.InstallerId, e.Key.Area, Amount = e.Sum(s => s.Amount), KW = e.Sum(s => s.InstallerInvoiceFK.JobFk.SystemCapacity) });

            var priceitem = installerInvoicePriceList.GroupBy(e => new { e.InstallerId }).Select(e => e.Key);

            var batteryKwList = await _jobProductItemRepository.GetAll()
               .Where(j => j.ProductItemFk.ProductTypeId == 5)
               .Select(e => new { kw = e.Quantity * e.ProductItemFk.Size, e.JobFk.InstallerId, e.JobFk.LeadFk.Area })
               .ToListAsync();

            var result = from o in priceitem
                         select new GetAllInstallerPaymentInstallerWiseDto
                         {
                             Installer = _userRepository.Get((long)o.InstallerId).FullName,
                             MetroKW = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.KW) + batteryKwList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s =>s.kw),
                             MetroPrice = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.Amount),
                             MetroPricePerKW = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.KW) > 0 && installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.Amount) > 0 ?
                                                installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.Amount) / installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.KW) + batteryKwList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Metro").Sum(s => s.kw) : 0,

                             RegionalKW = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.KW) + batteryKwList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.kw),
                             RegionalPrice = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.Amount),
                             RegionalPricePerKW = installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.Amount) > 0 && installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.KW) > 0 ?
                                                    installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.Amount) / installerInvoicePriceList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.KW) +batteryKwList.Where(e => e.InstallerId == o.InstallerId && e.Area == "Regional").Sum(s => s.kw) : 0

                         };

            var output = result.Where(e => e.MetroPrice > 0 || e.MetroKW > 0 || e.RegionalKW > 0 || e.RegionalPrice > 0).ToList();

            return _installerInvoicePaymentDetailExporter.InstallerWiseExportToFile(output, "InstallerWiseInvoicePayment.xlsx");
        }

        public async Task<PagedResultDto<GetAllInstallerPaymentInstallerJobWiseDto>> GetAllInstallerPaymentInstallerJobWise(GetInstallerPaymentInstallerWiseInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var installerInvoicePriceList = _installerInvoicePriceListServiceProxy.GetAll().Include(e => e.PriceItemListFK)
                 .Include(e => e.InstallerInvoiceFK)
                 .Include(e => e.InstallerInvoiceFK.JobFk)
                 .Include(e => e.InstallerInvoiceFK.JobFk.LeadFk)

                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.InstallerInvoiceFK.Installation_Maintenance_Inspection == input.InvoiceType)
                //.WhereIf(input.InstallerId > 0, e => e.InstallerInvoiceFK.InstallerId == input.InstallerId)
                .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .WhereIf(role.Contains("Admin"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID == User.Id).ToList()
                .Where(e => e.InstallerInvoiceFK.InstallerId == input.InstallerId)
                 //.GroupBy(e => new { e.InstallerInvoiceFK.InstallerId, e.InstallerInvoiceFK.JobFk.LeadFk.Area })
                 //.GroupBy(e => new { e.InstallerInvoiceFK, e.InstallerInvoiceFK.JobId })
                 .Select(e => new { e.InstallerInvoiceFK.InstallerId, e.InstallerInvoiceFK.JobId, e.InstallerInvoiceFK.JobFk.JobNumber, Amount = e.InstallerInvoiceFK.ApproedAmount, KW = e.InstallerInvoiceFK.JobFk.SystemCapacity });

            var priceitem = installerInvoicePriceList.GroupBy(e => new { e.JobId, e.JobNumber, e.Amount, e.KW,e.InstallerId });

            var ListbettryKw = _jobProductItemRepository.GetAll().Where(j => j.ProductItemFk.ProductTypeId == 5)
               .Select(e => new { kw = e.Quantity * e.ProductItemFk.Size, e.JobId, e.JobFk.LeadFk.Area }).ToList();

            var result = from o in priceitem
                         select new GetAllInstallerPaymentInstallerJobWiseDto
                         {
                             Installer = _userRepository.Get((long)o.Key.InstallerId).FullName,
                             installerId = o.Key.InstallerId,
                             JobNumber = o.Key.JobNumber,
                             JobPrice = o.Key.Amount,
                             priceKw = o.Key.KW + ListbettryKw.Where(e => e.JobId == o.Key.JobId ).Sum(s => s.kw) ,
                             priceperkw = o.Key.KW > 0 && o.Key.Amount > 0 ? o.Key.Amount/o.Key.KW + ListbettryKw.Where(e => e.JobId == o.Key.JobId).Sum(s => s.kw) : 0
                         };
           
            var output = result.ToList();

            var totalCount = output.Count();

            var pagedAndFilteredJobs = output.AsQueryable()

                .PageBy(input);

            var FilteredJobs = pagedAndFilteredJobs.ToList();



            return new PagedResultDto<GetAllInstallerPaymentInstallerJobWiseDto>(totalCount, FilteredJobs);
        }

        public async Task<List<JobNumberAmountDto>> GetAllInstallerPaymentJobNumberWise(InstallerPaymentJobNumberWiseInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var InvoicePriceList = await _installerInvoicePriceListServiceProxy.GetAll().Include(e => e.PriceItemListFK)
                 .Include(e => e.InstallerInvoiceFK)
                 .Include(e => e.InstallerInvoiceFK.JobFk)
                 .Where(e => e.InstallerInvoiceFK.JobFk.LeadFk.OrganizationId == input.orgId)
                 .WhereIf(!string.IsNullOrEmpty(input.filter), e => e.InstallerInvoiceFK.JobFk.JobNumber == input.filter)
                 .WhereIf(!string.IsNullOrEmpty(input.Areafilter), e => e.InstallerInvoiceFK.JobFk.LeadFk.Area == input.Areafilter)
                 .WhereIf(input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                 .WhereIf(input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                 .WhereIf(role.Contains("Admin"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID != null)
                 .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID))
                 .WhereIf(role.Contains("Sales Rep"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID == User.Id)
                 .WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.InstallerInvoiceFK.Installation_Maintenance_Inspection == input.InvoiceType)
                 .Where(e => e.InstallerInvoiceFK.JobFk.State == input.state) 
                 .Select(e => new { e.InstallerInvoiceFK.JobFk.JobNumber, e.Amount })
                 .ToListAsync();
            var installerInvoicePriceList = InvoicePriceList.GroupBy(e => e.JobNumber).Select(g => new{JobNumber = g.Key, Amount = g.Sum(e => e.Amount)}).ToList();
            var result = installerInvoicePriceList
                         .Select(e => new JobNumberAmountDto
                         {
                             JobNumber = e.JobNumber,
                             Amount = e.Amount
                         })
                         .ToList();

            return result;
        }
        public async Task<List<InstallerinvoiceInstallerWiseSummary>> GetAllInstallerInvoiceInstallerWise(int InstallerId, DateTime StartDate,DateTime EndDate,String AreaType)
        {
            var SDate = (_timeZoneConverter.Convert(StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var installerInvoicePriceList = _installerInvoicePriceListServiceProxy.GetAll().Include(e => e.PriceItemListFK)
                 .Include(e => e.InstallerInvoiceFK)
                 .Include(e => e.InstallerInvoiceFK.JobFk)
                 .Include(e => e.InstallerInvoiceFK.JobFk.LeadFk)
                 //.Where(e => e.InstallerInvoiceFK.JobFk.LeadFk.OrganizationId == input.orgId)
                 //.WhereIf(!string.IsNullOrEmpty(input.filter), e => e.InstallerInvoiceFK.JobFk.JobNumber == input.filter)
                 //.WhereIf(!string.IsNullOrEmpty(input.InvoiceType), e => e.InstallerInvoiceFK.Installation_Maintenance_Inspection == input.InvoiceType)
                //.WhereIf(input.InstallerId > 0, e => e.InstallerInvoiceFK.InstallerId == input.InstallerId)
                .WhereIf(StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                .WhereIf(EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                .WhereIf(role.Contains("Admin"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID != null)
                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID))
                .WhereIf(role.Contains("Sales Rep"), e => e.InstallerInvoiceFK.JobFk.LeadFk.AssignToUserID == User.Id).ToList()
                .Where(e => e.InstallerInvoiceFK.InstallerId == InstallerId && e.InstallerInvoiceFK.JobFk.LeadFk.Area == AreaType)
                 //.GroupBy(e => new { e.InstallerInvoiceFK.InstallerId, e.InstallerInvoiceFK.JobFk.LeadFk.Area })
                 .GroupBy(e => new { e.InstallerInvoiceFK, e.InstallerInvoiceFK.JobId,e.InstallerInvoiceFK.JobFk.LeadFk.Area })
                 .Select(e => new { e.Key.InstallerInvoiceFK.InstallerId, e.Key.InstallerInvoiceFK.JobId, e.Key.Area,jobNumber=e.Key.InstallerInvoiceFK.JobFk.JobNumber, Amount = e.Key.InstallerInvoiceFK.ApproedAmount, KW = e.Key.InstallerInvoiceFK.JobFk.SystemCapacity });



            var result = installerInvoicePriceList.Select(o => new InstallerinvoiceInstallerWiseSummary
            {
                installerId = o.InstallerId,
                Installer=  _userRepository.Get((long)o.InstallerId).FullName,
                KW = o.KW,
                InvoiceAmt = o.Amount,
                JobNumber = o.jobNumber,
            }).ToList();




            //var FilteredJobs = result.ToList();


            return result;
        }

    }
}
