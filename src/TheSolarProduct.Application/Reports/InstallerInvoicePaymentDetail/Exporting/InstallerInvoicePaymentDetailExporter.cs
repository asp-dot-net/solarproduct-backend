﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Dtos;
using TheSolarProduct.Reports.JobCost.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Reports.InstallerInvoicePaymentDetail.Exporting
{
    public class InstallerInvoicePaymentDetailExporter : NpoiExcelExporterBase
    {
        public InstallerInvoicePaymentDetailExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {

        }

        public FileDto ExportToFile(List<InstallerInvoicePaymentDetailDto> data, string filename)
        {
            var Header = new string[] { "PriceType", "NSW", "SA", "VIC", "QLD" };

            return CreateExcelPackage(
                filename,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InstallerPayment"));

                    AddHeader(sheet, Header);

                    AddObjects(
                        sheet, 2, data,
                        _ => _.Price,
                        _ => _.NSW,
                        _ => _.SA,
                        _ => _.VIC,
                        _ => _.QLD
                        );

                    for (var i = 1; i <= data.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[1], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[2], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[3], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0.00");
                    }
                });
        }

        public FileDto InstallerWiseExportToFile(List<GetAllInstallerPaymentInstallerWiseDto> data, string filename)
        {
            var Header = new string[] { "Installer", "Metro Price", "Metro KW", "Metro Price/Kw", "Regional Price", "Regional KW", "Regional Price/KW" };

            return CreateExcelPackage(
                filename,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InstallerWiseInvoicePayment"));

                    AddHeader(sheet, Header);

                    AddObjects(
                        sheet, 2, data,
                        _ => _.Installer,
                        _ => _.MetroPrice,
                        _ => _.MetroKW,
                        _ => _.MetroPricePerKW,
                        _ => _.RegionalPrice,
                        _ => _.RegionalKW,
                        _ => _.RegionalPricePerKW
                        );

                    for (var i = 1; i <= data.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[1], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[2], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[3], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "0.00");
                    }
                });
        }

    }
}
