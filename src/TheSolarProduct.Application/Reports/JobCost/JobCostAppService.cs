﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheSolarProduct.Authorization.Users;
//using TheSolarProduct.InstallationCost.BatteryInstallationCost;
//using TheSolarProduct.InstallationCost.ExtraInstallationCharges;
//using TheSolarProduct.InstallationCost.FixedCostPrices;
using TheSolarProduct.InstallationCost.InstallationItemLists;
//using TheSolarProduct.InstallationCost.StateWiseInstallationCosts;
using TheSolarProduct.InstallationCost.STCCost;
//using TheSolarProduct.InstallationCost.TravelCost;
using TheSolarProduct.Jobs;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.Timing;
using System.Linq;
using System.Linq.Dynamic.Core;
using TheSolarProduct.TheSolarDemo;
//using Microsoft.AspNetCore.Identity;
using TheSolarProduct.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Abp.Linq.Extensions;
using TheSolarProduct.Reports.JobCost.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.JobCost.Exporting;
//using NUglify.Helpers;
using Abp.EntityFrameworkCore;
using System.Data;
using Abp.Data;
using Abp.MultiTenancy;
using System.Data.Common;
//using Microsoft.Data.SqlClient;
//using System.Reflection;
//using NPOI.SS.Formula.Functions;
//using Microsoft.AspNetCore.Mvc;
//using TheSolarProduct.CallHistory.Dtos;
//using System.Windows.Forms;
//using Abp.Extensions;
using Abp.Authorization;
using TheSolarProduct.CategoryInstallations;
using Abp.Collections.Extensions;
using TheSolarProduct.InstallationCost.PostCodeCost;
using Abp.Organizations;
using Hangfire.Storage.Monitoring;
using static TheSolarProduct.SysJobs.LinkData;
using System.Windows.Forms;
using System.Globalization;
using TheSolarProduct.InstallationCost.BatteryInstallationCost;
using TheSolarProduct.InstallationCost.FixedCostPrices;
using TheSolarProduct.Expense;
using TheSolarProduct.InstallationCost.ExtraInstallationCharges;
using NPOI.SS.Formula.Functions;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.StockOrderFors.Dtos;
using TheSolarProduct.Reports.ProgressReport;
using Microsoft.AspNetCore.Components.Forms;
using TheSolarProduct.Reports.ProgressReport.Dtos;

namespace TheSolarProduct.Reports.JobCost
{
    [AbpAuthorize]
    public class JobCostAppService : TheSolarProductAppServiceBase, IJobCostAppService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<User, long> _userRepository;
        //private readonly IRepository<StateWiseInstallationCost> _stateWiseInstallationCostRepository;
        private readonly IRepository<InstallationItemList> _installationItemListRepository;
        private readonly IRepository<FixedCostPrice> _fixedCostPriceRepository;
        private readonly IRepository<ExtraInstallationCharge> _extraInstallationChargeRepository;
        private readonly IRepository<STCCost> _stcCostRepository;
        private readonly IRepository<BatteryInstallationCost> _batteryInstallationCostRepository;
        //private readonly IRepository<TravelCost> _travelCostRepository;

        private readonly ITimeZoneService _timeZoneService;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IJobCostExcelExporter _jobCostExcelExporter;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;
        private readonly IActiveTransactionProvider _transactionProvider;
        //private readonly IRepository<CategoryInstallationItemPeriod> _categoryInstallationItemPeriodRepository;
        private readonly IRepository<CategoryInstallationItemList> _categoryInstallationItemListRepository;
        private readonly IRepository<PostoCodePrice> _postoCodePriceRepository;
        private readonly IRepository<ProductType> _productTypeRepository;
        private readonly IRepository<InstallerInvoice> _installerInvoiceRepository;
        private readonly IRepository<FinanceOption> _financeOptionRepository;
        private readonly IRepository<LeadExpenseInvestment> _leadExpenseInvestmentsRepository;
        private readonly IRepository<JobCostFixExpenseList> _jobCostFixExpenseListRepository;

        public JobCostAppService(IRepository<Job> jobRepository
            , IRepository<User, long> userRepository
            , ITimeZoneService timeZoneService
            //, IRepository<StateWiseInstallationCost> stateWiseInstallationCostRepository
            , IRepository<InstallationItemList> installationItemListRepository
            , IRepository<FixedCostPrice> fixedCostPriceRepository
            , IRepository<ExtraInstallationCharge> extraInstallationChargeRepository
            , IRepository<STCCost> stcCostRepository
            , IRepository<BatteryInstallationCost> batteryInstallationCostRepository
            //, IRepository<TravelCost> travelCostRepository
            , ITimeZoneConverter timeZoneConverter
            , IRepository<UserTeam> userTeamRepository
            , UserManager userManager
            , IRepository<JobProductItem> jobProductItemRepository
            , IRepository<JobVariation> jobVariationRepository
            , IJobCostExcelExporter jobCostExcelExporter
            , IDbContextProvider<TheSolarProductDbContext> dbcontextprovider
            , IActiveTransactionProvider transactionProvider
            //, IRepository<CategoryInstallationItemPeriod> categoryInstallationItemPeriodRepository
            , IRepository<CategoryInstallationItemList> categoryInstallationItemListRepository
            , IRepository<PostoCodePrice> postoCodePriceRepository
            , IRepository<ProductType> productTypeRepository
            , IRepository<InstallerInvoice> installerInvoiceRepository
            , IRepository<FinanceOption> financeOptionRepository
            , IRepository<LeadExpenseInvestment> leadExpenseInvestmentsRepository
            , IRepository<JobCostFixExpenseList> jobCostFixExpenseListRepository
            )
        {
            _jobRepository = jobRepository;
            _userRepository = userRepository;
            _timeZoneService = timeZoneService;
            //_stateWiseInstallationCostRepository = stateWiseInstallationCostRepository;
            _installationItemListRepository = installationItemListRepository;
            _fixedCostPriceRepository = fixedCostPriceRepository;
            _extraInstallationChargeRepository = extraInstallationChargeRepository;
            _stcCostRepository = stcCostRepository;
            _batteryInstallationCostRepository = batteryInstallationCostRepository;
            //_travelCostRepository = travelCostRepository;
            _timeZoneConverter = timeZoneConverter;
            _userTeamRepository = userTeamRepository;
            _userManager = userManager;
            _jobProductItemRepository = jobProductItemRepository;
            _jobVariationRepository = jobVariationRepository;
            _jobCostExcelExporter = jobCostExcelExporter;
            _dbcontextprovider = dbcontextprovider;
            _transactionProvider = transactionProvider;
            _categoryInstallationItemListRepository = categoryInstallationItemListRepository;
            //_categoryInstallationItemPeriodRepository = categoryInstallationItemPeriodRepository;
            _postoCodePriceRepository = postoCodePriceRepository;
            _productTypeRepository = productTypeRepository;
            _installerInvoiceRepository = installerInvoiceRepository;
            _financeOptionRepository = financeOptionRepository;
            _leadExpenseInvestmentsRepository = leadExpenseInvestmentsRepository;
            _jobCostFixExpenseListRepository = jobCostFixExpenseListRepository;
        }

        public async Task<PagedResultDto<GetAllJobCostViewDto>> GetAll(GetAllJobCostInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var team_list = _userTeamRepository.GetAll();

            var User_List = _userRepository.GetAll();
            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            //var periodId = 0;
            //var resultItemList = new  List<CategoryInstallationItemList>();

            //if(input.StartDate != null && input.EndDate != null)
            //{
            //   var period = _categoryInstallationItemPeriodRepository.GetAll().Where( e => ((e.StartDate.Date >= SDate.Value.Date && e.EndDate.Date <= EDate.Value.Date) || (e.StartDate.Date <= SDate.Value.Date && e.EndDate.Date >= EDate.Value.Date)) && e.OrganizationUnit == input.OrganizationUnit).AsNoTracking().FirstOrDefault();
            //    periodId = period != null ? period.Id : 0;
            //    if(periodId > 0)
            //    {
            //        resultItemList = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodId == periodId).ToList();
            //    }
            //}

            #region OldCode
            //var filteredJobs = _jobRepository.GetAll()
            //            .Include(e => e.LeadFk)
            //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter))


            //            .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "Active" && input.StartDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "Active" && input.EndDate != null, e => e.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "InstallBook" && input.StartDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "InstallBook" && input.EndDate != null, e => e.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(input.DateType == "InstallComplete" && input.StartDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
            //            .WhereIf(input.DateType == "InstallComplete" && input.EndDate != null, e => e.InstalledcompleteDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

            //            .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
            //            .WhereIf(role.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
            //            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
            //            .WhereIf(role.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)

            //            .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit);

            //var pagedAndFilteredJobs1 = filteredJobs
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            //var insItemList = _installationItemListRepository.GetAll().Include(e => e.InstallationItemPeriodFk);
            //var btryCost = _batteryInstallationCostRepository.GetAll().FirstOrDefault();
            //var STCCost = _stcCostRepository.GetAll().FirstOrDefault();

            //var res = filteredJobs.FirstOrDefault();
            //var BatteryDetailsList = _jobProductItemRepository.GetAll().Where(e => e.JobId == res.Id && e.ProductItemFk.ProductTypeId == 5).ToList();

            //var jobs1 = from o in pagedAndFilteredJobs1

            //            let teamName = team_list.Where(e => e.UserId == o.LeadFk.AssignToUserID).Select(e => e.TeamFk.Name).FirstOrDefault()

            //           let JobProduct = _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id)

            //           //Panel Costs
            //           let PanelDetails = JobProduct.Where(e => e.JobId == o.Id && e.ProductItemFk.ProductTypeId == 1).FirstOrDefault()

            //           let noOfQty = PanelDetails != null ? (int)PanelDetails.Quantity : 0

            //           let panelItemCost = PanelDetails != null ? insItemList.Where(e => e.ProductItemId == PanelDetails.ProductItemId && e.InstallationItemPeriodFk.StartDate <= o.CreationTime && e.InstallationItemPeriodFk.EndDate >= o.CreationTime).Select(e => e.UnitPrice * (PanelDetails.Quantity == null ? 0 : PanelDetails.Quantity)).FirstOrDefault() : 0

            //           //Inverter Costs
            //           let InverterDetails = JobProduct.Where(e => e.JobId == o.Id && e.ProductItemFk.ProductTypeId == 2).FirstOrDefault()

            //           let inverterItemCost = InverterDetails != null ? insItemList.Where(e => e.ProductItemId == InverterDetails.ProductItemId && e.InstallationItemPeriodFk.StartDate <= o.CreationTime && e.InstallationItemPeriodFk.EndDate >= o.CreationTime).Select(e => e.UnitPrice * (InverterDetails.Quantity == null ? 0 : InverterDetails.Quantity)).FirstOrDefault() : 0

            //           //Bettery Costs
            //           let BatteryDetails = JobProduct.Where(e => e.JobId == o.Id && e.ProductItemFk.ProductTypeId == 5).FirstOrDefault()

            //           let batteryItemCost = BatteryDetails != null ? insItemList.Where(e => e.ProductItemId == BatteryDetails.ProductItemId && e.InstallationItemPeriodFk.StartDate <= o.CreationTime && e.InstallationItemPeriodFk.EndDate >= o.CreationTime).Select(e => e.UnitPrice * (BatteryDetails.Quantity == null ? 0 : BatteryDetails.Quantity)).FirstOrDefault() : 0

            //           //let BatteryDetailsList = JobProduct.Where(e => e.JobId == o.Id && e.ProductItemFk.ProductTypeId == 5).ToList()

            //           //let batteryItemCost = (from bl in BatteryDetailsList
            //           //                          join insItem in insItemList on bl.ProductItemId equals insItem.ProductItemId
            //           //                          where insItem.InstallationItemPeriodFk.StartDate <= o.CreationTime && insItem.InstallationItemPeriodFk.EndDate >= o.CreationTime
            //           //                          select insItem.UnitPrice * (bl.Quantity == null ? 0 : bl.Quantity)).ToList()

            //           //State Wise Installation Cost
            //           let installationCost = _stateWiseInstallationCostRepository.GetAll().Where(e => e.StateFk.Name == o.State).Select(e => (o.LeadFk.Area == "Metro" ? (decimal)e.Metro : o.LeadFk.Area == "Regional" ? (decimal)e.Regional : 0) * o.SystemCapacity).FirstOrDefault()

            //           //Battery Installation Cost
            //           let batterySize = BatteryDetails != null ? BatteryDetails.ProductItemFk.Size : 0
            //           let batteryInstallationCost = batterySize != null && batterySize != 0 && btryCost != null ? btryCost.FixCost : 0
            //           let extraBrtyInsCost = batterySize != null && batterySize != 0 && btryCost != null ? batterySize > btryCost.Kw ? (((decimal)batterySize - btryCost.Kw) * btryCost.ExtraCost) : 0 : 0
            //           let actualBatteryInstallationCost = batteryInstallationCost + extraBrtyInsCost

            //           //Racking
            //           let Racking = _fixedCostPriceRepository.GetAll().Where(e => e.Name == "Racking").FirstOrDefault()
            //           let rackingCost = Racking.Type == "Panel" ? (decimal)Racking.Cost * noOfQty : Racking.Type == "Kw" ? (decimal)Racking.Cost * (decimal)o.SystemCapacity : Racking.Type == "Job" ? (decimal)Racking.Cost : 0

            //           //Accessories
            //           let Accessories = _fixedCostPriceRepository.GetAll().Where(e => e.Name == "Accessories").FirstOrDefault()
            //           let accessoriesCost = Accessories.Type == "Panel" ? (decimal)Accessories.Cost * noOfQty : Accessories.Type == "Kw" ? (decimal)Accessories.Cost * (decimal)o.SystemCapacity : Accessories.Type == "Job" ? (decimal)Accessories.Cost : 0

            //           //Ad Cost
            //           let AdCost = _fixedCostPriceRepository.GetAll().Where(e => e.Name == "Ad Cost").FirstOrDefault()
            //           let adCost = AdCost.Type == "Panel" ? (decimal)AdCost.Cost * noOfQty : AdCost.Type == "Kw" ? (decimal)AdCost.Cost * (decimal)o.SystemCapacity : AdCost.Type == "Job" ? (decimal)AdCost.Cost : 0

            //           //Fix Expense
            //           let FixExpense = _fixedCostPriceRepository.GetAll().Where(e => e.Name == "Fix Expense").FirstOrDefault()
            //           let fixExpenseCost = FixExpense.Type == "Panel" ? (decimal)FixExpense.Cost * noOfQty : FixExpense.Type == "Kw" ? (decimal)FixExpense.Cost * (decimal)o.SystemCapacity : FixExpense.Type == "Job" ? (decimal)FixExpense.Cost : 0

            //           // Start Extra Installation Cost
            //           let extraInstallationCharges = _extraInstallationChargeRepository.GetAll().Where(e => e.StateFk.Name == o.State)
            //           //House Type
            //           let houseType = extraInstallationCharges.Where(e => e.Category == "House Type" && e.NameId == o.HouseTypeId).FirstOrDefault()
            //           let Hcost = houseType != null ? o.LeadFk.Area == "Metro" ? (decimal)houseType.MetroAmount : o.LeadFk.Area == "Regional" ? (decimal)houseType.RegionalAmount : 0 : 0
            //           let houseTypeCost = houseType != null ? houseType.Type == "Panel" ? (decimal)Hcost * noOfQty : houseType.Type == "Kw" ? (decimal)Hcost * (decimal)o.SystemCapacity : houseType.Type == "Job" ? (decimal)Hcost : 0 : 0

            //           //Roof Type
            //           let roofType = extraInstallationCharges.Where(e => e.Category == "Roof Type" && e.NameId == o.RoofTypeId).FirstOrDefault()
            //           let rtcost = roofType != null ? o.LeadFk.Area == "Metro" ? (decimal)roofType.MetroAmount : o.LeadFk.Area == "Regional" ? (decimal)roofType.RegionalAmount : 0 : 0
            //           let roofTypeCost = roofType != null ? roofType.Type == "Panel" ? (decimal)rtcost * noOfQty : roofType.Type == "Kw" ? (decimal)rtcost * (decimal)o.SystemCapacity : roofType.Type == "Job" ? (decimal)rtcost : 0 : 0

            //           //Roof Angle
            //           let roofAngle = extraInstallationCharges.Where(e => e.Category == "Roof Angle" && e.NameId == o.RoofAngleId).FirstOrDefault()
            //           let rAcost = roofAngle != null ? o.LeadFk.Area == "Metro" ? (decimal)roofAngle.MetroAmount : o.LeadFk.Area == "Regional" ? (decimal)roofAngle.RegionalAmount : 0 : 0
            //           let roofAngleCost = roofAngle != null ? roofAngle.Type == "Panel" ? (decimal)rAcost * noOfQty : roofAngle.Type == "Kw" ? (decimal)rAcost * (decimal)o.SystemCapacity : roofAngle.Type == "Job" ? (decimal)rAcost : 0 : 0

            //           //Variation Cost
            //           let variationList = _jobVariationRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.VariationId)

            //           let variationCost = (from v in variationList
            //                                join vc in extraInstallationCharges.Where(e => e.Category == "Price Variation")
            //                                on v equals vc.NameId

            //                                let cost = o.LeadFk.Area == "Metro" ? (decimal)vc.MetroAmount : o.LeadFk.Area == "Regional" ? (decimal)vc.RegionalAmount : 0

            //                                select vc.Type == "Panel" ? (decimal)cost * noOfQty : vc.Type == "Kw" ? (decimal)cost * (decimal)o.SystemCapacity : vc.Type == "Job" ? (decimal)cost : 0
            //                               ).ToList()

            //           let vcCost = 0//GetExtraInstallationCost(variationCost)

            //           let extraInsCost = houseTypeCost + roofTypeCost + roofAngleCost + vcCost
            //           //End Extra Installation Cost

            //           //STC Rebate
            //           let STCRebate = STCCost != null ? STCCost.Cost * (o.STC == null ? 0 : (decimal)o.STC) : 0

            //           //Total Actual Cost
            //           let totalCost = (panelItemCost != null ? (decimal)panelItemCost : 0) + (inverterItemCost != null ? (decimal)inverterItemCost : 0) + (batteryItemCost != null ? (decimal)batteryItemCost : 0) + (installationCost != null ? (decimal)installationCost : 0) + actualBatteryInstallationCost + rackingCost + accessoriesCost + adCost + fixExpenseCost + extraInsCost

            //           let np = (o.TotalCost != null ? (decimal)o.TotalCost : 0) - (totalCost - STCRebate)

            //           select new GetAllJobCostViewDto()
            //           {
            //               Id = o.Id,
            //               LeadId = o.LeadId,
            //               JobNumber = o.JobNumber,
            //               State = o.State,
            //               AreaName = o.LeadFk.Area,
            //               Team = teamName,
            //               SalesRep = _userRepository.GetAll().Where(e => e.Id == o.LeadFk.AssignToUserID).Select(e => e.FullName).FirstOrDefault(),
            //               PanelSize = PanelDetails != null ? PanelDetails.ProductItemFk.Size : 0,
            //               NoOfPanel = noOfQty,
            //               SystemCapacity = o.SystemCapacity,
            //               PanelCost = (panelItemCost != null ? panelItemCost : 0),
            //               InverterCost = (inverterItemCost != null ? inverterItemCost : 0),
            //               BatteryCost = (batteryItemCost != null ? batteryItemCost : 0),
            //               InstallationCost = (installationCost != null ? installationCost : 0),
            //               BatteryInstallation = actualBatteryInstallationCost,
            //               Racking = rackingCost,
            //               Accessories = accessoriesCost,
            //               AdCost = adCost,
            //               FixedExpense = fixExpenseCost,
            //               ExtraInsCost = extraInsCost,
            //               TotalCost = totalCost,
            //               STCRebate = STCRebate,
            //               CostAfteRebate = totalCost - STCRebate,
            //               SellPrice = o.TotalCost,
            //               NP = np,
            //               NPPercentage = totalCost != 0 ? (np * 100) / totalCost : 0,
            //               VariationCost = variationCost,

            //               //BatteryCost = (from bl in JobProduct
            //               //               join insItem in insItemList on bl.ProductItemId equals insItem.ProductItemId
            //               //               where insItem.InstallationItemPeriodFk.StartDate <= o.CreationTime && insItem.InstallationItemPeriodFk.EndDate >= o.CreationTime
            //               //               && bl.JobId == o.Id && bl.ProductItemFk.ProductTypeId == 5

            //               //               select insItem.UnitPrice * (bl.Quantity == null ? 0 : bl.Quantity)
            //               //                ).Sum()
            //           };




            //var totalCount = await filteredJobs.CountAsync();

            //var re = await jobs1.ToListAsync();
            //re.AddRange(jobs.ToList());

            ////return new PagedResultDto<GetAllJobCostViewDto>(totalCount, await jobs.ToListAsync());
            //return new PagedResultDto<GetAllJobCostViewDto>(totalCount, re);
            ////return new PagedResultDto<GetAllJobCostViewDto>(totalCount, pagedAndFilteredJob4s.ToList());
            ///
            #endregion OldCode

            var command = _dbcontextprovider.GetDbContext().Database.GetDbConnection().CreateCommand();
            command.CommandText = "GetJobCost";
            command.CommandType = CommandType.StoredProcedure;
            command.Transaction = GetActiveTransaction();
            command.CommandTimeout = 100000;

            var parameter = command.CreateParameter();
            parameter.ParameterName = "@JobNumber";
            if (!string.IsNullOrWhiteSpace(input.Filter))
                parameter.Value = input.Filter;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@OrganizationId";
            parameter.DbType = DbType.Int32;
            parameter.Value = input.OrganizationUnit;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DateType";
            if (!string.IsNullOrWhiteSpace(input.DateType))
                parameter.Value = input.DateType;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DiffHour";
            parameter.Value = diffHour;
            parameter.DbType = DbType.Double;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartDate";
            if (SDate != null)
                parameter.Value = SDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndDate";
            if (EDate != null)
                parameter.Value = EDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@CurruntDate";
            if (!string.IsNullOrWhiteSpace(input.CurruntDate))
                parameter.Value = input.CurruntDate;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@State";
            if (!string.IsNullOrWhiteSpace(input.State))
                parameter.Value = input.State;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Installation";
            if (input.Installation != null)
                parameter.Value = input.Installation;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartSysCapRange";
            if (input.StartSysCapRange != null && input.StartSysCapRange > 0)
                parameter.Value = input.StartSysCapRange;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndSysCapRange";
            if (input.EndSysCapRange != null && input.EndSysCapRange > 0)
                parameter.Value = input.EndSysCapRange;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            //command.Connection.Open();
            //DbDataReader reader = command.ExecuteReader();
            //var table = new DataTable();
            //table.Load(reader);
            //command.Connection.Close();

            //command.Connection.Open();
            DbDataReader reader = command.ExecuteReader();
            var table = new DataTable();
            table.Load(reader);


            var result = from item in table.AsEnumerable()
                         select new GetAllJobCostViewDto()
                         {
                             Id = item.Field<int>("Id"),
                             JobNumber = item.Field<string>("JobNUmber"),
                             State = item.Field<string>("State"),
                             PostCode = item.Field<string>("PostalCode"),
                             AreaName = item.Field<string>("Area"),
                             Team = item.Field<string>("TeamName"),
                             SalesRep = item.Field<string>("SalesRep"),
                             PanelSize = item.Field<decimal?>("PanelSize"),
                             NoOfPanel = item.Field<int?>("NoOfPanel"),
                             SystemCapacity = item.Field<decimal?>("SystemCapacity"),
                             PanelCost = item.Field<decimal?>("PanelCost") != null ? item.Field<decimal?>("PanelCost") : 0,
                             InverterCost = item.Field<decimal?>("InverterCost") != null ? item.Field<decimal?>("InverterCost") : 0,
                             BatteryCost = item.Field<decimal?>("BatteryCost") != null ? item.Field<decimal?>("BatteryCost") : 0,
                             InstallationCost = item.Field<decimal?>("InstallationCost") != null ? item.Field<decimal?>("InstallationCost") : 0,
                             InstallationCostType = item.Field<string>("InstallationCostType"),
                             BatteryInstallation = item.Field<decimal?>("BatteryInstallation"),
                             Racking = item.Field<decimal?>("Racking"),
                             Accessories = item.Field<decimal?>("Accessories"),
                             AdCost = item.Field<decimal?>("AdCost"),
                             FixedExpense = item.Field<decimal?>("FixedExpense"),
                             ExtraInsCost = (item.Field<decimal?>("HouseType") != null ? item.Field<decimal?>("HouseType") : 0) + (item.Field<decimal?>("RoofType") != null ? item.Field<decimal?>("RoofType") : 0) + (item.Field<decimal?>("RoofAngle") != null ? item.Field<decimal?>("RoofAngle") : 0) + (item.Field<decimal?>("Variation") != null ? item.Field<decimal?>("Variation") : 0),

                             STCRebate = item.Field<decimal?>("STCRebate"),
                             SellPrice = item.Field<decimal?>("TotalCost"),

                             JobStatusId = item.Field<int?>("JobStatusId"),
                             AssignToUserID = item.Field<int?>("AssignToUserID"),
                             FinOptionPercentage = item.Field<decimal?>("FinancePercentage"),
                             CategoryDate = item.Field<DateTime?>("CategoryDate"),
                             BatteryKw = item.Field<decimal?>("BatteryKw"),
                         };

            IQueryable<GetAllJobCostViewDto> itemDtos = result.AsQueryable();

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            var filteredJobs = itemDtos
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.JobStatus != null && input.JobStatus.Count > 0, e => input.JobStatus.Contains(e.JobStatusId))
                        .WhereIf(input.isEmpJobCost == true && role.Contains("Sales Rep"), e => e.SalesRep == User.FullName)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SalesRap), e => e.SalesRep == input.SalesRap)
                        //.WhereIf(input.Cost == 1, e => (e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0))

                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.AreaName == input.AreaName)

                        .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.SystemFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.SystemFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.SystemFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery

                        .WhereIf(input.SystemFilter == 0 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0) // For All Without Sys Filter
                        .WhereIf(input.SystemFilter == 1 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0) // For Full System
                        .WhereIf(input.SystemFilter == 2 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.InstallationCost == 0) // For Without Battery
                        .WhereIf(input.SystemFilter == 3 && input.Cost == 1, e => e.BatteryCost == 0) // For Only Battery
                        .WhereIf(input.SystemFilter == 4 && input.Cost == 1, e => e.InverterCost == 0) // For Only Inverter
                        .WhereIf(input.SystemFilter == 5 && input.Cost == 1, e => e.InverterCost == 0 || e.BatteryCost == 0) // For Inverter and Battery Only

                        .WhereIf(input.ProductItemId != null && input.ProductItemId > 0, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemId == input.ProductItemId))
                        ;

            var filteredOutput = from o in filteredJobs

                                 let finOptionPercentage = o.FinOptionPercentage != null ? (decimal)o.FinOptionPercentage : 0
                                 let totalPrice = o.SellPrice != null ? (decimal)o.SellPrice : 0

                                 let financeCost = finOptionPercentage != 0 && totalPrice != 0 ? ((totalPrice * finOptionPercentage) / 100) : 0

                                 let totalCost = ((o.PanelCost != null ? (decimal)o.PanelCost : 0) + (o.InverterCost != null ? (decimal)o.InverterCost : 0) + (o.BatteryCost != null ? (decimal)o.BatteryCost : 0) + (o.InstallationCost != null ? (decimal)o.InstallationCost : 0) + (o.BatteryInstallation != null ? (decimal)o.BatteryInstallation : 0) + (o.Racking != null ? (decimal)o.Racking : 0) + (o.Accessories != null ? (decimal)o.Accessories : 0) + (o.AdCost != null ? (decimal)o.AdCost : 0) + (o.FixedExpense != null ? (decimal)o.FixedExpense : 0) + (o.ExtraInsCost != null ? (decimal)o.ExtraInsCost : 0) + financeCost)

                                 let np = (o.SellPrice != null ? (decimal)o.SellPrice : 0) - (totalCost - o.STCRebate)

                                 let np_sys = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 && o.SystemCapacity > 0 ? np / o.SystemCapacity : 0

                                 let Category = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodFk.StartDate.Date <= o.CategoryDate && e.CategoryInstallationItemPeriodFk.EndDate >= o.CategoryDate && e.CategoryInstallationItemPeriodFk.OrganizationUnit == input.OrganizationUnit && e.StartValue <= np_sys && e.EndValue >= np_sys).FirstOrDefault()

                                 select new GetAllJobCostViewDto()
                                 {
                                     Id = o.Id,
                                     JobNumber = o.JobNumber,
                                     State = o.State,
                                     PostCode = o.PostCode,
                                     AreaName = o.AreaName,
                                     Team = o.Team,
                                     SalesRep = o.SalesRep,
                                     PanelSize = o.PanelSize,
                                     NoOfPanel = o.NoOfPanel,
                                     PricePerKw = o.SystemCapacity > 0 ? (o.SellPrice / o.SystemCapacity) : 0,
                                     SystemCapacity = o.SystemCapacity,
                                     PanelCost = o.PanelCost,
                                     InverterCost = o.InverterCost,
                                     BatteryCost = o.BatteryCost,
                                     InstallationCost = o.InstallationCost,
                                     InstallationCostType = o.InstallationCostType,
                                     BatteryInstallation = o.BatteryInstallation,
                                     Racking = o.Racking,
                                     Accessories = o.Accessories,
                                     AdCost = o.AdCost,
                                     FixedExpense = o.FixedExpense,
                                     ExtraInsCost = o.ExtraInsCost,
                                     TotalCost = totalCost,
                                     STCRebate = o.STCRebate,
                                     CostAfteRebate = totalCost - (o.STCRebate != null ? (decimal)o.STCRebate : 0),
                                     SellPrice = o.SellPrice,
                                     NP = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? np : 0,
                                     NPPercentage = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? (totalCost != 0 ? (np * 100) / totalCost : 0) : 0,
                                     GP = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? (np + (o.AdCost != null ? (decimal)o.AdCost : 0)) : 0,
                                     NP_SysCap = np_sys,
                                     Category = Category != null ? Category.CategoryName : "",
                                     FinanceCost = financeCost,
                                     BatteryKw = o.BatteryKw
                                 };

            var jobs = filteredOutput.WhereIf(!string.IsNullOrEmpty(input.Category), e => e.Category == input.Category);

            var jobsList = new List<GetAllJobCostViewDto>();

            if (input.Sorting == "NP ASC")
            {
                jobsList = jobs
                               .OrderBy(e => e.NP)
                               .PageBy(input).ToList();
            }
            else if (input.Sorting == "NP DESC")
            {
                jobsList = jobs
               .OrderByDescending(e => e.NP)
               .PageBy(input).ToList();
            }
            else
            {
                jobsList = jobs
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input).ToList();
            }



            #region Summary
            JobCostSummary summary = new JobCostSummary();
            var systemKw = jobs.Sum(e => e.SystemCapacity != null ? (decimal)e.SystemCapacity : 0);

            summary.NoOfPanels = jobs.Sum(e => e.NoOfPanel != null ? (int)e.NoOfPanel : 0);
            summary.PricePerKw = jobs.Sum(e => e.PricePerKw != null ? (decimal)e.PricePerKw : 0);
            summary.MegaWatt = systemKw > 0 ? (systemKw / 1000) : 0;
            summary.SystemCapacity = systemKw;
            summary.PanelCost = jobs.Sum(e => e.PanelCost != null ? (decimal)e.PanelCost : 0);
            summary.InverterCost = jobs.Sum(e => e.InverterCost != null ? (decimal)e.InverterCost : 0);
            summary.BatteryCost = jobs.Sum(e => e.BatteryCost != null ? (decimal)e.BatteryCost : 0);
            summary.InstallationCost = jobs.Sum(e => e.InstallationCost != null ? (decimal)e.InstallationCost : 0);
            summary.BatteryInstallation = jobs.Sum(e => e.BatteryInstallation != null ? (decimal)e.BatteryInstallation : 0);
            summary.Racking = jobs.Sum(e => e.Racking != null ? (decimal)e.Racking : 0);
            summary.Accessories = jobs.Sum(e => e.Accessories != null ? (decimal)e.Accessories : 0);
            summary.AdCost = jobs.Sum(e => e.AdCost != null ? (decimal)e.AdCost : 0);
            summary.FixedExpense = jobs.Sum(e => e.FixedExpense != null ? (decimal)e.FixedExpense : 0);
            summary.ExtraInsCost = jobs.Sum(e => e.ExtraInsCost != null ? (decimal)e.ExtraInsCost : 0);
            summary.FinanceCost = jobs.Sum(e => e.FinanceCost != null ? (decimal)e.FinanceCost : 0);
            summary.TotalCost = jobs.Sum(e => e.TotalCost != null ? (decimal)e.TotalCost : 0);
            summary.STCRebate = jobs.Sum(e => e.STCRebate != null ? (decimal)e.STCRebate : 0);
            summary.CostAfteRebate = jobs.Sum(e => e.CostAfteRebate != null ? (decimal)e.CostAfteRebate : 0);
            summary.SellPrice = jobs.Sum(e => e.SellPrice != null ? (decimal)e.SellPrice : 0);
            summary.NP = jobs.Sum(e => e.NP != null ? (decimal)e.NP : 0);
            summary.NPPercentage = jobs.Sum(e => e.NPPercentage != null ? (decimal)e.NPPercentage : 0);
            summary.GP = jobs.Sum(e => e.GP != null ? (decimal)e.GP : 0);
            summary.NP_SysCap = jobs.Sum(e => e.NP_SysCap != null ? (decimal)e.NP_SysCap : 0);
            summary.BatteryKw = jobs.Sum(e => e.BatteryKw != null ? e.BatteryKw : 0);
            #endregion

            // var jobsList = pagedAndFilteredJobs.ToList();
            if (jobsList.Count() > 0)
            {
                jobsList[0].JobCostSummary = summary;
            }
            var totalCount = jobs.Count();

            return new PagedResultDto<GetAllJobCostViewDto>(totalCount, jobsList);
        }

        public async Task<FileDto> GetJobCostToExcel(GetAllJobCostForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var team_list = _userTeamRepository.GetAll();

            var User_List = _userRepository.GetAll();
            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var command = _dbcontextprovider.GetDbContext().Database.GetDbConnection().CreateCommand();
            command.CommandText = "GetJobCost";
            command.CommandType = CommandType.StoredProcedure;
            command.Transaction = GetActiveTransaction();
            command.CommandTimeout = 100000;

            var parameter = command.CreateParameter();
            parameter.ParameterName = "@JobNumber";
            if (!string.IsNullOrWhiteSpace(input.Filter))
                parameter.Value = input.Filter;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@OrganizationId";
            parameter.DbType = DbType.Int32;
            parameter.Value = input.OrganizationUnit;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DateType";
            if (!string.IsNullOrWhiteSpace(input.DateType))
                parameter.Value = input.DateType;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DiffHour";
            parameter.Value = diffHour;
            parameter.DbType = DbType.Double;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartDate";
            if (SDate != null)
                parameter.Value = SDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndDate";
            if (EDate != null)
                parameter.Value = EDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@CurruntDate";
            if (!string.IsNullOrWhiteSpace(input.CurruntDate))
                parameter.Value = input.CurruntDate;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@State";
            if (!string.IsNullOrWhiteSpace(input.State))
                parameter.Value = input.State;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Installation";
            if (input.Installation != null)
                parameter.Value = input.Installation;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartSysCapRange";
            if (input.StartSysCapRange != null && input.StartSysCapRange > 0)
                parameter.Value = input.StartSysCapRange;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndSysCapRange";
            if (input.EndSysCapRange != null && input.EndSysCapRange > 0)
                parameter.Value = input.EndSysCapRange;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            //command.Connection.Open();
            //DbDataReader reader = command.ExecuteReader();
            //var table = new DataTable();
            //table.Load(reader);
            //command.Connection.Close();

            //command.Connection.Open();
            DbDataReader reader = command.ExecuteReader();
            var table = new DataTable();
            table.Load(reader);


            var result = from item in table.AsEnumerable()
                         select new GetAllJobCostViewDto()
                         {
                             Id = item.Field<int>("Id"),
                             JobNumber = item.Field<string>("JobNUmber"),
                             State = item.Field<string>("State"),
                             PostCode = item.Field<string>("PostalCode"),
                             AreaName = item.Field<string>("Area"),
                             Team = item.Field<string>("TeamName"),
                             SalesRep = item.Field<string>("SalesRep"),
                             PanelSize = item.Field<decimal?>("PanelSize"),
                             NoOfPanel = item.Field<int?>("NoOfPanel"),
                             SystemCapacity = item.Field<decimal?>("SystemCapacity"),
                             PanelCost = item.Field<decimal?>("PanelCost") != null ? item.Field<decimal?>("PanelCost") : 0,
                             InverterCost = item.Field<decimal?>("InverterCost") != null ? item.Field<decimal?>("InverterCost") : 0,
                             BatteryCost = item.Field<decimal?>("BatteryCost") != null ? item.Field<decimal?>("BatteryCost") : 0,
                             InstallationCost = item.Field<decimal?>("InstallationCost") != null ? item.Field<decimal?>("InstallationCost") : 0,
                             InstallationCostType = item.Field<string>("InstallationCostType"),
                             BatteryInstallation = item.Field<decimal?>("BatteryInstallation"),
                             Racking = item.Field<decimal?>("Racking"),
                             Accessories = item.Field<decimal?>("Accessories"),
                             AdCost = item.Field<decimal?>("AdCost"),
                             FixedExpense = item.Field<decimal?>("FixedExpense"),
                             ExtraInsCost = (item.Field<decimal?>("HouseType") != null ? item.Field<decimal?>("HouseType") : 0) + (item.Field<decimal?>("RoofType") != null ? item.Field<decimal?>("RoofType") : 0) + (item.Field<decimal?>("RoofAngle") != null ? item.Field<decimal?>("RoofAngle") : 0) + (item.Field<decimal?>("Variation") != null ? item.Field<decimal?>("Variation") : 0),

                             STCRebate = item.Field<decimal?>("STCRebate"),
                             SellPrice = item.Field<decimal?>("TotalCost"),

                             JobStatusId = item.Field<int?>("JobStatusId"),
                             AssignToUserID = item.Field<int?>("AssignToUserID"),
                             FinOptionPercentage = item.Field<decimal?>("FinancePercentage"),
                             CategoryDate = item.Field<DateTime?>("CategoryDate"),
                             BatteryKw = item.Field<decimal?>("BatteryKw"),
                         };

            IQueryable<GetAllJobCostViewDto> itemDtos = result.AsQueryable();

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            var filteredJobs = itemDtos
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.JobStatus != null && input.JobStatus.Count > 0, e => input.JobStatus.Contains(e.JobStatusId))
                        .WhereIf(input.isEmpJobCost == true && role.Contains("Sales Rep"), e => e.SalesRep == User.FullName)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SalesRap), e => e.SalesRep == input.SalesRap)
                        //.WhereIf(input.Cost == 1, e => (e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0))

                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.AreaName == input.AreaName)

                        .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.SystemFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.SystemFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.SystemFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery

                        .WhereIf(input.SystemFilter == 0 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0) // For All Without Sys Filter
                        .WhereIf(input.SystemFilter == 1 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0) // For Full System
                        .WhereIf(input.SystemFilter == 2 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.InstallationCost == 0) // For Without Battery
                        .WhereIf(input.SystemFilter == 3 && input.Cost == 1, e => e.BatteryCost == 0) // For Only Battery
                        .WhereIf(input.SystemFilter == 4 && input.Cost == 1, e => e.InverterCost == 0) // For Only Inverter
                        .WhereIf(input.SystemFilter == 5 && input.Cost == 1, e => e.InverterCost == 0 || e.BatteryCost == 0) // For Inverter and Battery Only

                        .WhereIf(input.ProductItemId != null && input.ProductItemId > 0, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemId == input.ProductItemId))
                        ;


            var filteredOutput = from o in filteredJobs

                                 let finOptionPercentage = o.FinOptionPercentage != null ? (decimal)o.FinOptionPercentage : 0
                                 let totalPrice = o.SellPrice != null ? (decimal)o.SellPrice : 0

                                 let financeCost = finOptionPercentage != 0 && totalPrice != 0 ? ((totalPrice * finOptionPercentage) / 100) : 0

                                 let totalCost = ((o.PanelCost != null ? (decimal)o.PanelCost : 0) + (o.InverterCost != null ? (decimal)o.InverterCost : 0) + (o.BatteryCost != null ? (decimal)o.BatteryCost : 0) + (o.InstallationCost != null ? (decimal)o.InstallationCost : 0) + (o.BatteryInstallation != null ? (decimal)o.BatteryInstallation : 0) + (o.Racking != null ? (decimal)o.Racking : 0) + (o.Accessories != null ? (decimal)o.Accessories : 0) + (o.AdCost != null ? (decimal)o.AdCost : 0) + (o.FixedExpense != null ? (decimal)o.FixedExpense : 0) + (o.ExtraInsCost != null ? (decimal)o.ExtraInsCost : 0) + financeCost)

                                 let np = (o.SellPrice != null ? (decimal)o.SellPrice : 0) - (totalCost - o.STCRebate)

                                 let np_sys = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 && o.SystemCapacity > 0 ? np / o.SystemCapacity : 0

                                 let Category = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodFk.StartDate.Date <= o.CategoryDate && e.CategoryInstallationItemPeriodFk.EndDate >= o.CategoryDate && e.CategoryInstallationItemPeriodFk.OrganizationUnit == input.OrganizationUnit && e.StartValue <= np_sys && e.EndValue >= np_sys).FirstOrDefault()

                                 select new GetAllJobCostViewForExcelExportDto()
                                 {
                                     Id = o.Id,
                                     JobNumber = o.JobNumber,
                                     State = o.State,
                                     //PostCode = o.PostCode,
                                     AreaName = o.AreaName,
                                     Team = o.Team,
                                     SalesRep = o.SalesRep,
                                     PanelSize = o.PanelSize,
                                     NoOfPanel = o.NoOfPanel,
                                     PricePerKw = o.SystemCapacity > 0 ? (o.SellPrice / o.SystemCapacity) : 0,
                                     SystemCapacity = o.SystemCapacity,
                                     PanelCost = o.PanelCost,
                                     InverterCost = o.InverterCost,
                                     BatteryCost = o.BatteryCost,
                                     InstallationCost = o.InstallationCost,
                                     InstallationCostType = o.InstallationCostType,
                                     BatteryInstallation = o.BatteryInstallation,
                                     Racking = o.Racking,
                                     Accessories = o.Accessories,
                                     AdCost = o.AdCost,
                                     FixedExpense = o.FixedExpense,
                                     ExtraInsCost = o.ExtraInsCost,
                                     TotalCost = totalCost,
                                     STCRebate = o.STCRebate,
                                     CostAfteRebate = totalCost - (o.STCRebate != null ? (decimal)o.STCRebate : 0),
                                     SellPrice = o.SellPrice,
                                     NP = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? np : 0,
                                     NPPercentage = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? (totalCost != 0 ? (np * 100) / totalCost : 0) : 0,
                                     GP = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? (np + (o.AdCost != null ? (decimal)o.AdCost : 0)) : 0,
                                     NP_SysCap = np_sys,
                                     Category = Category != null ? Category.CategoryName : "",
                                     FinanceCost = financeCost,
                                     BatteryKw = o.BatteryKw
                                 };

            var jobs = filteredOutput.WhereIf(!string.IsNullOrEmpty(input.Category), e => e.Category == input.Category);

            //var jobs = from o in filteredJobs

            //           let j = _jobRepository.GetAll().Include(e => e.JobTypeFk).Include(e => e.JobStatusFk).Where(e => e.Id == o.Id).FirstOrDefault()

            //           let SystemDetails = _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id).Select(e => e.ProductItemFk.Name).ToList()

            //           let finOptionPercentage = o.FinOptionPercentage != null ? (decimal)o.FinOptionPercentage : 0
            //           let totalPrice = o.SellPrice != null ? (decimal)o.SellPrice : 0

            //           let financeCost = finOptionPercentage != 0 && totalPrice != 0 ? ((totalPrice * finOptionPercentage) / 100) : 0

            //           let totalCost = ((o.PanelCost != null ? (decimal)o.PanelCost : 0) + (o.InverterCost != null ? (decimal)o.InverterCost : 0) + (o.BatteryCost != null ? (decimal)o.BatteryCost : 0) + (o.InstallationCost != null ? (decimal)o.InstallationCost : 0) + (o.BatteryInstallation != null ? (decimal)o.BatteryInstallation : 0) + (o.Racking != null ? (decimal)o.Racking : 0) + (o.Accessories != null ? (decimal)o.Accessories : 0) + (o.AdCost != null ? (decimal)o.AdCost : 0) + (o.FixedExpense != null ? (decimal)o.FixedExpense : 0) + (o.ExtraInsCost != null ? (decimal)o.ExtraInsCost : 0) + financeCost)

            //           let np = (o.SellPrice != null ? (decimal)o.SellPrice : 0) - (totalCost - o.STCRebate)

            //           let np_sys = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 && o.SystemCapacity > 0 ? np / o.SystemCapacity : 0

            //           let Category = _categoryInstallationItemListRepository.GetAll().Where(e => e.CategoryInstallationItemPeriodFk.StartDate.Date <= o.CategoryDate && e.CategoryInstallationItemPeriodFk.EndDate >= o.CategoryDate && e.CategoryInstallationItemPeriodFk.OrganizationUnit == input.OrganizationUnit && e.StartValue <= np_sys && e.EndValue >= np_sys).FirstOrDefault()

            //           select new GetAllJobCostViewForExcelExportDto()
            //           {
            //               Id = o.Id,
            //               JobNumber = o.JobNumber,
            //               State = o.State,
            //               PostalCode = j.PostalCode,
            //               AreaName = o.AreaName,
            //               Team = o.Team,
            //               SalesRep = o.SalesRep,
            //               JobType = j.JobTypeFk.Name,
            //               JobStatus = j.JobStatusFk.Name,
            //               SystemDetails = SystemDetails,
            //               PanelSize = o.PanelSize,
            //               NoOfPanel = o.NoOfPanel,
            //               PricePerKw = o.SystemCapacity > 0 ? (o.SellPrice / o.SystemCapacity) : 0,
            //               SystemCapacity = o.SystemCapacity,
            //               PanelCost = o.PanelCost,
            //               InverterCost = o.InverterCost,
            //               BatteryCost = o.BatteryCost,
            //               InstallationCost = o.InstallationCost,
            //               InstallationCostType = o.InstallationCostType,
            //               BatteryInstallation = o.BatteryInstallation,
            //               Racking = o.Racking,
            //               Accessories = o.Accessories,
            //               AdCost = o.AdCost,
            //               FixedExpense = o.FixedExpense,
            //               ExtraInsCost = o.ExtraInsCost,
            //               TotalCost = totalCost,
            //               STCRebate = o.STCRebate,
            //               CostAfteRebate = totalCost - (o.STCRebate != null ? (decimal)o.STCRebate : 0),
            //               SellPrice = o.SellPrice,
            //               NP = np,
            //               NPPercentage = totalCost != 0 ? (np * 100) / totalCost : 0,
            //               GP = np + (o.AdCost != null ? (decimal)o.AdCost : 0),
            //               NP_SysCap = np_sys,
            //               Category = Category != null ? Category.CategoryName : "",
            //               FinanceCost = financeCost,
            //               BatteryKw = o.BatteryKw
            //           };

            //jobs = jobs.WhereIf(!string.IsNullOrEmpty(input.Category), e => e.Category == input.Category);

            var list = jobs.ToList();

            if (input.isEmpJobCost == true)
                return _jobCostExcelExporter.ExportToFileEmpJobCost(jobs.ToList());
            else
                return _jobCostExcelExporter.ExportToFile(list);
        }

        private DbTransaction GetActiveTransaction()
        {
            return (DbTransaction)_transactionProvider.GetActiveTransaction(new ActiveTransactionProviderArgs
            {
                {"ContextType", typeof(TheSolarProductDbContext) },
                {"MultiTenancySide", MultiTenancySides.Tenant }
            });
        }

        public async Task<SummaryJobCostCount> TotalCount(GetAllJobCostForExcelInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var team_list = _userTeamRepository.GetAll();

            var User_List = _userRepository.GetAll();
            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var command = _dbcontextprovider.GetDbContext().Database.GetDbConnection().CreateCommand();
            command.CommandText = "GetJobCost";
            command.CommandType = CommandType.StoredProcedure;
            command.Transaction = GetActiveTransaction();
            command.CommandTimeout = 100000;

            var parameter = command.CreateParameter();
            parameter.ParameterName = "@JobNumber";
            if (!string.IsNullOrWhiteSpace(input.Filter))
                parameter.Value = input.Filter;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@OrganizationId";
            parameter.DbType = DbType.Int32;
            parameter.Value = input.OrganizationUnit;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DateType";
            if (!string.IsNullOrWhiteSpace(input.DateType))
                parameter.Value = input.DateType;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DiffHour";
            parameter.Value = diffHour;
            parameter.DbType = DbType.Double;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartDate";
            if (SDate != null)
                parameter.Value = SDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndDate";
            if (EDate != null)
                parameter.Value = EDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@CurruntDate";
            if (!string.IsNullOrWhiteSpace(input.CurruntDate))
                parameter.Value = input.CurruntDate;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@State";
            if (!string.IsNullOrWhiteSpace(input.State))
                parameter.Value = input.State;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Installation";
            if (input.Installation != null)
                parameter.Value = input.Installation;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartSysCapRange";
            if (input.StartSysCapRange != null && input.StartSysCapRange > 0)
                parameter.Value = input.StartSysCapRange;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndSysCapRange";
            if (input.EndSysCapRange != null && input.EndSysCapRange > 0)
                parameter.Value = input.EndSysCapRange;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            //command.Connection.Open();
            DbDataReader reader = command.ExecuteReader();
            //command.Connection.Open();
            var table = new DataTable();
            table.Load(reader);

            var result = from item in table.AsEnumerable()
                         select new GetAllJobCostViewDto()
                         {
                             Id = item.Field<int>("Id"),
                             JobNumber = item.Field<string>("JobNUmber"),
                             State = item.Field<string>("State"),
                             PostCode = item.Field<string>("PostalCode"),
                             AreaName = item.Field<string>("Area"),
                             Team = item.Field<string>("TeamName"),
                             SalesRep = item.Field<string>("SalesRep"),
                             PanelSize = item.Field<decimal?>("PanelSize"),
                             NoOfPanel = item.Field<int?>("NoOfPanel"),
                             SystemCapacity = item.Field<decimal?>("SystemCapacity"),
                             PanelCost = item.Field<decimal?>("PanelCost") != null ? item.Field<decimal?>("PanelCost") : 0,
                             InverterCost = item.Field<decimal?>("InverterCost") != null ? item.Field<decimal?>("InverterCost") : 0,
                             BatteryCost = item.Field<decimal?>("BatteryCost") != null ? item.Field<decimal?>("BatteryCost") : 0,
                             InstallationCost = item.Field<decimal?>("InstallationCost") != null ? item.Field<decimal?>("InstallationCost") : 0,
                             BatteryInstallation = item.Field<decimal?>("BatteryInstallation"),
                             Racking = item.Field<decimal?>("Racking"),
                             Accessories = item.Field<decimal?>("Accessories"),
                             AdCost = item.Field<decimal?>("AdCost"),
                             FixedExpense = item.Field<decimal?>("FixedExpense"),
                             ExtraInsCost = (item.Field<decimal?>("HouseType") != null ? item.Field<decimal?>("HouseType") : 0) + (item.Field<decimal?>("RoofType") != null ? item.Field<decimal?>("RoofType") : 0) + (item.Field<decimal?>("RoofAngle") != null ? item.Field<decimal?>("RoofAngle") : 0) + (item.Field<decimal?>("Variation") != null ? item.Field<decimal?>("Variation") : 0),

                             STCRebate = item.Field<decimal?>("STCRebate"),
                             SellPrice = item.Field<decimal?>("TotalCost"),

                             JobStatusId = item.Field<int?>("JobStatusId"),
                             AssignToUserID = item.Field<int?>("AssignToUserID"),
                             FinOptionPercentage = item.Field<decimal?>("FinancePercentage"),
                             CategoryDate = item.Field<DateTime?>("CategoryDate"),
                         };

            IQueryable<GetAllJobCostViewDto> itemDtos = result.AsQueryable();

            var filteredJobs = itemDtos
                        .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
                        .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
                        .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.JobStatus != null && input.JobStatus.Count > 0, e => input.JobStatus.Contains(e.JobStatusId))
                        .WhereIf(input.isEmpJobCost == true && role.Contains("Sales Rep"), e => e.SalesRep == User.FullName)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SalesRap), e => e.SalesRep == input.SalesRap);

            var jobs = from o in filteredJobs

                       let per = o.FinOptionPercentage != null ? (decimal)o.FinOptionPercentage : 0
                       let totalPrice = o.SellPrice != null ? (decimal)o.SellPrice : 0

                       let perValue = per != 0 && totalPrice != 0 ? ((totalPrice * per) / 100) : 0

                       let totalCost = ((o.PanelCost != null ? (decimal)o.PanelCost : 0) + (o.InverterCost != null ? (decimal)o.InverterCost : 0) + (o.BatteryCost != null ? (decimal)o.BatteryCost : 0) + (o.InstallationCost != null ? (decimal)o.InstallationCost : 0) + (o.BatteryInstallation != null ? (decimal)o.BatteryInstallation : 0) + (o.Racking != null ? (decimal)o.Racking : 0) + (o.Accessories != null ? (decimal)o.Accessories : 0) + (o.AdCost != null ? (decimal)o.AdCost : 0) + (o.FixedExpense != null ? (decimal)o.FixedExpense : 0) + (o.ExtraInsCost != null ? (decimal)o.ExtraInsCost : 0) + perValue)

                       let np = (o.SellPrice != null ? (decimal)o.SellPrice : 0) - (totalCost - o.STCRebate)

                       select new GetAllJobCostViewDto()
                       {
                           Id = o.Id,
                           JobNumber = o.JobNumber,
                           State = o.State,
                           AreaName = o.AreaName,
                           Team = o.Team,
                           SalesRep = o.SalesRep,
                           PanelSize = o.PanelSize,
                           NoOfPanel = o.NoOfPanel,
                           SystemCapacity = o.SystemCapacity,
                           PanelCost = o.PanelCost,
                           InverterCost = o.InverterCost,
                           BatteryCost = o.BatteryCost,
                           InstallationCost = o.InstallationCost,
                           BatteryInstallation = o.BatteryInstallation,
                           Racking = o.Racking,
                           Accessories = o.Accessories,
                           AdCost = o.AdCost,
                           FixedExpense = o.FixedExpense,
                           ExtraInsCost = o.ExtraInsCost,
                           TotalCost = totalCost,
                           STCRebate = o.STCRebate,
                           CostAfteRebate = totalCost - (o.STCRebate != null ? (decimal)o.STCRebate : 0),
                           SellPrice = o.SellPrice,
                           NP = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? np : 0,
                           NPPercentage = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? (totalCost != 0 ? (np * 100) / totalCost : 0) : 0,
                           GP = o.PanelCost != null && o.PanelCost != 0 && o.InverterCost != null && o.InverterCost != 0 ? (np + (o.AdCost != null ? (decimal)o.AdCost : 0)) : 0,

                       };

            SummaryJobCostCount summary = new SummaryJobCostCount();
            summary.TotalProfit = jobs.Where(o => o.NP > 0).Sum(e => e.NP);
            summary.TotalLoss = jobs.Where(o => o.NP < 0).Sum(e => e.NP);

            return summary;
        }

        public async Task<GetCheckInstallationCostOutput> GetCheckInstallationCost(int typeId, int jobId, string postCode, string dateType)
        {
            var output = new GetCheckInstallationCostOutput();

            var isExists = await _jobProductItemRepository.GetAll().AnyAsync(e => e.JobId == jobId && e.ProductItemFk.ProductTypeId == typeId);

            var jobs = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == jobId).FirstOrDefaultAsync();
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            DateTime date = new DateTime();
            if (dateType == "CreationDate")
            {
                date = jobs.CreationTime.AddHours(diffHour).Date;
            }
            else if (dateType == "FirstDeposite")
            {
                date = jobs.FirstDepositDate.Value.Date;
            }
            else if (dateType == "DepositeReceived")
            {
                date = jobs.DepositeRecceivedDate.Value.Date;
            }
            else if (dateType == "Active")
            {
                date = jobs.ActiveDate.Value.Date;
            }
            else if (dateType == "InstallBook")
            {
                date = jobs.InstallationDate.Value.Date;
            }
            else if (dateType == "InstallComplete")
            {
                date = jobs.InstalledcompleteDate.Value.Date;
            }

            var postCodeDetails = await _postoCodePriceRepository.GetAll().Where(e => e.PostCode != postCode && e.PostCodePricePeriodFk.Month == date.Month && e.PostCodePricePeriodFk.Year == date.Year).Select(e => e.PostCodePricePeriodId).FirstOrDefaultAsync();

            return new GetCheckInstallationCostOutput
            {
                IsExists = isExists,
                PostCodeId = postCodeDetails,
                Date = date,
            };
        }

        public async Task<GetCheckSystemCostOutput> GetCheckSystemCost(int typeId, int jobId, string dateType)
        {
            var output = new GetCheckSystemCostOutput();

            var isExists = await _jobProductItemRepository.GetAll().AnyAsync(e => e.JobId == jobId && e.ProductItemFk.ProductTypeId == typeId);
            var productType = await _productTypeRepository.FirstOrDefaultAsync(typeId);

            var jobs = await _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.Id == jobId).FirstOrDefaultAsync();
            var jobProduct = await _jobProductItemRepository.GetAll().Where(e => e.JobId == jobId && e.ProductItemFk.ProductTypeId == typeId).Select(e => new { e.ProductItemId, e.ProductItemFk.Name }).FirstOrDefaultAsync();

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            DateTime date = new DateTime();
            if (dateType == "CreationDate")
            {
                date = jobs.CreationTime.AddHours(diffHour).Date;
            }
            else if (dateType == "FirstDeposite")
            {
                date = jobs.FirstDepositDate.Value.Date;
            }
            else if (dateType == "DepositeReceived")
            {
                date = jobs.DepositeRecceivedDate.Value.Date;
            }
            else if (dateType == "Active")
            {
                date = jobs.ActiveDate.Value.Date;
            }
            else if (dateType == "InstallBook")
            {
                date = jobs.InstallationDate.Value.Date;
            }
            else if (dateType == "InstallComplete")
            {
                date = jobs.InstalledcompleteDate.Value.Date;
            }

            var ProductItemId = jobProduct != null ? jobProduct.ProductItemId : 0;
            var ProductItemName = jobProduct != null ? jobProduct.Name : "";

            var productItemList = await _installationItemListRepository.GetAll().Where(e => e.InstallationItemPeriodFk.StartDate.Date <= date && e.InstallationItemPeriodFk.EndDate >= date && e.InstallationItemPeriodFk.OrganizationUnit == jobs.LeadFk.OrganizationId && e.ProductItemId != ProductItemId).FirstOrDefaultAsync();

            return new GetCheckSystemCostOutput
            {
                IsExists = isExists,
                ProductType = productType.Name,
                OrganizationUnit = jobs.LeadFk.OrganizationId,
                ProductItemName = ProductItemName,
                InstallationItemPeriodId = productItemList != null ? productItemList.InstallationItemPeriodId : 0
            };
        }

        public async Task<PagedResultDto<GetAllJobCostMonthWiseViewDto>> GetAllJobCostMonthWise(GetAllJobCostMonthWiseInput input)
        {
            List<(DateTime StartDate, DateTime EndDate, string MonthName, decimal AdCost, decimal FixedExpense)> monthList = new List<(DateTime StartDate, DateTime EndDate, string MonthName, decimal AdCost, decimal FixedExpense)>();
            for (int i = 0; i < 12; i++)
            {
                (DateTime StartDate, DateTime EndDate, string MonthName, decimal AdCost, decimal FixedExpense) item;

                item.StartDate = new DateTime(input.Year, (i + 1), 1);
                item.EndDate = new DateTime(input.Year, (i + 1), DateTime.DaysInMonth(input.Year, (i + 1)));
                item.MonthName = CultureInfo.CurrentUICulture.DateTimeFormat.MonthNames[i];

                item.AdCost = await _leadExpenseInvestmentsRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && e.FromDate.Date >= item.StartDate.Date && e.ToDate.Date <= item.EndDate.Date).SumAsync(e => (decimal)e.Investment);

                item.FixedExpense = await _jobCostFixExpenseListRepository.GetAll().Where(e => e.JobCostFixExpensePeriodFK.OrganizationUnit == input.OrganizationUnit && e.JobCostFixExpensePeriodFK.Month == item.StartDate.Month && e.JobCostFixExpensePeriodFK.Year == item.StartDate.Year).SumAsync(e => e.Amount != null ? (decimal)e.Amount : 0);

                monthList.Add(item);
            }

            var batteryInstallationCosts = await _batteryInstallationCostRepository.GetAll().FirstOrDefaultAsync();

            var fixedCostPrices = _fixedCostPriceRepository.GetAll();
            var rackingCost = await fixedCostPrices.Where(e => e.Name == "Racking").FirstOrDefaultAsync();
            var accessoriesCost = await fixedCostPrices.Where(e => e.Name == "Accessories").FirstOrDefaultAsync();

            var stcCost = await _stcCostRepository.GetAll().Select(e => e.Cost).FirstOrDefaultAsync();
            //var adCostPrice = await fixedCostPrices.Where(e => e.Name == "Ad Cost").FirstOrDefaultAsync();

            input.State = input.State == null ? new List<string>() : input.State;

            var output = (from o in monthList

                          let data = GetMonthlyCostAsync(input.DateType, o.StartDate, o.EndDate, input.OrganizationUnit, batteryInstallationCosts, rackingCost, accessoriesCost, o.AdCost, o.FixedExpense, stcCost, input.State, input.AreaFilter)

                          select new GetAllJobCostMonthWiseViewDto()
                          {
                              Month = o.MonthName,
                              MegaWatt = data.Result.MegaWatt,
                              SystemCapacity = data.Result.SystemCapacity,
                              BatteryCapacity = data.Result.BatteryCapacity,
                              NoOfPanel = data.Result.NoOfPanel,
                              PricePerKw = data.Result.PricePerKw,

                              InstallationCost = data.Result.InstallationCost,
                              BatteryInstallationCost = data.Result.BatteryInstallationCost,

                              PanelCost = data.Result.PanelCost,
                              InverterCost = data.Result.InverterCost,
                              BatteryCost = data.Result.BatteryCost,

                              Racking = data.Result.Racking,
                              Accessories = data.Result.Accessories,
                              AdCost = data.Result.AdCost,
                              FixedExpense = data.Result.FixedExpense,
                              ExtraInstallationCost = data.Result.ExtraInstallationCost,
                              FinanceCost = data.Result.FinanceCost,

                              TotalCost = data.Result.TotalCost,
                              STCRebate = data.Result.STCRebate,
                              CostAfteRebate = data.Result.CostAfteRebate,
                              SellPrice = data.Result.SellPrice,

                              NP = data.Result.NP,
                              NPPercentage = data.Result.NPPercentage,
                              GP = data.Result.GP,
                              NP_SysCap = data.Result.NP_SysCap,
                          });

            var TotalCount = output.Count();


            return new PagedResultDto<GetAllJobCostMonthWiseViewDto>(TotalCount, output.ToList());

        }

        public async Task<FileDto> GetAllJobCostMonthWiseExcel(GetAllJobCostMonthWiseExcelInput input)
        {
            List<(DateTime StartDate, DateTime EndDate, string MonthName, decimal AdCost, decimal FixedExpense)> monthList = new List<(DateTime StartDate, DateTime EndDate, string MonthName, decimal AdCost, decimal FixedExpense)>();
            for (int i = 0; i < 12; i++)
            {
                (DateTime StartDate, DateTime EndDate, string MonthName, decimal AdCost, decimal FixedExpense) item;

                item.StartDate = new DateTime(input.Year, (i + 1), 1);
                item.EndDate = new DateTime(input.Year, (i + 1), DateTime.DaysInMonth(input.Year, (i + 1)));
                item.MonthName = CultureInfo.CurrentUICulture.DateTimeFormat.MonthNames[i];

                item.AdCost = await _leadExpenseInvestmentsRepository.GetAll().Where(e => e.OrganizationId == input.OrganizationUnit && e.FromDate.Date >= item.StartDate.Date && e.ToDate.Date <= item.EndDate.Date).SumAsync(e => (decimal)e.Investment);

                item.FixedExpense = await _jobCostFixExpenseListRepository.GetAll().Where(e => e.JobCostFixExpensePeriodFK.OrganizationUnit == input.OrganizationUnit && e.JobCostFixExpensePeriodFK.Month == item.StartDate.Month && e.JobCostFixExpensePeriodFK.Year == item.StartDate.Year).SumAsync(e => e.Amount != null ? (decimal)e.Amount : 0);

                monthList.Add(item);
            }

            var batteryInstallationCosts = await _batteryInstallationCostRepository.GetAll().FirstOrDefaultAsync();

            var fixedCostPrices = _fixedCostPriceRepository.GetAll();
            var rackingCost = await fixedCostPrices.Where(e => e.Name == "Racking").FirstOrDefaultAsync();
            var accessoriesCost = await fixedCostPrices.Where(e => e.Name == "Accessories").FirstOrDefaultAsync();

            var stcCost = await _stcCostRepository.GetAll().Select(e => e.Cost).FirstOrDefaultAsync();

            var output = (from o in monthList

                          let data = GetMonthlyCostAsync(input.DateType, o.StartDate, o.EndDate, input.OrganizationUnit, batteryInstallationCosts, rackingCost, accessoriesCost, o.AdCost, o.FixedExpense, stcCost, input.State, input.AreaFilter)

                          select new GetAllJobCostMonthWiseViewDto()
                          {
                              Month = o.MonthName,
                              MegaWatt = data.Result.MegaWatt,
                              SystemCapacity = data.Result.SystemCapacity,
                              BatteryCapacity = data.Result.BatteryCapacity,
                              NoOfPanel = data.Result.NoOfPanel,
                              PricePerKw = data.Result.PricePerKw,

                              InstallationCost = data.Result.InstallationCost,
                              BatteryInstallationCost = data.Result.BatteryInstallationCost,

                              PanelCost = data.Result.PanelCost,
                              InverterCost = data.Result.InverterCost,
                              BatteryCost = data.Result.BatteryCost,

                              Racking = data.Result.Racking,
                              Accessories = data.Result.Accessories,
                              AdCost = data.Result.AdCost,
                              FixedExpense = data.Result.FixedExpense,
                              ExtraInstallationCost = data.Result.ExtraInstallationCost,
                              FinanceCost = data.Result.FinanceCost,

                              TotalCost = data.Result.TotalCost,
                              STCRebate = data.Result.STCRebate,
                              CostAfteRebate = data.Result.CostAfteRebate,
                              SellPrice = data.Result.SellPrice,

                              NP = data.Result.NP,
                              NPPercentage = data.Result.NPPercentage,
                              GP = data.Result.GP,
                              NP_SysCap = data.Result.NP_SysCap,
                          });

            return _jobCostExcelExporter.ExportToFileJobCostMonthWise(output.ToList());

        }

        protected async Task<GetAllJobCostMonthWiseViewDto> GetMonthlyCostAsync(string dateType, DateTime startDate, DateTime endDate, int OrganizationUnitId, BatteryInstallationCost batteryInsCost, FixedCostPrice rackingCost, FixedCostPrice accessoriesCost, decimal adCost, decimal fixedExpense, decimal stcCost, List<string> states, string areaFilter)
        {
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var jobs = _jobRepository.GetAll().Where(e => e.LeadFk.OrganizationId == OrganizationUnitId)
                        .WhereIf(dateType == "CreationDate", e => e.CreationTime.AddHours(diffHour).Date >= startDate.Date && e.CreationTime.AddHours(diffHour).Date <= endDate.Date)
                        .WhereIf(dateType == "FirstDeposite", e => e.FirstDepositDate.Value.Date >= startDate.Date && e.FirstDepositDate.Value.Date <= endDate.Date)
                        .WhereIf(dateType == "DepositeReceived", e => e.DepositeRecceivedDate.Value.Date >= startDate.Date && e.DepositeRecceivedDate.Value.Date <= endDate.Date)
                        .WhereIf(dateType == "Active", e => e.ActiveDate.Value.Date >= startDate.Date && e.ActiveDate.Value.Date <= endDate.Date)
                        .WhereIf(dateType == "InstallBook", e => e.InstallationDate.Value.Date >= startDate.Date && e.InstallationDate.Value.Date <= endDate.Date)
                        .WhereIf(dateType == "InstallComplete", e => e.InstalledcompleteDate.Value.Date >= startDate.Date && e.InstalledcompleteDate.Value.Date <= endDate.Date)

                        .WhereIf(states != null && states.Count > 0, e => states.Contains(e.State))
                        .WhereIf(!string.IsNullOrEmpty(areaFilter), e => e.LeadFk.Area == areaFilter)

                        .AsNoTracking()
                        .Select(e => new
                        {
                            e.Id,
                            e.LeadFk.OrganizationId,
                            e.SystemCapacity,
                            e.BatteryKw,
                            TotalCost = (e.TotalCost != null ? (decimal)e.TotalCost : 0),
                            e.PostalCode,
                            e.State,
                            Area = e.LeadFk.Area,
                            e.HouseTypeId,
                            e.RoofTypeId,
                            e.RoofAngleId,
                            e.STC,

                            FinancePercentage = e.PaymentOptionId == 1 ? 0 : _financeOptionRepository.GetAll().Where(f => f.Id == e.FinanceOptionId).Select(f => f.Percentage).FirstOrDefault(),

                            Date = (dateType == "CreationDate" ? e.CreationTime.AddHours(diffHour).Date : dateType == "FirstDeposite" ? e.FirstDepositDate.Value.Date : dateType == "DepositeReceived" ? e.DepositeRecceivedDate.Value.Date : dateType == "Active" ? e.ActiveDate.Value.Date : dateType == "InstallBook" ? e.InstallationDate.Value.Date : dateType == "InstallComplete" ? e.InstalledcompleteDate.Value.Date : DateTime.UtcNow.AddHours(diffHour).Date)
                        });

            var jobProduct = from jp in _jobProductItemRepository.GetAll()
                             join job in jobs on jp.JobId equals job.Id

                             select new
                             {
                                 job.Id,
                                 jp.ProductItemFk.ProductTypeId,
                                 Quantity = (jp.Quantity == null ? 0 : jp.Quantity),

                                 Size = jp.ProductItemFk.ProductTypeId == 5 ? jp.ProductItemFk.Size : 0,

                                 job.SystemCapacity
                             };

            var jobProductForCost = from jp in _jobProductItemRepository.GetAll()
                                    join job in jobs on jp.JobId equals job.Id
                                    join ip in _installationItemListRepository.GetAll() on jp.ProductItemId equals ip.ProductItemId

                                    where ip.InstallationItemPeriodFk.OrganizationUnit == job.OrganizationId

                                    && (job.Date >= ip.InstallationItemPeriodFk.StartDate && job.Date <= ip.InstallationItemPeriodFk.EndDate)

                                    let insInvoiceCost = _installerInvoiceRepository.GetAll().Where(e => e.JobId == job.Id && e.Installation_Maintenance_Inspection == "Installation")

                                    select new
                                    {
                                        job.Id,
                                        jp.ProductItemFk.ProductTypeId,
                                        Quantity = (jp.Quantity == null ? 0 : jp.Quantity),
                                        UnitPrice = (ip.UnitPrice == null ? 0 : ip.UnitPrice),
                                    };

            var sellPrice = await jobs.SumAsync(e => e.TotalCost);
            var systemCapacity = await jobs.SumAsync(e => e.SystemCapacity);
            var batteryCapacity = await jobs.SumAsync(e => e.BatteryKw);
            var noOfPanel = await jobProduct.Where(e => e.ProductTypeId == 1).SumAsync(e => e.Quantity);

            var panelCost = await jobProductForCost.Where(e => e.ProductTypeId == 1).SumAsync(e => e.Quantity * e.UnitPrice);
            var inverterCost = await jobProductForCost.Where(e => e.ProductTypeId == 2).SumAsync(e => e.Quantity * e.UnitPrice);
            var batteryCost = await jobProductForCost.Where(e => e.ProductTypeId == 5).SumAsync(e => e.Quantity * e.UnitPrice);

            var jobInstallation = (from job in jobs

                                   let insInv = _installerInvoiceRepository.GetAll().Any(e => e.JobId == job.Id && e.Installation_Maintenance_Inspection == "Installation")

                                   let postCodePrice = !insInv ? _postoCodePriceRepository.GetAll().Where(e => e.PostCode == job.PostalCode && e.PostCodePricePeriodFk.Month == job.Date.Month && e.PostCodePricePeriodFk.Year == job.Date.Year).Select(e => (e.Price * job.SystemCapacity)).FirstOrDefault() : 0

                                   select new
                                   {
                                       InstallationCost = insInv ? _installerInvoiceRepository.GetAll().Where(e => e.JobId == job.Id).Sum(e => e.Amount) : postCodePrice,
                                   }).ToList();

            var installationCost = jobInstallation.Sum(e => e.InstallationCost);

            var batteryInstallationCost = await jobProduct.Where(e => e.ProductTypeId == 5).GroupBy(e => e.Id).Select(e => batteryInsCost.FixCost + (e.Sum(s => s.Size * s.Quantity) > batteryInsCost.Kw ? ((e.Sum(s => s.Size * s.Quantity) - batteryInsCost.Kw) * batteryInsCost.ExtraCost) : 0)).SumAsync();

            var racking = await jobProduct.Where(e => e.ProductTypeId == 1).GroupBy(e => new { e.Id, e.SystemCapacity }).Select(e => (rackingCost.Type == "Panel" ? (rackingCost.Cost * e.Sum(s => s.Quantity)) : rackingCost.Type == "Kw" ? (rackingCost.Cost * e.Key.SystemCapacity) : rackingCost.Type == "Job" ? (rackingCost.Cost) : 0)).SumAsync();

            var accessories = await jobProduct.Where(e => e.ProductTypeId == 1).GroupBy(e => new { e.Id, e.SystemCapacity }).Select(e => (accessoriesCost.Type == "Panel" ? (accessoriesCost.Cost * e.Sum(s => s.Quantity)) : accessoriesCost.Type == "Kw" ? (accessoriesCost.Cost * e.Key.SystemCapacity) : accessoriesCost.Type == "Job" ? (accessoriesCost.Cost) : 0)).SumAsync();

            var jobProductForExtraCost = from eic in _extraInstallationChargeRepository.GetAll()
                                         join job in jobs on eic.StateFk.Name equals job.State

                                         let panel = _jobProductItemRepository.GetAll().Where(e => e.ProductItemFk.ProductTypeId == 1 && e.JobId == job.Id).Sum(e => e.Quantity)

                                         select new
                                         {
                                             job.Id,
                                             job.SystemCapacity,
                                             job.HouseTypeId,
                                             job.RoofTypeId,
                                             job.RoofAngleId,
                                             eic.Type,
                                             Amount = job.Area == "Metro" ? eic.MetroAmount : job.Area == "Regional" ? eic.RegionalAmount : 0,
                                             eic.Category,
                                             eic.NameId,
                                             NoOfPanel = panel,
                                         };

            var houseType = jobProductForExtraCost.ToList().Where(e => e.Category == "House Type" && e.NameId == e.HouseTypeId).Select(e => (e.Type == "Panel" ? (e.Amount * e.NoOfPanel) : e.Type == "Kw" ? (e.Amount * e.SystemCapacity) : e.Type == "Job" ? (e.Amount) : 0)).Sum();

            var roofType = jobProductForExtraCost.ToList().Where(e => e.Category == "Roof Type" && e.NameId == e.RoofTypeId).Select(e => (e.Type == "Panel" ? (e.Amount * e.NoOfPanel) : e.Type == "Kw" ? (e.Amount * e.SystemCapacity) : e.Type == "Job" ? (e.Amount) : 0)).Sum();

            var roofAngle = jobProductForExtraCost.ToList().Where(e => e.Category == "Roof Angle" && e.NameId == e.RoofAngleId).Select(e => (e.Type == "Panel" ? (e.Amount * e.NoOfPanel) : e.Type == "Kw" ? (e.Amount * e.SystemCapacity) : e.Type == "Job" ? (e.Amount) : 0)).Sum();

            var jobVariation = from o in jobProductForExtraCost.Where(e => e.Category == "Price Variation")
                               join jv in _jobVariationRepository.GetAll() on o.NameId equals jv.Id
                               where jv.JobId == o.Id

                               select new
                               {
                                   o.SystemCapacity,
                                   o.Amount,
                                   o.Type,
                                   o.NoOfPanel
                               };

            var jVCost = jobVariation.ToList().Select(e => (e.Type == "Panel" ? (e.Amount * e.NoOfPanel) : e.Type == "Kw" ? (e.Amount * e.SystemCapacity) : e.Type == "Job" ? (e.Amount) : 0)).Sum();

            var extraInstallationCost = (houseType != null ? (decimal)houseType : 0) + (roofType != null ? (decimal)roofType : 0) + (roofAngle != null ? (decimal)roofAngle : 0) + (jVCost != null ? (decimal)jVCost : 0);

            var financeCost = jobs.ToList().Sum(e => e.TotalCost > 0 ? e.FinancePercentage > 0 ? (e.TotalCost * e.FinancePercentage) / 100 : 0 : 0);

            var stcRebate = await jobs.SumAsync(e => stcCost * e.STC);

            // Output Dtp
            var output = new GetAllJobCostMonthWiseViewDto();

            output.MegaWatt = ((systemCapacity != null ? (decimal)systemCapacity : 0) / 1000);
            output.SystemCapacity = (systemCapacity != null ? (decimal)systemCapacity : 0);
            output.BatteryCapacity = (batteryCapacity != null ? (decimal)batteryCapacity : 0);
            output.NoOfPanel = (noOfPanel != null ? (int)noOfPanel : 0);
            output.PricePerKw = output.SystemCapacity > 0 ? (sellPrice / (output.SystemCapacity + output.BatteryCapacity)) : 0;

            output.InstallationCost = (installationCost != null ? (decimal)installationCost : 0);
            output.BatteryInstallationCost = (batteryInstallationCost != null ? (decimal)batteryInstallationCost : 0);

            output.PanelCost = (panelCost != null ? (decimal)panelCost : 0);
            output.InverterCost = (inverterCost != null ? (decimal)inverterCost : 0);
            output.BatteryCost = (batteryCost != null ? (decimal)batteryCost : 0);

            output.Racking = (racking != null ? (decimal)racking : 0);
            output.Accessories = (accessories != null ? (decimal)accessories : 0);
            output.AdCost = adCost;
            output.FixedExpense = fixedExpense;
            output.ExtraInstallationCost = extraInstallationCost;
            output.FinanceCost = (financeCost != null ? (decimal)financeCost : 0);

            output.TotalCost = (output.PanelCost + output.InverterCost + output.BatteryCost + output.InstallationCost + output.BatteryInstallationCost + output.Racking + output.Accessories + output.AdCost + output.FixedExpense + output.ExtraInstallationCost + output.FinanceCost);

            output.STCRebate = (stcRebate != null ? (decimal)stcRebate : 0);

            output.CostAfteRebate = (output.TotalCost - output.STCRebate);

            output.SellPrice = sellPrice;

            output.NP = output.PanelCost > 0 && output.InverterCost > 0 ? (output.SellPrice - output.CostAfteRebate) : 0;
            output.NPPercentage = output.PanelCost > 0 && output.InverterCost > 0 ? (output.TotalCost > 0 ? ((output.NP * 100) / output.TotalCost) : 0) : 0;

            output.GP = output.PanelCost > 0 && output.InverterCost > 0 ? (output.NP + output.AdCost) : 0;
            output.NP_SysCap = output.PanelCost > 0 && output.InverterCost > 0 && output.SystemCapacity > 0 ? (output.NP / output.SystemCapacity) : 0;

            return output;
        }


        public async Task<PagedResultDto<SalesRepProfitLossDto>> GetEmpProfitReport(GetAllJobCostInput input)
        {

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var team_list = _userTeamRepository.GetAll();

            var User_List = _userRepository.GetAll();
            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = team_list.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var command = _dbcontextprovider.GetDbContext().Database.GetDbConnection().CreateCommand();
            command.CommandText = "GetJobCost";
            command.CommandType = CommandType.StoredProcedure;
            command.Transaction = GetActiveTransaction();
            command.CommandTimeout = 100000;

            var parameter = command.CreateParameter();
            parameter.ParameterName = "@JobNumber";
            if (!string.IsNullOrWhiteSpace(input.Filter))
                parameter.Value = input.Filter;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@OrganizationId";
            parameter.DbType = DbType.Int32;
            parameter.Value = input.OrganizationUnit;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DateType";
            if (!string.IsNullOrWhiteSpace(input.DateType))
                parameter.Value = input.DateType;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@DiffHour";
            parameter.Value = diffHour;
            parameter.DbType = DbType.Double;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartDate";
            if (SDate != null)
                parameter.Value = SDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndDate";
            if (EDate != null)
                parameter.Value = EDate.Value.Date;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.DateTime;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@CurruntDate";
            if (!string.IsNullOrWhiteSpace(input.CurruntDate))
                parameter.Value = input.CurruntDate;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@State";
            if (!string.IsNullOrWhiteSpace(input.State))
                parameter.Value = input.State;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Installation";
            if (input.Installation != null)
                parameter.Value = input.Installation;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@StartSysCapRange";
            if (input.StartSysCapRange != null && input.StartSysCapRange > 0)
                parameter.Value = input.StartSysCapRange;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@EndSysCapRange";
            if (input.EndSysCapRange != null && input.EndSysCapRange > 0)
                parameter.Value = input.EndSysCapRange;
            else
                parameter.Value = DBNull.Value;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);

            //command.Connection.Open();
            DbDataReader reader = command.ExecuteReader();
            //command.Connection.Open();
            var table = new DataTable();
            table.Load(reader);

            var result = from item in table.AsEnumerable()
                         select new GetAllJobCostViewDto()
                         {
                             Id = item.Field<int>("Id"),
                             JobNumber = item.Field<string>("JobNUmber"),
                             State = item.Field<string>("State"),
                             PostCode = item.Field<string>("PostalCode"),
                             AreaName = item.Field<string>("Area"),
                             Team = item.Field<string>("TeamName"),
                             SalesRep = item.Field<string>("SalesRep"),
                             PanelSize = item.Field<decimal?>("PanelSize"),
                             NoOfPanel = item.Field<int?>("NoOfPanel"),
                             SystemCapacity = item.Field<decimal?>("SystemCapacity"),
                             PanelCost = item.Field<decimal?>("PanelCost") != null ? item.Field<decimal?>("PanelCost") : 0,
                             InverterCost = item.Field<decimal?>("InverterCost") != null ? item.Field<decimal?>("InverterCost") : 0,
                             BatteryCost = item.Field<decimal?>("BatteryCost") != null ? item.Field<decimal?>("BatteryCost") : 0,
                             InstallationCost = item.Field<decimal?>("InstallationCost") != null ? item.Field<decimal?>("InstallationCost") : 0,
                             InstallationCostType = item.Field<string>("InstallationCostType"),
                             BatteryInstallation = item.Field<decimal?>("BatteryInstallation"),
                             Racking = item.Field<decimal?>("Racking"),
                             Accessories = item.Field<decimal?>("Accessories"),
                             AdCost = item.Field<decimal?>("AdCost"),
                             FixedExpense = item.Field<decimal?>("FixedExpense"),
                             ExtraInsCost = (item.Field<decimal?>("HouseType") != null ? item.Field<decimal?>("HouseType") : 0) + (item.Field<decimal?>("RoofType") != null ? item.Field<decimal?>("RoofType") : 0) + (item.Field<decimal?>("RoofAngle") != null ? item.Field<decimal?>("RoofAngle") : 0) + (item.Field<decimal?>("Variation") != null ? item.Field<decimal?>("Variation") : 0),

                             STCRebate = item.Field<decimal?>("STCRebate"),
                             SellPrice = item.Field<decimal?>("TotalCost"),

                             JobStatusId = item.Field<int?>("JobStatusId"),
                             AssignToUserID = item.Field<int?>("AssignToUserID"),
                             FinOptionPercentage = item.Field<decimal?>("FinancePercentage"),
                             CategoryDate = item.Field<DateTime?>("CategoryDate"),
                             BatteryKw = item.Field<decimal?>("BatteryKw"),

                         };

            IQueryable<GetAllJobCostViewDto> itemDtos = result.AsQueryable();

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };
            var filteredJobs = itemDtos
            .WhereIf(!role.Contains("Admin") && !role.Contains("Sales Rep") && !role.Contains("Sales Manager"), e => e.JobStatusId != 1)
            .WhereIf(role.Contains("Admin"), e => e.AssignToUserID != null)
            .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                        .WhereIf(role.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                        .WhereIf(input.JobStatus != null && input.JobStatus.Count > 0, e => input.JobStatus.Contains(e.JobStatusId))
                        .WhereIf(input.isEmpJobCost == true && role.Contains("Sales Rep"), e => e.SalesRep == User.FullName)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.SalesRap), e => e.SalesRep == input.SalesRap)
                        //.WhereIf(input.Cost == 1, e => (e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0))

                        .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.AreaName == input.AreaName)

                        .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

                        .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
                        .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                        .WhereIf(input.SystemFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                        .WhereIf(input.SystemFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                        .WhereIf(input.SystemFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery

                        .WhereIf(input.SystemFilter == 0 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0) // For All Without Sys Filter
                        .WhereIf(input.SystemFilter == 1 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.BatteryCost == 0 || e.InstallationCost == 0) // For Full System
                        .WhereIf(input.SystemFilter == 2 && input.Cost == 1, e => e.PanelCost == 0 || e.InverterCost == 0 || e.InstallationCost == 0) // For Without Battery
                        .WhereIf(input.SystemFilter == 3 && input.Cost == 1, e => e.BatteryCost == 0) // For Only Battery
                        .WhereIf(input.SystemFilter == 4 && input.Cost == 1, e => e.InverterCost == 0) // For Only Inverter
                        .WhereIf(input.SystemFilter == 5 && input.Cost == 1, e => e.InverterCost == 0 || e.BatteryCost == 0) // For Inverter and Battery Only

                        .WhereIf(input.ProductItemId != null && input.ProductItemId > 0, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemId == input.ProductItemId))
                        ;

            var salesRepProfitLoss = filteredJobs
                    .GroupBy(job => job.SalesRep)
                    .Select(group => new SalesRepProfitLossDto
                    {
                        SalesRep = group.Key,
                        BatteryKw = group.Sum(job => job.BatteryKw),
                        SystemKw = group.Sum(job => job.SystemCapacity),
                        Profit = group.Sum(job => CalculateNetProfit(job) > 0 ? CalculateNetProfit(job) : 0),
                        Loss = group.Sum(job => CalculateNetProfit(job) < 0 ? Math.Abs(CalculateNetProfit(job)) : 0)
                    })
                    .ToList();

            var jobs = salesRepProfitLoss.WhereIf(!string.IsNullOrEmpty(input.Category), e => e.Category == input.Category);

            var jobsList = jobs.ToList();

            var totalCount = jobs.Count();

            return new PagedResultDto<SalesRepProfitLossDto>(totalCount, jobsList);

        }
        private decimal CalculateNetProfit(GetAllJobCostViewDto job)
        {


            var finOptionPercentage = job.FinOptionPercentage != null ? (decimal)job.FinOptionPercentage : 0;
            var totalPrice = job.SellPrice != null ? (decimal)job.SellPrice : 0;

            var financeCost = finOptionPercentage != 0 && totalPrice != 0 ? ((totalPrice * finOptionPercentage) / 100) : 0;

            var totalCost = ((job.PanelCost != null ? (decimal)job.PanelCost : 0)
                + (job.InverterCost != null ? (decimal)job.InverterCost : 0) +
                (job.BatteryCost != null ? (decimal)job.BatteryCost : 0)
                + (job.InstallationCost != null ? (decimal)job.InstallationCost : 0)
                + (job.BatteryInstallation != null ? (decimal)job.BatteryInstallation : 0) +
                (job.Racking != null ? (decimal)job.Racking : 0) +
                (job.Accessories != null ? (decimal)job.Accessories : 0) +
                (job.AdCost != null ? (decimal)job.AdCost : 0) +
                (job.FixedExpense != null ? (decimal)job.FixedExpense : 0) +
                (job.ExtraInsCost != null ? (decimal)job.ExtraInsCost : 0) + financeCost);




            var rebate = job.STCRebate ?? 0;
            var sellPrice = (job.SellPrice != null ? (decimal)job.SellPrice : 0);

            return sellPrice - (totalCost - rebate);
        }

        public async Task<PagedResultDto<GetAllProgressReportDto>> GetAllProgressReport(GetAllProgressReportInput input)
        {
            var SDate = _timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            var EDate = _timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

            var jobs = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrganizationId)
                .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.JobNumber == input.Filter)
                .WhereIf(input.Datefilter == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                .WhereIf(input.Datefilter == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate.Value.Date <= EDate.Value.Date)
                .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                .AsNoTracking()
                .Select(e => new
                {
                    e.Id,
                    e.JobNumber,
                    e.State,
                    e.SystemCapacity,
                    e.CreationTime,
                    e.InstallationDate,
                    e.InstalledcompleteDate,
                    e.DepositeRecceivedDate,
                    e.FirstDepositDate,
                    e.ActiveDate
                });

            var output = from o in jobs
                         select new GetAllProgressReportDto()
                         {
                             Id = o.Id,
                             JobNumber = o.JobNumber,
                             State = o.State,
                             SystemkW = o.SystemCapacity,

                             FirstDepositDays = (o.FirstDepositDate != null && o.CreationTime != null)
                                ? (o.FirstDepositDate.Value.Date - o.CreationTime.Date).Days
                                : (int?)null,

                             ActiveDays = (o.ActiveDate != null && o.FirstDepositDate != null)
                                ? (o.ActiveDate.Value.Date - o.FirstDepositDate.Value.Date).Days
                                : (int?)null,

                             InstallationBookDays = (o.InstallationDate != null && o.ActiveDate != null)
                                ? (o.InstallationDate.Value.Date - o.ActiveDate.Value.Date).Days
                                : (int?)null,

                             InstallationCompleteDays = (o.InstalledcompleteDate != null && o.InstallationDate != null)
                                ? (o.InstalledcompleteDate.Value.Date - o.InstallationDate.Value.Date).Days
                                : (int?)null,
                         };

            var pagedAndFiltered = output
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var totalCount = await jobs.CountAsync();

            return new PagedResultDto<GetAllProgressReportDto>(totalCount, await output.ToListAsync());
        }

        public async Task<FileDto> GetAllProgressReportToExcel(GetAllProgressReportToExcelInput input)
        {
            var SDate = _timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            var EDate = _timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

            var jobs = _jobRepository.GetAll().Include(e => e.LeadFk).Where(e => e.LeadFk.OrganizationId == input.OrganizationId)
                .WhereIf(!string.IsNullOrEmpty(input.Filter), e => e.JobNumber == input.Filter)
                .WhereIf(input.Datefilter == "FirstDeposite" && input.StartDate != null, e => e.FirstDepositDate != null && e.FirstDepositDate.Value.Date >= SDate.Value.Date)
                .WhereIf(input.Datefilter == "FirstDeposite" && input.EndDate != null, e => e.FirstDepositDate != null && e.FirstDepositDate.Value.Date <= EDate.Value.Date)
                .WhereIf(!string.IsNullOrWhiteSpace(input.StateFilter), e => e.State == input.StateFilter)
                .AsNoTracking()
                .Select(e => new
                {
                    e.Id,
                    e.JobNumber,
                    e.State,
                    e.SystemCapacity,
                    e.CreationTime,
                    e.InstallationDate,
                    e.InstalledcompleteDate,
                    e.DepositeRecceivedDate,
                    e.FirstDepositDate,
                    e.ActiveDate
                });

            var output = from o in jobs
                         select new GetAllProgressReportDto()
                         {
                             JobNumber = o.JobNumber,
                             State = o.State,
                             SystemkW = o.SystemCapacity,

                             FirstDepositDays = (o.FirstDepositDate != null && o.CreationTime != null)
                                ? (o.FirstDepositDate.Value.Date - o.CreationTime.Date).Days
                                : (int?)null,

                             ActiveDays = (o.ActiveDate != null && o.FirstDepositDate != null)
                                ? (o.ActiveDate.Value.Date - o.FirstDepositDate.Value.Date).Days
                                : (int?)null,

                             InstallationBookDays = (o.InstallationDate != null && o.ActiveDate != null)
                                ? (o.InstallationDate.Value.Date - o.ActiveDate.Value.Date).Days
                                : (int?)null,

                             InstallationCompleteDays = (o.InstalledcompleteDate != null && o.InstallationDate != null)
                                ? (o.InstalledcompleteDate.Value.Date - o.InstallationDate.Value.Date).Days
                                : (int?)null,
                         };

            return _jobCostExcelExporter.ExportToFileProgressReport(await output.ToListAsync());
        }
    }
}



