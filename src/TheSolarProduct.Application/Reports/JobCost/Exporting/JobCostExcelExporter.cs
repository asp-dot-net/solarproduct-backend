﻿using Abp.Collections.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheSolarProduct.CancelReasons.Dtos;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.JobCost.Dtos;
using TheSolarProduct.Reports.ProgressReport;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Reports.JobCost.Exporting
{
    public class JobCostExcelExporter : NpoiExcelExporterBase, IJobCostExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobCostExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllJobCostViewForExcelExportDto> jobCosts)
        {
            var Header = new string[] { "Job Number", "State", "Post Code", "Area", "Team", "Sales Rep", "Job Type", "Job Status", "System Details", "Panel Size", "No Of Panel", "PricePerKw", "System Capacity", "Battery KW", "Panel Cost", "Inverter Cost", "Battery Cost", "Installation Cost", "Installation Cost Type", "Battery Installation", "Racking", "Accessories", "Ad Cost", "Fixed Expense", "Extra Ins Cost", "Finance Cost", "Total Cost", "STC Rebate", "Cost After STC Rebate", "Sell Price", "NP", "NP%", "GP", "NP/SysCap", "Category" };

            return CreateExcelPackage(
                "JobCost.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("JobCost"));

                    AddHeader(sheet, Header);

                    AddObjects(
                        sheet, 2, jobCosts,
                        _ => _.JobNumber,
                        _ => _.State,
                        _ => _.PostalCode,
                        _ => _.AreaName,
                        _ => _.Team,
                        _ => _.SalesRep,
                        _ => _.JobType,
                        _ => _.JobStatus,
                        _ => _.SystemDetails != null ? _.SystemDetails.Select(e => e).JoinAsString(" + ") : "",
                        _ => _.PanelSize,
                        _ => _.NoOfPanel,
                        _ => _.PricePerKw,
                        _ => _.SystemCapacity,
                        _ => _.BatteryKw,
                        _ => _.PanelCost,
                        _ => _.InverterCost,
                        _ => _.BatteryCost,
                        _ => _.InstallationCost,
                        _ => _.InstallationCostType,
                        _ => _.BatteryInstallation,
                        _ => _.Racking,
                        _ => _.Accessories,
                        _ => _.AdCost,
                        _ => _.FixedExpense,
                        _ => _.ExtraInsCost,
                        _ => _.FinanceCost,
                        _ => _.TotalCost,
                        _ => _.STCRebate,
                        _ => _.CostAfteRebate,
                        _ => _.SellPrice,
                        _ => _.NP,
                        _ => _.NPPercentage,
                        _ => _.GP,
                        _ => _.NP_SysCap,
                        _ => _.Category
                        );

                    //for (var i = 1; i <= jobCosts.Count; i++)
                    //{
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "0");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[11], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[13], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[14], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[15], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[16], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[17], "0.00");
                    //    //SetdecimalCellDataFormat(sheet.GetRow(i).Cells[18], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[19], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[20], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[21], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[22], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[23], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[24], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[25], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[26], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[27], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[28], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[29], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[30], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[31], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[32], "0.00");
                    //    SetdecimalCellDataFormat(sheet.GetRow(i).Cells[33], "0.00");
                    //}
                });
        }
        public FileDto ExportToFileEmpJobCost(List<GetAllJobCostViewForExcelExportDto> jobCosts)
        {
            var Header = new string[] { "Job Number", "Sales Rape", "State", "Area", "System Details", "Panel Size", "No Of Panel", "System Capacity","Battery KW", "Total Cost", "STC Rebate", "Cost After STC Rebate", "Sell Price", "NP" };
            return CreateExcelPackage(
                "EmpJobCost.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("JobCost"));

                    AddHeader(sheet, Header);

                    AddObjects(
                        sheet, 2, jobCosts,
                        _ => _.JobNumber,
                        _ => _.SalesRep,
                        _ => _.State,
                        _ => _.AreaName,
                        _ => _.SystemDetails != null ? _.SystemDetails.Select(e => e).JoinAsString(" + ") : "",
                        _ => _.PanelSize,
                        _ => _.NoOfPanel,
                        _ => _.SystemCapacity,
                        _ => _.BatteryKw,
                        _ => _.TotalCost,
                        _ => _.STCRebate,
                        _ => _.CostAfteRebate,
                        _ => _.SellPrice,
                        _ => _.NP
                        );

                    for (var i = 1; i <= jobCosts.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[6], "00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[11], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[13], "0.00");
                    }
                });
        }

        public FileDto ExportToFileJobCostMonthWise(List<GetAllJobCostMonthWiseViewDto> jobCosts)
        {
            var Header = new string[] { "Month", "MegaWatt", "System Capacity", "Panel Cost", "Inverter Cost", "Battery Cost", "Racking", "Accessories", "Installation Cost", "Ad Cost", "Fixed Expense", "Extra Ins Cost", "Finance Cost", "Total Cost", "STC Rebate", "Cost After STC Rebate", "Sell Price", "NP", "NP%", "GP", "NP/SysCap" };

            return CreateExcelPackage(
                "JobCostMonthWise.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("JobCostMonthWise"));

                    AddHeader(sheet, Header);

                    AddObjects(
                        sheet, 2, jobCosts,
                        _ => _.Month,
                        _ => _.MegaWatt,
                        _ => _.SystemCapacity,
                        _ => _.PanelCost,
                        _ => _.InverterCost,
                        _ => _.BatteryCost,
                        _ => _.Racking,
                        _ => _.Accessories,
                        _ => _.InstallationCost,
                        _ => _.AdCost,
                        _ => _.FixedExpense,
                        _ => _.ExtraInstallationCost,
                        _ => _.FinanceCost,
                        _ => _.TotalCost,
                        _ => _.STCRebate,
                        _ => _.CostAfteRebate,
                        _ => _.SellPrice,
                        _ => _.NP,
                        _ => _.NPPercentage,
                        _ => _.GP,
                        _ => _.NP_SysCap
                        );

                    for (var i = 1; i <= jobCosts.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[2], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[3], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[4], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[5], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[8], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[9], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[10], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[11], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[12], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[13], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[14], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[15], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[16], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[17], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[18], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[19], "0.00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[20], "0.00");
                    }
                });
        }

        public FileDto ExportToFileProgressReport(List<GetAllProgressReportDto> ProgressReport)
        {
            var Header = new string[] { "Job Number", "State",  "System Kw", "First Deposit Days", "Active Days", "Installation Book Days", "Installation Complete Days" };
            return CreateExcelPackage(
                "ProgressReport.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ProgressReport"));

                    AddHeader(sheet, Header);

                    AddObjects(
                        sheet, 2, ProgressReport,
                        _ => _.JobNumber,
                        _ => _.State,
                        _ => _.SystemkW,
                        _ => _.FirstDepositDays,
                        _ => _.ActiveDays,
                        _ => _.InstallationBookDays,
                        _ => _.InstallationCompleteDays
                       
                        );

                    for (var i = 1; i <= ProgressReport.Count; i++)
                    {
                       
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[2], "0.00");
                       
                    }
                });
        }
        
    }
}
