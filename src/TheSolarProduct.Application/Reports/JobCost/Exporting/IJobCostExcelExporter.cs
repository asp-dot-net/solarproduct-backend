﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Migrations;
using TheSolarProduct.Reports.JobCost.Dtos;
using TheSolarProduct.Reports.ProgressReport;

namespace TheSolarProduct.Reports.JobCost.Exporting
{
    public interface IJobCostExcelExporter
    {
        FileDto ExportToFile(List<GetAllJobCostViewForExcelExportDto> jobCosts);
        
        FileDto ExportToFileEmpJobCost(List<GetAllJobCostViewForExcelExportDto> jobCosts);

        FileDto ExportToFileJobCostMonthWise(List<GetAllJobCostMonthWiseViewDto> jobCosts);
        
        FileDto ExportToFileProgressReport(List<GetAllProgressReportDto> ProgressReport);
    }
}
