﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Timing.Timezone;
using Abp.UI;
using Hangfire.Common;
using NPOI.SS.Formula.Functions;
using System.Data;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using TheSolarProduct.Authorization.Roles;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs;
using TheSolarProduct.Leads;
using TheSolarProduct.Leads.Exporting;
using TheSolarProduct.LeadSources;
using TheSolarProduct.Reports.CallHistorys.UserCallHistorys.Dtos;
using TheSolarProduct.Reports.LeadSolds.Dtos;
using TheSolarProduct.TheSolarDemo;
using TheSolarProduct.Timing;
using Job = TheSolarProduct.Jobs.Job;

using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.Internal;

namespace TheSolarProduct.Reports.LeadSolds
{
    [AbpAuthorize]
    public class LeadSoldReportAppService : TheSolarProductAppServiceBase, ILeadSoldReportAppService
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadSource> _leadSourceRepository;
        private readonly IRepository<Lead> _leadRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IRepository<UserTeam> _userTeamRepository;

        public LeadSoldReportAppService(IRepository<User, long> userRepository, UserManager userManager, IRepository<LeadSource> leadSourceRepository, IRepository<Lead> leadRepository, IRepository<Job> jobRepository, ITimeZoneConverter timeZoneConverter, ITimeZoneService timeZoneService, ILeadsExcelExporter leadsExcelExporter, IRepository<UserTeam> userTeamRepository)
        {
            _userRepository = userRepository;
            _userManager = userManager;
            _leadSourceRepository = leadSourceRepository;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _leadsExcelExporter = leadsExcelExporter;
            _userTeamRepository = userTeamRepository;
        }

        public async Task<PagedResultDto<GetAllLeadSoldDto>> GetAll(GetAllLeadSoldInputDto input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

                var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });
                var FilterTeamList = new List<long?>();
                if (input.TeamId != null && input.TeamId != 0)
                {
                    FilterTeamList = user_teamList.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToList();
                }

                int UserId = (int)AbpSession.UserId;
                //var User_List = _userRepository.GetAll().AsNoTracking();
                var User = await _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
                IList<string> RoleName = await _userManager.GetRolesAsync(User);
                var TeamId = await user_teamList.Where(e => e.UserId == UserId).Select(e => e.TeamId).ToListAsync();
                var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();

                input.states = input.states != null ? input.states : new List<string>();

                var users = _userManager.Users
                    .WhereIf(input.UserId != 0 && input.UserId != null, e => e.Id == input.UserId)
                    .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.Id))
                    .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.Id))
                    .WhereIf(RoleName.Contains("Sales Rep"), e => e.Id == User.Id)
                    .Where(u => u.Roles.Any(r => r.RoleId == 3));

                var userIDs = users.Select(u => u.Id).ToList();

                var LeadSources = _leadSourceRepository.GetAll();

                //IQueryable leads = Enumerable.Empty<T>().AsQueryable();
                var Actualleads = _leadRepository.GetAll()
                   .Where(e => e.OrganizationId == input.OrganizationUnitId && userIDs.Contains((long)e.FirstAssignUserId))
                   .WhereIf(input.StartDate != null, e => e.FirstAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                   .WhereIf(input.EndDate != null, e => e.FirstAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                   .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                   .WhereIf(input.states.Count() > 0,e => input.states.Contains(e.State))
                   .WhereIf(RoleName.Contains("Admin"), e => e.FirstAssignUserId != null)
                   .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.FirstAssignUserId))
                   .WhereIf(RoleName.Contains("Sales Rep"), e => e.FirstAssignUserId == User.Id)
                   .GroupBy(e => new { e.FirstAssignUserId, e.LeadSource }).Select(e => new { useId = (int)e.Key.FirstAssignUserId, leadSource = e.Key.LeadSource, count = e.Count() });
                
                //if (input.ReportType == 2)
                //{
                   var leads = _leadRepository.GetAll()
                   .Where(e => userIDs.Contains((long)e.AssignToUserID))
                   .Where(e=> e.OrganizationId == input.OrganizationUnitId)
                   .WhereIf(input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                   .WhereIf(input.EndDate != null,e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                   .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                   .WhereIf(input.states.Count() > 0,e => input.states.Contains(e.State))
                   .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                   .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                   .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                   .GroupBy(e => new { e.AssignToUserID, e.LeadSource }).Select(e => new { useId = (int)e.Key.AssignToUserID, leadSource = e.Key.LeadSource, count = e.Count() });
                //}

                //var jobs = _jobRepository.GetAll().AsNoTracking().Include(e => e.LeadFk).Where(e => e.LeadFk.FirstAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date && e.LeadFk.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date && e.LeadFk.OrganizationId == input.OrganizationUnitId && e.FirstDepositDate != null && e.LeadFk.AssignToUserID != null)
                //  .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId));

                var Actualjobs = _jobRepository.GetAll().AsNoTracking()
                                         .Include(e => e.LeadFk)
                                         .Where(e =>  e.LeadFk.OrganizationId == input.OrganizationUnitId && e.FirstDepositDate != null && e.LeadFk.FirstAssignUserId != null)
                                         .WhereIf(input.StartDate != null, e => e.FirstDepositDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                         .WhereIf(input.EndDate != null, e => e.FirstDepositDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                                         .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                                         .WhereIf(input.states.Count() > 0,e => input.states.Contains(e.State))
                                         .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.FirstAssignUserId != null)
                                         .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.FirstAssignUserId))
                                         .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.FirstAssignUserId == User.Id)
                                           .Select(e => new { AssignToUserID = (int)e.LeadFk.FirstAssignUserId });

                //if (input.ReportType == 2)
                //{
                 var jobs = _jobRepository.GetAll().AsNoTracking().Include(e => e.LeadFk)
                        .Where(e =>  e.LeadFk.OrganizationId == input.OrganizationUnitId && e.FirstDepositDate != null && e.LeadFk.AssignToUserID != null)
                         .WhereIf(input.StartDate != null, e => e.LeadFk.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                         .WhereIf(input.EndDate != null, e => e.LeadFk.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                        .WhereIf(input.states.Count() > 0,e => input.states.Contains(e.State))
                        .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                        .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                        .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                        .Select(e => new { AssignToUserID = (int)e.LeadFk.AssignToUserID });
               // }

                var FilteredLead = leads.GroupBy(e => e.useId).Select(e => new { UID = e.Key, LeadCount = e.Sum(s => s.count) });
                var ActualFilteredLead = Actualleads.GroupBy(e => e.useId).Select(e => new { UID = e.Key, LeadCount = e.Sum(s => s.count) });

                var result = from u in FilteredLead
                             select new GetAllLeadSoldDto
                             {
                                 Id = (int)u.UID,
                                 UserName = users.Where(e => e.Id == u.UID).FirstOrDefault().FullName,
                                 LeadAssign = u.LeadCount,
                                 ActualLeadAssign = ActualFilteredLead.Where(e => e.UID == u.UID).Select(e => e.LeadCount).FirstOrDefault(),
                                 //LeadSorce = u.leadSource,
                                 SoldLead = jobs.Where(e => e.AssignToUserID == u.UID).Count(),
                                 ActualSoldLead = Actualjobs.Where(e => e.AssignToUserID == u.UID).Count(),
                                 Ratio = jobs.Where(e => e.AssignToUserID == u.UID).Count() != 0 && u.LeadCount != 0 ? (((jobs.Where(e => e.AssignToUserID == u.UID).Count()) * 100) / u.LeadCount) : 0,
                                 ActualRatio = Actualjobs.Where(e => e.AssignToUserID == u.UID).Count() != 0 && u.LeadCount != 0 ? (((Actualjobs.Where(e => e.AssignToUserID == u.UID).Count()) * 100) / u.LeadCount) : 0,
                                 Missing = jobs.Where(e => e.AssignToUserID == u.UID).Count() != 0 && u.LeadCount != 0 ?
                                            (leads.Where(e => e.useId == u.UID).Count() == 1 ?
                                                (LeadSources.Where(e => e.Name == (leads.Where(e => e.useId == u.UID).FirstOrDefault().leadSource)).FirstOrDefault().MissingPer != 0 ?
                                                    jobs.Where(e => e.AssignToUserID == u.UID).Count() - (u.LeadCount / (double)(LeadSources.Where(e => e.Name == (leads.Where(e => e.useId == u.UID).FirstOrDefault().leadSource)).FirstOrDefault().MissingPer))
                                                    : 0.0)
                                                : jobs.Where(e => e.AssignToUserID == u.UID).Count() - (u.LeadCount))
                                            : 0.0,
                                 ActualMissing = Actualjobs.Where(e => e.AssignToUserID == u.UID).Count() != 0 && u.LeadCount != 0 ?
                                            (Actualleads.Where(e => e.useId == u.UID).Count() == 1 ?
                                                (LeadSources.Where(e => e.Name == (Actualleads.Where(e => e.useId == u.UID).FirstOrDefault().leadSource)).FirstOrDefault().MissingPer != 0 ?
                                                    Actualjobs.Where(e => e.AssignToUserID == u.UID).Count() - (u.LeadCount / (double)(LeadSources.Where(e => e.Name == (Actualleads.Where(e => e.useId == u.UID).FirstOrDefault().leadSource)).FirstOrDefault().MissingPer))
                                                    : 0.0)
                                                : Actualjobs.Where(e => e.AssignToUserID == u.UID).Count() - (u.LeadCount))
                                            : 0.0
                             };

                var pagedAndFiltered = result
                   .OrderBy(input.Sorting ?? "Id desc")
                   .PageBy(input).ToList();

                var reSum = result.ToList();
                var summaryCount = new LeadSoldSummaryCount();
                summaryCount.LeadAssignCount = reSum.Sum(e => e.LeadAssign);
                summaryCount.SoldLeadCount = reSum.Sum(e => e.SoldLead);
                summaryCount.RatioCount = reSum.Sum(e => e.LeadAssign) > 0? (reSum.Sum(e => e.SoldLead) * 100) / reSum.Sum(e => e.LeadAssign):0;
                summaryCount.MissingCount = reSum.Sum(e => e.Missing);
                summaryCount.ActualLeadAssignCount = reSum.Sum(e => e.ActualLeadAssign);
                summaryCount.ActualSoldLeadCount = reSum.Sum(e => e.ActualSoldLead);
                summaryCount.ActualRatioCount = reSum.Sum(e => e.ActualLeadAssign) > 0 ? (reSum.Sum(e => e.ActualSoldLead) * 100) / reSum.Sum(e => e.ActualLeadAssign) : 0;
                summaryCount.ActualMissingCount = reSum.Sum(e => e.ActualMissing);

                var totalcount = FilteredLead.Count();

                if (pagedAndFiltered.Count() > 0)
                {
                    pagedAndFiltered[0].SummaryCount = summaryCount;
                }

                return new PagedResultDto<GetAllLeadSoldDto>(totalcount, pagedAndFiltered.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Opps there is problem!", ex.Message);
            }



        }

        public async Task<FileDto> GetLeadSoldToExcel(GetAllExportLeadSoldInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            var users = _userManager.Users
                .WhereIf(input.UserId != 0 && input.UserId != null, e => e.Id == input.UserId)
                .Where(u => u.Roles.Any(r => r.RoleId == 3));
            var User = await _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefaultAsync();
            IList<string> RoleName = await _userManager.GetRolesAsync(User);
            var userIDs = users.Select(u => u.Id).ToList();
            var user_teamList = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });
            var TeamId = await user_teamList.Where(e => e.UserId == AbpSession.UserId).Select(e => e.TeamId).ToListAsync();
            var UserList = await user_teamList.Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToListAsync();
            var LeadSources = _leadSourceRepository.GetAll();


            var leads = _leadRepository.GetAll()
                  .Where(e => e.OrganizationId == input.OrganizationUnitId && userIDs.Contains((long)e.FirstAssignUserId))
                  .WhereIf(input.StartDate != null, e => e.FirstAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                  .WhereIf(input.EndDate != null, e => e.FirstAssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                  .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                  .WhereIf(input.states.Count() > 0, e => input.states.Contains(e.StateId))
                  .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
                  .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
                  .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
                  .GroupBy(e => new { e.FirstAssignUserId, e.LeadSource }).Select(e => new { useId = (int)e.Key.FirstAssignUserId, leadSource = e.Key.LeadSource, count = e.Count() });

            if (input.ReportType == 2)
            {
                leads = _leadRepository.GetAll()
               .Where(e => userIDs.Contains((long)e.AssignToUserID))
               .Where(e => e.OrganizationId == input.OrganizationUnitId)
               .WhereIf(input.StartDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
               .WhereIf(input.EndDate != null, e => e.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
               .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
               .WhereIf(input.states.Count() > 0, e => input.states.Contains(e.StateId))
               .WhereIf(RoleName.Contains("Admin"), e => e.AssignToUserID != null)
               .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.AssignToUserID))
               .WhereIf(RoleName.Contains("Sales Rep"), e => e.AssignToUserID == User.Id)
               .GroupBy(e => new { e.AssignToUserID, e.LeadSource }).Select(e => new { useId = (int)e.Key.AssignToUserID, leadSource = e.Key.LeadSource, count = e.Count() });
            }

            //var jobs = _jobRepository.GetAll().AsNoTracking().Include(e => e.LeadFk).Where(e => e.LeadFk.FirstAssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date && e.LeadFk.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date && e.LeadFk.OrganizationId == input.OrganizationUnitId && e.FirstDepositDate != null && e.LeadFk.AssignToUserID != null)
            //  .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId));

            var jobs = _jobRepository.GetAll().AsNoTracking()
                                     .Include(e => e.LeadFk)
                                     .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnitId && e.FirstDepositDate != null && e.LeadFk.AssignToUserID != null)
                                     .WhereIf(input.StartDate != null, e => e.FirstDepositDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                     .WhereIf(input.EndDate != null, e => e.FirstDepositDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                                     .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                                     .WhereIf(input.states.Count() > 0, e => input.states.Contains(e.StateId))
                                     .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                                     .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                                     .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                                       .Select(e => new { AssignToUserID = (int)e.LeadFk.AssignToUserID });

            if (input.ReportType == 2)
            {
                jobs = _jobRepository.GetAll().AsNoTracking().Include(e => e.LeadFk)
                    .Where(e => e.LeadFk.OrganizationId == input.OrganizationUnitId && e.FirstDepositDate != null && e.LeadFk.AssignToUserID != null)
                     .WhereIf(input.StartDate != null, e => e.LeadFk.AssignDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                     .WhereIf(input.EndDate != null, e => e.LeadFk.AssignDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                    .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadFk.LeadSourceId))
                    .WhereIf(input.states.Count() > 0, e => input.states.Contains(e.StateId))
                    .WhereIf(RoleName.Contains("Admin"), e => e.LeadFk.AssignToUserID != null)
                    .WhereIf(RoleName.Contains("Sales Manager"), e => UserList.Contains(e.LeadFk.AssignToUserID))
                    .WhereIf(RoleName.Contains("Sales Rep"), e => e.LeadFk.AssignToUserID == User.Id)
                    .Select(e => new { AssignToUserID = (int)e.LeadFk.AssignToUserID });
            }

            var FilteredLead = leads.GroupBy(e => e.useId).Select(e => new { UID = e.Key, LeadCount = e.Sum(s => s.count) });
            var result = from u in FilteredLead
                         select new GetAllLeadSoldDto
                         {
                             Id = (int)u.UID,
                             UserName = users.Where(e => e.Id == u.UID).FirstOrDefault().FullName,
                             LeadAssign = u.LeadCount,
                             //LeadSorce = u.leadSource,
                             SoldLead = jobs.Where(e => e.AssignToUserID == u.UID).Count(),
                             Ratio = jobs.Where(e => e.AssignToUserID == u.UID).Count() != 0 && u.LeadCount != 0 ? (((jobs.Where(e => e.AssignToUserID == u.UID).Count()) * 100) / u.LeadCount) : 0,
                             Missing = jobs.Where(e => e.AssignToUserID == u.UID).Count() != 0 && u.LeadCount != 0 ?
                                            (leads.Where(e => e.useId == u.UID).Count() == 1 ?
                                                (LeadSources.Where(e => e.Name == (leads.Where(e => e.useId == u.UID).FirstOrDefault().leadSource)).FirstOrDefault().MissingPer != 0 ?
                                                    jobs.Where(e => e.AssignToUserID == u.UID).Count() - (u.LeadCount / (double)(LeadSources.Where(e => e.Name == (leads.Where(e => e.useId == u.UID).FirstOrDefault().leadSource)).FirstOrDefault().MissingPer))
                                                    : 0.0)
                                                : jobs.Where(e => e.AssignToUserID == u.UID).Count() - (u.LeadCount))
                                            : 0.0
                         };


            if (input.excelorcsv == 1)
            {
                return _leadsExcelExporter.LeadSoldExport(result.ToList(), "LeadSold.xlsx");
            }
            else
            {
                return _leadsExcelExporter.LeadSoldExport(result.ToList(), "LeadSold.csv");
            }
        }


    }
}
