﻿using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.JobCost.Dtos;
using TheSolarProduct.Reports.ProductSolds.Dtos;
using TheSolarProduct.Reports.SmsCountReports.Dtos;

namespace TheSolarProduct.Reports.ProductSolds.Exporting
{
    public interface IProductSoldExcelExport
    {
        FileDto ExportToFile(List<GetAllProductSoldDto> productSolds, string fileName);

        FileDto SmsCountExportToFile(List<GetAllSmsCountDto> smsCount, string fileName);


    }
}
