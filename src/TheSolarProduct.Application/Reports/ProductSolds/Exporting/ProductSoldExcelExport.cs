﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using NPOI.HPSF;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Jobs;
using TheSolarProduct.Reports.ProductSolds.Dtos;
using TheSolarProduct.Reports.SmsCountReports.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Reports.ProductSolds.Exporting
{
    public class ProductSoldExcelExport : NpoiExcelExporterBase, IProductSoldExcelExport
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;
        public ProductSoldExcelExport(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetAllProductSoldDto> productSolds, string fileName)
        {
            var Header = new string[] { "Item", "NSW", "SA", "VIC", "QLD" };

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ProductSold"));

                    AddHeader(sheet, Header);
                    AddObjects(
                        sheet, 2, productSolds,
                        _ => _.ProductItem,
                        _ => _.NSW,
                        _ => _.SA,
                        _ => _.VIC,
                        _ => _.QLD
                    );

                    for (var i = 1; i <= productSolds.Count; i++)
                    {
                        //SetintCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[2], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[3], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[4], "00");
                    }
                });
        }

        public FileDto SmsCountExportToFile(List<GetAllSmsCountDto> smsCount, string fileName)
        {
            var Header = new string[] { "User", "SMS", "Credit", "Amount"};

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("SMS_Count");

                    AddHeader(sheet, Header);
                    AddObjects(
                        sheet, 2, smsCount,
                        _ => _.userName,
                        _ => _.SmsCount,
                        _ => _.CreditCount,
                        _ => _.Amount
                    );

                    for (var i = 1; i <= smsCount.Count; i++)
                    {
                        //SetintCellDataFormat(sheet.GetRow(i).Cells[7], "0.00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[1], "00");
                        SetintCellDataFormat(sheet.GetRow(i).Cells[2], "00");
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[3], "0.00");
                    }
                });
        }
    }
}
