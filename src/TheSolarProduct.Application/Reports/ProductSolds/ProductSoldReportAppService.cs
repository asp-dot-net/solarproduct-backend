﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.TheSolarDemo;
//using System.Globalization;
using Abp.Timing.Timezone;
using TheSolarProduct.Timing;
using TheSolarProduct.Reports.ProductSolds.Dtos;
using Abp.Collections.Extensions;
using TheSolarProduct.Reports.ProductSolds.Exporting;
using Hangfire.Common;

namespace TheSolarProduct.Reports.ProductSolds
{
    public class ProductSoldReportAppService : TheSolarProductAppServiceBase, IProductSoldReportAppService
    {
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly UserManager _userManager;
        private readonly IRepository<UserTeam> _userTeamRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IProductSoldExcelExport _productSoldExcelExport;
        public ProductSoldReportAppService(
            IRepository<JobProductItem> jobProductItemRepository,
        ITimeZoneConverter timeZoneConverter,
        ITimeZoneService timeZoneService,
        UserManager userManager,
        IRepository<UserTeam> userTeamRepository,
        IRepository<User, long> userRepository,
        IProductSoldExcelExport productSoldExcelExport
            )
        {
            _jobProductItemRepository = jobProductItemRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _userManager = userManager;
            _userTeamRepository = userTeamRepository;
            _userRepository = userRepository;
            _productSoldExcelExport = productSoldExcelExport;
        }

        public async Task<PagedResultDto<GetAllProductSoldDto>> GetAll(GetAllProductSoldInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });


            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = await team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
            }

            List<string> states = new List<string>() { "NSW", "SA", "QLD", "VIC" };

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            var Filtered = await _jobProductItemRepository.GetAll()
                                .Include(e => e.JobFk)
                                .Include(e => e.ProductItemFk)
                                .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnitId)
                                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                                .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                                .WhereIf(input.DateType == "FirstDeposite" && input.StartDate != null, e => e.JobFk.FirstDepositDate.Value.Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "FirstDeposite" && input.EndDate != null, e => e.JobFk.FirstDepositDate.Value.Date <= EDate.Value.Date)

                                .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                                .WhereIf(input.DateType == "Active" && input.StartDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "Active" && input.EndDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                                .WhereIf(input.DateType == "InstallBook" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "InstallBook" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                                
                                .WhereIf(input.UserId != null && input.UserId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.UserId)
                                .WhereIf(input.JobStatus != null && input.JobStatus.Count() > 0, e => input.JobStatus.Contains(e.JobFk.JobStatusId))
                                .WhereIf(input.ProductItemId != null && input.ProductItemId != 0, e => e.ProductItemId == input.ProductItemId)
                                .WhereIf(input.ProductTypeId != null && input.ProductTypeId != 0, e => e.ProductItemFk.ProductTypeId == input.ProductTypeId)
                                .WhereIf(!string.IsNullOrEmpty(input.ProductItem), e => e.ProductItemFk.Name.Contains(input.ProductItem))
                                .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.JobFk.LeadFk.AssignToUserID))

                                .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.JobFk.LeadFk.Area == input.AreaName)

                                .Where(e => states.ToList().Contains(e.JobFk.State))

                                .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                                .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5))

                                .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                                .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                                .WhereIf(input.SystemFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                                .WhereIf(input.SystemFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                                .WhereIf(input.SystemFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery

                                .AsNoTracking()
                                .GroupBy(e => new { e.ProductItemId, e.ProductItemFk.Name, e.JobFk.State })
                                .Select(e => new
                                {
                                    e.Key.ProductItemId,
                                    e.Key.Name,
                                    e.Key.State,
                                    qty = e.Sum(s => s.Quantity)
                                })
                                .ToListAsync();



            var result = from u in Filtered.GroupBy(e => new { e.ProductItemId, e.Name })
                         select new GetAllProductSoldDto
                         {
                             Id = (int)u.Key.ProductItemId,
                             ProductItem = u.Key.Name,
                             Total = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId).Sum(s => s.qty),

                             NSW = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId && e.State == "NSW").Sum(s => s.qty),

                             SA = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId && e.State == "SA").Sum(s => s.qty),

                             QLD = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId && e.State == "QLD").Sum(s => s.qty),

                             VIC = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId && e.State == "VIC").Sum(s => s.qty),
                         };


            var pagedAndFiltered = result.AsQueryable()
                 .OrderBy(input.Sorting ?? "Id desc")
                 .PageBy(input).ToList();

            var totalcount = result.Count();

            //.WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
            //                    .WhereIf(input.BatteryFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5))

            //                    .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 1))
            //                    .WhereIf(input.BatteryFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

            //                    .WhereIf(input.BatteryFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

            //                    .WhereIf(input.BatteryFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

            SummaryProductSold summaryProduct = new SummaryProductSold();
            summaryProduct.SumNSW = result.Sum(e => e.NSW);
            summaryProduct.SumSA = result.Sum(e => e.SA);
            summaryProduct.SumVIC = result.Sum(e => e.VIC);
            summaryProduct.SumQLD = result.Sum(e => e.QLD);
            summaryProduct.TotalSum = result.Sum(e => e.Total);

            if (result.Count() > 0)
            {
                pagedAndFiltered[0].summary = summaryProduct;
            }

            return new PagedResultDto<GetAllProductSoldDto>(totalcount, pagedAndFiltered.ToList());

        }

        public async Task<FileDto> GetProductSoldToExcel(GetAllExcelProductSoldInputDto input)
        {

            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });

            IList<string> role = await _userManager.GetRolesAsync(User);
            var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();
            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = await team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
            }

            List<string> states = new List<string>() { "NSW", "SA", "QLD", "VIC" };

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            var Filtered = await _jobProductItemRepository.GetAll()
                                .Include(e => e.JobFk)
                                .Include(e => e.ProductItemFk)
                                .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnitId)
                                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                                .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                                .WhereIf(input.DateType == "FirstDeposite" && input.StartDate != null, e => e.JobFk.FirstDepositDate.Value.Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "FirstDeposite" && input.EndDate != null, e => e.JobFk.FirstDepositDate.Value.Date <= EDate.Value.Date)

                                .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                                .WhereIf(input.DateType == "Active" && input.StartDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "Active" && input.EndDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                                .WhereIf(input.DateType == "InstallBook" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "InstallBook" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)

                                .WhereIf(input.UserId != null && input.UserId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.UserId)
                                .WhereIf(input.JobStatus != null && input.JobStatus.Count() > 0, e => input.JobStatus.Contains(e.JobFk.JobStatusId))
                                .WhereIf(input.ProductItemId != null && input.ProductItemId != 0, e => e.ProductItemId == input.ProductItemId)
                                .WhereIf(input.ProductTypeId != null && input.ProductTypeId != 0, e => e.ProductItemFk.ProductTypeId == input.ProductTypeId)
                                .WhereIf(!string.IsNullOrEmpty(input.ProductItem), e => e.ProductItemFk.Name.Contains(input.ProductItem))
                                .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.JobFk.LeadFk.AssignToUserID))

                                .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.JobFk.LeadFk.Area == input.AreaName)

                                .Where(e => states.ToList().Contains(e.JobFk.State))

                                .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                                .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5))

                                .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                                .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                                .WhereIf(input.SystemFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                                .WhereIf(input.SystemFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                                .WhereIf(input.SystemFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery

                                .AsNoTracking()
                                .GroupBy(e => new { e.ProductItemId, e.ProductItemFk.Name, e.JobFk.State })
                                .Select(e => new
                                {
                                    e.Key.ProductItemId,
                                    e.Key.Name,
                                    e.Key.State,
                                    qty = e.Sum(s => s.Quantity)

                                    //, NSW = e.Where(s => s.JobFk.State == "NSW").Sum(s => s.Quantity)
                                    //, SA = e.Where(s => s.JobFk.State == "SA").Sum(s => s.Quantity)
                                    //, QLD = e.Where(s => s.JobFk.State == "QLD").Sum(s => s.Quantity)
                                    //, VIC = e.Where(s => s.JobFk.State == "VIC").Sum(s => s.Quantity)
                                })
                                .ToListAsync();



            var result = from u in Filtered.GroupBy(e => new { e.ProductItemId, e.Name })
                         select new GetAllProductSoldDto
                         {
                             Id = (int)u.Key.ProductItemId,
                             ProductItem = u.Key.Name,
                             NSW = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId && e.State == "NSW").Sum(s => s.qty),
                             SA = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId && e.State == "SA").Sum(s => s.qty),
                             QLD = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId && e.State == "QLD").Sum(s => s.qty),
                             VIC = Filtered.Where(e => e.ProductItemId == u.Key.ProductItemId && e.State == "VIC").Sum(s => s.qty),
                         };

            return _productSoldExcelExport.ExportToFile(result.ToList(), input.ExcelOrCSV);
        }

        public async Task<ProductSoldPriceDto> GetProductSoldPriceDto(GetAllExcelProductSoldInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);
            var team_list = _userTeamRepository.GetAll().AsNoTracking().Select(e => new { e.TeamId, e.UserId });


            var TeamId = await team_list.Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToListAsync();
            var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

            var FilterTeamList = new List<long?>();
            if (input.TeamId != null && input.TeamId != 0)
            {
                FilterTeamList = await team_list.Where(e => e.TeamId == input.TeamId).Select(e => e.UserId).Distinct().ToListAsync();
            }

            List<string> states = new List<string>() { "NSW", "SA", "QLD", "VIC" };

            List<int> batteryInverter = new List<int>() { 1, 3, 4, 6 };

            var Filtered = await _jobProductItemRepository.GetAll()
                                .Include(e => e.JobFk)
                                .Include(e => e.ProductItemFk)
                                .Where(e => e.JobFk.LeadFk.OrganizationId == input.OrganizationUnitId)
                                .WhereIf(role.Contains("Admin"), e => e.JobFk.LeadFk.AssignToUserID != null)
                                .WhereIf(role.Contains("Sales Manager"), e => UserList.Contains(e.JobFk.LeadFk.AssignToUserID))
                                .WhereIf(role.Contains("Sales Rep"), e => e.JobFk.LeadFk.AssignToUserID == User.Id)
                                .WhereIf(input.DateType == "CreationDate" && input.StartDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "CreationDate" && input.EndDate != null, e => e.JobFk.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                                .WhereIf(input.DateType == "FirstDeposite" && input.StartDate != null, e => e.JobFk.FirstDepositDate.Value.Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "FirstDeposite" && input.EndDate != null, e => e.JobFk.FirstDepositDate.Value.Date <= EDate.Value.Date)

                                .WhereIf(input.DateType == "DepositeReceived" && input.StartDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "DepositeReceived" && input.EndDate != null, e => e.JobFk.DepositeRecceivedDate.Value.Date <= EDate.Value.Date)

                                .WhereIf(input.DateType == "Active" && input.StartDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "Active" && input.EndDate != null, e => e.JobFk.ActiveDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                                .WhereIf(input.DateType == "InstallBook" && input.StartDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                                .WhereIf(input.DateType == "InstallBook" && input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                                .WhereIf(input.UserId != null && input.UserId != 0, e => e.JobFk.LeadFk.AssignToUserID == input.UserId)
                                .WhereIf(input.JobStatus != null && input.JobStatus.Count() > 0, e => input.JobStatus.Contains(e.JobFk.JobStatusId))
                                .WhereIf(input.ProductItemId != null && input.ProductItemId != 0, e => e.ProductItemId == input.ProductItemId)
                                .WhereIf(input.ProductTypeId != null && input.ProductTypeId != 0, e => e.ProductItemFk.ProductTypeId == input.ProductTypeId)
                                .WhereIf(!string.IsNullOrEmpty(input.ProductItem), e => e.ProductItemFk.Name.Contains(input.ProductItem))
                                .WhereIf(input.TeamId != 0 && input.TeamId != null, e => FilterTeamList.Contains(e.JobFk.LeadFk.AssignToUserID))
                                .Where(e => states.ToList().Contains(e.JobFk.State))

                                .WhereIf(!string.IsNullOrWhiteSpace(input.AreaName), e => e.JobFk.LeadFk.Area == input.AreaName)

                                .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                                .WhereIf(input.SystemFilter == 1, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5))

                                .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 1))
                                .WhereIf(input.SystemFilter == 2, e => _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId == 5).Count() == 0)

                                .WhereIf(input.SystemFilter == 3, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 5).Count() == 0) //For Only Battery

                                .WhereIf(input.SystemFilter == 4, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.JobId) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.JobId && j.ProductItemFk.ProductTypeId != 2).Count() == 0) //For Only Inverter

                                .WhereIf(input.SystemFilter == 5, e => _jobProductItemRepository.GetAll().Any(j => j.JobId == e.Id) && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 2).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && j.ProductItemFk.ProductTypeId == 5).Count() > 0 && _jobProductItemRepository.GetAll().Where(j => j.JobId == e.Id && batteryInverter.Contains(j.ProductItemFk.ProductTypeId)).Count() == 0) //For Only Inverter and Battery

                                .AsNoTracking()
                                .Select(e => new
                                {
                                    e.JobFk.State,
                                    TotalPrice = e.JobFk.SystemCapacity != null && e.JobFk.SystemCapacity != 0 ? (e.JobFk.TotalCost / (e.JobFk.SystemCapacity * 1000)) : 0,
                                    MetroPrice = e.JobFk.LeadFk.Area == "Metro" && e.JobFk.SystemCapacity != null && e.JobFk.SystemCapacity != 0 ? (e.JobFk.TotalCost / (e.JobFk.SystemCapacity * 1000)) : 0,
                                    RegionalPrice = e.JobFk.LeadFk.Area == "Regional" && e.JobFk.SystemCapacity != null && e.JobFk.SystemCapacity != 0 ? (e.JobFk.TotalCost / (e.JobFk.SystemCapacity * 1000)) : 0
                                })
                                .ToListAsync();

            var result = new ProductSoldPriceDto()
            {
                TotalPrice = Filtered.Sum(e => e.TotalPrice),
                NSWMetroPrice = Filtered.Where(e => e.State == "NSW").Average(s => s.MetroPrice),
                NSWRegionalPrice = Filtered.Where(e => e.State == "NSW").Average(s => s.RegionalPrice),
                SAMetroPrice = Filtered.Where(e => e.State == "SA").Average(s => s.MetroPrice),
                SARegionalPrice = Filtered.Where(e => e.State == "SA").Average(s => s.RegionalPrice),
                QLDMetroPrice = Filtered.Where(e => e.State == "QLD").Average(s => s.MetroPrice),
                QLDRegionalPrice = Filtered.Where(e => e.State == "QLD").Average(s => s.RegionalPrice),
                VICMetroPrice = Filtered.Where(e => e.State == "VIC").Average(s => s.MetroPrice),
                VICRegionalPrice = Filtered.Where(e => e.State == "VIC").Average(s => s.RegionalPrice),
            };

            return result;
        }
    }
}
