﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.TheSolarDemo;
//using System.Globalization;
using Abp.Timing.Timezone;
using TheSolarProduct.Timing;
using TheSolarProduct.Reports.ProductSolds.Dtos;
using Abp.Collections.Extensions;
using TheSolarProduct.Reports.ProductSolds.Exporting;
using TheSolarProduct.Reports.Variations.Dtos;
using TheSolarProduct.JobHistory;
using Abp.Extensions;

namespace TheSolarProduct.Reports.Variations
{
    public class JobVariationReportAppService : TheSolarProductAppServiceBase, IJobVariationReportAppService
    {
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IJobVariationReportExcelExport _jobVariationReportExcelExport;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<JobTrackerHistory> _jobTrackerHistoryRepository;
        private readonly IRepository<User, long> _userRepository;

        public JobVariationReportAppService(
            IRepository<JobVariation> jobVariationRepository,
            IJobVariationReportExcelExport jobVariationReportExcelExport,
            ITimeZoneConverter timeZoneConverter,
            ITimeZoneService timeZoneService,
            IRepository<JobTrackerHistory> jobTrackerHistoryRepository,
            IRepository<User, long> userRepository
            ) { 
            _jobVariationRepository = jobVariationRepository;   
            _jobVariationReportExcelExport = jobVariationReportExcelExport;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _jobTrackerHistoryRepository = jobTrackerHistoryRepository;
            _userRepository = userRepository;
        }

        public async Task<PagedResultDto<GetAllVariationDto>> GetAll(GetVariationInputDto input)
        {
            input.VariationIds = input.VariationIds != null? input.VariationIds : new List<int?>();
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            input.State = input.State != null ? input.State : new List<string>();

            var Variation = _jobVariationRepository.GetAll()
                            .Include(e => e.JobFk)
                            .Where(e => e.JobFk.LeadFk.OrganizationId == input.OId)
                            .WhereIf(!string.IsNullOrEmpty(input.JobNumber), e => e.JobFk.JobNumber == input.JobNumber)
                            .WhereIf(input.VariationIds.Count > 0, e => input.VariationIds.Contains(e.VariationId))
                            .WhereIf(input.StartDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                            .WhereIf(input.State.Count() > 0,e => input.State.Contains(e.JobFk.State))
                            .AsNoTracking()
                            .Select(e => new
                            {
                                e.Id,
                                e.JobFk.JobNumber,
                                Variation = e.VariationFk.Name,
                                e.Cost,
                                e.Notes,
                                e.JobId,
                                e.CreatorUserId,
                                e.CreationTime,
                                e.JobFk.State,
                                e.LastModifierUserId
                            });

            var pagedAndFiltered = Variation.AsQueryable()
                .OrderBy(input.Sorting ?? "Id desc")
                .PageBy(input).ToList();

            var result = from j in pagedAndFiltered
                         select new GetAllVariationDto()
                         {
                             Id = j.Id,
                             JobNumber = j.JobNumber,
                             Variation = j.Variation,
                             Amount = j.Cost,
                             Note = j.Notes,
                             Created = j.CreationTime.ToString("dd/MMM/yyyy"),
                             CreatedBy = j.CreatorUserId != null ? _userRepository.Get((long)j.CreatorUserId).FullName:(j.LastModifierUserId != null ? _userRepository.Get((long)j.LastModifierUserId).FullName : ""),
                             State = j.State,
                         };

            var totalcount = Variation.Count();

            var output = result.ToList();

            if(totalcount > 0)
            {
                output[0].summaryVariations = (from o in Variation.GroupBy(e => e.State).Select(e => new {e.Key, amt = e.Sum(s => s.Cost)})
                                              select new summaryVariationDto()
                                              {
                                                  State = o.Key,
                                                  Amount = o.amt
                                              }).ToList();
            }

            return new PagedResultDto<GetAllVariationDto>(totalcount, output);
        }

        public async Task<FileDto> GetAllForExport(GetVariationExportInputDto input)
        {
            input.VariationIds = input.VariationIds != null ? input.VariationIds : new List<int?>();
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);
            input.State = input.State != null ? input.State : new List<string>();

            var Variation = _jobVariationRepository.GetAll()
                            .Include(e => e.JobFk)
                            .Where(e => e.JobFk.LeadFk.OrganizationId == input.OId)
                            .WhereIf(!string.IsNullOrEmpty(input.JobNumber), e => e.JobFk.JobNumber == input.JobNumber)
                            .WhereIf(input.VariationIds.Count > 0, e => input.VariationIds.Contains(e.VariationId))
                            .WhereIf(input.StartDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                            .WhereIf(input.State.Count() > 0,e => input.State.Contains(e.JobFk.State))
                            .AsNoTracking()
                            .Select(e => new
                            {
                                e.Id,
                                e.JobFk.JobNumber,
                                Variation = e.VariationFk.Name,
                                e.CreatorUserId,
                                Created = e.CreationTime.ToString("dd/MMM/yyyy"),
                                e.Cost,
                                e.Notes,
                                e.JobFk.InstallationDate,
                                e.JobFk.State,
                                JobStatus = e.JobFk.JobStatusFk.Name,
                                e.LastModifierUserId
                            }).ToList();

            var result = from j in Variation
                         select new GetAllVariationDto()
                         {
                             Id = j.Id,
                             JobNumber = j.JobNumber,
                             Variation = j.Variation,
                             CreatedBy = j.CreatorUserId != null ? _userRepository.Get((long)j.CreatorUserId).FullName:(j.LastModifierUserId != null ? _userRepository.Get((long)j.LastModifierUserId).FullName : ""),
                             Created = j.Created,
                             Amount = j.Cost,
                             Note = j.Notes,
                             InstallationDate = j.InstallationDate,
                             State = j.State,
                             Staus = j.JobStatus
                         };

            return _jobVariationReportExcelExport.ExportToFile(result.ToList());
        }

        public async Task<List<GetAllVariationDto>> GetVariationTypeAmountByState(GetVariationInputDto input)
        {
            input.VariationIds = input.VariationIds != null ? input.VariationIds : new List<int?>();
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            input.State = input.State != null ? input.State : new List<string>();

            var Variation = _jobVariationRepository.GetAll()
                            .Include(e => e.JobFk)
                            .Where(e => e.JobFk.LeadFk.OrganizationId == input.OId)
                            .WhereIf(!string.IsNullOrEmpty(input.JobNumber), e => e.JobFk.JobNumber == input.JobNumber)
                            .WhereIf(input.VariationIds.Count > 0, e => input.VariationIds.Contains(e.VariationId))
                            .WhereIf(input.StartDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date >= SDate.Value.Date)
                            .WhereIf(input.EndDate != null, e => e.JobFk.InstallationDate.Value.AddHours(diffHour).Date <= EDate.Value.Date)
                            .WhereIf(input.State.Count() > 0, e => input.State.Contains(e.JobFk.State))
                            .AsNoTracking().GroupBy(e => e.VariationFk.Name)
                            .Select(e => new
                            {
                                Cost = e.Sum(s => s.Cost),
                                Variation = e.Key
                            });

            var result = from j in Variation
                         select new GetAllVariationDto()
                         {
                             Variation = j.Variation,
                             Amount = j.Cost
                         };

            return await result.ToListAsync();
        }
    }
}
