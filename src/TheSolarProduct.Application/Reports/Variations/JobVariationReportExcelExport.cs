﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using System;
using System.Collections.Generic;
using System.Text;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.Dto;
using TheSolarProduct.Reports.ProductSolds.Dtos;
using TheSolarProduct.Reports.Variations.Dtos;
using TheSolarProduct.Storage;

namespace TheSolarProduct.Reports.Variations
{
    public class JobVariationReportExcelExport : NpoiExcelExporterBase , IJobVariationReportExcelExport
    {
        public JobVariationReportExcelExport(TempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {}

        public FileDto ExportToFile(List<GetAllVariationDto> Listdata)
        {
            var Header = new string[] { "Job Number", "State", "JobStaus", "Variation", "CreatedBy", "Created", "Amount", "Notes", "InstallationDate" };

            return CreateExcelPackage(
                "JobVariationReport.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("JobVariation");

                    AddHeader(sheet, Header);
                    AddObjects(
                        sheet, 2, Listdata,
                        _ => _.JobNumber,
                        _ => _.State,
                        _ => _.Staus,
                        _ => _.Variation,
                        _ => _.CreatedBy,
                        _ => _.Created,
                        _ => _.Amount,
                        _ => _.Note,
                        _ => _.InstallationDate
                    );

                    for (var i = 1; i <= Listdata.Count; i++)
                    {
                        SetdecimalCellDataFormat(sheet.GetRow(i).Cells[6], "0.00");
                        SetCellDataFormat(sheet.GetRow(i).Cells[8], "dd/mm/yyyy");
                    }
                    for (int i = 0; i < sheet.GetRow(0).Cells.Count; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }

                });
        }

    }
}
