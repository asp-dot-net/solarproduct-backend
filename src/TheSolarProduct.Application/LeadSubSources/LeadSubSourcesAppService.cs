﻿using TheSolarProduct.LeadSources;
					using System.Collections.Generic;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.LeadSubSources.Exporting;
using TheSolarProduct.LeadSubSources.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.LeadSubSources
{
	[AbpAuthorize(AppPermissions.Pages_LeadSubSources)]
    public class LeadSubSourcesAppService : TheSolarProductAppServiceBase, ILeadSubSourcesAppService
    {
		 private readonly IRepository<LeadSubSource> _leadSubSourceRepository;
		 private readonly ILeadSubSourcesExcelExporter _leadSubSourcesExcelExporter;
		 private readonly IRepository<LeadSource,int> _lookup_leadSourceRepository;
		 

		  public LeadSubSourcesAppService(IRepository<LeadSubSource> leadSubSourceRepository, ILeadSubSourcesExcelExporter leadSubSourcesExcelExporter , IRepository<LeadSource, int> lookup_leadSourceRepository) 
		  {
			_leadSubSourceRepository = leadSubSourceRepository;
			_leadSubSourcesExcelExporter = leadSubSourcesExcelExporter;
			_lookup_leadSourceRepository = lookup_leadSourceRepository;
		
		  }

		 public async Task<PagedResultDto<GetLeadSubSourceForViewDto>> GetAll(GetAllLeadSubSourcesInput input)
         {
			
			var filteredLeadSubSources = _leadSubSourceRepository.GetAll()
						.Include( e => e.LeadSourceFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceFk != null && e.LeadSourceFk.Name == input.LeadSourceNameFilter);

			var pagedAndFilteredLeadSubSources = filteredLeadSubSources
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var leadSubSources = from o in pagedAndFilteredLeadSubSources
                         join o1 in _lookup_leadSourceRepository.GetAll() on o.LeadSourceId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetLeadSubSourceForViewDto() {
							LeadSubSource = new LeadSubSourceDto
							{
                                Name = o.Name,
                                Id = o.Id
							},
                         	LeadSourceName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
						};

            var totalCount = await filteredLeadSubSources.CountAsync();

            return new PagedResultDto<GetLeadSubSourceForViewDto>(
                totalCount,
                await leadSubSources.ToListAsync()
            );
         }
		 
		 public async Task<GetLeadSubSourceForViewDto> GetLeadSubSourceForView(int id)
         {
            var leadSubSource = await _leadSubSourceRepository.GetAsync(id);

            var output = new GetLeadSubSourceForViewDto { LeadSubSource = ObjectMapper.Map<LeadSubSourceDto>(leadSubSource) };

		    if (output.LeadSubSource.LeadSourceId != null)
            {
                var _lookupLeadSource = await _lookup_leadSourceRepository.FirstOrDefaultAsync((int)output.LeadSubSource.LeadSourceId);
                output.LeadSourceName = _lookupLeadSource?.Name?.ToString();
            }
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_LeadSubSources_Edit)]
		 public async Task<GetLeadSubSourceForEditOutput> GetLeadSubSourceForEdit(EntityDto input)
         {
            var leadSubSource = await _leadSubSourceRepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetLeadSubSourceForEditOutput {LeadSubSource = ObjectMapper.Map<CreateOrEditLeadSubSourceDto>(leadSubSource)};

		    if (output.LeadSubSource.LeadSourceId != null)
            {
                var _lookupLeadSource = await _lookup_leadSourceRepository.FirstOrDefaultAsync((int)output.LeadSubSource.LeadSourceId);
                output.LeadSourceName = _lookupLeadSource?.Name?.ToString();
            }
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditLeadSubSourceDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_LeadSubSources_Create)]
		 protected virtual async Task Create(CreateOrEditLeadSubSourceDto input)
         {
            var leadSubSource = ObjectMapper.Map<LeadSubSource>(input);

			
			if (AbpSession.TenantId != null)
			{
				leadSubSource.TenantId = (int) AbpSession.TenantId;
			}
		

            await _leadSubSourceRepository.InsertAsync(leadSubSource);
         }

		 [AbpAuthorize(AppPermissions.Pages_LeadSubSources_Edit)]
		 protected virtual async Task Update(CreateOrEditLeadSubSourceDto input)
         {
            var leadSubSource = await _leadSubSourceRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, leadSubSource);
         }

		 [AbpAuthorize(AppPermissions.Pages_LeadSubSources_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _leadSubSourceRepository.DeleteAsync(input.Id);
         } 

		public async Task<FileDto> GetLeadSubSourcesToExcel(GetAllLeadSubSourcesForExcelInput input)
         {
			
			var filteredLeadSubSources = _leadSubSourceRepository.GetAll()
						.Include( e => e.LeadSourceFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false  || e.Name.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter),  e => e.Name == input.NameFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceFk != null && e.LeadSourceFk.Name == input.LeadSourceNameFilter);

			var query = (from o in filteredLeadSubSources
                         join o1 in _lookup_leadSourceRepository.GetAll() on o.LeadSourceId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()
                         
                         select new GetLeadSubSourceForViewDto() { 
							LeadSubSource = new LeadSubSourceDto
							{
                                Name = o.Name,
                                Id = o.Id
							},
                         	LeadSourceName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
						 });


            var leadSubSourceListDtos = await query.ToListAsync();

            return _leadSubSourcesExcelExporter.ExportToFile(leadSubSourceListDtos);
         }


			[AbpAuthorize(AppPermissions.Pages_LeadSubSources)]
			public async Task<List<LeadSubSourceLeadSourceLookupTableDto>> GetAllLeadSourceForTableDropdown()
			{
				return await _lookup_leadSourceRepository.GetAll()
					.Select(leadSource => new LeadSubSourceLeadSourceLookupTableDto
					{
						Id = leadSource.Id,
						DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
					}).ToListAsync();
			}
							
    }
}