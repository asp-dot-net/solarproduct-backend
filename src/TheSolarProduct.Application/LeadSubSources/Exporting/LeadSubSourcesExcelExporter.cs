﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.LeadSubSources.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.LeadSubSources.Exporting
{
    public class LeadSubSourcesExcelExporter : NpoiExcelExporterBase, ILeadSubSourcesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LeadSubSourcesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetLeadSubSourceForViewDto> leadSubSources)
        {
            return CreateExcelPackage(
                "LeadSubSources.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("LeadSubSources"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        (L("LeadSource")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, leadSubSources,
                        _ => _.LeadSubSource.Name,
                        _ => _.LeadSourceName
                        );

					
					
                });
        }
    }
}
