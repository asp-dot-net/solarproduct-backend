﻿using System.Collections.Generic;
using TheSolarProduct.LeadSubSources.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.LeadSubSources.Exporting
{
    public interface ILeadSubSourcesExcelExporter
    {
        FileDto ExportToFile(List<GetLeadSubSourceForViewDto> leadSubSources);
    }
}