﻿using System.Collections.Generic;
using TheSolarProduct.PostCodes.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.PostCodes.Exporting
{
    public interface IPostCodesExcelExporter
    {
        FileDto ExportToFile(List<GetPostCodeForViewDto> postCodes);
    }
}