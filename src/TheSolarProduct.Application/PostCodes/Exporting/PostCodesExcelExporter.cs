﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.PostCodes.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.PostCodes.Exporting
{
    public class PostCodesExcelExporter : NpoiExcelExporterBase, IPostCodesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PostCodesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPostCodeForViewDto> postCodes)
        {
            return CreateExcelPackage(
                "PostCodes.xlsx",
                excelPackage =>
                {
                    
                    var sheet = excelPackage.CreateSheet(L("PostCodes"));

                    AddHeader(
                        sheet,
                        L("PostalCode"),
                        L("Suburb"),
                        L("POBoxes"),
                        L("Area"),
                        (L("State")) + L("Name")
                        );

                    AddObjects(
                        sheet, 2, postCodes,
                        _ => _.PostCode.PostalCode,
                        _ => _.PostCode.Suburb,
                        _ => _.PostCode.POBoxes,
                        _ => _.PostCode.Area,
                        _ => _.StateName
                        );

					
					
                });
        }
    }
}
