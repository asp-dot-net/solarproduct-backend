﻿using TheSolarProduct.States;
using System.Collections.Generic;


using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.PostCodes.Exporting;
using TheSolarProduct.PostCodes.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;

namespace TheSolarProduct.PostCodes
{
	[AbpAuthorize(AppPermissions.Pages_PostCodes)]
	public class PostCodesAppService : TheSolarProductAppServiceBase, IPostCodesAppService
	{
		private readonly IRepository<PostCode> _postCodeRepository;
		private readonly IPostCodesExcelExporter _postCodesExcelExporter;
		private readonly IRepository<State, int> _lookup_stateRepository;


		public PostCodesAppService(IRepository<PostCode> postCodeRepository, IPostCodesExcelExporter postCodesExcelExporter, IRepository<State, int> lookup_stateRepository)
		{
			_postCodeRepository = postCodeRepository;
			_postCodesExcelExporter = postCodesExcelExporter;
			_lookup_stateRepository = lookup_stateRepository;

		}

		public async Task<PagedResultDto<GetPostCodeForViewDto>> GetAll(GetAllPostCodesInput input)
		{

			var filteredPostCodes = _postCodeRepository.GetAll()
						.Include(e => e.StateFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false ||
							e.PostalCode.Contains(input.Filter) ||
							e.Suburb.Contains(input.Filter) ||
							e.POBoxes.Contains(input.Filter) ||
							e.Area.Contains(input.Filter) ||
							e.StateFk.Name.Contains(input.Filter)
							)
						.WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFilter), e => e.PostalCode == input.PostalCodeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.POBoxesFilter), e => e.POBoxes == input.POBoxesFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.AreaFilter), e => e.Area == input.AreaFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateFk != null && e.StateFk.Name == input.StateNameFilter);

			var pagedAndFilteredPostCodes = filteredPostCodes
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input);

			var postCodes = from o in pagedAndFilteredPostCodes
							join o1 in _lookup_stateRepository.GetAll() on o.StateId equals o1.Id into j1
							from s1 in j1.DefaultIfEmpty()

							select new GetPostCodeForViewDto()
							{
								PostCode = new PostCodeDto
								{
									PostalCode = o.PostalCode,
									Suburb = o.Suburb,
									POBoxes = o.POBoxes,
									Area = o.Area,
									Id = o.Id,
									Areas = o.Areas,
									IsActive = o.IsActive,
								},
								StateName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
							};

			var totalCount = await filteredPostCodes.CountAsync();

			return new PagedResultDto<GetPostCodeForViewDto>(
				totalCount,
				await postCodes.ToListAsync()
			);
		}

		public async Task<GetPostCodeForViewDto> GetPostCodeForView(int id)
		{
			var postCode = await _postCodeRepository.GetAsync(id);

			var output = new GetPostCodeForViewDto { PostCode = ObjectMapper.Map<PostCodeDto>(postCode) };

			if (output.PostCode.StateId != null)
			{
				var _lookupState = await _lookup_stateRepository.FirstOrDefaultAsync((int)output.PostCode.StateId);
				output.StateName = _lookupState?.Name?.ToString();
			}

			return output;
		}

		[AbpAuthorize(AppPermissions.Pages_PostCodes_Edit)]
		public async Task<GetPostCodeForEditOutput> GetPostCodeForEdit(EntityDto input)
		{
			var postCode = await _postCodeRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetPostCodeForEditOutput { PostCode = ObjectMapper.Map<CreateOrEditPostCodeDto>(postCode) };

			if (output.PostCode.StateId != null)
			{
				var _lookupState = await _lookup_stateRepository.FirstOrDefaultAsync((int)output.PostCode.StateId);
				output.StateName = _lookupState?.Name?.ToString();
			}

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditPostCodeDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_PostCodes_Create)]
		protected virtual async Task Create(CreateOrEditPostCodeDto input)
		{
			var postCode = ObjectMapper.Map<PostCode>(input);
			postCode.PostalCode = postCode.PostalCode.ToUpper();
			postCode.Suburb = postCode.Suburb.ToUpper();
			postCode.POBoxes = postCode.POBoxes.ToUpper();
			postCode.Area = postCode.Area.ToUpper();



			await _postCodeRepository.InsertAsync(postCode);
		}

		[AbpAuthorize(AppPermissions.Pages_PostCodes_Edit)]
		protected virtual async Task Update(CreateOrEditPostCodeDto input)
		{
			var postCode = await _postCodeRepository.FirstOrDefaultAsync((int)input.Id);
			postCode.PostalCode = postCode.PostalCode.ToUpper();
			postCode.Suburb = postCode.Suburb.ToUpper();
			postCode.POBoxes = postCode.POBoxes.ToUpper();
			postCode.Area = postCode.Area.ToUpper();
			ObjectMapper.Map(input, postCode);
		}

		[AbpAuthorize(AppPermissions.Pages_PostCodes_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _postCodeRepository.DeleteAsync(input.Id);
		}

		public async Task<FileDto> GetPostCodesToExcel(GetAllPostCodesForExcelInput input)
		{

			var filteredPostCodes = _postCodeRepository.GetAll()
						.Include(e => e.StateFk)
						.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.PostalCode.Contains(input.Filter) || e.Suburb.Contains(input.Filter) || e.POBoxes.Contains(input.Filter) || e.Area.Contains(input.Filter))
						.WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFilter), e => e.PostalCode == input.PostalCodeFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.SuburbFilter), e => e.Suburb == input.SuburbFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.POBoxesFilter), e => e.POBoxes == input.POBoxesFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.AreaFilter), e => e.Area == input.AreaFilter)
						.WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateFk != null && e.StateFk.Name == input.StateNameFilter);

			var query = (from o in filteredPostCodes
						 join o1 in _lookup_stateRepository.GetAll() on o.StateId equals o1.Id into j1
						 from s1 in j1.DefaultIfEmpty()

						 select new GetPostCodeForViewDto()
						 {
							 PostCode = new PostCodeDto
							 {
								 PostalCode = o.PostalCode,
								 Suburb = o.Suburb,
								 POBoxes = o.POBoxes,
								 Area = o.Area,
								 Id = o.Id,
								 Areas = o.Areas,
								 IsActive = o.IsActive,
							 },
							 StateName = s1 == null || s1.Name == null ? "" : s1.Name.ToString()
						 });


			var postCodeListDtos = await query.ToListAsync();

			return _postCodesExcelExporter.ExportToFile(postCodeListDtos);
		}


		[AbpAuthorize(AppPermissions.Pages_PostCodes)]
		public async Task<List<PostCodeStateLookupTableDto>> GetAllStateForTableDropdown()
		{
			return await _lookup_stateRepository.GetAll()
				.Select(state => new PostCodeStateLookupTableDto
				{
					Id = state.Id,
					DisplayName = state == null || state.Name == null ? "" : state.Name.ToString()
				}).ToListAsync();
		}

	}
}