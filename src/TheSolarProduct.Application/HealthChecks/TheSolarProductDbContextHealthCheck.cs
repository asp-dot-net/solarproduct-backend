﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.HealthChecks
{
    public class TheSolarProductDbContextHealthCheck : IHealthCheck
    {
        private readonly DatabaseCheckHelper _checkHelper;

        public TheSolarProductDbContextHealthCheck(DatabaseCheckHelper checkHelper)
        {
            _checkHelper = checkHelper;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            if (_checkHelper.Exist("db"))
            {
                return Task.FromResult(HealthCheckResult.Healthy("TheSolarProductDbContext connected to database."));
            }

            return Task.FromResult(HealthCheckResult.Unhealthy("TheSolarProductDbContext could not connect to database"));
        }
    }
}
