﻿using TheSolarProduct.Jobs;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Repositories;
using TheSolarProduct.Jobs.Dtos;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.Storage;
using TheSolarProduct.Common;
using TheSolarProduct.Wholesales.Ecommerces;
using TheSolarProduct.ECommerces.EcommerceProductItems.Dtos;
using TheSolarProduct.MultiTenancy;
using NPOI.HPSF;
using TheSolarProduct.ApplicationSettings;
using System.Windows.Forms.Design;
using Microsoft.Azure.KeyVault.Models;
using System.IO;
using TheSolarProduct.Authorization.Users;
using TheSolarProduct.Migrations;
using TheSolarProduct.Wholesales.DataVault;
using Abp.EntityFrameworkCore;
using TheSolarProduct.EntityFrameworkCore;
using TheSolarProduct.ECommerceSliders;
using TheSolarProduct.EcommerceProducctSpecification.Dtos;
using TheSolarProduct.CommmonActivityLogs;
using TheSolarProduct.UserActivityLogs.Dtos;
using Abp.Timing.Timezone;
using TheSolarProduct.Timing;
using TheSolarProduct.WholeSaleLeads.Dtos;
using TheSolarProduct.WholeSaleStatuses;

namespace TheSolarProduct.UserActivityLogs
{
    public class UserActivityLogAppService : TheSolarProductAppServiceBase, IUserActivityLogAppService
    {
        private readonly IRepository<UserActivityLog> _userActivityLogRepository;
        private readonly IRepository<CommonSection> _commonSectionRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ITimeZoneService _timeZoneService;
        private readonly IRepository<User, long> _userRepository;

        public UserActivityLogAppService(
           IRepository<UserActivityLog> userActivityLogRepository
            , IRepository<CommonSection> commonSectionRepository
            , ITimeZoneConverter timeZoneConverter
            , ITimeZoneService timeZoneService
            , IRepository<User, long> userRepository) 
        { 
            _userActivityLogRepository = userActivityLogRepository;
            _commonSectionRepository = commonSectionRepository;
            _timeZoneConverter = timeZoneConverter;
            _timeZoneService = timeZoneService;
            _userRepository = userRepository;
        }

        public async Task Create(UserActivityLogDto input)
        {
            var log = ObjectMapper.Map<UserActivityLog>(input);
            if (!string.IsNullOrEmpty(input.Section))
            {
                log.SectionId = await _commonSectionRepository.GetAll().Where(e => e.SectionName == input.Section).Select(e => e.Id).FirstOrDefaultAsync();               
            }
            log.CreatorUserId = AbpSession.UserId;
           // await _userActivityLogRepository.InsertAsync(log);
        }

        public async Task<PagedResultDto<GetAllUserActivityLogDto>> GetAll(GetUserActivitylogInputDto input)
        {
            var SDate = (_timeZoneConverter.Convert(input.Sdate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.Edate, (int)AbpSession.TenantId));
            var diffHour = _timeZoneService.GetTimeZoneDiffHours((int)AbpSession.TenantId);

            var filteredLog = _userActivityLogRepository.GetAll().Include( e=> e.LeadAction).Include(e => e.SectionFK)
                        .WhereIf(input.Sdate != null, e => e.CreationTime.AddHours(diffHour).Date >= SDate.Value.Date)
                        .WhereIf(input.Edate != null, e => e.CreationTime.AddHours(diffHour).Date <= EDate.Value.Date)
                        .WhereIf(input.UserId > 0, e => e.CreatorUserId == input.UserId);

            var pagedAndFilteredLog = filteredLog
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input);

            var output = from o in pagedAndFilteredLog
                         select new GetAllUserActivityLogDto()
                         {
                             UserActivityLogs = new UserActivityLogDto()
                             {
                                 Id = o.Id,
                                 ActionId = o.ActionId,
                                 ActionNote = o.ActionNote,
                                 Section = o.SectionFK.SectionName,
                             },
                             CreatedBy = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                             CreationTime = o.CreationTime,
                             Action = o.LeadAction.ActionName
                         };

            var totalCount = await filteredLog.CountAsync();

            var result = await output.ToListAsync();

            return new PagedResultDto<GetAllUserActivityLogDto>(
               totalCount,
               result
           );
        }
    }
}
