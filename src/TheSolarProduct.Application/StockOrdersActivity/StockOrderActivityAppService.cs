﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using TheSolarProduct.StockOrders;
using TheSolarProduct.StockOrdersActivity.Dtos;
using TheSolarProduct.Vendors;
using System.Linq;
using TheSolarProduct.Jobs.Dtos;
using TheSolarProduct.SmsTemplates;
using Microsoft.EntityFrameworkCore;
using TheSolarProduct.ApplicationSettings.Dto;
using TheSolarProduct.Authorization.Users;
using System.Data.Entity;
using TheSolarProduct.ApplicationSettings;
using TheSolarProduct.QuickStocks;
using Abp.Runtime.Session;
using System.Net.Mail;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.UI;
using Abp.Timing.Timezone;
using TheSolarProduct.Notifications;
using TheSolarProduct.LeadActivityLogs;



namespace TheSolarProduct.StockOrdersActivity
{
    public class StockOrderActivityAppService : IStockOrderActivityAppService
    {
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<SmsTemplate> _smsTemplateRepository;
        private readonly IRepository<QuickStockSection> _SectionRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<QuickStockActivityLog> _QuickStockActivityLogRepository;
        private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IEmailSender _emailSender;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAppNotifier _appNotifier;
        public IAbpSession AbpSession { get; set; }

        public StockOrderActivityAppService(
            IRepository<Vendor> vendorRepository,
            IRepository<PurchaseOrder> purchaseOrderRepository,
            IRepository<SmsTemplate> smsTemplateRepository,
            IRepository<QuickStockSection> SectionRepository,
            IRepository<User, long> userRepository,
            IApplicationSettingsAppService applicationSettings,
            IRepository<QuickStockActivityLog> QuickStockActivityLogRepository,
            IEmailSender emailSender,
            ITimeZoneConverter timeZoneConverter,
            IAppNotifier appNotifier

            )
        {
            _vendorRepository = vendorRepository;
            _purchaseOrderRepository = purchaseOrderRepository;
            _smsTemplateRepository = smsTemplateRepository;
            _SectionRepository =SectionRepository;
            _userRepository = userRepository;
            _applicationSettings=applicationSettings;
            _QuickStockActivityLogRepository = QuickStockActivityLogRepository;
            _emailSender = emailSender;
            _timeZoneConverter = timeZoneConverter;
            _appNotifier = appNotifier; 
        }
        public async Task<GetVendorForSMSEmailDto> GetVendorForSMSEmailActivity(int id)
        {
            var purchaseOrder = await _purchaseOrderRepository.GetAsync(id);

            var vendorId = purchaseOrder.VendorId;
            var vendor = await _vendorRepository.GetAsync(vendorId.Value);

            var dto = new GetVendorForSMSEmailDto
            {
                CompanyName = vendor.CompanyName,
                Mobile = vendor.Phone,
                Email = vendor.Email,
                OrderNo = purchaseOrder.OrderNo.ToString()
            };


            return dto;
        }

        public async Task SendSms(SMSEmailDto input)
        {
            
            var sectionName = _SectionRepository.GetAll().Where(e => e.Id == input.TrackerId).FirstOrDefault().SectionName;
           
            var purchaseorder = _purchaseOrderRepository.GetAll().Where(e => e.Id == input.PurchaseOrderId).FirstOrDefault();
            //var output = ObjectMapper.Map<CreateOrEditStockOrderDto>(purchaseorder);
          
            var vendorId = purchaseorder.VendorId;
            var mobilenumber =  _vendorRepository.GetAll().Where(e => e.Id == vendorId).Select(e => e.Phone).FirstOrDefault();

            //if (!string.IsNullOrEmpty(mobilenumber))
            //{
            //var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            QuickStockActivityLog quickStockActivity = new QuickStockActivityLog();
            quickStockActivity.ActionId = 6;
            quickStockActivity.ActionNote = "Sms Send From " + sectionName;
            quickStockActivity.PurchaseOrderId = (int)input.PurchaseOrderId;
            quickStockActivity.Subject = input.Body;
            quickStockActivity.ActivityDate = DateTime.UtcNow;
            quickStockActivity.SectionId = input.TrackerId;
            quickStockActivity.Type = "StockOrder";
            if (input.SMSTemplateId != null && input.SMSTemplateId != 0)
            {
                quickStockActivity.TemplateId = input.SMSTemplateId;
            }
            if (AbpSession.TenantId != null)
            {
                quickStockActivity.TenantId = (int)AbpSession.TenantId;
            }
            else
            {
                quickStockActivity.TemplateId = 0;
            }
            quickStockActivity.Body = input.Body;
            await _QuickStockActivityLogRepository.InsertAndGetIdAsync(quickStockActivity);

                SendSMSInput sendSMSInput = new SendSMSInput();
                sendSMSInput.PhoneNumber = mobilenumber;
                sendSMSInput.Text = input.Body;
                sendSMSInput.ActivityId = quickStockActivity.Id;
                await _applicationSettings.SendStockOrderSMS(sendSMSInput);
            

        }

        public async Task SendEmail(SMSEmailDto input)
        {
            var Email = "";
            var leadid = 0;
            var sectionName = _SectionRepository.GetAll().Where(e => e.Id == input.TrackerId).FirstOrDefault().SectionName;

            //if (input.TrackerId == 2)
            //{
            //    var jobPromotion = await _jobPromotionRepository.FirstOrDefaultAsync((int)input.ID);
            //    var output = ObjectMapper.Map<CreateOrEditJobPromotionDto>(jobPromotion);
            //    var link = _freebieTransportRepository.GetAll().Where(e => e.Id == output.FreebieTransportId).Select(e => e.TransportLink).FirstOrDefault();
            //    leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).FirstOrDefault().LeadId;
            //    Email = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
            //    var lead = await _leadRepository.FirstOrDefaultAsync((int)leadid);
            //    var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
            //    if (!string.IsNullOrEmpty(Email))
            //    {
            //        output.EmailSend = true;
            //        output.EmailSendDate = DateTime.UtcNow;
            //        ObjectMapper.Map(output, jobPromotion);
            //    }

            //}
            //else if (input.TrackerId == 4)
            //{
            //    var jobRefund = await _jobRefundRepository.FirstOrDefaultAsync((int)input.ID);
            //    var output = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefund);
            //    leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
            //    Email = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
            //    var lead = await _leadRepository.FirstOrDefaultAsync((int)leadid);
            //    var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
            //    if (!string.IsNullOrEmpty(Email))
            //    {
            //        output.JobRefundEmailSend = true;
            //        output.JobRefundSmsSendDate = DateTime.UtcNow;
            //        ObjectMapper.Map(output, jobRefund);
            //    }
            //}
            //else if (input.TrackerId == 10)
            //{
            //    var inmportdatas = await _InvoiceImportDataRepository.FirstOrDefaultAsync((int)input.ID);
            //    /// var output = ObjectMapper.Map<CreateOrEditImportDto>(inmportdatas);
            //    leadid = (int)_jobRepository.GetAll().Where(e => e.Id == input.LeadId).Select(e => e.LeadId).FirstOrDefault();
            //    Email = _leadRepository.GetAll().Where(e => e.Id == input.LeadId).Select(e => e.Email).FirstOrDefault();
            //    var lead = await _leadRepository.FirstOrDefaultAsync((int)input.LeadId);
            //    var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();

            //    List<SendEmailAttachmentDto> emailattch = new List<SendEmailAttachmentDto>();
            //    SendEmailAttachmentDto doc = new SendEmailAttachmentDto();
            //    if (input.TaxInvoiceDocPath != null)
            //    {
            //        doc.FileName = input.TaxInvoiceFileName;
            //        doc.FilePath = input.TaxInvoiceDocPath;
            //        emailattch.Add(doc);
            //    }
            //}
            //else if (input.TrackerId == 26)
            //{
            //    var installerinvoice = await _jobInstallerInvoiceRepository.FirstOrDefaultAsync((int)input.ID);
            //    var output = ObjectMapper.Map<CreateOrEditJobInstallerInvoiceDto>(installerinvoice);
            //    leadid = (int)_jobRepository.GetAll().Where(e => e.Id == output.JobId).Select(e => e.LeadId).FirstOrDefault();
            //    Email = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.Email).FirstOrDefault();
            //    var lead = await _leadRepository.FirstOrDefaultAsync((int)leadid);
            //    var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.AssignToUserID).FirstOrDefault();
            //    if (!string.IsNullOrEmpty(Email))
            //    {
            //        output.installerEmailSend = true;
            //        output.EmaiSendDate = DateTime.UtcNow;
            //        ObjectMapper.Map(output, installerinvoice);
            //    }
            //}
            //else
            //{
            var purchaseorder = _purchaseOrderRepository.GetAll().Where(e => e.Id == input.PurchaseOrderId).FirstOrDefault();
            //var output = ObjectMapper.Map<CreateOrEditStockOrderDto>(purchaseorder);

            var vendorId = purchaseorder.VendorId;
            Email = _vendorRepository.GetAll().Where(e => e.Id == vendorId).Select(e => e.Email).FirstOrDefault();
            //var Jobid = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).Select(e => e.Id).FirstOrDefault();
            //var job = await _jobRepository.FirstOrDefaultAsync(Jobid);
            //var output = ObjectMapper.Map<CreateOrEditJobDto>(job);
            //if (!string.IsNullOrEmpty(Email))
            //{
            //    if (input.TrackerId == 1 && job.DistApproveDate != null)
            //    {
            //        output.EmailSend = true;
            //        output.EmailSendDate = DateTime.UtcNow;
            //    }
            //    if (input.TrackerId == 3)
            //    {
            //        output.FinanceEmailSend = true;
            //        output.FinanceEmailSendDate = DateTime.UtcNow;
            //    }
            //    if (input.TrackerId == 5)
            //    {
            //        output.JobActiveEmailSend = true;
            //        output.JobActiveEmailSendDate = DateTime.UtcNow;
            //    }
            //    if (input.TrackerId == 6)
            //    {
            //        output.GridConnectionEmailSend = true;
            //        output.GridConnectionEmailSendDate = DateTime.UtcNow;
            //    }
            //    if (input.TrackerId == 7)
            //    {
            //        output.StcEmailSend = true;
            //        output.StcEmailSendDate = DateTime.UtcNow;
            //    }
            //    if (input.TrackerId == 8)
            //    {
            //        output.JobGridEmailSend = true;
            //        output.JobGridEmailSendDate = DateTime.UtcNow;
            //    }
            //    if (input.TrackerId == 11)
            //    {
            //        output.PendingInstallerEmailSend = true;
            //        output.PendingInstallerEmailSendDate = DateTime.UtcNow;
            //    }
            //    ObjectMapper.Map(output, job);
            //}


            int? TemplateId;
            if (input.EmailTemplateId != null && input.EmailTemplateId != 0)
            {
                TemplateId = input.EmailTemplateId;
            }
            else
            {
                TemplateId = 0;
            }

            if (!string.IsNullOrEmpty(Email))
            {
                if (input.cc != null && input.Bcc != null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { "dakshina.quickforms@gmail.com" }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else if (input.cc != null && input.cc != "" && input.Bcc == null)
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { "dakshina.quickforms@gmail.com" }, //{ "viral.jain@meghtechnologies.com" }, //
                        CC = { input.cc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else if (input.cc == null && input.Bcc != null && input.Bcc != "")
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        //To = { Email }, 
                        To = { "dakshina.quickforms@gmail.com" },
                        Bcc = { input.Bcc },
                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                else
                {
                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress(input.EmailFrom),
                        To = { "dakshina.quickforms@gmail.com" },

                        Subject = input.Subject,
                        Body = input.Body,
                        IsBodyHtml = true
                    };
                    await this._emailSender.SendAsync(mail);
                }

                var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                QuickStockActivityLog quickStockactivity = new QuickStockActivityLog();
                quickStockactivity.ActionId = 7;
                quickStockactivity.ActionNote = "Email Send From " + sectionName;
                quickStockactivity.PurchaseOrderId = (int)input.PurchaseOrderId;
                quickStockactivity.TemplateId = TemplateId;
                quickStockactivity.Subject = input.Subject;
                quickStockactivity.Body = input.Body;
                quickStockactivity.Type = "StockOrder";
                quickStockactivity.ActivityDate = DateTime.UtcNow;
                quickStockactivity.SectionId = input.TrackerId;
               

                if (AbpSession.TenantId != null)
                {
                    quickStockactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _QuickStockActivityLogRepository.InsertAsync(quickStockactivity);
            }

        }

        public async Task AddReminderActivityLog(StockOrderActivityLogInput input)
        {
            input.ActivityDate = _timeZoneConverter.Convert(input.ActivityDate, (int)AbpSession.TenantId);
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var PurchaseOrder = await _purchaseOrderRepository.FirstOrDefaultAsync((int)input.PurchaseOrderId);
            var sectionName = _SectionRepository.GetAll().Where(e => e.Id == input.SectionId).FirstOrDefault().SectionName;
            //va/*r assignedToUser = _userRepository.GetAll().Where(u => u.Id == PurchaseOrder.AssignToUserID).FirstOrDefault();*/

            DateTime utc = DateTime.UtcNow;
            var CurrentTime = (_timeZoneConverter.Convert(utc, (int)AbpSession.TenantId));

            if (input.ActivityDate < CurrentTime)
            {
                throw new UserFriendlyException(("PleaseSelectGreaterDatethanCurrentDate"));
            }
            var activity = new QuickStockActivityLog();
            activity.ActionId = 8;
            activity.ActionNote = "Reminder Set";
            activity.SectionId = input.SectionId;
            activity.CreatorUserId = AbpSession.UserId;
            //activity.Action = input.ActivityNote;
            activity.Type = "StockOrder";
            activity.ActivityDate = input.ActivityDate;
            activity.PurchaseOrderId = (int)input.PurchaseOrderId;
           

            await _QuickStockActivityLogRepository.InsertAndGetIdAsync(activity);

           
            if (input.SectionId == 1) // Add My Jobs Tracker
            {
                var StockOrder = await _purchaseOrderRepository.FirstOrDefaultAsync(e => e.Id == activity.PurchaseOrderId);
                StockOrder.ReminderTime = activity.ActivityDate;
                StockOrder.ReminderDes = input.ActivityNote;

                await _purchaseOrderRepository.UpdateAsync(StockOrder);
            }

            string msg = string.Format("Reminder " + input.ActivityNote + " For " + PurchaseOrder.OrderNo + " By " + CurrentUser.Name + " From " + sectionName);
            await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);
        }

        public async Task AddCommentActivityLog(StockOrderActivityLogInput input)
        {
            var purchaseOrder = await _purchaseOrderRepository.FirstOrDefaultAsync((int)input.PurchaseOrderId);

            var activity = new QuickStockActivityLog();
            activity.ActionId = 24;
            activity.ActionNote = "Comment";
            activity.SectionId = input.SectionId;
            activity.Type = "StockOrder";
            activity.CreatorUserId = AbpSession.UserId;
            activity.PurchaseOrderId = (int)input.PurchaseOrderId;
            await _QuickStockActivityLogRepository.InsertAndGetIdAsync(activity);
            if (input.SectionId == 1)
            { 
                var StockOrder = await _purchaseOrderRepository.FirstOrDefaultAsync(e => e.Id == activity.PurchaseOrderId);

                StockOrder.Comment = input.ActivityNote;

                await _purchaseOrderRepository.UpdateAsync(StockOrder);
            }
        }

        public async Task AddToDoActivityLog(StockOrderActivityLogInput input)
        {
            var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var StockOrder = await _purchaseOrderRepository.FirstOrDefaultAsync((int)input.PurchaseOrderId);
            var sectionName = _SectionRepository.GetAll().Where(e => e.Id == input.SectionId).FirstOrDefault().SectionName;
            var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();

            var companyName= _vendorRepository.GetAll().Where(e =>e.Id ==StockOrder.VendorId).FirstOrDefault().CompanyName;

            var activity = new QuickStockActivityLog();
            activity.ActionId = 25;
            activity.ActionNote = "ToDo";
            activity.SectionId = input.SectionId;
            activity.CreatorUserId = AbpSession.UserId;
            activity.Type = "StockOrder";
            activity.PurchaseOrderId = (int)input.PurchaseOrderId;
           

            await _QuickStockActivityLogRepository.InsertAndGetIdAsync(activity);

            string msg = "ToDo " + input.ActivityNote + " For " + companyName + " By " + CurrentUser.Name + " From " + sectionName;
            await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);
        }
        public async Task<List<CommonLookupDto>> GetallSMSTemplates(int PurchaseOrderId)
        {
            var organizationId = _purchaseOrderRepository.FirstOrDefault(PurchaseOrderId).OrganizationId;

            return await _smsTemplateRepository.GetAll().Where(e => e.OrganizationUnitId == organizationId)
            .Select(smsTemplate => new CommonLookupDto
            {
                Id = smsTemplate.Id,
                DisplayName = smsTemplate == null || smsTemplate.Name == null ? "" : smsTemplate.Name.ToString()
            }).ToListAsync();
        }
    }
}
