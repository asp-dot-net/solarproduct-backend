﻿using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TheSolarProduct.DataExporting.Excel.NPOI;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.Dto;
using TheSolarProduct.Storage;

namespace TheSolarProduct.SmsTemplates.Exporting
{
    public class SmsTemplatesExcelExporter : NpoiExcelExporterBase, ISmsTemplatesExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SmsTemplatesExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager) :
    base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetSmsTemplateForViewDto> smsTemplates)
        {
            return CreateExcelPackage(
                "SmsTemplates.xlsx",
                excelPackage =>
                {

                    var sheet = excelPackage.CreateSheet(L("SmsTemplates"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Text")
                        );

                    AddObjects(
                        sheet, 2, smsTemplates,
                        _ => _.SmsTemplate.Name,
                        _ => _.SmsTemplate.Text
                        );

                });
        }
    }
}