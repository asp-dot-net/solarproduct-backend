﻿using System.Collections.Generic;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.Dto;

namespace TheSolarProduct.SmsTemplates.Exporting
{
    public interface ISmsTemplatesExcelExporter
    {
        FileDto ExportToFile(List<GetSmsTemplateForViewDto> smsTemplates);
    }
}