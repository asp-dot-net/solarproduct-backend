﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TheSolarProduct.SmsTemplates.Exporting;
using TheSolarProduct.SmsTemplates.Dtos;
using TheSolarProduct.Dto;
using Abp.Application.Services.Dto;
using TheSolarProduct.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using Abp.UI;
using TheSolarProduct.Storage;
using Abp.Organizations;
using Abp.EntityFrameworkCore;
using TheSolarProduct.DataVaults;
using TheSolarProduct.EntityFrameworkCore;

namespace TheSolarProduct.SmsTemplates
{
    [AbpAuthorize(AppPermissions.Pages_SmsTemplates)]
    public class SmsTemplatesAppService : TheSolarProductAppServiceBase, ISmsTemplatesAppService
    {
        private readonly IRepository<SmsTemplate> _smsTemplateRepository;
        private readonly ISmsTemplatesExcelExporter _smsTemplatesExcelExporter;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<DataVaultActivityLog> _dataVaultActivityLogRepository;
        private readonly IDbContextProvider<TheSolarProductDbContext> _dbcontextprovider;

        public SmsTemplatesAppService(IRepository<SmsTemplate> smsTemplateRepository, ISmsTemplatesExcelExporter smsTemplatesExcelExporter, IRepository<OrganizationUnit, long> organizationUnitRepository, IRepository<DataVaultActivityLog> dataVaultActivityLogRepository, IDbContextProvider<TheSolarProductDbContext> dbcontextprovider)
        {
            _smsTemplateRepository = smsTemplateRepository;
            _smsTemplatesExcelExporter = smsTemplatesExcelExporter;
            _organizationUnitRepository = organizationUnitRepository;
            _dbcontextprovider = dbcontextprovider;
            _dataVaultActivityLogRepository = dataVaultActivityLogRepository;

        }

        public async Task<PagedResultDto<GetSmsTemplateForViewDto>> GetAll(GetAllSmsTemplatesInput input)
        {

            var filteredSmsTemplates = _smsTemplateRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Text.Contains(input.Filter));
            var pagedAndFilteredSmsTemplates = filteredSmsTemplates
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var smsTemplates = from o in pagedAndFilteredSmsTemplates
                               select new
                               {

                                   o.Name,
                                   o.Text,
                                   Id = o.Id,
                                   Organization = o.OrganizationUnitId
                               };

            var totalCount = await filteredSmsTemplates.CountAsync();

            var dbList = await smsTemplates.ToListAsync();
            var results = new List<GetSmsTemplateForViewDto>();

            foreach (var o in dbList)
            {
                var res = new GetSmsTemplateForViewDto()
                {
                    SmsTemplate = new SmsTemplateDto
                    {

                        Name = o.Name,
                        Text = o.Text,
                        Id = o.Id,
                       
                    },
                    Organization = _organizationUnitRepository.GetAll().Where(e => e.Id == o.Organization).Select(e => e.DisplayName).FirstOrDefault()
                };

                results.Add(res);
            }

            return new PagedResultDto<GetSmsTemplateForViewDto>(
                totalCount,
                results
            );

        }

        public async Task<GetSmsTemplateForViewDto> GetSmsTemplateForView(int id)
        {
            var smsTemplate = await _smsTemplateRepository.GetAsync(id);

            var output = new GetSmsTemplateForViewDto { SmsTemplate = ObjectMapper.Map<SmsTemplateDto>(smsTemplate) };

            return output;
        }

         [AbpAuthorize(AppPermissions.Pages_SmsTemplates_Edit)]
        public async Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEdit(EntityDto input)
        {
            var smsTemplate = await _smsTemplateRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSmsTemplateForEditOutput { SmsTemplate = ObjectMapper.Map<CreateOrEditSmsTemplateDto>(smsTemplate) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditSmsTemplateDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_SmsTemplates_Create)]
        protected virtual async Task Create(CreateOrEditSmsTemplateDto input)
        {
            var smsTemplate = ObjectMapper.Map<SmsTemplate>(input);

            if (AbpSession.TenantId != null)
            {
                smsTemplate.TenantId = (int)AbpSession.TenantId;
            }

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Created";
            dataVaultLog.SectionId = 27;
            dataVaultLog.ActionNote = "SMS Templates Created : " + input.Name;
            await _dataVaultActivityLogRepository.InsertAsync(dataVaultLog);

            await _smsTemplateRepository.InsertAsync(smsTemplate);

        }

        [AbpAuthorize(AppPermissions.Pages_SmsTemplates_Edit)]
        protected virtual async Task Update(CreateOrEditSmsTemplateDto input)
        {
            var smsTemplate = await _smsTemplateRepository.FirstOrDefaultAsync((int)input.Id);

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Updated";
            dataVaultLog.SectionId = 27;
            dataVaultLog.ActionNote = "SMS Templates Updated : " + smsTemplate.Name;

            var dataVaultLogId = await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);
            var List = new List<DataVaultActivityLogHistory>();

            if (input.Name != smsTemplate.Name)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Name";
                history.PrevValue = smsTemplate.Name;
                history.CurValue = input.Name;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.Text != smsTemplate.Text)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Text";
                history.PrevValue = smsTemplate.Text;
                history.CurValue = input.Text;
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            if (input.OrganizationUnitId != smsTemplate.OrganizationUnitId)
            {
                DataVaultActivityLogHistory history = new DataVaultActivityLogHistory();
                history.FieldName = "Organization";
                history.PrevValue = smsTemplate.OrganizationUnitId > 0 ? _organizationUnitRepository.Get((long)smsTemplate.OrganizationUnitId).DisplayName : "";
                history.CurValue = input.OrganizationUnitId > 0 ? _organizationUnitRepository.Get((long)input.OrganizationUnitId).DisplayName : "";
                history.Action = "Update";
                history.ActivityLogId = dataVaultLogId;
                List.Add(history);
            }

            await _dbcontextprovider.GetDbContext().DataVaultActivityLogHistory.AddRangeAsync(List);
            await _dbcontextprovider.GetDbContext().SaveChangesAsync();

            ObjectMapper.Map(input, smsTemplate);

        }

        [AbpAuthorize(AppPermissions.Pages_SmsTemplates_Delete)]
        public async Task Delete(EntityDto input)
        {
            var Name = _smsTemplateRepository.Get(input.Id).Name;

            var dataVaultLog = new DataVaultActivityLog();
            dataVaultLog.Action = "Deleted";
            dataVaultLog.SectionId = 27;
            dataVaultLog.ActionNote = "SMS Templates Deleted : " + Name;

            await _dataVaultActivityLogRepository.InsertAndGetIdAsync(dataVaultLog);

            await _smsTemplateRepository.DeleteAsync(input.Id);
        }

        public async Task<FileDto> GetSmsTemplatesToExcel(GetAllSmsTemplatesForExcelInput input)
        {

            var filteredSmsTemplates = _smsTemplateRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Text.Contains(input.Filter));

            var query = (from o in filteredSmsTemplates
                         select new GetSmsTemplateForViewDto()
                         {
                             SmsTemplate = new SmsTemplateDto
                             {
                                 Name = o.Name,
                                 Text = o.Text,
                                 Id = o.Id
                             }
                         });

            var smsTemplateListDtos = await query.ToListAsync();

            return _smsTemplatesExcelExporter.ExportToFile(smsTemplateListDtos);
        }
        
    }
}