﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using TheSolarProduct.Dto;

namespace TheSolarProduct.Gdpr
{
    public interface IUserCollectedDataProvider
    {
        Task<List<FileDto>> GetFiles(UserIdentifier user);
    }
}
