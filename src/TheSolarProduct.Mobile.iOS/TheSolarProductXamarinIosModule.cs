﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace TheSolarProduct
{
    [DependsOn(typeof(TheSolarProductXamarinSharedModule))]
    public class TheSolarProductXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductXamarinIosModule).GetAssembly());
        }
    }
}