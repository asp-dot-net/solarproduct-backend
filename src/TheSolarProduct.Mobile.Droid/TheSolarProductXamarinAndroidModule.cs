﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace TheSolarProduct
{
    [DependsOn(typeof(TheSolarProductXamarinSharedModule))]
    public class TheSolarProductXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TheSolarProductXamarinAndroidModule).GetAssembly());
        }
    }
}