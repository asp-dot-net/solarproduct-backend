
namespace TheSolarProduct.Report
{
	partial class NewQuote
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewQuote));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.textBox123 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox138 = new Telerik.Reporting.TextBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox147 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.textBox165 = new Telerik.Reporting.TextBox();
            this.textBox164 = new Telerik.Reporting.TextBox();
            this.textBox167 = new Telerik.Reporting.TextBox();
            this.textBox166 = new Telerik.Reporting.TextBox();
            this.textBox169 = new Telerik.Reporting.TextBox();
            this.textBox168 = new Telerik.Reporting.TextBox();
            this.textBox170 = new Telerik.Reporting.TextBox();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.textBox173 = new Telerik.Reporting.TextBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.pictureBox8 = new Telerik.Reporting.PictureBox();
            this.pictureBox9 = new Telerik.Reporting.PictureBox();
            this.textBox175 = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox171 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(148.5D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox29,
            this.textBox28,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox61,
            this.textBox65,
            this.textBox64,
            this.textBox63,
            this.textBox62,
            this.textBox67,
            this.textBox66,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox80,
            this.textBox79,
            this.textBox78,
            this.textBox77,
            this.textBox76,
            this.textBox85,
            this.textBox84,
            this.textBox83,
            this.textBox82,
            this.textBox81,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox89,
            this.textBox90,
            this.textBox91,
            this.textBox92,
            this.textBox93,
            this.textBox60,
            this.textBox94,
            this.textBox95,
            this.textBox97,
            this.textBox96,
            this.textBox98,
            this.textBox99,
            this.textBox100,
            this.textBox101,
            this.pictureBox2,
            this.textBox124,
            this.textBox123,
            this.textBox122,
            this.textBox121,
            this.textBox120,
            this.textBox119,
            this.textBox118,
            this.textBox117,
            this.textBox116,
            this.textBox115,
            this.textBox114,
            this.textBox113,
            this.textBox112,
            this.textBox111,
            this.textBox110,
            this.textBox109,
            this.textBox108,
            this.textBox107,
            this.textBox106,
            this.textBox104,
            this.textBox103,
            this.textBox102,
            this.textBox126,
            this.textBox125,
            this.pictureBox3,
            this.pictureBox4,
            this.pictureBox5,
            this.textBox127,
            this.textBox129,
            this.textBox128,
            this.textBox130,
            this.textBox132,
            this.textBox131,
            this.textBox134,
            this.textBox133,
            this.textBox136,
            this.textBox135,
            this.textBox138,
            this.textBox137,
            this.textBox140,
            this.textBox139,
            this.textBox142,
            this.textBox141,
            this.textBox144,
            this.textBox143,
            this.textBox146,
            this.textBox145,
            this.textBox148,
            this.textBox147,
            this.textBox149,
            this.textBox151,
            this.textBox150,
            this.textBox165,
            this.textBox164,
            this.textBox167,
            this.textBox166,
            this.textBox169,
            this.textBox168,
            this.textBox170,
            this.textBox174,
            this.textBox173,
            this.pictureBox6,
            this.pictureBox7,
            this.pictureBox8,
            this.pictureBox9,
            this.textBox175,
            this.panel1});
            this.detailSection1.Name = "detailSection1";
            this.detailSection1.Style.Font.Name = "Verdana";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.Font.Name = "Verdana";
            this.pictureBox1.Value = "";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.2D), Telerik.Reporting.Drawing.Unit.Cm(1.6D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Verdana";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(2.2D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Verdana";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(5.1D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.55D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Verdana";
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(5.7D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox4.Style.Font.Bold = false;
            this.textBox4.Style.Font.Name = "Verdana";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(6.1D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox5.Style.Font.Bold = false;
            this.textBox5.Style.Font.Name = "Verdana";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(6.5D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox6.Style.Font.Bold = false;
            this.textBox6.Style.Font.Name = "Verdana";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(6.9D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox7.Style.Font.Bold = false;
            this.textBox7.Style.Font.Name = "Verdana";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(7.7D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Verdana";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(8.1D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Verdana";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(8.5D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Verdana";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(8.9D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Verdana";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(9.3D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Verdana";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(9.7D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Verdana";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = "";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(10.1D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Verdana";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(5.1D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.75D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Verdana";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(5.7D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Verdana";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.Value = "Photovoltaic System, Including";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(12.4D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox19.Style.Font.Name = "Verdana";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.Value = "25 years performance warranty on the solar panels";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(12.9D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox20.Style.Font.Name = "Verdana";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = "5+5 Years Inverter Warranty";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(13.4D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox21.Style.Font.Name = "Verdana";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = "10 Years of Workmanship Warranty";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(13.9D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox22.Style.Font.Name = "Verdana";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Style.Visible = false;
            this.textBox22.Value = "10 Years of Workmanship Warranty";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(5.7D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Verdana";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(15.9D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Verdana";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.Value = "";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(16.5D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox25.Style.Font.Bold = false;
            this.textBox25.Style.Font.Name = "Verdana";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "";
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.7D), Telerik.Reporting.Drawing.Unit.Cm(16.5D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Verdana";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(17.1D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Verdana";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.Value = "";
            // 
            // textBox29
            // 
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(17.7D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox29.Style.Font.Bold = false;
            this.textBox29.Style.Font.Name = "Verdana";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.Value = "";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(18.3D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox28.Style.Font.Bold = false;
            this.textBox28.Style.Font.Name = "Verdana";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.Value = "";
            // 
            // textBox30
            // 
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(18.9D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox30.Style.Font.Bold = false;
            this.textBox30.Style.Font.Name = "Verdana";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.Value = "";
            // 
            // textBox31
            // 
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(20.1D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Verdana";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.Value = "";
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.2D), Telerik.Reporting.Drawing.Unit.Cm(22.2D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.4D), Telerik.Reporting.Drawing.Unit.Cm(3.2D));
            this.textBox32.Style.Font.Name = "Verdana";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox32.Value = "";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(14D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Verdana";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "";
            // 
            // textBox34
            // 
            this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(14.5D));
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Verdana";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "";
            // 
            // textBox35
            // 
            this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(15D));
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Name = "Verdana";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "";
            // 
            // textBox36
            // 
            this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(2D));
            this.textBox36.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Verdana";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox36.Value = "Solar Bridge";
            // 
            // textBox37
            // 
            this.textBox37.KeepTogether = true;
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.textBox37.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Name = "Verdana";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox37.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.TextWrap = true;
            this.textBox37.Value = "QUOTATION";
            // 
            // textBox38
            // 
            this.textBox38.KeepTogether = true;
            this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(1.6D));
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox38.Style.Color = System.Drawing.Color.Black;
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Verdana";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox38.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.TextWrap = true;
            this.textBox38.Value = "No.";
            // 
            // textBox39
            // 
            this.textBox39.KeepTogether = true;
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(2.8D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.textBox39.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Name = "Verdana";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox39.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.TextWrap = true;
            this.textBox39.Value = "Quote is Valid for 14 days Only";
            // 
            // textBox40
            // 
            this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(0.75D));
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Font.Name = "Verdana";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.Value = "1300 999 600";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.9D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Name = "Verdana";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.Value = "SOLAR BRIDGE PTY LTD";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.867D), Telerik.Reporting.Drawing.Unit.Cm(2D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox42.Style.Font.Bold = false;
            this.textBox42.Style.Font.Name = "Verdana";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.Value = "Suite 221/52 Currumbin Creek Rd";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.9D), Telerik.Reporting.Drawing.Unit.Cm(2.5D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox43.Style.Font.Bold = false;
            this.textBox43.Style.Font.Name = "Verdana";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.Value = "Currumbin Waters QLD 4223";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.9D), Telerik.Reporting.Drawing.Unit.Cm(3D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox44.Style.Font.Bold = true;
            this.textBox44.Style.Font.Name = "Verdana";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.Value = "ABN 78 644 795 853";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(3.6D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox45.Style.Color = System.Drawing.Color.White;
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Name = "Verdana";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.Value = "CUSTOMER INFORMATION";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(3.6D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox46.Style.Color = System.Drawing.Color.White;
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Font.Name = "Verdana";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.Value = "PRODUCT INFORMATION";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.4D), Telerik.Reporting.Drawing.Unit.Cm(3.8D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox47.Style.Color = System.Drawing.Color.White;
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Name = "Verdana";
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox47.Value = "TOTAL COST";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.4D), Telerik.Reporting.Drawing.Unit.Cm(4.2D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox48.Style.Color = System.Drawing.Color.White;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Name = "Verdana";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.Value = "(Inc. GST)";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(5.7D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox49.Style.Font.Bold = false;
            this.textBox49.Style.Font.Name = "Verdana";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "Mobile";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(6.1D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox50.Style.Font.Bold = false;
            this.textBox50.Style.Font.Name = "Verdana";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = "Email";
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(6.5D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox51.Style.Font.Bold = false;
            this.textBox51.Style.Font.Name = "Verdana";
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.Value = "Site Address";
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(7.7D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox52.Style.Font.Bold = false;
            this.textBox52.Style.Font.Name = "Verdana";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.Value = "Meter Phase";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(8.1D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox53.Style.Font.Bold = false;
            this.textBox53.Style.Font.Name = "Verdana";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = "Meter Upgrade";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(8.5D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox54.Style.Font.Bold = false;
            this.textBox54.Style.Font.Name = "Verdana";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.Value = "Roof Type";
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(8.9D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox55.Style.Font.Bold = false;
            this.textBox55.Style.Font.Name = "Verdana";
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = "Property Type";
            // 
            // textBox56
            // 
            this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(9.3D));
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox56.Style.Font.Bold = false;
            this.textBox56.Style.Font.Name = "Verdana";
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.Value = "Roof Pitch";
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(9.7D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox57.Style.Font.Bold = false;
            this.textBox57.Style.Font.Name = "Verdana";
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.Value = "Electricity Dist.";
            // 
            // textBox58
            // 
            this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(10.1D));
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox58.Style.Font.Bold = false;
            this.textBox58.Style.Font.Name = "Verdana";
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox58.Value = "Electricity Retailer";
            // 
            // textBox59
            // 
            this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(11.2D));
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.Font.Name = "Verdana";
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox59.Value = "Payment Method";
            // 
            // textBox61
            // 
            this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(12.593D));
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox61.Style.Color = System.Drawing.Color.White;
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.Style.Font.Name = "Verdana";
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.Value = "Solar Adviser & Contact Us";
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(15.8D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.Font.Name = "Verdana";
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = "Installation\r\n";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(16.3D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox64.Style.Font.Name = "Verdana";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.Value = "install@solarbridge.com.au";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(16.8D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.Font.Name = "Verdana";
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.Value = "Grid Connection";
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(17.3D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox62.Style.Font.Name = "Verdana";
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.Value = "grid@solarbridge.com.au";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(17.8D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.Font.Name = "Verdana";
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.Value = "Services\r\n";
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(18.3D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox66.Style.Font.Name = "Verdana";
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.Value = "service@solarbridge.com.au";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(19.2D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox68.Style.Color = System.Drawing.Color.White;
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.Font.Name = "Verdana";
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.Value = "Payment Methods";
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(20.5D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.Font.Name = "Verdana";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.Value = "BANK TRANSFER / EFT";
            // 
            // textBox70
            // 
            this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(21D));
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.367D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox70.Style.Font.Bold = false;
            this.textBox70.Style.Font.Name = "Verdana";
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox70.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.Value = "Account Name :";
            // 
            // textBox71
            // 
            this.textBox71.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.167D), Telerik.Reporting.Drawing.Unit.Cm(21D));
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox71.Style.Font.Bold = true;
            this.textBox71.Style.Font.Name = "Verdana";
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.Value = "SOLAR BRIDGE";
            // 
            // textBox72
            // 
            this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(21.4D));
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox72.Style.Font.Name = "Verdana";
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.Value = "BSB : 034439";
            // 
            // textBox73
            // 
            this.textBox73.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(21.8D));
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox73.Style.Font.Name = "Verdana";
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.Value = "Account No. : 270045";
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(22.2D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox74.Style.Color = System.Drawing.Color.Red;
            this.textBox74.Style.Font.Name = "Verdana";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.Value = "Bank : Westpac";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(22.6D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox75.Style.Font.Name = "Verdana";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.Value = "Reference No :";
            // 
            // textBox80
            // 
            this.textBox80.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(23.7D));
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox80.Style.Font.Bold = true;
            this.textBox80.Style.Font.Name = "Verdana";
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox80.Value = "Cheque Payable to";
            // 
            // textBox79
            // 
            this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(24.1D));
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox79.Style.Font.Bold = false;
            this.textBox79.Style.Font.Name = "Verdana";
            this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.Value = "SOLAR BRIDGE PTY LTD.";
            // 
            // textBox78
            // 
            this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(24.5D));
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox78.Style.Font.Name = "Verdana";
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.Value = "Post To";
            // 
            // textBox77
            // 
            this.textBox77.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(24.9D));
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox77.Style.Font.Name = "Verdana";
            this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.Value = "Suite 221/52 Currumbin Creek Rd";
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(25.3D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox76.Style.Color = System.Drawing.Color.Black;
            this.textBox76.Style.Font.Name = "Verdana";
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.Value = "Currumbin Waters QLD 4223";
            // 
            // textBox85
            // 
            this.textBox85.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(26.1D));
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox85.Style.Font.Bold = true;
            this.textBox85.Style.Font.Name = "Verdana";
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.Value = "Over The Phone Payment";
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(26.5D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox84.Style.Font.Bold = false;
            this.textBox84.Style.Font.Name = "Verdana";
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.Value = "1300 999 600";
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(26.9D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox83.Style.Font.Name = "Verdana";
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.Value = "Visa or Master Card Only ";
            // 
            // textBox82
            // 
            this.textBox82.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(27.4D));
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox82.Style.Color = System.Drawing.Color.Red;
            this.textBox82.Style.Font.Name = "Verdana";
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.Value = "A Surcharge of 1% will apply on The Final Payment";
            // 
            // textBox81
            // 
            this.textBox81.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(7D));
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox81.Style.Font.Bold = true;
            this.textBox81.Style.Font.Name = "Verdana";
            this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox81.Value = "System Details";
            // 
            // textBox86
            // 
            this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(11.9D));
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.Style.Font.Name = "Verdana";
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.Value = "Warranty";
            // 
            // textBox87
            // 
            this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(15.9D));
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox87.Style.Font.Bold = false;
            this.textBox87.Style.Font.Name = "Verdana";
            this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox87.Value = "Sub Total (A)";
            // 
            // textBox88
            // 
            this.textBox88.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(16.5D));
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox88.Style.Font.Bold = false;
            this.textBox88.Style.Font.Name = "Verdana";
            this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox88.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox88.Value = "Less STC INCENTIVE";
            // 
            // textBox89
            // 
            this.textBox89.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(17.1D));
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox89.Style.Font.Bold = false;
            this.textBox89.Style.Font.Name = "Verdana";
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox89.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.Value = "Grand Total (A-B)";
            // 
            // textBox90
            // 
            this.textBox90.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(17.7D));
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox90.Style.Font.Bold = false;
            this.textBox90.Style.Font.Name = "Verdana";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox90.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.Value = "Plus Additional Charges";
            // 
            // textBox91
            // 
            this.textBox91.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(18.3D));
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox91.Style.Font.Bold = false;
            this.textBox91.Style.Font.Name = "Verdana";
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox91.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.Value = "Less All Discount";
            // 
            // textBox92
            // 
            this.textBox92.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(18.9D));
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox92.Style.Font.Bold = false;
            this.textBox92.Style.Font.Name = "Verdana";
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox92.Value = "";
            // 
            // textBox93
            // 
            this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(20.1D));
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox93.Style.Font.Bold = true;
            this.textBox93.Style.Font.Name = "Verdana";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.Value = "Balance (Inc. Gst)";
            // 
            // textBox60
            // 
            this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(28.4D));
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
            this.textBox60.Style.Color = System.Drawing.Color.White;
            this.textBox60.Style.Font.Name = "Verdana";
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox60.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox60.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox60.Value = "Provided panel layout for your property is just for a reference only. A final dec" +
    "ision will be made on the day of Installation by CEC Accredited Installer.";
            // 
            // textBox94
            // 
            this.textBox94.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(28.7D));
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
            this.textBox94.Style.Color = System.Drawing.Color.White;
            this.textBox94.Style.Font.Name = "Verdana";
            this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox94.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox94.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox94.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox94.Style.Visible = false;
            this.textBox94.Value = "� Clean Energy Council Limited 2016. For Use By Permitted Licensees Only. Terms O" +
    "f Use Apply ";
            // 
            // textBox95
            // 
            this.textBox95.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(21.6D));
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox95.Style.Font.Bold = true;
            this.textBox95.Style.Font.Name = "Verdana";
            this.textBox95.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox95.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox95.Value = "Special Notes";
            // 
            // textBox97
            // 
            this.textBox97.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(10.5D));
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox97.Style.Font.Bold = true;
            this.textBox97.Style.Font.Name = "Verdana";
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.Value = "";
            // 
            // textBox96
            // 
            this.textBox96.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(10.5D));
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox96.Style.Font.Bold = false;
            this.textBox96.Style.Font.Name = "Verdana";
            this.textBox96.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox96.Value = "NMI Number";
            // 
            // textBox98
            // 
            this.textBox98.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(11.7D));
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.textBox98.Style.Font.Bold = false;
            this.textBox98.Style.Font.Name = "Verdana";
            this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox98.Value = "";
            // 
            // textBox99
            // 
            this.textBox99.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(26.1D));
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.Font.Name = "Verdana";
            this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox99.Value = "Installation Date";
            // 
            // textBox100
            // 
            this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(26.5D));
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox100.Style.Font.Bold = false;
            this.textBox100.Style.Font.Name = "Verdana";
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.Value = "2 to 4 weeks from the date of electricity distributor\'s approval";
            // 
            // textBox101
            // 
            this.textBox101.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.2D), Telerik.Reporting.Drawing.Unit.Cm(16.5D));
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox101.Style.Font.Bold = false;
            this.textBox101.Style.Font.Name = "Verdana";
            this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox101.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox101.Value = "( B )";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox2.MimeType = "";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Style.Font.Name = "Verdana";
            this.pictureBox2.Value = "";
            // 
            // textBox124
            // 
            this.textBox124.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.2D), Telerik.Reporting.Drawing.Unit.Cm(31.156D));
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox124.Style.Font.Bold = true;
            this.textBox124.Style.Font.Name = "Verdana";
            this.textBox124.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox124.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox124.Value = "";
            // 
            // textBox123
            // 
            this.textBox123.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.967D), Telerik.Reporting.Drawing.Unit.Cm(31.756D));
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox123.Style.Font.Bold = true;
            this.textBox123.Style.Font.Name = "Verdana";
            this.textBox123.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox123.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox123.Value = "";
            // 
            // textBox122
            // 
            this.textBox122.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.967D), Telerik.Reporting.Drawing.Unit.Cm(30.956D));
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(2D));
            this.textBox122.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
            this.textBox122.Style.Font.Bold = true;
            this.textBox122.Style.Font.Name = "Verdana";
            this.textBox122.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
            this.textBox122.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox122.Value = "Solar Bridge";
            // 
            // textBox121
            // 
            this.textBox121.KeepTogether = true;
            this.textBox121.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.967D), Telerik.Reporting.Drawing.Unit.Cm(30.456D));
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.textBox121.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
            this.textBox121.Style.Font.Bold = true;
            this.textBox121.Style.Font.Name = "Verdana";
            this.textBox121.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.textBox121.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox121.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox121.TextWrap = true;
            this.textBox121.Value = "QUOTATION";
            // 
            // textBox120
            // 
            this.textBox120.KeepTogether = true;
            this.textBox120.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.967D), Telerik.Reporting.Drawing.Unit.Cm(31.156D));
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.233D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox120.Style.Color = System.Drawing.Color.Black;
            this.textBox120.Style.Font.Bold = true;
            this.textBox120.Style.Font.Name = "Verdana";
            this.textBox120.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox120.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox120.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox120.TextWrap = true;
            this.textBox120.Value = "No.";
            // 
            // textBox119
            // 
            this.textBox119.KeepTogether = true;
            this.textBox119.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.967D), Telerik.Reporting.Drawing.Unit.Cm(32.4D));
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.833D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.textBox119.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
            this.textBox119.Style.Font.Bold = true;
            this.textBox119.Style.Font.Name = "Verdana";
            this.textBox119.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox119.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox119.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox119.TextWrap = true;
            this.textBox119.Value = "Quote is Valid for 14 days Only";
            // 
            // textBox118
            // 
            this.textBox118.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.467D), Telerik.Reporting.Drawing.Unit.Cm(30.4D));
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox118.Style.Font.Bold = true;
            this.textBox118.Style.Font.Name = "Verdana";
            this.textBox118.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox118.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox118.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox118.Value = "1300 999 600";
            // 
            // textBox117
            // 
            this.textBox117.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.867D), Telerik.Reporting.Drawing.Unit.Cm(31.05D));
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox117.Style.Font.Bold = true;
            this.textBox117.Style.Font.Name = "Verdana";
            this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox117.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox117.Value = "SOLAR BRIDGE PTY LTD";
            // 
            // textBox116
            // 
            this.textBox116.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.867D), Telerik.Reporting.Drawing.Unit.Cm(31.65D));
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox116.Style.Font.Bold = false;
            this.textBox116.Style.Font.Name = "Verdana";
            this.textBox116.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox116.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox116.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox116.Value = "Suite 221/52 Currumbin Creek Rd";
            // 
            // textBox115
            // 
            this.textBox115.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.867D), Telerik.Reporting.Drawing.Unit.Cm(32.15D));
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox115.Style.Font.Bold = false;
            this.textBox115.Style.Font.Name = "Verdana";
            this.textBox115.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox115.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox115.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox115.Value = "Currumbin Waters QLD 4223";
            // 
            // textBox114
            // 
            this.textBox114.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.867D), Telerik.Reporting.Drawing.Unit.Cm(32.65D));
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox114.Style.Font.Bold = true;
            this.textBox114.Style.Font.Name = "Verdana";
            this.textBox114.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox114.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox114.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox114.Value = "ABN 78 644 795 853";
            // 
            // textBox113
            // 
            this.textBox113.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.967D), Telerik.Reporting.Drawing.Unit.Cm(33.606D));
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.366D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox113.Style.Color = System.Drawing.Color.White;
            this.textBox113.Style.Font.Bold = true;
            this.textBox113.Style.Font.Name = "Verdana";
            this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox113.Value = "YOUR SOLAR PROPOSAL";
            // 
            // textBox112
            // 
            this.textBox112.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.006D), Telerik.Reporting.Drawing.Unit.Cm(34.656D));
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.327D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox112.Style.Color = System.Drawing.Color.Black;
            this.textBox112.Style.Font.Bold = true;
            this.textBox112.Style.Font.Name = "Verdana";
            this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox112.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox112.Value = "ACKNOWLEDGEMENT AND CONSENTS";
            // 
            // textBox111
            // 
            this.textBox111.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.967D), Telerik.Reporting.Drawing.Unit.Cm(35.567D));
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.textBox111.Style.Font.Bold = false;
            this.textBox111.Style.Font.Name = "Verdana";
            this.textBox111.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox111.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox111.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox111.Value = resources.GetString("textBox111.Value");
            // 
            // textBox110
            // 
            this.textBox110.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(38.2D));
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.textBox110.Style.Font.Bold = false;
            this.textBox110.Style.Font.Name = "Verdana";
            this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox110.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox110.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox110.Value = "You acknowledge and agree that under this agreement you assign all STCs associate" +
    "d with the System and its operation to us, as set out in clause 15.";
            // 
            // textBox109
            // 
            this.textBox109.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(40.429D));
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.textBox109.Style.Font.Bold = false;
            this.textBox109.Style.Font.Name = "Verdana";
            this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox109.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox109.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox109.Value = resources.GetString("textBox109.Value");
            // 
            // textBox108
            // 
            this.textBox108.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(42.714D));
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.textBox108.Style.Font.Bold = false;
            this.textBox108.Style.Font.Name = "Verdana";
            this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox108.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox108.Value = "You acknowledge and agree that we may vary the prices under this agreement in cer" +
    "tain circumstances, as set out in clause 15.";
            // 
            // textBox107
            // 
            this.textBox107.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(44.943D));
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.textBox107.Style.Font.Bold = false;
            this.textBox107.Style.Font.Name = "Verdana";
            this.textBox107.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox107.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox107.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox107.Value = resources.GetString("textBox107.Value");
            // 
            // textBox106
            // 
            this.textBox106.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(47.171D));
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.textBox106.Style.Font.Bold = false;
            this.textBox106.Style.Font.Name = "Verdana";
            this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox106.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox106.Value = "You acknowledge and agree that we will provide you with the Maintenance Documents" +
    ", if any, once the system is installed and commissioned.";
            // 
            // textBox104
            // 
            this.textBox104.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.006D), Telerik.Reporting.Drawing.Unit.Cm(56.667D));
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.161D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox104.Style.Font.Bold = false;
            this.textBox104.Style.Font.Name = "Verdana";
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox104.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox104.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox104.Value = "Customer Signature";
            // 
            // textBox103
            // 
            this.textBox103.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.457D), Telerik.Reporting.Drawing.Unit.Cm(56.657D));
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.161D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox103.Style.Font.Bold = false;
            this.textBox103.Style.Font.Name = "Verdana";
            this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox103.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.Value = "Customer Name";
            // 
            // textBox102
            // 
            this.textBox102.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.2D), Telerik.Reporting.Drawing.Unit.Cm(56.657D));
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.667D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox102.Style.Font.Bold = false;
            this.textBox102.Style.Font.Name = "Verdana";
            this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox102.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox102.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox102.Value = "Date";
            // 
            // textBox126
            // 
            this.textBox126.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(58.057D));
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.1D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
            this.textBox126.Style.Color = System.Drawing.Color.White;
            this.textBox126.Style.Font.Name = "Verdana";
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox126.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox126.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox126.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox126.Value = "Provided panel layout for your property is just for a reference only. A final dec" +
    "ision will be made on the day of Installation by CEC Accredited Installer.";
            // 
            // textBox125
            // 
            this.textBox125.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.6D), Telerik.Reporting.Drawing.Unit.Cm(58.4D));
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
            this.textBox125.Style.Color = System.Drawing.Color.White;
            this.textBox125.Style.Font.Name = "Verdana";
            this.textBox125.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox125.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox125.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox125.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox125.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox125.Style.Visible = false;
            this.textBox125.Value = "� Clean Energy Council Limited 2016. For Use By Permitted Licensees Only. Terms O" +
    "f Use Apply ";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(59.4D));
            this.pictureBox3.MimeType = "";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox3.Style.Font.Name = "Verdana";
            this.pictureBox3.Value = "";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(89.1D));
            this.pictureBox4.MimeType = "";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox4.Style.Font.Name = "Verdana";
            this.pictureBox4.Value = "";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(118.8D));
            this.pictureBox5.MimeType = "";
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox5.Style.Font.Name = "Verdana";
            this.pictureBox5.Value = "";
            // 
            // textBox127
            // 
            this.textBox127.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(60D));
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox127.Style.Color = System.Drawing.Color.White;
            this.textBox127.Style.Font.Bold = true;
            this.textBox127.Style.Font.Name = "Verdana";
            this.textBox127.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.Value = "Terms and Conditions";
            // 
            // textBox129
            // 
            this.textBox129.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(60.9D));
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(0.911D));
            this.textBox129.Style.Color = System.Drawing.Color.Black;
            this.textBox129.Style.Font.Bold = true;
            this.textBox129.Style.Font.Name = "Verdana";
            this.textBox129.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox129.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox129.Value = "These terms and conditions continue from the front page and forms part of the sup" +
    "ply and installation contract for your System and invoicing.";
            // 
            // textBox128
            // 
            this.textBox128.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(61.811D));
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox128.Style.Font.Bold = true;
            this.textBox128.Style.Font.Name = "Verdana";
            this.textBox128.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox128.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox128.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox128.Value = "1. \tACCEPTANCE : ";
            // 
            // textBox130
            // 
            this.textBox130.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(62.411D));
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.textBox130.Style.Font.Bold = false;
            this.textBox130.Style.Font.Name = "Verdana";
            this.textBox130.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox130.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox130.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox130.Value = resources.GetString("textBox130.Value");
            // 
            // textBox132
            // 
            this.textBox132.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(63.7D));
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.467D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox132.Style.Font.Bold = true;
            this.textBox132.Style.Font.Name = "Verdana";
            this.textBox132.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox132.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox132.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox132.Value = "2.\tYOU DECLARE THAT :";
            // 
            // textBox131
            // 
            this.textBox131.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(64.3D));
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1.7D));
            this.textBox131.Style.Font.Bold = false;
            this.textBox131.Style.Font.Name = "Verdana";
            this.textBox131.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox131.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox131.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox131.Value = resources.GetString("textBox131.Value");
            // 
            // textBox134
            // 
            this.textBox134.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(66.1D));
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.417D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox134.Style.Font.Bold = true;
            this.textBox134.Style.Font.Name = "Verdana";
            this.textBox134.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox134.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox134.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox134.Value = "3. \tPAYMENT TERMS :";
            // 
            // textBox133
            // 
            this.textBox133.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(66.7D));
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(2.4D));
            this.textBox133.Style.Font.Bold = false;
            this.textBox133.Style.Font.Name = "Verdana";
            this.textBox133.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox133.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox133.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox133.Value = resources.GetString("textBox133.Value");
            // 
            // textBox136
            // 
            this.textBox136.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(69.1D));
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox136.Style.Font.Bold = true;
            this.textBox136.Style.Font.Name = "Verdana";
            this.textBox136.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox136.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox136.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox136.Value = "4.\t RECOVERY AND COSTS :";
            // 
            // textBox135
            // 
            this.textBox135.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(69.7D));
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1.6D));
            this.textBox135.Style.Font.Bold = false;
            this.textBox135.Style.Font.Name = "Verdana";
            this.textBox135.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox135.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox135.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox135.Value = resources.GetString("textBox135.Value");
            // 
            // textBox138
            // 
            this.textBox138.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(71.3D));
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox138.Style.Font.Bold = true;
            this.textBox138.Style.Font.Name = "Verdana";
            this.textBox138.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox138.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox138.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox138.Value = "5.\t REFUNDS :";
            // 
            // textBox137
            // 
            this.textBox137.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(71.9D));
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(3.2D));
            this.textBox137.Style.Font.Bold = false;
            this.textBox137.Style.Font.Name = "Verdana";
            this.textBox137.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox137.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox137.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox137.Value = resources.GetString("textBox137.Value");
            // 
            // textBox140
            // 
            this.textBox140.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.006D), Telerik.Reporting.Drawing.Unit.Cm(75.1D));
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox140.Style.Font.Bold = true;
            this.textBox140.Style.Font.Name = "Verdana";
            this.textBox140.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox140.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox140.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox140.Value = "6.\t GRID CONNECTION :";
            // 
            // textBox139
            // 
            this.textBox139.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(75.7D));
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
            this.textBox139.Style.Font.Bold = false;
            this.textBox139.Style.Font.Name = "Verdana";
            this.textBox139.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox139.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox139.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox139.Value = resources.GetString("textBox139.Value");
            // 
            // textBox142
            // 
            this.textBox142.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.039D), Telerik.Reporting.Drawing.Unit.Cm(77D));
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox142.Style.Font.Bold = true;
            this.textBox142.Style.Font.Name = "Verdana";
            this.textBox142.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox142.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox142.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox142.Value = "7.\t SUPPLY AND INSTALL :";
            // 
            // textBox141
            // 
            this.textBox141.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.033D), Telerik.Reporting.Drawing.Unit.Cm(77.6D));
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
            this.textBox141.Style.Font.Bold = false;
            this.textBox141.Style.Font.Name = "Verdana";
            this.textBox141.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox141.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox141.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox141.Value = resources.GetString("textBox141.Value");
            // 
            // textBox144
            // 
            this.textBox144.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.006D), Telerik.Reporting.Drawing.Unit.Cm(79.1D));
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox144.Style.Font.Bold = true;
            this.textBox144.Style.Font.Name = "Verdana";
            this.textBox144.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox144.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox144.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox144.Value = "8.\t SITE ACCESS : ";
            // 
            // textBox143
            // 
            this.textBox143.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(79.7D));
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1.7D));
            this.textBox143.Style.Font.Bold = false;
            this.textBox143.Style.Font.Name = "Verdana";
            this.textBox143.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox143.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox143.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox143.Value = resources.GetString("textBox143.Value");
            // 
            // textBox146
            // 
            this.textBox146.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.006D), Telerik.Reporting.Drawing.Unit.Cm(81.4D));
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox146.Style.Font.Bold = true;
            this.textBox146.Style.Font.Name = "Verdana";
            this.textBox146.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox146.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox146.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox146.Value = "9.\t BUILDING DEFECTS :";
            // 
            // textBox145
            // 
            this.textBox145.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(82D));
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(2.7D));
            this.textBox145.Style.Font.Bold = false;
            this.textBox145.Style.Font.Name = "Verdana";
            this.textBox145.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox145.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox145.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox145.Value = resources.GetString("textBox145.Value");
            // 
            // textBox148
            // 
            this.textBox148.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.012D), Telerik.Reporting.Drawing.Unit.Cm(84.7D));
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.788D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox148.Style.Font.Bold = true;
            this.textBox148.Style.Font.Name = "Verdana";
            this.textBox148.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox148.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox148.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox148.Value = "10.\t ACCREDITED INSTALLER :";
            // 
            // textBox147
            // 
            this.textBox147.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(85.3D));
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1.735D));
            this.textBox147.Style.Font.Bold = false;
            this.textBox147.Style.Font.Name = "Verdana";
            this.textBox147.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox147.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox147.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox147.Value = resources.GetString("textBox147.Value");
            // 
            // textBox149
            // 
            this.textBox149.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(89.6D));
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox149.Style.Color = System.Drawing.Color.White;
            this.textBox149.Style.Font.Bold = true;
            this.textBox149.Style.Font.Name = "Verdana";
            this.textBox149.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox149.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox149.Value = "Terms and Conditions";
            // 
            // textBox151
            // 
            this.textBox151.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(91D));
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox151.Style.Font.Bold = true;
            this.textBox151.Style.Font.Name = "Verdana";
            this.textBox151.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox151.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox151.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox151.Value = "11.\t SYSTEM GUARANTEES :";
            // 
            // textBox150
            // 
            this.textBox150.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.988D), Telerik.Reporting.Drawing.Unit.Cm(91.6D));
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(11.7D));
            this.textBox150.Style.Font.Bold = false;
            this.textBox150.Style.Font.Name = "Verdana";
            this.textBox150.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox150.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox150.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox150.Value = resources.GetString("textBox150.Value");
            // 
            // textBox165
            // 
            this.textBox165.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(103.3D));
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox165.Style.Font.Bold = true;
            this.textBox165.Style.Font.Name = "Verdana";
            this.textBox165.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox165.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox165.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox165.Value = "12.\t MAKING A COMPLAINT :";
            // 
            // textBox164
            // 
            this.textBox164.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(103.918D));
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1.182D));
            this.textBox164.Style.Font.Bold = false;
            this.textBox164.Style.Font.Name = "Verdana";
            this.textBox164.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox164.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox164.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox164.Value = resources.GetString("textBox164.Value");
            // 
            // textBox167
            // 
            this.textBox167.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(105.1D));
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox167.Style.Font.Bold = true;
            this.textBox167.Style.Font.Name = "Verdana";
            this.textBox167.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox167.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox167.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox167.Value = "13.\t PRIVACY :";
            // 
            // textBox166
            // 
            this.textBox166.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(105.718D));
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox166.Style.Font.Bold = false;
            this.textBox166.Style.Font.Name = "Verdana";
            this.textBox166.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox166.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox166.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox166.Value = "We at Solar Bridge will comply with all relevant privacy legislation in relation " +
    "to your personal information. Copy of our privacy policy is available on www.sol" +
    "arbridge.com.au";
            // 
            // textBox169
            // 
            this.textBox169.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.988D), Telerik.Reporting.Drawing.Unit.Cm(106.718D));
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.512D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox169.Style.Font.Bold = true;
            this.textBox169.Style.Font.Name = "Verdana";
            this.textBox169.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox169.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox169.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox169.Value = "14.\t SMALL SCALE TECHNOLOGY CERTIFICATES (STCs) :";
            // 
            // textBox168
            // 
            this.textBox168.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.988D), Telerik.Reporting.Drawing.Unit.Cm(107.336D));
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.467D), Telerik.Reporting.Drawing.Unit.Cm(3.5D));
            this.textBox168.Style.Font.Bold = false;
            this.textBox168.Style.Font.Name = "Verdana";
            this.textBox168.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox168.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox168.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox168.Value = resources.GetString("textBox168.Value");
            // 
            // textBox170
            // 
            this.textBox170.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.6D), Telerik.Reporting.Drawing.Unit.Cm(119.3D));
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox170.Style.Color = System.Drawing.Color.White;
            this.textBox170.Style.Font.Bold = true;
            this.textBox170.Style.Font.Name = "Verdana";
            this.textBox170.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox170.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox170.Value = "Panel Layout";
            // 
            // textBox174
            // 
            this.textBox174.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.457D), Telerik.Reporting.Drawing.Unit.Cm(55.9D));
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.161D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox174.Style.Font.Bold = false;
            this.textBox174.Style.Font.Name = "Verdana";
            this.textBox174.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox174.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox174.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox174.Value = "";
            // 
            // textBox173
            // 
            this.textBox173.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.2D), Telerik.Reporting.Drawing.Unit.Cm(55.9D));
            this.textBox173.Name = "textBox173";
            this.textBox173.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.667D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox173.Style.Font.Bold = false;
            this.textBox173.Style.Font.Name = "Verdana";
            this.textBox173.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox173.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox173.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox173.Value = "";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.139D), Telerik.Reporting.Drawing.Unit.Cm(53.5D));
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.761D), Telerik.Reporting.Drawing.Unit.Cm(2.889D));
            this.pictureBox6.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox6.Style.Font.Name = "Verdana";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(121.3D));
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8D), Telerik.Reporting.Drawing.Unit.Cm(12D));
            this.pictureBox7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox7.Style.Font.Name = "Verdana";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11D), Telerik.Reporting.Drawing.Unit.Cm(121.3D));
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8D), Telerik.Reporting.Drawing.Unit.Cm(12D));
            this.pictureBox8.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox8.Style.Font.Name = "Verdana";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(134.2D));
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8D), Telerik.Reporting.Drawing.Unit.Cm(12D));
            this.pictureBox9.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox9.Style.Font.Name = "Verdana";
            // 
            // textBox175
            // 
            this.textBox175.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.2D), Telerik.Reporting.Drawing.Unit.Cm(22.6D));
            this.textBox175.Name = "textBox175";
            this.textBox175.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.textBox175.Style.Font.Name = "Verdana";
            this.textBox175.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox175.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox175.Value = "";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(7.6D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.455D), Telerik.Reporting.Drawing.Unit.Cm(4D));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(11.402D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.609D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox171);
            tableGroup1.Name = "tableGroup";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox171});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.table1.Name = "table1";
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup2);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.402D), Telerik.Reporting.Drawing.Unit.Cm(0.609D));
            // 
            // textBox171
            // 
            this.textBox171.Culture = new System.Globalization.CultureInfo("en-US");
            this.textBox171.Format = "{0:C2}";
            this.textBox171.Name = "textBox171";
            this.textBox171.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.402D), Telerik.Reporting.Drawing.Unit.Cm(0.609D));
            this.textBox171.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
            this.textBox171.Style.Font.Name = "Verdana";
            this.textBox171.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox171.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox171.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox171.StyleName = "";
            this.textBox171.Value = "=Fields.Name";
            // 
            // NewQuote
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1});
            this.Name = "Quote";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(21D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private Telerik.Reporting.DetailSection detailSection1;
		private Telerik.Reporting.PictureBox pictureBox1;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.TextBox textBox8;
		private Telerik.Reporting.TextBox textBox9;
		private Telerik.Reporting.TextBox textBox10;
		private Telerik.Reporting.TextBox textBox11;
		private Telerik.Reporting.TextBox textBox12;
		private Telerik.Reporting.TextBox textBox13;
		private Telerik.Reporting.TextBox textBox14;
		private Telerik.Reporting.TextBox textBox15;
		private Telerik.Reporting.TextBox textBox16;
		private Telerik.Reporting.TextBox textBox19;
		private Telerik.Reporting.TextBox textBox20;
		private Telerik.Reporting.TextBox textBox21;
		private Telerik.Reporting.TextBox textBox22;
		private Telerik.Reporting.TextBox textBox23;
		private Telerik.Reporting.TextBox textBox24;
		private Telerik.Reporting.TextBox textBox25;
		private Telerik.Reporting.TextBox textBox26;
		private Telerik.Reporting.TextBox textBox27;
		private Telerik.Reporting.TextBox textBox29;
		private Telerik.Reporting.TextBox textBox28;
		private Telerik.Reporting.TextBox textBox30;
		private Telerik.Reporting.TextBox textBox31;
		private Telerik.Reporting.TextBox textBox32;
		private Telerik.Reporting.TextBox textBox33;
		private Telerik.Reporting.TextBox textBox34;
		private Telerik.Reporting.TextBox textBox35;
		private Telerik.Reporting.TextBox textBox36;
		private Telerik.Reporting.TextBox textBox37;
		private Telerik.Reporting.TextBox textBox38;
		private Telerik.Reporting.TextBox textBox39;
		private Telerik.Reporting.TextBox textBox40;
		private Telerik.Reporting.TextBox textBox41;
		private Telerik.Reporting.TextBox textBox42;
		private Telerik.Reporting.TextBox textBox43;
		private Telerik.Reporting.TextBox textBox44;
		private Telerik.Reporting.TextBox textBox45;
		private Telerik.Reporting.TextBox textBox46;
		private Telerik.Reporting.TextBox textBox47;
		private Telerik.Reporting.TextBox textBox48;
		private Telerik.Reporting.TextBox textBox49;
		private Telerik.Reporting.TextBox textBox50;
		private Telerik.Reporting.TextBox textBox51;
		private Telerik.Reporting.TextBox textBox52;
		private Telerik.Reporting.TextBox textBox53;
		private Telerik.Reporting.TextBox textBox54;
		private Telerik.Reporting.TextBox textBox55;
		private Telerik.Reporting.TextBox textBox56;
		private Telerik.Reporting.TextBox textBox57;
		private Telerik.Reporting.TextBox textBox58;
		private Telerik.Reporting.TextBox textBox59;
		private Telerik.Reporting.TextBox textBox61;
		private Telerik.Reporting.TextBox textBox65;
		private Telerik.Reporting.TextBox textBox64;
		private Telerik.Reporting.TextBox textBox63;
		private Telerik.Reporting.TextBox textBox62;
		private Telerik.Reporting.TextBox textBox67;
		private Telerik.Reporting.TextBox textBox66;
		private Telerik.Reporting.TextBox textBox68;
		private Telerik.Reporting.TextBox textBox69;
		private Telerik.Reporting.TextBox textBox70;
		private Telerik.Reporting.TextBox textBox71;
		private Telerik.Reporting.TextBox textBox72;
		private Telerik.Reporting.TextBox textBox73;
		private Telerik.Reporting.TextBox textBox74;
		private Telerik.Reporting.TextBox textBox75;
		private Telerik.Reporting.TextBox textBox80;
		private Telerik.Reporting.TextBox textBox79;
		private Telerik.Reporting.TextBox textBox78;
		private Telerik.Reporting.TextBox textBox77;
		private Telerik.Reporting.TextBox textBox76;
		private Telerik.Reporting.TextBox textBox85;
		private Telerik.Reporting.TextBox textBox84;
		private Telerik.Reporting.TextBox textBox83;
		private Telerik.Reporting.TextBox textBox82;
		private Telerik.Reporting.TextBox textBox81;
		private Telerik.Reporting.TextBox textBox86;
		private Telerik.Reporting.TextBox textBox87;
		private Telerik.Reporting.TextBox textBox88;
		private Telerik.Reporting.TextBox textBox89;
		private Telerik.Reporting.TextBox textBox90;
		private Telerik.Reporting.TextBox textBox91;
		private Telerik.Reporting.TextBox textBox92;
		private Telerik.Reporting.TextBox textBox93;
		private Telerik.Reporting.TextBox textBox60;
		private Telerik.Reporting.TextBox textBox94;
		private Telerik.Reporting.TextBox textBox95;
		private Telerik.Reporting.TextBox textBox97;
		private Telerik.Reporting.TextBox textBox96;
		private Telerik.Reporting.TextBox textBox98;
		private Telerik.Reporting.TextBox textBox99;
		private Telerik.Reporting.TextBox textBox100;
		private Telerik.Reporting.TextBox textBox101;
		private Telerik.Reporting.PictureBox pictureBox2;
		private Telerik.Reporting.TextBox textBox124;
		private Telerik.Reporting.TextBox textBox123;
		private Telerik.Reporting.TextBox textBox122;
		private Telerik.Reporting.TextBox textBox121;
		private Telerik.Reporting.TextBox textBox120;
		private Telerik.Reporting.TextBox textBox119;
		private Telerik.Reporting.TextBox textBox118;
		private Telerik.Reporting.TextBox textBox117;
		private Telerik.Reporting.TextBox textBox116;
		private Telerik.Reporting.TextBox textBox115;
		private Telerik.Reporting.TextBox textBox114;
		private Telerik.Reporting.TextBox textBox113;
		private Telerik.Reporting.TextBox textBox112;
		private Telerik.Reporting.TextBox textBox111;
		private Telerik.Reporting.TextBox textBox110;
		private Telerik.Reporting.TextBox textBox109;
		private Telerik.Reporting.TextBox textBox108;
		private Telerik.Reporting.TextBox textBox107;
		private Telerik.Reporting.TextBox textBox106;
		private Telerik.Reporting.TextBox textBox104;
		private Telerik.Reporting.TextBox textBox103;
		private Telerik.Reporting.TextBox textBox102;
		private Telerik.Reporting.TextBox textBox126;
		private Telerik.Reporting.TextBox textBox125;
		private Telerik.Reporting.PictureBox pictureBox3;
		private Telerik.Reporting.PictureBox pictureBox4;
		private Telerik.Reporting.PictureBox pictureBox5;
		private Telerik.Reporting.TextBox textBox127;
		private Telerik.Reporting.TextBox textBox129;
		private Telerik.Reporting.TextBox textBox128;
		private Telerik.Reporting.TextBox textBox130;
		private Telerik.Reporting.TextBox textBox132;
		private Telerik.Reporting.TextBox textBox131;
		private Telerik.Reporting.TextBox textBox134;
		private Telerik.Reporting.TextBox textBox133;
		private Telerik.Reporting.TextBox textBox136;
		private Telerik.Reporting.TextBox textBox135;
		private Telerik.Reporting.TextBox textBox138;
		private Telerik.Reporting.TextBox textBox137;
		private Telerik.Reporting.TextBox textBox140;
		private Telerik.Reporting.TextBox textBox139;
		private Telerik.Reporting.TextBox textBox142;
		private Telerik.Reporting.TextBox textBox141;
		private Telerik.Reporting.TextBox textBox144;
		private Telerik.Reporting.TextBox textBox143;
		private Telerik.Reporting.TextBox textBox146;
		private Telerik.Reporting.TextBox textBox145;
		private Telerik.Reporting.TextBox textBox148;
		private Telerik.Reporting.TextBox textBox147;
		private Telerik.Reporting.TextBox textBox149;
		private Telerik.Reporting.TextBox textBox151;
		private Telerik.Reporting.TextBox textBox150;
		private Telerik.Reporting.TextBox textBox165;
		private Telerik.Reporting.TextBox textBox164;
		private Telerik.Reporting.TextBox textBox167;
		private Telerik.Reporting.TextBox textBox166;
		private Telerik.Reporting.TextBox textBox169;
		private Telerik.Reporting.TextBox textBox168;
		private Telerik.Reporting.TextBox textBox170;
		private Telerik.Reporting.TextBox textBox174;
		private Telerik.Reporting.TextBox textBox173;
		private Telerik.Reporting.PictureBox pictureBox6;
		private Telerik.Reporting.PictureBox pictureBox7;
		private Telerik.Reporting.PictureBox pictureBox8;
		private Telerik.Reporting.PictureBox pictureBox9;
		private Telerik.Reporting.TextBox textBox175;
		private Telerik.Reporting.Panel panel1;
		private Telerik.Reporting.Table table1;
		private Telerik.Reporting.TextBox textBox171;
	}
}