
namespace TheSolarProduct.Report
{
	partial class NewPickList
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
			this.detailSection1 = new Telerik.Reporting.DetailSection();
			this.pictureBox1 = new Telerik.Reporting.PictureBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox39 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.panel1 = new Telerik.Reporting.Panel();
			this.table1 = new Telerik.Reporting.Table();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detailSection1
			// 
			this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(29.7D);
			this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox22,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox23,
            this.textBox46,
            this.textBox45,
            this.textBox39,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox12,
            this.textBox13,
            this.textBox3,
            this.textBox2,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox48,
            this.textBox47,
            this.textBox21,
            this.textBox20,
            this.textBox1,
            this.textBox49,
            this.panel1,
            this.textBox18});
			this.detailSection1.Name = "detailSection1";
			this.detailSection1.Style.Font.Name = "Verdana";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.pictureBox1.MimeType = "";
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
			this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox1.Style.Font.Name = "Verdana";
			this.pictureBox1.Value = "";
			// 
			// textBox22
			// 
			this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(2.8D));
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox22.Style.Font.Bold = true;
			this.textBox22.Style.Font.Name = "Verdana";
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.Value = "";
			// 
			// textBox36
			// 
			this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(2D));
			this.textBox36.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
			this.textBox36.Style.Font.Bold = true;
			this.textBox36.Style.Font.Name = "Verdana";
			this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(24D);
			this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox36.Value = "Solar Bridge";
			// 
			// textBox37
			// 
			this.textBox37.KeepTogether = true;
			this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
			this.textBox37.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(85)))), ((int)(((byte)(0)))));
			this.textBox37.Style.Font.Bold = true;
			this.textBox37.Style.Font.Name = "Verdana";
			this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
			this.textBox37.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox37.TextWrap = true;
			this.textBox37.Value = "Stock";
			// 
			// textBox38
			// 
			this.textBox38.KeepTogether = true;
			this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox38.Style.Color = System.Drawing.Color.Black;
			this.textBox38.Style.Font.Bold = true;
			this.textBox38.Style.Font.Name = "Verdana";
			this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(17D);
			this.textBox38.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox38.TextWrap = true;
			this.textBox38.Value = "Allocation";
			// 
			// textBox40
			// 
			this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.4D), Telerik.Reporting.Drawing.Unit.Cm(0.65D));
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox40.Style.Font.Bold = true;
			this.textBox40.Style.Font.Name = "Verdana";
			this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox40.Value = "1300 999 600";
			// 
			// textBox41
			// 
			this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.8D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox41.Style.Font.Bold = true;
			this.textBox41.Style.Font.Name = "Verdana";
			this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox41.Value = "SOLAR BRIDGE PTY LTD";
			// 
			// textBox42
			// 
			this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.8D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox42.Style.Font.Bold = false;
			this.textBox42.Style.Font.Name = "Verdana";
			this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox42.Value = "Suite 221/52 Currumbin Creek Rd";
			// 
			// textBox43
			// 
			this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.8D), Telerik.Reporting.Drawing.Unit.Cm(2.4D));
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox43.Style.Font.Bold = false;
			this.textBox43.Style.Font.Name = "Verdana";
			this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox43.Value = "Currumbin Waters QLD 4223";
			// 
			// textBox44
			// 
			this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.8D), Telerik.Reporting.Drawing.Unit.Cm(2.9D));
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox44.Style.Font.Bold = true;
			this.textBox44.Style.Font.Name = "Verdana";
			this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox44.Value = "ABN 78 644 795 853";
			// 
			// textBox23
			// 
			this.textBox23.KeepTogether = true;
			this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(2.1D));
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox23.Style.Color = System.Drawing.Color.Black;
			this.textBox23.Style.Font.Bold = true;
			this.textBox23.Style.Font.Name = "Verdana";
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(17D);
			this.textBox23.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.TextWrap = true;
			this.textBox23.Value = "Pick List";
			// 
			// textBox46
			// 
			this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.1D), Telerik.Reporting.Drawing.Unit.Cm(3.8D));
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox46.Style.Color = System.Drawing.Color.White;
			this.textBox46.Style.Font.Bold = true;
			this.textBox46.Style.Font.Name = "Verdana";
			this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBox46.Value = "";
			// 
			// textBox45
			// 
			this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(5D));
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox45.Style.Color = System.Drawing.Color.Black;
			this.textBox45.Style.Font.Bold = true;
			this.textBox45.Style.Font.Name = "Verdana";
			this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBox45.Value = "";
			// 
			// textBox39
			// 
			this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(5.65D));
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox39.Style.Color = System.Drawing.Color.Black;
			this.textBox39.Style.Font.Bold = false;
			this.textBox39.Style.Font.Name = "Verdana";
			this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox39.Value = "";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(6.15D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.9D), Telerik.Reporting.Drawing.Unit.Cm(1.05D));
			this.textBox4.Style.Color = System.Drawing.Color.Black;
			this.textBox4.Style.Font.Bold = false;
			this.textBox4.Style.Font.Name = "Verdana";
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox4.Value = "";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15D), Telerik.Reporting.Drawing.Unit.Cm(5.61D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.4D), Telerik.Reporting.Drawing.Unit.Cm(0.89D));
			this.textBox5.Style.Color = System.Drawing.Color.Black;
			this.textBox5.Style.Font.Bold = false;
			this.textBox5.Style.Font.Name = "Verdana";
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox5.Value = "";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15D), Telerik.Reporting.Drawing.Unit.Cm(6.5D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.4D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
			this.textBox6.Style.Color = System.Drawing.Color.Black;
			this.textBox6.Style.Font.Bold = false;
			this.textBox6.Style.Font.Name = "Verdana";
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox6.Value = "";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(7.5D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox7.Style.Color = System.Drawing.Color.Black;
			this.textBox7.Style.Font.Bold = false;
			this.textBox7.Style.Font.Name = "Verdana";
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox7.Value = "";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(8D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox8.Style.Color = System.Drawing.Color.Black;
			this.textBox8.Style.Font.Bold = false;
			this.textBox8.Style.Font.Name = "Verdana";
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox8.Value = "";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(8.5D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox9.Style.Color = System.Drawing.Color.Black;
			this.textBox9.Style.Font.Bold = false;
			this.textBox9.Style.Font.Name = "Verdana";
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox9.Value = "";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(9D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox10.Style.Color = System.Drawing.Color.Black;
			this.textBox10.Style.Font.Bold = false;
			this.textBox10.Style.Font.Name = "Verdana";
			this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox10.Value = "";
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3D), Telerik.Reporting.Drawing.Unit.Cm(9.5D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox12.Style.Color = System.Drawing.Color.Black;
			this.textBox12.Style.Font.Bold = false;
			this.textBox12.Style.Font.Name = "Verdana";
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox12.Value = "";
			// 
			// textBox13
			// 
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(5.6D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox13.Style.Color = System.Drawing.Color.Black;
			this.textBox13.Style.Font.Bold = false;
			this.textBox13.Style.Font.Name = "Verdana";
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox13.Value = "";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(3.8D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox3.Style.Color = System.Drawing.Color.White;
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Name = "Verdana";
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox3.Value = "Installer Name";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(5D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox2.Style.Color = System.Drawing.Color.Black;
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Name = "Verdana";
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBox2.Value = "Installation Date";
			// 
			// textBox24
			// 
			this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15D), Telerik.Reporting.Drawing.Unit.Cm(5D));
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox24.Style.Color = System.Drawing.Color.Black;
			this.textBox24.Style.Font.Bold = true;
			this.textBox24.Style.Font.Name = "Verdana";
			this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBox24.Value = "Site Address";
			// 
			// textBox25
			// 
			this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(7.5D));
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox25.Style.Color = System.Drawing.Color.Black;
			this.textBox25.Style.Font.Bold = false;
			this.textBox25.Style.Font.Name = "Verdana";
			this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox25.Value = "House Type";
			// 
			// textBox26
			// 
			this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(8D));
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox26.Style.Color = System.Drawing.Color.Black;
			this.textBox26.Style.Font.Bold = false;
			this.textBox26.Style.Font.Name = "Verdana";
			this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox26.Value = "Roof Type";
			// 
			// textBox27
			// 
			this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(8.5D));
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox27.Style.Color = System.Drawing.Color.Black;
			this.textBox27.Style.Font.Bold = false;
			this.textBox27.Style.Font.Name = "Verdana";
			this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox27.Value = "Roof Angle";
			// 
			// textBox28
			// 
			this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(9D));
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox28.Style.Color = System.Drawing.Color.Black;
			this.textBox28.Style.Font.Bold = false;
			this.textBox28.Style.Font.Name = "Verdana";
			this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox28.Value = "Manual Quote";
			// 
			// textBox30
			// 
			this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(9.5D));
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox30.Style.Color = System.Drawing.Color.Black;
			this.textBox30.Style.Font.Bold = false;
			this.textBox30.Style.Font.Name = "Verdana";
			this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox30.Value = "Store Location";
			// 
			// textBox31
			// 
			this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(11.9D));
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox31.Style.Color = System.Drawing.Color.Black;
			this.textBox31.Style.Font.Bold = true;
			this.textBox31.Style.Font.Name = "Verdana";
			this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox31.Value = "QTY";
			// 
			// textBox32
			// 
			this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(11.9D));
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox32.Style.Color = System.Drawing.Color.Black;
			this.textBox32.Style.Font.Bold = true;
			this.textBox32.Style.Font.Name = "Verdana";
			this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox32.Value = "ITEMS";
			// 
			// textBox33
			// 
			this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13D), Telerik.Reporting.Drawing.Unit.Cm(11.9D));
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox33.Style.Color = System.Drawing.Color.Black;
			this.textBox33.Style.Font.Bold = true;
			this.textBox33.Style.Font.Name = "Verdana";
			this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox33.Value = "MODEL";
			// 
			// textBox34
			// 
			this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.8D), Telerik.Reporting.Drawing.Unit.Cm(11.9D));
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox34.Style.Color = System.Drawing.Color.Black;
			this.textBox34.Style.Font.Bold = true;
			this.textBox34.Style.Font.Name = "Verdana";
			this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox34.Value = "DONE";
			// 
			// textBox35
			// 
			this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(3.8D));
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox35.Style.Color = System.Drawing.Color.White;
			this.textBox35.Style.Font.Bold = true;
			this.textBox35.Style.Font.Name = "Verdana";
			this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBox35.Value = "Customer Information";
			// 
			// textBox48
			// 
			this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(24D));
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox48.Style.Color = System.Drawing.Color.Black;
			this.textBox48.Style.Font.Bold = false;
			this.textBox48.Style.Font.Name = "Verdana";
			this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox48.Value = "";
			// 
			// textBox47
			// 
			this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(24.6D));
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox47.Style.Color = System.Drawing.Color.Black;
			this.textBox47.Style.Font.Bold = false;
			this.textBox47.Style.Font.Name = "Verdana";
			this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox47.Value = "";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(24D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox21.Style.Color = System.Drawing.Color.Black;
			this.textBox21.Style.Font.Bold = false;
			this.textBox21.Style.Font.Name = "Verdana";
			this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox21.Value = "Pickup By";
			// 
			// textBox20
			// 
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(24.6D));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox20.Style.Color = System.Drawing.Color.Black;
			this.textBox20.Style.Font.Bold = false;
			this.textBox20.Style.Font.Name = "Verdana";
			this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox20.Value = "Date";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(25.2D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox1.Style.Color = System.Drawing.Color.Black;
			this.textBox1.Style.Font.Bold = false;
			this.textBox1.Style.Font.Name = "Verdana";
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox1.Value = "Signature";
			// 
			// textBox49
			// 
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.5D), Telerik.Reporting.Drawing.Unit.Cm(23.8D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox49.Style.Color = System.Drawing.Color.Black;
			this.textBox49.Style.Font.Bold = true;
			this.textBox49.Style.Font.Name = "Verdana";
			this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox49.Value = "Notes";
			// 
			// panel1
			// 
			this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1});
			this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.483D), Telerik.Reporting.Drawing.Unit.Cm(12.733D));
			this.panel1.Name = "panel1";
			this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.017D), Telerik.Reporting.Drawing.Unit.Cm(7.067D));
			this.panel1.Style.Font.Name = "Verdana";
			// 
			// table1
			// 
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.729D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(10.627D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.865D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.753D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.01D)));
			this.table1.Body.SetCellContent(0, 3, this.textBox14);
			this.table1.Body.SetCellContent(0, 0, this.textBox16);
			this.table1.Body.SetCellContent(0, 1, this.textBox15);
			this.table1.Body.SetCellContent(0, 2, this.textBox17);
			tableGroup1.Name = "tableGroup";
			tableGroup2.Name = "tableGroup1";
			tableGroup3.Name = "tableGroup2";
			tableGroup4.Name = "group";
			this.table1.ColumnGroups.Add(tableGroup1);
			this.table1.ColumnGroups.Add(tableGroup2);
			this.table1.ColumnGroups.Add(tableGroup3);
			this.table1.ColumnGroups.Add(tableGroup4);
			this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16,
            this.textBox15,
            this.textBox17,
            this.textBox14});
			this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.022D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.table1.Name = "table1";
			tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup5.Name = "detailTableGroup";
			this.table1.RowGroups.Add(tableGroup5);
			this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.974D), Telerik.Reporting.Drawing.Unit.Cm(1.01D));
			this.table1.Style.Font.Name = "Verdana";
			// 
			// textBox14
			// 
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.753D), Telerik.Reporting.Drawing.Unit.Cm(1.01D));
			this.textBox14.Style.Font.Name = "Verdana";
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox14.StyleName = "";
			// 
			// textBox16
			// 
			this.textBox16.Format = "";
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.729D), Telerik.Reporting.Drawing.Unit.Cm(1.01D));
			this.textBox16.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.textBox16.Style.Font.Name = "Verdana";
			this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.StyleName = "";
			this.textBox16.Value = "=Fields.Qty";
			// 
			// textBox15
			// 
			this.textBox15.Format = "{0:dd-MM-yyyy}";
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.627D), Telerik.Reporting.Drawing.Unit.Cm(1.01D));
			this.textBox15.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.textBox15.Style.Font.Name = "Verdana";
			this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox15.StyleName = "";
			this.textBox15.Value = "=Fields.Item";
			// 
			// textBox17
			// 
			this.textBox17.Format = "{0:dd-MM-yyyy}";
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.865D), Telerik.Reporting.Drawing.Unit.Cm(1.01D));
			this.textBox17.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
			this.textBox17.Style.Font.Name = "Verdana";
			this.textBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox17.StyleName = "";
			this.textBox17.Value = "=Fields.Model";
			// 
			// textBox18
			// 
			this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.5D), Telerik.Reporting.Drawing.Unit.Cm(24.6D));
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(3.1D));
			this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox18.Value = "";
			// 
			// NewPickList
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1});
			this.Name = "Picklist";
			this.PageSettings.ContinuousPaper = false;
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2});
			this.Width = Telerik.Reporting.Drawing.Unit.Cm(21D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private Telerik.Reporting.DetailSection detailSection1;
		private Telerik.Reporting.PictureBox pictureBox1;
		private Telerik.Reporting.TextBox textBox22;
		private Telerik.Reporting.TextBox textBox36;
		private Telerik.Reporting.TextBox textBox37;
		private Telerik.Reporting.TextBox textBox38;
		private Telerik.Reporting.TextBox textBox40;
		private Telerik.Reporting.TextBox textBox41;
		private Telerik.Reporting.TextBox textBox42;
		private Telerik.Reporting.TextBox textBox43;
		private Telerik.Reporting.TextBox textBox44;
		private Telerik.Reporting.TextBox textBox23;
		private Telerik.Reporting.TextBox textBox46;
		private Telerik.Reporting.TextBox textBox45;
		private Telerik.Reporting.TextBox textBox39;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.TextBox textBox8;
		private Telerik.Reporting.TextBox textBox9;
		private Telerik.Reporting.TextBox textBox10;
		private Telerik.Reporting.TextBox textBox12;
		private Telerik.Reporting.TextBox textBox13;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox24;
		private Telerik.Reporting.TextBox textBox25;
		private Telerik.Reporting.TextBox textBox26;
		private Telerik.Reporting.TextBox textBox27;
		private Telerik.Reporting.TextBox textBox28;
		private Telerik.Reporting.TextBox textBox30;
		private Telerik.Reporting.TextBox textBox31;
		private Telerik.Reporting.TextBox textBox32;
		private Telerik.Reporting.TextBox textBox33;
		private Telerik.Reporting.TextBox textBox34;
		private Telerik.Reporting.TextBox textBox35;
		private Telerik.Reporting.TextBox textBox48;
		private Telerik.Reporting.TextBox textBox47;
		private Telerik.Reporting.TextBox textBox21;
		private Telerik.Reporting.TextBox textBox20;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox textBox49;
		private Telerik.Reporting.Panel panel1;
		private Telerik.Reporting.Table table1;
		private Telerik.Reporting.TextBox textBox14;
		private Telerik.Reporting.TextBox textBox16;
		private Telerik.Reporting.TextBox textBox15;
		private Telerik.Reporting.TextBox textBox17;
		private Telerik.Reporting.TextBox textBox18;
	}
}