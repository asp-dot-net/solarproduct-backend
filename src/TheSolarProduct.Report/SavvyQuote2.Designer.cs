
namespace TheSolarProduct.Report
{
    partial class SavvyQuote2
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SavvyQuote2));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            this.detailSection1 = new Telerik.Reporting.DetailSection();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.pictureBox10 = new Telerik.Reporting.PictureBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.pictureBox8 = new Telerik.Reporting.PictureBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.pictureBox9 = new Telerik.Reporting.PictureBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox4 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox5 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox6 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox7 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox8 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox9 = new Telerik.Reporting.HtmlTextBox();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox138 = new Telerik.Reporting.TextBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.htmlTextBox10 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox11 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox12 = new Telerik.Reporting.HtmlTextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.htmlTextBox13 = new Telerik.Reporting.HtmlTextBox();
            this.textBox123 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.htmlTextBox15 = new Telerik.Reporting.HtmlTextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.htmlTextBox22 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox21 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox20 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox19 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox18 = new Telerik.Reporting.HtmlTextBox();
            this.textBox175 = new Telerik.Reporting.TextBox();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.htmlTextBox17 = new Telerik.Reporting.HtmlTextBox();
            this.textBox173 = new Telerik.Reporting.TextBox();
            this.textBox172 = new Telerik.Reporting.TextBox();
            this.textBox171 = new Telerik.Reporting.TextBox();
            this.textBox170 = new Telerik.Reporting.TextBox();
            this.textBox169 = new Telerik.Reporting.TextBox();
            this.textBox168 = new Telerik.Reporting.TextBox();
            this.textBox167 = new Telerik.Reporting.TextBox();
            this.textBox166 = new Telerik.Reporting.TextBox();
            this.shape2 = new Telerik.Reporting.Shape();
            this.textBox165 = new Telerik.Reporting.TextBox();
            this.textBox164 = new Telerik.Reporting.TextBox();
            this.htmlTextBox16 = new Telerik.Reporting.HtmlTextBox();
            this.textBox163 = new Telerik.Reporting.TextBox();
            this.textBox162 = new Telerik.Reporting.TextBox();
            this.textBox161 = new Telerik.Reporting.TextBox();
            this.textBox160 = new Telerik.Reporting.TextBox();
            this.textBox159 = new Telerik.Reporting.TextBox();
            this.textBox158 = new Telerik.Reporting.TextBox();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox155 = new Telerik.Reporting.TextBox();
            this.textBox154 = new Telerik.Reporting.TextBox();
            this.textBox153 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.htmlTextBox14 = new Telerik.Reporting.HtmlTextBox();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox147 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.htmlTextBox33 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox32 = new Telerik.Reporting.HtmlTextBox();
            this.textBox198 = new Telerik.Reporting.TextBox();
            this.textBox197 = new Telerik.Reporting.TextBox();
            this.textBox196 = new Telerik.Reporting.TextBox();
            this.textBox195 = new Telerik.Reporting.TextBox();
            this.textBox194 = new Telerik.Reporting.TextBox();
            this.textBox193 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.shape3 = new Telerik.Reporting.Shape();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox188 = new Telerik.Reporting.TextBox();
            this.textBox187 = new Telerik.Reporting.TextBox();
            this.htmlTextBox31 = new Telerik.Reporting.HtmlTextBox();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox185 = new Telerik.Reporting.TextBox();
            this.textBox184 = new Telerik.Reporting.TextBox();
            this.htmlTextBox30 = new Telerik.Reporting.HtmlTextBox();
            this.textBox183 = new Telerik.Reporting.TextBox();
            this.textBox182 = new Telerik.Reporting.TextBox();
            this.textBox181 = new Telerik.Reporting.TextBox();
            this.htmlTextBox29 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox28 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox27 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox26 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox25 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox24 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox23 = new Telerik.Reporting.HtmlTextBox();
            this.textBox180 = new Telerik.Reporting.TextBox();
            this.textBox179 = new Telerik.Reporting.TextBox();
            this.textBox178 = new Telerik.Reporting.TextBox();
            this.htmlTextBox38 = new Telerik.Reporting.HtmlTextBox();
            this.textBox218 = new Telerik.Reporting.TextBox();
            this.shape4 = new Telerik.Reporting.Shape();
            this.textBox217 = new Telerik.Reporting.TextBox();
            this.textBox216 = new Telerik.Reporting.TextBox();
            this.textBox215 = new Telerik.Reporting.TextBox();
            this.htmlTextBox37 = new Telerik.Reporting.HtmlTextBox();
            this.textBox214 = new Telerik.Reporting.TextBox();
            this.htmlTextBox36 = new Telerik.Reporting.HtmlTextBox();
            this.textBox213 = new Telerik.Reporting.TextBox();
            this.htmlTextBox35 = new Telerik.Reporting.HtmlTextBox();
            this.textBox212 = new Telerik.Reporting.TextBox();
            this.htmlTextBox34 = new Telerik.Reporting.HtmlTextBox();
            this.textBox211 = new Telerik.Reporting.TextBox();
            this.textBox210 = new Telerik.Reporting.TextBox();
            this.textBox209 = new Telerik.Reporting.TextBox();
            this.textBox208 = new Telerik.Reporting.TextBox();
            this.textBox207 = new Telerik.Reporting.TextBox();
            this.textBox206 = new Telerik.Reporting.TextBox();
            this.textBox205 = new Telerik.Reporting.TextBox();
            this.textBox204 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox202 = new Telerik.Reporting.TextBox();
            this.textBox201 = new Telerik.Reporting.TextBox();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.textBox176 = new Telerik.Reporting.TextBox();
            this.textBox177 = new Telerik.Reporting.TextBox();
            this.textBox199 = new Telerik.Reporting.TextBox();
            this.textBox200 = new Telerik.Reporting.TextBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.pictureBox11 = new Telerik.Reporting.PictureBox();
            this.pictureBox12 = new Telerik.Reporting.PictureBox();
            this.pictureBox13 = new Telerik.Reporting.PictureBox();
            this.textBox219 = new Telerik.Reporting.TextBox();
            this.textBox220 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detailSection1
            // 
            this.detailSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(207.9D);
            this.detailSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox4,
            this.pictureBox3,
            this.pictureBox10,
            this.pictureBox1,
            this.pictureBox2,
            this.pictureBox7,
            this.pictureBox8,
            this.textBox12,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox86,
            this.textBox87,
            this.pictureBox9,
            this.textBox104,
            this.textBox103,
            this.textBox102,
            this.textBox101,
            this.textBox100,
            this.textBox99,
            this.textBox98,
            this.textBox97,
            this.textBox96,
            this.textBox95,
            this.textBox94,
            this.textBox93,
            this.textBox92,
            this.textBox91,
            this.textBox90,
            this.textBox89,
            this.textBox88,
            this.textBox19,
            this.textBox23,
            this.textBox22,
            this.textBox21,
            this.textBox20,
            this.textBox24,
            this.textBox25,
            this.textBox34,
            this.textBox33,
            this.textBox32,
            this.textBox31,
            this.textBox30,
            this.textBox29,
            this.textBox28,
            this.textBox27,
            this.textBox26,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox18,
            this.textBox41,
            this.textBox42,
            this.textBox46,
            this.textBox45,
            this.textBox44,
            this.textBox43,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox66,
            this.textBox65,
            this.textBox75,
            this.textBox74,
            this.textBox73,
            this.textBox72,
            this.textBox71,
            this.textBox70,
            this.textBox69,
            this.textBox68,
            this.textBox67,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.textBox81,
            this.textBox84,
            this.textBox83,
            this.textBox82,
            this.textBox85,
            this.textBox17,
            this.textBox16,
            this.textBox141,
            this.htmlTextBox1,
            this.htmlTextBox2,
            this.htmlTextBox3,
            this.htmlTextBox4,
            this.htmlTextBox5,
            this.htmlTextBox6,
            this.htmlTextBox7,
            this.htmlTextBox8,
            this.htmlTextBox9,
            this.textBox139,
            this.textBox138,
            this.textBox137,
            this.textBox136,
            this.textBox135,
            this.textBox134,
            this.textBox133,
            this.textBox132,
            this.textBox131,
            this.textBox130,
            this.textBox129,
            this.textBox128,
            this.textBox127,
            this.textBox126,
            this.htmlTextBox10,
            this.htmlTextBox11,
            this.htmlTextBox12,
            this.textBox125,
            this.textBox124,
            this.htmlTextBox13,
            this.textBox123,
            this.textBox122,
            this.textBox121,
            this.textBox120,
            this.textBox119,
            this.textBox118,
            this.textBox117,
            this.textBox116,
            this.textBox115,
            this.textBox114,
            this.textBox113,
            this.textBox112,
            this.textBox111,
            this.htmlTextBox15,
            this.textBox110,
            this.textBox109,
            this.shape1,
            this.htmlTextBox22,
            this.htmlTextBox21,
            this.htmlTextBox20,
            this.htmlTextBox19,
            this.htmlTextBox18,
            this.textBox175,
            this.textBox174,
            this.htmlTextBox17,
            this.textBox173,
            this.textBox172,
            this.textBox171,
            this.textBox170,
            this.textBox169,
            this.textBox168,
            this.textBox167,
            this.textBox166,
            this.shape2,
            this.textBox165,
            this.textBox164,
            this.htmlTextBox16,
            this.textBox163,
            this.textBox162,
            this.textBox161,
            this.textBox160,
            this.textBox159,
            this.textBox158,
            this.textBox157,
            this.textBox156,
            this.textBox155,
            this.textBox154,
            this.textBox153,
            this.textBox152,
            this.textBox151,
            this.textBox150,
            this.textBox149,
            this.htmlTextBox14,
            this.textBox148,
            this.textBox147,
            this.textBox146,
            this.textBox145,
            this.textBox144,
            this.textBox143,
            this.htmlTextBox33,
            this.htmlTextBox32,
            this.textBox198,
            this.textBox197,
            this.textBox196,
            this.textBox195,
            this.textBox194,
            this.textBox193,
            this.textBox192,
            this.shape3,
            this.textBox191,
            this.textBox190,
            this.textBox189,
            this.textBox188,
            this.textBox187,
            this.htmlTextBox31,
            this.textBox186,
            this.textBox185,
            this.textBox184,
            this.htmlTextBox30,
            this.textBox183,
            this.textBox182,
            this.textBox181,
            this.htmlTextBox29,
            this.htmlTextBox28,
            this.htmlTextBox27,
            this.htmlTextBox26,
            this.htmlTextBox25,
            this.htmlTextBox24,
            this.htmlTextBox23,
            this.textBox180,
            this.textBox179,
            this.textBox178,
            this.htmlTextBox38,
            this.textBox218,
            this.shape4,
            this.textBox217,
            this.textBox216,
            this.textBox215,
            this.htmlTextBox37,
            this.textBox214,
            this.htmlTextBox36,
            this.textBox213,
            this.htmlTextBox35,
            this.textBox212,
            this.htmlTextBox34,
            this.textBox211,
            this.textBox210,
            this.textBox209,
            this.textBox208,
            this.textBox207,
            this.textBox206,
            this.textBox205,
            this.textBox204,
            this.textBox203,
            this.textBox202,
            this.textBox201,
            this.pictureBox5,
            this.textBox108,
            this.textBox107,
            this.textBox106,
            this.textBox105,
            this.textBox140,
            this.textBox142,
            this.textBox176,
            this.textBox177,
            this.textBox199,
            this.textBox200,
            this.pictureBox6,
            this.pictureBox11,
            this.pictureBox12,
            this.pictureBox13,
            this.textBox219,
            this.textBox220});
            this.detailSection1.Name = "detailSection1";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(148.446D));
            this.pictureBox4.MimeType = "";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox4.Value = "";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(118.746D));
            this.pictureBox3.MimeType = "";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox3.Value = "";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(89.046D));
            this.pictureBox10.MimeType = "";
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox10.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox10.Value = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(29.646D));
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(178.146D));
            this.pictureBox7.MimeType = "";
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox7.Value = "";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(29.646D));
            this.pictureBox8.MimeType = "";
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox8.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox8.Value = "";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(30D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.4D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox12.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "Your Solar Proposal";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(31.1D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.061D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox4.Style.Color = System.Drawing.Color.Black;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "ACKNOWLEDGEMENT AND CONSENTS";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(32.2D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(2.1D));
            this.textBox5.Style.Font.Bold = false;
            this.textBox5.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = resources.GetString("textBox5.Value");
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(34.3D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(1.8D));
            this.textBox6.Style.Font.Bold = false;
            this.textBox6.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "You acknowledge and agree that under this agreement you assign all STCs associate" +
    "d with the System and its operation to us, as set out in clause 10.";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(36.1D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(2.3D));
            this.textBox7.Style.Font.Bold = false;
            this.textBox7.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = resources.GetString("textBox7.Value");
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(38.4D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(1.7D));
            this.textBox8.Style.Font.Bold = false;
            this.textBox8.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "You acknowledge and agree that we may vary the prices under this agreement in cer" +
    "tain circumstances, as set out in clause 5.";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(40.1D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(2.5D));
            this.textBox9.Style.Font.Bold = false;
            this.textBox9.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = resources.GetString("textBox9.Value");
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(42.5D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
            this.textBox10.Style.Font.Bold = false;
            this.textBox10.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "You acknowledge and agree that we will provide you with the Maintenance Documents" +
    " listed in Attachment 2 once the System is installed and commissioned";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(44D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(2.1D));
            this.textBox11.Style.Font.Bold = false;
            this.textBox11.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "You acknowledge and agree that we may vary the prices under this agreement in cer" +
    "tain";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(46.1D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(2.7D));
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = resources.GetString("textBox1.Value");
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(49.6D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.8D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox2.Style.Font.Bold = false;
            this.textBox2.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "I accept the above quotation, and have read the terms & conditions, and wish to p" +
    "rocedd with the installation";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(50.8D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "Customer Name";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(51.939D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = "Date";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(53.1D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "Signature";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.7D), Telerik.Reporting.Drawing.Unit.Cm(56.3D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.7D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox15.Style.Font.Bold = false;
            this.textBox15.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "* By signing this or making deposit, you are entering into a binding Contract";
            // 
            // textBox86
            // 
            this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(58D));
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.8D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox86.Style.Color = System.Drawing.Color.Black;
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.Value = "Quote Valid for 7 Days only";
            // 
            // textBox87
            // 
            this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(58.5D));
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.4D), Telerik.Reporting.Drawing.Unit.Cm(0.846D));
            this.textBox87.Style.Color = System.Drawing.Color.White;
            this.textBox87.Style.Font.Bold = true;
            this.textBox87.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox87.Value = "(c) Clean Energy Council Limited 2016. For use by permitted only. Terms of use ap" +
    "ply";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox9.MimeType = "image/jpeg";
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox9.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.pictureBox9.Value = ((object)(resources.GetObject("pictureBox9.Value")));
            // 
            // textBox104
            // 
            this.textBox104.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.6D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.textBox104.Style.Color = System.Drawing.Color.White;
            this.textBox104.Style.Font.Bold = true;
            this.textBox104.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox104.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox104.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox104.Value = "SOLAR ENERGY";
            // 
            // textBox103
            // 
            this.textBox103.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.6D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.textBox103.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox103.Style.Font.Bold = true;
            this.textBox103.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(28D);
            this.textBox103.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.Value = "QUOTATION";
            // 
            // textBox102
            // 
            this.textBox102.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.4D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.textBox102.Style.Font.Bold = true;
            this.textBox102.Style.Font.Name = "Verdana";
            this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox102.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox102.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox102.Value = "";
            // 
            // textBox101
            // 
            this.textBox101.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.4D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox101.Style.Color = System.Drawing.Color.White;
            this.textBox101.Style.Font.Bold = true;
            this.textBox101.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox101.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox101.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox101.Value = "QUOTE NO";
            // 
            // textBox100
            // 
            this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.9D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox100.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox100.Style.Font.Bold = true;
            this.textBox100.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.Value = "SAVVY SOLAR PTY LTD";
            // 
            // textBox99
            // 
            this.textBox99.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.9D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox99.Style.Color = System.Drawing.Color.Black;
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox99.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox99.Value = "ABN : 65 617 964 539";
            // 
            // textBox98
            // 
            this.textBox98.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.8D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox98.Style.Color = System.Drawing.Color.Black;
            this.textBox98.Style.Font.Bold = false;
            this.textBox98.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox98.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox98.Value = "C5 Level 1, 2 Main Street";
            // 
            // textBox97
            // 
            this.textBox97.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.8D), Telerik.Reporting.Drawing.Unit.Cm(1.6D));
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox97.Style.Color = System.Drawing.Color.Black;
            this.textBox97.Style.Font.Bold = false;
            this.textBox97.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox97.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox97.Value = "PointCook, VIC 3030";
            // 
            // textBox96
            // 
            this.textBox96.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.9D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox96.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox96.Style.Font.Bold = true;
            this.textBox96.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox96.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox96.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox96.Value = "1800 728 897";
            // 
            // textBox95
            // 
            this.textBox95.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.9D), Telerik.Reporting.Drawing.Unit.Cm(2.3D));
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox95.Style.Color = System.Drawing.Color.Black;
            this.textBox95.Style.Font.Bold = false;
            this.textBox95.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox95.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox95.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox95.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox95.Value = "www.savvysolar.com.au";
            // 
            // textBox94
            // 
            this.textBox94.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.839D), Telerik.Reporting.Drawing.Unit.Cm(3.6D));
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.6D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox94.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox94.Style.Font.Bold = true;
            this.textBox94.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox94.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox94.Value = "CUSTOMER INFORMATION";
            // 
            // textBox93
            // 
            this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.759D), Telerik.Reporting.Drawing.Unit.Cm(3.6D));
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.7D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox93.Style.Color = System.Drawing.Color.White;
            this.textBox93.Style.Font.Bold = true;
            this.textBox93.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.Value = "Installation Date : 4 weeks from Solar VIC rebate Approval Date";
            // 
            // textBox92
            // 
            this.textBox92.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.989D), Telerik.Reporting.Drawing.Unit.Cm(4.9D));
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox92.Style.Font.Bold = true;
            this.textBox92.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox92.Value = "Name";
            // 
            // textBox91
            // 
            this.textBox91.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.989D), Telerik.Reporting.Drawing.Unit.Cm(5.4D));
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox91.Style.Font.Bold = true;
            this.textBox91.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox91.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.Value = "Phone";
            // 
            // textBox90
            // 
            this.textBox90.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.989D), Telerik.Reporting.Drawing.Unit.Cm(5.939D));
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox90.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.Value = "Email";
            // 
            // textBox89
            // 
            this.textBox89.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.989D), Telerik.Reporting.Drawing.Unit.Cm(6.439D));
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox89.Style.Font.Bold = true;
            this.textBox89.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox89.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.Value = "Site Details";
            // 
            // textBox88
            // 
            this.textBox88.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.989D), Telerik.Reporting.Drawing.Unit.Cm(6.939D));
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.961D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox88.Style.Font.Bold = true;
            this.textBox88.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox88.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox88.Value = "Meter Phase";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(6.939D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.111D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.Value = "Switchboard Upgrade";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.789D), Telerik.Reporting.Drawing.Unit.Cm(4.9D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.372D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Verdana";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.789D), Telerik.Reporting.Drawing.Unit.Cm(5.4D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.372D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Verdana";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Value = "";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.789D), Telerik.Reporting.Drawing.Unit.Cm(5.939D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.372D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Verdana";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = "";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.789D), Telerik.Reporting.Drawing.Unit.Cm(6.439D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.372D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Verdana";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = "";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.789D), Telerik.Reporting.Drawing.Unit.Cm(6.939D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.711D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Verdana";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.Value = "";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.611D), Telerik.Reporting.Drawing.Unit.Cm(6.939D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.55D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Verdana";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "";
            // 
            // textBox34
            // 
            this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.8D), Telerik.Reporting.Drawing.Unit.Cm(4.9D));
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.739D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "Roof Type";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.8D), Telerik.Reporting.Drawing.Unit.Cm(5.4D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.739D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "No of Storey";
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.8D), Telerik.Reporting.Drawing.Unit.Cm(5.939D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.739D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.Value = "Roof Pitch";
            // 
            // textBox31
            // 
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.8D), Telerik.Reporting.Drawing.Unit.Cm(6.439D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.739D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.Value = "Energy Distributor";
            // 
            // textBox30
            // 
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.8D), Telerik.Reporting.Drawing.Unit.Cm(6.939D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.739D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.Value = "Energy Retailer";
            // 
            // textBox29
            // 
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.7D), Telerik.Reporting.Drawing.Unit.Cm(4.9D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.039D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Verdana";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.Value = "";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.7D), Telerik.Reporting.Drawing.Unit.Cm(5.4D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Verdana";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.Value = "";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.7D), Telerik.Reporting.Drawing.Unit.Cm(5.939D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Verdana";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.Value = "";
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.7D), Telerik.Reporting.Drawing.Unit.Cm(6.439D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Verdana";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "";
            // 
            // textBox35
            // 
            this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.7D), Telerik.Reporting.Drawing.Unit.Cm(6.939D));
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.039D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Name = "Verdana";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "";
            // 
            // textBox36
            // 
            this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.892D), Telerik.Reporting.Drawing.Unit.Cm(8.339D));
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.6D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox36.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.Value = "PRODUCT DESCRIPTION";
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.9D), Telerik.Reporting.Drawing.Unit.Cm(8.189D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.839D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox37.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = "Total Cost";
            // 
            // textBox38
            // 
            this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.9D), Telerik.Reporting.Drawing.Unit.Cm(8.589D));
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.839D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox38.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.Value = "Inc. GST";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(9.5D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.6D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox18.Style.Color = System.Drawing.Color.Black;
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Verdana";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.Value = "Solar Photovoltaic System Including";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.692D), Telerik.Reporting.Drawing.Unit.Cm(9.292D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.174D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox41.Style.Color = System.Drawing.Color.Black;
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.Value = "";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.759D), Telerik.Reporting.Drawing.Unit.Cm(13.826D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.574D), Telerik.Reporting.Drawing.Unit.Cm(0.641D));
            this.textBox42.Style.Color = System.Drawing.Color.Black;
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Name = "Verdana";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.Value = "Warranties";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.826D), Telerik.Reporting.Drawing.Unit.Cm(14.526D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox46.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.Value = "25 Years of Performance Warranty";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.826D), Telerik.Reporting.Drawing.Unit.Cm(15.026D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox45.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.Value = "10 Years Panels Manufacturere\'s Warranty";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.359D), Telerik.Reporting.Drawing.Unit.Cm(14.526D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox44.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.Value = "5 Years Standard Inverter Warranty";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.359D), Telerik.Reporting.Drawing.Unit.Cm(15.026D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.212D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox43.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.Value = "10 Years of Wokmanship Warranty";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.759D), Telerik.Reporting.Drawing.Unit.Cm(16.359D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.541D), Telerik.Reporting.Drawing.Unit.Cm(0.641D));
            this.textBox47.Style.Color = System.Drawing.Color.Black;
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.Value = "Special Notes";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.3D), Telerik.Reporting.Drawing.Unit.Cm(16.3D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.574D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox48.Style.Color = System.Drawing.Color.Black;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.Value = "Sub Total (A)";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.5D), Telerik.Reporting.Drawing.Unit.Cm(16.8D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.374D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox49.Style.Color = System.Drawing.Color.Black;
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "Less STC Insentive (B)";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.141D), Telerik.Reporting.Drawing.Unit.Cm(17.3D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.733D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox50.Style.Color = System.Drawing.Color.Black;
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = "Grand Total (A)";
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.5D), Telerik.Reporting.Drawing.Unit.Cm(17.8D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.374D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox51.Style.Color = System.Drawing.Color.Black;
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.Value = "Final Quote Price after STC Insentive";
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(18.3D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.274D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox52.Style.Color = System.Drawing.Color.Black;
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.Value = "Less : Solar VIC Rebate";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(18.8D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.274D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox53.Style.Color = System.Drawing.Color.Black;
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = "Less : Solar VIC Interest Free Loan";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(19.5D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.415D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox54.Style.Color = System.Drawing.Color.Black;
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.Value = "Final Price after STC Insentive, Solar VIC Rebate & Solar VIC Interest Free Loan";
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(20.2D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.415D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox55.Style.Color = System.Drawing.Color.Black;
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = "Deposite [ Inc GST ]";
            // 
            // textBox56
            // 
            this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(21D));
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.415D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox56.Style.Color = System.Drawing.Color.Black;
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.Value = "Balance [ Inc GST ]";
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(21.6D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.5D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox57.Style.Color = System.Drawing.Color.Black;
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.Value = "* Subject to Solar VIC Rebate and Interest Free Loan";
            // 
            // textBox58
            // 
            this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.759D), Telerik.Reporting.Drawing.Unit.Cm(22.1D));
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.6D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox58.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox58.Value = "Payment Methods";
            // 
            // textBox59
            // 
            this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.759D), Telerik.Reporting.Drawing.Unit.Cm(23.3D));
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.841D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox59.Style.Color = System.Drawing.Color.Black;
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.Value = "1. Direst Deposit / EFT";
            // 
            // textBox60
            // 
            this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11D), Telerik.Reporting.Drawing.Unit.Cm(23.3D));
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.841D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox60.Style.Color = System.Drawing.Color.Black;
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox60.Value = "3. Cheque";
            // 
            // textBox61
            // 
            this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(26.1D));
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.841D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox61.Style.Color = System.Drawing.Color.Black;
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.Value = "2.Card Payment";
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(24.2D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox62.Style.Color = System.Drawing.Color.Black;
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.Value = "Account Name : Savvy Solar Pty Ltd";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(24.7D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox63.Style.Color = System.Drawing.Color.Black;
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.Value = "BSB No : 034001";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(25.2D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox64.Style.Color = System.Drawing.Color.Black;
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.Value = "Acc No : 632451";
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.2D), Telerik.Reporting.Drawing.Unit.Cm(24.7D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox66.Style.Color = System.Drawing.Color.Black;
            this.textBox66.Style.Font.Bold = true;
            this.textBox66.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.Value = "Refrence No";
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.2D), Telerik.Reporting.Drawing.Unit.Cm(25.2D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox65.Style.Color = System.Drawing.Color.Black;
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = "Deposit Require";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(16.3D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox75.Style.Color = System.Drawing.Color.Black;
            this.textBox75.Style.Font.Bold = true;
            this.textBox75.Style.Font.Name = "Verdana";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox75.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.Value = "";
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(16.8D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox74.Style.Color = System.Drawing.Color.Black;
            this.textBox74.Style.Font.Bold = true;
            this.textBox74.Style.Font.Name = "Verdana";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.Value = "";
            // 
            // textBox73
            // 
            this.textBox73.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(17.3D));
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox73.Style.Color = System.Drawing.Color.Black;
            this.textBox73.Style.Font.Bold = true;
            this.textBox73.Style.Font.Name = "Verdana";
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.Value = "";
            // 
            // textBox72
            // 
            this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(17.8D));
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox72.Style.Color = System.Drawing.Color.Black;
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.Style.Font.Name = "Verdana";
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.Value = "";
            // 
            // textBox71
            // 
            this.textBox71.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(18.3D));
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox71.Style.Color = System.Drawing.Color.Black;
            this.textBox71.Style.Font.Bold = true;
            this.textBox71.Style.Font.Name = "Verdana";
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.Value = "";
            // 
            // textBox70
            // 
            this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(18.8D));
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox70.Style.Color = System.Drawing.Color.Black;
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Name = "Verdana";
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.Value = "";
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(19.5D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox69.Style.Color = System.Drawing.Color.Black;
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.Font.Name = "Verdana";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.Value = "";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(20.2D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox68.Style.Color = System.Drawing.Color.Black;
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.Font.Name = "Verdana";
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.Value = "";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.1D), Telerik.Reporting.Drawing.Unit.Cm(21D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox67.Style.Color = System.Drawing.Color.Black;
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.Font.Name = "Verdana";
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.Value = "";
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(26.8D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox76.Style.Color = System.Drawing.Color.Black;
            this.textBox76.Style.Font.Bold = true;
            this.textBox76.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox76.Value = "Call Savvy Solar on 1800 728 897";
            // 
            // textBox77
            // 
            this.textBox77.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(27.2D));
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox77.Style.Color = System.Drawing.Color.Black;
            this.textBox77.Style.Font.Bold = true;
            this.textBox77.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox77.Value = "Visa / Master Card Only";
            // 
            // textBox78
            // 
            this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(27.6D));
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox78.Style.Color = System.Drawing.Color.Black;
            this.textBox78.Style.Font.Bold = true;
            this.textBox78.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox78.Value = "1.5% Surchage Applicable";
            // 
            // textBox79
            // 
            this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.5D), Telerik.Reporting.Drawing.Unit.Cm(24.2D));
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox79.Style.Color = System.Drawing.Color.Black;
            this.textBox79.Style.Font.Bold = true;
            this.textBox79.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox79.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.Value = "Payable to  Savvy Solar Pty Ltd";
            // 
            // textBox80
            // 
            this.textBox80.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.5D), Telerik.Reporting.Drawing.Unit.Cm(24.7D));
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox80.Style.Color = System.Drawing.Color.Black;
            this.textBox80.Style.Font.Bold = true;
            this.textBox80.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox80.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox80.Value = "Address : C5 , Level 1, Main Street";
            // 
            // textBox81
            // 
            this.textBox81.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.9D), Telerik.Reporting.Drawing.Unit.Cm(25.2D));
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox81.Style.Color = System.Drawing.Color.Black;
            this.textBox81.Style.Font.Bold = true;
            this.textBox81.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox81.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox81.Value = "Point Cook, VIC 3000";
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.5D), Telerik.Reporting.Drawing.Unit.Cm(26.2D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox84.Style.Color = System.Drawing.Color.Black;
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.Value = "For more info contact us on : info@savvysolar.com.au";
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.5D), Telerik.Reporting.Drawing.Unit.Cm(26.6D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox83.Style.Color = System.Drawing.Color.Black;
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.Value = "For Installation email us on : install@savvysolar.com.au";
            // 
            // textBox82
            // 
            this.textBox82.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.5D), Telerik.Reporting.Drawing.Unit.Cm(27D));
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox82.Style.Color = System.Drawing.Color.Black;
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox82.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.Value = "For Grid Con. email us on : install@savvysolar.com.au";
            // 
            // textBox85
            // 
            this.textBox85.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.5D), Telerik.Reporting.Drawing.Unit.Cm(27.4D));
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.textBox85.Style.Color = System.Drawing.Color.Black;
            this.textBox85.Style.Font.Bold = true;
            this.textBox85.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox85.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.Value = "For Service email us on : install@savvysolar.com.au";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(28.3D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.8D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
            this.textBox17.Style.Color = System.Drawing.Color.Black;
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.Value = "Quote Valid for 7 Days only";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(28.8D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.4D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox16.Style.Color = System.Drawing.Color.White;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.Value = "(c) Clean Energy Council Limited 2016. For use by permitted only. Terms of use ap" +
    "ply";
            // 
            // textBox141
            // 
            this.textBox141.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(90.323D));
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.061D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox141.Style.Color = System.Drawing.Color.Black;
            this.textBox141.Style.Font.Bold = true;
            this.textBox141.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox141.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox141.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox141.Value = "Terms of Use";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(91.088D));
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
            this.htmlTextBox1.Style.Font.Name = "Gotham";
            this.htmlTextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox1.Value = "<strong>1</strong> The Clean Energy Council Limited (CEC) owns all intellectual p" +
    "roperty rights in the Solar PV Sale and Installation Agreement (Agreement).";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(90.7D));
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.htmlTextBox2.Style.Font.Name = "Gotham";
            this.htmlTextBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox2.Value = "1.6 This agreement ends when we have finished installing and commissioning the Sy" +
    "stem, unless we or you end it earlier in accordance with its terms";
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(92.2D));
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.htmlTextBox3.Style.Font.Name = "Gotham";
            this.htmlTextBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox3.Value = "<strong>2</strong> The Licensee must not remove these terms of use or any copyrig" +
    "ht statement from the Agreement";
            // 
            // htmlTextBox4
            // 
            this.htmlTextBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(93D));
            this.htmlTextBox4.Name = "htmlTextBox4";
            this.htmlTextBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.htmlTextBox4.Style.Font.Name = "Gotham";
            this.htmlTextBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox4.Value = "<strong>3</strong> The Agreement must only be used by a party designated by the C" +
    "EC as a \"Licensee\" for the Agreement.";
            // 
            // htmlTextBox5
            // 
            this.htmlTextBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(93.8D));
            this.htmlTextBox5.Name = "htmlTextBox5";
            this.htmlTextBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.174D));
            this.htmlTextBox5.Style.Font.Name = "Gotham";
            this.htmlTextBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox5.Value = resources.GetString("htmlTextBox5.Value");
            // 
            // htmlTextBox6
            // 
            this.htmlTextBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(95.1D));
            this.htmlTextBox6.Name = "htmlTextBox6";
            this.htmlTextBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.076D));
            this.htmlTextBox6.Style.Font.Name = "Gotham";
            this.htmlTextBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox6.Value = "<strong>5</strong> The Licensee may tailor the Agreement for the Permitted Purpos" +
    "e. Use of the Agreement for any other purpose is prohibited";
            // 
            // htmlTextBox7
            // 
            this.htmlTextBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(96.3D));
            this.htmlTextBox7.Name = "htmlTextBox7";
            this.htmlTextBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.743D));
            this.htmlTextBox7.Style.Font.Name = "Gotham";
            this.htmlTextBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox7.Value = resources.GetString("htmlTextBox7.Value");
            // 
            // htmlTextBox8
            // 
            this.htmlTextBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(98.2D));
            this.htmlTextBox8.Name = "htmlTextBox8";
            this.htmlTextBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.076D));
            this.htmlTextBox8.Style.Font.Name = "Gotham";
            this.htmlTextBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox8.Value = resources.GetString("htmlTextBox8.Value");
            // 
            // htmlTextBox9
            // 
            this.htmlTextBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(100.3D));
            this.htmlTextBox9.Name = "htmlTextBox9";
            this.htmlTextBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
            this.htmlTextBox9.Style.Font.Name = "Gotham";
            this.htmlTextBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox9.Value = resources.GetString("htmlTextBox9.Value");
            // 
            // textBox139
            // 
            this.textBox139.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(102.4D));
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox139.Style.Color = System.Drawing.Color.Black;
            this.textBox139.Style.Font.Bold = true;
            this.textBox139.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox139.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox139.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox139.Value = "Solar PV Sale and Installation Agreement";
            // 
            // textBox138
            // 
            this.textBox138.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(103D));
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox138.Style.Color = System.Drawing.Color.Black;
            this.textBox138.Style.Font.Bold = true;
            this.textBox138.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox138.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox138.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox138.Value = "1. Introduction";
            // 
            // textBox137
            // 
            this.textBox137.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(103.539D));
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox137.Style.Color = System.Drawing.Color.Black;
            this.textBox137.Style.Font.Bold = true;
            this.textBox137.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox137.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox137.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox137.Value = "Who does this agreement apply to?";
            // 
            // textBox136
            // 
            this.textBox136.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(103.98D));
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox136.Style.Color = System.Drawing.Color.Black;
            this.textBox136.Style.Font.Bold = false;
            this.textBox136.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox136.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox136.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox136.Value = "1.1    This agreement is between";
            // 
            // textBox135
            // 
            this.textBox135.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(104.439D));
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox135.Style.Color = System.Drawing.Color.Black;
            this.textBox135.Style.Font.Bold = false;
            this.textBox135.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox135.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox135.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox135.Value = "(a)    The Savvy Solar PTY LTD  ABN : 65 617 964 539, referred to as \"we\" or \"us\"" +
    "; and";
            // 
            // textBox134
            // 
            this.textBox134.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(105.339D));
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox134.Style.Color = System.Drawing.Color.Black;
            this.textBox134.Style.Font.Bold = false;
            this.textBox134.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox134.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox134.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox134.Value = "(b)    the customer named in the Quote, referred to as \"you\"";
            // 
            // textBox133
            // 
            this.textBox133.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(105.939D));
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox133.Style.Color = System.Drawing.Color.Black;
            this.textBox133.Style.Font.Bold = true;
            this.textBox133.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox133.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox133.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox133.Value = "What is this agreement made up of?";
            // 
            // textBox132
            // 
            this.textBox132.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(106.439D));
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox132.Style.Color = System.Drawing.Color.Black;
            this.textBox132.Style.Font.Bold = false;
            this.textBox132.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox132.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox132.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox132.Value = "1.2    This agreement is made up of:";
            // 
            // textBox131
            // 
            this.textBox131.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(106.939D));
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox131.Style.Color = System.Drawing.Color.Black;
            this.textBox131.Style.Font.Bold = false;
            this.textBox131.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox131.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox131.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox131.Value = "(a)    these Terms and Conditions; and (b)    the Quote attached to these Terms a" +
    "nd Conditions";
            // 
            // textBox130
            // 
            this.textBox130.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(107.839D));
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox130.Style.Color = System.Drawing.Color.Black;
            this.textBox130.Style.Font.Bold = true;
            this.textBox130.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox130.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox130.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox130.Value = "What does this agreement cover?";
            // 
            // textBox129
            // 
            this.textBox129.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(108.38D));
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox129.Style.Color = System.Drawing.Color.Black;
            this.textBox129.Style.Font.Bold = false;
            this.textBox129.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox129.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox129.Value = "1.3 The agreement covers:";
            // 
            // textBox128
            // 
            this.textBox128.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(108.839D));
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox128.Style.Color = System.Drawing.Color.Black;
            this.textBox128.Style.Font.Bold = false;
            this.textBox128.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox128.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox128.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox128.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox128.Value = "(a)    your purchase from us of the solar photovoltaic system and other equipment" +
    ", referred to as the \"System\" and described in the Full System. Design attached " +
    "to this agreement; and  ";
            // 
            // textBox127
            // 
            this.textBox127.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(109.739D));
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.textBox127.Style.Color = System.Drawing.Color.Black;
            this.textBox127.Style.Font.Bold = false;
            this.textBox127.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox127.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox127.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.Value = "(b)    delivery and installation of the System at your Premises. When does this a" +
    "greement start and end?";
            // 
            // textBox126
            // 
            this.textBox126.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(110.439D));
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(3.6D));
            this.textBox126.Style.Color = System.Drawing.Color.Black;
            this.textBox126.Style.Font.Bold = false;
            this.textBox126.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox126.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox126.Value = resources.GetString("textBox126.Value");
            // 
            // htmlTextBox10
            // 
            this.htmlTextBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(91.9D));
            this.htmlTextBox10.Name = "htmlTextBox10";
            this.htmlTextBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.htmlTextBox10.Style.Font.Name = "Gotham";
            this.htmlTextBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox10.Value = "1.7 If we have delivered and installed the System, then after the agreement ends " +
    "the guarantees and related terms in clause 11 will continue for the Guarantee Pe" +
    "riod";
            // 
            // htmlTextBox11
            // 
            this.htmlTextBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(93.6D));
            this.htmlTextBox11.Name = "htmlTextBox11";
            this.htmlTextBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(3.136D));
            this.htmlTextBox11.Style.Font.Name = "Gotham";
            this.htmlTextBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox11.Value = resources.GetString("htmlTextBox11.Value");
            // 
            // htmlTextBox12
            // 
            this.htmlTextBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(96.7D));
            this.htmlTextBox12.Name = "htmlTextBox12";
            this.htmlTextBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.htmlTextBox12.Style.Font.Name = "Gotham";
            this.htmlTextBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox12.Value = "1.9 Capitalised terms used in the agreement have the meanings given to them in cl" +
    "ause 16";
            // 
            // textBox125
            // 
            this.textBox125.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(93.1D));
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox125.Style.Color = System.Drawing.Color.Black;
            this.textBox125.Style.Font.Bold = true;
            this.textBox125.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox125.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox125.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox125.Value = "Others";
            // 
            // textBox124
            // 
            this.textBox124.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(97.5D));
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox124.Style.Color = System.Drawing.Color.Black;
            this.textBox124.Style.Font.Bold = true;
            this.textBox124.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox124.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox124.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox124.Value = "2. Sale of the System";
            // 
            // htmlTextBox13
            // 
            this.htmlTextBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(98.1D));
            this.htmlTextBox13.Name = "htmlTextBox13";
            this.htmlTextBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.htmlTextBox13.Style.Font.Name = "Gotham";
            this.htmlTextBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox13.Value = "2.1 Provided the conditions in clause 1.5 have been satisfied, we agree to sell, " +
    "and you agree to purchase, the System on the terms of this agreement.";
            // 
            // textBox123
            // 
            this.textBox123.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(98.9D));
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox123.Style.Color = System.Drawing.Color.Black;
            this.textBox123.Style.Font.Bold = true;
            this.textBox123.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox123.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox123.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox123.Value = "3. Payment";
            // 
            // textBox122
            // 
            this.textBox122.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(99.5D));
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.566D));
            this.textBox122.Style.Color = System.Drawing.Color.Black;
            this.textBox122.Style.Font.Bold = true;
            this.textBox122.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox122.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox122.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox122.Value = "Payment of the Deposit";
            // 
            // textBox121
            // 
            this.textBox121.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(100.1D));
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.82D));
            this.textBox121.Style.Color = System.Drawing.Color.Black;
            this.textBox121.Style.Font.Bold = false;
            this.textBox121.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox121.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox121.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox121.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox121.Value = "3.1 You must pay us the Deposit at the same time as you accept our offer set out " +
    "in the Quote. Payment of the Balance";
            // 
            // textBox120
            // 
            this.textBox120.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(100.959D));
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox120.Style.Color = System.Drawing.Color.Black;
            this.textBox120.Style.Font.Bold = false;
            this.textBox120.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox120.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox120.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox120.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox120.Value = "3.2 You must pay us the Balance at the same time as we deliver the System to the " +
    "Premises";
            // 
            // textBox119
            // 
            this.textBox119.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(101.859D));
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.041D));
            this.textBox119.Style.Color = System.Drawing.Color.Black;
            this.textBox119.Style.Font.Bold = false;
            this.textBox119.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox119.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox119.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox119.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox119.Value = "3.3 Title in the System passes to you on payment of the Balance, provided you hav" +
    "e already paid the Deposit and all other amounts you owe us.";
            // 
            // textBox118
            // 
            this.textBox118.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(102.918D));
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox118.Style.Color = System.Drawing.Color.Black;
            this.textBox118.Style.Font.Bold = true;
            this.textBox118.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox118.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox118.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox118.Value = "Payment methods";
            // 
            // textBox117
            // 
            this.textBox117.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(103.459D));
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.textBox117.Style.Color = System.Drawing.Color.Black;
            this.textBox117.Style.Font.Bold = false;
            this.textBox117.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox117.Value = "3.4 Payments under this agreement can be made by bank cheque, money order, cash, " +
    "debit card, credit card or direct deposit";
            // 
            // textBox116
            // 
            this.textBox116.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(104.659D));
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox116.Style.Color = System.Drawing.Color.Black;
            this.textBox116.Style.Font.Bold = true;
            this.textBox116.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox116.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox116.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox116.Value = "When payment is taken to be made";
            // 
            // textBox115
            // 
            this.textBox115.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(105.159D));
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.82D));
            this.textBox115.Style.Color = System.Drawing.Color.Black;
            this.textBox115.Style.Font.Bold = false;
            this.textBox115.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox115.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox115.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox115.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox115.Value = "3.5 You will be taken to have made payment on the date on which we receive your p" +
    "ayment as cleared funds in our bank account.";
            // 
            // textBox114
            // 
            this.textBox114.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(106.059D));
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox114.Style.Color = System.Drawing.Color.Black;
            this.textBox114.Style.Font.Bold = true;
            this.textBox114.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox114.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox114.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox114.Value = "4. Refunds";
            // 
            // textBox113
            // 
            this.textBox113.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(106.659D));
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.5D));
            this.textBox113.Style.Color = System.Drawing.Color.Black;
            this.textBox113.Style.Font.Bold = false;
            this.textBox113.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox113.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox113.Value = resources.GetString("textBox113.Value");
            // 
            // textBox112
            // 
            this.textBox112.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(108.159D));
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(3.2D));
            this.textBox112.Style.Color = System.Drawing.Color.Black;
            this.textBox112.Style.Font.Bold = false;
            this.textBox112.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox112.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox112.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox112.Value = resources.GetString("textBox112.Value");
            // 
            // textBox111
            // 
            this.textBox111.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(111.359D));
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox111.Style.Color = System.Drawing.Color.Black;
            this.textBox111.Style.Font.Bold = true;
            this.textBox111.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox111.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox111.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox111.Value = "5. Price increases";
            // 
            // htmlTextBox15
            // 
            this.htmlTextBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.459D), Telerik.Reporting.Drawing.Unit.Cm(114.1D));
            this.htmlTextBox15.Name = "htmlTextBox15";
            this.htmlTextBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.5D));
            this.htmlTextBox15.Style.Font.Name = "Gotham";
            this.htmlTextBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox15.Value = resources.GetString("htmlTextBox15.Value");
            // 
            // textBox110
            // 
            this.textBox110.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(111.951D));
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.208D));
            this.textBox110.Style.Color = System.Drawing.Color.Black;
            this.textBox110.Style.Font.Bold = false;
            this.textBox110.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox110.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox110.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox110.Value = resources.GetString("textBox110.Value");
            // 
            // textBox109
            // 
            this.textBox109.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.659D), Telerik.Reporting.Drawing.Unit.Cm(114.159D));
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.741D));
            this.textBox109.Style.Color = System.Drawing.Color.Black;
            this.textBox109.Style.Font.Bold = false;
            this.textBox109.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox109.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox109.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox109.Value = resources.GetString("textBox109.Value");
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.159D), Telerik.Reporting.Drawing.Unit.Cm(91.123D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(26.7D));
            // 
            // htmlTextBox22
            // 
            this.htmlTextBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(120.339D));
            this.htmlTextBox22.Name = "htmlTextBox22";
            this.htmlTextBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.835D));
            this.htmlTextBox22.Style.Font.Name = "Gotham";
            this.htmlTextBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox22.Value = resources.GetString("htmlTextBox22.Value");
            // 
            // htmlTextBox21
            // 
            this.htmlTextBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(122.166D));
            this.htmlTextBox21.Name = "htmlTextBox21";
            this.htmlTextBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.1D));
            this.htmlTextBox21.Style.Font.Name = "Gotham";
            this.htmlTextBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox21.Value = resources.GetString("htmlTextBox21.Value");
            // 
            // htmlTextBox20
            // 
            this.htmlTextBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(124.3D));
            this.htmlTextBox20.Name = "htmlTextBox20";
            this.htmlTextBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.174D));
            this.htmlTextBox20.Style.Font.Name = "Gotham";
            this.htmlTextBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox20.Value = "5.5 If we send you notice of a price increase and you do not end this agreement u" +
    "nder clause 5.3 by the relevant date, you will be taken to have agreed to the pr" +
    "ice increase.";
            // 
            // htmlTextBox19
            // 
            this.htmlTextBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(126.566D));
            this.htmlTextBox19.Name = "htmlTextBox19";
            this.htmlTextBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.533D));
            this.htmlTextBox19.Style.Font.Name = "Gotham";
            this.htmlTextBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox19.Value = resources.GetString("htmlTextBox19.Value");
            // 
            // htmlTextBox18
            // 
            this.htmlTextBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(129.1D));
            this.htmlTextBox18.Name = "htmlTextBox18";
            this.htmlTextBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.774D));
            this.htmlTextBox18.Style.Font.Name = "Gotham";
            this.htmlTextBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox18.Value = "6.2 Your purchase of the System is subject to Grid Connection Approval being gran" +
    "ted.";
            // 
            // textBox175
            // 
            this.textBox175.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(134.833D));
            this.textBox175.Name = "textBox175";
            this.textBox175.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox175.Style.Color = System.Drawing.Color.Black;
            this.textBox175.Style.Font.Bold = true;
            this.textBox175.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox175.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox175.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox175.Value = "7. Delivery and installation";
            // 
            // textBox174
            // 
            this.textBox174.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(137.033D));
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox174.Style.Color = System.Drawing.Color.Black;
            this.textBox174.Style.Font.Bold = false;
            this.textBox174.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox174.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox174.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox174.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox174.Value = "7.2 The risk of loss or theft of, or damage to, the System passes to you on deliv" +
    "ery of the System to the Premises.";
            // 
            // htmlTextBox17
            // 
            this.htmlTextBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(123.166D));
            this.htmlTextBox17.Name = "htmlTextBox17";
            this.htmlTextBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.241D));
            this.htmlTextBox17.Style.Font.Name = "Gotham";
            this.htmlTextBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox17.Value = "7.9 After installation of the System, we will give you any certificate or similar" +
    " document regarding the electrical safety of the System which is required by law" +
    ".";
            // 
            // textBox173
            // 
            this.textBox173.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(127.766D));
            this.textBox173.Name = "textBox173";
            this.textBox173.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox173.Style.Color = System.Drawing.Color.Black;
            this.textBox173.Style.Font.Bold = true;
            this.textBox173.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox173.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox173.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox173.Value = "8. Accessing the Premises";
            // 
            // textBox172
            // 
            this.textBox172.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(128.367D));
            this.textBox172.Name = "textBox172";
            this.textBox172.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.568D));
            this.textBox172.Style.Color = System.Drawing.Color.Black;
            this.textBox172.Style.Font.Bold = false;
            this.textBox172.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox172.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox172.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox172.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox172.Value = resources.GetString("textBox172.Value");
            // 
            // textBox171
            // 
            this.textBox171.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(130.974D));
            this.textBox171.Name = "textBox171";
            this.textBox171.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
            this.textBox171.Style.Color = System.Drawing.Color.Black;
            this.textBox171.Style.Font.Bold = false;
            this.textBox171.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox171.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox171.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox171.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox171.Value = "8.2 You or your representative must be present at the Premises for any site inspe" +
    "ction and for the delivery and installation of the System.";
            // 
            // textBox170
            // 
            this.textBox170.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(134.625D));
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox170.Style.Color = System.Drawing.Color.Black;
            this.textBox170.Style.Font.Bold = true;
            this.textBox170.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox170.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox170.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox170.Value = "9. System maintenance";
            // 
            // textBox169
            // 
            this.textBox169.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(135.166D));
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.508D));
            this.textBox169.Style.Color = System.Drawing.Color.Black;
            this.textBox169.Style.Font.Bold = false;
            this.textBox169.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox169.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox169.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox169.Value = "9.1 We must provide you with the Maintenance Documents.";
            // 
            // textBox168
            // 
            this.textBox168.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.592D), Telerik.Reporting.Drawing.Unit.Cm(135.674D));
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.82D));
            this.textBox168.Style.Color = System.Drawing.Color.Black;
            this.textBox168.Style.Font.Bold = false;
            this.textBox168.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox168.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox168.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox168.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox168.Value = "9.2 It is your responsibility to maintain the System in accordance with these doc" +
    "uments.";
            // 
            // textBox167
            // 
            this.textBox167.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(136.766D));
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox167.Style.Color = System.Drawing.Color.Black;
            this.textBox167.Style.Font.Bold = true;
            this.textBox167.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox167.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox167.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox167.Value = "10. System performance and STCs";
            // 
            // textBox166
            // 
            this.textBox166.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(137.966D));
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.241D));
            this.textBox166.Style.Color = System.Drawing.Color.Black;
            this.textBox166.Style.Font.Bold = false;
            this.textBox166.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox166.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox166.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox166.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox166.Value = "10.1 We have calculated the Site-Specific Performance Estimate for the System and" +
    " your Premises in accordance with the CEC System Design Guidelines.";
            // 
            // shape2
            // 
            this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1D), Telerik.Reporting.Drawing.Unit.Cm(120.374D));
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(26.7D));
            // 
            // textBox165
            // 
            this.textBox165.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(125.433D));
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox165.Style.Color = System.Drawing.Color.Black;
            this.textBox165.Style.Font.Bold = true;
            this.textBox165.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox165.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox165.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox165.Value = "6. Approvals";
            // 
            // textBox164
            // 
            this.textBox164.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.4D), Telerik.Reporting.Drawing.Unit.Cm(126.033D));
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.566D));
            this.textBox164.Style.Color = System.Drawing.Color.Black;
            this.textBox164.Style.Font.Bold = true;
            this.textBox164.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox164.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox164.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox164.Value = "Grid Connection Approval";
            // 
            // htmlTextBox16
            // 
            this.htmlTextBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(129.9D));
            this.htmlTextBox16.Name = "htmlTextBox16";
            this.htmlTextBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.035D));
            this.htmlTextBox16.Style.Font.Name = "Gotham";
            this.htmlTextBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox16.Value = "6.3 If Grid Connection Approval is refused, then this agreement will end and we w" +
    "ill give you any refund required under clause 4.1(c).";
            // 
            // textBox163
            // 
            this.textBox163.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(130.974D));
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox163.Style.Color = System.Drawing.Color.Black;
            this.textBox163.Style.Font.Bold = false;
            this.textBox163.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox163.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox163.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox163.Value = "Other Approvals";
            // 
            // textBox162
            // 
            this.textBox162.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(131.5D));
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.174D));
            this.textBox162.Style.Color = System.Drawing.Color.Black;
            this.textBox162.Style.Font.Bold = false;
            this.textBox162.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox162.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox162.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox162.Value = "6.4 You are responsible for applying for and obtaining any other approvals, permi" +
    "ts or consents required in respect of the installation of the System at the Prem" +
    "ises.";
            // 
            // textBox161
            // 
            this.textBox161.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(132.7D));
            this.textBox161.Name = "textBox161";
            this.textBox161.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.733D));
            this.textBox161.Style.Color = System.Drawing.Color.Black;
            this.textBox161.Style.Font.Bold = false;
            this.textBox161.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox161.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox161.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox161.Value = "6.5 You must apply for these approvals, permits and consents as soon as possible." +
    "";
            // 
            // textBox160
            // 
            this.textBox160.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(133.433D));
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.041D));
            this.textBox160.Style.Color = System.Drawing.Color.Black;
            this.textBox160.Style.Font.Bold = false;
            this.textBox160.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox160.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox160.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox160.Value = resources.GetString("textBox160.Value");
            // 
            // textBox159
            // 
            this.textBox159.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(135.366D));
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox159.Style.Color = System.Drawing.Color.Black;
            this.textBox159.Style.Font.Bold = true;
            this.textBox159.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox159.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox159.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox159.Value = "Delivery";
            // 
            // textBox158
            // 
            this.textBox158.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(135.9D));
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.108D));
            this.textBox158.Style.Color = System.Drawing.Color.Black;
            this.textBox158.Style.Font.Bold = false;
            this.textBox158.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox158.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox158.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox158.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox158.Value = "7.1 Provided the conditions in clause 1.5 have been satisfied, we must, or must p" +
    "rocure, the delivery of the System to the Premises.";
            // 
            // textBox157
            // 
            this.textBox157.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(137.966D));
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox157.Style.Color = System.Drawing.Color.Black;
            this.textBox157.Style.Font.Bold = true;
            this.textBox157.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox157.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox157.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox157.Value = "Installation";
            // 
            // textBox156
            // 
            this.textBox156.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(138.5D));
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.108D));
            this.textBox156.Style.Color = System.Drawing.Color.Black;
            this.textBox156.Style.Font.Bold = false;
            this.textBox156.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox156.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox156.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox156.Value = "7.3 Provided you have paid the Balance, we must install, or must procure the inst" +
    "allation of, the System at the Premises, in accordance with the Full System Desi" +
    "gn.";
            // 
            // textBox155
            // 
            this.textBox155.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(139.632D));
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox155.Style.Color = System.Drawing.Color.Black;
            this.textBox155.Style.Font.Bold = true;
            this.textBox155.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox155.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox155.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox155.Value = "Target Date";
            // 
            // textBox154
            // 
            this.textBox154.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(140.166D));
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.808D));
            this.textBox154.Style.Color = System.Drawing.Color.Black;
            this.textBox154.Style.Font.Bold = false;
            this.textBox154.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox154.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox154.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox154.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox154.Value = "7.4 We will use reasonable endeavours to deliver and install the System at the Pr" +
    "emises on the Target Date.";
            // 
            // textBox153
            // 
            this.textBox153.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(140.966D));
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.508D));
            this.textBox153.Style.Color = System.Drawing.Color.Black;
            this.textBox153.Style.Font.Bold = false;
            this.textBox153.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox153.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox153.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox153.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox153.Value = resources.GetString("textBox153.Value");
            // 
            // textBox152
            // 
            this.textBox152.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(142.366D));
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.266D));
            this.textBox152.Style.Color = System.Drawing.Color.Black;
            this.textBox152.Style.Font.Bold = false;
            this.textBox152.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox152.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox152.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox152.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox152.Value = "7.6 We will notify you if we do not think we can deliver and install the System a" +
    "t the Premises by the Target Date, and give you a new Target Date.";
            // 
            // textBox151
            // 
            this.textBox151.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.392D), Telerik.Reporting.Drawing.Unit.Cm(143.7D));
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.608D));
            this.textBox151.Style.Color = System.Drawing.Color.Black;
            this.textBox151.Style.Font.Bold = false;
            this.textBox151.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox151.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox151.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox151.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox151.Value = resources.GetString("textBox151.Value");
            // 
            // textBox150
            // 
            this.textBox150.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(120.366D));
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox150.Style.Color = System.Drawing.Color.Black;
            this.textBox150.Style.Font.Bold = true;
            this.textBox150.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox150.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox150.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox150.Value = "Installation requirements";
            // 
            // textBox149
            // 
            this.textBox149.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(120.9D));
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.274D));
            this.textBox149.Style.Color = System.Drawing.Color.Black;
            this.textBox149.Style.Font.Bold = false;
            this.textBox149.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox149.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox149.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox149.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox149.Value = resources.GetString("textBox149.Value");
            // 
            // htmlTextBox14
            // 
            this.htmlTextBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.592D), Telerik.Reporting.Drawing.Unit.Cm(124.433D));
            this.htmlTextBox14.Name = "htmlTextBox14";
            this.htmlTextBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(3.141D));
            this.htmlTextBox14.Style.Font.Name = "Gotham";
            this.htmlTextBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox14.Value = resources.GetString("htmlTextBox14.Value");
            // 
            // textBox148
            // 
            this.textBox148.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(132.074D));
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.4D));
            this.textBox148.Style.Color = System.Drawing.Color.Black;
            this.textBox148.Style.Font.Bold = false;
            this.textBox148.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox148.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox148.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox148.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox148.Value = resources.GetString("textBox148.Value");
            // 
            // textBox147
            // 
            this.textBox147.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.592D), Telerik.Reporting.Drawing.Unit.Cm(137.366D));
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox147.Style.Color = System.Drawing.Color.Black;
            this.textBox147.Style.Font.Bold = true;
            this.textBox147.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox147.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox147.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox147.Value = "Site-Specific Performance Estimate";
            // 
            // textBox146
            // 
            this.textBox146.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(139.766D));
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.133D));
            this.textBox146.Style.Color = System.Drawing.Color.Black;
            this.textBox146.Style.Font.Bold = false;
            this.textBox146.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox146.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox146.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox146.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox146.Value = resources.GetString("textBox146.Value");
            // 
            // textBox145
            // 
            this.textBox145.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.592D), Telerik.Reporting.Drawing.Unit.Cm(139.166D));
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox145.Style.Color = System.Drawing.Color.Black;
            this.textBox145.Style.Font.Bold = true;
            this.textBox145.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox145.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox145.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox145.Value = "STC Incentive";
            // 
            // textBox144
            // 
            this.textBox144.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(142.5D));
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(4.008D));
            this.textBox144.Style.Color = System.Drawing.Color.Black;
            this.textBox144.Style.Font.Bold = false;
            this.textBox144.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox144.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox144.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox144.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox144.Value = resources.GetString("textBox144.Value");
            // 
            // textBox143
            // 
            this.textBox143.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.592D), Telerik.Reporting.Drawing.Unit.Cm(141.9D));
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox143.Style.Color = System.Drawing.Color.Black;
            this.textBox143.Style.Font.Bold = true;
            this.textBox143.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox143.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox143.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox143.Value = "Assignment of STCs to us";
            // 
            // htmlTextBox33
            // 
            this.htmlTextBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.504D), Telerik.Reporting.Drawing.Unit.Cm(150.866D));
            this.htmlTextBox33.Name = "htmlTextBox33";
            this.htmlTextBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.608D));
            this.htmlTextBox33.Style.Font.Name = "Gotham";
            this.htmlTextBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox33.Value = resources.GetString("htmlTextBox33.Value");
            // 
            // htmlTextBox32
            // 
            this.htmlTextBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.504D), Telerik.Reporting.Drawing.Unit.Cm(154.333D));
            this.htmlTextBox32.Name = "htmlTextBox32";
            this.htmlTextBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(3.141D));
            this.htmlTextBox32.Style.Font.Name = "Gotham";
            this.htmlTextBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox32.Value = resources.GetString("htmlTextBox32.Value");
            // 
            // textBox198
            // 
            this.textBox198.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.504D), Telerik.Reporting.Drawing.Unit.Cm(164.933D));
            this.textBox198.Name = "textBox198";
            this.textBox198.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.528D));
            this.textBox198.Style.Color = System.Drawing.Color.Black;
            this.textBox198.Style.Font.Bold = false;
            this.textBox198.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox198.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox198.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox198.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox198.Value = resources.GetString("textBox198.Value");
            // 
            // textBox197
            // 
            this.textBox197.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(158.133D));
            this.textBox197.Name = "textBox197";
            this.textBox197.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox197.Style.Color = System.Drawing.Color.Black;
            this.textBox197.Style.Font.Bold = true;
            this.textBox197.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox197.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox197.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox197.Value = "13.Privacy";
            // 
            // textBox196
            // 
            this.textBox196.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(158.733D));
            this.textBox196.Name = "textBox196";
            this.textBox196.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.568D));
            this.textBox196.Style.Color = System.Drawing.Color.Black;
            this.textBox196.Style.Font.Bold = false;
            this.textBox196.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox196.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox196.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox196.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox196.Value = resources.GetString("textBox196.Value");
            // 
            // textBox195
            // 
            this.textBox195.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(161.474D));
            this.textBox195.Name = "textBox195";
            this.textBox195.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox195.Style.Color = System.Drawing.Color.Black;
            this.textBox195.Style.Font.Bold = true;
            this.textBox195.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox195.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox195.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox195.Value = "14.What happens if you fail to perform this agreement";
            // 
            // textBox194
            // 
            this.textBox194.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(162.015D));
            this.textBox194.Name = "textBox194";
            this.textBox194.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(4.446D));
            this.textBox194.Style.Color = System.Drawing.Color.Black;
            this.textBox194.Style.Font.Bold = false;
            this.textBox194.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox194.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox194.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox194.Value = resources.GetString("textBox194.Value");
            // 
            // textBox193
            // 
            this.textBox193.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(166.666D));
            this.textBox193.Name = "textBox193";
            this.textBox193.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox193.Style.Color = System.Drawing.Color.Black;
            this.textBox193.Style.Font.Bold = true;
            this.textBox193.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox193.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox193.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox193.Value = "15.GST";
            // 
            // textBox192
            // 
            this.textBox192.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(167.266D));
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.608D));
            this.textBox192.Style.Color = System.Drawing.Color.Black;
            this.textBox192.Style.Font.Bold = false;
            this.textBox192.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox192.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox192.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox192.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox192.Value = "15.1 All amounts specified in the Quote are inclusive of GST.";
            // 
            // shape3
            // 
            this.shape3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.204D), Telerik.Reporting.Drawing.Unit.Cm(150.274D));
            this.shape3.Name = "shape3";
            this.shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
            this.shape3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(26.7D));
            // 
            // textBox191
            // 
            this.textBox191.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.504D), Telerik.Reporting.Drawing.Unit.Cm(153.733D));
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox191.Style.Color = System.Drawing.Color.Black;
            this.textBox191.Style.Font.Bold = true;
            this.textBox191.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox191.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox191.Value = "11. System guarantees";
            // 
            // textBox190
            // 
            this.textBox190.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.496D), Telerik.Reporting.Drawing.Unit.Cm(166.8D));
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox190.Style.Color = System.Drawing.Color.Black;
            this.textBox190.Style.Font.Bold = true;
            this.textBox190.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox190.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox190.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox190.Value = "12. System guarantees";
            // 
            // textBox189
            // 
            this.textBox189.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.496D), Telerik.Reporting.Drawing.Unit.Cm(167.333D));
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox189.Style.Color = System.Drawing.Color.Black;
            this.textBox189.Style.Font.Bold = true;
            this.textBox189.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox189.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox189.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox189.Value = "Making a complaint";
            // 
            // textBox188
            // 
            this.textBox188.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.496D), Telerik.Reporting.Drawing.Unit.Cm(167.867D));
            this.textBox188.Name = "textBox188";
            this.textBox188.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.107D));
            this.textBox188.Style.Color = System.Drawing.Color.Black;
            this.textBox188.Style.Font.Bold = false;
            this.textBox188.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox188.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox188.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox188.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox188.Value = resources.GetString("textBox188.Value");
            // 
            // textBox187
            // 
            this.textBox187.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.496D), Telerik.Reporting.Drawing.Unit.Cm(172.666D));
            this.textBox187.Name = "textBox187";
            this.textBox187.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.508D));
            this.textBox187.Style.Color = System.Drawing.Color.Black;
            this.textBox187.Style.Font.Bold = false;
            this.textBox187.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox187.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox187.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox187.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox187.Value = "12.3    If you are not satisfied with the outcome of your complaint, you can refe" +
    "r the complaint to with the relevant Fair Trading or Consumer Affairs office in " +
    "your state or territory, as follows:";
            // 
            // htmlTextBox31
            // 
            this.htmlTextBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.696D), Telerik.Reporting.Drawing.Unit.Cm(150.333D));
            this.htmlTextBox31.Name = "htmlTextBox31";
            this.htmlTextBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.708D), Telerik.Reporting.Drawing.Unit.Cm(1.841D));
            this.htmlTextBox31.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.htmlTextBox31.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.htmlTextBox31.Style.BorderColor.Right = System.Drawing.Color.Black;
            this.htmlTextBox31.Style.BorderColor.Top = System.Drawing.Color.Black;
            this.htmlTextBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox31.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox31.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox31.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox31.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox31.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox31.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox31.Style.Font.Name = "Gotham";
            this.htmlTextBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox31.Value = "<p><strong>ACT: Office of Regulatory Services </strong></p><p><strong>Phone: (02)" +
    " 6207 3000 </strong></p>";
            // 
            // textBox186
            // 
            this.textBox186.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(169.4D));
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(3.6D));
            this.textBox186.Style.Color = System.Drawing.Color.Black;
            this.textBox186.Style.Font.Bold = false;
            this.textBox186.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox186.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox186.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox186.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox186.Value = resources.GetString("textBox186.Value");
            // 
            // textBox185
            // 
            this.textBox185.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.696D), Telerik.Reporting.Drawing.Unit.Cm(168.2D));
            this.textBox185.Name = "textBox185";
            this.textBox185.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox185.Style.Color = System.Drawing.Color.Black;
            this.textBox185.Style.Font.Bold = true;
            this.textBox185.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox185.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox185.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox185.Value = "16.General";
            // 
            // textBox184
            // 
            this.textBox184.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.504D), Telerik.Reporting.Drawing.Unit.Cm(150.266D));
            this.textBox184.Name = "textBox184";
            this.textBox184.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox184.Style.Color = System.Drawing.Color.Black;
            this.textBox184.Style.Font.Bold = true;
            this.textBox184.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox184.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox184.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox184.Value = "Charging you the STC Incentive";
            // 
            // htmlTextBox30
            // 
            this.htmlTextBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.496D), Telerik.Reporting.Drawing.Unit.Cm(157.533D));
            this.htmlTextBox30.Name = "htmlTextBox30";
            this.htmlTextBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(4.208D));
            this.htmlTextBox30.Style.Font.Name = "Gotham";
            this.htmlTextBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox30.Value = resources.GetString("htmlTextBox30.Value");
            // 
            // textBox183
            // 
            this.textBox183.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.496D), Telerik.Reporting.Drawing.Unit.Cm(161.741D));
            this.textBox183.Name = "textBox183";
            this.textBox183.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(3.125D));
            this.textBox183.Style.Color = System.Drawing.Color.Black;
            this.textBox183.Style.Font.Bold = false;
            this.textBox183.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox183.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox183.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox183.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox183.Value = resources.GetString("textBox183.Value");
            // 
            // textBox182
            // 
            this.textBox182.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.496D), Telerik.Reporting.Drawing.Unit.Cm(170D));
            this.textBox182.Name = "textBox182";
            this.textBox182.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(2.107D));
            this.textBox182.Style.Color = System.Drawing.Color.Black;
            this.textBox182.Style.Font.Bold = false;
            this.textBox182.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox182.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox182.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox182.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox182.Value = resources.GetString("textBox182.Value");
            // 
            // textBox181
            // 
            this.textBox181.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.496D), Telerik.Reporting.Drawing.Unit.Cm(172.133D));
            this.textBox181.Name = "textBox181";
            this.textBox181.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox181.Style.Color = System.Drawing.Color.Black;
            this.textBox181.Style.Font.Bold = true;
            this.textBox181.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox181.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox181.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox181.Value = "If you are still not satisfied";
            // 
            // htmlTextBox29
            // 
            this.htmlTextBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.763D), Telerik.Reporting.Drawing.Unit.Cm(150.333D));
            this.htmlTextBox29.Name = "htmlTextBox29";
            this.htmlTextBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.708D), Telerik.Reporting.Drawing.Unit.Cm(1.841D));
            this.htmlTextBox29.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.htmlTextBox29.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.htmlTextBox29.Style.BorderColor.Right = System.Drawing.Color.Black;
            this.htmlTextBox29.Style.BorderColor.Top = System.Drawing.Color.Black;
            this.htmlTextBox29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox29.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox29.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox29.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox29.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox29.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox29.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox29.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox29.Style.Font.Bold = true;
            this.htmlTextBox29.Style.Font.Name = "Gotham";
            this.htmlTextBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox29.Value = "<p>SA: Consumer and Business Services </p><p>Phone: 13 18 82</p>";
            // 
            // htmlTextBox28
            // 
            this.htmlTextBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(152.492D));
            this.htmlTextBox28.Name = "htmlTextBox28";
            this.htmlTextBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.708D), Telerik.Reporting.Drawing.Unit.Cm(1.841D));
            this.htmlTextBox28.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.htmlTextBox28.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.htmlTextBox28.Style.BorderColor.Right = System.Drawing.Color.Black;
            this.htmlTextBox28.Style.BorderColor.Top = System.Drawing.Color.Black;
            this.htmlTextBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox28.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox28.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox28.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox28.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox28.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox28.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox28.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox28.Style.Font.Bold = true;
            this.htmlTextBox28.Style.Font.Name = "Gotham";
            this.htmlTextBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox28.Value = "<p>NSW: Fair Trading </p><p>Phone: (02) 6207 3000 </p>";
            // 
            // htmlTextBox27
            // 
            this.htmlTextBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.763D), Telerik.Reporting.Drawing.Unit.Cm(152.492D));
            this.htmlTextBox27.Name = "htmlTextBox27";
            this.htmlTextBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.708D), Telerik.Reporting.Drawing.Unit.Cm(1.841D));
            this.htmlTextBox27.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.htmlTextBox27.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.htmlTextBox27.Style.BorderColor.Right = System.Drawing.Color.Black;
            this.htmlTextBox27.Style.BorderColor.Top = System.Drawing.Color.Black;
            this.htmlTextBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox27.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox27.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox27.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox27.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox27.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox27.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox27.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox27.Style.Font.Bold = true;
            this.htmlTextBox27.Style.Font.Name = "Gotham";
            this.htmlTextBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox27.Value = "<p>Tas: Consumer Affairs and Fair Trading </p><p>Phone: 1300 654 499 </p>";
            // 
            // htmlTextBox26
            // 
            this.htmlTextBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(154.574D));
            this.htmlTextBox26.Name = "htmlTextBox26";
            this.htmlTextBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.708D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.htmlTextBox26.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.htmlTextBox26.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.htmlTextBox26.Style.BorderColor.Right = System.Drawing.Color.Black;
            this.htmlTextBox26.Style.BorderColor.Top = System.Drawing.Color.Black;
            this.htmlTextBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox26.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox26.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox26.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox26.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox26.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox26.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox26.Style.Font.Bold = true;
            this.htmlTextBox26.Style.Font.Name = "Gotham";
            this.htmlTextBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox26.Value = "<p>NT: Consumer Affairs </p><p>Phone: 1800 019 319 </p>";
            // 
            // htmlTextBox25
            // 
            this.htmlTextBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.763D), Telerik.Reporting.Drawing.Unit.Cm(154.574D));
            this.htmlTextBox25.Name = "htmlTextBox25";
            this.htmlTextBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.708D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
            this.htmlTextBox25.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.htmlTextBox25.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.htmlTextBox25.Style.BorderColor.Right = System.Drawing.Color.Black;
            this.htmlTextBox25.Style.BorderColor.Top = System.Drawing.Color.Black;
            this.htmlTextBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox25.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox25.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox25.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox25.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox25.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox25.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox25.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox25.Style.Font.Bold = true;
            this.htmlTextBox25.Style.Font.Name = "Gotham";
            this.htmlTextBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox25.Value = "<p>Vic: Consumer Affairs </p><p>Phone: 1300 558 181 </p>";
            // 
            // htmlTextBox24
            // 
            this.htmlTextBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(156.2D));
            this.htmlTextBox24.Name = "htmlTextBox24";
            this.htmlTextBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.708D), Telerik.Reporting.Drawing.Unit.Cm(1.774D));
            this.htmlTextBox24.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.htmlTextBox24.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.htmlTextBox24.Style.BorderColor.Right = System.Drawing.Color.Black;
            this.htmlTextBox24.Style.BorderColor.Top = System.Drawing.Color.Black;
            this.htmlTextBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox24.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox24.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox24.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox24.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox24.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox24.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox24.Style.Font.Bold = true;
            this.htmlTextBox24.Style.Font.Name = "Gotham";
            this.htmlTextBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox24.Value = "<p>Qld: Office of Fair Trading </p><p>Phone: 13 74 68 </p>";
            // 
            // htmlTextBox23
            // 
            this.htmlTextBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.763D), Telerik.Reporting.Drawing.Unit.Cm(156.2D));
            this.htmlTextBox23.Name = "htmlTextBox23";
            this.htmlTextBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.708D), Telerik.Reporting.Drawing.Unit.Cm(1.774D));
            this.htmlTextBox23.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.htmlTextBox23.Style.BorderColor.Left = System.Drawing.Color.Black;
            this.htmlTextBox23.Style.BorderColor.Right = System.Drawing.Color.Black;
            this.htmlTextBox23.Style.BorderColor.Top = System.Drawing.Color.Black;
            this.htmlTextBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox23.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox23.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox23.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox23.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox23.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox23.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.htmlTextBox23.Style.Font.Bold = true;
            this.htmlTextBox23.Style.Font.Name = "Gotham";
            this.htmlTextBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox23.Value = "<p>WA: Consumer Protection </p><p>Phone: 1300 304 054 </p>";
            // 
            // textBox180
            // 
            this.textBox180.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.696D), Telerik.Reporting.Drawing.Unit.Cm(168.8D));
            this.textBox180.Name = "textBox180";
            this.textBox180.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox180.Style.Color = System.Drawing.Color.Black;
            this.textBox180.Style.Font.Bold = true;
            this.textBox180.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox180.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox180.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox180.Value = "Notices";
            // 
            // textBox179
            // 
            this.textBox179.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.704D), Telerik.Reporting.Drawing.Unit.Cm(173.6D));
            this.textBox179.Name = "textBox179";
            this.textBox179.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.674D));
            this.textBox179.Style.Color = System.Drawing.Color.Black;
            this.textBox179.Style.Font.Bold = false;
            this.textBox179.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox179.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox179.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox179.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox179.Value = "16.3 Neither party can assign its rights or novate its obligations under this agr" +
    "eement without the other party\'s prior written consent, not to be unreasonably w" +
    "ithheld or delayed.";
            // 
            // textBox178
            // 
            this.textBox178.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.696D), Telerik.Reporting.Drawing.Unit.Cm(173D));
            this.textBox178.Name = "textBox178";
            this.textBox178.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox178.Style.Color = System.Drawing.Color.Black;
            this.textBox178.Style.Font.Bold = true;
            this.textBox178.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox178.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox178.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox178.Value = "Assignment and novation of the agreement";
            // 
            // htmlTextBox38
            // 
            this.htmlTextBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(180.726D));
            this.htmlTextBox38.Name = "htmlTextBox38";
            this.htmlTextBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(4.508D));
            this.htmlTextBox38.Style.Font.Name = "Gotham";
            this.htmlTextBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox38.Value = resources.GetString("htmlTextBox38.Value");
            // 
            // textBox218
            // 
            this.textBox218.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(180.2D));
            this.textBox218.Name = "textBox218";
            this.textBox218.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
            this.textBox218.Style.Color = System.Drawing.Color.Black;
            this.textBox218.Style.Font.Bold = false;
            this.textBox218.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox218.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox218.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox218.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox218.Value = "GST has the meaning given in the A New Tax System (Goods and Services Tax) Act 19" +
    "99 (Cth).";
            // 
            // shape4
            // 
            this.shape4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.2D), Telerik.Reporting.Drawing.Unit.Cm(180.2D));
            this.shape4.Name = "shape4";
            this.shape4.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
            this.shape4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(26.7D));
            // 
            // textBox217
            // 
            this.textBox217.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.492D), Telerik.Reporting.Drawing.Unit.Cm(192.9D));
            this.textBox217.Name = "textBox217";
            this.textBox217.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox217.Style.Color = System.Drawing.Color.Black;
            this.textBox217.Style.Font.Bold = true;
            this.textBox217.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox217.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox217.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox217.Value = "17.Meaning of capitalised terms in this agreement";
            // 
            // textBox216
            // 
            this.textBox216.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(193.459D));
            this.textBox216.Name = "textBox216";
            this.textBox216.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.241D));
            this.textBox216.Style.Color = System.Drawing.Color.Black;
            this.textBox216.Style.Font.Bold = false;
            this.textBox216.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox216.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox216.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox216.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox216.Value = "Australian Consumer Law means the Australian Consumer Law as set out in Schedule " +
    "2 to the Competition and Consumer Act 2010 (Cth).";
            // 
            // textBox215
            // 
            this.textBox215.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(180.192D));
            this.textBox215.Name = "textBox215";
            this.textBox215.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox215.Style.Color = System.Drawing.Color.Black;
            this.textBox215.Style.Font.Bold = true;
            this.textBox215.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox215.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox215.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox215.Value = "Sub-contracting";
            // 
            // htmlTextBox37
            // 
            this.htmlTextBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(185.768D));
            this.htmlTextBox37.Name = "htmlTextBox37";
            this.htmlTextBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.832D));
            this.htmlTextBox37.Style.Font.Name = "Gotham";
            this.htmlTextBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox37.Value = "16.5 This Agreement can only be amended in writing signed by both parties.";
            // 
            // textBox214
            // 
            this.textBox214.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(185.234D));
            this.textBox214.Name = "textBox214";
            this.textBox214.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox214.Style.Color = System.Drawing.Color.Black;
            this.textBox214.Style.Font.Bold = true;
            this.textBox214.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox214.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox214.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox214.Value = "Amendment of the agreement";
            // 
            // htmlTextBox36
            // 
            this.htmlTextBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.492D), Telerik.Reporting.Drawing.Unit.Cm(187.193D));
            this.htmlTextBox36.Name = "htmlTextBox36";
            this.htmlTextBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.207D));
            this.htmlTextBox36.Style.Font.Name = "Gotham";
            this.htmlTextBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox36.Value = "16.6 A waiver in connection with this agreement is not valid or binding on the pa" +
    "rty granting that waiver unless made in writing by that party.";
            // 
            // textBox213
            // 
            this.textBox213.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.492D), Telerik.Reporting.Drawing.Unit.Cm(186.659D));
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox213.Style.Color = System.Drawing.Color.Black;
            this.textBox213.Style.Font.Bold = true;
            this.textBox213.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox213.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox213.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox213.Value = "Waivers";
            // 
            // htmlTextBox35
            // 
            this.htmlTextBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(188.9D));
            this.htmlTextBox35.Name = "htmlTextBox35";
            this.htmlTextBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.207D));
            this.htmlTextBox35.Style.Font.Name = "Gotham";
            this.htmlTextBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox35.Value = "16.7 Any term of this agreement which is or becomes invalid or unenforceable does" +
    " not render the other terms of the agreement invalid or unenforceable.";
            // 
            // textBox212
            // 
            this.textBox212.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(188.366D));
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox212.Style.Color = System.Drawing.Color.Black;
            this.textBox212.Style.Font.Bold = true;
            this.textBox212.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox212.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox212.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox212.Value = "Severance";
            // 
            // htmlTextBox34
            // 
            this.htmlTextBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(191.059D));
            this.htmlTextBox34.Name = "htmlTextBox34";
            this.htmlTextBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.541D));
            this.htmlTextBox34.Style.Font.Name = "Gotham";
            this.htmlTextBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.htmlTextBox34.Value = resources.GetString("htmlTextBox34.Value");
            // 
            // textBox211
            // 
            this.textBox211.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(190.159D));
            this.textBox211.Name = "textBox211";
            this.textBox211.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.841D));
            this.textBox211.Style.Color = System.Drawing.Color.Black;
            this.textBox211.Style.Font.Bold = true;
            this.textBox211.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox211.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox211.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox211.Value = "Governing law of the agreement and submission to jurisdiction";
            // 
            // textBox210
            // 
            this.textBox210.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.492D), Telerik.Reporting.Drawing.Unit.Cm(194.659D));
            this.textBox210.Name = "textBox210";
            this.textBox210.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.241D));
            this.textBox210.Style.Color = System.Drawing.Color.Black;
            this.textBox210.Style.Font.Bold = false;
            this.textBox210.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox210.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox210.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox210.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox210.Value = "Balance means the amount specified as such in the Quote, subject to any adjustmen" +
    "t of this amount in accordance with clause 5.1 or 10.6.";
            // 
            // textBox209
            // 
            this.textBox209.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.492D), Telerik.Reporting.Drawing.Unit.Cm(195.926D));
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.241D));
            this.textBox209.Style.Color = System.Drawing.Color.Black;
            this.textBox209.Style.Font.Bold = false;
            this.textBox209.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox209.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox209.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox209.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox209.Value = "Business Day means a day which is not a Saturday, Sunday or public holiday in the" +
    " State or Territory in which the Premises are located.";
            // 
            // textBox208
            // 
            this.textBox208.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(197.167D));
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.492D));
            this.textBox208.Style.Color = System.Drawing.Color.Black;
            this.textBox208.Style.Font.Bold = false;
            this.textBox208.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox208.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox208.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox208.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox208.Value = resources.GetString("textBox208.Value");
            // 
            // textBox207
            // 
            this.textBox207.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(198.659D));
            this.textBox207.Name = "textBox207";
            this.textBox207.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.941D));
            this.textBox207.Style.Color = System.Drawing.Color.Black;
            this.textBox207.Style.Font.Bold = false;
            this.textBox207.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox207.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox207.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox207.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox207.Value = "CEC System Design Guidelines means the Clean Energy Council System Design Guideli" +
    "nes for Accredited Designers.";
            // 
            // textBox206
            // 
            this.textBox206.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(199.6D));
            this.textBox206.Name = "textBox206";
            this.textBox206.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.941D));
            this.textBox206.Style.Color = System.Drawing.Color.Black;
            this.textBox206.Style.Font.Bold = false;
            this.textBox206.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox206.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox206.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox206.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox206.Value = "Deposit means the amount specified as such in the Quote, subject to any adjustmen" +
    "t of this amount in accordance with clause 5.1 or 10.6.";
            // 
            // textBox205
            // 
            this.textBox205.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(200.541D));
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.459D));
            this.textBox205.Style.Color = System.Drawing.Color.Black;
            this.textBox205.Style.Font.Bold = false;
            this.textBox205.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox205.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox205.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox205.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox205.Value = resources.GetString("textBox205.Value");
            // 
            // textBox204
            // 
            this.textBox204.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(202D));
            this.textBox204.Name = "textBox204";
            this.textBox204.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
            this.textBox204.Style.Color = System.Drawing.Color.Black;
            this.textBox204.Style.Font.Bold = false;
            this.textBox204.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox204.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox204.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox204.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox204.Value = "Grid Connection Approval means approval from your electricity distributor for the" +
    " connection of the System to the electricity grid at the Premises.";
            // 
            // textBox203
            // 
            this.textBox203.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(181.3D));
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox203.Style.Color = System.Drawing.Color.Black;
            this.textBox203.Style.Font.Bold = false;
            this.textBox203.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox203.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox203.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox203.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox203.Value = "Guarantee Period has the meaning given to it in clause 11.1";
            // 
            // textBox202
            // 
            this.textBox202.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(181.9D));
            this.textBox202.Name = "textBox202";
            this.textBox202.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox202.Style.Color = System.Drawing.Color.Black;
            this.textBox202.Style.Font.Bold = false;
            this.textBox202.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox202.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox202.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox202.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox202.Value = "Maintenance Documents means the System maintenance documents listed in Attachment" +
    " 2 to this agreement.";
            // 
            // textBox201
            // 
            this.textBox201.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.7D), Telerik.Reporting.Drawing.Unit.Cm(183D));
            this.textBox201.Name = "textBox201";
            this.textBox201.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox201.Style.Color = System.Drawing.Color.Black;
            this.textBox201.Style.Font.Bold = false;
            this.textBox201.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox201.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox201.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox201.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox201.Value = "Guarantee Period has the meaning given to it in clause 11.1";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(59.346D));
            this.pictureBox5.MimeType = "";
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox5.Value = "";
            // 
            // textBox108
            // 
            this.textBox108.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5D), Telerik.Reporting.Drawing.Unit.Cm(86.6D));
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.4D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.textBox108.Style.Color = System.Drawing.Color.White;
            this.textBox108.Style.Font.Bold = true;
            this.textBox108.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox108.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox108.Value = "Next to Meterbox, on the North Facing wall, Sunshade wil be Installed to protect " +
    "the inverter from direct sunlight";
            // 
            // textBox107
            // 
            this.textBox107.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.1D), Telerik.Reporting.Drawing.Unit.Cm(88.2D));
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.4D), Telerik.Reporting.Drawing.Unit.Cm(0.846D));
            this.textBox107.Style.Color = System.Drawing.Color.White;
            this.textBox107.Style.Font.Bold = true;
            this.textBox107.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox107.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox107.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox107.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox107.Value = "(c) Clean Energy Council Limited 2016. For use by permitted only. Terms of use ap" +
    "ply";
            // 
            // textBox106
            // 
            this.textBox106.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(59.7D));
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.4D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox106.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox106.Style.Font.Bold = true;
            this.textBox106.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox106.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox106.Value = "System Design";
            // 
            // textBox105
            // 
            this.textBox105.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.6D), Telerik.Reporting.Drawing.Unit.Cm(86.5D));
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox105.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox105.Style.Font.Bold = true;
            this.textBox105.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox105.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox105.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox105.Value = "Inverter Location";
            // 
            // textBox140
            // 
            this.textBox140.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(3.1D));
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.959D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox140.Style.Font.Bold = true;
            this.textBox140.Style.Font.Name = "Verdana";
            this.textBox140.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox140.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox140.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox140.Value = "";
            // 
            // textBox142
            // 
            this.textBox142.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(10.159D));
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.612D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox142.Style.Font.Bold = true;
            this.textBox142.Style.Font.Name = "Verdana";
            this.textBox142.Value = "textBox142";
            // 
            // textBox176
            // 
            this.textBox176.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.826D), Telerik.Reporting.Drawing.Unit.Cm(10.8D));
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.574D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox176.Style.Font.Bold = true;
            this.textBox176.Style.Font.Name = "Verdana";
            this.textBox176.Value = "textBox176";
            // 
            // textBox177
            // 
            this.textBox177.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(9.5D));
            this.textBox177.Name = "textBox177";
            this.textBox177.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.659D));
            this.textBox177.Style.Font.Bold = true;
            this.textBox177.Style.Font.Name = "Verdana";
            this.textBox177.Value = "textBox177";
            // 
            // textBox199
            // 
            this.textBox199.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.7D), Telerik.Reporting.Drawing.Unit.Cm(51D));
            this.textBox199.Name = "textBox199";
            this.textBox199.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox199.Style.Font.Bold = true;
            this.textBox199.Style.Font.Name = "Verdana";
            this.textBox199.Value = "textBox199";
            // 
            // textBox200
            // 
            this.textBox200.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.7D), Telerik.Reporting.Drawing.Unit.Cm(52.2D));
            this.textBox200.Name = "textBox200";
            this.textBox200.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.7D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox200.Style.Font.Bold = true;
            this.textBox200.Style.Font.Name = "Verdana";
            this.textBox200.Value = "textBox200";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.7D), Telerik.Reporting.Drawing.Unit.Cm(53.1D));
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.104D), Telerik.Reporting.Drawing.Unit.Cm(2.9D));
            // 
            // pictureBox11
            // 
            this.pictureBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.259D), Telerik.Reporting.Drawing.Unit.Cm(61D));
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.1D), Telerik.Reporting.Drawing.Unit.Cm(12D));
            this.pictureBox11.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11D), Telerik.Reporting.Drawing.Unit.Cm(61D));
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(12D));
            this.pictureBox12.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.139D), Telerik.Reporting.Drawing.Unit.Cm(73.5D));
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.4D), Telerik.Reporting.Drawing.Unit.Cm(12D));
            this.pictureBox13.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            // 
            // textBox219
            // 
            this.textBox219.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.3D), Telerik.Reporting.Drawing.Unit.Cm(16.359D));
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.959D), Telerik.Reporting.Drawing.Unit.Cm(2.921D));
            this.textBox219.Style.Font.Name = "Verdana";
            this.textBox219.Value = "";
            // 
            // textBox220
            // 
            this.textBox220.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
            this.textBox220.Style.Font.Bold = true;
            this.textBox220.Style.Font.Name = "Verdana";
            this.textBox220.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox220.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox220.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox220.Value = "";
            // 
            // SavvyQuote
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detailSection1});
            this.Name = "Report1";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(21D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detailSection1;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.PictureBox pictureBox10;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.PictureBox pictureBox8;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.PictureBox pictureBox9;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox141;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.HtmlTextBox htmlTextBox4;
        private Telerik.Reporting.HtmlTextBox htmlTextBox5;
        private Telerik.Reporting.HtmlTextBox htmlTextBox6;
        private Telerik.Reporting.HtmlTextBox htmlTextBox7;
        private Telerik.Reporting.HtmlTextBox htmlTextBox8;
        private Telerik.Reporting.HtmlTextBox htmlTextBox9;
        private Telerik.Reporting.TextBox textBox139;
        private Telerik.Reporting.TextBox textBox138;
        private Telerik.Reporting.TextBox textBox137;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.HtmlTextBox htmlTextBox10;
        private Telerik.Reporting.HtmlTextBox htmlTextBox11;
        private Telerik.Reporting.HtmlTextBox htmlTextBox12;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox124;
        private Telerik.Reporting.HtmlTextBox htmlTextBox13;
        private Telerik.Reporting.TextBox textBox123;
        private Telerik.Reporting.TextBox textBox122;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.TextBox textBox120;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.HtmlTextBox htmlTextBox15;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox22;
        private Telerik.Reporting.HtmlTextBox htmlTextBox21;
        private Telerik.Reporting.HtmlTextBox htmlTextBox20;
        private Telerik.Reporting.HtmlTextBox htmlTextBox19;
        private Telerik.Reporting.HtmlTextBox htmlTextBox18;
        private Telerik.Reporting.TextBox textBox175;
        private Telerik.Reporting.TextBox textBox174;
        private Telerik.Reporting.HtmlTextBox htmlTextBox17;
        private Telerik.Reporting.TextBox textBox173;
        private Telerik.Reporting.TextBox textBox172;
        private Telerik.Reporting.TextBox textBox171;
        private Telerik.Reporting.TextBox textBox170;
        private Telerik.Reporting.TextBox textBox169;
        private Telerik.Reporting.TextBox textBox168;
        private Telerik.Reporting.TextBox textBox167;
        private Telerik.Reporting.TextBox textBox166;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox textBox165;
        private Telerik.Reporting.TextBox textBox164;
        private Telerik.Reporting.HtmlTextBox htmlTextBox16;
        private Telerik.Reporting.TextBox textBox163;
        private Telerik.Reporting.TextBox textBox162;
        private Telerik.Reporting.TextBox textBox161;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.TextBox textBox159;
        private Telerik.Reporting.TextBox textBox158;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox155;
        private Telerik.Reporting.TextBox textBox154;
        private Telerik.Reporting.TextBox textBox153;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox151;
        private Telerik.Reporting.TextBox textBox150;
        private Telerik.Reporting.TextBox textBox149;
        private Telerik.Reporting.HtmlTextBox htmlTextBox14;
        private Telerik.Reporting.TextBox textBox148;
        private Telerik.Reporting.TextBox textBox147;
        private Telerik.Reporting.TextBox textBox146;
        private Telerik.Reporting.TextBox textBox145;
        private Telerik.Reporting.TextBox textBox144;
        private Telerik.Reporting.TextBox textBox143;
        private Telerik.Reporting.HtmlTextBox htmlTextBox33;
        private Telerik.Reporting.HtmlTextBox htmlTextBox32;
        private Telerik.Reporting.TextBox textBox198;
        private Telerik.Reporting.TextBox textBox197;
        private Telerik.Reporting.TextBox textBox196;
        private Telerik.Reporting.TextBox textBox195;
        private Telerik.Reporting.TextBox textBox194;
        private Telerik.Reporting.TextBox textBox193;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.Shape shape3;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox188;
        private Telerik.Reporting.TextBox textBox187;
        private Telerik.Reporting.HtmlTextBox htmlTextBox31;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox185;
        private Telerik.Reporting.TextBox textBox184;
        private Telerik.Reporting.HtmlTextBox htmlTextBox30;
        private Telerik.Reporting.TextBox textBox183;
        private Telerik.Reporting.TextBox textBox182;
        private Telerik.Reporting.TextBox textBox181;
        private Telerik.Reporting.HtmlTextBox htmlTextBox29;
        private Telerik.Reporting.HtmlTextBox htmlTextBox28;
        private Telerik.Reporting.HtmlTextBox htmlTextBox27;
        private Telerik.Reporting.HtmlTextBox htmlTextBox26;
        private Telerik.Reporting.HtmlTextBox htmlTextBox25;
        private Telerik.Reporting.HtmlTextBox htmlTextBox24;
        private Telerik.Reporting.HtmlTextBox htmlTextBox23;
        private Telerik.Reporting.TextBox textBox180;
        private Telerik.Reporting.TextBox textBox179;
        private Telerik.Reporting.TextBox textBox178;
        private Telerik.Reporting.HtmlTextBox htmlTextBox38;
        private Telerik.Reporting.TextBox textBox218;
        private Telerik.Reporting.Shape shape4;
        private Telerik.Reporting.TextBox textBox217;
        private Telerik.Reporting.TextBox textBox216;
        private Telerik.Reporting.TextBox textBox215;
        private Telerik.Reporting.HtmlTextBox htmlTextBox37;
        private Telerik.Reporting.TextBox textBox214;
        private Telerik.Reporting.HtmlTextBox htmlTextBox36;
        private Telerik.Reporting.TextBox textBox213;
        private Telerik.Reporting.HtmlTextBox htmlTextBox35;
        private Telerik.Reporting.TextBox textBox212;
        private Telerik.Reporting.HtmlTextBox htmlTextBox34;
        private Telerik.Reporting.TextBox textBox211;
        private Telerik.Reporting.TextBox textBox210;
        private Telerik.Reporting.TextBox textBox209;
        private Telerik.Reporting.TextBox textBox208;
        private Telerik.Reporting.TextBox textBox207;
        private Telerik.Reporting.TextBox textBox206;
        private Telerik.Reporting.TextBox textBox205;
        private Telerik.Reporting.TextBox textBox204;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox202;
        private Telerik.Reporting.TextBox textBox201;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox140;
        private Telerik.Reporting.TextBox textBox142;
        private Telerik.Reporting.TextBox textBox176;
        private Telerik.Reporting.TextBox textBox177;
        private Telerik.Reporting.TextBox textBox199;
        private Telerik.Reporting.TextBox textBox200;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.PictureBox pictureBox11;
        private Telerik.Reporting.PictureBox pictureBox12;
        private Telerik.Reporting.PictureBox pictureBox13;
        private Telerik.Reporting.TextBox textBox219;
        private Telerik.Reporting.TextBox textBox220;
    }
}